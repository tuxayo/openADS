<?php
//$Id: dossier_autorisation_type_detaille.class.php 5839 2016-01-29 08:50:12Z fmichon $ 
//gen openMairie le 29/01/2016 09:39

require_once "../gen/obj/dossier_autorisation_type_detaille.class.php";

class dossier_autorisation_type_detaille extends dossier_autorisation_type_detaille_gen {


    function __construct($id) {
        $this->constructeur($id);
    }


    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 004 - get_affichage_form
        //
        $this->class_actions[4] = array(
            "identifier" => "get_affichage_form",
            "view" => "get_affichage_form",
            "permission_suffix" => "aff_form",
        );
    }

    /**
     * AJAX - get_affichage_form
     * 
     * @return json
     */
    function get_affichage_form() {
        $this->f->disableLog();
        require_once '../obj/dossier_autorisation_type.class.php';
        $tda = new dossier_autorisation_type(
            $this->getVal('dossier_autorisation_type')
        );
        echo json_encode($tda->getVal('affichage_form'));
        return true;
    }
}

?>
