<?php
/**
 * DBFORM - 'demande_type' - Surcharge gen.
 *
 * Ce script permet de définir la classe 'demande_type'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../gen/obj/demande_type.class.php";

class demande_type extends demande_type_gen {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
        // Initialisation des variables de classe
        $this->lib_dossier_autorisation_type_detaille = _('dossier_autorisation_type_detaille');
        $this->lib_dossier_instruction_type = _("type de dossier d'instruction a creer");
    }

    // Variables de classe
    var $lib_dossier_autorisation_type_detaille;
    var $lib_dossier_instruction_type;

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        parent::init_class_actions();

        // ACTION - 100 - non_frequent
        // Finalise l'enregistrement
        $this->class_actions[100] = array(
            "identifier" => "doc_checklist",
            "view" => "doc_checklist_json",
            "permission_suffix" => "show_checklist",
        );
    }

    /**
     * Permet de retourner les informations necessaires à l'affichage de la checklist
     * des documents obligatoires lors du dépôt de demande.
     */
    function doc_checklist_json() {
        $this->f->disableLog();

        if($this->getVal("document_obligatoire") != "") {
            // Si des documents obligatoires sont définis on constitue le json
            $res = array(
                "title"=>_("Liste des documents obligatoires"),
                "documents" => preg_split("/\\r\\n|\\r|\\n/", $this->getVal("document_obligatoire")),
                "libelle_ok" => _("Valider"),
                "libelle_cancel" => _("Rejeter la demande"),
                "message_ko" => _("Tous les documents doivent-etre presents.\nDans le cas contraire, rejeter la demande."),
                "message_rejet" => _("Etes vous sur de vouloir rejeter la demande ?"),
            );
            // On retourne le JSON
            printf("%s", json_encode($res));
        } else {
            // Sinon on retourn false
            printf("false");
        }
    }

    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $maj);
        //
        $form->setLib("dossier_instruction_type", $this->lib_dossier_instruction_type);
        $form->setLib("evenement", _("type de l'evenement d'instruction a creer"));
        $form->setLib("etats_autorises", _("etats du dernier dossier d'instruction"));
        $form->setLib('dossier_autorisation_type_detaille', $this->lib_dossier_autorisation_type_detaille);
    }

    function setLayout(&$form, $maj) {
        //
        parent::setLayout($form, $maj);
        //
        $form->setFieldset("demande_type", "D", _("demande_type"));
        $form->setFieldset("description", "F");
        //
        $form->setFieldset("groupe", "D", _("categorisation"));
        $form->setFieldset("demande_nature", "F");
        //
        $form->setFieldset("etats_autorises", "D", _("comportement de la demande"));
        $form->setFieldset("qualification", "F");
        //
        $form->setFieldset("evenement", "DF", _("recepisse de la demande"));
        //
        $form->setFieldset("document_obligatoire", "DF", _("documents obligatoires"));
    }

    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);

        if ($maj < 2 ) {
            $form->setType("contraintes","select");
            $form->setType("etats_autorises","select_multiple");
        }
        if ($maj >= 2) {
            $form->setType("contraintes","selecthiddenstatic");
            $form->setType("etats_autorises","select_multiple_static");
        }
    }

    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        parent::setSelect($form, $maj,$db,$debug);

        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        // Liste des contraintes applicables lors de la création d'une nouvelle demande
        $contraintes = array(
                     array('', 'avec_recup','sans_recup'),
                     array(_('choisir une contrainte'), _('Avec recuperation demandeur'),_('Sans recuperation demandeur')),
                     );
        
        $form->setSelect("contraintes", $contraintes);

        //Initialisation de la liste des états des dossiers d'instruction
        $this->init_select($form, $db, $maj, $debug, "etats_autorises",
            $sql_etats_autorises, $sql_etats_autorises_by_id, false, true);
        
        // Si en ajout ou en modification
        if ($maj < 2) {
            // Initialise les select en fonction de la valeur d'un autre champ
            $form->setSelect('dossier_autorisation_type_detaille', $this->loadSelect_dossier_autorisation_type_detaille($form, $maj, $db, $debug, "groupe"));
            $form->setSelect('dossier_instruction_type', $this->loadSelect_dossier_instruction_type($form, $maj, $db, $debug, "dossier_autorisation_type_detaille"));
        }
    }

    /**
     * Permet de définir l’attribut “onchange” sur chaque champ
     * @param object $form Formumaire
     * @param int    $maj  Mode d'insertion
     */
    function setOnchange(&$form,$maj) {
        parent::setOnchange($form,$maj);

        //Au changement sur le champ groupe, réinitilisation des champs dossier_autorisation_type_detaille et dossier_instruction_type
        $form->setOnchange('groupe', 'filterSelect(this.value, \'dossier_autorisation_type_detaille\', \'groupe\', \'demande_type\'), filterSelect(dossier_autorisation_type_detaille.value, \'dossier_instruction_type\', \'dossier_autorisation_type_detaille\', \'demande_type\')');
        $form->setOnchange('dossier_autorisation_type_detaille', 'filterSelect(this.value, \'dossier_instruction_type\', \'dossier_autorisation_type_detaille\', \'demande_type\')');
        $form->setOnchange("demande_nature", "VerifNum(this);required_fields_demande_type(this.value, '".addslashes($this->lib_dossier_autorisation_type_detaille)."', '".addslashes($this->lib_dossier_instruction_type)."');");
        
    }

    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        parent::triggerajouterapres($id,$db,$val,$DEBUG);

        // Ajoute autant de lien_demande_type_etat que d'états séléctionnés

        // Récupération des données du select multiple
        $etats_autorises = $this->getPostedValues('etats_autorises');
        
        // Ne traite les données que s'il y en a et qu'elles sont correctes
        if (is_array($etats_autorises)
            && count($etats_autorises) > 0 ){
            // Initialisation
            $nb_liens_etat = 0;
            // Boucle sur la liste des états sélectionnés
            foreach ($etats_autorises as $value) {
                // Test si la valeur par défaut est sélectionnée
                if ($value != "") {
                    // 
                    $donnees = array(
                        'demande_type' => $this->valF['demande_type'],
                        'etat' => $value
                    );
                    // On ajoute l'enregistrement
                    $this->ajouter_etats_autorises($donnees);
                    // On compte le nombre d'éléments ajoutés
                    $nb_liens_etat++;
                }
            }
            // Message de confirmation
            if ($nb_liens_etat > 0) {
                if ($nb_liens_etat == 1 ){
                    $this->addToMessage(_("Creation de ").$nb_liens_etat._(" nouvelle liaison realisee avec succes."));
                } else{
                    $this->addToMessage(_("Creation de ").$nb_liens_etat._(" nouvelles liaisons realisees avec succes."));
                }
            }
        }
        
    }
    
    //Fonction générique permettant de récupérer les données d'un champ postées
    function getPostedValues($champ) {
        
        // Récupération des demandeurs dans POST
        if ($this->f->get_submitted_post_value($champ) !== null) {
            
            return $this->f->get_submitted_post_value($champ);
        }
    }
    
    function triggermodifierapres($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        parent::triggermodifierapres($id, $db, $val, $DEBUG);

        // Suppression de tous les liens de la table lien_demande_type_etat
        $this->supprimer_etats_autorises($this->valF['demande_type']);

        // Récupération des données du select multiple
        $etats_autorises = $this->getPostedValues('etats_autorises');

        // Ne traite les données que s'il y en a et qu'elles sont correctes
        if (is_array($etats_autorises)
            && count($etats_autorises) > 0 ){
            // Initialisation
            $nb_liens_etat = 0;
            // Boucle sur la liste des états sélectionnés
            foreach ($etats_autorises as $value) {
                // Test si la valeur par défaut est sélectionnée
                if ($value != "") {
                    // 
                    $donnees = array(
                        'demande_type' => $this->valF['demande_type'],
                        'etat' => $value
                    );
                    // On ajoute l'enregistrement
                    $this->ajouter_etats_autorises($donnees);
                    // On compte le nombre d'éléments ajoutés
                    $nb_liens_etat++;
                }
            }
            // Message de confirmation
            if ($nb_liens_etat > 0) {
                $this->addToMessage(_("Mise a jour des liaisons realisee avec succes."));
            }
        }
    }

    /**
     * Ajout d'un lien entre la demande_type et les états de dossiers d'autorisation
     * séléctionné
     * @param mixed[] $data couples demande_type/etat à ajouter
     */
    function ajouter_etats_autorises($data) {
        //
        require_once '../obj/lien_demande_type_etat.class.php';
        $lien_demande_type_etat = new lien_demande_type_etat("]", $this->db, false);
        // initialisation de la clé primaire
        $val['lien_demande_type_etat'] = "";
        //
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $val[$key]=$value;
            }
        }
        //
        $lien_demande_type_etat->ajouter($val, $this->db, false);
    }

    /**
     * Suppression de tous les liens entre la demande_type et les états
     * @param integer $id Identifiant du type de la demande
     */
    function supprimer_etats_autorises($id) {
        // Suppression de tous les enregistrements correspondants à l'id de
        // la demande_type
        $sql = "DELETE
            FROM ".DB_PREFIXE."lien_demande_type_etat
            WHERE demande_type=".$id;
        $res = $this->db->query($sql);
        $this->f->addToLog("supprimer_etats_autorises(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ( database::isError($res)){
            die();
        }
    }
    
    /**
     * Suppression des liens entre demande_type et etat lors de la suppression
     * d'un type de demande
     */
    function triggersupprimer($id, &$db = null, $val = array(), $DEBUG = null) {

        //Supression des liens entre le type de la demande et les états
        $this->supprimer_etats_autorises($id);
    }
    
    /**
     * Suppression de la recherche de clés sur la table lien_demande_type_etat_dossier_autorisation
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // Verification de la cle secondaire : demande
        $this->rechercheTable($db, "demande", "demande_type", $id);
    }

    /**
     * Charge le select du champ type de dossier d'autorisation détaillé
     * @param  object $form  Formulaire
     * @param  int    $maj   Mode d'insertion
     * @param  object $db    Database
     * @param  bool   $debug Debug active ou pas
     * @param  string $champ champ activant le filtre
     * @return array         Contenu du select
     */
    function loadSelect_dossier_autorisation_type_detaille(&$form, $maj, &$db, $debug, $champ) {

        //
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        //
        $contenu=array();
        $contenu[0][0]='';
        $contenu[1][0]=_("Choisir type de dossier d'autorisation detaille");

        //Récupère l'id du type de dossier d'autorisation détaillé 
        $id_groupe = "";
        if ($this->f->get_submitted_post_value($champ) !== null) {
            $id_groupe = $this->f->get_submitted_post_value($champ);
        } elseif($this->getParameter($champ) != "") {
            $id_groupe = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $id_groupe = $form->val[$champ];
        }

        //
        if ($id_groupe != "") {
            //Si l'id l'id du type de dossier d'autorisation détaillé est renseigné
            $sql_dossier_autorisation_type_detaille_by_groupe = str_replace('<idx_groupe>', $id_groupe, $sql_dossier_autorisation_type_detaille_by_groupe);
            $res = $this->db->query($sql_dossier_autorisation_type_detaille_by_groupe);
            $this->addToLog("db->query(\"".$sql_dossier_autorisation_type_detaille_by_groupe."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            //Les résultats de la requête sont stocké dans le tableau contenu
            $k=1;
            while ($row=& $res->fetchRow()){
                $contenu[0][$k]=$row[0];
                $contenu[1][$k]=$row[1];
                $k++;
            } 
        }
        //
        return $contenu;
    }

    /**
     * Charge le select du champ type de dossier d'instruction à créer 
     * @param  object $form  Formulaire
     * @param  int    $maj   Mode d'insertion
     * @param  object $db    Database
     * @param  bool   $debug Debug active ou pas
     * @param  string $champ champ activant le filtre
     * @return array         Contenu du select
     */
    function loadSelect_dossier_instruction_type(&$form, $maj, &$db, $debug, $champ) {

        //
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        //
        $contenu=array();
        $contenu[0][0]='';
        $contenu[1][0]=_("Choisir type de dossier d'instruction");

        //Récupère l'id du type de dossier d'autorisation détaillé 
        $id_dossier_autorisation_type_detaille = "";
        if ($this->f->get_submitted_post_value($champ) !== null) {
            $id_dossier_autorisation_type_detaille = $this->f->get_submitted_post_value($champ);
        } elseif($this->getParameter($champ) != "") {
            $id_dossier_autorisation_type_detaille = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $id_dossier_autorisation_type_detaille = $form->val[$champ];
        }

        //
        if ($id_dossier_autorisation_type_detaille != "") {
            //Si l'id l'id du type de dossier d'autorisation détaillé est renseigné
            $sql_dossier_instruction_type_by_dossier_autorisation_type_detaille = str_replace('<idx_dossier_autorisation_type_detaille>', $id_dossier_autorisation_type_detaille, $sql_dossier_instruction_type_by_dossier_autorisation_type_detaille);
            $res = $this->db->query($sql_dossier_instruction_type_by_dossier_autorisation_type_detaille);
            $this->addToLog("db->query(\"".$sql_dossier_instruction_type_by_dossier_autorisation_type_detaille."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            //Les résultats de la requête sont stocké dans le tableau contenu
            $k=1;
            while ($row=& $res->fetchRow()){
                $contenu[0][$k]=$row[0];
                $contenu[1][$k]=$row[1];
                $k++;
            } 
        }
        //
        return $contenu;
    }

    /**
     * Méthode de vérification des données et de retour d’erreurs
     * @param  array    $val    Tableau des valeurs
     * @param  mixed    $db     Instance de la base de données
     * @param  mixed    DEBUG   Mode de debug
     */
    function verifier($val = array(), &$db = null, $DEBUG = null) {
        parent::verifier($val, $db, $DEBUG);

        // Si c'est une nouvelle demande
        if ($this->get_demande_nature_code($this->valF['demande_nature']) == 'NOUV') {

            // Si le champ dossier_autorisation_type_detaille est vide
            if ($this->valF['dossier_autorisation_type_detaille'] == ''
                || $this->valF['dossier_autorisation_type_detaille'] == null) {
                $this->correct = false;
                $this->addToMessage(_("Le champ")." <span class=\"bold\">".$this->lib_dossier_autorisation_type_detaille."</span> "._("est obligatoire"));
            }

            // Si dossier_instruction_type est vide
            if ($this->valF['dossier_instruction_type'] == ''
                || $this->valF['dossier_instruction_type'] == null) {
                $this->correct = false;
                $this->addToMessage(_("Le champ")." <span class=\"bold\">".$this->lib_dossier_instruction_type."</span> "._("est obligatoire"));
            }
            
        }
    }

    /**
     * Récupère le code de la nature de la demande
     * @param  integer $demande_nature  Identifiant de la nature de la demande
     * @return string                   Code de la nature de la demande
     */
    function get_demande_nature_code($demande_nature) {

        // Initialisation de la variable de résultat
        $code = "";

        // Si la condition n'est pas vide
        if ($demande_nature != ""
            && $demande_nature != NULL
            && is_numeric($demande_nature)) {

            // Requête SQL
            $sql = "SELECT code
                    FROM ".DB_PREFIXE."demande_nature
                    WHERE demande_nature = $demande_nature";
            $this->f->addToLog("get_demande_nature_code() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $code = $this->db->getOne($sql);
            $this->f->isDatabaseError($code);
        }

        // Retourne le résultat
        return $code;
        
    }

}

?>
