<?php
/**
 * DBFORM - 'architecte_frequent' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'architecte_frequent'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/architecte.class.php";

class architecte_frequent extends architecte {
    
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        //
        parent::setVal($form, $maj, $validation, $db, $DEBUG);
        $form->setVal("frequent",true);
    }

}

?>
