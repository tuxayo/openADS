<?php
/**
 * DBFORM - 'commission_type' - Surcharge gen.
 *
 * Ce script permet de définir la classe 'commission_type'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../gen/obj/commission_type.class.php";

class commission_type extends commission_type_gen {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 011 - json_data
        //
        $this->class_actions[11] = array(
            "identifier" => "json_data",
            "view" => "view_json_data",
            "permission_suffix" => "consulter",
        );

    }

    /**
     * VIEW - view_json_data.
     *
     * Affiche un tableau JSON représentant toutes les données de 
     * l'enregistrement courant.
     *
     * @return void
     */
    function view_json_data() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // La désactivation des logs est obligatoire pour une vue JSON.
        $this->f->disableLog();
        //
        $data = array();
        foreach ($this->champs as $key => $value) {
            $data[$value] = $this->getVal($value);
        }
        //
        echo json_encode($data);
    }

    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        parent::setSelect($form, $maj, $db, $debug);

        $crud = $this->get_action_crud($maj);
        // Le but ici est de brider aux collectivités mono en cas d'ajout par collectivité multi.
        if ($crud == 'create' || $maj == 0 || $_SESSION["niveau"] == 2) {
            if (file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
                include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
                $this->init_select($form, $db, $maj, $debug, "om_collectivite",
                    $sql_om_collectivite_multi, $sql_om_collectivite_by_id, true);
            }
        }
    }

}

?>
