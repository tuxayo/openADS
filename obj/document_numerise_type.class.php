<?php
//$Id: document_numerise_type.class.php 5839 2016-01-29 08:50:12Z fmichon $ 
//gen openMairie le 29/01/2016 09:41

require_once "../gen/obj/document_numerise_type.class.php";

class document_numerise_type extends document_numerise_type_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }


    /**
     *
     */
    var $activate_class_action = true;

    /**
     * Permet de définir les valeurs des champs en contexte formulaire.
     *
     * @param object  &$form      L'objet formulaire.
     * @param integer $maj        Mode du formulaire.
     * @param integer $validation Nombre de validation du formulaire.
     */
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        //
        parent::setVal($form, $maj, $validation, $db, $DEBUG);

        // En mode ajout
        if ($this->getParameter('maj') === 0) {
            // Coche par défaut les cases à cocher suivantes
            $form->setVal('aff_da', true);
            $form->setVal('aff_service_consulte', true);
        }
    }


    /**
     * Permet de définir les valeurs des champs en contexte sous-formulaire.
     *
     * @param object  &$form            L'objet formulaire.
     * @param integer $maj              Mode du formulaire.
     * @param integer $validation       Nombre de validation du formulaire.
     * @param mixed   $idxformulaire    Identifiant du formulaire parent.
     * @param string  $retourformulaire Nom du formulaire parent.
     * @param string  $typeformulaire   Type du formulaire.
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);

        //
        if ($this->getParameter('maj') === 0) {
            // Coche par défaut les cases à cocher suivantes
            $form->setVal('aff_da', true);
            $form->setVal('aff_service_consulte', true);
        }
    }


    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form L'objet formulaire.
     * @param integer $maj   Mode du formulaire.
     */
    public function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);

        // Cache les champs
        $form->setType('synchro_metadonnee', 'hidden');
        if ($maj < 2 ) {
            $form->setType("instructeur_qualite","select_multiple");
        }
        if ($maj >= 2) {
            $form->setType("instructeur_qualite","select_multiple_static");
        }
    }


    /**
     * Méthode qui effectue les requêtes de configuration des champs
     *
     * @param object  &$form L'objet formulaire.
     * @param integer $maj   Mode du formulaire.
     * @param null    &$db   Ressource de base de données.
     * @param null    $debug Marqueur de débogage.
     */
    function setSelect(&$form, $maj, &$db = null, $debug = null) {

        // On appelle la methode de la classe parent
        parent::setSelect($form, $maj);

        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        //Initialisation de la liste des états des dossiers d'instruction
        $this->init_select($form, $db, $maj, $debug, "instructeur_qualite",
            $sql_instructeur_qualite, $sql_instructeur_qualite_by_id, false, true);
    }


    /**
     * Permet d’effectuer des actions après l’insertion des données dans la
     * base.
     *
     * @param integer $id    Identifiant de l'enregistrement.
     * @param null    &$db   Ressource de base de données.
     * @param array   $val   Liste des valeurs.
     * @param null    $DEBUG Marqueur de débogage.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        parent::triggerajouterapres($id, $db, $val, $DEBUG);

        // Ajoute autant de lien_document_numerise_type_instructeur_qualite que
        // de qualité d'instructeur ajoutés

        // Récupération des données du select multiple
        $instructeur_qualite = $this->getPostedValues('instructeur_qualite');

        // Ne traite les données que s'il y en a et qu'elles sont correctes
        if (is_array($instructeur_qualite)
            && count($instructeur_qualite) > 0 ){
            // Initialisation
            $nb_liens_instructeur_qualite = 0;
            // Boucle sur la liste des états sélectionnés
            foreach ($instructeur_qualite as $value) {
                // Test si la valeur par défaut est sélectionnée
                if ($value != "") {
                    // 
                    $donnees = array(
                        'document_numerise_type' => $this->valF['document_numerise_type'],
                        'instructeur_qualite' => $value
                    );
                    // On ajoute l'enregistrement
                    $add = $this->ajouter_instructeur_qualite($donnees);
                    //
                    if ($add === false) {
                        //
                        return false;
                    }
                    // On compte le nombre d'éléments ajoutés
                    $nb_liens_instructeur_qualite++;
                }
            }
            // Message de confirmation
            if ($nb_liens_instructeur_qualite > 0) {
                if ($nb_liens_instructeur_qualite == 1 ){
                    $this->addToMessage(_("Creation de ").$nb_liens_instructeur_qualite._(" nouvelle liaison realisee avec succes."));
                } else{
                    $this->addToMessage(_("Creation de ").$nb_liens_instructeur_qualite._(" nouvelles liaisons realisees avec succes."));
                }
            }
        }

        //
        return true;
    }


    /**
     * Methode de recupération des valeurs postées
     *
     * @param string $champ Champ du formulaire.
     *
     * @return string
     */
    public function getPostedValues($champ) {
        
        // Récupération des demandeurs dans POST
        if ($this->f->get_submitted_post_value($champ) !== null) {
            
            return $this->f->get_submitted_post_value($champ);
        }
    }


    /**
     * Permet d’effectuer des actions après la modification des données dans la
     * base.
     *
     * @param integer $id    Identifiant de l'enregistrement.
     * @param null    &$db   Ressource de base de données.
     * @param array   $val   Liste des valeurs.
     * @param null    $DEBUG Marqueur de débogage.
     *
     * @return boolean
     */
    public function triggerModifierApres($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        parent::triggerModifierApres($id, $db, $val, $DEBUG);

        // Suppression de tous les liens de la table
        // lien_document_numerise_type_instructeur_qualite
        $this->supprimer_instructeur_qualite($this->valF['document_numerise_type']);

        // Récupération des données du select multiple
        $instructeur_qualite = $this->getPostedValues('instructeur_qualite');

        // Ne traite les données que s'il y en a et qu'elles sont correctes
        if (is_array($instructeur_qualite)
            && count($instructeur_qualite) > 0 ){
            // Initialisation
            $nb_liens_instructeur_qualite = 0;
            // Boucle sur la liste des états sélectionnés
            foreach ($instructeur_qualite as $value) {
                // Test si la valeur par défaut est sélectionnée
                if ($value != "") {
                    // 
                    $donnees = array(
                        'document_numerise_type' => $this->valF['document_numerise_type'],
                        'instructeur_qualite' => $value
                    );
                    // On ajoute l'enregistrement
                    $add = $this->ajouter_instructeur_qualite($donnees);
                    //
                    if ($add === false) {
                        //
                        return false;
                    }
                    // On compte le nombre d'éléments ajoutés
                    $nb_liens_instructeur_qualite++;
                }
            }
            // Message de confirmation
            if ($nb_liens_instructeur_qualite > 0) {
                $this->addToMessage(_("Mise a jour des liaisons realisee avec succes."));
            }
        }

        //
        return true;
    }


    /**
     * Ajout d'un lien entre le type de document numérisé et les qualités
     * d'instructeur sélectionnés.
     *
     * @param mixed[] $data Couples document_numerise_type/instructeur_qualite
     *                      à ajouter.
     *
     * @return boolean
     */
    public function ajouter_instructeur_qualite($data) {
        //
        require_once '../obj/lien_document_numerise_type_instructeur_qualite.class.php';
        $lien_document_numerise_type_instructeur_qualite = new lien_document_numerise_type_instructeur_qualite("]", $this->db, false);
        // initialisation de la clé primaire
        $val['lien_document_numerise_type_instructeur_qualite'] = "";
        //
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $val[$key] = $value;
            }
        }
        //
        $add = $lien_document_numerise_type_instructeur_qualite->ajouter($val, $this->db, false);
        //
        return $add;
    }


    /**
     * Suppression de tous les liens entre le type de document numérisé et les
     * qualités d'instructeur.
     *
     * @param integer $id Identifiant du type de document numérisé.
     *
     * @return boolean
     */
    public function supprimer_instructeur_qualite($id) {
        // Suppression de tous les enregistrements correspondants à l'id de
        // la document_numerise_type
        $sql = "DELETE
            FROM ".DB_PREFIXE."lien_document_numerise_type_instructeur_qualite
            WHERE document_numerise_type=".$id;
        $res = $this->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            return false;
        }
        //
        return true;
    }


    /**
     * Permet d’effectuer des actions avant la modification des données dans la
     * base.
     *
     * @param integer $id    Identifiant de l'enregistrement.
     * @param null    $db    Ressource de base de données.
     * @param array   $val   Liste des valeurs.
     * @param null    $DEBUG Marqueur de débogage.
     *
     * @return boolean
     */
    public function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        //Supression des liens entre le type de la demande et les états
        $delete = $this->supprimer_instructeur_qualite($id);

        //
        return $delete;
    }


    /**
     * Cette méthode est appelée lors de la suppression d’un objet, elle permet
     * de vérifier si l’objet supprimé n’est pas lié à une autre table pour en
     * empêcher la suppression.
     *
     * @param integer $id    Identifiant de l'enregistrement.
     * @param null    &$dnu1 Ancienne ressource de base de données.
     * @param array   $val   Liste des valeurs.
     * @param null    $dnu2  Ancien marqueur de débogage.
     *
     * @return void
     */
    public function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Verification de la cle secondaire : document_numerise
        $this->rechercheTable($this->f->db, "document_numerise", "document_numerise_type", $id);
    }


    /**
     * Récupère la liste des codes dont le champ filtre est à 'true'.
     *
     * @param string $filtre Nom champ filtre (aff_da ou aff_service_consulte).
     *
     * @return mixed Tableau des codes ou false
     */
    public function get_code_by_filtre($filtre) {

        // On vérifie que le champ permettant le filtre, passé en paramètre,
        // existe dans la table des types de pièce
        if ($filtre !== 'aff_da' && $filtre !== 'aff_service_consulte') {
            //
            return false;
        }

        // Requête SQL
        $sql = "SELECT code
                FROM ".DB_PREFIXE."document_numerise_type
                WHERE ".$filtre." = 't'";
        // Exécution de la requête
        $res = $this->db->query($sql);
        // Log
        $this->addToLog(__METHOD__."() : db->query(\"".$sql."\");", VERBOSE_MODE);
        // Erreur BDD
        $this->f->isDatabaseError($res);
        //
        $list_code = array();
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $list_code[] = $row['code'];
        }

        // Retourne la liste des codes
        return $list_code;
    }


    /**
     * Permet de modifier le fil d'Ariane.
     * 
     * @param string $ent Fil d'Ariane.
     *
     * @return string
     */
    public function getFormTitle($ent) {

        // Fil d'ariane par défaut
        $ent = _("parametrage")." -> "._("Gestion des pièces")." -> "._("document_numerise_type");

        // Si différent de l'ajout
        if($this->getParameter("maj") != 0) {

            // Affiche la clé primaire
            $ent .= " -> ".$this->getVal("document_numerise_type");

            // Affiche le code
            $ent .= " ".mb_strtoupper($this->getVal("code"), 'UTF-8');
        }

        // Change le fil d'Ariane
        return $ent;
    }


    /**
     * Permet d’effectuer des actions avant la modification des données dans la
     * base.
     *
     * @param integer $id Identifiant de l'enregistrement.
     *
     * @return boolean
     */
    function triggermodifier($id, &$db = null, $val = array(), $DEBUG = null) {

        //
        $aff_da = $this->get_boolean_from_pgsql_value($this->getVal('aff_da'));
        //
        $aff_service_consulte = $this->get_boolean_from_pgsql_value($this->getVal('aff_service_consulte'));

        // Si le champ 'aff_da' ou le champ 'aff_service_consulte' sont modifiés
        if ($aff_da !== $this->valF['aff_da']
            || $aff_service_consulte !== $this->valF['aff_service_consulte']) {
            // Met à vrai le marqueur pour mettre à jour les métadonnées
            $this->valF['synchro_metadonnee'] = true;
        }

        //
        return true;
    }


    /**
     * Récupère la liste des identifiants de type de pièces dont le champ
     * 'synchro_metadonnee' est égale à la valeur passé en paramètre.
     *
     * @param boolean $sm_value True ou false.
     *
     * @return mixed Tableau des identifiants ou false.
     */
    public function get_ids_by_synchro_metadonnee($sm_value) {

        // Le champ 'synchro_metadonnee' est un booléen
        if ($sm_value !== true && $sm_value !== false) {
            //
            return false;
        }

        // Requête SQL
        $sql = "SELECT document_numerise_type
                FROM ".DB_PREFIXE."document_numerise_type
                WHERE synchro_metadonnee = '".$sm_value."'";
        // Exécution de la requête
        $res = $this->db->query($sql);
        // Log
        $this->addToLog(__METHOD__."() : db->query(\"".$sql."\");", VERBOSE_MODE);
        // Erreur BDD
        $this->f->isDatabaseError($res);
        // Tableau des résultats
        $list_ids = array();
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $list_ids[] = $row['document_numerise_type'];
        }

        // Retourne la liste des codes
        return $list_ids;
    }


    /**
     * Récupère la liste des documents numérisés dont le type est présent dans
     * le tableau passé en paramètre.
     *
     * @param array $dnt_ids Liste des identifiants de type.
     *
     * @return mixed Tableau des identifiants ou false.
     */
    public function get_dn_by_dnt_ids(array $dnt_ids) {

        // Vérifie que c'est un tableau en paramètre
        if (is_array($dnt_ids) === false) {
            //
            return false;
        }

        // Récupère les documents numérisés dont le type est passé en paramètre
        // Requête SQL
        $sql = "SELECT document_numerise
                FROM ".DB_PREFIXE."document_numerise
                WHERE document_numerise_type IN (".implode(',', $dnt_ids).")";
        // Exécution de la requête
        $res = $this->db->query($sql);
        // Log
        $this->addToLog(__METHOD__."() : db->query(\"".$sql."\");", VERBOSE_MODE);
        // Erreur BDD
        $this->f->isDatabaseError($res);
        // Tableau des résultats
        $list_ids = array();
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $list_ids[] = $row['document_numerise'];
        }

        //
        return $list_ids;
    }


    /**
     * Récupère l'instance de document_numerise.
     *
     * @param string $document_numerise Identifiant du document numérisé.
     *
     * @return object
     */
    protected function get_inst_document_numerise($document_numerise) {
        //
        return $this->get_inst_common("document_numerise", $document_numerise);
    }


}

?>
