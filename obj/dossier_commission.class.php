<?php
//$Id: dossier_commission.class.php 4701 2015-04-30 16:36:34Z vpihour $ 
//gen openMairie le 07/12/2012 17:34

require_once ("../gen/obj/dossier_commission.class.php");

class dossier_commission extends dossier_commission_gen {

    function dossier_commission($id,&$db,$debug) {
        $this->constructeur($id,$db,$debug);
    }// fin constructeur

    // {{{ Gestion de la confidentialité des données spécifiques

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();

        // ACTION - 000 - ajouter
        // Modifie la condition d'affichage du bouton ajouter
        $this->class_actions[0]["condition"] = "can_user_access_dossier_contexte_ajout";

        // ACTION - 001 - modifier
        // 
        $this->class_actions[1]["condition"] = array("is_editable", "can_user_access_dossier_contexte_modification");
        
        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = array("is_deletable", "can_user_access_dossier_contexte_modification");

        // ACTION - 050 - marquer_comme_lu
        // Pour qu'un cadre valide l'analyse
        $this->class_actions[50] = array(
            "identifier" => "marquer_comme_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("Marquer comme lu"),
                "order" => 50,
                "class" => "lu-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_comme_lu",
            "permission_suffix" => "modifier_lu",
            "condition" => array("is_markable",
              "show_marquer_comme_lu_portlet_action",
              "can_user_access_dossier_contexte_modification",
            ),
        );
    }

    
    /**
     * TREATMENT - marquer_comme_lu.
     * 
     * Cette methode permet de passer la consultation en "lu"
     *
     * @return boolean true si maj effectué false sinon
     */
    function marquer_comme_lu() {
        // Cette méthode permet d'exécuter une routine en début des méthodes
        // dites de TREATMENT.
        $this->begin_treatment(__METHOD__);

        if($this->getVal("lu") == 'f') {
            $this->correct = true;
            $this->valF["lu"] = true;
        
            $res = $this->f->db->autoExecute(
                        DB_PREFIXE.$this->table, 
                        $this->valF, 
                        DB_AUTOQUERY_UPDATE,
                        $this->clePrimaire."=".$this->getVal($this->clePrimaire)
                    );
            if ($this->f->isDatabaseError($res, true)) {
                // Appel de la methode de recuperation des erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                $this->correct = false;
                // Termine le traitement
                return $this->end_treatment(__METHOD__, false);
            } else {
                $this->addToMessage(_("Mise a jour effectue avec succes"));
                return $this->end_treatment(__METHOD__, true);
            }

        } else {
            $this->addToMessage(_("Element deja marque comme lu"));
        }

        // Termine le traitement
        return $this->end_treatment(__METHOD__, false);
    }


    function setType(&$form,$maj) {
        parent::setType($form, $maj);
        
        //En ajout ou modification pour l'instructeur
        if ( $maj < 2 &&
            (
                $this->retourformulaire=='dossier'
                || $this->retourformulaire=='dossier_instruction'
                || $this->retourformulaire=='dossier_instruction_mes_encours'
                || $this->retourformulaire=='dossier_instruction_tous_encours'
                || $this->retourformulaire=='dossier_instruction_mes_clotures'
                || $this->retourformulaire=='dossier_instruction_tous_clotures'
                || $this->retourformulaire=='dossier_contentieux_mes_infractions'
                || $this->retourformulaire=='dossier_contentieux_toutes_infractions'
                || $this->retourformulaire=='dossier_contentieux_mes_recours'
                || $this->retourformulaire=='dossier_contentieux_tous_recours'
            ) ){ 
                    
            $form->setType('dossier','hiddenstatic');
            $form->setType('commission','hidden');
            $form->setType('avis','hidden');
            $form->setType('lu', 'hidden');
        }

        //En modification pour l'instructeur
        if ( $maj == 1 &&
            (
                $this->retourformulaire=='dossier'
                || $this->retourformulaire=='dossier_instruction'
                || $this->retourformulaire=='dossier_instruction_mes_encours'
                || $this->retourformulaire=='dossier_instruction_tous_encours'
                || $this->retourformulaire=='dossier_instruction_mes_clotures'
                || $this->retourformulaire=='dossier_instruction_tous_clotures'
                || $this->retourformulaire=='dossier_contentieux_mes_infractions'
                || $this->retourformulaire=='dossier_contentieux_toutes_infractions'
                || $this->retourformulaire=='dossier_contentieux_mes_recours'
                || $this->retourformulaire=='dossier_contentieux_tous_recours'
            ) ){ 
                    
            $form->setType('lu','checkboxstatic');
        }

        // En consultation et modification pour la cellule suivi
        if ( $maj == 1 && $this->retourformulaire == 'commission'){ 

            $form->setType('dossier','hiddenstatic');
            $form->setType('commission_type','hiddenstatic');
            $form->setType('date_souhaitee','hiddenstaticdate');
            $form->setType('motivation','hiddenstatic');
            $form->setType('commission','hiddenstatic');
            $form->setType('lu','hidden');
        }

        if ( $maj == 3 && $this->retourformulaire == 'commission'){ 

            $form->setType('lu','hidden');
        }
        // 
        if ($this->getParameter("retourformulaire") == "dossier"
            || $this->getParameter("retourformulaire") == "dossier_instruction"
            || $this->getParameter("retourformulaire") == "dossier_instruction_mes_encours"
            || $this->getParameter("retourformulaire") == "dossier_instruction_tous_encours"
            || $this->getParameter("retourformulaire") == "dossier_instruction_mes_clotures"
            || $this->getParameter("retourformulaire") == "dossier_instruction_tous_clotures"
            || $this->getParameter("retourformulaire") == "dossier_contentieux_mes_infractions"
            || $this->getParameter("retourformulaire") == "dossier_contentieux_toutes_infractions"
            || $this->getParameter("retourformulaire") == "dossier_contentieux_mes_recours"
            || $this->getParameter("retourformulaire") == "dossier_contentieux_tous_recours") {
            //
            $form->setType('dossier', 'hidden');
        }
        if($maj == 50 ) {
            foreach ($this->champs as $value) {
                $form->setType($value, 'hidden');
            }
        }
    }


    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, _("id"));
    }

    // la demande de commission est mis à lu par défaut
    function triggerajouter($id, &$db = null, $val = array(), $DEBUG = null) {

        $this->valF['lu'] = TRUE;
    }

    // Si la commission rend son avis, la demande de commission est non lu pour que 
    // l'instructeur soit notifié
    function triggermodifier($id, &$db = null, $val = array(), $DEBUG = null) {
        
        if ( $val['avis'] != "" ){
            
            $this->valF['lu'] = FALSE;
        }
    }

    // Met la date au bon format
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = db, $DEBUG = null) {
        //
        $this->retourformulaire = $retourformulaire;
        // En mode AJOUTER
        if ($maj == 0) {
            // On positionne la date du jour par défaut
            $form->setVal('date_souhaitee', date("d/m/Y"));
            // Afficher le numéro de dossier d'instruction
            if($retourformulaire =='dossier'
               || $retourformulaire =='dossier_instruction'
               || $retourformulaire =='dossier_instruction_mes_encours'
               || $retourformulaire =='dossier_instruction_tous_encours'
               || $retourformulaire =='dossier_instruction_mes_clotures'
               || $retourformulaire =='dossier_instruction_tous_clotures'
               || $retourformulaire =='dossier_contentieux_mes_infractions'
               || $retourformulaire =='dossier_contentieux_toutes_infractions'
               || $retourformulaire =='dossier_contentieux_mes_recours'
               || $retourformulaire =='dossier_contentieux_tous_recours'){
                $form->setVal('dossier', $idxformulaire);
            }
        }
    }

    //Surcharge du bouton retour de la gestion des commissions afin de retourner directement vers le tableau
    function retoursousformulaire($idxformulaire = NULL, $retourformulaire = NULL, $val = NULL,
                                  $objsf = NULL, $premiersf = NULL, $tricolsf = NULL, $validation = NULL,
                                  $idx = NULL, $maj = NULL, $retour = NULL) {
                                      
        if( $maj = 1 && $retourformulaire === "commission") {
            
            echo "\n<a class=\"retour\" ";
            echo "href=\"#\" ";
            
            echo "onclick=\"ajaxIt('".$objsf."', '";
            echo "../scr/soustab.php?obj=dossier_commission&idxformulaire="
                   .$val['commission']. 
                "&retourformulaire=commission');\"";
            echo  "\" ";
            echo ">";
            //
            echo _("Retour");
            //
            echo "</a>\n";
            
        } else {
            parent::retoursousformulaire($idxformulaire, $retourformulaire, $val,
                                  $objsf, $premiersf, $tricolsf, $validation,
                                  $idx, $maj, ($this->getParameter("maj")==2&&$validation==1)?"tab":$retour);
        }
    }

    /**
     * Si le dossier d'instruction auquel est rattachée la consultation est 
     * cloturé, on affiche pas les liens du portlet.
     *
     * @return boolean true si non cloturé false sinon
     */
    function is_dossier_instruction_not_closed() {
        $idxformulaire = $this->getParameter("idxformulaire");
        $retourformulaire = $this->getParameter("retourformulaire");
        //Si le dossier d'instruction auquel est rattachée la consultation est 
        //cloturé, on affiche pas les liens du portlet
        if ( $idxformulaire != '' && 
            (
                $retourformulaire == 'dossier' ||
                $retourformulaire == 'dossier_instruction' ||
                $retourformulaire == 'dossier_instruction_mes_encours' ||
                $retourformulaire == 'dossier_instruction_tous_encours' ||
                $retourformulaire == 'dossier_instruction_mes_clotures' ||
                $retourformulaire == 'dossier_instruction_tous_clotures'
                || $retourformulaire == 'dossier_contentieux_mes_infractions'
                || $retourformulaire == 'dossier_contentieux_toutes_infractions'
                || $retourformulaire == 'dossier_contentieux_mes_recours'
                || $retourformulaire == 'dossier_contentieux_tous_recours'
            )){
                
            //On récuppère le statut du dossier d'instruction        
            $statut = $this->f->getStatutDossier($idxformulaire);
            if ( $this->f->isUserInstructeur() && $statut == "cloture" ){
                return false;
            }
        }
        return true;
    }
    
    
    function is_editable(){
        
        if ($this->f->can_bypass("dossier_commission", "modifier")){
            return true;
        }
        
        if ($this->is_dossier_instruction_not_closed() === true &&
            $this->is_instructeur_from_division_dossier() === true) {
            return true;
        }
        return false;
    }
    
    function is_deletable(){
        
        if ($this->f->can_bypass("dossier_commission", "supprimer")){
            return true;
        }
        
        if ($this->is_dossier_instruction_not_closed() === true
            && $this->getVal("avis") == ''
            && ($this->is_instructeur_from_division_dossier() === true
                || $this->f->can_bypass("dossier_commission", "supprimer_division"))) {
            //
            return true;
        }
        return false;
    }
    
    function is_markable(){
        
        if($this->f->can_bypass("dossier_commission", "modifier_lu")){
            return true;
        }
        
        if ($this->is_instructeur_from_division_dossier() === true){
            return true;
        }
        return false;
    }
    
    /**
     * Si le champ lu est à true l'action "Marquer comme lu" n'est pas affichée
     *
     * @return boolean true sinon lu false sinon
     */
    function show_marquer_comme_lu_portlet_action() {
        if (isset($this->val[array_search("lu", $this->champs)])
            && $this->val[array_search("lu", $this->champs)]== "t") {
            return false;
        }
        return true;
    }

    /*
     * CONDITION - can_user_access_dossier_contexte_ajout
     *
     * Vérifie que l'utilisateur a bien accès au dossier d'instruction passé dans le
     * formulaire d'ajout.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_ajout() {

        ($this->f->get_submitted_get_value('idxformulaire') !== null ? $id_dossier = 
            $this->f->get_submitted_get_value('idxformulaire') : $id_dossier = "");
        //
        if ($id_dossier !== "") {
            require_once "../obj/dossier_instruction.class.php";
            $dossier = new dossier_instruction($id_dossier, $f->db, DEBUG);
            //
            return $dossier->can_user_access_dossier();
        }
        return false;
    }

   /*
     * CONDITION - can_user_access_dossier_contexte_modification
     *
     * Vérifie que l'utilisateur a bien accès au dossier lié à la commission instanciée.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_modification() {

        $id_dossier = $this->getVal('dossier');
        //
        if ($id_dossier !== "" && $id_dossier !== null) {
            require_once "../obj/dossier_instruction.class.php";
            $dossier = new dossier_instruction($id_dossier, $f->db, DEBUG);
            //
            return $dossier->can_user_access_dossier();
        }
        return false;
    }



    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        parent::setSelect($form, $maj, $db, $debug);

        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        $crud = $this->get_action_crud($maj);
        // Le but ici est de brider les types aux types de la même commune que le dossier en cas d'ajout
        if (($this->retourformulaire == 'dossier_commission'
            || $this->retourformulaire == 'dossier_instruction')
            && ($crud == 'create' OR ($crud === null AND $maj == 0)
                || $crud === 'update' OR ($crud === null AND $maj == 1))) {
            require_once "../obj/dossier_instruction.class.php";
            $di = new dossier_instruction($this->getParameter('idxformulaire'), $db, $debug);
            $sql_commission_type_by_di = str_replace('<collectivite_di>',
                $di->getVal("om_collectivite"), $sql_commission_type_by_di);
            $this->init_select($form, $db, $maj, $debug, "commission_type",
                $sql_commission_type_by_di, $sql_commission_type_by_id, true);
        }
    }

}// fin classe
?>
