<?php
/**
 * DBFORM - 'delegataire' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'delegataire'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/demandeur.class.php";

/**
 * Classe qui hérite de la classe demandeur
 */
class delegataire extends demandeur {

    /*
     * Cache les champs de notification, fréquent et type_demandeur.
     */
    function setType(&$form,$maj) {
        parent::setType($form,$maj);

        $form->setType('type_demandeur', 'hidden');
        $form->setType('notification', 'hidden');
        $form->setType('frequent', 'hidden');
    }

    /*
     * Met le champ type_demandeur à delegataire par défaut
     */
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        if($maj == 0 ) {
             $form->setVal("type_demandeur","delegataire");
        }
    }
        /*
     * Met le champ type_demandeur à delegataire par défaut
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValSousFormulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);
        $form->setVal("type_demandeur","delegataire");
        if($maj == 0 ) {
            // Récupération des infos du demandeur passé en paramètre
            if($this->getParameter('idx_demandeur') != "") {
                include '../sql/pgsql/delegataire.form.inc.php';
                $sql = "SELECT ".implode(", ",$champs)." FROM ".DB_PREFIXE."demandeur ".
                        "WHERE demandeur=".$this->getParameter('idx_demandeur');
                $res = $this->db->query($sql);
                $this->f->addToLog("setValSousFormulaire() : db->query(\"".$sql."\")", VERBOSE_MODE);
                if ( database::isError($res)){
                    die();
                }
                $row = & $res->fetchRow(DB_FETCHMODE_ASSOC);
                foreach ($row as $key => $value) {
                    $form->setVal($key, $value);
                }
                $form->setVal("frequent","f");
            }
        }
    }

}

?>
