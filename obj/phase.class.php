<?php
/**
 * DBFORM - 'phase' - Surcharge gen.
 *
 * Ce script permet de définir la classe 'phase'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../gen/obj/phase.class.php";

class phase extends phase_gen {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Permet de définir les valeurs des champs en contexte formulaire.
     *
     * @param object  $form       Instance de formulaire.
     * @param integer $maj        Mode du formulaire.
     * @param integer $validation Validation du formulaire.
     *
     * @return void
     */
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        //
        parent::setVal($form, $maj, $validation, $db, $DEBUG);

        //
        $this->set_om_validite_debut_today($form, $maj, $validation);
    }


    /**
     * Permet de définir les valeurs des champs en contexte sous-formulaire.
     *
     * @param object  $form             Instance de formulaire.
     * @param integer $maj              Mode du formulaire.
     * @param integer $validation       Validation du formulaire.
     * @param integer $idxformulaire    Identifiant du formulaire parent.
     * @param string  $retourformulaire Formulaire parent.
     * @param string  $typeformulaire   Type du formulaire.
     *
     * @return void
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        //
        parent::setValsousformulaire($form,$maj,$validation,$idxformulaire,$retourformulaire,$typeformulaire, $db, $DEBUG);

        //
        $this->set_om_validite_debut_today($form, $maj, $validation);
    }


    /**
     * Permet de mettre la valeur du jour dans le champ om_validite_debut.
     *
     * @param object  $form       Instance de formulaire.
     * @param integer $maj        Mode du formulaire.
     * @param integer $validation Validation du formulaire.
     *
     * @return void
     */
    private function set_om_validite_debut_today(&$form, $maj, $validation) {

        //
        if ($validation == 0 && $maj == 0) {
            $form->setVal("om_validite_debut", date('d/m/Y'));
        }
    }


    /**
     * Permet de modifier le fil d'Ariane.
     * 
     * @param string $ent Fil d'Ariane.
     *
     * @return string
     */
    public function getFormTitle($ent) {

        // Fil d'ariane par défaut
        $ent = _("parametrage")." -> "._("Gestion des dossiers")." -> "._("phase");

        // Si différent de l'ajout
        if($this->getParameter("maj") != 0) {

            // Affiche la clé primaire
            $ent .= " -> ".$this->getVal("phase");

            // Affiche le libellé de la phase
            $ent .= " ".mb_strtoupper($this->getVal("code"), 'UTF-8');
        }

        // Change le fil d'Ariane
        return $ent;
    }

}

?>
