<?php
//$Id: donnees_techniques.class.php 5856 2016-02-03 11:35:25Z stimezouaght $ 
//gen openMairie le 13/02/2013 14:41

require_once ("../gen/obj/donnees_techniques.class.php");

class donnees_techniques extends donnees_techniques_gen {

    var $cerfa;     // Instance de la classe cerfa

    /**
     * Instance de la classe dossier.
     *
     * @var null
     */
    var $inst_dossier = null;

    /**
     * Instance de la classe dossier_autorisation
     *
     * @var null
     */
    var $inst_dossier_autorisation = null;

    /**
     * Instance de la classe lot
     *
     * @var null
     */
    var $inst_lot = null;

    /**
     * Instance de la classe cerfa.
     *
     * @var mixed (resource | null)
     */
    var $inst_cerfa = null;

    /**
     * Instance de la classe taxe_amenagement.
     *
     * @var mixed (resource | null)
     */
    var $inst_taxe_amenagement = null;

    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        //
        "lien_donnees_techniques_moyen_souleve" => array(
            "table_l" => "lien_donnees_techniques_moyen_souleve",
            "table_f" => "moyen_souleve",
            "field" => "ctx_moyen_souleve",
        ),
        //
        "lien_donnees_techniques_moyen_retenu_juge" => array(
            "table_l" => "lien_donnees_techniques_moyen_retenu_juge",
            "table_f" => "moyen_retenu_juge",
            "field" => "ctx_moyen_retenu_juge",
        ),
    );

    function donnees_techniques($id, &$db = null, $debug = null) {
        $this->constructeur($id,$db,$debug);
    }// fin constructeur

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        //
        $this->class_actions[1]["condition"] = "is_editable";

        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = "is_deletable";
        
        // ACTION - 003 - consulter
        $this->class_actions[3]["condition"] = "can_user_access_dossier_contexte_modification";

        // ACTION - 004 - action consulter spécifique au contexte de dossier d'autorisation
        $this->class_actions[4] = array(
            "identifier" => "consulter_contexte_da",
            "permission_suffix" => "consulter",
            "crud" => "read",
            "condition" => "can_user_access_dossier_contexte_dossier_autorisation_modification"
        );

        // ACTION - 005 - action consulter spécifique au contexte de lot
        $this->class_actions[5] = array(
            "identifier" => "consulter_contexte_lot",
            "permission_suffix" => "consulter",
            "crud" => "read",
            "condition" => "can_user_access_dossier_contexte_lot_modification"
        );
    }

    /**
     * CONDITION - is_deletable.
     *
     * Condition pour afficher le bouton de suppression. Cette action n'est pas utile donc
     * ne doit pas être disponible, même pour l'admin.
     *
     * @return boolean
     */
    function is_deletable() {
        //
        return false;
    }

    /**
     * CONDITION - is_editable.
     *
     * Condition pour afficher le bouton de modification.
     *
     * @return boolean
     */
    function is_editable() {

        // Si c'est un sous-formulaire du dossier d'autorisation
        if ($this->getParameter("retourformulaire") == 'dossier_autorisation'
            || $this->getParameter("retourformulaire") == 'dossier_autorisation_avis') {

            //
            return false;
        }

        // Contrôle si l'utilisateur possède un bypass
        $bypass = $this->f->isAccredited(get_class($this)."_modifier_bypass");
        //
        if ($bypass == true) {
            //
            return true;
        }

        // Si l'utilisateur est un intructeur qui correspond à la
        // division du dossier
        if ($this->is_instructeur_from_division_dossier() === true) {
            //
            return true;
        }

        //
        return false;
    }

    /**
     * Méthode permettant de récupérer l'id du cerfa lié au dossier.
     *
     * @return cerfa handler du cerfa
     */
    function getCerfa() {
        if($this->cerfa != null) {
            return $this->cerfa;
        }
        require_once ("../obj/cerfa.class.php");
        $this->cerfa = new cerfa($this->getVal("cerfa"), $this->db, DEBUG);
        return $this->cerfa;
    }

    /**
     * Méthode permettant de vérifier si le tableau passé en parametre est défini
     * dans la table des cerfa
     */

    function setTabSelect($tab, $idchamp) {
        // Test si un tableau de surface a été défini
        if ( $this->cerfa->getVal($idchamp) !=  "" ){
            
            // Configuration du tableau des surfaces
            $contenu['column_header']=$tab[$this->cerfa->getVal($idchamp)]['column_header'];
            $contenu['row_header']=$tab[$this->cerfa->getVal($idchamp)]['row_header'];
    
            foreach($tab[$this->cerfa->getVal($idchamp)]['values'] as $champ) {
                $contenu['values'][$champ] = $this->getVal($champ);
            }
    
            $this->form->setSelect($idchamp,$contenu);
        }
    }


    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        if(empty($this->cerfa)) {
            $this->getCerfa();
        }

        $this->setTabSelect($tab_surface, "tab_surface");
        $this->setTabSelect($tab_surface2, "tab_surface2");
        $this->setTabSelect($tab_tax_su_princ, "tab_tax_su_princ");
        $this->setTabSelect($tab_tax_su_heber, "tab_tax_su_heber");
        $this->setTabSelect($tab_tax_su_secon, "tab_tax_su_secon");
        $this->setTabSelect($tab_tax_su_tot, "tab_tax_su_tot");
        $this->setTabSelect($tab_tax_su_non_habit_surf, "tab_tax_su_non_habit_surf");
        $this->setTabSelect($tab_tax_su_parc_statio_expl_comm, "tab_tax_su_parc_statio_expl_comm");
        $this->setTabSelect($tab_tax_am, "tab_tax_am");
        $this->setTabSelect($tab_erp_eff, "tab_erp_eff");
        
        // Définition des champs Oui/Non/Je ne sais pas
        $value[] = array(
            "nesaispas",
            "non",
            "oui",
            );
        $value[] = array(
            _("Je ne sais pas"),
            _("Non"),
            _("Oui"),
            );
        
        $form->setSelect('terr_juri_titul',$value);
        $form->setSelect('terr_juri_lot',$value);
        $form->setSelect('terr_juri_zac',$value);
        $form->setSelect('terr_juri_afu',$value);
        $form->setSelect('terr_juri_pup',$value);
        $form->setSelect('terr_juri_oin',$value);
        $form->setSelect('terr_juri_desc',$value);
        $form->setSelect('terr_div_surf_etab',$value);
        $form->setSelect('terr_div_surf_av_div',$value);

        // Définition du champ type de dpc
        $dpc_type_values = array();
        $dpc_type_values[] = array(
            _("Fonds de commerce"),
            _("Fonds artisanal"),
            _("Bail commercial"),
            _("Terrain"),
        );
        $dpc_type_values[] = array(
            _("Fonds de commerce"),
            _("Fonds artisanal"),
            _("Bail commercial"),
            _("Terrain"),
        );
        $form->setSelect('dpc_type',$dpc_type_values);

        //
        $this->init_select($form, $this->f->db, $maj, null, "erp_class_cat", $sql_erp_class_cat, $sql_erp_class_cat_by_id, false);
        //
        $this->init_select($form, $this->f->db, $maj, null, "erp_class_type", $sql_erp_class_type, $sql_erp_class_type_by_id, false);
        //
        $this->init_select($form, $this->f->db, $maj, null, "ctx_objet_recours", $sql_ctx_objet_recours, $sql_ctx_objet_recours_by_id, true);

        //Récupérer le nom et le prénom de l'architecte
        $coordonneesArchitecte = $this->getPrenomNomArchitecte($this->getVal('architecte'));
        $value = "";
        if ($maj<2){
            $value = array(
                "data" => $coordonneesArchitecte,
                "obj" => "architecte", 
            );
        }
        else {
            $value[0][0]=$this->getVal('architecte');
            $value[1][0]=$coordonneesArchitecte;// table
        }
        $form->setSelect('architecte', $value);

        // Champ select_multiple - ctx_moyen_souleve
        $sql_ctx_moyen_souleve = $sql_ctx_moyen_souleve_select.$sql_ctx_moyen_souleve_from.$sql_ctx_moyen_souleve_where;
        $sql_ctx_moyen_souleve_by_id = $sql_ctx_moyen_souleve_select.$sql_ctx_moyen_souleve_from.$sql_ctx_moyen_souleve_by_id_where;
        //
        $this->init_select($form, $db, $maj, $debug, "ctx_moyen_souleve", $sql_ctx_moyen_souleve, $sql_ctx_moyen_souleve_by_id, true, true);

        // Champ select_multiple - ctx_moyen_retenu_juge
        $sql_ctx_moyen_retenu_juge = $sql_ctx_moyen_retenu_juge_select.$sql_ctx_moyen_retenu_juge_from.$sql_ctx_moyen_retenu_juge_where;
        $sql_ctx_moyen_retenu_juge_by_id = $sql_ctx_moyen_retenu_juge_select.$sql_ctx_moyen_retenu_juge_from.$sql_ctx_moyen_retenu_juge_by_id_where;
        //
        $this->init_select($form, $db, $maj, $debug, "ctx_moyen_retenu_juge", $sql_ctx_moyen_retenu_juge, $sql_ctx_moyen_retenu_juge_by_id, true, true);
    }


    // XXX Créer une nouvelle méthode au même endroit que l'appel a checkAccessibility()
    function checkAccessibility() {

        parent::checkAccessibility();

        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        //
        if(empty($this->cerfa)) {
            $this->getCerfa();
        }

        $id_tab_surface = $this->cerfa->getVal("tab_surface");
        $id_tab_surface2 = $this->cerfa->getVal("tab_surface2");
        $id_tab_tax_su_princ = $this->cerfa->getVal("tab_tax_su_princ");
        $id_tab_tax_su_heber = $this->cerfa->getVal("tab_tax_su_heber");
        $id_tab_tax_su_secon = $this->cerfa->getVal("tab_tax_su_secon");
        $id_tab_tax_su_tot = $this->cerfa->getVal("tab_tax_su_tot");
        $id_tab_tax_su_non_habit_surf = $this->cerfa->getVal("tab_tax_su_non_habit_surf");
        $id_tab_tax_su_parc_statio_expl_comm = $this->cerfa->getVal("tab_tax_su_parc_statio_expl_comm");
        $id_tab_tax_am = $this->cerfa->getVal("tab_tax_am");
        $id_tab_erp_eff = $this->cerfa->getVal("tab_erp_eff");

        //Suppression des champs de tableaux
        if(!empty($id_tab_surface)) {
            foreach($tab_surface[$this->cerfa->getVal("tab_surface")]['values'] as $champ) {
                unset($this->champs[array_search($champ,$this->champs)]);
            }
        }
        if(!empty($id_tab_surface2)) {
            foreach($tab_surface2[$this->cerfa->getVal("tab_surface2")]['values'] as $champ) {
                unset($this->champs[array_search($champ,$this->champs)]);
            }
        }
        if(!empty($id_tab_tax_su_princ)) {
            foreach($tab_tax_su_princ[$this->cerfa->getVal("tab_tax_su_princ")]['values'] as $champ) {
                unset($this->champs[array_search($champ,$this->champs)]);
            }
        }
        if(!empty($id_tab_tax_su_heber)) {
            foreach($tab_tax_su_heber[$this->cerfa->getVal("tab_tax_su_heber")]['values'] as $champ) {
                unset($this->champs[array_search($champ,$this->champs)]);
            }
        }
        if(!empty($id_tab_tax_su_secon)) {
            foreach($tab_tax_su_secon[$this->cerfa->getVal("tab_tax_su_secon")]['values'] as $champ) {
                unset($this->champs[array_search($champ,$this->champs)]);
            }
        }
        if(!empty($id_tab_tax_su_tot)) {
            foreach($tab_tax_su_tot[$this->cerfa->getVal("tab_tax_su_tot")]['values'] as $champ) {
                unset($this->champs[array_search($champ,$this->champs)]);
            }
        }
        if(!empty($id_tab_tax_su_non_habit_surf)) {
            foreach($tab_tax_su_non_habit_surf[$this->cerfa->getVal("tab_tax_su_non_habit_surf")]['values'] as $champ) {
                unset($this->champs[array_search($champ,$this->champs)]);
            }
        }
        if(!empty($id_tab_tax_su_parc_statio_expl_comm)) {
            foreach($tab_tax_su_parc_statio_expl_comm[$this->cerfa->getVal("tab_tax_su_parc_statio_expl_comm")]['values'] as $champ) {
                unset($this->champs[array_search($champ,$this->champs)]);
            }
        }
        if(!empty($id_tab_tax_am)) {
            foreach($tab_tax_am[$this->cerfa->getVal("tab_tax_am")]['values'] as $champ) {
                unset($this->champs[array_search($champ,$this->champs)]);
            }
        }
        if(!empty($id_tab_erp_eff)) {
            foreach($tab_erp_eff[$this->cerfa->getVal("tab_erp_eff")]['values'] as $champ) {
                unset($this->champs[array_search($champ,$this->champs)]);
            }
        }
        // Renumérotation
        $this->champs = array_values($this->champs);
    }

    /**
     * Méthode permettant de définir le type des différents tableaux en fonction 
     * des valeurs du cerfa
     **/
    function setTabType($tab) {
        // Définition du type "tableau"
        if ( $this->cerfa->getVal($tab) !=  "" ){
            
            $this->form->setType($tab,'tableau');
        }
        //Le chache si non défini
        else {
            
            $this->form->setType($tab,'hidden');
        }
    }

    /**
     * Méthode permettant de définir le type des champs des tableaux en fonction 
     * des valeurs du cerfa
     **/
    function setTabChampType($tab) {
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");
        // Pour chaque champ dans la liste des champs du cerfa 
        $tableau = $$tab;
        foreach ($this->champs as $champ) {
            
            if(array_search($champ, $this->cerfa->champs) !== false) {
                // On les cache si décoché dans le formulaire de cerfa
                if($this->cerfa->getVal($champ) == 'f') {
                    $this->form->setType($champ,'hidden');
                }
            } else {
                
                if(!in_array($champ, $tableau[$this->cerfa->getVal($tab)]['values'])) {

                    $this->form->setType($champ,'hidden');
                }
            }
        }
    }



    function setType(&$form,$maj) {
        parent::setType($form,$maj);

        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        if(empty($this->cerfa)) {
            $this->getCerfa();
        }

        $this->setTabType("tab_surface");
        $this->setTabType("tab_surface2");
        $this->setTabType("tab_tax_su_princ");
        $this->setTabType("tab_tax_su_heber");
        $this->setTabType("tab_tax_su_secon");
        $this->setTabType("tab_tax_su_tot");
        $this->setTabType("tab_tax_su_non_habit_surf");
        $this->setTabType("tab_tax_su_parc_statio_expl_comm");
        $this->setTabType("tab_tax_am");
        $this->setTabType("tab_erp_eff");

        //Champs select pour les liste a choix oui/non/je ne sais pas (terr_*)
        if($maj == 0) {
            $form->setType('terr_juri_titul','select');
            $form->setType('terr_juri_lot','select');
            $form->setType('terr_juri_zac','select');
            $form->setType('terr_juri_afu','select');
            $form->setType('terr_juri_pup','select');
            $form->setType('terr_juri_oin','select');
            $form->setType('architecte', 'manage_with_popup');
            $form->setType('erp_class_cat','select');
            $form->setType('erp_class_type','select');
            $form->setType('ctx_objet_recours','select');
            $form->setType('ctx_moyen_souleve','select_multiple');
            $form->setType('ctx_moyen_retenu_juge','select_multiple');
            $form->setType('dpc_type','select');

        } elseif($maj == 1) {
            $form->setType('terr_juri_titul','select');
            $form->setType('terr_juri_lot','select');
            $form->setType('terr_juri_zac','select');
            $form->setType('terr_juri_afu','select');
            $form->setType('terr_juri_pup','select');
            $form->setType('terr_juri_oin','select');
            $form->setType('architecte', 'manage_with_popup');
            $form->setType('erp_class_cat','select');
            $form->setType('erp_class_type','select');
            $form->setType('ctx_objet_recours','select');
            $form->setType('ctx_moyen_souleve','select_multiple');
            $form->setType('ctx_moyen_retenu_juge','select_multiple');
            $form->setType('dpc_type','select');

        } elseif($maj == 2) {
            $form->setType('terr_juri_titul','selectstatic');
            $form->setType('terr_juri_lot','selectstatic');
            $form->setType('terr_juri_zac','selectstatic');
            $form->setType('terr_juri_afu','selectstatic');
            $form->setType('terr_juri_pup','selectstatic');
            $form->setType('terr_juri_oin','selectstatic');
            $form->setType('architecte', 'selectstatic');
            $form->setType('erp_class_cat','selectstatic');
            $form->setType('erp_class_type','selectstatic');
            $form->setType('ctx_objet_recours','selectstatic');
            $form->setType('ctx_moyen_souleve','select_multiple_static');
            $form->setType('ctx_moyen_retenu_juge','select_multiple_static');
            $form->setType('dpc_type','selectstatic');

        } elseif($maj == 3 || $maj == 4 || $maj == 5) {
            $form->setType('terr_juri_titul','selectstatic');
            $form->setType('terr_juri_lot','selectstatic');
            $form->setType('terr_juri_zac','selectstatic');
            $form->setType('terr_juri_afu','selectstatic');
            $form->setType('terr_juri_pup','selectstatic');
            $form->setType('terr_juri_oin','selectstatic');
            $form->setType('architecte', 'selectstatic');
            $form->setType('erp_class_cat','selectstatic');
            $form->setType('erp_class_type','selectstatic');
            $form->setType('ctx_objet_recours','selectstatic');
            $form->setType('ctx_moyen_souleve','select_multiple_static');
            $form->setType('ctx_moyen_retenu_juge','select_multiple_static');
            $form->setType('dpc_type','selectstatic');

        }

        // Anciens champs à conserver pour les anciens cerfa
        $form->setType("co_statio_avt_shob", "hidden");
        $form->setType("co_statio_apr_shob", "hidden");
        $form->setType("co_statio_avt_surf", "hidden");
        $form->setType("co_statio_apr_surf", "hidden");
        $form->setType("co_trx_amgt", "hidden");
        $form->setType("co_modif_aspect", "hidden");
        $form->setType("co_modif_struct", "hidden");
        $form->setType("co_trx_imm", "hidden");
        $form->setType("co_cstr_shob", "hidden");
        $form->setType("am_voyage_deb", "hidden");
        $form->setType("am_voyage_fin", "hidden");
        $form->setType("am_modif_amgt", "hidden");
        $form->setType("am_lot_max_shob", "hidden");
        $form->setType("mod_desc", "hidden");
        $form->setType("tr_total", "hidden");
        $form->setType("tr_partiel", "hidden");
        $form->setType("tr_desc", "hidden");
        $form->setType("avap_co_clot", "hidden");
        $form->setType("avap_aut_coup_aba_arb", "hidden");
        $form->setType("avap_ouv_infra", "hidden");
        $form->setType("avap_aut_inst_mob", "hidden");
        $form->setType("avap_aut_plant", "hidden");
        $form->setType("avap_aut_auv_elec", "hidden");
        $form->setType("tax_dest_loc_tr", "hidden");


        //Cache les champs des clés étrangères, elles sont renseignées automatiquement
        $form->setType('dossier_instruction', 'hidden');
        $form->setType('lot', 'hidden');
        $form->setType('cerfa', 'hidden');
        
        // Boucler sur les champs des données techniques pour cacher les données qui ne
        // doivent pas être saisies

        foreach ($this->champs as $champ) {
            if(array_search($champ, $this->cerfa->champs) !== false) {
                if($this->cerfa->getVal($champ) == 'f') {
                    $form->setType($champ,'hidden');
                }
            } else {
                $id_tab_surface = $this->cerfa->getVal("tab_surface");
                $id_tab_surface2 = $this->cerfa->getVal("tab_surface2");
                $id_tab_tax_su_princ = $this->cerfa->getVal("tab_tax_su_princ");
                $id_tab_tax_su_heber = $this->cerfa->getVal("tab_tax_su_heber");
                $id_tab_tax_su_secon = $this->cerfa->getVal("tab_tax_su_secon");
                $id_tab_tax_su_tot = $this->cerfa->getVal("tab_tax_su_tot");
                $id_tab_tax_su_non_habit_surf = $this->cerfa->getVal("tab_tax_su_non_habit_surf");
                $id_tab_tax_su_parc_statio_expl_comm = $this->cerfa->getVal("tab_tax_su_parc_statio_expl_comm");
                $id_tab_tax_am = $this->cerfa->getVal("tab_tax_am");
                $id_tab_erp_eff = $this->cerfa->getVal("tab_erp_eff");
                $hidden = true;

                // On cache tous les champs
                $form->setType($champ,'hidden');

                // On défini l'affichage des champs des tableaux de configuration
                // Si les tableau sont définis dans le cerfa on test si les champs des données
                // techniques sont définis dans les tableaux de configuration des tableaux
                // pour chaque cerfa  alors on les affiche en type "text"
                if(!empty($id_tab_surface)) {
                    if(in_array($champ, $tab_surface[$this->cerfa->getVal("tab_surface")]['values'])) {
                        $hidden = false;
                    }
                }
                if(!empty($id_tab_surface2)) {
                    if(in_array($champ, $tab_surface2[$this->cerfa->getVal("tab_surface2")]['values'])) {
                        $hidden = false;
                    }
                }
                if(!empty($id_tab_tax_su_princ)) {
                    if(in_array($champ, $tab_tax_su_princ[$this->cerfa->getVal("tab_tax_su_princ")]['values'])) {
                        $hidden = false;
                    }
                }
                if(!empty($id_tab_tax_su_heber)) {
                    if(in_array($champ, $tab_tax_su_heber[$this->cerfa->getVal("tab_tax_su_heber")]['values'])) {
                        $hidden = false;
                    }
                }
                if(!empty($id_tab_tax_su_secon)) {
                    if(in_array($champ, $tab_tax_su_secon[$this->cerfa->getVal("tab_tax_su_secon")]['values'])) {
                        $hidden = false;
                    }
                }
                if(!empty($id_tab_tax_su_tot)) {
                    if(in_array($champ, $tab_tax_su_tot[$this->cerfa->getVal("tab_tax_su_tot")]['values'])) {
                        $hidden = false;
                    }
                }
                if(!empty($id_tab_tax_su_non_habit_surf)) {
                    if(in_array($champ, $tab_tax_su_non_habit_surf[$this->cerfa->getVal("tab_tax_su_non_habit_surf")]['values'])) {
                        $hidden = false;
                    }
                }
                if(!empty($id_tab_tax_su_parc_statio_expl_comm)) {
                    if(in_array($champ, $tab_tax_su_parc_statio_expl_comm[$this->cerfa->getVal("tab_tax_su_parc_statio_expl_comm")]['values'])) {
                        $hidden = false;
                    }
                }
                if(!empty($id_tab_tax_am)) {
                    if(in_array($champ, $tab_tax_am[$this->cerfa->getVal("tab_tax_am")]['values'])) {
                        $hidden = false;
                    }
                }
                if(!empty($id_tab_erp_eff)) {
                    if(in_array($champ, $tab_erp_eff[$this->cerfa->getVal("tab_erp_eff")]['values'])) {
                        $hidden = false;
                    }
                }

                if(!$hidden){
                    if($maj < 2) {
                        $form->setType($champ,'text');
                    } else {
                        $form->setType($champ,'static');
                    }
                    
                }
            }
        }
    }

    // Ajout des clés étrangères
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            
            //Si on est dans le dossier
            if($retourformulaire =='dossier' || $retourformulaire =='dossier_instruction' ) {
                
                $form->setVal('dossier_instruction', $idxformulaire);
                $form->setVal('lot', "");
            }
            
            //Si on est dans le lot
            if($retourformulaire =='lot') {
                
                $form->setVal('dossier_instruction', "");
                $form->setVal('lot', $idxformulaire);
            }
        }// fin validation
    }// fin setValsousformulaire

    function setLib(&$form,$maj) {
        parent::setLib($form,$maj);
        //libelle des champs
        $form->setLib('tab_surface', "");
        $form->setLib('tab_surface2', "");
        $form->setLib('tab_tax_su_princ', "");
        $form->setLib('tab_tax_su_heber', "");
        $form->setLib('tab_tax_su_secon', "");
        $form->setLib('tab_tax_su_tot', "");
        $form->setLib('tab_tax_su_non_habit_surf', "");
        $form->setLib('tab_tax_su_parc_statio_expl_comm', "");
        $form->setLib('tab_tax_am', "");
        $form->setLib('tab_erp_eff', "");
        $form->setLib('ope_proj_desc', _("description"));

        // Champs select_multiple
        $form->setLib('ctx_moyen_souleve', _("ctx_moyen_souleve"));
        $form->setLib('ctx_moyen_retenu_juge', _("ctx_moyen_retenu_juge"));
    }

    function setLayout(&$form, $maj) {

        // Terrain
        $form->setBloc('terr_juri_titul','D',"","col_12");
            $form->setFieldset('terr_juri_titul','D'
                               ,_("Terrain"), "startClosed");

                $form->setBloc('terr_juri_titul','D',_("Situation juridique du terrain"), "col_12 alignFormSpec");
                $form->setBloc('terr_juri_oin','F');
                $form->setBloc('terr_juri_desc','DF',"", "group");
                $form->setBloc('terr_div_surf_etab','D',_("Terrain issu d'une division de propriete"), "col_12 alignFormSpec");
                $form->setBloc('terr_div_surf_av_div', 'F');

            $form->setFieldset('terr_div_surf_av_div','F','');
            
        $form->setBloc('terr_div_surf_av_div','F');

        // Description de la demande / du projet
        $form->setFieldset('ope_proj_desc', 'D',
            _("Description de la demande / du projet"), "col_12 startClosed");
            $form->setBloc('ope_proj_desc', 'DF', "", "group");
            $form->setBloc('ope_proj_div_co', 'DF', "", "group");
            $form->setBloc('ope_proj_div_contr', 'DF', "", "group");
        $form->setFieldset('ope_proj_div_contr', 'F');

        // Construire, aménager ou modifier un ERP
        $form->setBloc('erp_class_cat','D',"","col_12");
            $form->setFieldset('erp_class_cat','D'
                               ,_("Construire, amenager ou modifier un ERP"), "startClosed");

            $form->setBloc('erp_class_cat','DF', _("Activite"),"alignFormSpec");
            $form->setBloc('erp_class_type','DF', "","alignFormSpec");

            $form->setBloc('erp_cstr_neuve','DF', _("Nature des travaux (plusieurs cases possibles)"),"col_12");
            $form->setBloc('erp_cstr_neuve','DF', "","alignFormSpec");
            $form->setBloc('erp_trvx_acc','DF', "","alignFormSpec");
            $form->setBloc('erp_extension','DF', "","alignFormSpec");
            $form->setBloc('erp_rehab','DF', "","alignFormSpec");
            $form->setBloc('erp_trvx_am','DF', "","alignFormSpec");
            $form->setBloc('erp_vol_nouv_exist','DF', "","alignFormSpec");

            $form->setBloc('tab_erp_eff','D',_("Effectif"),"col_12");
                $form->setBloc('tab_erp_eff','DF', "", "col_12");
            $form->setBloc('tab_erp_eff','F', "","");

            $form->setFieldset('tab_erp_eff','F', '');
        $form->setBloc('tab_erp_eff','F');

        // Aménager
        $form->setBloc('am_lotiss','D',"","col_12");
            $form->setFieldset('am_lotiss','D'
                                       ,_("Amenager"), "startClosed");
                $form->setBloc('am_lotiss','D',"","col_12");
                    $form->setFieldset('am_lotiss','D'
                                       ,_("Projet d'amenagement"), "startClosed alignFormSpec");
                        // bloc 4.1
                        $form->setBloc('am_lotiss','DF',_("Nature des travaux, installations
                                       ou amenagements envisages"), "group");
                        $form->setBloc('am_div_mun','DF',"", "group");
                        $form->setBloc('am_autre_div','DF',"", "group");
                        $form->setBloc('am_camping','DF',"", "group");
                        $form->setBloc('am_parc_resid_loi','DF',"", "group");
                        $form->setBloc('am_sport_moto','DF',"", "group");
                        $form->setBloc('am_sport_attrac','DF',"", "group");
                        $form->setBloc('am_sport_golf','DF',"", "group");
                        $form->setBloc('am_caravane','DF',"", "group");
                        $form->setBloc('am_carav_duree','DF',"", "group");
                        $form->setBloc('am_statio','DF',"", "group");
                        $form->setBloc('am_statio_cont','DF',"", "group");
                        $form->setBloc('am_affou_exhau','DF',"", "group");
                        $form->setBloc('am_affou_exhau_sup','DF',"", "group");
                        $form->setBloc('am_affou_prof','DF',"", "group");
                        $form->setBloc('am_exhau_haut','DF',"", "group");
                        $form->setBloc('am_terr_res_demon','DF',"", "group");
                        $form->setBloc('am_air_terr_res_mob','DF',"", "group");
                        
                        $form->setBloc('am_chem_ouv_esp','D',_("Dans les secteurs proteges :"),"col_12");
                            $form->setBloc('am_chem_ouv_esp','DF',_("Amenagement situe dans un espace remarquable :"), "group");
                            $form->setBloc('am_agri_peche','DF',"", "group");
                            
                            $form->setBloc('am_crea_voie','DF',_("Amenagement situe dans un secteur sauvegarde :"), "group");
                            $form->setBloc('am_modif_voie_exist','DF',"", "group");
                            $form->setBloc('am_crea_esp_sauv','DF',"", "group");
                            
                            $form->setBloc('am_crea_esp_class','DF',_("Amenagement situe dans un site classe ou une reserve naturelle 1 :"), "group");
                            $form->setBloc('am_coupe_abat','DF',"", "group");
                            $form->setBloc('am_prot_plu','DF',"", "group");
                            $form->setBloc('am_prot_muni','DF',"", "group");
                            $form->setBloc('am_mobil_voyage','DF',"", "group");
                            $form->setBloc('am_aire_voyage','DF',"", "group");
                            $form->setBloc('am_rememb_afu','DF',"", "group");
                            $form->setBloc('co_ouvr_infra','DF',"", "group");
                        $form->setBloc('co_ouvr_infra','F');

                        $form->setBloc('am_mob_art','DF',_("Dans un secteur sauvegarde, site classe ou reserve naturelle :"), "group");
                        $form->setBloc('am_modif_voie_esp','DF',"", "group");
                        $form->setBloc('am_plant_voie_esp','DF',"", "group");
                        $form->setBloc('co_ouvr_elec','DF',"", "group");
                    $form->setFieldset('co_ouvr_elec','F','');
                $form->setBloc('co_ouvr_elec','F');
                $form->setBloc('am_projet_desc','D',"","col_12");
                    $form->setFieldset('am_projet_desc','D'
                                       ,_("Description amenagement"), "startClosed");

                        $form->setBloc('am_projet_desc','DF',"", "group");
                        $form->setBloc('am_terr_surf','DF',"", "alignFormSpec");
                        $form->setBloc('am_tranche_desc','DF',"", "group");
                    $form->setFieldset('am_tranche_desc','F','');
                $form->setBloc('am_tranche_desc','F');
                $form->setBloc('am_lot_max_nb','D',"","col_12");
                    $form->setFieldset('am_lot_max_nb','D'
                                       ,_("Complement d'amenagement"), "startClosed");
                        // bloc 4.2
                        $form->setBloc('am_lot_max_nb','D',_("Demande concernant un lotissement"),"col_12 alignFormSpec");
                        
                            $form->setBloc('am_lot_max_nb','DF',"", "group");
                            $form->setBloc('am_lot_max_shon','DF',"", "group");
                        
                            $form->setBloc('am_lot_cstr_cos','DF',_("Comment la constructibilite globale sera-t-elle repartie ?"), "group");
                            $form->setBloc('am_lot_cstr_plan','DF',"", "group");
                            $form->setBloc('am_lot_cstr_vente','DF',"", "group");
                            $form->setBloc('am_lot_fin_diff','DF',"", "group");
                            
                            $form->setBloc('am_lot_consign','DF',_("si oui, quelle garantie sera utilisee ?"), "group");
                            $form->setBloc('am_lot_gar_achev','DF',"", "group");
                            $form->setBloc('am_lot_vente_ant','DF',"", "group");
                        $form->setBloc('am_lot_vente_ant','F');

                        // bloc 4.3
                        $form->setBloc('am_exist_agrand','D',_("Amenagement d'un camping ou
                                       d'un terrain amenage en vue de l'hebergement
                                       touristique"),"col_12");
                                       
                            $form->setBloc('am_exist_agrand','DF',"", "alignFormSpec");
                                $form->setBloc('am_exist_date','DF',"", "alignFormSpec-type-date");
                                $form->setBloc('am_exist_num','D',"", "alignFormSpec");
                            $form->setBloc('am_empl_nb','F',"", "");
                            
                            $form->setBloc('am_tente_nb','D',_("Nombre maximum d’emplacements reserves aux :"), "col_12 alignForm");
                            $form->setBloc('am_mobil_nb','F',"", "");
                            
                            $form->setBloc('am_pers_nb','DF',"", "alignFormSpec group");
                            
                            $form->setBloc('am_empl_hll_nb','D',_("Implantation d’habitations legeres de loisirs (HLL) :"), "col_12 alignFormSpec");
                            //$form->setBloc('am_empl_hll_nb','DF',"", "group");
                            $form->setBloc('am_hll_shon','F');
                                
                            $form->setBloc('am_periode_exploit','DF',"", "group");
                            
                            $form->setBloc('am_coupe_bois','D',_("Declaration de coupe et/ou abattage d’arbres :"),"col_12 cerfasubtitle");
                                
                                $form->setBloc('am_coupe_bois','D',_("Courte description du lieu :"), "cerfasubtitle alignForm");
                                $form->setBloc('am_coupe_align','F',"", "");
                                
                                $form->setBloc('am_coupe_ess','D',_("Nature du boisement :"),"col_12 cerfasubtitle alignForm");
                                $form->setBloc('am_coupe_autr','F');
                            $form->setBloc('am_coupe_autr','F');
                            
                        $form->setBloc('am_coupe_autr','F');


                    $form->setFieldset('am_coupe_autr','F','');
                
                $form->setBloc('am_coupe_autr','F');
            $form->setFieldset('am_coupe_autr','F','');
        $form->setBloc('am_coupe_autr','F');
        // Fin amménager
        // Construire
        $form->setBloc('co_archi_recours','D',"","col_12");
            $form->setFieldset('co_archi_recours','D'
                               ,_("Construire"), "startClosed");
                $form->setBloc('co_archi_recours','D', "","col_12");
                    $form->setFieldset('co_archi_recours','D'
                               ,_("Projet construction"), "startClosed");
                
                        $form->setBloc('co_archi_recours','D',_("Architecte"), "col_12");
                            $form->setBloc('co_archi_recours','DF',"", "alignFormSpec group");
                            $form->setBloc('architecte','DF', "", "group");
                        $form->setBloc('architecte','F');
                        
                        $form->setBloc('co_cstr_nouv','D',_("Nature du projet"), "col_12 alignFormSpec");
                        $form->setBloc('avap_co_elt_pro','DF',"", "group");
                        $form->setBloc('avap_nouv_haut_surf','DF',"", "group");
                        $form->setBloc('co_cloture','F');
                        $form->setBloc('co_projet_desc','DF',"", "group");
                        $form->setBloc('co_elec_tension','DF', "", "alignFormSpec");
                    $form->setFieldset('co_elec_tension','F','');
                    $form->setFieldset('co_tot_log_nb','D'
                               ,_("Complement construction"), "startClosed");

                        $form->setBloc('co_tot_log_nb','D',"", "alignForm");
                        $form->setBloc('co_tot_coll_nb','F',"", "");
                        
                        $form->setBloc('co_mais_piece_nb','D',"", "alignForm");
                        $form->setBloc('co_mais_niv_nb','F',"", "");
                        
                        $form->setBloc('co_fin_lls_nb','D', _("Repartition du nombre total de logement crees par type de financement :"),"col_12");
                            $form->setBloc('co_fin_lls_nb','D',"", "alignForm");
                            $form->setBloc('co_fin_autr_nb','F',"", "");
                            
                            $form->setBloc('co_fin_autr_desc','DF',"", "alignFormSpec group");
                            $form->setBloc('co_mais_contrat_ind','DF',"", "alignFormSpec group");
                        $form->setBloc('co_mais_contrat_ind','F');
                        
                        $form->setBloc('co_uti_pers','D',_("Mode d'utilisation principale des logements :"), "col_12");
                            $form->setBloc('co_uti_pers','D', "", "alignForm");
                            $form->setBloc('co_uti_loc','F',"", "");
                        $form->setBloc('co_uti_loc','F',"", "");
                        
                        $form->setBloc('co_uti_princ','D',_("S’il s’agit d’une occupation personnelle, veuillez preciser :"), "col_12");
                            $form->setBloc('co_uti_princ','D',"", "alignForm");
                            $form->setBloc('co_uti_secon','F',"", "");
                        $form->setBloc('co_uti_secon','F',"", "group");

                        $form->setBloc('co_anx_pisc','D',_("Si le projet porte sur une annexe a l’habitation, veuillez preciser :"), "col_12");
                            $form->setBloc('co_anx_pisc','D',"", "alignForm");
                            $form->setBloc('co_anx_autr_desc','F',"", "");
                        $form->setBloc('co_anx_autr_desc','F',"", "group");
                        
                        $form->setBloc('co_resid_agees','D',_("Si le projet est un foyer ou une residence, a quel titre :"), "col_12 alignForm");
                            $form->setBloc('co_resid_agees','D',"", "alignForm");
                            $form->setBloc('co_resid_hand','F',"", "");
                        $form->setBloc('co_resid_hand','F',"", "group");
                        
                        $form->setBloc('co_resid_autr','DF',"", "group alignFormSpec");
                        $form->setBloc('co_resid_autr_desc','DF',"", "group");
                        $form->setBloc('co_foyer_chamb_nb','DF',"", "group alignFormSpec");
                        
                        $form->setBloc('co_log_1p_nb','D',_("Repartition du nombre de logements crees selon le nombre de pieces :"), "col_12");
                            $form->setBloc('co_log_1p_nb','D', "", "alignForm");
                            $form->setBloc('co_log_6p_nb','F',"", "group");
                        $form->setBloc('co_log_6p_nb','F',"", "group");
                        
                        $form->setBloc('co_bat_niv_nb','DF',"", "alignFormSpec");
                        
                        $form->setBloc('co_trx_exten','D',_("Indiquez si vos travaux comprennent notamment :"), "col_12");
                            $form->setBloc('co_trx_exten','D',"", "alignForm");
                            $form->setBloc('co_trx_nivsup','F',"", "group");

                            $form->setBloc('co_demont_periode','DF', _("Construction periodiquement demontee et re-installee :"),"col_12 group");
                        $form->setBloc('co_demont_periode','F',"", "group");

                    $form->setFieldset('co_demont_periode','F','');
                    $form->setFieldset('tab_surface','D'
                               ,_("Destinations et surfaces des constructions"), "startClosed");
                        $form->setBloc('tab_surface','D', _("Destination des constructions et tableau des surfaces (uniquement à remplir si votre projet de construction est situé dans une commune couverte par un plan local d’urbanisme ou un document en tenant lieu appliquant l’article R.123-9 du code de l’urbanisme dans sa rédaction antérieure au 1er janvier 2016)."),"col_12 group");
                        $form->setBloc('tab_surface','F');
                        $form->setBloc('co_sp_transport','D', _("Destination des constructions futures en cas de realisation au benefice d'un service public ou d'interet collectif :"),"col_12");
                            $form->setBloc('co_sp_transport','D', "","alignForm");
                            $form->setBloc('co_sp_culture','F', "","");
                        $form->setBloc('co_sp_culture','F', "","");

                        $form->setBloc('tab_surface2','DF', _("Destination, sous-destination des constructions et tableau des surfaces (uniquement à remplir si votre projet de construction est situé dans une commune couverte par le règlement national d’urbanisme, une carte communale ou dans une commune non visée à la rubrique précédente"),"col_12 group");

                    $form->setFieldset('tab_surface2','F','');
                    $form->setFieldset('co_statio_avt_nb','D'
                               ,_("Divers construction"), "startClosed");

                        $form->setBloc('co_statio_avt_nb','D', _("Nombre de places de stationnement"),"col_12");
                            $form->setBloc('co_statio_avt_nb','D', "","alignForm");
                            $form->setBloc('co_statio_apr_nb','F', "","");
                        $form->setBloc('co_statio_apr_nb','F', "","");
                        
                        $form->setBloc('co_statio_adr','D', _("Places de stationnement affectees au projet, amenagees ou reservees en dehors du terrain sur lequel est situe le projet"),"col_12");
                            $form->setBloc('co_statio_adr','DF', "","group");
                            
                            $form->setBloc('co_statio_place_nb','D', "","col_12");
                                $form->setBloc('co_statio_place_nb','D', "","alignForm");
                                $form->setBloc('co_statio_tot_shob','F', "","");
                            $form->setBloc('co_statio_tot_shob','F', "","");
                        $form->setBloc('co_statio_tot_shob','F');
                        $form->setBloc('co_statio_comm_cin_surf','D', _("Pour les commerces et cinemas :"),"col_12 alignFormSpec");
                        $form->setBloc('co_perf_energ','F',"", "");
                
                    $form->setFieldset('co_perf_energ','F','');
                $form->setBloc('co_perf_energ','F');

            $form->setFieldset('co_perf_energ','F','');
        
        $form->setBloc('co_perf_energ','F');
        // Fin construire

        /*Fieldset n°6 Projet necessitant demolitions */
        $form->setBloc('dm_constr_dates','D',"","col_12");
            $form->setFieldset('dm_constr_dates','D'
                               ,_("Demolir"), "startClosed");
                $form->setBloc('dm_constr_dates','DF', "","group");
                $form->setBloc('dm_total','D', "","alignFormSpec");
                $form->setBloc('dm_partiel','F');
                $form->setBloc('dm_projet_desc','DF', "","group");
                $form->setBloc('dm_tot_log_nb','DF', "","alignFormSpec");
            $form->setFieldset('dm_tot_log_nb','F','');
        
        $form->setBloc('dm_tot_log_nb','F');

        /*Fieldset n°4 Ouverture de chantier */
        $form->setBloc('doc_date','D',"","col_12");
            $form->setFieldset('doc_date','D'
                               ,_("Ouverture de chantier"), "startClosed alignFormSpec");
                $form->setBloc('doc_date','DF', "","group");
                $form->setBloc('doc_tot_trav','DF', "","group");
                $form->setBloc('doc_tranche_trav','DF', "","group");
                $form->setBloc('doc_tranche_trav_desc','DF', "","group");
                $form->setBloc('doc_surf','DF', "","group");
                $form->setBloc('doc_nb_log','DF', "","group");
                $form->setBloc('doc_nb_log_indiv','DF', "","group");
                $form->setBloc('doc_nb_log_coll','DF', "","group");
                //
                $form->setBloc('doc_nb_log_lls','DF', _("Repartition du nombre de logements commences par type de financement"), "group");
                $form->setBloc('doc_nb_log_aa','DF', "","group");
                $form->setBloc('doc_nb_log_ptz','DF', "","group");
                $form->setBloc('doc_nb_log_autre','DF', "","group");
            $form->setFieldset('doc_nb_log_autre','F','');
        $form->setBloc('doc_nb_log_autre','F');

        /*Fieldset n°4  Achèvement des travaux */
        $form->setBloc('daact_date','D',"","col_12");
            $form->setFieldset('daact_date','D'
                               ,_("Achevement des travaux") , "startClosed alignFormSpec");
                $form->setBloc('daact_date','DF', "","group");
                $form->setBloc('daact_date_chgmt_dest','DF', "","group");
                $form->setBloc('daact_tot_trav','DF', "","group");
                $form->setBloc('daact_tranche_trav','DF', "","group");
                $form->setBloc('daact_tranche_trav_desc','DF', "","group");
                $form->setBloc('daact_surf','DF', "","group");
                $form->setBloc('daact_nb_log','DF', "","group");
                $form->setBloc('daact_nb_log_indiv','DF', "","group");
                $form->setBloc('daact_nb_log_coll','DF', "","group");
                //
                $form->setBloc('daact_nb_log_lls','DF', _("Repartition du nombre de logements commences par type de financement"), "group");
                $form->setBloc('daact_nb_log_aa','DF', "","group");
                $form->setBloc('daact_nb_log_ptz','DF', "","group");
                $form->setBloc('daact_nb_log_autre','DF', "","group");
            $form->setFieldset('daact_nb_log_autre','F','');
        $form->setBloc('daact_nb_log_autre','F');

        // Début DIA
        $form->setBloc('dia_imm_non_bati','D',"","col_12");
        $form->setFieldset('dia_imm_non_bati','D',_("Déclaration d’intention d’aliéner un bien"), "startClosed");

            // Désignation du bien
            $form->setFieldset('dia_imm_non_bati','D',_("Désignation du bien"), "startClosed");

                // Immeuble
                $form->setBloc('dia_imm_non_bati','D', _("Immeuble"),"alignForm group");
                $form->setBloc('dia_imm_bati_terr_autr_desc','F', "","");

                // Occupation du sol en superficie
                $form->setBloc('dia_occ_sol_su_terre','D', _("Occupation du sol en superficie (m²)"),"alignForm group");
                $form->setBloc('dia_occ_sol_su_sol','F', "","");

                //
                $form->setBloc('dia_bati_vend_tot','D', " ","alignForm group");
                $form->setBloc('dia_bati_vend_tot_txt','F', "","");
                $form->setBloc('dia_su_co_sol','D', "","alignForm group");
                $form->setBloc('dia_su_util_hab','F', "","");
                $form->setBloc('dia_nb_niv','D', _("Nombre de"),"alignForm group");
                $form->setBloc('dia_nb_autre_loc','F', "","");
                $form->setBloc('dia_vente_lot_volume','D', " ","alignForm group");
                $form->setBloc('dia_vente_lot_volume_txt','F', "","");

                // Copropriété
                $form->setBloc('dia_bat_copro','D', " ","alignForm group");
                $form->setBloc('dia_bat_copro_desc','F', "","");
                // Tableau lot
                $form->setBloc('dia_lot_numero','D', "","alignForm group");
                $form->setBloc('dia_lot_nat_su','F', "","");
                $form->setBloc('dia_lot_bat_achv_plus_10','D', _("Le bâtiment est achevé depuis"),"alignForm group");
                $form->setBloc('dia_lot_bat_achv_moins_10','F', "","");
                $form->setBloc('dia_lot_regl_copro_publ_hypo_plus_10','D', _("Le réglement de copropriété a été publié aux hypothèques depuis"),"alignForm group");
                $form->setBloc('dia_lot_regl_copro_publ_hypo_moins_10','F', "","");

                //
                $form->setBloc('dia_indivi_quote_part','DF', " ","alignFormSpec");
                $form->setBloc('dia_design_societe','DF', _("Droits sociaux"),"alignFormSpec group");
                $form->setBloc('dia_design_droit','DF', "","alignFormSpec");
                $form->setBloc('dia_droit_soc_nat','D', "","alignForm group");
                $form->setBloc('dia_droit_soc_num_part','F', "","");

            $form->setFieldset('dia_droit_soc_num_part','F','');

            // Usage et occupation
            $form->setFieldset('dia_us_hab','D',_("Usage et occupation"), "startClosed");

                // Usage
                $form->setBloc('dia_us_hab','D', _("Usage"),"alignForm group");
                $form->setBloc('dia_us_autre_prec','F', "","");

                // Occupation
                $form->setBloc('dia_occ_prop','D', _("Occupation"),"alignForm group");
                $form->setBloc('dia_occ_autre_prec','F', "","");

            $form->setFieldset('dia_occ_autre_prec','F','');

            // Droits réels ou personnels
            $form->setFieldset('dia_droit_reel_perso_grevant_bien_oui','D',_("Droits réels ou personnels"), "startClosed");

                //
                $form->setBloc('dia_droit_reel_perso_grevant_bien_oui','D', _("Grevant les biens"),"alignForm group");
                $form->setBloc('dia_droit_reel_perso_grevant_bien_non','F', "","");

                //
                $form->setBloc('dia_droit_reel_perso_nat','D', " ","alignForm group");
                $form->setBloc('dia_droit_reel_perso_viag','F', "","");

            $form->setFieldset('dia_droit_reel_perso_viag','F','');

            // Modalités de la cession
            $form->setFieldset('dia_mod_cess_prix_vente','D',_("Modalités de la cession"), "startClosed");

                // Vente amiable
                $form->setFieldset('dia_mod_cess_prix_vente','D', _("Vente amiable"), "startClosed");

                $form->setBloc('dia_mod_cess_prix_vente','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_prix_vente_mob','D', _("Dont éventuellement inclus"),"alignForm group");
                $form->setBloc('dia_mod_cess_prix_vente_autre','F', "","");

                //
                $form->setBloc('dia_mod_cess_adr','DF', _("Si vente indissociable d'autres biens"),"alignFormSpec");

                // Modalité de paiement
                $form->setBloc('dia_mod_cess_sign_act_auth','D', _("Modalités de paiement"),"alignForm group");
                $form->setBloc('dia_mod_cess_terme_prec','F', "","");
                $form->setBloc('dia_mod_cess_commi','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_commi_ht','F', "","");
                $form->setBloc('dia_mod_cess_bene_acquereur','D', _("Bénéficiaire"),"alignForm group");
                $form->setBloc('dia_mod_cess_bene_vendeur','F', "","");
                $form->setBloc('dia_mod_cess_paie_nat','DF', " ","alignFormSpec group");
                $form->setBloc('dia_mod_cess_design_contr_alien','DF', "","alignFormSpec group");
                $form->setBloc('dia_mod_cess_eval_contr','DF', "","alignFormSpec group");
                $form->setBloc('dia_mod_cess_rente_viag','DF', "","alignFormSpec group");
                $form->setBloc('dia_mod_cess_mnt_an','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_mnt_compt','F', "","");
                $form->setBloc('dia_mod_cess_bene_rente','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_droit_usa_hab','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_droit_usa_hab_prec','F', "","");
                $form->setBloc('dia_mod_cess_eval_usa_usufruit','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_vente_nue_prop','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_vente_nue_prop_prec','F', "","");
                $form->setBloc('dia_mod_cess_echange','DF', "","alignFormSpec group");
                $form->setBloc('dia_mod_cess_design_bien_recus_ech','DF', "","alignFormSpec group");
                $form->setBloc('dia_mod_cess_mnt_soulte','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_prop_contre_echan','F', "","");
                $form->setBloc('dia_mod_cess_apport_societe','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_bene','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_esti_bien','F', "","");
                $form->setBloc('dia_mod_cess_cess_terr_loc_co','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_esti_terr','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_esti_loc','F', "","");
                $form->setBloc('dia_mod_cess_esti_imm_loca','DF', "","alignFormSpec");

                $form->setFieldset('dia_mod_cess_esti_imm_loca','F','');

                // Adjudication
                $form->setFieldset('dia_mod_cess_adju_vol','D', _("Adjudication"), "startClosed");

                $form->setBloc('dia_mod_cess_adju_vol','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_adju_obl','F', "","");
                $form->setBloc('dia_mod_cess_adju_fin_indivi','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_adju_date_lieu','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_mnt_mise_prix','F', "","");

                $form->setFieldset('dia_mod_cess_mnt_mise_prix','F','');

            $form->setFieldset('dia_mod_cess_mnt_mise_prix','F','');

            // Les soussignés déclarent
            $form->setFieldset('dia_prop_titu_prix_indique','D',_("Les soussignés déclarent"), "startClosed");

                //
                $form->setBloc('dia_prop_titu_prix_indique','DF', _("Que le(s) propriétaire(s) nommé(s) à la rubrique 1"),"alignFormSpec group");
                $form->setBloc('dia_prop_recherche_acqu_prix_indique','DF', "","alignFormSpec group");
                $form->setBloc('dia_acquereur_nom_prenom','DF', "","alignFormSpec group");
                $form->setBloc('dia_acquereur_prof','DF', "","alignFormSpec group");

                // Adresse
                $form->setBloc('dia_acquereur_adr_num_voie','D', _("Adresse"),"alignForm group");
                $form->setBloc('dia_acquereur_adr_localite','F', "","");

                //
                $form->setBloc('dia_indic_compl_ope','DF', " ","alignFormSpec group");
                $form->setBloc('dia_vente_adju','DF', "","alignFormSpec groupe");

            $form->setFieldset('dia_vente_adju','F','');

            // Observation
            $form->setFieldset('dia_observation','D',_("Observations"), "startClosed");

                //
                $form->setBloc('dia_observation','DF', "","alignFormSpec group");

            $form->setFieldset('dia_observation','F','');

        $form->setFieldset('dia_observation','F','');
        $form->setBloc('dia_observation','F',"","");
        // Fin DIA

        $form->setBloc('code_cnil','D',"","col_12");
            $form->setFieldset('code_cnil','D'
                               ,_("cnil (opposition à l’utilisation des informations du formulaire à des fins commerciales)") , "startClosed alignFormSpec");
                $form->setBloc('code_cnil','DF', "","group");
            $form->setFieldset('code_cnil','F','');
        $form->setBloc('code_cnil','F');

        $form->setBloc('tax_surf_tot_cstr','D',"","col_12");
            $form->setFieldset('tax_surf_tot_cstr','D'
                               ,_("Declaration des elements necessaires au calcul des impositions"), "startClosed");

                $form->setBloc('tax_surf_tot_cstr','D', _("Renseignement"),"col_12");
                    $form->setBloc('tax_surf_tot_cstr','D', "", "alignFormSpec");
                    $form->setBloc('tax_surf_suppr_mod','F', "","");
                $form->setBloc('tax_surf_suppr_mod','F', "","");

                $form->setBloc('tab_tax_su_princ','D',_("Creation de locaux destines a l’habitation :"),"col_12");
                    $form->setBloc('tab_tax_su_princ','DF',_("tab_tax_su_princ"), "col_12");
                    $form->setBloc('tab_tax_su_heber','DF',_("tab_tax_su_heber"), "col_12");
                $form->setBloc('tab_tax_su_tot','F', "","");
                //
                $form->setBloc('tax_ext_pret','DF', _("Extension de l’habitation principale, creation d’un batiment annexe a cette habitation ou d’un garage clos et couvert."), "alignFormSpec");
                $form->setBloc('tax_ext_desc','DF', "","group");
                $form->setBloc('tax_surf_tax_exist_cons','D', "","alignFormSpec");
                $form->setBloc('tax_log_ap_trvx_nb','F');

                //
                $form->setBloc('tax_surf_abr_jard_pig_colom','DF', _("Creation d’abris de jardin, de pigeonniers et colombiers"), "col_12");

                $form->setBloc('tax_comm_nb','D', _("Creation ou extension de locaux non destines a l'habitation :"),"col_12");
                    $form->setBloc('tax_comm_nb','DF', "","col_12 alignFormSpec");
                    $form->setBloc('tab_tax_su_parc_statio_expl_comm','DF');
                $form->setBloc('tax_su_non_habit_abr_jard_pig_colom','F');
                //
                $form->setBloc('tab_tax_am','DF',_("tab_tax_am"),"col_12");

                $form->setBloc('tax_am_statio_ext_cr','D', _("Autres elements crees soumis à la taxe d’amenagement :"),"col_12");
                $form->setBloc('tax_pann_volt_sup_cr','F');

                $form->setBloc('tax_surf_loc_arch','D', _("Redevance d’archeologie preventive"),"col_12 alignFormSpec");
                    $form->setBloc('tax_surf_loc_arch','D', _("Veuillez preciser la profondeur du(des) terrassement(s) necessaire(s) a la realisation de votre projet (en mètre)"),"");
                    $form->setBloc('tax_eol_haut_nb_arch','F');
                $form->setBloc('tax_eol_haut_nb_arch','F');

                $form->setBloc('tax_trx_presc_ppr','D', _("Cas particuliers"),"col_12 alignFormSpec");
                $form->setBloc('tax_monu_hist','F');

                $form->setBloc('vsd_surf_planch_smd','D', _("Versement pour sous-densite (VSD)"),"col_12");
                    $form->setBloc('vsd_surf_planch_smd','D', "","alignFormSpec");
                    $form->setBloc('vsd_const_sxist_non_dem_surf','F');
                    
                    $form->setBloc('vsd_rescr_fisc','DF',"", "alignFormSpec-type-date");
                $form->setBloc('vsd_rescr_fisc','F');
                
                $form->setBloc('pld_val_terr','D', _("Plafond legal de densite (PLD)"),"col_12 alignFormSpec");
                $form->setBloc('pld_const_exist_dem_surf','F');

                $form->setBloc('tax_desc','DF', _("Autres renseignements"),"col_12 alignFormSpec");

                $form->setBloc('exo_ta_1','D',"","col_12");
                    $form->setFieldset('exo_ta_1','D',_("Exonérations"), "startClosed alignFormSpec");

                    $form->setBloc('exo_ta_1','D',"","col_12");
                        $form->setFieldset('exo_ta_1','D',_("Taxe d'aménagement"), "collapsible alignFormSpec");

                        $form->setBloc('exo_ta_1','DF', _("Exonérations de plein droit"),"col_12 group");
                        $form->setBloc('exo_ta_2','DF', "","group");
                        $form->setBloc('exo_ta_3','DF', "","group");
                        $form->setBloc('exo_ta_4','DF', "","group");
                        $form->setBloc('exo_ta_5','DF', "","group");
                        $form->setBloc('exo_ta_6','DF', "","group");
                        $form->setBloc('exo_ta_7','DF', "","group");
                        $form->setBloc('exo_ta_8','DF', "","group");
                        $form->setBloc('exo_ta_9','DF', "","group");

                        $form->setBloc('exo_facul_1','DF', _("Exonérations facultatives"),"col_12 group");
                        $form->setBloc('exo_facul_2','DF', "","group");
                        $form->setBloc('exo_facul_3','DF', "","group");
                        $form->setBloc('exo_facul_4','DF', "","group");
                        $form->setBloc('exo_facul_5','DF', "","group");
                        $form->setBloc('exo_facul_6','DF', "","group");
                        $form->setBloc('exo_facul_7','DF', "","group");
                        $form->setBloc('exo_facul_8','DF', "","group");
                        $form->setBloc('exo_facul_9','DF', "","group");

                        $form->setBloc('mtn_exo_ta_part_commu','DF', _("Montants des exonérations"),"col_12 group");
                        $form->setBloc('mtn_exo_ta_part_depart','DF', "","group");
                        $form->setBloc('mtn_exo_ta_part_reg','DF', "","group");

                        $form->setFieldset('mtn_exo_ta_part_reg','F','');
                    $form->setBloc('mtn_exo_ta_part_reg','F');

                    $form->setBloc('exo_rap_1','D',"","col_12");
                        $form->setFieldset('exo_rap_1','D',_("Redevance d'archéologie préventive"), "collapsible alignFormSpec");

                        $form->setBloc('exo_rap_1','DF', _("Exonérations"),"col_12 group");
                        $form->setBloc('exo_rap_2','DF', "","group");
                        $form->setBloc('exo_rap_3','DF', "","group");
                        $form->setBloc('exo_rap_4','DF', "","group");
                        $form->setBloc('exo_rap_5','DF', "","group");
                        $form->setBloc('exo_rap_6','DF', "","group");
                        $form->setBloc('exo_rap_7','DF', "","group");
                        $form->setBloc('exo_rap_8','DF', "","group");

                        $form->setBloc('mtn_exo_rap','DF', _("Montant des exonérations"),"col_12 group");

                        $form->setFieldset('mtn_exo_rap','F','');
                    $form->setBloc('mtn_exo_rap','F');

                $form->setFieldset('mtn_exo_rap','F','');
            $form->setBloc('mtn_exo_rap','F');

            $form->setFieldset('mtn_exo_rap','F','');
        $form->setBloc('mtn_exo_rap','F');

        // Début contentieux
        $form->setBloc('ctx_objet_recours','D',"","col_12");
        $form->setFieldset('ctx_objet_recours','D',_("Contentieux"), "startClosed alignFormSpec");

                //
                $form->setBloc('ctx_objet_recours','DF', "","group");
                $form->setBloc('ctx_moyen_souleve','DF', "","group");
                $form->setBloc('ctx_moyen_retenu_juge','DF', "","group");
                $form->setBloc('ctx_reference_sagace','DF', "","group");
                $form->setBloc('ctx_nature_travaux_infra_om_html','DF', "","group");
                $form->setBloc('ctx_synthese_nti','DF', "","group");
                $form->setBloc('ctx_article_non_resp_om_html','DF', "","group");
                $form->setBloc('ctx_synthese_anr','DF', "","group");
                $form->setBloc('ctx_reference_parquet','DF', "","group");
                $form->setBloc('ctx_element_taxation','DF', "","group");
                $form->setBloc('ctx_infraction','DF', "","group");
                $form->setBloc('ctx_regularisable','DF', "","group");
                $form->setBloc('ctx_reference_courrier','DF', "","group");
                $form->setBloc('ctx_date_audience','DF', "","group");
                $form->setBloc('ctx_date_ajournement','DF', "","group");

        $form->setFieldset('ctx_date_ajournement','F','');
        $form->setBloc('ctx_date_ajournement','F');
        // Fin contentieux

        // Début droit de préemption commercial
        $form->setBloc('dpc_type','D',"","col_12");
        $form->setFieldset('dpc_type','D',_("Droit de préemption commercial"), "startClosed alignFormSpec");

            // Description du bien
            $form->setBloc('dpc_type','D',"","col_12");
            $form->setFieldset('dpc_type','D',_("Description du bien"), "startClosed alignFormSpec");
                //
                $form->setBloc('dpc_type','DF', "","group");
                //
                $form->setBloc('dpc_desc_actv_ex','D', _("Description du fonds artisanal, du fonds de commerce ou du bail commercial"),"col_12");
                $form->setBloc('dpc_desc_aut_prec','F');
                //
                $form->setBloc('dpc_desig_comm_arti','D', _("Désignation du fonds artisanal, du fonds de commerce, ou du bail commercial ou du terrain"),"col_12");
                $form->setBloc('dpc_desig_loc_ann_prec','F');
                //
                $form->setBloc('dpc_bail_comm_date','D', _("S'il s'agit d'un bail commercial"),"col_12 group");
                $form->setBloc('dpc_bail_comm_loyer','F');
                //
                $form->setBloc('dpc_actv_acqu','DF',  _("Activité de l'acquéreur pressenti"),"group");
                //
                $form->setBloc('dpc_nb_sala_di','D', _("Nombre de salariés et nature de leur contrat de travail"),"col_12 group");
                $form->setBloc('dpc_nb_sala_td','F');
                //
                $form->setBloc('dpc_nb_sala_tc','D', "","col_12 group");
                $form->setBloc('dpc_nb_sala_tp','F');

            $form->setFieldset('dpc_nb_sala_tp','F','');
            $form->setBloc('dpc_nb_sala_tp','F');

            // Modalité de la cession
            $form->setBloc('dpc_moda_cess_vente_am','D',"","col_12");
            $form->setFieldset('dpc_moda_cess_vente_am','D',_("Modalité de la cession"), "startClosed alignFormSpec");
                //
                $form->setBloc('dpc_moda_cess_vente_am','D', "","col_12 group");
                $form->setBloc('dpc_moda_cess_prix','F');
                //
                $form->setBloc('dpc_moda_cess_adj_date','D', "","col_12 group");
                $form->setBloc('dpc_moda_cess_adj_prec','F');
                //
                $form->setBloc('dpc_moda_cess_paie_comp','DF', _("Modalités de paiement"),"col_12");

            $form->setFieldset('dpc_moda_cess_paie_aut_prec','F','');
            $form->setBloc('dpc_moda_cess_paie_aut_prec','F');

            // Le(s) soussigné(s) déclare(nt) que le bailleur
            $form->setBloc('dpc_ss_signe_demande_acqu','D',"","col_12");
            $form->setFieldset('dpc_ss_signe_demande_acqu','D',_("Le(s) soussigné(s) déclare(nt) que le bailleur"), "startClosed alignFormSpec");

            $form->setFieldset('dpc_ss_signe_recher_trouv_acqu','F','');
            $form->setBloc('dpc_ss_signe_recher_trouv_acqu','F');

            // Notification des décisions du titulaire du droit de préemption
            $form->setBloc('dpc_notif_adr_prop','D',"","col_12");
            $form->setFieldset('dpc_notif_adr_prop','D',_("Notification des décisions du titulaire du droit de préemption"), "startClosed alignFormSpec");

            $form->setFieldset('dpc_notif_adr_manda','F','');
            $form->setBloc('dpc_notif_adr_manda','F');

            //
            $form->setBloc('dpc_obs','D',"","col_12");
            $form->setFieldset('dpc_obs','D',_("Observations éventuelles"), "startClosed alignFormSpec");

            $form->setFieldset('dpc_obs','F','');
            $form->setBloc('dpc_obs','F');

        $form->setFieldset('dpc_obs','F','');
        $form->setBloc('dpc_obs','F');
        // Fin droit de préemption commercial
    }

    /**
     * Surcharge de la méthode setOnChange
     */
    function setOnchange(&$form,$maj){
        parent::setOnchange($form,$maj);

        $form->setOnchange("co_tot_ind_nb","sommeChampsCerfa('co_tot_log_nb',['co_tot_ind_nb','co_tot_coll_nb']);");
        $form->setOnchange("co_tot_coll_nb","sommeChampsCerfa('co_tot_log_nb',['co_tot_ind_nb','co_tot_coll_nb']);");

        $form->setOnchange("doc_nb_log_indiv","sommeChampsCerfa('doc_nb_log',['doc_nb_log_indiv','doc_nb_log_coll']);");
        $form->setOnchange("doc_nb_log_coll","sommeChampsCerfa('doc_nb_log',['doc_nb_log_indiv','doc_nb_log_coll']);");
        
        $form->setOnchange("su_avt_shon1","calculSurfaceTotal(1);");
        $form->setOnchange("su_avt_shon2","calculSurfaceTotal(1);");
        $form->setOnchange("su_avt_shon3","calculSurfaceTotal(1);");
        $form->setOnchange("su_avt_shon4","calculSurfaceTotal(1);");
        $form->setOnchange("su_avt_shon5","calculSurfaceTotal(1);");
        $form->setOnchange("su_avt_shon6","calculSurfaceTotal(1);");
        $form->setOnchange("su_avt_shon7","calculSurfaceTotal(1);");
        $form->setOnchange("su_avt_shon8","calculSurfaceTotal(1);");
        $form->setOnchange("su_avt_shon9","calculSurfaceTotal(1);");
        $form->setOnchange("su_cstr_shon1","calculSurfaceTotal(1);");
        $form->setOnchange("su_cstr_shon2","calculSurfaceTotal(1);");
        $form->setOnchange("su_cstr_shon3","calculSurfaceTotal(1);");
        $form->setOnchange("su_cstr_shon4","calculSurfaceTotal(1);");
        $form->setOnchange("su_cstr_shon5","calculSurfaceTotal(1);");
        $form->setOnchange("su_cstr_shon6","calculSurfaceTotal(1);");
        $form->setOnchange("su_cstr_shon7","calculSurfaceTotal(1);");
        $form->setOnchange("su_cstr_shon8","calculSurfaceTotal(1);");
        $form->setOnchange("su_cstr_shon9","calculSurfaceTotal(1);");
        $form->setOnchange("su_chge_shon1","calculSurfaceTotal(1);");
        $form->setOnchange("su_chge_shon2","calculSurfaceTotal(1);");
        $form->setOnchange("su_chge_shon3","calculSurfaceTotal(1);");
        $form->setOnchange("su_chge_shon4","calculSurfaceTotal(1);");
        $form->setOnchange("su_chge_shon5","calculSurfaceTotal(1);");
        $form->setOnchange("su_chge_shon6","calculSurfaceTotal(1);");
        $form->setOnchange("su_chge_shon7","calculSurfaceTotal(1);");
        $form->setOnchange("su_chge_shon8","calculSurfaceTotal(1);");
        $form->setOnchange("su_chge_shon9","calculSurfaceTotal(1);");
        $form->setOnchange("su_demo_shon1","calculSurfaceTotal(1);");
        $form->setOnchange("su_demo_shon2","calculSurfaceTotal(1);");
        $form->setOnchange("su_demo_shon3","calculSurfaceTotal(1);");
        $form->setOnchange("su_demo_shon4","calculSurfaceTotal(1);");
        $form->setOnchange("su_demo_shon5","calculSurfaceTotal(1);");
        $form->setOnchange("su_demo_shon6","calculSurfaceTotal(1);");
        $form->setOnchange("su_demo_shon7","calculSurfaceTotal(1);");
        $form->setOnchange("su_demo_shon8","calculSurfaceTotal(1);");
        $form->setOnchange("su_demo_shon9","calculSurfaceTotal(1);");
        $form->setOnchange("su_sup_shon1","calculSurfaceTotal(1);");
        $form->setOnchange("su_sup_shon2","calculSurfaceTotal(1);");
        $form->setOnchange("su_sup_shon3","calculSurfaceTotal(1);");
        $form->setOnchange("su_sup_shon4","calculSurfaceTotal(1);");
        $form->setOnchange("su_sup_shon5","calculSurfaceTotal(1);");
        $form->setOnchange("su_sup_shon6","calculSurfaceTotal(1);");
        $form->setOnchange("su_sup_shon7","calculSurfaceTotal(1);");
        $form->setOnchange("su_sup_shon8","calculSurfaceTotal(1);");
        $form->setOnchange("su_sup_shon9","calculSurfaceTotal(1);");
        $form->setOnchange("su_tot_shon1","calculSurfaceTotal(1);");
        $form->setOnchange("su_tot_shon2","calculSurfaceTotal(1);");
        $form->setOnchange("su_tot_shon3","calculSurfaceTotal(1);");
        $form->setOnchange("su_tot_shon4","calculSurfaceTotal(1);");
        $form->setOnchange("su_tot_shon5","calculSurfaceTotal(1);");
        $form->setOnchange("su_tot_shon6","calculSurfaceTotal(1);");
        $form->setOnchange("su_tot_shon7","calculSurfaceTotal(1);");
        $form->setOnchange("su_tot_shon8","calculSurfaceTotal(1);");
        $form->setOnchange("su_tot_shon9","calculSurfaceTotal(1);");

        // Tableau des sous-destinations
        $form->setOnChange("su2_avt_shon1", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon2", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon3", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon4", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon5", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon6", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon7", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon8", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon9", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon10", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon11", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon12", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon13", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon14", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon15", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon16", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon17", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon18", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon19", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_avt_shon20", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon1", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon2", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon3", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon4", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon5", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon6", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon7", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon8", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon9", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon10", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon11", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon12", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon13", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon14", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon15", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon16", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon17", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon18", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon19", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_cstr_shon20", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon1", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon2", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon3", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon4", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon5", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon6", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon7", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon8", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon9", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon10", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon11", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon12", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon13", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon14", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon15", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon16", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon17", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon18", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon19", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_chge_shon20", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon1", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon2", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon3", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon4", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon5", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon6", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon7", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon8", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon9", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon10", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon11", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon12", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon13", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon14", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon15", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon16", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon17", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon18", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon19", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_demo_shon20", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon1", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon2", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon3", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon4", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon5", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon6", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon7", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon8", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon9", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon10", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon11", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon12", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon13", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon14", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon15", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon16", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon17", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon18", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon19", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_sup_shon20", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon1", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon2", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon3", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon4", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon5", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon6", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon7", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon8", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon9", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon10", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon11", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon12", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon13", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon14", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon15", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon16", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon17", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon18", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon19", "calculSurfaceTotal(2);");
        $form->setOnChange("su2_tot_shon20", "calculSurfaceTotal(2);");
    }

    
    /**
     * Surcharge du bouton retour afin de retourner sur le dossier d'instruction selon de cas
     */
    function retoursousformulaire($idxformulaire = NULL, $retourformulaire = NULL, $val = NULL,
                                  $objsf = NULL, $premiersf = NULL, $tricolsf = NULL, $validation = NULL,
                                  $idx = NULL, $maj = NULL, $retour = NULL) {

        $visualisation = $this->getParameter('visualisation');
        
        if ( $visualisation == "" ){
            
            // Ajout et consultation, retour dossier
            if ( ( $maj == 0 && $validation == 0 ) || 
                ( $maj == 3 && $validation == 0 ) ||
                ( $maj == 0 && $validation == 1 )  && $retourformulaire == "dossier_instruction" ){
             
                echo "\n<a class=\"retour\" ";
                    echo "href=\"#\" ";
                    echo "onclick=\"redirectPortletAction(1,'main');\" ";
                    echo ">";
                    echo _("Retour");
                echo "</a>\n";
            }
            //Sinon affiche un retour normal
            else{
                
                parent::retoursousformulaire($idxformulaire, $retourformulaire, $val,
                                      $objsf, $premiersf, $tricolsf, $validation,
                                      $idx, $maj, $retour);
            }
        }
    }


    /**
     * Retourne le nom et le prénom de l'architecte qui a l'identifiant $id
     * @param integer $id
     * @param object $db
     * @return string
     */
    function getPrenomNomArchitecte($id){
        
        $coordonneesArchitecte = "";
        if ( $id != '' && is_numeric($id) ){
            
            $sql = "SELECT 
                CONCAT(architecte.prenom, ' ', architecte.nom)
            FROM
                ".DB_PREFIXE."architecte
            WHERE 
                architecte.architecte = ".$id;
            $coordonneesArchitecte = $this->db->getOne($sql);
            $this->f->addToLog("setSelect() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            if ( database::isError($coordonneesArchitecte)){
                $this->f->addToError("", $coordonneesArchitecte, $coordonneesArchitecte);
                return false;
            }
        }
        return $coordonneesArchitecte;
    }

    /**
     * Récupère l'instance de la classe dossier.
     *
     * @param string $dossier Identifiant
     *
     * @return object
     */
    function get_inst_dossier($dossier = null) {
        //
        if (is_null($this->inst_dossier)) {
            //
            if (is_null($dossier)) {
                $dossier = $this->getVal("dossier_instruction");
            }
            //
            require_once "../obj/dossier.class.php";
            $this->inst_dossier = new dossier($dossier);
        }
        //
        return $this->inst_dossier;
    }

    /**
     * Récupère l'instance de la classe dossier_autorisation.
     *
     * @param string $dossier_autorisation Identifiant
     *
     * @return object
     */
    function get_inst_dossier_autorisation($dossier_autorisation = null) {
        //
        if (is_null($this->inst_dossier_autorisation)) {
            //
            if (is_null($dossier_autorisation)) {
                $dossier_autorisation = $this->getVal("dossier_autorisation");
            }
            //
            require_once "../obj/dossier_autorisation.class.php";
            $this->inst_dossier_autorisation = new dossier_autorisation($dossier_autorisation);
        }
        //
        return $this->inst_dossier_autorisation;
    }

    /**
     * Récupère l'instance de la classe lot.
     *
     * @param string $lot Identifiant
     *
     * @return object
     */
    function get_inst_lot($lot = null) {
        //
        if (is_null($this->inst_lot)) {
            //
            if (is_null($lot)) {
                $lot = $this->getVal("lot");
            }
            //
            require_once "../obj/lot.class.php";
            $this->inst_lot = new lot($lot);
        }
        //
        return $this->inst_lot;
    }

    /**
     * Permet d’effectuer des actions après l’insertion des données dans la base.
     *
     * @param integer $id    Identifiant de l'objet.
     * @param object  &$db   Instance de la bdd.
     * @param array   $val   Liste des valeurs.
     * @param mixed   $DEBUG Debug.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        parent::triggerajouterapres($id, $db, $val, $DEBUG);

        // Liaison NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Si le champ possède une valeur
            if (isset($val[$liaison_nan["field"]]) === true) {
                // Ajout des liaisons table Nan
                $nb_liens = $this->ajouter_liaisons_table_nan(
                    $liaison_nan["table_l"], 
                    $liaison_nan["table_f"], 
                    $liaison_nan["field"],
                    $val[$liaison_nan["field"]]
                );
                // Message de confirmation
                if ($nb_liens > 0) {
                    if ($nb_liens == 1 ){
                        $this->addToMessage(sprintf(_("Création d'une nouvelle liaison réalisee avec succès.")));
                    } else {
                        $this->addToMessage(sprintf(_("Création de %s nouvelles liaisons réalisée avec succès."), $nb_liens));
                    }
                }
            }
        }

        //
        return true;
    }


    /**
     * Permet d’effectuer des actions après la modification des données dans la
     * base.
     *
     * @param integer $id    Identifiant de l'objet.
     * @param object  &$db   Instance de la bdd.
     * @param array   $val   Liste des valeurs.
     * @param mixed   $DEBUG Debug.
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$db = null, $val = array(), $DEBUG = null) {

        /**
         * Gestion des taxes.
         */
        // Si les données techniques concernent un dossier d'instruction
        if ($val['dossier_instruction'] !== null && $val['dossier_instruction'] !== '') {

            // Instance du dossier d'instruction
            $inst_dossier = $this->get_inst_dossier();
            // Instance du paramétrage des taxes
            $inst_taxe_amenagement = $this->get_inst_taxe_amenagement_by_om_collectivite($inst_dossier->getVal('om_collectivite'));
            // Instance du cerfa lié au dossier
            $inst_cerfa = $this->get_inst_cerfa_by_dossier($inst_dossier->getVal($inst_dossier->clePrimaire));

            // Si l'option de simulation est activée pour la collectivité du
            // dossier, la collectivité à un paramétrage pour les taxes et que
            // le cerfa du dossier à les champs requis
            if ($this->f->is_option_simulation_taxes_enabled($inst_dossier->getVal('om_collectivite')) === true
                && $inst_taxe_amenagement !== null
                && $inst_cerfa->can_simulate_taxe_amenagement() === true) {

                // Récupération des champs nécessaires à la simulation
                $list_fields = $inst_taxe_amenagement->get_list_fields_simulation();

                // Valeurs pour le calcul
                $valF = array();
                // Pour chaque champ
                foreach ($list_fields as $field) {
                    // Si un seul des champs requis a une valeur
                    $getVal = $this->valF[$field];
                    if ($getVal !== '' && $getVal !== null) {
                        //
                        $valF = $val;
                    }
                }

                // Met à jour les montants du dossier
                $update_dossier_tax_mtn = $inst_dossier->update_dossier_tax_mtn($inst_dossier->getVal('tax_secteur'), $valF);
                if ($update_dossier_tax_mtn === false) {
                    //
                    $this->addToMessage(_("La mise a jour des montants de la simulation de la taxe d'amenagement a echouee."));
                    //
                    return false;
                }
            }
        }

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"],
                $val[$liaison_nan["field"]]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                $this->addToMessage(_("Mise a jour des liaisons realisee avec succes."));
            }
        }

        return true;
    }


    /**
     * Permet d’effectuer des actions avant la modification des données dans la
     * base.
     *
     * @param integer $id Identifiant de l'objet.
     *
     * @return boolean
     */
    function triggersupprimer($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        parent::triggersupprimer($id);

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
        }

        //
        return true;
    }


    /**
     * Récupère toutes les valeurs de l'enregistrement.
     *
     * @return array
     */
    function get_form_val() {

        // Initialisation du tableau des résultats
        $result = array();

        // Pour chaque champs
        foreach ($this->champs as $champ) {
            // Récupère sa valeur
            $result[$champ] = $this->getVal($champ);
        }
        
        // Retourne le résultat
        return $result;
    }


    /**
     * Retourne les données techniques applicables au dossier courant, càd les données
     * techniques liées au dossier avec seulement les champs du CERFA associé au type de
     * dossier.
     *
     * @return array $donnees_techniques_applicables  Tableau associatif contenant
     *                                                seulement les données techniques
     *                                                applicables au dossier.
     */
    public function get_donnees_techniques_applicables() {

        // Récupération du tableau associatif contenant données techniques du dossier
        $donnees_techniques_dossier = $this->get_form_val();
        // Récupération du tableau associatif contenant les champs du CERFA lié au dossier
        $inst_cerfa = $this->getCerfa();
        $cerfa_dossier = $inst_cerfa->get_form_val();

        if (file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        }

        // Tableau associatif contenant la liste des champs de DT applicables au dossier
        $donnees_techniques_applicables = array();
        foreach ($cerfa_dossier as $key => $value) {
            // Si le champ du CERFA est préfixé par tab_ il s'agit d'un tableau qui
            // contient plusieurs champs, on récupère alors tous les champs du tableau.
            if (preg_match("/tab_/", $key)) {
                // Contient les champs de données techniques qui composent le tableau tab_
                $tableau_donnees_techniques = ${$key};
                // Pour chaque champ qui compose le tableau de données techniques
                if (isset($tableau_donnees_techniques[$inst_cerfa->getVal($key)]['values']) === true) {
                    foreach ($tableau_donnees_techniques[$inst_cerfa->getVal($key)]['values'] as $key) {
                        $donnees_techniques_applicables[$key] = $this->getVal($key);
                    }
                }
            }
            // Si le champ est activé dans le CERFA correspond a une donnée technique
            if ($value === 't' && array_key_exists($key, $donnees_techniques_dossier)) {
                $donnees_techniques_applicables[$key] = $this->getVal($key);
            }
        }
        return $donnees_techniques_applicables;

    }


    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le 
     * générateur en devant surcharger la méthode cleSecondaire afin de supprimer
     * les éléments liés dans les tables NaN.
     *
     * @param [type] $dnu1      Instance BDD - À ne pas utiliser
     * @param [type] $table     Table
     * @param [type] $field     Champ
     * @param [type] $id        Identifiant
     * @param [type] $dnu2      Marqueur de débogage - À ne pas utiliser
     * @param string $selection Condition de la requête
     *
     * @return [type] [description]
     */
    function rechercheTable(&$dnu1 = null, $table, $field, $id, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array_keys($this->liaisons_nan))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table." car liaison nan.", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }


    /**
     * Récupère l'identifiant des données techniques avec le numéro de dossier.
     *
     * @param string $dossier Identifiant du dossier.
     *
     * @return integer
     */
    public function get_id_by_dossier($dossier) {
        //
        $sql = 'SELECT donnees_techniques
                FROM '.DB_PREFIXE.'donnees_techniques
                WHERE dossier_instruction = \''.$dossier.'\'';
        // Exécution de la requête
        $id = $this->db->getOne($sql);
        // Log
        $this->addToLog(__METHOD__."() : db->getOne(\"".$sql."\");", VERBOSE_MODE);
        // Erreur BDD
        $this->f->isDatabaseError($id);

        //
        return $dossier;
    }

   /*
     * CONDITION - can_user_access_dossier_contexte_modification
     *
     * Vérifie que l'utilisateur a bien accès au dossier lié aux données techniques
     * instanciées.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_modification() {
        //
        $inst_dossier = $this->get_inst_dossier();
        //
        if ($inst_dossier->getVal('dossier') !== '') {
            return $inst_dossier->can_user_access_dossier();
        }
        return false;
    }

   /*
     * CONDITION - can_user_access_dossier_contexte_dossier_autorisation_modification
     *
     * Vérifie que l'utilisateur a bien accès au dossier d'autorisation lié aux données
     * techniques instanciées.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_dossier_autorisation_modification() {
        //
        $inst_dossier_autorisation = $this->get_inst_dossier_autorisation();
        //
        if ($inst_dossier_autorisation->getVal('dossier_autorisation') !== '') {
            return $inst_dossier_autorisation->can_user_access_dossier();
        }
        return false;
    }

   /*
     * CONDITION - can_user_access_dossier_contexte_lot_modification
     *
     * Vérifie que l'utilisateur a bien accès au dossier lié aux données techniques
     * instanciées.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_lot_modification() {
        //
        $inst_lot = $this->get_inst_lot();
        $inst_dossier = $this->get_inst_dossier($inst_lot->getVal('dossier'));
        //
        if ($inst_dossier->getVal('dossier') !== '') {
            return $inst_dossier->can_user_access_dossier();
        }
        return false;
    }


    /**
     * Récupère l'instance de la classe taxe_amenagement.
     *
     * @param integer $om_collectivite La collectivité
     *
     * @return object
     */
    protected function get_inst_taxe_amenagement_by_om_collectivite($om_collectivite) {
        //
        if ($this->inst_taxe_amenagement === null) {
            //
            $taxe_amenagement = $this->get_taxe_amenagement_by_om_collectivite($om_collectivite);

            // Si aucun paramétrage de taxe trouvé et que la collectivité
            // est mono
            if ($taxe_amenagement === null
                && $this->f->isCollectiviteMono($om_collectivite) === true) {
                // Récupère la collectivité multi
                $om_collectivite_multi = $this->f->get_idx_collectivite_multi();
                //
                $taxe_amenagement = $this->get_taxe_amenagement_by_om_collectivite($om_collectivite_multi);
            }

            //
            if ($taxe_amenagement === null) {
                //
                return null;
            }

            //
            require_once "../obj/taxe_amenagement.class.php";
            $this->inst_taxe_amenagement = new taxe_amenagement($taxe_amenagement);
        }
        //
        return $this->inst_taxe_amenagement;
    }


    /**
     * Récupère l'identifiant de la taxe d'aménagement par rapport à la collectivité.
     *
     * @param integer $om_collectivite La collectivité
     *
     * @return integer
     */
    protected function get_taxe_amenagement_by_om_collectivite($om_collectivite) {
        //
        $taxe_amenagement = null;

        // Si la collectivité n'est pas renseigné
        if ($om_collectivite !== '' && $om_collectivite !== null) {

            // SQL
            $sql = "SELECT taxe_amenagement
                    FROM ".DB_PREFIXE."taxe_amenagement
                    WHERE om_collectivite = ".intval($om_collectivite);
            $taxe_amenagement = $this->f->db->getOne($sql);
            $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($taxe_amenagement);
        }

        //
        return $taxe_amenagement;
    }


    /**
     * Récupère l'instance du cerfa par le dossier d'instruction.
     *
     * @param integer $dossier Identifiant du dossier d'instruction.
     *
     * @return object
     */
    protected function get_inst_cerfa_by_dossier($dossier = null) {
        //
        if ($this->inst_cerfa === null) {
            //
            $inst_dossier = $this->get_inst_dossier($dossier);
            //
            $inst_da = $this->get_inst_dossier_autorisation($inst_dossier->getVal('dossier_autorisation'));
            //
            $inst_datd = $this->get_inst_common("dossier_autorisation_type_detaille", $inst_da->getVal('dossier_autorisation_type_detaille'));
            //
            $cerfa = $inst_datd->getVal('cerfa');
            //
            if ($cerfa !== '' && $cerfa !== null) {
                //
                require_once "../obj/cerfa.class.php";
                $this->inst_cerfa = new cerfa($cerfa);
            }
        }

        //
        return $this->inst_cerfa;
    }


}// fin classe
?>
