<?php
/**
 * Gestion des requerants.
 * 
 * @package openads
 * @version SVN : $Id: requerant.class.php 6565 2017-04-21 16:14:15Z softime $
 */
require_once("../obj/demandeur.class.php");


/**
 * Les requerants héritent des demandeurs.
 */
class requerant extends demandeur {

    /**
     * Constructeur.
     *
     * @param string   $id    Identifiant du requerant.
     * @param database $db    Instance de base de données.
     * @param boolean  $debug Flag de debug.
     */
    function __construct($id, $db, $debug) {
        $this->constructeur($id, $db, $debug);
    }

    /**
     * Cache les champs de notification, fréquent et type_demandeur.
     *
     * @param formulaire $form Instance de la classe om_formulaire.
     * @param integer    $maj  Identifiant de l'action.
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);

        $form->setType('type_demandeur', 'hidden');
        $form->setType('notification', 'hidden');
        $form->setType('frequent', 'hidden');
    }

    /**
     * Met le champ type_demandeur à requerant par défaut.
     * 
     * @param formulaire $form       Instance de la classe om_formulaire.
     * @param integer    $maj        Identifiant de l'action.
     * @param integer    $validation Nombre de validation du formulaire.
     * @param database   $db         Instance de la classe om_database.
     */
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        if ($maj == 0 ) {
             $form->setVal("type_demandeur", "requerant");
        }
    }
    
    /*
     * Met le champ type_demandeur à requerant par défaut
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValSousFormulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);
        $form->setVal("type_demandeur", "requerant");
        if ($maj == 0 ) {
            // Récupération des infos du demandeur passé en paramètre
            if ($this->getParameter('idx_demandeur') != "") {
                include '../sql/pgsql/delegataire.form.inc.php';
                $sql = "SELECT ".implode(", ", $champs)."
                        FROM ".DB_PREFIXE."demandeur ".
                        "WHERE demandeur=".$this->getParameter('idx_demandeur');
                $res = $this->db->query($sql);
                $this->f->addToLog(
                    "setValSousFormulaire() : db->query(\"".$sql."\")",
                    VERBOSE_MODE
                );
                if ( database::isError($res)) {
                    die();
                }
                $row = & $res->fetchRow(DB_FETCHMODE_ASSOC);
                foreach ($row as $key => $value) {
                    $form->setVal($key, $value);
                }
                $form->setVal("frequent", "f");
            }
        }
    }
}
