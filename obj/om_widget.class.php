<?php
/**
 * OM_WIDGET - Surcharge du core
 *
 * L'objectif de la surcharge est de coder les vues spécifiques des widgets
 * de type 'file'.
 *
 * @package openads
 * @version SVN : $Id: om_widget.class.php 5673 2015-12-21 19:35:24Z nmeucci $
 */

require_once "../core/obj/om_widget.class.php";

class om_widget extends om_widget_core {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     *
     */
    var $template_help = '<div class="widget-help"><span class="info-16" title="%s"></span></div>';

    /**
     *
     */
    var $template_footer = '
    <div class="widget-footer">
        <a href="%s">
            %s
        </a>
    </div>
    ';

    /**
     * Cette méthode retourne un arbre html représentant un raccourci.
     * 
     * Un raccourci est composé d'un lien, d'une image et d'une description.
     * Voir les widgets :
     *  - widget_nouvelle_demande_autre_dossier
     *  - widget_nouvelle_demande_dossier_encours
     *  - widget_nouvelle_demande_nouveau_dossier
     */
    function get_display_widget_shortlink($config) {
        return sprintf (
            '<a href="%s"><span><img src="%s" align="left" alt="%s" />%s</span></a>',
            $config["a_href"],
            $config["img_src"],
            $config["img_alt"],
            $config["a_content"]
        );
    }

    /**
     * WIDGET DASHBOARD - widget_nouvelle_demande_autre_dossier.
     */
    function view_widget_nouvelle_demande_autre_dossier($content = null) {
        echo $this->get_display_widget_shortlink(array(
            "a_href" => "../scr/tab.php?obj=demande_autre_dossier",
            "img_src" => "../app/img/dossier-existant.png",
            "img_alt" => _("Autres dossiers"),
            "a_content" => _("Cliquer ici pour saisir une nouvelle demande concernant un dossier en cours ou une autorisation existante"),
        ));
    }

    /**
     * WIDGET DASHBOARD - widget_nouvelle_demande_dossier_encours.
     */
    function view_widget_nouvelle_demande_dossier_encours($content = null) {
        echo $this->get_display_widget_shortlink(array(
            "a_href" => "../scr/tab.php?obj=demande_dossier_encours",
            "img_src" => "../app/img/dossier-existant.png",
            "img_alt" => _("Dossier en cours"),
            "a_content" => _("Cliquer ici pour saisir une nouvelle demande concernant un dossier en cours"),
        ));
    }

    /**
     * WIDGET DASHBOARD - widget_nouvelle_demande_nouveau_dossier.
     */
    function view_widget_nouvelle_demande_nouveau_dossier($content = null) {
        $params = array("contexte");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        $arguments_default = array(
            "contexte" => "standard"
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "contexte"
                    && in_array($elem, array("standard", "contentieux"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        $contexte = $arguments["contexte"];
        $widget_config = array(
            "a_href" => "../scr/form.php?obj=demande_nouveau_dossier&amp;action=0&amp;advs_id=&amp;tricol=&amp;valide=&amp;retour=tab&amp;new=",
            "img_src" => "../app/img/dossier-nouveau.png",
            "img_alt" => _("Nouveau dossier"),
            "a_content" => _("Cliquer ici pour saisir une nouvelle demande concernant le depot d'un nouveau dossier"),
        );
        if ($contexte == "contentieux") {
            $widget_config["a_href"] = "../scr/form.php?obj=demande_nouveau_dossier_contentieux&amp;action=0&amp;advs_id=&amp;tricol=&amp;valide=&amp;retour=tab&amp;new=";
        }
        echo $this->get_display_widget_shortlink($widget_config);
    }

    /**
     * WIDGET DASHBOARD - widget_recherche_dossier.
     *
     * Quatre cas d'utilisation :
     *  - 1 - La valeur recherchée correspond exactement au code d'un DI,
     *    alors on accède directement à la fiche de visualisation du DI.
     *  - 2 - La valeur recherchée renvoi plusieurs DI, alors on accède au
     *    listing des dossiers d'instruction avec le formulaire de 
     *    recherche avancée pré-rempli avec la valeur saisie.
     *  - 3 - La valeur recherchée renvoi aucun enregistrement, alors on 
     *    affiche un message en ce sens directement dans le widget.
     *  - 4 - Aucune valeur n'est saisie, alors on affiche un message en ce 
     *    sens directement dans le widget.
     *
     * @return void
     */
    function view_widget_recherche_dossier($content = null) {

        /**
         * Traitement de la validation du formulaire
         */
        //
        if ($this->f->get_submitted_post_value("validation") != null
            && $this->f->get_submitted_post_value("dossier") === '') {

            //
            // AUCUNE VALEUR SAISIE
            //

            // Cas d'utilisation n°4
            // Affiche un message d'erreur
            $erreur = _("Veuillez saisir un No de dossier.");
        }
        //
        if ($this->f->get_submitted_post_value("validation") != null
            && $this->f->get_submitted_post_value("dossier") !== null
            && $this->f->get_submitted_post_value("dossier") !== '') {
            //
            $list_dossiers = $this->execute_recherche_dossier();

            $total_dossiers = count($list_dossiers);
            // Si on obtient un seul résultat
            if ($total_dossiers == 1) {
                // On reformate le dossier
                $dossier = strtoupper(str_replace(' ', '', $list_dossiers[0]));
                // On redirige vers le listing des DI
                echo '
                <script type="text/javascript" >
                    widget_recherche_dossier(\''.$dossier.'\', 1, \'dossier_instruction\');
                </script>
                ';
                // On arrête l'exécution du script car l'utilisateur a été 
                // redirigé vers un autre script
                exit();
            }
            // Si on obtient plusieurs résultats
            if ($total_dossiers > 1) {
                // Mémorisation de la recherche
                $search = $this->f->db->escapesimple($this->f->get_submitted_post_value("dossier"));
                // Ajout d'une étoile au début s'il n'y en n'a pas.
                // Par defaut * est toujours ajouté à la fin des recherches.
                if (substr($search, 0, 1) != '*') {
                    $search = '*'.$search;
                }
                // On redirige vers le listing des DI
                // 
                echo '
                <script type="text/javascript" >
                    widget_recherche_dossier(\''.$search.'\', '.$total_dossiers.', \'dossier_instruction\');
                </script>
                ';
                // On arrête l'exécution du script car l'utilisateur a été 
                // redirigé vers un autre script
                exit();
            }
            // Cas d'utilisation n°3
            // Si aucun dossier trouve
            // Affiche un message d'erreur
            $erreur = _("Aucun dossier trouvé.");
        }

        /**
         * Affichage du widget
         */
        // Affichage du message d'informations
        printf(
            '<div class="widget-help"><span class="info-16" title="%s"></span></div>',
            _("Permet la recherche directe de dossiers d'instruction.\n\n".
               "Deux modes de saisie sont possibles :\n".
               "- Code de dossier intégral 'PC0130551300027P0' ou 'PC 013055 13 00027P0' pour un accès direct à la fiche de visualisation du dossier d'instruction,\n".
               "- Code de dossier partiel 'DP' ou 'PC*P0' ou '*013055*' pour un accès au formulaire de recherche avancée des dossiers d'instruction.")
        );
        // Ouverture du form
        echo "\t<form method=\"post\" id=\"widget_recherche_dossier_form\" action=\"../scr/dashboard.php\">\n";
        // Affichage d'un éventuel message d'erreur
        if (isset($erreur) && $erreur != "") {
            $class = "error";
            $this->f->displayMessage($class, $erreur);
        }
        // Configuration du formulaire de recherche
        $champs = array("dossier");
        $form = new $this->om_formulaire(null, 0, 0, $champs);
        $form->setLib("dossier", '<span class="om-icon om-icon-16 om-icon-fix loupe-16">Recherche</span>');
        $form->setType("dossier", "text");
        $form->setTaille("dossier", 20);
        $form->setMax("dossier", 250);
        // Affichage du formulaire
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        // Affichage des contrôles du formulaire
        echo "\t<div class=\"formControls\">\n";
        echo "\t\t<input type=\"submit\" class=\"om-button ui-button ui-widget ui-state-default ui-corner-all\"
        value=\""._("Valider")."\" name=\"validation\" />\n";
        echo "\t</div>\n";
        // Fermeture du form
        echo "\t</form>\n";
    }


    /**
     * WIDGET DASHBOARD - widget_recherche_dossier_par_type.
     *
     * Le widget de recherche accès direct par type est similaire au widget de recherche
     * accès direct classique. Il permet en plus de choisir la portée de la recherche par
     * le biais d'un select : ADS ou RE* ou IN.
     * Selon le type de dossier choisi, lors de la recherche la redirection se fera vers
     * le menu Instruction ou Contentieux.
     *
     * @return void
     */
    function view_widget_recherche_dossier_par_type($content = null) {

        /**
         * Traitement de la validation du formulaire
         */
        if ($this->f->get_submitted_post_value("validation") != null
            && $this->f->get_submitted_post_value("dossier") !== null
            && $this->f->get_submitted_post_value("dossier") == '') {

            // AUCUNE VALEUR SAISIE
            // Affiche un message d'erreur
            $erreur = _("Veuillez saisir un No de dossier.");
        }
        //
        if ($this->f->get_submitted_post_value("validation") != null
            && $this->f->get_submitted_post_value("dossier") !== null
            && $this->f->get_submitted_post_value("dossier") != '') {
            //
            $list_dossiers = $this->execute_recherche_dossier("type");
            $posted_type_dossier = $this->f->get_submitted_post_value("type_dossier_recherche");
            // Définition des objets sur lesquels la redirection se fera
            if ($posted_type_dossier === "ADS") {
                $obj = "dossier_instruction";
            }
            if ($posted_type_dossier === "RE*") {
                $obj = "dossier_contentieux_tous_recours";
            }
            if ($posted_type_dossier === "IN") {
                $obj = "dossier_contentieux_toutes_infractions";
            }
            $total_dossiers = count($list_dossiers);
            // Si on obtient un seul résultat
            if ($total_dossiers == 1) {
                // On reformate le dossier
                $dossier = strtoupper(str_replace(' ', '', $list_dossiers[0]));
                // On redirige vers le listing des DI
                echo '
                <script type="text/javascript" >
                    widget_recherche_dossier(\'' . $dossier . '\', 1, \'' . $obj . '\');
                </script>
                ';
                // On arrête l'exécution du script car l'utilisateur a été 
                // redirigé vers un autre script
                exit();
            }
            // Si on obtient plusieurs résultats
            if ($total_dossiers > 1) {
                // Mémorisation de la recherche
                $search = $this->f->db->escapesimple($this->f->get_submitted_post_value("dossier"));
                // Ajout d'une étoile au début s'il n'y en n'a pas.
                // Par defaut * est toujours ajouté à la fin des recherches.
                if (substr($search, 0, 1) != '*') {
                    $search = '*'.$search;
                }
                // On redirige vers le listing des DI
                // 
                echo '
                <script type="text/javascript" >
                    widget_recherche_dossier(\''.$search.'\', '.$total_dossiers.', \'' . $obj . '\');
                </script>
                ';
                // On arrête l'exécution du script car l'utilisateur a été 
                // redirigé vers un autre script
                exit();
            }
            // Si aucun dossier trouve
            // Affiche un message d'erreur
            $erreur = _("Aucun dossier trouvé.");
        }

        /**
         * Affichage du widget
         */
        // Liste des paramètres
        $params = array("type_defaut", );
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "type_defaut" => "ADS",
        );
        $arguments = $this->get_arguments($content, $params);

        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "type_defaut"
                    && in_array($elem, array("ADS", "RE*", "IN"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        //
        $type_defaut = $arguments["type_defaut"];

        // Affichage du message d'informations
        printf(
            '<div class="widget-help"><span class="info-16" title="%s"></span></div>',
            _("Permet la recherche directe de dossiers d'instruction, avec choix de la portée de la recherche : ADS ou RE* ou IN.\n".
               "La sélection de la famille de dossier filtre les résultats et conditionne la redirection de l'utilisateur :\n".
               "- ADS : Instruction > Dossiers d'instruction > Recherche\n".
               "- RE* : Contentieux > Recours > Tous les Recours \n".
               "- IN : Contentieux > Infractions > Toutes les Infractions\n\n".
               "Deux modes de saisie sont possibles :\n".
               "- Code de dossier intégral 'PC0130551300027P0' ou 'PC 013055 13 00027P0' pour un accès direct à la fiche de visualisation du dossier d'instruction,\n".
               "- Code de dossier partiel 'DP' ou 'PC*P0' ou '*013055*' pour un accès au formulaire de recherche avancée des dossiers d'instruction.")
        );
        // Ouverture du form
        echo "\t<form method=\"post\" id=\"widget_recherche_dossier_par_type_form\" action=\"../scr/dashboard.php\">\n";
        // Affichage d'un éventuel message d'erreur
        if (isset($erreur) && $erreur != "") {
            $class = "error";
            $this->f->displayMessage($class, $erreur);
        }
        // Configuration du formulaire de recherche
        $champs = array("dossier", "type_dossier_recherche");
        $form = new $this->om_formulaire(null, 0, 0, $champs);
        $form->setLib("dossier", '<span class="om-icon om-icon-16 om-icon-fix loupe-16">Recherche</span>');
        $form->setType("dossier", "text");
        $form->setTaille("dossier", 20);
        $form->setMax("dossier", 250);

        // Définition des types de dossiers visibles dans le select
        $options[0] = array("ADS",
            "RE*",
            "IN");
        $options[1] = array(
            _("ADS"),
            _("RE*"),
            _("IN"));

        $form->setType("type_dossier_recherche", "select");
        $form->setLib("om_profil", _("Tableau de bord pour le profil"));
        $form->setSelect('type_dossier_recherche', $options);
        $form->setVal('type_dossier_recherche', $type_defaut);
        $form->setLib('type_dossier_recherche', 'Type de dossier');

        // Affichage du formulaire
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        // Affichage des contrôles du formulaire
        echo "\t<div class=\"formControls\">\n";
        echo "\t\t<input type=\"submit\" class=\"om-button ui-button ui-widget ui-state-default ui-corner-all\"
        value=\""._("Valider")."\" name=\"validation\" />\n";
        echo "\t</div>\n";
        // Fermeture du form
        echo "\t</form>\n";
    }


    /**
     * Méthode générique exécutant une recherche par numéro de dossier dans 2 contextes
     * différents :
     * - le widget de recherche classique
     * - le widget de recherche avec choix du type/groupe de dossier
     *
     * @param string $filtre Indique si un filtre supplémentaire doit être appliqué à la
     * recherche de dossier. Seule valeur possible : "type".
     *
     * @return string $error_message Le message d'erreur s'il y en a un, sinon chaîne vide
     */
    protected function execute_recherche_dossier($filtre = null) {

        // Traitement des valeurs postées
        $posted_dossiers = $this->f->db->escapesimple(strtolower(str_replace("*", "%", $this->f->get_submitted_post_value("dossier"))));
        $posted_dossiers = str_replace(';', ',', $posted_dossiers);

        //
        // UNE VALEUR SAISIE
        //

        // WHERE - Filtre Collectivité
        $query_ct_where_collectivite = "";
        // Si collectivité utilisateur mono alors filtre sur celle-ci
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            //
            $query_ct_where_collectivite = " AND dossier.om_collectivite=".$_SESSION['collectivite'];
        }

        // WHERE - Filtre par groupe
        $query_ct_where_groupe = "";
        $query_ct_where_type_da = "";
        $query_ct_from = "";
        $query_ct_where_common = "";
        $posted_type_dossier = $this->f->get_submitted_post_value("type_dossier_recherche");
        // La variable $posted_type_dossier vaut null si on est dans le contexte du widget
        // de recherche de dossier classique (sans choix du type)
        if ($posted_type_dossier === null OR $posted_type_dossier === "ADS") {
            $query_ct_from = "
                LEFT JOIN ".DB_PREFIXE."dossier_autorisation 
                    ON dossier.dossier_autorisation = 
                        dossier_autorisation.dossier_autorisation 
                LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
                    ON dossier_autorisation.dossier_autorisation_type_detaille = 
                        dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
                LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
                    ON dossier_autorisation_type_detaille.dossier_autorisation_type = 
                        dossier_autorisation_type.dossier_autorisation_type
                LEFT JOIN ".DB_PREFIXE."groupe
                    ON dossier_autorisation_type.groupe = groupe.groupe";
            //
            $query_ct_where_groupe = " 
                AND groupe.code != 'CTX'
            ";
        }
        if ($posted_type_dossier === "RE*" OR $posted_type_dossier === "IN") {
            $query_ct_from = "
                LEFT JOIN ".DB_PREFIXE."dossier_autorisation 
                    ON dossier.dossier_autorisation = 
                        dossier_autorisation.dossier_autorisation 
                LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
                    ON dossier_autorisation.dossier_autorisation_type_detaille = 
                        dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
                LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
                    ON dossier_autorisation_type_detaille.dossier_autorisation_type = 
                        dossier_autorisation_type.dossier_autorisation_type
                    ";
            if ($posted_type_dossier === "RE*") {
                $posted_type_dossier = "RE";
            }
            $query_ct_where_type_da = "
                AND dossier_autorisation_type.code = '". $posted_type_dossier . "'
            ";
        }

        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        // Construction de la requête
        // Pour chaque dossier on cherche sur les deux champs du code du
        // dossier (un avec et un sans espaces)
        $posted_dossiers = explode(',', $posted_dossiers);
        $liste_dossiers = array();
        foreach ($posted_dossiers as $posted_dossier) {
            $sql = "
            SELECT 
                dossier
            FROM
                " . DB_PREFIXE . "dossier
                " . $query_ct_from . "
            WHERE
                (LOWER(dossier.dossier) LIKE '%".$posted_dossier."%' 
                OR LOWER(dossier.dossier_libelle) LIKE '%".$posted_dossier."%' )
            ";
            $sql .= $query_ct_where_collectivite . $query_ct_where_type_da . $query_ct_where_groupe;
            // Exécution de la requête
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            // On récupère les numéros de dossier dans les résultats
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $liste_dossiers[] = $row['dossier'];
            }
        }

        return $liste_dossiers;
    }


    /**
     * Cette méthode permet de récupérer la configuration du widget 'Dossiers
     * limites'.
     *
     * @return array
     */
    function get_config_dossiers_limites($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "nombre_de_jours" => 15,
            "codes_datd" => null,
            "filtre" => "instructeur",
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "nombre_de_jours" 
                    && intval($elem) > 0) {
                    // Ce doit être un entier
                    $arguments[$key] = intval($elem);
                    continue;
                } elseif ($key === "codes_datd"
                    && $elem != "") {
                    // Ce doit être un tableau
                    $arguments[$key] = explode(";", $elem);
                    continue;
                } elseif ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        //
        $nombre_de_jours = $arguments["nombre_de_jours"];
        $codes_datd = $arguments["codes_datd"];
        $filtre = $arguments["filtre"];

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
        dossier.dossier,
        dossier.dossier_libelle,
        dossier.date_limite,
        dossier.date_limite_incompletude,
        CASE 
            WHEN dossier.incomplet_notifie IS TRUE AND dossier.incompletude IS TRUE 
                THEN to_char(dossier.date_limite_incompletude, 'DD/MM/YYYY') 
            ELSE 
                to_char(dossier.date_limite, 'DD/MM/YYYY')
        END as date_limite_na,
        COALESCE(
            demandeur.particulier_nom, 
            demandeur.personne_morale_denomination
        ) AS nom_petitionnaire,
        CASE 
            WHEN dossier.enjeu_erp is TRUE 
                THEN '<span class=''om-icon om-icon-16 om-icon-fix enjeu_erp-16'' title=''"._('Enjeu ERP')."''>ERP</span>' 
            ELSE 
                '' 
        END 
        ||
        CASE
            WHEN dossier.enjeu_urba is TRUE 
                THEN '<span class=''om-icon om-icon-16 om-icon-fix enjeu_urba-16'' title=''"._('Enjeu Urba')."''>URBA</span>' 
            ELSE 
                '' 
        END AS enjeu
        ";
        // SELECT - CHAMPAFFICHE
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            'COALESCE(
                demandeur.particulier_nom, 
                demandeur.personne_morale_denomination
            ) AS "'._("nom_petitionnaire").'"',
            'CASE 
                WHEN dossier.incomplet_notifie IS TRUE AND dossier.incompletude IS TRUE 
                    THEN to_char(dossier.date_limite_incompletude, \'DD/MM/YYYY\') 
                ELSE
                    to_char(dossier.date_limite, \'DD/MM/YYYY\') 
            END as "'._("date_limite").'"',
            'CASE 
                WHEN dossier.enjeu_erp is TRUE 
                    THEN \'<span class="om-icon om-icon-16 om-icon-fix enjeu_erp-16" title="'._('Enjeu ERP').'">ERP</span>\'
                ELSE 
                    \'\' 
            END 
            ||
            CASE
                WHEN dossier.enjeu_urba is TRUE 
                    THEN \'<span class="om-icon om-icon-16 om-icon-fix enjeu_urba-16" title="'._('Enjeu Urba').'">URBA</span>\'
                ELSE 
                    \'\' 
            END AS "'._("enjeu").'"',
            // XXX Attention cette colonne est cachée en css est doit donc restée la dernière du tableau
            'CASE WHEN incomplet_notifie IS TRUE AND incompletude IS TRUE THEN
                dossier.date_limite_incompletude ELSE
                dossier.date_limite END as date_limite_na',
        );
        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."dossier
        LEFT JOIN ".DB_PREFIXE."etat
            ON dossier.etat = etat.etat
        LEFT JOIN ".DB_PREFIXE."lien_dossier_demandeur
            ON dossier.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
        LEFT JOIN ".DB_PREFIXE."demandeur
            ON lien_dossier_demandeur.demandeur = demandeur.demandeur
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation
            ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
            ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
        %s
        %s
        %s
        ";

        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            //
            $query_ct_where_instructeur_filter = " JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
            JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                AND om_utilisateur.login='".$_SESSION['login']."' 
            ";
        } else {
            $query_ct_where_instructeur_filter = " LEFT JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur ";
        }
        // Filtre sur les dossier de la division
        if ($filtre == "division") {
            //
            $query_ct_where_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
            AND division.division = ".$_SESSION['division']."
            ";
        } else {
            $query_ct_where_division_filter = " LEFT JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division";
        }
        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);
        // WHERE - COMMON
        $query_ct_where_common = "
        groupe.code != 'CTX'
        AND
        (
            (
                dossier.incomplet_notifie IS FALSE
                AND date_limite <= CURRENT_TIMESTAMP + ".$nombre_de_jours." * interval '1 day'
            )
            OR
            (
                dossier.incomplet_notifie IS TRUE
                AND dossier.incompletude IS TRUE
                AND date_limite_incompletude <= CURRENT_TIMESTAMP + ".$nombre_de_jours." * interval '1 day'
            )
        )
        AND etat.statut != 'cloture'
        AND dossier.avis_decision IS NULL
        AND LOWER(dossier.accord_tacite) = 'oui' 
        ";
        // WHERE - DATD
        // Filtre sur le type détaillé des dossiers
        $query_ct_where_datd_filter = "";
        if (!is_null($codes_datd)
            && is_array($codes_datd)
            && count($codes_datd) != 0) {
            //
            $sql_codes = "";
            //
            foreach ($codes_datd as $code) {
                $sql_codes .= " LOWER(dossier_autorisation_type_detaille.code) = '".$this->f->db->escapesimple(strtolower($code))."' OR ";
            }
            $sql_codes = substr($sql_codes, 0, count($sql_codes) - 4);
            //
            $query_ct_where_datd_filter = " AND ( ".$sql_codes." ) ";
        }
        // ORDER BY
        $query_ct_orderby = "
        date_limite_na
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch($filtre) {
            case "instructeur" : 
                $message_filtre = " "._("(filtrés par instructeur)"); 
                break;
            case "division" : 
                $message_filtre = " "._("(filtrés par division)"); 
                break;
        }
        //
        $message_help = sprintf(
            _("Les dossiers%s avec acceptation tacite sur lesquels aucune décision n'a été prise et dont la date limite est dans moins de %s jours%s."),
            (is_null($codes_datd) ? "": " (".implode(";",$codes_datd).")"),
            $nombre_de_jours,
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where_common" => $query_ct_where_common,
            "query_ct_where_datd_filter" => $query_ct_where_datd_filter,
            "query_ct_where_groupe" => $query_ct_where_groupe,
            "query_ct_orderby" => $query_ct_orderby,
        );
    }

    /**
     * WIDGET DASHBOARD - Dossiers limites
     *
     * @return void
     */
    function view_widget_dossiers_limites($content = null) {

        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         *
         * - nombre_de_jours : c'est le délai en jours avant la date limite à 
         *   partir duquel on souhaite voir apparaître les dossiers dans le 
         *   widget.
         *   (default) Par défaut la valeur est 15 jours.
         *
         * - codes_datd : la liste des types de dossiers à afficher. exemple :
         *   "PCI;PCA;DPS;CUa;CUb".
         *   (default) Par défaut tous les types sont affichés. [null]
         *
         * - filtre : 
         *    = instructeur
         *    = division
         *    = aucun   
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("nombre_de_jours", "codes_datd", "filtre", );
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossiers_limites($arguments);
        //
        $nombre_de_jours = $conf["arguments"]["nombre_de_jours"];
        $codes_datd = $conf["arguments"]["codes_datd"];
        $filtre = $conf["arguments"]["filtre"];

        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
                %s
                %s
            ORDER BY
                %s
            LIMIT 10",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where_common"],
            $conf["query_ct_where_datd_filter"],
            $conf["query_ct_where_groupe"],
            $conf["query_ct_orderby"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );

        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            // Affichage du message d'informations
            echo _("Vous n'avez pas de dossiers limites pour le moment.");
            // Exit
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-3 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-1 firstcol">
                %s
            </td>
            <td class="col-1">
                %s
            </td>
            <td class="col-2">
                %s
            </td>
            <td class="col-3 lastcol">
                %s
            </td>
        </tr>
        ';
        //
        $template_href = '../scr/form.php?obj=dossier_instruction&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 2 - Nom du pétitionnaire
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["nom_petitionnaire"]
                ),
                // Colonne 3 - Date limite
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_limite_na"])
                ),
                // Colonne 4 - Enjeu
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["enjeu"]
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Numéro de dossier
            _('dossier'),
            // Colonne 2 - Nom du pétitionnaire
            _('nom_petitionnaire'),
            // Colonne 3 - Date limite
            _('date_limite'),
            // Colonne 4 - Enjeu
            _('enjeu'),
            // Contenu du tableau
            $ct_tbody
        );
        // Affichage du footer
        printf(
            $this->template_footer,
            // href (avec les paramètres du widget)
            sprintf(
                "../scr/tab.php?obj=dossiers_limites&nombre_de_jours=%s&codes_datd=%s&filtre=%s",
                $nombre_de_jours,
                (is_null($codes_datd) ? "" : implode(";",$codes_datd)),
                $filtre
            ),
            // titre
            _("Voir +")
        );
    }


    /**
     * Cette méthode permet de récupérer la configuration du widget 'Mes
     * recours'.
     *
     * @return array
     */
    function get_config_dossier_contentieux_recours($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "instructeur"
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        $filtre = $arguments["filtre"];

        // SELECT - CHAMPAFFICHE
        //
        $case_demandeur = "CASE WHEN demandeur.qualite='particulier' 
        THEN TRIM(CONCAT(demandeur.particulier_nom, ' ', demandeur.particulier_prenom)) 
        ELSE TRIM(CONCAT(demandeur.personne_morale_raison_sociale, ' ', demandeur.personne_morale_denomination)) 
        END";
        //
        $trim_concat_terrain ='TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',
            dossier.terrain_adresse_voie,\' \',
            dossier.terrain_adresse_lieu_dit,\' \',
            dossier.terrain_adresse_code_postal,\' \',
            dossier.terrain_adresse_localite,\' \',
            dossier.terrain_adresse_bp,\' \',
            dossier.terrain_adresse_cedex
        )) as "'._("localisation").'"'; 
        //
        $case_requerant = "
        CASE WHEN demandeur_requerant.qualite = 'particulier' 
            THEN TRIM(CONCAT(demandeur_requerant.particulier_nom, ' ', demandeur_requerant.particulier_prenom)) 
            ELSE TRIM(CONCAT(demandeur_requerant.personne_morale_raison_sociale, ' ', demandeur_requerant.personne_morale_denomination)) 
        END
        ";
        //
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            'dossier_autorisation_type_detaille.libelle as "'._("type de dossier").'"',
            'dossier_autorisation_contestee.dossier_libelle as "'._("autorisation").'"',
            $case_demandeur.' as "'._("petitionnaire").'"',
            $trim_concat_terrain,
            $case_requerant.' as "'._("requerant").'"',
            'to_char(dossier.date_depot ,\'DD/MM/YYYY\') as "'._("date de recours").'"',
            'avis_decision.libelle as "'._("decision").'"',
            'etat.libelle as "'._("etat").'"',
        );

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
        dossier.dossier,
        dossier.dossier_libelle,
        dossier.date_depot
        ";

        // FROM
        $query_ct_from = 
        DB_PREFIXE."dossier
        LEFT JOIN (
            SELECT * 
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('petitionnaire')
        ) as demandeur
            ON demandeur.dossier = dossier.dossier
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation
            ON dossier_autorisation.dossier_autorisation = dossier.dossier_autorisation
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON dossier_autorisation_type_detaille.dossier_autorisation_type_detaille = dossier_autorisation.dossier_autorisation_type_detaille
        LEFT JOIN ".DB_PREFIXE."etat
            ON dossier.etat = etat.etat
        LEFT JOIN ".DB_PREFIXE."division
            ON dossier.division = division.division
        LEFT JOIN ".DB_PREFIXE."avis_decision
           ON avis_decision.avis_decision=dossier.avis_decision
        LEFT OUTER JOIN ".DB_PREFIXE."arrondissement
            ON arrondissement.code_postal = dossier.terrain_adresse_code_postal
        LEFT JOIN (
            SELECT *
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('requerant')
        ) as demandeur_requerant
            ON demandeur_requerant.dossier = dossier.dossier
        LEFT JOIN ".DB_PREFIXE."dossier as dossier_autorisation_contestee
            ON dossier.autorisation_contestee = dossier_autorisation_contestee.dossier
        %s
        %s
        ";

        $query_ct_from_collectivite_filter = "";
        $query_ct_from_instructeur_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            $query_ct_from_instructeur_filter = " 
                JOIN ".DB_PREFIXE."instructeur
                    ON dossier.instructeur=instructeur.instructeur
                        OR dossier.instructeur_2=instructeur.instructeur
                JOIN ".DB_PREFIXE."om_utilisateur
                    ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                        AND om_utilisateur.login='".$_SESSION['login']."' 
                ";
        }
        else {
            $query_ct_from_instructeur_filter = " LEFT JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur ";
        }

        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_from_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        }
        else {
            $query_ct_from_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_from_instructeur_filter, $query_ct_from_collectivite_filter);

        // WHERE - COMMON
        $query_ct_where_common = "
            LOWER(dossier_autorisation_type.code) = LOWER('RE')
        ";

        // ORDER BY
        $query_ct_orderby = "
        dossier.dossier DESC
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " "._("dont je suis l'instructeur");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les derniers recours%s."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where_common,
            "query_ct_where_groupe" => $query_ct_where_groupe,
            "query_ct_orderby" => $query_ct_orderby
        );
    }


    /**
     * WIDGET DASHBOARD - Mes recours
     * @return void
     */
    function view_widget_dossier_contentieux_recours($content = null) {
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         * - filtre : 
         *    = instructeur
         *    = aucun   
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossier_contentieux_recours($arguments);
        // Récupération du filtre
        $filtre = $conf["arguments"]["filtre"];
        // Définit l'objet cible
        $obj = 'dossier_contentieux_tous_recours';
        if ($filtre === 'instructeur') {
            //
            $obj = 'dossier_contentieux_mes_recours';
        }

        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_orderby"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );
        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            echo _("Vous n'avez pas de recours.");
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-0 firstcol">
                %s
            </td>
            <td class="col-1 ">
                %s
            </td>
            <td class="col-2 lastcol">
                %s
            </td>
        </tr>
        ';
        //
        $template_href = '../scr/form.php?obj=%s&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';
        // Bouton consulter
        $template_btn_consulter = '
        <span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'
            ._('Consulter')
        .'</span>
        ';
        //

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // obj
                $obj,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $template_btn_consulter
                ),
                // Colonne 2 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 3 - Date de depot
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_depot"])
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Consulter
            '',
            // Colonne 2 - Numéro de dossier
            _('dossier'),
            // Colonne 3 - Date de depot
            _("Date de recours"),
            // Le Contenu
            $ct_tbody
        );

        /**
         * Affichage du footer
         */
        //
        $template_link_footer = '../scr/tab.php?obj=%s';
        //
        $link_footer = sprintf(
            $template_link_footer,
            $obj
        );
        //
        printf(
            $this->template_footer,
            $link_footer,
            _("Voir +")
        );
    }


    /**
     * Cette méthode permet de récupérer la configuration du widget 'Mes
     * infractions'.
     *
     * @return array
     */
    function get_config_dossier_contentieux_infraction($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "instructeur"
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        $filtre = $arguments["filtre"];

        // SELECT - CHAMPAFFICHE
        //
        $trim_concat_terrain ='TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',
            dossier.terrain_adresse_voie,\' \',
            dossier.terrain_adresse_lieu_dit,\' \',
            dossier.terrain_adresse_code_postal,\' \',
            dossier.terrain_adresse_localite,\' \',
            dossier.terrain_adresse_bp,\' \',
            dossier.terrain_adresse_cedex
        )) as "'._("localisation").'"'; 
        //
        $case_contrevenant = "
        CASE WHEN demandeur_contrevenant.qualite = 'particulier' 
            THEN TRIM(CONCAT(demandeur_contrevenant.particulier_nom, ' ', demandeur_contrevenant.particulier_prenom)) 
            ELSE TRIM(CONCAT(demandeur_contrevenant.personne_morale_raison_sociale, ' ', demandeur_contrevenant.personne_morale_denomination)) 
        END
        ";
        //
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            $trim_concat_terrain,
            $case_contrevenant.' as "'._("contrevenant").'"',
            'to_char(dossier.date_premiere_visite ,\'DD/MM/YYYY\') as "'._("date_premiere_visite").'"',
            'to_char(dossier.date_derniere_visite ,\'DD/MM/YYYY\') as "'._("date_derniere_visite").'"',
            'etat.libelle as "'._("etat").'"',
        );

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
        dossier.dossier,
        dossier.dossier_libelle,
        dossier.date_depot
        ";

        // FROM
        $query_ct_from = 
        DB_PREFIXE."dossier
        LEFT JOIN (
            SELECT * 
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('petitionnaire')
        ) as demandeur
            ON demandeur.dossier = dossier.dossier
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation
            ON dossier_autorisation.dossier_autorisation = dossier.dossier_autorisation
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON dossier_autorisation_type_detaille.dossier_autorisation_type_detaille = dossier_autorisation.dossier_autorisation_type_detaille
        LEFT JOIN ".DB_PREFIXE."etat
            ON dossier.etat = etat.etat
        LEFT JOIN ".DB_PREFIXE."division
            ON dossier.division = division.division
        LEFT JOIN ".DB_PREFIXE."avis_decision
           ON avis_decision.avis_decision=dossier.avis_decision
        LEFT OUTER JOIN ".DB_PREFIXE."arrondissement
            ON arrondissement.code_postal = dossier.terrain_adresse_code_postal
        LEFT JOIN (
            SELECT *
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('contrevenant')
        ) as demandeur_contrevenant
            ON demandeur_contrevenant.dossier = dossier.dossier
        %s
        %s
        ";

        $query_ct_from_collectivite_filter = "";
        $query_ct_from_instructeur_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            $query_ct_from_instructeur_filter = " 
                JOIN ".DB_PREFIXE."instructeur
                    ON dossier.instructeur=instructeur.instructeur
                        OR dossier.instructeur_2=instructeur.instructeur
                JOIN ".DB_PREFIXE."om_utilisateur
                    ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                        AND om_utilisateur.login='".$_SESSION['login']."' 
                ";
        }
        else {
            $query_ct_from_instructeur_filter = " LEFT JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur ";
        }

        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_from_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        }
        else {
            $query_ct_from_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_from_instructeur_filter, $query_ct_from_collectivite_filter);

        // WHERE - COMMON
        $query_ct_where_common = "
            LOWER(dossier_autorisation_type.code) = LOWER('IN')
        ";

        // ORDER BY
        $query_ct_orderby = "
        dossier.dossier DESC
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " "._("dont je suis l'instructeur");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les dernières infractions%s."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where_common,
            "query_ct_orderby" => $query_ct_orderby
        );
    }


    /**
     * WIDGET DASHBOARD - Mes infractions
     * @return void
     */
    function view_widget_dossier_contentieux_infraction($content = null) {
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         * - filtre : 
         *    = instructeur
         *    = aucun   
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossier_contentieux_infraction($arguments);
        // Récupération du filtre
        $filtre = $conf["arguments"]["filtre"];
        // Définit l'objet cible
        $obj = 'dossier_contentieux_toutes_infractions';
        if ($filtre === 'instructeur') {
            //
            $obj = 'dossier_contentieux_mes_infractions';
        }

        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_orderby"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );
        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            // Affichage du message d'informations
            echo _("Vous n'avez pas d'infraction.");
            // Exit
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-0 firstcol">
                %s
            </td>
            <td class="col-1 ">
                %s
            </td>
            <td class="col-2 lastcol">
                %s
            </td>
        </tr>
        ';

        // Bouton consulter
        $template_btn_consulter = '
        <span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'
            ._('Consulter')
        .'</span>
        ';
        //
        $template_href = '../scr/form.php?obj=%s&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // obj
                $obj,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $template_btn_consulter
                ),
                // Colonne 2 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 3 - Date de depot
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_depot"])
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Consulter
            '',
            // Colonne 2 - Numéro de dossier
            _('dossier'),
            // Colonne 3 - Date de depot
            _("Date de réception"),
            // Le Contenu
            $ct_tbody
        );

        /**
         * Affichage du footer
         */
        //
        $template_link_footer = '../scr/tab.php?obj=%s';
        //
        $link_footer = sprintf(
            $template_link_footer,
            $obj
        );
        //
        printf(
            $this->template_footer,
            $link_footer,
            _("Voir +")
        );
    }

    /**
     * Cet méthode permet de formater, la chaîne de caractères reçue du 
     * paramétrage du widget en un tableau de valeurs dont les clés 
     * correspondent aux clés passées en paramètre.
     *
     * @param string $content
     * @param array $params
     *
     * @return array
     */
    function get_arguments($content = null, $params = array()) {
        //
        $arguments = array();
        // On explose les paramètres reçus avec un élément par ligne
        $params_tmp1 = explode("\n", $content);
        // On boucle sur chaque ligne de paramètre
        foreach ($params_tmp1 as $key => $value) {
            // On explose le paramètre de sa valeur avec le séparateur '='
            $params_tmp2[] = explode("=", $value);
        }
        // On boucle sur chaque paramètre reçu pour vérifier si la valeur reçue
        // est acceptable ou non
        foreach ($params_tmp2 as $key => $value) {
            //
            if (!isset($value[0]) || !isset($value[1])) {
                continue;
            }
            //
            if (in_array(trim($value[0]), $params)) {
                $arguments[trim($value[0])] = trim($value[1]);
            }
        }
        //
        return $arguments;
    }

    /**
     * WIDGET DASHBOARD - Retours de messages
     *
     * @return void
     */
    function view_widget_messages_retours($content = null) {

        /**
         * Ce widget est configurable via l'interface Web. Lors de la création
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         *
         * - filtre :
         *    = instructeur
         *    = division
         *    = aucun
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         *
         * - contexte :
         *    = standard
         *    = contentieux
         *    (defaut) Par défaut le contexte est standard.
         */
        // Liste des paramètres
        $params = array("filtre", "contexte");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_messages_retours($arguments);
        //
        $filtre = $conf["arguments"]["filtre"];
        //
        $contexte = $conf["arguments"]["contexte"];

        /**
         * Composition de la requête
         */
        //
        $query = sprintf(
            "SELECT
                count(*)
            FROM
                %s
            WHERE
                %s",
            $conf["query_ct_from"],
            $conf["query_ct_where_common"],
            $conf["query_ct_where_groupe"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->getone($query);
        $this->addToLog(__METHOD__."(): db->getone(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );

        //
        if (intval($res) === 0) {
            //
            echo _("Aucun message non lu.");
            return;
        }


        /**
         *
         */
        $template_panel = '
        <div class="panel panel-box">
            <div class="list-justified-container">
                <ul class="list-justified text-center">
                    %s
                </ul>
            </div>
        </div>';
        /**
         *
         */
        //
        $panel = "";
        //
        $template_elem = '
        <li>
            <span class="size-h3 box-icon rounded %s">%s</span>
            <p class="text-muted">%s %s</p> 
        </li>
        ';
        $panel_elem = sprintf(
            $template_elem,
            "bg-info",
            intval($res),
            _("Non lu"),
            ""
        );
        //
        $panel .= sprintf(
            $template_panel,
            $panel_elem
        );
        echo $panel;

        // Définit le lien de redirection vers le listing en fonction du
        // contexte et du filtre
        if ($contexte === 'standard') {
            //
            switch ($filtre) {
                case 'instructeur':
                    $obj_href_more_link = 'messages_mes_retours';
                    break;
                case 'division':
                    $obj_href_more_link = 'messages_retours_ma_division';
                    break;
                case 'aucun':
                    $obj_href_more_link = 'messages_tous_retours';
                    break;
            }
        }
        //
        if ($contexte === 'contentieux') {
            //
            switch ($filtre) {
                case 'instructeur':
                    $obj_href_more_link = 'messages_contentieux_mes_retours';
                    break;
                case 'division':
                    $obj_href_more_link = 'messages_contentieux_retours_ma_division';
                    break;
                case 'aucun':
                    $obj_href_more_link = 'messages_contentieux_tous_retours';
                    break;
            }
        }

        //
        if (!$this->f->isAccredited(array($obj_href_more_link, $obj_href_more_link."_tab"), "OR")) {
            return;
        }
        // Affichage du footer
        printf(
            $this->template_footer,
            // href
            sprintf(
                '../scr/tab.php?obj=%s',
                $obj_href_more_link
            ),
            // titre
            _("Voir +")
        );

    }


    /**
     * Cette méthode permet de récupérer la configuration du widget 'Retours de
     * messages'.
     *
     * @return array
     */
    function get_config_messages_retours($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "instructeur",
            "contexte" => "standard",
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
                //
                if ($key === "contexte"
                    && in_array($elem, array("standard", "contentieux"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        //
        $filtre = $arguments["filtre"];
        //
        $contexte = $arguments["contexte"];

        /**
         * Construction de la requête
         */
        //
        $query_ct_from_instructeur_filter = "";
        $query_ct_from_division_filter = "";
        $query_ct_from_collectivite_filter = "";
        //
        $query_ct_from_instructeur_filter = " LEFT JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
            LEFT JOIN ".DB_PREFIXE."instructeur as instructeur_2
                ON dossier.instructeur_2=instructeur_2.instructeur ";
        //
        if ($filtre == "instructeur") {
            $query_ct_from_instructeur_filter = " JOIN ".DB_PREFIXE."instructeur
                    ON dossier.instructeur=instructeur.instructeur
                        OR dossier.instructeur_2=instructeur.instructeur
                JOIN ".DB_PREFIXE."om_utilisateur
                    ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
            ";
        }
        //
        $query_ct_from_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
        ";
        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_from_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_from_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."dossier_message
        LEFT JOIN ".DB_PREFIXE."dossier
            ON dossier_message.dossier=dossier.dossier
        %s
        %s
        %s";

        //
        $query_ct_from = sprintf($query_ct_from, $query_ct_from_instructeur_filter, $query_ct_from_division_filter, $query_ct_from_collectivite_filter);

        //
        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            //
            $query_ct_where_instructeur_filter = " AND om_utilisateur.login='".$_SESSION['login']."'
                AND dossier_message.destinataire = 'instructeur'
            ";
            //
            if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                $query_ct_where_instructeur_filter = " AND (om_utilisateur.login='".$_SESSION['login']."'
                    OR dossier_message.destinataire = 'commune') 
                ";
            }
        }
        // Filtre sur les dossier de la division
        if ($filtre == "division") {
            //
            $query_ct_where_division_filter = " AND division.division = ".$_SESSION['division']."
                AND dossier_message.destinataire = 'instructeur'
            ";
            //
            if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                $query_ct_where_division_filter = " AND (division.division = ".$_SESSION['division']."
                    OR dossier_message.destinataire = 'commune') 
                ";
            }
        }

        // Filtre les dossiers par contexte
        $query_ct_where_groupe_filter = " AND LOWER(groupe.code) != 'ctx'";
        if ($contexte === 'contentieux') {
            //
            $query_ct_where_groupe_filter = " AND LOWER(groupe.code) = 'ctx'";
        }

        // WHERE - COMMON
        $query_ct_where_common = "
        dossier_message.lu IS FALSE
        %s
        %s
        %s
        %s
        ";

        //
        $query_ct_where_common = sprintf($query_ct_where_common, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter, $query_ct_where_groupe_filter);

        // Filtre du groupe
        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("dont je suis l'instructeur ou dont le destinataire est 'commune'");
                } else {
                    $message_filtre = " "._("dont je suis l'instructeur et dont le destinataire est 'instructeur'");
                }
                break;
            case "division":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma division ou dont le destinataire est 'commune'");
                } else {
                    $message_filtre = " "._("situés dans ma division et dont le destinataire est 'instructeur'");
                }
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $template_message_help = _("Les messages marqués comme 'non lu' qui concernent des dossiers d'instruction%s.");
        //
        if ($contexte === 'contentieux') {
            //
            $template_message_help = _("Les messages marqués comme 'non lu' qui concernent des dossiers contentieux%s.");
        }
        //
        $message_help = sprintf(
            $template_message_help,
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_from" => $query_ct_from,
            "query_ct_where_common" => $query_ct_where_common,
            "query_ct_where_groupe" => $query_ct_where_groupe,
        );
    }

    /**
     * WIDGET DASHBOARD - Retours de consultation
     *
     * @return void
     */
    function view_widget_consultation_retours($content = null) {

        /**
         * Ce widget est configurable via l'interface Web. Lors de la création
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         *
         * - filtre :
         *    = instructeur
         *    = division
         *    = aucun
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre", );
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_consultation_retours($arguments);
        //
        $filtre = $conf["arguments"]["filtre"];

        /**
         * Composition de la requête
         */
        //
        $query = sprintf(
            "SELECT
                count(*)
            FROM
                %s
            WHERE
                %s",
            $conf["query_ct_from"],
            $conf["query_ct_where_common"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->getone($query);
        $this->addToLog(__METHOD__."(): db->getone(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );

        //
        if (intval($res) === 0) {
            //
            echo _("Aucun retour de consultation non lu.");
            return;
        }


        /**
         *
         */
        $template_panel = '
        <div class="panel panel-box">
            <div class="list-justified-container">
                <ul class="list-justified text-center">
                    %s
                </ul>
            </div>
        </div>';
        /**
         *
         */
        //
        $panel = "";
        //
        $template_elem = '
        <li>
            <span class="size-h3 box-icon rounded %s">%s</span>
            <p class="text-muted">%s %s</p>
        </li>
        ';
        $panel_elem = sprintf(
            $template_elem,
            "bg-info",
            intval($res),
            _("Non lu"),
            ""
        );
        //
        $panel .= sprintf(
            $template_panel,
            $panel_elem
        );
        echo $panel;

        /**
         *
         */
        if ($filtre === "aucun") {
            $obj_href_more_link = "consultation_tous_retours";
        } elseif ($filtre === "division") {
            $obj_href_more_link = "consultation_retours_ma_division";
        } else {
            $obj_href_more_link = "consultation_mes_retours";
        }

        //
        if (!$this->f->isAccredited(array($obj_href_more_link, $obj_href_more_link."_tab"), "OR")) {
            return;
        }
        // Affichage du footer
        printf(
            $this->template_footer,
            // href
            sprintf(
                '../scr/tab.php?obj=%s',
                $obj_href_more_link
            ),
            // titre
            _("Voir +")
        );

    }


    /**
     * Cette méthode permet de récupérer la configuration du widget 'Retours de
     * consultation'.
     *
     * @return array
     */
    function get_config_consultation_retours($arguments) {
        // Initialisation du tableau des paramètres avec ses valeurs par défaut
        $arguments_default = array(
            "filtre" => "instructeur",
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        //
        $filtre = $arguments["filtre"];

        /**
         * Construction de la requête
         */
        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."consultation
        LEFT JOIN ".DB_PREFIXE."avis_consultation
            ON consultation.avis_consultation=avis_consultation.avis_consultation
        LEFT JOIN ".DB_PREFIXE."dossier
            ON consultation.dossier=dossier.dossier
        LEFT JOIN ".DB_PREFIXE."service
            ON consultation.service=service.service
        %s
        %s
        %s";

        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre === "instructeur") {
            //
            $query_ct_where_instructeur_filter = " JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
            JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                AND om_utilisateur.login='".$_SESSION['login']."' 
            ";
        } else {
            $query_ct_where_instructeur_filter = " LEFT JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur ";
        }
        // Filtre sur les dossier de la division
        if ($filtre === "division") {
            //
            $query_ct_where_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
            AND division.division = ".$_SESSION['division']."
            ";
        } else {
            $query_ct_where_division_filter = " LEFT JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division";
        }
        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);

        // WHERE - COMMON
        $query_ct_where_common = "
        consultation.lu IS FALSE
        ";

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " "._("dont je suis l'instructeur");
                break;
            case "division":
                $message_filtre = " "._("situés dans ma division");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les consultations marquées comme 'non lu' qui concernent des ".
               "dossiers d'instruction%s."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_from" => $query_ct_from,
            "query_ct_where_common" => $query_ct_where_common,
        );
    }


    /**
     * WIDGET DASHBOARD - widget_commission_retours
     *
     * Ce widget est configurable via l'interface Web. Lors de la création
     * du widget dans le paramétrage il est possible de spécifier la ou les
     * options suivantes :
     *
     * - filtre :
     *    = instructeur
     *    = division
     *    = aucun
     *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
     *
     * @param string $content Arguments pour le widget, ici les filtres.
     * @return void
     */
    function view_widget_commission_retours($content = null) {
        // Liste des paramètres
        $params = array("filtre", );
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_commission_retours($arguments);
        //
        $filtre = $conf["arguments"]["filtre"];

        /**
         * Composition de la requête
         */
        //
        $query = sprintf(
            "SELECT
                count(*)
            FROM
                %s
            WHERE
                %s",
            $conf["query_ct_from"],
            $conf["query_ct_where_common"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->getone($query);
        $this->addToLog(__METHOD__ . "(): db->getone(\"" . $query . "\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );

        if (intval($res) === 0) {
            //
            echo _("Aucun retour de commission non lu.");
            return;
        }


        /**
         * Panel
         */
        $template_panel = '
        <div class="panel panel-box">
            <div class="list-justified-container">
                <ul class="list-justified text-center">
                    %s
                </ul>
            </div>
        </div>';
        $panel = "";
        //
        $template_elem = '
        <li>
            <span class="size-h3 box-icon rounded %s">%s</span>
            <p class="text-muted">%s %s</p>
        </li>
        ';
        $panel_elem = sprintf(
            $template_elem,
            "bg-info",
            intval($res),
            _("Non lu"),
            ""
        );
        $panel .= sprintf(
            $template_panel,
            $panel_elem
        );
        echo $panel;

        /**
         * Widget footer
         */
        if ($filtre === "aucun") {
            $obj_href_more_link = "commission_tous_retours";
        } elseif ($filtre === "division") {
            $obj_href_more_link = "commission_retours_ma_division";
        } else {
            $obj_href_more_link = "commission_mes_retours";
        }

        //
        if (!$this->f->isAccredited(array($obj_href_more_link, $obj_href_more_link . "_tab"), "OR")) {
            return;
        }
        // Affichage du footer
        printf(
            $this->template_footer,
            // href
            sprintf(
                '../scr/tab.php?obj=%s',
                $obj_href_more_link
            ),
            // titre
            _("Voir +")
        );
    }

    /**
     * Cette méthode permet de récupérer la configuration du widget 'Retours de
     * consultation'.
     *
     * @return array
     */
    function get_config_commission_retours($arguments) {
        // Initialisation du tableau des paramètres avec ses valeurs par défaut
        $arguments_default = array(
            "filtre" => "instructeur",
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        //
        $filtre = $arguments["filtre"];

        /**
         * Construction de la requête
         */
        // FROM
        $query_ct_from ="
        " . DB_PREFIXE . "dossier_commission
        LEFT JOIN " . DB_PREFIXE . "dossier
            ON dossier_commission.dossier=dossier.dossier
        LEFT JOIN " . DB_PREFIXE . "commission
            ON dossier_commission.commission=commission.commission
        %s
        %s
        %s";

        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";

        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre === "instructeur") {
            //
            $query_ct_where_instructeur_filter = " JOIN " . DB_PREFIXE . "instructeur
                ON dossier.instructeur=instructeur.instructeur
            JOIN " . DB_PREFIXE . "om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur
                AND om_utilisateur.login='" . $_SESSION['login'] . "'
            ";
        } else {
            $query_ct_where_instructeur_filter = " LEFT JOIN " . DB_PREFIXE . "instructeur
                ON dossier.instructeur=instructeur.instructeur
            LEFT JOIN " . DB_PREFIXE . "om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur ";
        }
        // Filtre sur les dossier de la division
        if ($filtre === "division") {
            //
            $query_ct_where_division_filter = " JOIN " . DB_PREFIXE . "division
            ON dossier.division=division.division
            AND division.division = " . $_SESSION['division'] . "
            ";
        } else {
            $query_ct_where_division_filter = " LEFT JOIN " . DB_PREFIXE . "division
            ON dossier.division=division.division";
        }
        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN " . DB_PREFIXE . "om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            AND om_collectivite.om_collectivite=" . $_SESSION['collectivite'] . "
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN " . DB_PREFIXE . "om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);

        // WHERE - COMMON
        $query_ct_where_common = "
        dossier_commission.lu IS FALSE
        ";

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " " . _("dont je suis l'instructeur");
                break;
            case "division":
                $message_filtre = " " . _("situés dans ma division");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " " . _("situés dans ma collectivité");
                } else {
                    $message_filtre = " " . _("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les retours de commission marqués comme 'non lu' qui concernent des " .
              "dossiers d'instruction%s."),
            $message_filtre
        );

        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_from" => $query_ct_from,
            "query_ct_where_common" => $query_ct_where_common,
        );

    }


    /**
     * WIDGET DASHBOARD - Dossiers en incomplet ou majoration sans retour RAR
     *
     * @return void
     */
    function view_widget_dossiers_evenement_incomplet_majoration($content = null) {

        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         *
         * - filtre : 
         *    = instructeur
         *    = division
         *    = aucun
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre", );
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossiers_evenement_incomplet_majoration($arguments);
        //
        $filtre = $conf["arguments"]["filtre"];

        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
                %s
            ORDER BY
                %s
            LIMIT 10",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where_common"],
            $conf["query_ct_where_groupe"],
            $conf["query_ct_orderby"]
        );

        // Exécution de la requête
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );

        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            // Affichage du message d'informations
            echo _("Vous n'avez pas de dossiers avec un evenement incomplet ou majoration sans RAR pour le moment.");
            // Exit
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        // Lien vers le dossier d'instructions

        // Bouton consulter
        $template_btn_consulter = '
        <span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'
            ._('Consulter')
        .'</span>
        ';

        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';

        // Données dans le tableau
        // 
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-1 firstcol">
                %s
            </td>
            <td class="col-1">
                %s
            </td>
            <td class="col-2 lastcol">
                %s
            </td>
        </tr>
        ';
        //
        $template_href = '../scr/form.php?obj=dossier_instruction&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Bouton consulter
                sprintf(
                    $template_link,
                    $ct_href,
                    $template_btn_consulter
                ),
                // Colonne 2 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 3 - Date de dépôt
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_depot"])
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Bouton consulter
            '',
            // Colonne 2 - Numéro de dossier
            _('dossier'),
            // Colonne 3 - Date de dépôt du dossier
            _('date_depot'),
            // Contenu du tableau
            $ct_tbody
        );

        // Affichage du footer
        printf(
            $this->template_footer,
            // href (avec les paramètres du widget)
            sprintf(
                "../scr/tab.php?obj=dossiers_evenement_incomplet_majoration&filtre=%s",
                $filtre
            ),
            // titre
            _("Voir tous les dossiers evenement incomplet ou majoration sans RAR")
        );
    }


    /**
     * Cette méthode permet de récupérer la configuration du widget 'Retours de
     * consultation'.
     *
     * @return array
     */
    function get_config_dossiers_evenement_incomplet_majoration($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "instructeur",
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        //
        $filtre = $arguments["filtre"];

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
            dossier.dossier,
            dossier.dossier_libelle,
            dossier.date_depot
        ";
        // SELECT - CHAMPAFFICHE
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            'to_char(dossier.date_depot ,\'DD/MM/YYYY\') as "'._("date_depot").'"'
        );
        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."dossier
        LEFT JOIN 
            ".DB_PREFIXE."instruction
            ON
                dossier.dossier = instruction.dossier
        LEFT JOIN
            ".DB_PREFIXE."evenement
            ON
                instruction.evenement = evenement.evenement
        %s
        %s
        %s
        ";

        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            //
            $query_ct_where_instructeur_filter = " JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
            JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                AND om_utilisateur.login='".$_SESSION['login']."' 
            ";
        } else {
            $query_ct_where_instructeur_filter = " LEFT JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur ";
        }
        // Filtre sur les dossier de la division
        if ($filtre == "division") {
            //
            $query_ct_where_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
            AND division.division = ".$_SESSION['division']."
            ";
        } else {
            $query_ct_where_division_filter = " LEFT JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division";
        }
        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);

        // WHERE - COMMON
        $query_ct_where_common = "
            (
                LOWER(evenement.type) = 'incompletude' OR
                LOWER(evenement.type) = 'majoration_delai'
            ) AND
            (
                instruction.date_envoi_rar > CURRENT_TIMESTAMP  - interval '1 month' AND
                instruction.date_envoi_rar <= CURRENT_TIMESTAMP
            ) AND
            instruction.date_retour_rar IS NULL AND
            evenement.retour = 'f'
        ";

        // ORDER BY
        $query_ct_orderby = "
            dossier.date_depot desc
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " "._("dont je suis l'instructeur");
                break;
            case "division":
                $message_filtre = " "._("situés dans ma division");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les dossiers d'instruction%s qui ont un evenement incomplet ou majoration".
            " avec une date d'envoi RAR, mais pas de date de retour RAR."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where_common" => $query_ct_where_common,
            "query_ct_where_groupe" => $query_ct_where_groupe,
            "query_ct_orderby" => $query_ct_orderby,
        );
    }


    /**
     * WIDGET DASHBOARD - widget_infos_profil.
     */
    function view_widget_infos_profil($content = null) {

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        </br>
        <h4>Liste des accès</h4>
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-1 firstcol">
                %s
            </td>
            <td class="col-1">
                %s
            </td>
            <td class="col-2 lastcol">
                %s
            </td>
        </tr>
        ';


        // Récupère les informations sur l'utilisateur
        $this->f->getUserInfos();

        // Si l'utilisateur est loggé $_SESSION existe
        if(isset($_SESSION['login']) AND !empty($_SESSION['login'])) {

            // On compose le bloc html d'affichage des informations de l'utilisateur
            $bloc_infos_profil = "
                %s
                <div class=\"profil-infos\">
                    <h4>Utilisateur</h4>
                    <div class=\"profil-infos-profil\">
                        <span class=\"libelle\">%s</span> : <span class=\"value\">%s</span>
                    </div>
                    <div class=\"profil-infos-nom\">
                        <span class=\"libelle\">%s</span> : <span class=\"value\">%s</span>
                    </div>
            ";

            // Si l'utilisateur connecté est un instructeur
            if ($this->f->isUserInstructeur() === true) {

                // On compose le bloc html d'affichage des informations de l'utilisateur
                $bloc_infos_profil .= "
                        <div class=\"profil-infos-division\">
                            <span class=\"libelle\">%s</span> : <span class=\"value\">%s</span>
                        </div>
                ";

                // Requête de récupération de la qualité de l'instructeur
                $query = sprintf(
                    "SELECT
                        instructeur_qualite.libelle
                    FROM
                        " . DB_PREFIXE. "instructeur
                        INNER JOIN " . DB_PREFIXE . "instructeur_qualite 
                            ON instructeur.instructeur_qualite=instructeur_qualite.instructeur_qualite
                    WHERE
                        instructeur.om_utilisateur = " . intval($this->f->om_utilisateur["om_utilisateur"])
                );
                $instr_qualite_lib = $this->f->db->getone($query);
                $this->addToLog(__METHOD__."(): db->getone(\"".$query."\");", VERBOSE_MODE);
                $this->f->isDatabaseError($instr_qualite_lib);
                // S'il y a un résultat
                if ($instr_qualite_lib !== null) {
                    $bloc_infos_profil .= "
                        <div class=\"profil-infos-instructeur_qualite\">
                            <span class=\"libelle\">" . _("Qualité") . "</span> : <span class=\"value\">" . $instr_qualite_lib . "</span>
                        </div>
                    ";
                }
            } else {
                // Pour éviter une NOTICE
                $this->f->om_utilisateur["code"] = '';
            }
            //
            $bloc_infos_profil  .= "</div>";

            // Ajout d'un tableau listant les groupes de l'utilisateur ainsi que ses
            // accès aux groupes
            $msg_erreur_groupe = '';
            $bloc_tableau_droits = '';
            // Si le profil et l'utilisateur n'ont pas de groupe défini
            if (isset($_SESSION['groupe']) === false) {
                $msg_erreur_groupe = '
                    <div class="message ui-widget ui-corner-all ui-state-highlight ui-state-error borderless">
                        <p>
                            <span class="ui-icon ui-icon-info"></span> 
                            <span class="text">Problème de paramétrage : vous n\'avez pas de groupe associé.</span>
                        </p>
                    </div>
                ';
            } else {
                $ct_tbody = '';
                // On construit le contenu du tableau
                foreach ($_SESSION['groupe'] as $key => $value) {
                    if ($value['confidentiel'] === true) {
                        $value['confidentiel'] = _('Oui');
                    }
                    else {
                        $value['confidentiel'] = _('Non');
                    }
                    if ($value['enregistrement_demande'] === true) {
                        $value['enregistrement_demande'] = _('Oui');
                    }
                    else {
                        $value['enregistrement_demande'] = _('Non');
                    }
                    // On construit la ligne
                    $ct_tbody .= sprintf(
                        $template_line,
                        // Colonne 1 - Libellé du groupe
                        $value["libelle"],
                        // Colonne 2 - A accès aux dossiers confidentiels
                        $value["confidentiel"],
                        // Colonne 3 - Peut créer une demande
                        $value["enregistrement_demande"]
                    );
                }
                // Affichage du tableau listant les dossiers
                $bloc_infos_profil .= sprintf(
                    $template_table,
                    // Colonne 1 - Libellé du groupe
                    _('groupe'),
                    // Colonne 2 - A accès aux dossiers confidentiels
                    _('dossiers confidentiels'),
                    // Colonne 3 - Peut créer une demande
                    _('enregistrement demande'),
                    // Contenu du tableau
                    $ct_tbody
                );
            }
            // Affichage du bloc html avec les variables associées
            printf(
                $bloc_infos_profil,
                $msg_erreur_groupe,
                _('Profil'), $this->f->om_utilisateur["libelle_profil"],
                _('Nom'), $this->f->om_utilisateur["nom"],
                _('Division'), $this->f->om_utilisateur["code"]
            );
        }
    }


    /**
     * Cette méthode permet de récupérer la configuration du widget
     * 'Mes contradictoires' ou 'Les contradictoires'.
     *
     * @return array
     */
    function get_config_dossier_contentieux_contradictoire($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "instructeur"
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        $filtre = $arguments["filtre"];
        
        // SELECT - CHAMPAFFICHE
        $trim_concat_terrain = 'TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',
            dossier.terrain_adresse_voie,\' \',
            dossier.terrain_adresse_lieu_dit,\' \',
            dossier.terrain_adresse_code_postal,\' \',
            dossier.terrain_adresse_localite,\' \',
            dossier.terrain_adresse_bp,\' \',
            dossier.terrain_adresse_cedex
        )) as "'._("localisation").'"';
        //
        $case_contrevenant = "
        CASE WHEN demandeur_contrevenant.qualite = 'particulier' 
            THEN TRIM(CONCAT(demandeur_contrevenant.particulier_nom, ' ', demandeur_contrevenant.particulier_prenom)) 
            ELSE TRIM(CONCAT(demandeur_contrevenant.personne_morale_raison_sociale, ' ', demandeur_contrevenant.personne_morale_denomination)) 
        END
        ";
        //
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            $trim_concat_terrain,
            $case_contrevenant.' as "'._("contrevenant").'"',
            'to_char(dossier.date_premiere_visite ,\'DD/MM/YYYY\') as "'._("date_premiere_visite").'"',
            'to_char(dossier.date_derniere_visite ,\'DD/MM/YYYY\') as "'._("date_derniere_visite").'"',
            'etat.libelle as "'._("etat").'"',
            'to_char(dossier.date_contradictoire, \'DD/MM/YYYY\') as "'._("date_contradictoire").'"',
            'to_char(dossier.date_retour_contradictoire, \'DD/MM/YYYY\') as "'._("date_retour_contradictoire").'"',
        );

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
            dossier.dossier,
            dossier.dossier_libelle,
            dossier.date_contradictoire,
            dossier.date_retour_contradictoire
        ";

        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."dossier
        LEFT JOIN ".DB_PREFIXE."etat
            ON dossier.etat = etat.etat
        LEFT JOIN (
            SELECT *
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('contrevenant')
        ) as demandeur_contrevenant
            ON demandeur_contrevenant.dossier = dossier.dossier
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation
            ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
            ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
        LEFT JOIN ".DB_PREFIXE."avis_decision
            ON avis_decision.avis_decision=dossier.avis_decision
        %s
        %s
        %s
        ";

        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            //
            $query_ct_where_instructeur_filter = " JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
                    OR dossier.instructeur_2=instructeur.instructeur
            JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                AND om_utilisateur.login='".$_SESSION['login']."' 
            ";
        }

        // Filtre sur les dossier de la division
        if ($filtre == "division") {
            //
            $query_ct_where_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
            AND division.division = ".$_SESSION['division']."
            ";
        }

        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);

        // WHERE - COMMON
        $query_ct_where_common = "
            LOWER(dossier_autorisation_type.code) = LOWER('IN')
            AND NOT EXISTS
                (SELECT NULL
                FROM ".DB_PREFIXE."instruction
                INNER JOIN ".DB_PREFIXE."evenement
                    ON instruction.evenement = evenement.evenement
                WHERE instruction.dossier = dossier.dossier
                AND evenement.type = 'annul_contradictoire')
            AND (dossier.date_contradictoire >= CURRENT_DATE + interval '3 weeks'
                OR (dossier.date_contradictoire IS NOT NULL
                   AND dossier.date_retour_contradictoire IS NULL))
            AND date_ait IS NULL
        ";

        // ORDER BY
        $query_ct_orderby = "
            dossier.date_depot ASC
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " "._("dont je suis l'instructeur");
                break;
            case "division":
                $message_filtre = " "._("situés dans ma division");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les infractions%s les plus anciennes pour lesquelles la date de contradictoire est saisie (soit elle est supérieure ou égale à la date du jour + 3 semaines, soit elle ne rentre pas dans cette condition et la date de retour du contradictoire est vide), il n'y a pas d'événements de type 'Annulation de contradictoire' et il n'y a pas d'AIT créé."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where_common,
            "query_ct_where_groupe" => $query_ct_where_groupe,
            "query_ct_orderby" => $query_ct_orderby,
        );
    }

    /**
     * WIDGET DASHBOARD - Les ou Mes dossiers contradictoires
     * @return void
     */
    function view_widget_dossier_contentieux_contradictoire($content = null) {
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         * - filtre : 
         *    = instructeur
         *    = division
         *    = aucun   
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossier_contentieux_contradictoire($arguments);

        $filtre = $conf["arguments"]["filtre"];
        //

       
        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_where_groupe"],
            $conf["query_ct_orderby"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );
        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            echo _("Il n'y a pas d'infractions pour lesquelles la date de contradictoire est saisie (soit elle est supérieure ou égale à la date du jour + 3 semaines, soit elle ne rentre pas dans cette condition et la date de retour du contradictoire est vide), il n'y a pas d'événements de type 'Annulation de contradictoire' et il n'y a pas d'AIT créé.");
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-3 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-0 firstcol">
                %s
            </td>
            <td class="col-1 ">
                %s
            </td>
            <td class="col-2">
                %s
            </td>
            <td class="col-3 lastcol">
                %s
            </td>
        </tr>
        ';
        //
        $template_href = '../scr/form.php?obj=dossier_contentieux_toutes_infractions&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';
        // Bouton consulter
        $template_btn_consulter = '
        <span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'
            ._('Consulter')
        .'</span>
        ';
        //

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $template_btn_consulter
                ),
                // Colonne 2 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 3 - Date contradictoire
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_contradictoire"])
                ),
                // Colonne 4 - Date retour contradictoire
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_retour_contradictoire"])
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Consulter
            '',
            // Colonne 2 - Numéro de dossier
            _('dossier'),
            // Colonne 3 - Date contradictoire
            _('date_contradictoire'),
            // Colonne 4 - Date retour contradictoire
            _('date_retour_contradictoire'),
            // Le Contenu
            $ct_tbody
        );
        // Affichage du footer
        printf(
            $this->template_footer,
            "../scr/tab.php?obj=dossier_contentieux_contradictoire&filtre=" . $filtre,
            _("Voir +")
        );
    }


    /**
     * Cette méthode permet de récupérer la configuration du widget 'Mes AIT'
     * ou 'Les AIT'.
     *
     * @return array
     */
    function get_config_dossier_contentieux_ait($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "instructeur"
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        $filtre = $arguments["filtre"];
        
        // SELECT - CHAMPAFFICHE
        $trim_concat_terrain = 'TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',
            dossier.terrain_adresse_voie,\' \',
            dossier.terrain_adresse_lieu_dit,\' \',
            dossier.terrain_adresse_code_postal,\' \',
            dossier.terrain_adresse_localite,\' \',
            dossier.terrain_adresse_bp,\' \',
            dossier.terrain_adresse_cedex
        )) as "'._("localisation").'"';
        //
        $case_contrevenant = "
        CASE WHEN demandeur_contrevenant.qualite = 'particulier' 
            THEN TRIM(CONCAT(demandeur_contrevenant.particulier_nom, ' ', demandeur_contrevenant.particulier_prenom)) 
            ELSE TRIM(CONCAT(demandeur_contrevenant.personne_morale_raison_sociale, ' ', demandeur_contrevenant.personne_morale_denomination)) 
        END
        ";
        //
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            $trim_concat_terrain,
            $case_contrevenant.' as "'._("contrevenant").'"',
            'to_char(dossier.date_premiere_visite ,\'DD/MM/YYYY\') as "'._("date_premiere_visite").'"',
            'to_char(dossier.date_derniere_visite ,\'DD/MM/YYYY\') as "'._("date_derniere_visite").'"',
            'etat.libelle as "'._("etat").'"',
            'to_char(dossier.date_ait, \'DD/MM/YYYY\') as "'._("date_ait").'"',
            'to_char(instruction.date_retour_signature, \'DD/MM/YYYY\') as "'._("date_retour_signature").'"',
        );

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
            dossier.dossier,
            dossier.dossier_libelle,
            dossier.date_ait,
            instruction.date_retour_signature
        ";

        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."dossier
        LEFT JOIN ".DB_PREFIXE."etat
            ON dossier.etat = etat.etat
        LEFT JOIN (
            SELECT *
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('contrevenant')
        ) as demandeur_contrevenant
            ON demandeur_contrevenant.dossier = dossier.dossier
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation
            ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
            ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
        LEFT JOIN ".DB_PREFIXE."avis_decision
            ON avis_decision.avis_decision=dossier.avis_decision
        INNER JOIN ".DB_PREFIXE."instruction
            ON dossier.dossier = instruction.dossier
            AND date_retour_signature IS NOT NULL
        INNER JOIN ".DB_PREFIXE."evenement
           ON instruction.evenement = evenement.evenement
           AND LOWER(evenement.type) LIKE 'ait'
        %s
        %s
        %s
        ";

        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            //
            $query_ct_where_instructeur_filter = " JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
                    OR dossier.instructeur_2=instructeur.instructeur
            JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                AND om_utilisateur.login='".$_SESSION['login']."' 
            ";
        }

        // Filtre sur les dossier de la division
        if ($filtre == "division") {
            //
            $query_ct_where_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
            AND division.division = ".$_SESSION['division']."
            ";
        }

        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);

        // WHERE
        $query_ct_where_common = "
            LOWER(dossier_autorisation_type.code) = LOWER('IN')
        ";

        // ORDER BY
        $query_ct_orderby = "
            dossier.date_depot DESC
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " "._("dont je suis l'instructeur");
                break;
            case "division":
                $message_filtre = " "._("situés dans ma division");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les infractions%s les plus récentes pour lesquelles il y a un AIT signé."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where_common,
            "query_ct_where_groupe" => $query_ct_where_groupe,
            "query_ct_orderby" => $query_ct_orderby
        );
    }


    /**
     * WIDGET DASHBOARD - Les ou Mes dossiers AIT
     * @return void
     */
    function view_widget_dossier_contentieux_ait($content = null) {
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         * - filtre : 
         *    = instructeur
         *    = division
         *    = aucun   
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossier_contentieux_ait($arguments);
        $filtre = $conf["arguments"]["filtre"];

       
        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_where_groupe"],
            $conf["query_ct_orderby"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );
        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            echo _("Il n'y a pas d'infractions pour lesquelles il y a un AIT signé.");
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-3 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-0 firstcol">
                %s
            </td>
            <td class="col-1 ">
                %s
            </td>
            <td class="col-2">
                %s
            </td>
            <td class="col-3 lastcol">
                %s
            </td>
        </tr>
        ';
        //
        $template_href = '../scr/form.php?obj=dossier_contentieux_toutes_infractions&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';
        // Bouton consulter
        $template_btn_consulter = '
        <span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'
            ._('Consulter')
        .'</span>
        ';
        //

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $template_btn_consulter
                ),
                // Colonne 2 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 3 - Date AIT
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_ait"])
                ),
                // Colonne 4 - Date retour signature
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_retour_signature"])
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Consulter
            '',
            // Colonne 2 - Numéro de dossier
            _('dossier'),
            // Colonne 3 - Date AIT
            _('date_ait'),
            // Colonne 4 - Date retour signature
            _('date_retour_signature'),
            // Le Contenu
            $ct_tbody
        );
        // Affichage du footer
        printf(
            $this->template_footer,
            "../scr/tab.php?obj=dossier_contentieux_ait&filtre=" . $filtre,
            _("Voir +")
        );
    }


    /**
     * Cette méthode permet de récupérer la configuration du widget 'Les infractions
     * non affectées'.
     *
     * @return array
     */
    function get_config_dossier_contentieux_inaffectes($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "division"
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        $filtre = $arguments["filtre"];
        
        // SELECT - CHAMPAFFICHE
        $trim_concat_terrain = 'TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',
            dossier.terrain_adresse_voie,\' \',
            dossier.terrain_adresse_lieu_dit,\' \',
            dossier.terrain_adresse_code_postal,\' \',
            dossier.terrain_adresse_localite,\' \',
            dossier.terrain_adresse_bp,\' \',
            dossier.terrain_adresse_cedex
        )) as "'._("localisation").'"';
        //
        $case_contrevenant = "
        CASE WHEN demandeur_contrevenant.qualite = 'particulier' 
            THEN TRIM(CONCAT(demandeur_contrevenant.particulier_nom, ' ', demandeur_contrevenant.particulier_prenom)) 
            ELSE TRIM(CONCAT(demandeur_contrevenant.personne_morale_raison_sociale, ' ', demandeur_contrevenant.personne_morale_denomination)) 
        END
        ";
        //
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            $trim_concat_terrain,
            $case_contrevenant.' as "'._("contrevenant").'"',
            'to_char(dossier.date_premiere_visite ,\'DD/MM/YYYY\') as "'._("date_premiere_visite").'"',
            'to_char(dossier.date_derniere_visite ,\'DD/MM/YYYY\') as "'._("date_derniere_visite").'"',
            'etat.libelle as "'._("etat").'"',
            'to_char(dossier.date_depot, \'DD/MM/YYYY\') as "'._("Date de réception").'"',
        );

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
            dossier.dossier,
            dossier.dossier_libelle,
            dossier.date_depot
        ";

        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."dossier
        LEFT JOIN ".DB_PREFIXE."etat
            ON dossier.etat = etat.etat
        LEFT JOIN (
            SELECT *
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('contrevenant')
        ) as demandeur_contrevenant
            ON demandeur_contrevenant.dossier = dossier.dossier
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation
            ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
            ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
        LEFT JOIN ".DB_PREFIXE."avis_decision
            ON avis_decision.avis_decision=dossier.avis_decision
        %s
        %s
        ";

        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossier de la division
        if ($filtre == "division") {
            //
            $query_ct_where_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
            AND division.division = ".$_SESSION['division']."
            ";
        } else {
            $query_ct_where_division_filter = " LEFT JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division";
        }

        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);
        
        // WHERE - COMMON
        $query_ct_where_common = "
            LOWER(dossier_autorisation_type.code) = LOWER('IN')
            AND dossier.instructeur_2 IS NULL
        ";

        // ORDER BY

        $query_ct_orderby = "
            dossier.date_depot ASC
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "division":
                $message_filtre = " "._("situés dans ma division");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les infractions%s les plus anciennes non-affectées à un technicien."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where_common,
            "query_ct_where_groupe" => $query_ct_where_groupe,
            "query_ct_orderby" => $query_ct_orderby
        );
    }

    /**
     * WIDGET DASHBOARD - Les infractions non affectées
     * @return void
     */
    function view_widget_dossier_contentieux_inaffectes($content = null) {
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         * - filtre : 
         *    = instructeur
         *    = division
         *    = aucun   
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossier_contentieux_inaffectes($arguments);

        $filtre = $conf["arguments"]["filtre"];
        //

       
        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_where_groupe"],
            $conf["query_ct_orderby"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );
        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            echo _("Il n'y a pas d'infractions non-affectées à un technicien.");
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-0 firstcol">
                %s
            </td>
            <td class="col-1 ">
                %s
            </td>
            <td class="col-2 lastcol">
                %s
            </td>
        </tr>
        ';
        //
        $template_href = '../scr/form.php?obj=dossier_contentieux_toutes_infractions&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';
        // Bouton consulter
        $template_btn_consulter = '
        <span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'
            ._('Consulter')
        .'</span>
        ';
        //

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $template_btn_consulter
                ),
                // Colonne 2 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 3 - Date de réception
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_depot"])
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Consulter
            '',
            // Colonne 2 - Numéro de dossier
            _('dossier'),
            // Colonne 3 - Date de réception
            _('Date de réception'),
            // Le Contenu
            $ct_tbody
        );
        // Affichage du footer
        printf(
            $this->template_footer,
            "../scr/tab.php?obj=dossier_contentieux_inaffectes&filtre=" . $filtre,
            _("Voir +")
        );
    }


    /**
     * Cette méthode permet de récupérer la configuration du widget 'Alerte Visite'.
     *
     * @return array
     */
    function get_config_dossier_contentieux_alerte_visite($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "instructeur"
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        $filtre = $arguments["filtre"];

        // SELECT - CHAMPAFFICHE
        $trim_concat_terrain = 'TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',
            dossier.terrain_adresse_voie,\' \',
            dossier.terrain_adresse_lieu_dit,\' \',
            dossier.terrain_adresse_code_postal,\' \',
            dossier.terrain_adresse_localite,\' \',
            dossier.terrain_adresse_bp,\' \',
            dossier.terrain_adresse_cedex
        )) as "'._("localisation").'"';
        //
        $case_contrevenant = "
        CASE WHEN demandeur_contrevenant.qualite = 'particulier' 
            THEN TRIM(CONCAT(demandeur_contrevenant.particulier_nom, ' ', demandeur_contrevenant.particulier_prenom)) 
            ELSE TRIM(CONCAT(demandeur_contrevenant.personne_morale_raison_sociale, ' ', demandeur_contrevenant.personne_morale_denomination)) 
        END
        ";
        //
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            $trim_concat_terrain,
            $case_contrevenant.' as "'._("contrevenant").'"',
            'to_char(dossier.date_premiere_visite ,\'DD/MM/YYYY\') as "'._("date_premiere_visite").'"',
            'to_char(dossier.date_derniere_visite ,\'DD/MM/YYYY\') as "'._("date_derniere_visite").'"',
            'etat.libelle as "'._("etat").'"',
            'to_char(dossier.date_depot, \'DD/MM/YYYY\') as "'._("Date de réception").'"',
        );

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
            dossier.dossier,
            dossier.dossier_libelle,
            dossier.date_depot
        ";

        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."dossier
        LEFT JOIN ".DB_PREFIXE."etat
            ON dossier.etat = etat.etat
        LEFT JOIN (
            SELECT *
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('contrevenant')
        ) as demandeur_contrevenant
            ON demandeur_contrevenant.dossier = dossier.dossier
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation
            ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
            ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
        LEFT JOIN ".DB_PREFIXE."avis_decision
            ON avis_decision.avis_decision=dossier.avis_decision
        %s
        %s
        %s
        ";

        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            //
            $query_ct_where_instructeur_filter = " JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
                    OR dossier.instructeur_2=instructeur.instructeur
            JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                AND om_utilisateur.login='".$_SESSION['login']."' 
            ";
        }

        // Filtre sur les dossier de la division
        if ($filtre == "division") {
            //
            $query_ct_where_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
            AND division.division = ".$_SESSION['division']."
            ";
        }

        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);

        // WHERE - COMMON
        $query_ct_where_common = "
            LOWER(dossier_autorisation_type.code) = LOWER('IN')
            AND dossier.date_depot < CURRENT_TIMESTAMP - interval '3 months'
            AND dossier.date_premiere_visite IS NULL
        ";
        // ORDER BY

        $query_ct_orderby = "
            dossier.date_depot ASC
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " "._("dont je suis l'instructeur");
                break;
            case "division":
                $message_filtre = " "._("situés dans ma division");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }

        //
        $message_help = sprintf(
            _("Les infractions%s les plus anciennes pour lesquelles la date de réception est dépassée depuis plus de 3 mois et dont la date de première visite n'est pas saisie."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where_common,
            "query_ct_where_groupe" => $query_ct_where_groupe,
            "query_ct_orderby" => $query_ct_orderby
        );
    }

    /**
     * WIDGET DASHBOARD - Alerte Visite
     * @return void
     */
    function view_widget_dossier_contentieux_alerte_visite($content = null) {
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         * - filtre : 
         *    = instructeur
         *    = division
         *    = aucun   
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossier_contentieux_alerte_visite($arguments);

        //
        $filtre = $conf["arguments"]["filtre"];
        //

       
        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_where_groupe"],
            $conf["query_ct_orderby"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );
        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            echo _("Il n'y a pas d'infractions pour lesquelles la date de réception est dépassée depuis plus de 3 mois et dont la date de première visite n'est pas saisie.");
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-0 firstcol">
                %s
            </td>
            <td class="col-1 ">
                %s
            </td>
            <td class="col-2 lastcol">
                %s
            </td>
        </tr>
        ';
        //
        $template_href = '../scr/form.php?obj=dossier_contentieux_toutes_infractions&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';
        // Bouton consulter
        $template_btn_consulter = '
        <span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'
            ._('Consulter')
        .'</span>
        ';
        //

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $template_btn_consulter
                ),
                // Colonne 2 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 3 - Date de réception
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_depot"])
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Consulter
            '',
            // Colonne 2 - Numéro de dossier
            _('dossier'),
            // Colonne 3 - Date de réception
            _('Date de réception'),
            // Le Contenu
            $ct_tbody
        );
        // Affichage du footer
        printf(
            $this->template_footer,
            "../scr/tab.php?obj=dossier_contentieux_alerte_visite&filtre=" . $filtre,
            _("Voir +")
        );
    }

    /**
     * Cette méthode permet de récupérer la configuration du widget 'Alerte Parquet'.
     *
     * @return array
     */
    function get_config_dossier_contentieux_alerte_parquet($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "instructeur"
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }
        $filtre = $arguments["filtre"];

        // SELECT - CHAMPAFFICHE
        $trim_concat_terrain = 'TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',
            dossier.terrain_adresse_voie,\' \',
            dossier.terrain_adresse_lieu_dit,\' \',
            dossier.terrain_adresse_code_postal,\' \',
            dossier.terrain_adresse_localite,\' \',
            dossier.terrain_adresse_bp,\' \',
            dossier.terrain_adresse_cedex
        )) as "'._("localisation").'"';
        //
        $case_contrevenant = "
        CASE WHEN demandeur_contrevenant.qualite = 'particulier' 
            THEN TRIM(CONCAT(demandeur_contrevenant.particulier_nom, ' ', demandeur_contrevenant.particulier_prenom)) 
            ELSE TRIM(CONCAT(demandeur_contrevenant.personne_morale_raison_sociale, ' ', demandeur_contrevenant.personne_morale_denomination)) 
        END
        ";
        //
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            $trim_concat_terrain,
            $case_contrevenant.' as "'._("contrevenant").'"',
            'to_char(dossier.date_premiere_visite ,\'DD/MM/YYYY\') as "'._("date_premiere_visite").'"',
            'to_char(dossier.date_derniere_visite ,\'DD/MM/YYYY\') as "'._("date_derniere_visite").'"',
            'etat.libelle as "'._("etat").'"',
            'to_char(dossier.date_depot, \'DD/MM/YYYY\') as "'._("Date de réception").'"',
        );

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
            dossier.dossier,
            dossier.dossier_libelle,
            dossier.date_depot
        ";

        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."dossier
        LEFT JOIN ".DB_PREFIXE."etat
            ON dossier.etat = etat.etat
        LEFT JOIN (
            SELECT *
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('contrevenant')
        ) as demandeur_contrevenant
            ON demandeur_contrevenant.dossier = dossier.dossier
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation
            ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
            ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
        LEFT JOIN ".DB_PREFIXE."avis_decision
            ON avis_decision.avis_decision=dossier.avis_decision
        %s
        %s
        %s
        ";

        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            //
            $query_ct_where_instructeur_filter = " JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
                    OR dossier.instructeur_2=instructeur.instructeur
            JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                AND om_utilisateur.login='".$_SESSION['login']."' 
            ";
        }

        // Filtre sur les dossier de la division
        if ($filtre == "division") {
            //
            $query_ct_where_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
            AND division.division = ".$_SESSION['division']."
            ";
        }

        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);

        // WHERE - COMMON
        $query_ct_where_common = "
            LOWER(dossier_autorisation_type.code) = LOWER('IN')
            AND dossier.date_depot < CURRENT_TIMESTAMP - interval '9 months'
            AND dossier.date_transmission_parquet IS NULL
        ";

        // ORDER BY
        $query_ct_orderby = "
            dossier.date_depot ASC
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " "._("dont je suis l'instructeur");
                break;
            case "division":
                $message_filtre = " "._("situés dans ma division");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les infractions%s les plus anciennes pour lesquelles la date de réception est dépassée depuis plus de 9 mois et dont la date de transmission au parquet n'est pas saisie."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where_common,
            "query_ct_where_groupe" => $query_ct_where_groupe,
            "query_ct_orderby" => $query_ct_orderby
        );
    }

    /**
     * WIDGET DASHBOARD - Alerte Parquet
     * @return void
     */
    function view_widget_dossier_contentieux_alerte_parquet($content = null) {
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         * - filtre : 
         *    = instructeur
         *    = division
         *    = aucun   
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossier_contentieux_alerte_parquet($arguments);

        //
        $filtre = $conf["arguments"]["filtre"];
        //

       
        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_where_groupe"],
            $conf["query_ct_orderby"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );
        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            echo _("Il n'y a pas d'infractions pour lesquelles la date de réception est dépassée depuis plus de 9 mois et dont la date de transmission au parquet n'est pas saisie.");
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-0 firstcol">
                %s
            </td>
            <td class="col-1 ">
                %s
            </td>
            <td class="col-2 lastcol">
                %s
            </td>
        </tr>
        ';
        //
        $template_href = '../scr/form.php?obj=dossier_contentieux_toutes_infractions&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';
        // Bouton consulter
        $template_btn_consulter = '
        <span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'
            ._('Consulter')
        .'</span>
        ';
        //

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $template_btn_consulter
                ),
                // Colonne 2 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 3 - Date de réception
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_depot"])
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Consulter
            '',
            // Colonne 2 - Numéro de dossier
            _('dossier'),
            // Colonne 3 - Date de réception
            _('Date de réception'),
            // Le Contenu
            $ct_tbody
        );
        // Affichage du footer
        printf(
            $this->template_footer,
            "../scr/tab.php?obj=dossier_contentieux_alerte_parquet&filtre=" . $filtre,
            _("Voir +")
        );
    }

    /**
     * Cette méthode permet de récupérer la configuration du widget 'Mes clôtures'.
     *
     * @return array
     */
    function get_config_dossier_contentieux_clotures($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "instructeur",
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }

        $filtre = $arguments["filtre"];

        // SELECT - CHAMPAFFICHE
        $trim_concat_terrain = 'TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',
            dossier.terrain_adresse_voie,\' \',
            dossier.terrain_adresse_lieu_dit,\' \',
            dossier.terrain_adresse_code_postal,\' \',
            dossier.terrain_adresse_localite,\' \',
            dossier.terrain_adresse_bp,\' \',
            dossier.terrain_adresse_cedex
        )) as "'._("localisation").'"';
        //
        $case_requerant = "
        CASE WHEN demandeur_requerant.qualite = 'particulier' 
            THEN TRIM(CONCAT(demandeur_requerant.particulier_nom, ' ', demandeur_requerant.particulier_prenom)) 
            ELSE TRIM(CONCAT(demandeur_requerant.personne_morale_raison_sociale, ' ', demandeur_requerant.personne_morale_denomination)) 
        END
        ";
        //
        $case_demandeur = "CASE WHEN demandeur.qualite='particulier' 
        THEN TRIM(CONCAT(demandeur.particulier_nom, ' ', demandeur.particulier_prenom)) 
        ELSE TRIM(CONCAT(demandeur.personne_morale_raison_sociale, ' ', demandeur.personne_morale_denomination)) 
        END";
        //
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            'dossier_autorisation_type_detaille.libelle as "'._("type de dossier").'"',
            'dossier_autorisation_contestee.dossier_libelle as "'._("autorisation").'"',
            $case_demandeur.' as "'._("petitionnaire").'"',
            $trim_concat_terrain,
            $case_requerant.' as "'._("requerant").'"',
            'to_char(dossier.date_depot ,\'DD/MM/YYYY\') as "'._("date_depot").'"',
            'avis_decision.libelle as "'._("decision").'"',
            'etat.libelle as "'._("etat").'"',
            'to_char(dossier.date_cloture_instruction, \'DD/MM/YYYY\') as "'._("date_cloture_instruction").'"',
        );

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
            dossier.dossier,
            dossier.dossier_libelle,
            dossier.date_cloture_instruction
        ";

        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."dossier
        LEFT JOIN ".DB_PREFIXE."etat
            ON dossier.etat = etat.etat
        LEFT JOIN (
            SELECT * 
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('petitionnaire')
        ) as demandeur
            ON demandeur.dossier = dossier.dossier
        LEFT JOIN (
            SELECT *
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('requerant')
        ) as demandeur_requerant
            ON demandeur_requerant.dossier = dossier.dossier
        LEFT JOIN ".DB_PREFIXE."dossier as dossier_autorisation_contestee
            ON dossier.autorisation_contestee = dossier_autorisation_contestee.dossier
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation
            ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
            ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
        LEFT JOIN ".DB_PREFIXE."avis_decision
            ON avis_decision.avis_decision=dossier.avis_decision
        %s
        %s
        %s
        ";

        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            //
            $query_ct_where_instructeur_filter = " JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
                    OR dossier.instructeur_2=instructeur.instructeur
            JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                AND om_utilisateur.login='".$_SESSION['login']."' 
            ";
        }

        // Filtre sur les dossier de la division
        if ($filtre == "division") {
            //
            $query_ct_where_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
            AND division.division = ".$_SESSION['division']."
            ";
        }

        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);

        // WHERE - COMMON
        $query_ct_where_common = "
            LOWER(dossier_autorisation_type.code) = LOWER('RE')
            AND dossier.date_cloture_instruction >= CURRENT_TIMESTAMP
            AND dossier.date_cloture_instruction <= CURRENT_TIMESTAMP + interval '1 month'
        ";

        // ORDER BY

        $query_ct_orderby = "
        dossier.date_cloture_instruction ASC
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " "._("dont je suis l'instructeur");
                break;
            case "division":
                $message_filtre = " "._("situés dans ma division");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les recours%s les plus récents pour lesquels une date de clôture d'instruction existe et est comprise entre le jour courant et un mois dans le futur."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where_common,
            "query_ct_where_groupe" => $query_ct_where_groupe,
            "query_ct_orderby" => $query_ct_orderby
        );
    }

    /**
     * WIDGET DASHBOARD - Les clôtures
     * @return void
     */
    function view_widget_dossier_contentieux_clotures($content = null) {

        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         * - filtre : 
         *    = instructeur
         *    = division
         *    = aucun   
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossier_contentieux_clotures($arguments);

        $filtre = $conf["arguments"]["filtre"];
        //

       
        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_where_groupe"],
            $conf["query_ct_orderby"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );
        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            echo _("Il n'y a pas de recours pour lesquels une date de clôture d'instruction existe et est comprise entre le jour courant et un mois dans le futur.");
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-0 firstcol">
                %s
            </td>
            <td class="col-1 ">
                %s
            </td>
            <td class="col-2 lastcol">
                %s
            </td>
        </tr>
        ';
        //
        $template_href = '../scr/form.php?obj=dossier_contentieux_tous_recours&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';
        // Bouton consulter
        $template_btn_consulter = '
        <span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'
            ._('Consulter')
        .'</span>
        ';
        //

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $template_btn_consulter
                ),
                // Colonne 2 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 3 - Date de clôture d'instruction
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["date_cloture_instruction"])
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Consulter
            '',
            // Colonne 2 - Numéro de dossier
            _('dossier'),
            // Colonne 3 - Date de clôture d'instruction
            _("date_cloture_instruction"),
            // Le Contenu
            $ct_tbody
        );
        // Affichage du footer
        printf(
            $this->template_footer,
            "../scr/tab.php?obj=dossier_contentieux_clotures&filtre=" . $filtre,
            _("Voir +")
        );
    }

    /**
     * Cette méthode permet de récupérer la configuration du widget 'Les audience'.
     *
     * @return array
     */
    function get_config_dossier_contentieux_audience($arguments) {
        // Initialisation du tableau des paramètres avec ses valeur par défaut
        $arguments_default = array(
            "filtre" => "instructeur"
        );
        // Vérification des arguments
        foreach ($arguments_default as $key => $value) {
            //
            if (isset($arguments[$key])) {
                //
                $elem = trim($arguments[$key]);
                //
                if ($key === "filtre"
                    && in_array($elem, array("instructeur", "division", "aucun"))) {
                    // La valeur doit être dans cette liste
                    $arguments[$key] = $elem;
                    continue;
                }
            }
            //
            $arguments[$key] = $value;
        }

        $filtre = $arguments["filtre"];

        // SELECT - CHAMPAFFICHE
        $trim_concat_terrain = 'TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',
            dossier.terrain_adresse_voie,\' \',
            dossier.terrain_adresse_lieu_dit,\' \',
            dossier.terrain_adresse_code_postal,\' \',
            dossier.terrain_adresse_localite,\' \',
            dossier.terrain_adresse_bp,\' \',
            dossier.terrain_adresse_cedex
        )) as "'._("localisation").'"';
        //
        $case_contrevenant = "
        CASE WHEN demandeur_contrevenant.qualite = 'particulier' 
            THEN TRIM(CONCAT(demandeur_contrevenant.particulier_nom, ' ', demandeur_contrevenant.particulier_prenom)) 
            ELSE TRIM(CONCAT(demandeur_contrevenant.personne_morale_raison_sociale, ' ', demandeur_contrevenant.personne_morale_denomination)) 
        END
        ";
        //
        $query_ct_select_champaffiche = array(
            'dossier.dossier as "'._("dossier").'"',
            'dossier.dossier_libelle as "'._("dossier").'"',
            $trim_concat_terrain,
            $case_contrevenant.' as "'._("contrevenant").'"',
            'to_char(dossier.date_premiere_visite ,\'DD/MM/YYYY\') as "'._("date_premiere_visite").'"',
            'to_char(dossier.date_derniere_visite ,\'DD/MM/YYYY\') as "'._("date_derniere_visite").'"',
            'etat.libelle as "'._("etat").'"',
            'to_char(donnees_techniques.ctx_date_audience, \'DD/MM/YYYY\') as "'._("ctx_date_audience").'"',
        );

        /**
         * Construction de la requête
         */
        // SELECT
        $query_ct_select = "
            dossier.dossier,
            dossier.dossier_libelle,
            donnees_techniques.ctx_date_audience
        ";

        // FROM
        $query_ct_from = "
        ".DB_PREFIXE."dossier
        LEFT JOIN ".DB_PREFIXE."etat
            ON dossier.etat = etat.etat
        LEFT JOIN (
            SELECT *
            FROM ".DB_PREFIXE."lien_dossier_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur
                ON demandeur.demandeur = lien_dossier_demandeur.demandeur
            WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
            AND LOWER(demandeur.type_demandeur) = LOWER('contrevenant')
        ) as demandeur_contrevenant
            ON demandeur_contrevenant.dossier = dossier.dossier
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation
            ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
            ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
        LEFT JOIN ".DB_PREFIXE."avis_decision
            ON avis_decision.avis_decision=dossier.avis_decision
        LEFT JOIN ".DB_PREFIXE."donnees_techniques
            ON donnees_techniques.dossier_instruction = dossier.dossier
        %s
        %s
        %s
        ";

        $query_ct_where_instructeur_filter = "";
        $query_ct_where_division_filter = "";
        $query_ct_where_collectivite_filter = "";
        // Filtre sur les dossiers qui concernent l'utilisateur
        if ($filtre == "instructeur") {
            //
            $query_ct_where_instructeur_filter = " JOIN ".DB_PREFIXE."instructeur
                ON dossier.instructeur=instructeur.instructeur
                    OR dossier.instructeur_2=instructeur.instructeur
            JOIN ".DB_PREFIXE."om_utilisateur
                ON instructeur.om_utilisateur=om_utilisateur.om_utilisateur 
                AND om_utilisateur.login='".$_SESSION['login']."' 
            ";
        }

        // Filtre sur les dossier de la division
        if ($filtre == "division") {
            //
            $query_ct_where_division_filter = " JOIN ".DB_PREFIXE."division
            ON dossier.division=division.division
            AND division.division = ".$_SESSION['division']."
            ";
        }

        // Dans tous les cas si l'utilisateur fait partie d'une collectivité
        // de niveau 1 (mono), on restreint le listing sur les dossiers de sa
        // collectivité
        if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
            $query_ct_where_collectivite_filter = " JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite 
            AND om_collectivite.om_collectivite=".$_SESSION['collectivite']." 
            ";
        } else {
            $query_ct_where_collectivite_filter = " LEFT JOIN ".DB_PREFIXE."om_collectivite
            ON dossier.om_collectivite=om_collectivite.om_collectivite
            ";
        }

        $query_ct_from = sprintf($query_ct_from, $query_ct_where_instructeur_filter, $query_ct_where_division_filter, $query_ct_where_collectivite_filter);
        
        // WHERE - COMMON
        $query_ct_where_common = "
            LOWER(dossier_autorisation_type.code) = LOWER('IN')
            AND donnees_techniques.ctx_date_audience IS NOT NULL
            AND donnees_techniques.ctx_date_audience >= CURRENT_TIMESTAMP
            AND donnees_techniques.ctx_date_audience <= CURRENT_TIMESTAMP + interval '1 month'
        ";
        // ORDER BY

        $query_ct_orderby = "
            donnees_techniques.ctx_date_audience ASC
        ";

        $query_ct_where_groupe = "";
        // Gestion des groupes et confidentialité
        include('../sql/pgsql/filter_group_widgets.inc.php');

        /**
         * Message d'aide
         */
        //
        $message_filtre = "";
        //
        switch ($filtre) {
            case "instructeur":
                $message_filtre = " "._("dont je suis l'instructeur");
                break;
            case "division":
                $message_filtre = " "._("situés dans ma division");
                break;
            case "aucun":
                if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {
                    $message_filtre = " "._("situés dans ma collectivité");
                } else {
                    $message_filtre = " "._("situés dans toutes les collectivités");
                }
                break;
        }
        //
        $message_help = sprintf(
            _("Les infractions%s les plus récentes pour lesquelles une date d'audience existe et est comprise entre le jour courant et un mois dans le futur."),
            $message_filtre
        );

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where_common,
            "query_ct_where_groupe" => $query_ct_where_groupe,
            "query_ct_orderby" => $query_ct_orderby,
        );
    }

    /**
     * WIDGET DASHBOARD - Les audiences
     * @return void
     */
    function view_widget_dossier_contentieux_audience($content = null) {
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création 
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         * - filtre : 
         *    = instructeur
         *    = division
         *    = aucun   
         *   (default) Par défaut les dossiers sont filtrés sur l'instructeur.
         */
        // Liste des paramètres
        $params = array("filtre");
        // Formatage des arguments reçus en paramètres
        $arguments = $this->get_arguments($content, $params);
        // Récupération de la configuration du widget
        $conf = $this->get_config_dossier_contentieux_audience($arguments);

        //
        $filtre = $conf["arguments"]["filtre"];
        //
       
        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_where_groupe"],
            $conf["query_ct_orderby"]
        );

        /**
         * Exécution de la requête
         */
        //
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Affichage du message d'informations
        printf(
            $this->template_help,
            $conf["message_help"]
        );
        /**
         * Si il n'y a aucun dossier à afficher, alors on affiche un message 
         * clair à l'utilisateur et on sort de la vue.
         */
        // Si il n'y a aucun dossier à afficher
        if ($res->numrows() == 0) {
            echo _("Il n'y a pas d'infractions pour lesquelles une date d'audience existe et est comprise entre le jour courant et un mois dans le futur.");
            return;
        }

        /**
         * Template nécessaires à l'affichage du widget
         */
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-0 firstcol">
                %s
            </td>
            <td class="col-1 ">
                %s
            </td>
            <td class="col-2 lastcol">
                %s
            </td>
        </tr>
        ';
        //
        $template_href = '../scr/form.php?obj=dossier_contentieux_toutes_infractions&amp;action=3&amp;idx=%s';
        //
        $template_link = '
        <a class="lienTable" href="%s">
            %s
        </a>
        ';
        // Bouton consulter
        $template_btn_consulter = '
        <span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'
            ._('Consulter')
        .'</span>
        ';
        //

        /**
         * Si il y a des dossiers à afficher, alors on affiche le widget.
         */
        // On construit le contenu du tableau
        $ct_tbody = '';
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // On construit l'attribut href du lien
            $ct_href = sprintf(
                $template_href,
                // idx
                $row["dossier"]
            );
            // On construit la ligne
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 1 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $template_btn_consulter
                ),
                // Colonne 2 - Numéro de dossier
                sprintf(
                    $template_link,
                    $ct_href,
                    $row["dossier_libelle"]
                ),
                // Colonne 3 - Date d'audience
                sprintf(
                    $template_link,
                    $ct_href,
                    $this->f->formatDate($row["ctx_date_audience"])
                )
            );
        }
        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 1 - Consulter
            '',
            // Colonne 2 - Numéro de dossier
            _('dossier'),
            // Colonne 3 - Date d'audience
            _('ctx_date_audience'),
            // Le Contenu
            $ct_tbody
        );
        // Affichage du footer
        printf(
            $this->template_footer,
            "../scr/tab.php?obj=dossier_contentieux_audience&filtre=" . $filtre,
            _("Voir +")
        );
    }


}

?>
