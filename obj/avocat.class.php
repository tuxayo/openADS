<?php
/**
 * Gestion des avodats.
 * 
 * @package openads
 * @version SVN : $Id: avocat.class.php 6565 2017-04-21 16:14:15Z softime $
 */
require_once("../obj/demandeur.class.php");


/**
 * Les avodats héritent des demandeurs.
 */
class avocat extends demandeur {

    /**
     * Constructeur.
     *
     * @param string   $id    Identifiant du avodat.
     * @param database $db    Instance de base de données.
     * @param boolean  $debug Flag de debug.
     */
    function __construct($id, $db, $debug) {
        $this->constructeur($id, $db, $debug);
    }
    
    // {{{ Gestion de la confidentialité des données spécifiques
    
    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        parent::init_class_actions();

        // ACTION - 003 - consulter
        //
        $this->class_actions[3]["condition"] = "is_user_from_allowed_collectivite";


        // ACTION - 110 - recuperer_frequent
        // Finalise l'enregistrement
        $this->class_actions[110] = array(
            "identifier" => "recuperer_frequent",
            "view" => "formulaire",
            "method" => "modifier",
            "button" => "valider",
            "permission_suffix" => "modifier",
        );
    }

    //}}}

    /**
     * Cache les champs de notification, fréquent et type_demandeur.
     *
     * @param formulaire $form Instance de la classe om_formulaire.
     * @param integer    $maj  Identifiant de l'action.
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);

        $form->setType('type_demandeur', 'hidden');
        $form->setType('notification', 'hidden');
        
        if($maj == 3) {
            // En consultation le bouton "Sauvegarder" n'a pas lieu d'être
            $form->setType('frequent','hidden');
            // Gestion de la catégorie de personne
            if ($this->getVal('qualite') == 'particulier') {
                $form->setType('personne_morale_denomination','hidden');
                $form->setType('personne_morale_raison_sociale','hidden');
                $form->setType('personne_morale_siret','hidden');
                $form->setType('personne_morale_categorie_juridique','hidden');
                $form->setType('personne_morale_nom','hidden');
                $form->setType('personne_morale_prenom','hidden');
                $form->setType('personne_morale_civilite','hidden');
            } else {
                $form->setType('particulier_civilite','hidden');
                $form->setType('particulier_nom','hidden');
                $form->setType('particulier_prenom','hidden');
                $form->setType('particulier_date_naissance','hidden');
                $form->setType('particulier_commune_naissance','hidden');
                $form->setType('particulier_departement_naissance','hidden');
            }
        }

        // Champs disabled pour la modif d'avocat frequents 
        if ($maj==110 or $maj==1) { //modifier
            if($this->getVal('frequent') == 't') {
                $form->setType('qualite','selecthiddenstatic');
                $form->setType('particulier_nom','static');
                $form->setType('particulier_prenom','static');
                $form->setType('particulier_date_naissance','datestatic');
                $form->setType('particulier_commune_naissance','static');
                $form->setType('particulier_departement_naissance','static');
                $form->setType('personne_morale_denomination','static');
                $form->setType('personne_morale_raison_sociale','static');
                $form->setType('personne_morale_siret','static');
                $form->setType('personne_morale_categorie_juridique','static');
                $form->setType('personne_morale_nom','static');
                $form->setType('personne_morale_prenom','static');
                $form->setType('numero','static');
                $form->setType('voie','static');
                $form->setType('complement','static');
                $form->setType('lieu_dit','static');
                $form->setType('localite','static');
                $form->setType('code_postal','static');
                $form->setType('bp','static');
                $form->setType('cedex','static');
                $form->setType('pays','static');
                $form->setType('division_territoriale','static');
                $form->setType('telephone_fixe','static');
                $form->setType('telephone_mobile','static');
                $form->setType('fax','static');
                $form->setType('indicatif','static');
                $form->setType('courriel','static');
                $form->setType('notification','checkboxstatic');
                $form->setType('particulier_civilite','selecthiddenstatic');
                $form->setType('personne_morale_civilite','selecthiddenstatic');
                $form->setType('om_collectivite','selecthiddenstatic');
                // on masque le bouton "Sauvegarder"
                $form->setType('frequent','hidden');
            }
        }// fin modifier
        
        $form->setType('type_demandeur', 'hidden');
    }


    /**
     * Met le champ type_demandeur à avocat par défaut.
     * 
     * @param formulaire $form       Instance de la classe om_formulaire.
     * @param integer    $maj        Identifiant de l'action.
     * @param integer    $validation Nombre de validation du formulaire.
     * @param database   $db         Instance de la classe om_database.
     */
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        if ($maj == 0 ) {
             $form->setVal("type_demandeur", "avocat");
        }
    }
    
    function setLib(&$form,$maj) {
        //libelle des champs
        parent::setLib($form, $maj);
        $form->setLib('frequent',"<span class=\"om-form-button copy-16\"
                      title=\""._("Sauvegarder cet avocat")."\">"._("Sauvegarder (avocat fréquent)")."</span>");

    }
    
    /*
     * Met le champ type_demandeur à avocat par défaut.
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValSousFormulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);
        $form->setVal("type_demandeur", "avocat");
        if ($maj == 0 ) {
            // Récupération des infos du demandeur passé en paramètre
            if ($this->getParameter('idx_demandeur') != "") {
                include '../sql/pgsql/delegataire.form.inc.php';
                $sql = "SELECT ".implode(", ", $champs)."
                        FROM ".DB_PREFIXE."demandeur ".
                        "WHERE demandeur=".$this->getParameter('idx_demandeur');
                $res = $this->db->query($sql);
                $this->f->addToLog(
                    "setValSousFormulaire() : db->query(\"".$sql."\")",
                    VERBOSE_MODE
                );
                if ( database::isError($res)) {
                    die();
                }
                $row = & $res->fetchRow(DB_FETCHMODE_ASSOC);
                foreach ($row as $key => $value) {
                    $form->setVal($key, $value);
                }
                $form->setVal("frequent", "f");
            }
        }
    }
    
    
    /**
     * Surcharge du bouton pour empécher l'utilisateur de modifier un fréquent
     * Et ajout d'un bouton pour vider le formulaire
     */
    function boutonsousformulaire($datasubmit, $maj, $val=null) {
        if($maj == 0 OR $this->getVal('frequent') != 't') {
            if (!$this->correct) {
                //
                switch ($maj) {
                    case 0:
                        $bouton = _("Ajouter");
                        break;
                    case 1:
                        $bouton = _("Modifier");
                        break;
                    case 2:
                        $bouton = _("Supprimer");
                        break;
                }
                //
                echo "<input type=\"button\" value=\"".$bouton."\" ";
                echo "onclick=\"affichersform('".get_class($this)."', '$datasubmit', this.form);\" ";
                echo "class=\"om-button\" />";
            }
        }
        if(!$this->correct) {
            echo '<span class="om-form-button erase-avocat delete-16" '. 
                        'title="Supprimer le contenu">'._("Vider le formulaire").'</span>';
        }
    }

}
