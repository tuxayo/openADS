<?php
/**
 * DOCUMENT_NUMERISE
 *
 * @package openads
 * @version SVN : $Id: document_numerise.class.php 6565 2017-04-21 16:14:15Z softime $
 */

//
require_once "../gen/obj/document_numerise.class.php";

/**
 *
 */
class document_numerise extends document_numerise_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     *
     */
    var $activate_class_action = true;

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 000 - ajouter
        // 
        $this->class_actions[0]["condition"] = array("is_ajoutable", "can_user_access_dossier_contexte_ajout");
        
        // ACTION - 001 - modifier
        // 
        $this->class_actions[1]["condition"] = array("is_modifiable", "can_user_access_dossier_contexte_modification");
        
        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = array("is_supprimable", "can_user_access_dossier_contexte_modification");

        // ACTION - 004 - view_tab
        // Interface spécifique du tableau des pièces
        $this->class_actions[4] = array(
            "identifier" => "view_tab",
            "view" => "view_tab",
            "permission_suffix" => "tab",
        );

        // ACTION - 100 - Télécharger toutes les pièces numérisées
        $this->class_actions[100] = array(
            "identifier" => "archive_doc",
            "view" => "generate_archive_doc",
             "permission_suffix" => "tab",
        );
    }

    /**
     *
     */
    //Métadonnées spécifiques
    var $metadata = array(
        "uid" => array(
            "dossier" => "getDossier",
            "dossier_version" => "getDossierVersion",
            "numDemandeAutor" => "getNumDemandeAutor",
            "anneemoisDemandeAutor" => "getAnneemoisDemandeAutor",
            "typeInstruction" => "getTypeInstruction",
            "statutAutorisation" => "getStatutAutorisation",
            "typeAutorisation" => "getTypeAutorisation",
            "dateEvenementDocument" => "getDateEvenementDocument",
            "filename" => "getFilename",
            "groupeInstruction" => 'getGroupeInstruction',
            "title" => 'getTitle',
            'consultationPublique' => 'getConsultationPublique',
            'consultationTiers' => 'getConsultationTiers',
            'concerneERP' => 'get_concerne_erp',
        ),
    );

    var $metadonneesArrete = array();
    
    var $abstract_type = array(
        "uid" => "file",
    );

    /**
     * Instance de dossier_message
     *
     * @var null
     */
    var $inst_dossier_message = null;

    /**
     * Instance de document_numerise_type
     *
     * @var null
     */
    var $inst_document_numerise_type = null;

    /**
     * Identifiant du message de notification à l'ajout d'une pièce numérisée.
     *
     * @var null
     */
    var $dossier_message_id = null;

    /**
     * VIEW - view_tab
     * 
     * Cette vue permet d'afficher l'interface spécifique du tableau
     * des pièces (liste et bouton d'ajout).
     *
     * @return void
     */
    function view_tab() {

        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        /**
         *
         */
        echo "\n\n";
        echo "\n<!-- ########## START VIEW DOCUMENT_NUMERISE ########## -->\n";
        echo "\n\n";

        //
        ($this->f->get_submitted_get_value('idxformulaire') !== null ? $idxformulaire = $this->f->get_submitted_get_value('idxformulaire') : $idxformulaire = "");
        ($this->f->get_submitted_get_value('retourformulaire') !== null ? $retourformulaire = $this->f->get_submitted_get_value('retourformulaire') : $retourformulaire = "");
        ($this->f->get_submitted_get_value('obj') !== null ? $obj = $this->f->get_submitted_get_value('obj') : $obj = "");

        /**
         * Récupération de l'identifiant du dossier
         *
         * En fonction du contexte dans lequel on se trouve, on récupère 
         * l'identifiant du dossier selon une méthode différente : 
         * - dans le contexte du DI, on récupère directement depuis les 
         *   paramètres GET passsés à la vue (idxformulaire) l'id du DI
         * - dans le contexte du DA, on récupère directement depuis les 
         *   paramètres GET passsés à la vue (idxformulaire) l'id du DA
         * - dans le contexte d'une consultation pour un service consulté
         *   (demande_avis), on récupère depuis les paramètres GET 
         *   passsés à la vue (idxformulaire) l'id de la consultation puis
         *   on fait une requête pour récupérer l'id du DI
         */

        //
        $contexte = "";
        if ($retourformulaire == "dossier_autorisation") {
            // Si on se trouve dans le contexte d'un dossier d'autorisation
            $contexte = "dossier_autorisation";
            $dossier_autorisation = $idxformulaire;
        } elseif ($retourformulaire == 'dossier'
            || $retourformulaire == 'dossier_instruction'
            || $retourformulaire == 'dossier_instruction_mes_encours'
            || $retourformulaire == 'dossier_instruction_tous_encours'
            || $retourformulaire == 'dossier_instruction_mes_clotures'
            || $retourformulaire == 'dossier_instruction_tous_clotures'
            || $retourformulaire == 'dossier_contentieux_mes_recours'
            || $retourformulaire == 'dossier_contentieux_tous_recours'
            || $retourformulaire == 'dossier_contentieux_mes_infractions'
            || $retourformulaire == 'dossier_contentieux_toutes_infractions') {
            // Si on se trouve dans le contexte d'un dossier d'instruction
            $contexte = "dossier_instruction";
            $dossier_instruction = $idxformulaire;
        } elseif ($retourformulaire == "demande_avis"
            || $retourformulaire == "demande_avis_encours"
            || $retourformulaire == "demande_avis_passee") {
            // Si on se trouve dans le contexte d'une demande d'avis
            $contexte = "demande_avis";
            $demande_avis = $idxformulaire;
            // Récupération du dossier en fonction du numéro de consultation
            $sql = "
            SELECT 
                dossier 
            FROM 
                ".DB_PREFIXE."consultation 
            WHERE 
                consultation=".$demande_avis;
            $dossier_instruction = $this->f->db->getone($sql);
            $this->f->addToLog(__METHOD__."() : db->getone(\"".$sql."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($dossier_instruction);
        }

        /**
         * Gestion des templates html
         */
        //
        $template_view = '
<div class="formEntete ui-corner-all">
<!-- Action ajouter -->
%s
%s
<!-- Liste des pièces -->
%s
</div>
';
        // ACTION AJOUTER
        $template_link_add = '
<p>
    <a id="action-soustab-blocnote-message-ajouter" onclick="ajaxIt(\'' . $obj . '\', \'../scr/sousform.php?obj=' . $obj .'&action=0&tri=&objsf=document_numerise&premiersf=0&retourformulaire='.$retourformulaire.'&idxformulaire='.$idxformulaire.'&trisf=&retour=tab\');"
       href="#">
        <span class="om-icon om-icon-16 om-icon-fix add-16" title="Ajouter">Ajouter</span>
        '._("Ajouter une pièce").'
    </a>
</p>
';
        // ACTION TÉLÉCHARGER TOUTES LES PIÈCES
        $template_link_download_zip = "
<p>
    <a id='zip_download_link' onclick='zip_doc_numerise(%s, \"%s\", \"%s\");' href='#'>
        <span class='om-icon om-icon-16 om-icon-fix archive-16'
        title='"._("Télécharger toutes les pièces")."'>"._("Télécharger toutes les pièces")."</span>
        "._("Télécharger toutes les pièces")."
    </a>
</p>
";
        // TABLE - HEAD
        // En-tête de tableau pour afficher la date et la catégorie des documents
        $template_header  = '
    <thead>
        <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
            <th class="title col-0 firstcol %s" colspan="%s">
                <span class="name">
                    %s
                </span>
            </th>
        </tr>
    </thead>
';
        // TABLE - CELL content - NOM DU FICHIER
        // Template de visualisation du nom du fichier avec accès au 
        // téléchargement de la pièce
        $template_filename_download = '
<a class="lienTable lienDocumentNumerise"
   href="../spg/file.php?obj=document_numerise&champ=uid&id=%s" 
   target="_blank"
   id="document_numerise_%s">
    <span class="om-prev-icon reqmo-16" title="'._("Télécharger").'">
        %s
    </span>
</a>
';
        // Template de visualisation du nom du fichier en lecture seule 
        // (Pas d'accès au téléchargement de la pièce)
        $template_filename_readonly = '
<p class="lienDocumentNumerise" id="document_numerise_%2$s">%3$s';
        // TABLE - CELL content - ICON
        // Template de visualisation de l'icône de l'action Consulter 
        $template_icon_view_link = '
<a onclick="ajaxIt(\'' . $obj . '\',\'../scr/sousform.php?obj=' . $obj .'&action=3&idx=%s&tri=&premier=0&recherche=&objsf=document_numerise&premiersf=0&retourformulaire='.$retourformulaire.'&idxformulaire='.$idxformulaire.'&trisf=&retour=tab\');"
   href="#">
    <span class="om-icon om-icon-16 om-icon-fix consult-16" title="Consulter">
        Consulter
    </span>
</a>
';
        // TABLE - CELL content - DOSSIER ou TYPE DE DOCUMENT
        // 
        $template_info_view_link = '
<a class="lienTable"
   onclick="ajaxIt(\'' . $obj . '\',\'../scr/sousform.php?obj=' . $obj .'&action=3&idx=%s&tri=&premier=0&recherche=&objsf=document_numerise&premiersf=0&retourformulaire='.$retourformulaire.'&idxformulaire='.$idxformulaire.'&trisf=&retour=tab\');"
   href="#">
    %s
</a>
';
        // TABLE - BODY - TR TD - STRUCTURE 3 colonnes
        $template_line_3col = '
<tr class="tab-data %s">
    <td class="icons">
        %s
    </td>
    <td class="col-0 firstcol">
        %s
    </td>
    <td class="col-1">
        %s
    </td>
</tr>
';
        // TABLE - BODY - TR TD - STRUCTURE 4 colonnes
        $template_line_4col = '
<tr class="tab-data %s">
    <td class="icons">
        %s
    </td>
    <td class="col-0 firstcol">
        %s
    </td>
    <td class="col-1">
        %s
    </td>
    <td class="col-2">
        %s
    </td>
</tr>
';

        /**
         * Récupération de la liste des pièces (document_numerise)
         * 
         * En fonction du contexte dans lequel on se trouve la requête est 
         * différente.
         */
        //
        $sql = "
        SELECT  
            document_numerise.document_numerise as document_numerise,
            document_numerise.date_creation as date_creation,
            document_numerise_type_categorie.libelle as categorie,
            document_numerise.nom_fichier as nom_fichier,
            document_numerise_type.libelle as type_document,
            document_numerise.uid as uid, 
            dossier.dossier as dossier
        FROM 
            ".DB_PREFIXE."document_numerise 
            LEFT JOIN ".DB_PREFIXE."document_numerise_type
                ON document_numerise.document_numerise_type = document_numerise_type.document_numerise_type
            LEFT JOIN ".DB_PREFIXE."document_numerise_type_categorie
                ON document_numerise_type.document_numerise_type_categorie = document_numerise_type_categorie.document_numerise_type_categorie 
            LEFT JOIN ".DB_PREFIXE."dossier 
                ON document_numerise.dossier = dossier.dossier 
            LEFT JOIN ".DB_PREFIXE."dossier_autorisation
                ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
        WHERE
        ";
        // Filtre sur le DI ou le DA en fonction du contexte
        if ($contexte == "dossier_instruction"
            || $contexte == "demande_avis") {
            $sql .= "
            document_numerise.dossier = '".$dossier_instruction."'
            ";
        } elseif ($contexte == "dossier_autorisation") {
            $sql .= "
            dossier_autorisation.dossier_autorisation = '".$dossier_autorisation."' 
            ";
        }
        // Filtre sur les types de documents lorsqu'on se trouve dans le 
        // contexte du dossier d'autorisation
        if ($contexte === 'dossier_autorisation') {
            // Instance de la classe document_numerise_type
            $inst_dnt = $this->get_inst_document_numerise_type();
            // Récupère la liste des codes dont le champ 'aff_da' est à 'true'
            $da_listing_pieces_filtre = $inst_dnt->get_code_by_filtre('aff_da');
            //
            $filtre = "";
            //
            if ($da_listing_pieces_filtre !== false) {
                // Compose la condition de la requête
                foreach ($da_listing_pieces_filtre as $filtre_code) {
                    $filtre .= " document_numerise_type.code = '".$this->f->db->escapesimple(trim($filtre_code))."' OR ";
                }
                $filtre = substr($filtre, 0, count($filtre) - 4);
            }
            //
            if ($filtre === "" || $filtre === false) {
                //
                $filtre = "document_numerise_type.code = ''";
            }
            //
            $sql .= sprintf(" AND (%s) ", $filtre);
        }

        // Filtre sur les types de documents lorsqu'on se trouve dans le 
        // contexte de la demande d'avis
        if ($contexte === 'demande_avis') {
            // Instance de la classe document_numerise_type
            $inst_dnt = $this->get_inst_document_numerise_type();
            // Récupère la liste des codes dont le champ 'aff_service_consulte' est à 'true'
            $sc_listing_pieces_filtre = $inst_dnt->get_code_by_filtre('aff_service_consulte');
            //
            $filtre = "";
            //
            if ($sc_listing_pieces_filtre !== false) {
                // Compose la condition de la requête
                foreach ($sc_listing_pieces_filtre as $filtre_code) {
                    $filtre .= " document_numerise_type.code = '".$this->f->db->escapesimple(trim($filtre_code))."' OR ";
                }
                $filtre = substr($filtre, 0, count($filtre) - 4);
            }
            //
            if ($filtre === "" || $filtre === false) {
                //
                $filtre = "document_numerise_type.code = ''";
            }
            //
            $sql .= sprintf(" AND (%s) ", $filtre);
        }
        // Le tri sur la requête
        $sql .= " 
        ORDER BY 
            document_numerise.date_creation, 
            document_numerise.nom_fichier
        ";
        // Exécution de la requête
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        /**
         * Gestion du lien vers l'ajout d'une nouvelle pièce
         *
         * Le lien d'ajout d'une nouvelle pièce est disponible uniquement dans
         * le contexte du dossier d'instruction.
         */
        //
        $ct_link_add = "";
        // 
        if ($contexte == "dossier_instruction" 
            && $this->is_action_available(0)) {
            // Affiche bouton ajouter
            $ct_link_add = $template_link_add;
        }

        /**
         * Gestion du lien vers le téléchargement de toutes les pièces dans une archive.
         *
         * Le lien est disponible dans l'onglet pièce du DA, du DI, des demandes d'avis
         * en cours et passées.
         */
        //
        $ct_link_download_zip = "";
        if (($contexte == "dossier_instruction"
            OR $contexte == "dossier_autorisation" 
            OR $contexte == "demande_avis")
            AND ($this->f->isAccredited("document_numerise_tab")
                OR $this->f->isAccredited("document_numerise"))) {
            // Identifiant du DA ou DI à passer dans le lien de l'archive zip
            if ($contexte == "dossier_autorisation") {
                $dossier = $dossier_autorisation;
            } else {
                $dossier = $dossier_instruction;
            }
            // Affichage du bouton "Télécharger toutes les pièces" et passage des messages
            // d'erreur à la fonction Javascript qui va afficher la fenêtre modale qui 
            // permet de télécharger l'archive zip.
            if($res->numRows() > 0) {
                $zip_messages = array(
                    "title" => _("Téléchargement de l'archive"),
                    "confirm_message" => _("Êtes vous sûr de vouloir télécharger l'intégralité des pièces du dossier ?"),
                    "confirm_button_ok" => _("Oui"),
                    "confirm_button_ko" => _("Non"),
                    "waiting_message" => _("Votre archive est en cours de préparation. Veuillez patienter."),
                    "download_message" => _("Votre archive est prête,"),
                    "download_link_message" => _("Cliquez ici pour la télécharger"),
                    "error_message" => _("L'archive n'a pas pu être créée. Veuillez contacter votre administrateur."),
                );
                $zip_messages_json = json_encode($zip_messages, JSON_HEX_APOS);
                // Remplacement des messages en JSON dans le template
                $ct_link_download_zip = sprintf(
                    $template_link_download_zip,
                    $zip_messages_json,
                    $dossier,
                    $obj
                );
            }
        }

        /**
         * Gestion du lien vers la fiche de visualisation de la pièce
         * 
         * Le lien vers la fiche de visualisation d'une pièce est disponible 
         * uniquement dans le contexte du dossier d'instruction.
         */
        //
        if ($contexte == "dossier_instruction" 
            && ($this->f->isAccredited("document_numerise_consulter")
                || $this->f->isAccredited("document_numerise"))
            ) {
            //
            $template_info_view = $template_info_view_link;
            $template_icon_view = $template_icon_view_link;
        } else {
            //
            $template_info_view = "";
            $template_icon_view = "";
        }

        /**
         * Gestion du lien de téléchargement de la pièce
         */
        //
        if ($this->f->isAccredited(array("document_numerise", "document_numerise_uid_telecharger"), "OR")) {
            //
            $template_filename = $template_filename_download;
        } else {
            //
            $template_filename = $template_filename_readonly;
        }

        /**
         * Gestion de l'affichage
         */
        // Si on se trouve dans le contexte du dossier d'autorisation alors le 
        // tableau affiche une colonne supplémentaire pour afficher le numéro
        // du dossier
        if ($contexte == "dossier_autorisation") {
            $nb_col = 4;
        } else {
            $nb_col = 3;
        }
        //
        $ct_list_dn = "";
        //
        $i = 1;

        //Résultat à $i - 1 pour tester la date et catégorie des documents
        $lastRes = array();
        $lastRes['date_creation'][0] = "";
        $lastRes['categorie'][0] = "";

        //Tant qu'il y a des résultats
        while( $row = &$res->fetchRow(DB_FETCHMODE_ASSOC) ) {

            $lastRes['date_creation'][$i] = $row['date_creation'];
            $lastRes['categorie'][$i] = $row['categorie'];

            //Si la date de création est différente de celle du résultat juste avant
            if ($row['date_creation'] != $lastRes['date_creation'][$i-1]) {
                //Si ce n'est pas le premier résultat on ferme la table
                if($i != 1) {
                    $ct_list_dn .= "</table>";
                } 
                //Affiche la table 
                $ct_list_dn .= "<table class='tab-tab document_numerise'>";
                //Affiche le header de la date
                $ct_list_dn .= sprintf(
                    $template_header, 
                    'headerDate',
                    $nb_col,
                    $this->f->formatDate($row['date_creation'])
                );
                //Affiche le header de la catégorie
                $ct_list_dn .= sprintf(
                    $template_header,
                    'headerCat', 
                    $nb_col,
                    $row['categorie']
                );
                //Style des lignes
                $style = 'odd';
            }
            
            //Si la date de création est identique à celle du résultat juste avant
            //et la catégorie est différente de celle du résultat juste avant
            if ($row['date_creation'] == $lastRes['date_creation'][$i-1] && $row['categorie'] != $lastRes['categorie'][$i-1]) {
                //Affiche le header de la catégorie
                $ct_list_dn .= sprintf(
                    $template_header,
                    'headerCat',
                    $nb_col,
                    $row['categorie']
                );
                //Style des lignes
                $style = 'odd';
            }

            //Si toujours dans la catégorie on change le style de la ligne
            if ($row['categorie'] == $lastRes['categorie'][$i-1] && $row['date_creation'] == $lastRes['date_creation'][$i-1]) {
                $style = ($style=='even')?'odd':'even';
            }

            //
            $style .= " col".$nb_col;

            // Si on est dans la visualisation du DA, on affiche le numéro du dossier 
            // d'instruction auquel est rataché la pièce et le nom du fichier
            if ($contexte === 'dossier_autorisation') {
                //
                $ct_list_dn .= sprintf(
                    $template_line_4col,
                    $style,
                    "",
                    $row['dossier'],
                    sprintf(
                        $template_filename,
                        $row['document_numerise'],
                        $row['document_numerise'],
                        $row['nom_fichier']
                    ),
                    $row['type_document']
                );
            } elseif ($contexte === "demande_avis") {
                //
                $ct_list_dn .= sprintf(
                    $template_line_3col,
                    $style,
                    "",
                    sprintf(
                        $template_filename,
                        $row['document_numerise'],
                        $row['document_numerise'],
                        $row['nom_fichier']
                    ),
                    $row['type_document']
                );
            } elseif ($contexte === 'dossier_instruction') {
                //
                $ct_list_dn .= sprintf(
                    $template_line_3col,
                    $style,
                    sprintf(
                        $template_icon_view, 
                        $row['document_numerise']
                    ),
                    sprintf(
                        $template_filename, 
                        $row['document_numerise'],
                        $row['document_numerise'],
                        $row['nom_fichier']
                    ),
                    sprintf(
                        $template_info_view, 
                        $row['document_numerise'],
                        $row['type_document']
                    )
                );
            }

            //
            $i++;

        }

        //On ferme la table
        $ct_list_dn .= "</table>";

        //S'il n'y a pas de résultat on affiche "Aucun enregistrement"
        if ($res->numrows() == 0) {
            //
            $ct_list_dn = "<p class='noData'>"._("Aucun enregistrement")."</p>";
        }

        /**
         *
         */
        printf(
            $template_view,
            $ct_link_add,
            $ct_link_download_zip,
            $ct_list_dn
        );

        /**
         *
         */
        echo "\n\n";
        echo "\n<!-- ########## END VIEW DOCUMENT_NUMERISE ########## -->\n";
        echo "\n\n";
    }

    function setType(&$form,$maj) {
        parent::setType($form,$maj);

        //type
        $form->setType('document_numerise','hidden');
        $form->setType('dossier','hidden');
        $form->setType('nom_fichier','hidden');

        if ($maj==0){ //ajout
            $form->setType('nom_fichier','hidden');
            if($this->retourformulaire == "") {
                $form->setType('uid','upload');
            } else {
                $form->setType('uid','upload2');
            }
        }// fin ajout

        if ($maj==1){ //modifier
            $form->setType('nom_fichier','hiddenstatic');
            if($this->retourformulaire == "") {
                $form->setType('uid','upload');
            } else {
                $form->setType('uid','upload2');
            }
        }// fin modifier

        if ($maj==2){ //supprimer
            $form->setType('uid','filestatic');
            $form->setType('date_creation','datestatic');
        }// fin supprimer

        if ($maj==3){ //consulter
            $form->setType('uid','file');
        }// fin consulter
    }

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);
        
        $this->retourformulaire = $retourformulaire;

        if($maj == 0 && 
            ($retourformulaire =='dossier' 
              || $retourformulaire =='dossier_instruction'
              || $retourformulaire=='dossier_instruction_tous_clotures'
              || $retourformulaire=='dossier_instruction_mes_clotures'
              || $retourformulaire=='dossier_instruction_tous_encours'
              || $retourformulaire=='dossier_instruction_mes_encours'
              || $retourformulaire=='dossier_contentieux_mes_infractions'
              || $retourformulaire=='dossier_contentieux_toutes_infractions'
              || $retourformulaire=='dossier_contentieux_mes_recours'
              || $retourformulaire=='dossier_contentieux_tous_recours')) {
            $form->setVal('dossier', $idxformulaire);
            $form->setVal('date_creation', date("d/m/Y"));
        }

    }// fin setValsousformulaire

    /**
     * [setLib description]
     * @param [type] $form [description]
     * @param [type] $maj  [description]
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);

        //
        $form->setLib("uid", _("fichier"));
    }


    /**
     * Méthode de traitement des données retournées par le formulaire
     */
    function setvalF($val = array()) {
        parent::setvalF($val);

        //Si le type est renseigné
        if ($val['document_numerise_type'] != "") {
          //Recupère le code du type du document
          $document_numerise_type = $this->get_document_numerise_type_code_by_id($val["document_numerise_type"]);

        } else {
          $document_numerise_type = "";
        }

        if ($val["date_creation"] == "") {
            $this->valF["date_creation"] = "";
        }

        //Génération automatique nom du fichier
        if ($val["nom_fichier"] == "" || $this->getParameter("maj") != 0) {
            $this->valF['nom_fichier'] = $this->generateFilename($this->valF["date_creation"], $document_numerise_type);
        }
    }

    /**
     * Permet de mettre à jour un champs dans la table instruction sans passer par ses triggers
     * @param   string $document_numerise Identifiant du fichier
     * @param   integer $instruction L'identifiant de l'instruction à mettre à jour
     */
    private function updateInstructionAutoExecute($document_numerise, $instruction) {

      // valeurs à mettre à jour
      $val = array("document_numerise"=>$document_numerise);
      // met à jour la table instruction sans passer par ses triggers
      $res = $this->db->autoExecute(DB_PREFIXE."instruction", $val, DB_AUTOQUERY_UPDATE,"instruction=".$instruction);
      // Exécution du traitement
      $this->f->addToLog("updateInstructionDocumentArrete() : db->autoExecute(".$res.")", VERBOSE_MODE);
      if (database::isError($res)) {
          die();
      }
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - Interface avec le référentiel ERP [113]
     * - Notification de l'instructeur par message
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {

        // On a besoin de l'instance du dossier lié au document numérisé
        $inst_di = $this->get_inst_dossier($this->valF['dossier']);

        /**
         * Interface avec le référentiel ERP.
         *
         * (WS->ERP)[113] Ajout d'une nouvelle pièce
         * Déclencheur :
         *  - L'option ERP est activée
         *  - Le dossier est marqué comme "connecté au référentiel ERP"
         *  - Une nouvelle pièce est ajoutée au dossier
         */
        //
        if ($this->f->is_option_referentiel_erp_enabled($inst_di->getVal('om_collectivite')) === true
            && $inst_di->is_connected_to_referentiel_erp() === true) {
            //
            $inst_document_numerise_type = $this->get_inst_document_numerise_type($this->valF['document_numerise_type']);

            // Vérifie que le type du document peut être partagé avec des
            // services tiers
            if ($inst_document_numerise_type->getVal('aff_service_consulte') === 't') {
                //
                $infos = array(
                    "dossier_instruction" => $inst_di->getVal($inst_di->clePrimaire),
                    "date_creation" => $this->valF["date_creation"],
                    "nom_fichier" => $this->valF["nom_fichier"],
                    "type" => $inst_document_numerise_type->getVal('libelle'),
                    "categorie" => $this->get_document_numerise_type_categorie_libelle($this->valF['document_numerise_type']),
                );
                //
                $ret = $this->f->send_message_to_referentiel_erp(113, $infos);
                if ($ret !== true) {
                    $this->cleanMessage();
                    $this->addToMessage(_("Une erreur s'est produite lors de la notification (113) du référentiel ERP. Contactez votre administrateur."));
                    return false;
                }
                $this->addToMessage(_("Notification (113) du référentiel ERP OK."));
            }
        }

        /**
         * Notification de l'instructeur par message.
         */
        // Si l'option de notification par message est activée
        if ($this->f->getParameter('option_notification_piece_numerisee') === 'true') {
            // Instancie la classe dossier_message
            $dossier_message = $this->get_inst_dossier_message(0);
            // Ajoute le message de notification
            $dossier_message_val = array();
            $dossier_message_val['dossier'] = $val['dossier'];
            $dossier_message_val['type'] = _('Ajout de pièce(s)');
            $dossier_message_val['emetteur'] = $this->f->get_connected_user_login_name();
            $dossier_message_val['login'] = $_SESSION['login'];
            $dossier_message_val['date_emission'] = date('Y-m-d H:i:s');
            $dossier_message_val['contenu'] = _('Une ou plusieurs pièces ont été ajoutées sur le dossier.');
            $add = $dossier_message->add_notification_message($dossier_message_val);
            // Si une erreur se produit pendant l'ajout
            if ($add !== true) {
                // Message d'erreur affiché à l'utilisateur
                $this->addToMessage(_("Le message de notification n'a pas pu être ajouté."));
                $this->correct = false;
                return false;
            }

            // Récupère l'identifiant du message
            if (isset($dossier_message->valF[$dossier_message->clePrimaire]) === true) {
                $this->set_dossier_message_id($dossier_message->valF[$dossier_message->clePrimaire]);
            }
        }

        //
        return true;
    }

    /**
     * Affecte une valeur à l'attribut de l'identifiant du message.
     *
     * @param integer $value Valeur à stocker dans l'attribut.
     *
     * @return void
     */
    private function set_dossier_message_id($value) {
        //
        $this->dossier_message_id = $value;
    }


    /**
     * Retourne l'identifiant du message stocké dans l'attribut.
     *
     * @return integer
     */
    private function get_dossier_message_id() {
        //
        return $this->dossier_message_id;
    }


    /**
     * Permet d’effectuer des actions avant la modification des données dans la base
     */
    function triggermodifier($id, &$db = null, $val = array(), $DEBUG = null) {

      // si le fichier n'a pas été déjà modifié
      if (stripos($val['uid'], 'tmp') === false) {
        // récupération du fichier et de ses métadonnées
        $file = $this->f->storage->get($val['uid']);
        // créé un fichier temporaire
        $tmp_file = $this->f->storage->create_temporary($file['file_content'], $file['metadata'], "from_content");
        // remplace le fichier par le temporaire pour obliger la modification du fichier
        $this->valF['uid'] = 'tmp|'.$tmp_file;
      }
    }

    /**
     * Surcharge du bouton retour des sous-formulaire
     */
    function retoursousformulaire($idxformulaire = NULL, $retourformulaire = NULL, $val = NULL,
                                  $objsf = NULL, $premiersf = NULL, $tricolsf = NULL, $validation = NULL,
                                  $idx = NULL, $maj = NULL, $retour = NULL) {

        if(($maj == 0 || $maj == 3 || $maj == 2) && ($retourformulaire === "dossier_instruction"
          || $retourformulaire==='dossier_instruction_tous_clotures'
          || $retourformulaire==='dossier_instruction_mes_clotures'
          || $retourformulaire==='dossier_instruction_tous_encours'
          || $retourformulaire==='dossier_instruction_mes_encours'
          || $retourformulaire==='dossier_contentieux_mes_infractions'
          || $retourformulaire==='dossier_contentieux_toutes_infractions'
          || $retourformulaire==='dossier_contentieux_mes_recours'
          || $retourformulaire==='dossier_contentieux_tous_recours')) {
            
            echo "\n<a class=\"retour\" ";
            echo "href=\"#\" ";
            
            echo "onclick=\"ajaxIt('".$objsf."', '"; 
            echo "../scr/sousform.php?obj=" . $objsf
                ."&idx=$idxformulaire"
                ."&action=4"
                ."&retourformulaire=$retourformulaire"
                ."&idxformulaire=$idxformulaire"
                ."&retour=form');\"";
            echo  "\" ";
            echo ">";
            //
            echo _("Retour");
            //
            echo "</a>\n";
            
        } else {
            parent::retoursousformulaire($idxformulaire, $retourformulaire, $val,
                                  $objsf, $premiersf, $tricolsf, $validation,
                                  $idx, $maj, $retour);
        }
    }

    /**
     * Ajout des contraintes spécifiques pour l'ajout d'un fichier en retour de 
     * consultation
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // On appelle la methode de la classe parent
        // parent::setSelect($form, $maj);

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }
        // Si l'utilisateur a une entrée dans la table instructeur, on affiche les types
        // de pièces ayant la valeur ajout_instructeur valant true
        if ($this->f->isUserInstructeur() !== null AND $this->f->isUserInstructeur() !== false){
            $sql_document_numerise_type = str_replace('<om_utilisateur_login>', $this->f->om_utilisateur['login'], $sql_document_numerise_type_for_user);
        }
        $this->init_select($form, $this->f->db, $maj, null, "document_numerise_type", $sql_document_numerise_type, $sql_document_numerise_type_by_id, false);
        
        //Seulement dans le cas d'un dossier d'instruction
        if($this->retourformulaire =='dossier_instruction'
              || $this->retourformulaire=='dossier_instruction_tous_clotures'
              || $this->retourformulaire=='dossier_instruction_mes_clotures'
              || $this->retourformulaire=='dossier_instruction_tous_encours'
              || $this->retourformulaire=='dossier_instruction_mes_encours'
              || $this->retourformulaire=='dossier_contentieux_mes_infractions'
              || $this->retourformulaire=='dossier_contentieux_toutes_infractions'
              || $this->retourformulaire=='dossier_contentieux_mes_recours'
              || $this->retourformulaire=='dossier_contentieux_tous_recours') {
            
            //Tableau des contraintes
            $params = array(
                "constraint" => array(
                    "extension" => ".pdf"
                ),
            );
            
            $form->setSelect("uid", $params);
        }
    }

    // {{{ 
    // Méthodes de récupération des métadonnées document
    /**
     * Récupération du numéro de dossier d'instruction à ajouter aux métadonnées
     * @return [type] [description]
     */
    protected function getDossier() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->dossier;
    }
    /**
     * Récupération la version du dossier d'instruction à ajouter aux métadonnées
     * @return int Version
     */
    protected function getDossierVersion() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->version;
    }
    /**
     * Récupération du numéro de dossier d'autorisation à ajouter aux métadonnées
     * @return [type] [description]
     */
    protected function getNumDemandeAutor() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->dossier_autorisation;
    }
    /**
     * Récupération de la date de demande initiale du dossier à ajouter aux métadonnées
     * @return [type] [description]
     */
    protected function getAnneemoisDemandeAutor() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->date_demande_initiale;
    }
    /**
     * Récupération du type de dossier d'instruction à ajouter aux métadonnées
     * @return [type] [description]
     */
    protected function getTypeInstruction() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->dossier_instruction_type;
    }
    /**
     * Récupération du statut du dossier d'autorisation à ajouter aux métadonnées
     * @return [type] [description]
     */
    protected function getStatutAutorisation() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->statut;
    }
    /**
     * Récupération du type de dossier d'autorisation à ajouter aux métadonnées
     * @return [type] [description]
     */
    protected function getTypeAutorisation() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->dossier_autorisation_type;
    }
    /**
     * Récupération nom du fichier à ajouter aux métadonnées
     * @return [type] [description]
     */
    protected function getFilename() {
        return $this->valF["nom_fichier"];
    }
    /**
     * Récupération de la date d'ajout de document à ajouter aux métadonnées
     * @return [type] [description]
     */
    protected function getDateEvenementDocument() {
        return date("Y-m-d", strtotime($this->valF["date_creation"]));
    }
    /**
     * Récupération du groupe d'instruction à ajouter aux métadonnées
     * @return string Groupe d'instruction
     */
    protected function getGroupeInstruction() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->groupe_instruction;
    }
    /**
     * Récupération du libellé du type du document à ajouter aux métadonnées
     * @return string Groupe d'instruction
     */
    protected function getTitle() {
        //Requête sql
        $sql = "SELECT libelle
                FROM ".DB_PREFIXE."document_numerise_type
                WHERE document_numerise_type = ".$this->valF["document_numerise_type"];
        $document_numerise_type_libelle = $this->db->getOne($sql);
        $this->addToLog("db->getone(\"".$sql."\");", VERBOSE_MODE);
        $this->f->addToLog("getTitle() : db->getOne(".$sql.")", VERBOSE_MODE);
        if (database::isError($document_numerise_type_libelle)) {
            die();
        }

        //Retourne le code du type de document
        return $document_numerise_type_libelle;
    }


    /**
     * Récupération du paramètre d'affichage publique du document.
     *
     * @return boolean
     */
    public function getConsultationPublique() {

        // Identifiant du type de pièce
        $dnt_id = null;
        if ($this->getParameter('maj') === 0) {
            $dnt_id = $this->valF['document_numerise_type'];
        }

        // Instance du type de la pièce
        $inst_dnt = $this->get_inst_document_numerise_type($dnt_id);

        //
        $value = $this->get_boolean_from_pgsql_value($inst_dnt->getVal('aff_da'));
        //
        if ($value === true) {
            return 'true';
        }
        //
        if ($value === false) {
            return 'false';
        }
        //
        return null;
    }


    /**
     * Récupération du paramètre d'affichage aux tiers du document.
     *
     * @return boolean
     */
    public function getConsultationTiers() {

        //  Identifiant du type de pièce
        $dnt_id = null;
        if ($this->getParameter('maj') === 0) {
            $dnt_id = $this->valF['document_numerise_type'];
        }

        // Instance du type de la pièce
        $inst_dnt = $this->get_inst_document_numerise_type($dnt_id);

        //
        $value = $this->get_boolean_from_pgsql_value($inst_dnt->getVal('aff_service_consulte'));
        //
        if ($value === true) {
            return 'true';
        }
        //
        if ($value === false) {
            return 'false';
        }
        //
        return null;
    }


    /**
     * Récupération du champ ERP du dossier d'instruction.
     *
     * @return boolean
     */
    public function get_concerne_erp() {
        //
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        //
        return $this->specificMetadata->erp;
    }


    // Fin des méthodes de récupération des métadonnées
    // }}}

    /**
     * Génère le nom du fichier avec les information du formulaire
     * @param  string $date    Date de création du document
     * @param  string $codeDoc Code du type de document
     * @return string          Nom du fichier
     */
    private function generateFilename($date, $codeDoc) {

        //Change le format de la date
        $date = date("Ymd", strtotime($date));

        //Compose le nom du fichier date + code document + extension
        $filename = $date.$codeDoc.'.pdf';
        $counter = 1;
        while ($this->filename_exists_for_dossier_instruction($filename)) {
            $filename = $date.$codeDoc.'-'.$counter.'.pdf';
            $counter++;
        }
        //Retourne le nom du fichier
        return $filename;
    }

    /**
     * Permet de vérifier si le du document numérisé est déjà utilisé dans la BDD pour le
     * dossier d'instruction courant.
     * 
     * @param  string $filename Le nom du fichier dont on veut vérifier l'existence.
     * @return bool             true si le fichier existe, sinon false.
     */
    private function filename_exists_for_dossier_instruction($filename) {

        $sql = "SELECT count(document_numerise)
                FROM ".DB_PREFIXE."document_numerise
                WHERE dossier ='".$this->valF['dossier']."'
                AND nom_fichier ='".$filename."'";
        // Exécution de la requête
        $nb_lignes = $this->f->db->getOne($sql);
        $this->addToLog(__METHOD__."() db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($nb_lignes);
        // Si le nom de fichier existe une ou plusieurs fois
        if ($nb_lignes >= 1) {
            return true;
        }
        //
        return false;
    }

    /**
     * VIEW - generate_archive_doc
     * Permet de générer une archive zip contenant les documents passés en paramètre
     * 
     * @param string $ids Identifiant des documents numérisés séparés par une virgule.
     *
     * @return uid du fichier zip ou false si erreur.
     */
    function generate_archive_doc() {
        $this->f->disableLog();

        $return = array();

        if($this->f->get_submitted_get_value('ids') === null
            || $this->f->get_submitted_get_value('dossier') === null) {
            $return['status'] = false;
        }
        $ids = $this->f->get_submitted_get_value('ids');
        // Identifiant du DI ou DA (selon contexte des pièces)
        $dossier = $this->f->get_submitted_get_value('dossier');

        // Création du storage temporaire
        $temp = "0";
        $metadata = array(
            "filename" => time().".zip",
            "size" => 1,
            "mimetype" => "application/zip",
        );

        // Création du fichier zip en filestorage temporary
        if($this->f->storage == null) {
            $return['status'] = false;
        }
        $temp_uid = $this->f->storage->create_temporary($temp, $metadata);
        if($temp_uid == OP_FAILURE) {
            $return['status'] = false;
        }
        // Récupération de son path pour ziparchive
        $temp_path = $this->f->storage->getPath_temporary($temp_uid);
        // Récupération des uid de documents
        $sql_uid = "SELECT uid FROM ".DB_PREFIXE."document_numerise WHERE document_numerise IN (".$ids.")";
        $res_uid = $this->f->db->query($sql_uid);
        if($this->f->isDatabaseError($res_uid, true) === true) {
            $return['status'] = false;
        }
        // Instanciation de l'archive
        $zip = new ZipArchive;
        $zip->open($temp_path, ZipArchive::OVERWRITE);
        // On rempli l'archive avec les documents récupérés sur le storage
        while($row = $res_uid->fetchRow(DB_FETCHMODE_ASSOC)) {
            $file = $this->f->storage->get($row['uid']);
            if($file == OP_FAILURE) {
            $return['status'] = false;
            }
            $zip->addFromString($file['metadata']['filename'], $file['file_content']);
        }
        $zip->close();

        // Création d'un second temporary pour mettre à jour les métadonnées
        $size = filesize($temp_path);
        $id_doc = explode(',', $ids);
        $file_name = $dossier.'_'.date("Ymd");
        $metadata = array(
            "filename" => $file_name.".zip",
            "size" => $size,
            "mimetype" => "application/zip",
        );
        $uid = $this->f->storage->create_temporary($temp_path, $metadata, "from_path");
        // Suppression du temporaire
        $this->f->storage->delete_temporary($temp_uid);
        $return['status'] = true;

        if($uid == OP_FAILURE) {
            $return['status'] = false;
        }
        $return['file'] = $uid;
        echo json_encode($return);
    }


    /**
     * Permet de recupérer le code du type de document par la clé primaire
     * @param  int $document_numerise_type Clé primaire d'un donnée de document_numerise_type
     * @return string                      Code du type de document 
     */
    private function get_document_numerise_type_code_by_id($document_numerise_type) {

        //Requête sql
        $sql = "SELECT code
                FROM ".DB_PREFIXE."document_numerise_type
                WHERE document_numerise_type = ".$document_numerise_type;
        $document_numerise_type_code = $this->db->getOne($sql);
        $this->addToLog("db->getone(\"".$sql."\");", VERBOSE_MODE);
        $this->f->addToLog("obj/document_numerise.class.php : db->getOne(".$sql.")", VERBOSE_MODE);
        if (database::isError($document_numerise_type_code)) {
            die();
        }

        //Retourne le code du type de document
        return $document_numerise_type_code;
    }

    /**
     * Permet de récupérer le libellé de la catégorie du type de document
     * @param  int $document_numerise_type    Clé primaire d'un donnée de document_numerise_type
     * @return string                         Libellé de la catégorie du type de document
     */
    private function get_document_numerise_type_categorie_libelle($document_numerise_type) {

      // Requête sql
      $sql = "SELECT document_numerise_type_categorie.libelle 
              FROM ".DB_PREFIXE."document_numerise_type
                LEFT JOIN ".DB_PREFIXE."document_numerise_type_categorie 
                  ON document_numerise_type.document_numerise_type_categorie = document_numerise_type_categorie.document_numerise_type_categorie
              WHERE document_numerise_type.document_numerise_type = ".$document_numerise_type;
      $document_numerise_type_categorie_libelle = $this->db->getOne($sql);
      $this->addToLog("db->getone(\"".$sql."\");", VERBOSE_MODE);
      $this->f->addToLog("obj/document_numerise.class.php : db->getOne(".$sql.")", VERBOSE_MODE);
      if (database::isError($document_numerise_type_categorie_libelle)) {
          die();
      }

      //Retourne le code du type de document
      return $document_numerise_type_categorie_libelle;
    }


    /**
     * Cette méthode permet de stocker en attribut toutes les métadonnées
     * nécessaire à l'ajout d'un document.
     */
    public function getSpecificMetadata() {

        //Requête pour récupérer les informations essentiels sur le dossier d'instruction
        $sql = "SELECT dossier.dossier as dossier,
                        dossier_autorisation.dossier_autorisation as dossier_autorisation, 
                        to_char(dossier.date_demande, 'YYYY/MM') as date_demande_initiale,
                        dossier_instruction_type.code as dossier_instruction_type, 
                        etat_dossier_autorisation.libelle as statut,
                        dossier_autorisation_type.code as dossier_autorisation_type,
                        groupe.code as groupe_instruction,
                        CASE WHEN dossier.erp IS TRUE
                            THEN 'true'
                            ELSE 'false'
                        END as erp
                FROM ".DB_PREFIXE."dossier 
                    LEFT JOIN ".DB_PREFIXE."dossier_instruction_type  
                        ON dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
                    LEFT JOIN ".DB_PREFIXE."dossier_autorisation 
                        ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation 
                    LEFT JOIN ".DB_PREFIXE."etat_dossier_autorisation
                        ON  dossier_autorisation.etat_dossier_autorisation = etat_dossier_autorisation.etat_dossier_autorisation
                    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
                        ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
                    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
                        ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
                    LEFT JOIN ".DB_PREFIXE."groupe
                        ON dossier_autorisation_type.groupe = groupe.groupe
                WHERE dossier.dossier = '".$this->valF["dossier"]."'";
        $res = $this->db->query($sql);
        $this->f->addToLog("getSpecificMetadata : db->query(".$sql.")", VERBOSE_MODE);
        if ( database::isError($res)){
            die();
        }
        
        //Le résultat est récupéré dans un objet
        $row =& $res->fetchRow(DB_FETCHMODE_OBJECT);

        //Si il y a un résultat
        if ($row !== null) {

            // Instrance de la classe dossier
            $inst_dossier = $this->get_inst_dossier($this->valF["dossier"]);

            // Insère l'attribut version à l'objet
            $row->version = $inst_dossier->get_dossier_instruction_version();

            //Alors on créé l'objet dossier_instruction
            $this->specificMetadata = $row;

        }
    }

    /**
     * CONDITION - is_ajoutable.
     *
     * Condition pour pouvoir ajouter
     *
     * @return boolean
     */
    function is_ajoutable() {
        // Test du bypass
        if ($this->f->isAccredited(get_class($this)."_ajouter_bypass")) {
            return true;
        }
        // Test des autres conditions
        return $this->is_ajoutable_or_modifiable_or_supprimable($this->getParameter("idxformulaire"));
    }

    /**
     * CONDITION - is_modifiable.
     *
     * Condition pour afficher le bouton modifier
     *
     * @return boolean
     */
    function is_modifiable() {
        // Test du bypass
        if ($this->f->isAccredited(get_class($this)."_modifier_bypass")) {
            return true;
        }
        // Test des autres conditions
        return $this->is_ajoutable_or_modifiable_or_supprimable();
    }

    /**
     * CONDITION - is_supprimable.
     *
     * Condition pour afficher le bouton supprimer
     * @return boolean
     */
    function is_supprimable() {
        // Test du bypass
        if ($this->f->isAccredited(get_class($this)."_supprimer_bypass")) {
            return true;
        }
        // Test des autres conditions
        return $this->is_ajoutable_or_modifiable_or_supprimable();
    }


    /**
     * Conditions pour afficher les boutons modifier et supprimer
     *
     * @return boolean
     */
    function is_ajoutable_or_modifiable_or_supprimable() {
        // Tester si le dossier est cloturé ,
        // et si l'instructeur est de la même division
        if ($this->is_instructeur_from_division_dossier() === true and
            $this->is_dossier_instruction_not_closed() === true){
            return true;
        }

        return false;
    }


    /**
     * Retourne le statut du dossier d'instruction
     * @param string $idx Identifiant du dossier d'instruction
     * @return string Le statut du dossier d'instruction
     */
    function getStatutAutorisationDossier($idx){
        
        $statut = '';
        
        //Si l'identifiant du dossier d'instruction fourni est correct
        if ( $idx != '' ){
            
            //On récupère le statut de l'état du dossier d'instruction à partir de 
            //l'identifiant du dossier d'instruction
            $sql = "SELECT etat.statut
                FROM ".DB_PREFIXE."dossier
                LEFT JOIN
                    ".DB_PREFIXE."etat
                    ON
                        dossier.etat = etat.etat
                WHERE dossier ='".$idx."'";
            $statut = $this->db->getOne($sql);
            $this->f->addToLog("getStatutAutorisationDossier() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            if ( database::isError($statut)){
                die();
            }
        }
        return $statut;
    }


    /**
     * Récupère l'état d'un dossier d'instruction
     * @param $idxDossier L'identifiant du dossier d'instruction
     * @return L'état du dossier d'instruction 
     */
    function getEtatDossier($idxDossier){
            
        $etat = "";
        
        $sql = "SELECT etat.etat 
            FROM ".DB_PREFIXE."etat
            LEFT JOIN
                ".DB_PREFIXE."dossier
                ON
                dossier.etat = etat.etat
            WHERE dossier.dossier = '".$idxDossier."'";
        $etat = $this->db->getOne($sql);
        $this->addToLog("getEtatDossier(): db->getOne(\"".$sql."\");", VERBOSE_MODE);
        // Si une erreur survient on die
        if (database::isError($etat, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($etat->getDebugInfo(), $etat->getMessage(), 'document_numerise');
        }
        
        return $etat;
    }


    /**
     * Instance de la demande d'avis
     *
     * @var null
     */
    var $inst_demande_avis = null;

    /**
     * Récupère l'instance de la demande d'avis
     *
     * @param integer $demande_avis Identifiant de la demande d'avis
     *
     * @return object
     */
    function get_inst_demande_avis($demande_avis = null) {
        //
        if (is_null($this->inst_demande_avis)) {
            //
            if (is_null($demande_avis)) {
                // Récupère l'identifiant du formulaire parent
                $demande_avis = $this->getParameter("idxformulaire");
            }
            // Instancie la demande d'avis
            require_once "../obj/demande_avis.class.php";
            $this->inst_demande_avis = new demande_avis($demande_avis, $this->f->db, 0);
        }
        //
        return $this->inst_demande_avis;
    }


    /**
    * Permet de recupérer l'identifiant du dossier d'instruction.
    *
    * @param integer $document_numerise Clé primaire de document_numerise.
    *
    * @return string dossier
    */
    private function get_libelle_dossier($document_numerise) {
    
        //Requête sql
        $sql = "SELECT dossier
            FROM ".DB_PREFIXE."document_numerise
            WHERE document_numerise = ".$document_numerise;
        $dossier = $this->db->getOne($sql);
        $this->addToLog("db->getone(\"".$sql."\");", VERBOSE_MODE);
        $this->f->addToLog("obj/document_numerise.class.php : db->getOne(".$sql.")", VERBOSE_MODE);
        if($this->f->isDatabaseError($dossier, true) === true) {
            return false;
        }
        
        //Retourne le code du dossier
        return $dossier;
    }


    /**
     * Récupère l'instance de dossier message.
     *
     * @param string $dossier_message Identifiant du message.
     *
     * @return object
     */
    private function get_inst_dossier_message($dossier_message = null) {
        //
        return $this->get_inst_common("dossier_message", $dossier_message);
    }


    /**
     * Récupère l'instance de document_numerise_type.
     *
     * @param string $document_numerise_type Identifiant du message.
     *
     * @return object
     */
    private function get_inst_document_numerise_type($document_numerise_type = null) {
        //
        return $this->get_inst_common("document_numerise_type", $document_numerise_type);
    }


    /**
     * Ajout d'un contenu spécifique à la fin du sous-formulaire.
     *
     * @param integer $maj État du formulaire.
     *
     * @return void
     */
    public function sousFormSpecificContent($maj) {
        // Si l'option de notification par message de l'ajout d'une pièce
        // numérisée est activée
        if ($this->f->getParameter('option_notification_piece_numerisee') === 'true') {
            //
            echo "<input id=\"dossier_message_id\" name=\"dossier_message_id\" type=\"hidden\" value=\"".$this->get_dossier_message_id()."\" />";
        }
    }

    /*
     * CONDITION - can_user_access_dossier_contexte_ajout
     *
     * Vérifie que l'utilisateur a bien accès au dossier d'instruction passé dans le
     * formulaire d'ajout.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_ajout() {

        ($this->f->get_submitted_get_value('idxformulaire') !== null ? $id_dossier = 
            $this->f->get_submitted_get_value('idxformulaire') : $id_dossier = "");
        //
        if ($id_dossier !== "") {
            require_once "../obj/dossier_instruction.class.php";
            $dossier = new dossier_instruction($id_dossier, $f->db, DEBUG);
            //
            return $dossier->can_user_access_dossier();
        }
        return false;
    }

   /*
     * CONDITION - can_user_access_dossier_contexte_modification
     *
     * Vérifie que l'utilisateur a bien accès au dossier lié au document instancié.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_modification() {

        $id_dossier = $this->getVal('dossier');
        //
        if ($id_dossier !== "" && $id_dossier !== null) {
            require_once "../obj/dossier_instruction.class.php";
            $dossier = new dossier_instruction($id_dossier, $f->db, DEBUG);
            //
            return $dossier->can_user_access_dossier();
        }
        return false;
    }

}// fin classe
?>
