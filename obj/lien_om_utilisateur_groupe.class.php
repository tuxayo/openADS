<?php
//$Id$ 
//gen openMairie le 03/11/2016 12:44

require_once "../gen/obj/lien_om_utilisateur_groupe.class.php";

class lien_om_utilisateur_groupe extends lien_om_utilisateur_groupe_gen {

    var $foreign_keys_extended = array(
        "groupe" => array("groupe", ),
        "om_utilisateur" => array("om_utilisateur", ),
    );
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);
        $this->retourformulaire = $retourformulaire;
        if ($validation == 0) {
            if ($this->is_in_context_of_foreign_key('om_utilisateur', $this->retourformulaire)) {
                $sql = "SELECT login FROM ".DB_PREFIXE."om_utilisateur WHERE om_utilisateur=".$idxformulaire;
                $login = $this->db->getone($sql);
                $form->setVal('login', $login);
            }
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        $form->setType("login", "hiddenstatic");
    }
}

?>
