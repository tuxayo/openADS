<?php
/**
 * DBFORM - 'demande_nouveau_dossier' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'demande_nouveau_dossier'.
 *
 * @package openads
 * @version SVN : $Id: demande_nouveau_dossier.class.php 5984 2016-02-19 08:41:12Z fmichon $
 */

require_once "../obj/demande.class.php";

/**
 * Définition de la classe 'demande_nouveau_dossier'.
 *
 * Cette classe permet d'interfacer l'ajout de demande concernant un nouveau
 * dossier.
 */
class demande_nouveau_dossier extends demande {

    /**
     * Surcharge du bouton retour afin de retourner sur le dashboard
     */
    function retour($premier = 0, $recherche = "", $tricol = "") {

        echo "\n<a class=\"retour\" ";
        echo "href=\"".$this->f->url_dashboard;
        //
        echo "\"";
        echo ">";
        //
        echo _("Retour");
        //
        echo "</a>\n";

    }

}

?>
