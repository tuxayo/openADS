<?php
//$Id: document_numerise_type_categorie.class.php 5839 2016-01-29 08:50:12Z fmichon $ 
//gen openMairie le 29/01/2016 09:41

require_once "../gen/obj/document_numerise_type_categorie.class.php";

class document_numerise_type_categorie extends document_numerise_type_categorie_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }


    /**
     * Permet de modifier le fil d'Ariane.
     * 
     * @param string $ent Fil d'Ariane.
     *
     * @return string
     */
    public function getFormTitle($ent) {

        // Fil d'ariane par défaut
        $ent = _("parametrage")." -> "._("Gestion des pièces")." -> "._("document_numerise_type_categorie");

        // Si différent de l'ajout
        if($this->getParameter("maj") != 0) {

            // Affiche la clé primaire
            $ent .= " -> ".$this->getVal("document_numerise_type_categorie");

            // Affiche le code
            $ent .= " ".mb_strtoupper($this->getVal("libelle"), 'UTF-8');
        }

        // Change le fil d'Ariane
        return $ent;
    }


}

?>
