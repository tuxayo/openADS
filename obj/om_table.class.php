<?php
/**
 * Ce fichier est destine a permettre la surcharge de certaines methodes de
 * la classe om_table pour des besoins specifiques de l'application
 *
 * @package openmairie_exemple
 * @version SVN : $Id: om_table.class.php 4418 2015-02-24 17:30:28Z tbenita $
 */

/**
 *
 */
require_once PATH_OPENMAIRIE."om_table.class.php";

/**
 *
 */
class om_table extends table {

    
}

?>
