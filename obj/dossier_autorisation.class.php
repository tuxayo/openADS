<?php
/**
 * DBFORM - 'dossier_autorisation' - Surcharge gen.
 *
 * Ce script permet de définir la classe 'dossier_autorisation'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../gen/obj/dossier_autorisation.class.php";

class dossier_autorisation extends dossier_autorisation_gen {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    var $valIdDemandeur = array("petitionnaire_principal" => array(),
                                "delegataire" => array(),
                                "petitionnaire" => array(),
                                "plaignant_principal" => array(),
                                "plaignant" => array(),
                                "contrevenant_principal" => array(),
                                "contrevenant" => array(),
                                "requerant_principal" => array(),
                                "requerant" => array(),
                                "avocat_principal" => array(),
                                "avocat" => array(),
                                "bailleur_principal" => array(),
                                "bailleur" => array(),
                            );

    /**
     * Instance de la classe dossier_autorisation_type.
     *
     * @var null
     */
    var $inst_dossier_autorisation_type = null;

    /**
     * Instance de la classe dossier_autorisation_type_detaille.
     *
     * @var null
     */
    var $inst_dossier_autorisation_type_detaille = null;

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();

        // ACTION - 003 - view_consulter
        // Interface spécifique du formulaire de consultation
        $this->class_actions[3] = array(
            "identifier" => "view_consulter",
            "view" => "view_consulter",
            "permission_suffix" => "consulter",
            "condition" => "is_dossier_autorisation_visible",
        );

        // ACTION - 004 - view_document_numerise
        // Interface spécifique du tableau des pièces
        $this->class_actions[4] = array(
            "identifier" => "view_document_numerise",
            "view" => "view_document_numerise",
            "permission_suffix" => "document_numerise",
        );

        // ACTION - 777 - Redirection vers la classe fille adéquate
        // 
        $this->class_actions[777] = array(
            "identifier" => "redirect",
            "view" => "redirect",
            "permission_suffix" => "consulter",
        );
    }

    /**
     * VIEW - view_document_numerise.
     *
     * Vue du tableau des pièces du dossier d'autorisation.
     *
     * Cette vue permet de gérer le contenu de l'onglet "Pièce(s)" sur un 
     * dossier d'autorisation. Cette vue spécifique est nécessaire car
     * l'ergonomie standard du framework ne prend pas en charge ce cas.
     * C'est ici la vue spécifique des pièces liées au dossier qui est
     * affichée directement au clic de l'onglet au lieu du soustab.
     * 
     * L'idée est donc de simuler l'ergonomie standard en créant un container 
     * et d'appeler la méthode javascript 'ajaxit' pour charger le contenu 
     * de la vue visualisation de l'objet lié.
     * 
     * @return void
     */
    function view_document_numerise() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Récupération des variables GET
        ($this->f->get_submitted_get_value('idxformulaire')!==null ? $idxformulaire = 
            $this->f->get_submitted_get_value('idxformulaire') : $idxformulaire = "");
        ($this->f->get_submitted_get_value('retourformulaire')!==null ? $retourformulaire = 
            $this->f->get_submitted_get_value('retourformulaire') : $retourformulaire = "");
        // Objet à charger
        $obj = "document_numerise";
        // Construction de l'url de sousformulaire à appeler
        $url = "../scr/sousform.php?obj=".$obj;
        $url .= "&idx=".$idxformulaire;
        $url .= "&action=4";
        $url .= "&retourformulaire=".$retourformulaire;
        $url .= "&idxformulaire=".$idxformulaire;
        $url .= "&retour=form";
        // Affichage du container permettant le reffraichissement du contenu
        // dans le cas des action-direct.
        printf('
            <div id="sousform-href" data-href="%s">
            </div>',
            $url
        );
        // Affichage du container permettant de charger le retour de la requête
        // ajax récupérant le sous formulaire.
        printf('
            <div id="sousform-%s">
            </div>
            <script>
            ajaxIt(\'%s\', \'%s\');
            </script>',
            $obj,
            $obj,
            $url
        );
    }


    /**
     * Affiche la fiche du dossier d'autorisation passé en paramètre.
     *
     * @param string  $idx           Identifiant du DA.
     * @param mixed   $bouton_retour Affiche ou non le bouton retour.
     * @param boolean $display_cerfa Affiche ou non l'overlay sur le CERFA.
     *
     * @return void
     */
    protected function display_dossier_autorisation_data($idx, $bouton_retour, $display_cerfa = true) {
        //Récupération des données
        //Données du dossier d'autorisation
        $sqlDonneesDA = "SELECT dossier_autorisation_libelle,
                CASE WHEN etat_dernier_dossier_instruction_accepte IS NULL
                    THEN eda.libelle
                    ELSE edda.libelle
                END as etat,
                CASE WHEN demandeur.qualite='particulier' THEN 
                    TRIM(CONCAT(civilite.code, ' ', demandeur.particulier_nom, ' ', demandeur.particulier_prenom)) 
                ELSE 
                    TRIM(CONCAT(demandeur.personne_morale_raison_sociale, ' ', demandeur.personne_morale_denomination)) 
                END as \"demandeur\", 
                CONCAT( 
                    CASE WHEN terrain_references_cadastrales IS NULL THEN 
                        ''
                    ELSE
                        TRIM(CONCAT(replace(terrain_references_cadastrales,';',' '), '<br/>')) END,
                    CASE WHEN terrain_adresse_voie_numero IS NULL AND
                        terrain_adresse_voie IS NULL THEN 
                        ''
                    ELSE
                        TRIM(CONCAT(terrain_adresse_voie_numero, ' ', terrain_adresse_voie, '<br/>')) END,
                    CASE WHEN terrain_adresse_code_postal IS NULL AND
                        terrain_adresse_localite IS NULL THEN 
                        ''
                    ELSE
                        TRIM(CONCAT(terrain_adresse_code_postal, ' ', terrain_adresse_localite, '<br/>')) END
                    ) as \"infos_localisation_terrain\",
                    to_char(depot_initial ,'DD/MM/YYYY') as \"depot_initial\",
                    to_char(date_decision ,'DD/MM/YYYY') as \"date_decision\",
                    to_char(date_validite ,'DD/MM/YYYY') as \"date_validite\",
                    to_char(date_chantier ,'DD/MM/YYYY') as \"date_chantier\",
                    to_char(date_achevement ,'DD/MM/YYYY') as \"date_achevement\",
                    dossier_autorisation_type_detaille.libelle as \"type_detaille\"      
            FROM 
                ".DB_PREFIXE."dossier_autorisation
            LEFT JOIN
                ".DB_PREFIXE."etat_dossier_autorisation as eda
                ON
                    dossier_autorisation.etat_dossier_autorisation = eda.etat_dossier_autorisation
            LEFT JOIN
                ".DB_PREFIXE."etat_dossier_autorisation as edda
                ON
                    dossier_autorisation.etat_dernier_dossier_instruction_accepte = edda.etat_dossier_autorisation
            LEFT JOIN
                ".DB_PREFIXE."lien_dossier_autorisation_demandeur
                ON
                    dossier_autorisation.dossier_autorisation = lien_dossier_autorisation_demandeur.dossier_autorisation
            LEFT JOIN
                ".DB_PREFIXE."demandeur
                ON
                    lien_dossier_autorisation_demandeur.demandeur = demandeur.demandeur AND
                    lien_dossier_autorisation_demandeur.petitionnaire_principal IS TRUE
            LEFT JOIN 
                ".DB_PREFIXE."civilite
            ON 
                civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
            LEFT JOIN
                ".DB_PREFIXE."dossier_autorisation_type_detaille
            ON
                dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
                = dossier_autorisation.dossier_autorisation_type_detaille 
            WHERE 
                dossier_autorisation.dossier_autorisation = '".$idx."'";
        $resDonneesDA = $this->f->db->query($sqlDonneesDA);
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sqlDonneesDA."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($resDonneesDA);
        $rowDonneesDA = &$resDonneesDA->fetchRow(DB_FETCHMODE_ASSOC); 
        
        // Date du DAACT et DOC
        $sqlDonneesDateDossiersDA = "SELECT 
                dossier_instruction_type.code as \"code\", 
                to_char(dossier.date_depot ,'DD/MM/YYYY') as \"date_depot\"
            FROM ".DB_PREFIXE."dossier 
            LEFT JOIN
                ".DB_PREFIXE."dossier_instruction_type
                ON 
                    dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
            WHERE 
                dossier.dossier_autorisation='".$idx."' AND 
                (
                    dossier_instruction_type.code='DAACT' OR
                    dossier_instruction_type.code='DOC'
                )
            ORDER BY code desc;";
        $resDonneesDateDossiersDA = $this->f->db->query($sqlDonneesDateDossiersDA);
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sqlDonneesDateDossiersDA."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($resDonneesDateDossiersDA);
        
        //Récupération des données principales des données techniques rattachées au DA
        $sqlPrincDonneesTechniques = "SELECT
            donnees_techniques as \"donnees_techniques\" ,
            CONCAT_WS(
                '<br/>',
                CASE WHEN co_projet_desc = '' THEN
                    NULL
                ELSE
                    TRIM(co_projet_desc)
                END,
                CASE WHEN ope_proj_desc = '' THEN
                    NULL
                ELSE
                    TRIM(ope_proj_desc)
                END,
                CASE WHEN am_projet_desc = '' THEN
                    NULL
                ELSE
                    TRIM(am_projet_desc)
                END,
                CASE WHEN dm_projet_desc = '' THEN
                    NULL
                ELSE
                    TRIM(dm_projet_desc)
                END
            ) as \"description_projet\",
            -- Si une valeur est saisie dans la deuxième version du tableau des
            -- surfaces alors on récupère seulement ses valeurs
            CASE WHEN su2_avt_shon1 IS NOT NULL
                OR su2_avt_shon2 IS NOT NULL
                OR su2_avt_shon3 IS NOT NULL
                OR su2_avt_shon4 IS NOT NULL
                OR su2_avt_shon5 IS NOT NULL
                OR su2_avt_shon6 IS NOT NULL
                OR su2_avt_shon7 IS NOT NULL
                OR su2_avt_shon8 IS NOT NULL
                OR su2_avt_shon9 IS NOT NULL
                OR su2_avt_shon10 IS NOT NULL
                OR su2_avt_shon11 IS NOT NULL
                OR su2_avt_shon12 IS NOT NULL
                OR su2_avt_shon13 IS NOT NULL
                OR su2_avt_shon14 IS NOT NULL
                OR su2_avt_shon15 IS NOT NULL
                OR su2_avt_shon16 IS NOT NULL
                OR su2_avt_shon17 IS NOT NULL
                OR su2_avt_shon18 IS NOT NULL
                OR su2_avt_shon19 IS NOT NULL
                OR su2_avt_shon20 IS NOT NULL
                OR su2_cstr_shon1 IS NOT NULL
                OR su2_cstr_shon2 IS NOT NULL
                OR su2_cstr_shon3 IS NOT NULL
                OR su2_cstr_shon4 IS NOT NULL
                OR su2_cstr_shon5 IS NOT NULL
                OR su2_cstr_shon6 IS NOT NULL
                OR su2_cstr_shon7 IS NOT NULL
                OR su2_cstr_shon8 IS NOT NULL
                OR su2_cstr_shon9 IS NOT NULL
                OR su2_cstr_shon10 IS NOT NULL
                OR su2_cstr_shon11 IS NOT NULL
                OR su2_cstr_shon12 IS NOT NULL
                OR su2_cstr_shon13 IS NOT NULL
                OR su2_cstr_shon14 IS NOT NULL
                OR su2_cstr_shon15 IS NOT NULL
                OR su2_cstr_shon16 IS NOT NULL
                OR su2_cstr_shon17 IS NOT NULL
                OR su2_cstr_shon18 IS NOT NULL
                OR su2_cstr_shon19 IS NOT NULL
                OR su2_cstr_shon20 IS NOT NULL
                OR su2_chge_shon1 IS NOT NULL
                OR su2_chge_shon2 IS NOT NULL
                OR su2_chge_shon3 IS NOT NULL
                OR su2_chge_shon4 IS NOT NULL
                OR su2_chge_shon5 IS NOT NULL
                OR su2_chge_shon6 IS NOT NULL
                OR su2_chge_shon7 IS NOT NULL
                OR su2_chge_shon8 IS NOT NULL
                OR su2_chge_shon9 IS NOT NULL
                OR su2_chge_shon10 IS NOT NULL
                OR su2_chge_shon11 IS NOT NULL
                OR su2_chge_shon12 IS NOT NULL
                OR su2_chge_shon13 IS NOT NULL
                OR su2_chge_shon14 IS NOT NULL
                OR su2_chge_shon15 IS NOT NULL
                OR su2_chge_shon16 IS NOT NULL
                OR su2_chge_shon17 IS NOT NULL
                OR su2_chge_shon18 IS NOT NULL
                OR su2_chge_shon19 IS NOT NULL
                OR su2_chge_shon20 IS NOT NULL
                OR su2_demo_shon1 IS NOT NULL
                OR su2_demo_shon2 IS NOT NULL
                OR su2_demo_shon3 IS NOT NULL
                OR su2_demo_shon4 IS NOT NULL
                OR su2_demo_shon5 IS NOT NULL
                OR su2_demo_shon6 IS NOT NULL
                OR su2_demo_shon7 IS NOT NULL
                OR su2_demo_shon8 IS NOT NULL
                OR su2_demo_shon9 IS NOT NULL
                OR su2_demo_shon10 IS NOT NULL
                OR su2_demo_shon11 IS NOT NULL
                OR su2_demo_shon12 IS NOT NULL
                OR su2_demo_shon13 IS NOT NULL
                OR su2_demo_shon14 IS NOT NULL
                OR su2_demo_shon15 IS NOT NULL
                OR su2_demo_shon16 IS NOT NULL
                OR su2_demo_shon17 IS NOT NULL
                OR su2_demo_shon18 IS NOT NULL
                OR su2_demo_shon19 IS NOT NULL
                OR su2_demo_shon20 IS NOT NULL
                OR su2_sup_shon1 IS NOT NULL
                OR su2_sup_shon2 IS NOT NULL
                OR su2_sup_shon3 IS NOT NULL
                OR su2_sup_shon4 IS NOT NULL
                OR su2_sup_shon5 IS NOT NULL
                OR su2_sup_shon6 IS NOT NULL
                OR su2_sup_shon7 IS NOT NULL
                OR su2_sup_shon8 IS NOT NULL
                OR su2_sup_shon9 IS NOT NULL
                OR su2_sup_shon10 IS NOT NULL
                OR su2_sup_shon11 IS NOT NULL
                OR su2_sup_shon12 IS NOT NULL
                OR su2_sup_shon13 IS NOT NULL
                OR su2_sup_shon14 IS NOT NULL
                OR su2_sup_shon15 IS NOT NULL
                OR su2_sup_shon16 IS NOT NULL
                OR su2_sup_shon17 IS NOT NULL
                OR su2_sup_shon18 IS NOT NULL
                OR su2_sup_shon19 IS NOT NULL
                OR su2_sup_shon20 IS NOT NULL
                THEN
                    REGEXP_REPLACE(CONCAT(
                        CASE WHEN donnees_techniques.su2_cstr_shon1 IS NULL
                            THEN ''
                            ELSE CONCAT ('Exploitation agricole - ', donnees_techniques.su2_cstr_shon1, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon2 IS NULL
                            THEN ''
                            ELSE CONCAT ('Exploitation forestière - ', donnees_techniques.su2_cstr_shon2, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon3 IS NULL
                            THEN ''
                            ELSE CONCAT ('Logement - ', donnees_techniques.su2_cstr_shon3, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon4 IS NULL
                            THEN ''
                            ELSE CONCAT ('Hébergement - ', donnees_techniques.su2_cstr_shon4, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon5 IS NULL
                            THEN ''
                            ELSE CONCAT ('Artisanat et commerce de détail - ', donnees_techniques.su2_cstr_shon5, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon6 IS NULL
                            THEN ''
                            ELSE CONCAT ('Restauration - ', donnees_techniques.su2_cstr_shon6, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon7 IS NULL
                            THEN ''
                            ELSE CONCAT ('Commerce de gros - ', donnees_techniques.su2_cstr_shon7, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon8 IS NULL
                            THEN ''
                            ELSE CONCAT ('Activités de services où s''effectue l''accueil d''une clientèle - ', donnees_techniques.su2_cstr_shon8, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon9 IS NULL
                            THEN ''
                            ELSE CONCAT ('Hébergement hôtelier et touristique - ', donnees_techniques.su2_cstr_shon9, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon10 IS NULL
                            THEN ''
                            ELSE CONCAT ('Cinéma - ', donnees_techniques.su2_cstr_shon10, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon11 IS NULL
                            THEN ''
                            ELSE CONCAT ('Locaux et bureaux accueillant du public des administrations publiques et assimilés - ', donnees_techniques.su2_cstr_shon11, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon12 IS NULL
                            THEN ''
                            ELSE CONCAT ('Locaux techniques et industriels des administrations publiques et assimilés - ', donnees_techniques.su2_cstr_shon12, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon13 IS NULL
                            THEN ''
                            ELSE CONCAT ('Établissements d''enseignement, de santé et d''action sociale - ', donnees_techniques.su2_cstr_shon13, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon14 IS NULL
                            THEN ''
                            ELSE CONCAT ('Salles d''art et de spectacles - ', donnees_techniques.su2_cstr_shon14, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon15 IS NULL
                            THEN ''
                            ELSE CONCAT ('Équipements sportifs - ', donnees_techniques.su2_cstr_shon15, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon16 IS NULL
                            THEN ''
                            ELSE CONCAT ('Autres équipements recevant du public - ', donnees_techniques.su2_cstr_shon16, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon17 IS NULL
                            THEN ''
                            ELSE CONCAT ('Industrie - ', donnees_techniques.su2_cstr_shon17, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon18 IS NULL
                            THEN ''
                            ELSE CONCAT ('Entrepôt - ', donnees_techniques.su2_cstr_shon18, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon19 IS NULL
                            THEN ''
                            ELSE CONCAT ('Bureau - ', donnees_techniques.su2_cstr_shon19, ' m² <br/>')
                        END,
                        CASE WHEN donnees_techniques.su2_cstr_shon20 IS NULL
                            THEN ''
                            ELSE CONCAT ('Centre de congrès et d''exposition - ', donnees_techniques.su2_cstr_shon20, ' m²')
                        END
                    ), ' <br/>$', '')
                ELSE
                    REGEXP_REPLACE(CONCAT(
                        CASE
                            WHEN donnees_techniques.su_cstr_shon1 IS NULL
                            THEN ''
                            ELSE CONCAT('Habitation - ', donnees_techniques.su_cstr_shon1, ' m² <br/>')
                        END,
                        CASE
                            WHEN donnees_techniques.su_cstr_shon2 IS NULL
                            THEN ''
                            ELSE CONCAT('Hébergement hôtelier - ', donnees_techniques.su_cstr_shon2, ' m² <br/>')
                        END,
                        CASE
                            WHEN donnees_techniques.su_cstr_shon3 IS NULL
                            THEN ''
                            ELSE CONCAT('Bureaux - ', donnees_techniques.su_cstr_shon3, ' m² <br/>')
                        END,
                        CASE
                            WHEN donnees_techniques.su_cstr_shon4 IS NULL
                            THEN ''
                            ELSE CONCAT('Commerce - ', donnees_techniques.su_cstr_shon4, ' m² <br/>')
                        END,
                        CASE
                            WHEN donnees_techniques.su_cstr_shon5 IS NULL
                            THEN ''
                            ELSE CONCAT('Artisanat - ', donnees_techniques.su_cstr_shon5, ' m² <br/>')
                        END,
                        CASE
                            WHEN donnees_techniques.su_cstr_shon6 IS NULL
                            THEN ''
                            ELSE CONCAT('Industrie - ', donnees_techniques.su_cstr_shon6, ' m² <br/>')
                        END,
                        CASE
                            WHEN donnees_techniques.su_cstr_shon7 IS NULL
                            THEN ''
                            ELSE CONCAT('Exploitation agricole ou forestière - ', donnees_techniques.su_cstr_shon7, ' m² <br/>')
                        END,
                        CASE
                            WHEN donnees_techniques.su_cstr_shon8 IS NULL
                            THEN ''
                            ELSE CONCAT('Entrepôt - ', donnees_techniques.su_cstr_shon8, ' m² <br/>')
                        END, 
                        CASE
                            WHEN donnees_techniques.su_cstr_shon9 IS NULL
                            THEN ''
                            ELSE CONCAT('Service public ou d''intérêt collectif - ', donnees_techniques.su_cstr_shon9, ' m²')
                        END
                    ), ' <br/>$', '')
            END as \"surface\",
            co_tot_ind_nb as \"nombre_logement_crees_individuel\",
            co_tot_coll_nb as \"nombre_logement_crees_collectif\"
            FROM
                ".DB_PREFIXE."donnees_techniques
            WHERE dossier_autorisation = '".$idx."'";
        $resPrincDonneesTechniques = $this->f->db->query($sqlPrincDonneesTechniques);
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sqlPrincDonneesTechniques."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($resPrincDonneesTechniques);
        $rowPrincDonneesTechniques = &$resPrincDonneesTechniques->fetchRow(DB_FETCHMODE_ASSOC); 
        
        //Historique des décisions du dossier d'autorisation
        $sqlDonneesDecisionsDA = "SELECT 
                avis_decision.libelle as \"avis_libelle\", 
                dossier_instruction_type.libelle as \"di_libelle\",
                civilite.code as \"code\", 
                CASE WHEN demandeur.qualite='particulier' THEN 
                    TRIM(CONCAT(demandeur.particulier_nom, ' ', demandeur.particulier_prenom)) 
                ELSE 
                    TRIM(CONCAT(demandeur.personne_morale_raison_sociale, ' ', demandeur.personne_morale_denomination)) 
                END as \"demandeur\",
                to_char(dossier.date_decision ,'DD/MM/YYYY') as \"date_decision\"
            FROM 
                ".DB_PREFIXE."dossier
            LEFT JOIN 
                ".DB_PREFIXE."dossier_instruction_type
                ON 
                    dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
            LEFT JOIN 
                ".DB_PREFIXE."lien_dossier_demandeur
                ON 
                    dossier.dossier = lien_dossier_demandeur.dossier
            LEFT JOIN 
                ".DB_PREFIXE."demandeur
                ON 
                    lien_dossier_demandeur.demandeur = demandeur.demandeur 
            LEFT JOIN 
                ".DB_PREFIXE."avis_decision
                ON 
                    dossier.avis_decision = avis_decision.avis_decision
            LEFT JOIN 
                ".DB_PREFIXE."civilite
                ON 
                    civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
            WHERE 
                dossier.dossier_autorisation = '".$idx."' AND
                dossier.avis_decision IS NOT NULL AND
                demandeur.type_demandeur = 'petitionnaire'
            ORDER BY dossier.date_decision ASC";
        $resDonneesDecisionsDA = $this->f->db->query($sqlDonneesDecisionsDA);
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sqlDonneesDecisionsDA."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($resDonneesDecisionsDA);
            
        //Les données des lots des dossiers d'autorisation
        $sqlDonneesLotsDA = "SELECT 
                lot.libelle as \"libelle\", civilite.code as \"code\", 
                CASE WHEN demandeur.qualite='particulier' THEN 
                    TRIM(CONCAT(demandeur.particulier_nom, ' ', demandeur.particulier_prenom)) 
                ELSE 
                    TRIM(CONCAT(demandeur.personne_morale_raison_sociale, ' ', demandeur.personne_morale_denomination)) 
                END as \"demandeur\"
            FROM ".DB_PREFIXE."lot
            LEFT JOIN 
                ".DB_PREFIXE."lien_lot_demandeur
                ON 
                    lot.lot = lien_lot_demandeur.lot
            LEFT JOIN 
                ".DB_PREFIXE."demandeur
                ON 
                    demandeur.demandeur = lien_lot_demandeur.demandeur
            LEFT JOIN 
                ".DB_PREFIXE."civilite
                ON 
                    civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
            WHERE lot.dossier_autorisation = '".$idx."' AND lien_lot_demandeur.petitionnaire_principal IS TRUE";
        $resDonneesLotsDA = $this->f->db->query($sqlDonneesLotsDA);
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sqlDonneesLotsDA."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($resDonneesLotsDA);
            
        //Données du dossier d'instruction en cours d'instruction
        // Informations générales du dossier d'instruction
        $sqlDonneesDI = "SELECT 
                dossier.dossier as \"dossier\", dossier.dossier_libelle as \"dossier_libelle\",
                dossier_instruction_type.libelle as \"libelle_di\",
                etat.libelle as \"etat\",
                CASE WHEN demandeur.qualite='particulier' THEN 
                    TRIM(CONCAT(civilite.code, ' ', demandeur.particulier_nom, ' ', demandeur.particulier_prenom)) 
                ELSE 
                    TRIM(CONCAT(demandeur.personne_morale_raison_sociale, ' ', demandeur.personne_morale_denomination)) 
                END as \"demandeur\"
            FROM 
                ".DB_PREFIXE."dossier
            LEFT JOIN
                ".DB_PREFIXE."etat
                ON
                    dossier.etat = etat.etat
            LEFT JOIN
                ".DB_PREFIXE."dossier_instruction_type
                ON
                    dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
            LEFT JOIN 
                ".DB_PREFIXE."lien_dossier_demandeur
                ON 
                    dossier.dossier = lien_dossier_demandeur.dossier
            LEFT JOIN
                ".DB_PREFIXE."demandeur
                ON
                    lien_dossier_demandeur.demandeur = demandeur.demandeur AND
                    lien_dossier_demandeur.petitionnaire_principal IS TRUE
            LEFT JOIN 
                ".DB_PREFIXE."civilite
            ON 
                civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
            WHERE 
                dossier.dossier_autorisation = '".$idx."' AND etat.statut = 'encours'";
        $resDonneesDI = $this->f->db->query($sqlDonneesDI);
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sqlDonneesDI."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($resDonneesDI);
        $rowDonneesDI = &$resDonneesDI->fetchRow(DB_FETCHMODE_ASSOC);
        
        //Données techniques du dossier d'instruction en cours d'instruction
        if ( $rowDonneesDI != NULL ){
            
            //Date importante du di
            $sqlDateImpDI = "SELECT
                    etat as \"etat_dossier\",  
                    CASE WHEN dossier.incomplet_notifie IS TRUE AND dossier.incompletude IS TRUE 
                        THEN to_char(dossier.date_limite_incompletude ,'DD/MM/YYYY') 
                        ELSE to_char(dossier.date_limite ,'DD/MM/YYYY')
                    END as \"date_limite\"
                FROM 
                    ".DB_PREFIXE."dossier 
                WHERE 
                    dossier.dossier_autorisation = '".$idx."' AND 
                    version = (SELECT max(version) FROM ".DB_PREFIXE."dossier WHERE
                    dossier_autorisation = '".$idx."' )";
            $resDateImpDI = $this->f->db->query($sqlDateImpDI);
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sqlDateImpDI."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($resDateImpDI);
            $rowDateImpDI = &$resDateImpDI->fetchRow(DB_FETCHMODE_ASSOC);
            
            //Récupération des données principales des données techniques rattachées au DI
            $sqlPrincDonneesTechniquesDI = "SELECT
                CONCAT_WS(
                    '<br/>',
                    CASE WHEN co_projet_desc = '' THEN
                        NULL
                    ELSE
                        TRIM(co_projet_desc)
                    END,
                    CASE WHEN ope_proj_desc = '' THEN
                        NULL
                    ELSE
                        TRIM(ope_proj_desc)
                    END,
                    CASE WHEN am_projet_desc = '' THEN
                        NULL
                    ELSE
                        TRIM(am_projet_desc)
                    END,
                    CASE WHEN dm_projet_desc = '' THEN
                        NULL
                    ELSE
                        TRIM(dm_projet_desc)
                    END
                ) as \"description_projet\",
                -- Si une valeur est saisie dans la deuxième version du tableau
                -- des surfaces alors on récupère seulement ses valeurs
                CASE WHEN su2_avt_shon1 IS NOT NULL
                    OR su2_avt_shon2 IS NOT NULL
                    OR su2_avt_shon3 IS NOT NULL
                    OR su2_avt_shon4 IS NOT NULL
                    OR su2_avt_shon5 IS NOT NULL
                    OR su2_avt_shon6 IS NOT NULL
                    OR su2_avt_shon7 IS NOT NULL
                    OR su2_avt_shon8 IS NOT NULL
                    OR su2_avt_shon9 IS NOT NULL
                    OR su2_avt_shon10 IS NOT NULL
                    OR su2_avt_shon11 IS NOT NULL
                    OR su2_avt_shon12 IS NOT NULL
                    OR su2_avt_shon13 IS NOT NULL
                    OR su2_avt_shon14 IS NOT NULL
                    OR su2_avt_shon15 IS NOT NULL
                    OR su2_avt_shon16 IS NOT NULL
                    OR su2_avt_shon17 IS NOT NULL
                    OR su2_avt_shon18 IS NOT NULL
                    OR su2_avt_shon19 IS NOT NULL
                    OR su2_avt_shon20 IS NOT NULL
                    OR su2_cstr_shon1 IS NOT NULL
                    OR su2_cstr_shon2 IS NOT NULL
                    OR su2_cstr_shon3 IS NOT NULL
                    OR su2_cstr_shon4 IS NOT NULL
                    OR su2_cstr_shon5 IS NOT NULL
                    OR su2_cstr_shon6 IS NOT NULL
                    OR su2_cstr_shon7 IS NOT NULL
                    OR su2_cstr_shon8 IS NOT NULL
                    OR su2_cstr_shon9 IS NOT NULL
                    OR su2_cstr_shon10 IS NOT NULL
                    OR su2_cstr_shon11 IS NOT NULL
                    OR su2_cstr_shon12 IS NOT NULL
                    OR su2_cstr_shon13 IS NOT NULL
                    OR su2_cstr_shon14 IS NOT NULL
                    OR su2_cstr_shon15 IS NOT NULL
                    OR su2_cstr_shon16 IS NOT NULL
                    OR su2_cstr_shon17 IS NOT NULL
                    OR su2_cstr_shon18 IS NOT NULL
                    OR su2_cstr_shon19 IS NOT NULL
                    OR su2_cstr_shon20 IS NOT NULL
                    OR su2_chge_shon1 IS NOT NULL
                    OR su2_chge_shon2 IS NOT NULL
                    OR su2_chge_shon3 IS NOT NULL
                    OR su2_chge_shon4 IS NOT NULL
                    OR su2_chge_shon5 IS NOT NULL
                    OR su2_chge_shon6 IS NOT NULL
                    OR su2_chge_shon7 IS NOT NULL
                    OR su2_chge_shon8 IS NOT NULL
                    OR su2_chge_shon9 IS NOT NULL
                    OR su2_chge_shon10 IS NOT NULL
                    OR su2_chge_shon11 IS NOT NULL
                    OR su2_chge_shon12 IS NOT NULL
                    OR su2_chge_shon13 IS NOT NULL
                    OR su2_chge_shon14 IS NOT NULL
                    OR su2_chge_shon15 IS NOT NULL
                    OR su2_chge_shon16 IS NOT NULL
                    OR su2_chge_shon17 IS NOT NULL
                    OR su2_chge_shon18 IS NOT NULL
                    OR su2_chge_shon19 IS NOT NULL
                    OR su2_chge_shon20 IS NOT NULL
                    OR su2_demo_shon1 IS NOT NULL
                    OR su2_demo_shon2 IS NOT NULL
                    OR su2_demo_shon3 IS NOT NULL
                    OR su2_demo_shon4 IS NOT NULL
                    OR su2_demo_shon5 IS NOT NULL
                    OR su2_demo_shon6 IS NOT NULL
                    OR su2_demo_shon7 IS NOT NULL
                    OR su2_demo_shon8 IS NOT NULL
                    OR su2_demo_shon9 IS NOT NULL
                    OR su2_demo_shon10 IS NOT NULL
                    OR su2_demo_shon11 IS NOT NULL
                    OR su2_demo_shon12 IS NOT NULL
                    OR su2_demo_shon13 IS NOT NULL
                    OR su2_demo_shon14 IS NOT NULL
                    OR su2_demo_shon15 IS NOT NULL
                    OR su2_demo_shon16 IS NOT NULL
                    OR su2_demo_shon17 IS NOT NULL
                    OR su2_demo_shon18 IS NOT NULL
                    OR su2_demo_shon19 IS NOT NULL
                    OR su2_demo_shon20 IS NOT NULL
                    OR su2_sup_shon1 IS NOT NULL
                    OR su2_sup_shon2 IS NOT NULL
                    OR su2_sup_shon3 IS NOT NULL
                    OR su2_sup_shon4 IS NOT NULL
                    OR su2_sup_shon5 IS NOT NULL
                    OR su2_sup_shon6 IS NOT NULL
                    OR su2_sup_shon7 IS NOT NULL
                    OR su2_sup_shon8 IS NOT NULL
                    OR su2_sup_shon9 IS NOT NULL
                    OR su2_sup_shon10 IS NOT NULL
                    OR su2_sup_shon11 IS NOT NULL
                    OR su2_sup_shon12 IS NOT NULL
                    OR su2_sup_shon13 IS NOT NULL
                    OR su2_sup_shon14 IS NOT NULL
                    OR su2_sup_shon15 IS NOT NULL
                    OR su2_sup_shon16 IS NOT NULL
                    OR su2_sup_shon17 IS NOT NULL
                    OR su2_sup_shon18 IS NOT NULL
                    OR su2_sup_shon19 IS NOT NULL
                    OR su2_sup_shon20 IS NOT NULL
                    THEN
                        REGEXP_REPLACE(CONCAT(
                            CASE WHEN donnees_techniques.su2_cstr_shon1 IS NULL
                                THEN ''
                                ELSE CONCAT ('Exploitation agricole - ', donnees_techniques.su2_cstr_shon1, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon2 IS NULL
                                THEN ''
                                ELSE CONCAT ('Exploitation forestière - ', donnees_techniques.su2_cstr_shon2, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon3 IS NULL
                                THEN ''
                                ELSE CONCAT ('Logement - ', donnees_techniques.su2_cstr_shon3, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon4 IS NULL
                                THEN ''
                                ELSE CONCAT ('Hébergement - ', donnees_techniques.su2_cstr_shon4, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon5 IS NULL
                                THEN ''
                                ELSE CONCAT ('Artisanat et commerce de détail - ', donnees_techniques.su2_cstr_shon5, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon6 IS NULL
                                THEN ''
                                ELSE CONCAT ('Restauration - ', donnees_techniques.su2_cstr_shon6, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon7 IS NULL
                                THEN ''
                                ELSE CONCAT ('Commerce de gros - ', donnees_techniques.su2_cstr_shon7, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon8 IS NULL
                                THEN ''
                                ELSE CONCAT ('Activités de services où s''effectue l''accueil d''une clientèle - ', donnees_techniques.su2_cstr_shon8, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon9 IS NULL
                                THEN ''
                                ELSE CONCAT ('Hébergement hôtelier et touristique - ', donnees_techniques.su2_cstr_shon9, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon10 IS NULL
                                THEN ''
                                ELSE CONCAT ('Cinéma - ', donnees_techniques.su2_cstr_shon10, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon11 IS NULL
                                THEN ''
                                ELSE CONCAT ('Locaux et bureaux accueillant du public des administrations publiques et assimilés - ', donnees_techniques.su2_cstr_shon11, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon12 IS NULL
                                THEN ''
                                ELSE CONCAT ('Locaux techniques et industriels des administrations publiques et assimilés - ', donnees_techniques.su2_cstr_shon12, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon13 IS NULL
                                THEN ''
                                ELSE CONCAT ('Établissements d''enseignement, de santé et d''action sociale - ', donnees_techniques.su2_cstr_shon13, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon14 IS NULL
                                THEN ''
                                ELSE CONCAT ('Salles d''art et de spectacles - ', donnees_techniques.su2_cstr_shon14, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon15 IS NULL
                                THEN ''
                                ELSE CONCAT ('Équipements sportifs - ', donnees_techniques.su2_cstr_shon15, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon16 IS NULL
                                THEN ''
                                ELSE CONCAT ('Autres équipements recevant du public - ', donnees_techniques.su2_cstr_shon16, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon17 IS NULL
                                THEN ''
                                ELSE CONCAT ('Industrie - ', donnees_techniques.su2_cstr_shon17, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon18 IS NULL
                                THEN ''
                                ELSE CONCAT ('Entrepôt - ', donnees_techniques.su2_cstr_shon18, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon19 IS NULL
                                THEN ''
                                ELSE CONCAT ('Bureau - ', donnees_techniques.su2_cstr_shon19, ' m² <br/>')
                            END,
                            CASE WHEN donnees_techniques.su2_cstr_shon20 IS NULL
                                THEN ''
                                ELSE CONCAT ('Centre de congrès et d''exposition - ', donnees_techniques.su2_cstr_shon20, ' m²')
                            END
                        ), ' <br/>$', '')
                    ELSE
                        REGEXP_REPLACE(CONCAT(
                            CASE
                                WHEN donnees_techniques.su_cstr_shon1 IS NULL
                                THEN ''
                                ELSE CONCAT('Habitation - ', donnees_techniques.su_cstr_shon1, ' m² <br/>')
                            END,
                            CASE
                                WHEN donnees_techniques.su_cstr_shon2 IS NULL
                                THEN ''
                                ELSE CONCAT('Hébergement hôtelier - ', donnees_techniques.su_cstr_shon2, ' m² <br/>')
                            END,
                            CASE
                                WHEN donnees_techniques.su_cstr_shon3 IS NULL
                                THEN ''
                                ELSE CONCAT('Bureaux - ', donnees_techniques.su_cstr_shon3, ' m² <br/>')
                            END,
                            CASE
                                WHEN donnees_techniques.su_cstr_shon4 IS NULL
                                THEN ''
                                ELSE CONCAT('Commerce - ', donnees_techniques.su_cstr_shon4, ' m² <br/>')
                            END,
                            CASE
                                WHEN donnees_techniques.su_cstr_shon5 IS NULL
                                THEN ''
                                ELSE CONCAT('Artisanat - ', donnees_techniques.su_cstr_shon5, ' m² <br/>')
                            END,
                            CASE
                                WHEN donnees_techniques.su_cstr_shon6 IS NULL
                                THEN ''
                                ELSE CONCAT('Industrie - ', donnees_techniques.su_cstr_shon6, ' m² <br/>')
                            END,
                            CASE
                                WHEN donnees_techniques.su_cstr_shon7 IS NULL
                                THEN ''
                                ELSE CONCAT('Exploitation agricole ou forestière - ', donnees_techniques.su_cstr_shon7, ' m² <br/>')
                            END,
                            CASE
                                WHEN donnees_techniques.su_cstr_shon8 IS NULL
                                THEN ''
                                ELSE CONCAT('Entrepôt - ', donnees_techniques.su_cstr_shon8, ' m² <br/>')
                            END, 
                            CASE
                                WHEN donnees_techniques.su_cstr_shon9 IS NULL
                                THEN ''
                                ELSE CONCAT('Service public ou d''intérêt collectif - ', donnees_techniques.su_cstr_shon9, ' m²')
                            END
                        ), ' <br/>$', '')
                END as \"surface\",
                co_tot_ind_nb as \"nombre_logement_crees_individuel\",
                co_tot_coll_nb as \"nombre_logement_crees_collectif\"
                FROM ".DB_PREFIXE."donnees_techniques
                LEFT JOIN ".DB_PREFIXE."dossier
                    ON donnees_techniques.dossier_instruction = dossier.dossier
                WHERE dossier.dossier = '".$rowDonneesDI['dossier']."'";
            $resPrincDonneesTechniquesDI = $this->f->db->query($sqlPrincDonneesTechniquesDI);
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sqlPrincDonneesTechniquesDI."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($resPrincDonneesTechniquesDI);
            $rowPrincDonneesTechniquesDI = &$resPrincDonneesTechniquesDI->fetchRow(DB_FETCHMODE_ASSOC); 
            
            //La liste des lots
            $sqlDonneesLotsDI = "SELECT 
                    lot.libelle as \"libelle\", civilite.code as \"code\", 
                    CASE WHEN demandeur.qualite='particulier' THEN 
                        TRIM(CONCAT(demandeur.particulier_nom, ' ', demandeur.particulier_prenom)) 
                    ELSE 
                        TRIM(CONCAT(demandeur.personne_morale_raison_sociale, ' ', demandeur.personne_morale_denomination)) 
                    END as \"demandeur\"
                FROM ".DB_PREFIXE."lot
                LEFT JOIN 
                    ".DB_PREFIXE."lien_lot_demandeur
                    ON 
                        lot.lot = lien_lot_demandeur.lot
                LEFT JOIN 
                    ".DB_PREFIXE."dossier
                    ON 
                        lot.dossier = dossier.dossier
                LEFT JOIN 
                    ".DB_PREFIXE."demandeur
                    ON 
                        demandeur.demandeur = lien_lot_demandeur.demandeur
                LEFT JOIN 
                    ".DB_PREFIXE."civilite
                    ON 
                        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
                WHERE dossier.dossier = '".$rowDonneesDI['dossier']."' AND lien_lot_demandeur.petitionnaire_principal IS TRUE";
            $resDonneesLotsDI = $this->f->db->query($sqlDonneesLotsDI);
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sqlDonneesLotsDI."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($resDonneesLotsDI);
        }

        $da_liste_lots = '';
        // Liste des lots du dossier d'autorisation
        if ($resDonneesLotsDA->numrows() > 0 ){

            // Entête de tableau
            $header = '
            <table class="tab-tab">
                <thead>
                    <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                        <th class="title col-0 firstcol">
                            <span class="name">
                            %s
                            </span>
                        </th>
                        <th class="title col-0 firstcol">
                            <span class="name">
                            %s
                            </span>
                        </th>
                    </tr>
                </thead>
            ';
            $da_liste_lots .= sprintf($header, _('Libelle'), _('Demandeur'));

            $da_liste_lots .= '<tbody>';

            while($rowDonneesLotsDA = &$resDonneesLotsDA->fetchRow(DB_FETCHMODE_ASSOC)){

                $content = '
                    <tr class="tab-data odd">
                        <td class="col-1 firstcol">
                        %s
                        </td>
                        <td class="col-1">
                        %s
                        </td>
                    </tr>
                ';
                $da_liste_lots .= sprintf($content, $rowDonneesLotsDA["libelle"], $rowDonneesLotsDA["code"]." ".$rowDonneesLotsDA["demandeur"]);
            }

            $da_liste_lots .= '</tbody></table>';
        }
        else {
            $da_liste_lots .= _("Aucun lot lie a ce dossier d'autorisation.");
        }

        //Historique des décisions
        $histo_decisions = '';
        if ($resDonneesDecisionsDA->numrows() > 0 ){

            // Entête de tableau
            $header = '
            <table class="tab-tab">
                <thead>
                    <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                        <th class="title col-0 firstcol">
                            <span class="name">
                            %s
                            </span>
                        </th>
                        <th class="title col-0 firstcol">
                            <span class="name">
                            %s
                            </span>
                        </th>
                        <th class="title col-0 firstcol">
                            <span class="name">
                            %s
                            </span>
                        </th>
                        <th class="title col-0 firstcol">
                            <span class="name">
                            %s
                            </span>
                        </th>
                    </tr>
                </thead>
            ';
            $histo_decisions .= sprintf($header, _('Decision'), _('Type de dossier'),
                _('Demandeur'), _('date_decision'));

            $histo_decisions .= '<tbody>';

            while($rowDonneesDecisionsDA = &$resDonneesDecisionsDA->fetchRow(DB_FETCHMODE_ASSOC)){

                $content = '
                    <tr class="tab-data odd">
                        <td class="col-1 firstcol">
                        %s
                        </td>
                        <td class="col-1 firstcol">
                        %s
                        </td>
                        <td class="col-1">
                        %s
                        </td>
                        <td class="col-1">
                        %s
                        </td>
                    </tr>
                    ';
                $histo_decisions .= sprintf($content, $rowDonneesDecisionsDA["avis_libelle"], 
                    $rowDonneesDecisionsDA["di_libelle"], 
                    $rowDonneesDecisionsDA["code"]." ".$rowDonneesDecisionsDA["demandeur"],
                    $rowDonneesDecisionsDA["date_decision"]);
            }

            $histo_decisions .= '</tbody></table>';
        }
        else {
            $histo_decisions .= _("Aucune decision liee a ce dossier d'autorisation.");
        }

        // Affiche le bouton des données technique
        $donnees_techniques = '';
        if ($rowPrincDonneesTechniques['donnees_techniques'] != ''
            && $display_cerfa === true
            && $this->f->isAccredited(array('donnees_techniques', 'donnees_techniques_consulter'), 'OR') === true) {

            // Toutes les données du cerfa
            $donnees_techniques = sprintf ("<a><span id=\"donnees_techniques_da\" class=\"om-prev-icon om-icon-16 om-form-button\"
            onclick=\"popupIt('donnees_techniques',
            '../scr/sousform.php?obj=donnees_techniques&action=4&idx=".$rowPrincDonneesTechniques['donnees_techniques']."'+
            '&idxformulaire=".$idx."&retourformulaire=dossier_autorisation', 860, 'auto',
            '', '');\"".
            ">%s</span></a>", _("Cliquez pour voir les donnees techniques"));
        }

        // Si un DI est en cours d'instruction
        if ($resDonneesDI->numrows() > 0 ) {

            // Liste des lots du dossier d'instruction en cours
            $liste_lots = '';
            if ($resDonneesLotsDI->numrows() > 0 ){
                
                // Entête de tableau
                $header = '
                <table class="tab-tab">
                    <thead>
                        <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                            <th class="title col-0 firstcol">
                                <span class="name">
                                %s
                                </span>
                            </th>
                            <th class="title col-0 firstcol">
                                <span class="name">
                                %s
                                </span>
                            </th>
                        </tr>
                    </thead>
                ';
                $liste_lots .= sprintf($header, _('Libelle'), _('Demandeur'));
                
                $liste_lots .= '<tbody>';
                
                while($rowDonneesLotsDI = &$resDonneesLotsDI->fetchRow(DB_FETCHMODE_ASSOC)){
                        
                    $content = '
                        <tr class="tab-data odd">
                            <td class="col-1 firstcol">
                            %s
                            </td>
                            <td class="col-1">
                            %s
                            </td>
                        </tr>
                    ';
                    $liste_lots .= sprintf($content, $rowDonneesLotsDI["libelle"], $rowDonneesLotsDI["code"]." ".$rowDonneesLotsDI["demandeur"]);
                }
                    
                $liste_lots .= '</tbody></table>';
            }
            else {
                $liste_lots .= _("Aucun lot lie a ce dossier d'instruction.");
            }
        }

        //
        if ($bouton_retour !== false) {
            //
            printf("<div class=\"formControls\">%s</div>", $bouton_retour);
        } else {
            //
            printf("<h3>%s</h3>", $rowDonneesDA["dossier_autorisation_libelle"]);
        }

        printf("<div class=\"formulaire\"><form method=\"post\" id=\"dossier_autorisation\" action=\"#\" class=\"formEntete ui-corner-all\">");

        //Le formualaire n'a pas été validé
        $validation = 1;
        //
        $champs = array("dossier_autorisation","dossier_autorisation_libelle",
            "type_detaille", "da_etat", "da_demandeur", "infos_localisation_terrain", 
            "depot_initial", "date_decision", "date_validite", 
            "date_depot_DAACT", "date_depot_DOC", "da_description_projet",
            "surface", "da_nombre_logement_crees_individuel",
            "da_nombre_logement_crees_collectif", "da_liste_lots", "histo_decisions", "donnees_techniques");

        // Si un DI est en cours d'instruction
        if ($resDonneesDI->numrows() > 0 ) {
            //
            $champs[] = "dossier";
            $champs[] = "dossier_libelle";
            $champs[] = "libelle_di";
            $champs[] = "etat";
            $champs[] = "demandeur";
            $champs[] = "date_limite";
            $champs[] = "description_projet";
            $champs[] = "surface";
            $champs[] = "nombre_logement_crees_individuel";
            $champs[] = "nombre_logement_crees_collectif";
            $champs[] = "liste_lots";
        }

        //Création d'un nouvel objet de type formulaire
        $form = new formulaire(null, $validation, 3, $champs);

        //Configuration des types de champs
        foreach ($champs as $key) {
            $form->setType($key, 'static');
        }

        //Le numéro de dossier d'instruction est caché, on affiche celui
        //qui est formatté
        $form->setType('dossier_autorisation', 'hidden');

        //
        $form->setType('da_liste_lots', 'htmlstatic');
        $form->setType('histo_decisions', 'htmlstatic');
        $form->setType('donnees_techniques', 'htmlstatic');
        
        //Configuration des libellés
        $form->setLib("dossier_autorisation", _("dossier_autorisation"));
        $form->setLib("dossier_autorisation_libelle", _("No dossier autorisation"));
        $form->setLib("type_detaille", _("Type d'autorisation"));
        $form->setLib("da_etat", _("etat"));
        $form->setLib("da_demandeur", _("Demandeur principal"));
        $form->setLib("infos_localisation_terrain", _("localisation"));
        $form->setLib("depot_initial", _("Depot initial"));
        $form->setLib("date_decision", _("Decision initiale"));
        $form->setLib("date_validite", _("Date de validite"));
        $form->setLib("date_depot_DAACT", _("Date de depot de la DAACT"));
        $form->setLib("date_depot_DOC", _("Date de depot de la DOC"));
        $form->setLib("da_description_projet", _("description_projet"));
        $form->setLib("surface", _("Surface creee"));
        $form->setLib("da_nombre_logement_crees_individuel", _("nombre_logement_crees_individuel"));
        $form->setLib("da_nombre_logement_crees_collectif", _("nombre_logement_crees_collectif"));
        $form->setLib("da_liste_lots", "");
        $form->setLib("histo_decisions", "");
        $form->setLib("donnees_techniques", "");

        //Configuration des données
        $form->setVal("dossier_autorisation", $idx);
        $form->setVal("dossier_autorisation_libelle", $rowDonneesDA["dossier_autorisation_libelle"]);
        $form->setVal("type_detaille", $rowDonneesDA["type_detaille"]);
        $form->setVal("da_etat", $rowDonneesDA["etat"]);
        $form->setVal("da_demandeur", $rowDonneesDA["demandeur"]);
        $form->setVal("infos_localisation_terrain", ($rowDonneesDA["infos_localisation_terrain"]!=="")?$rowDonneesDA["infos_localisation_terrain"]:"-");
        $form->setVal("depot_initial", ($rowDonneesDA["depot_initial"]!=="")?$rowDonneesDA["depot_initial"]:"-");
        $form->setVal("date_decision", ($rowDonneesDA["date_decision"]!=="")?$rowDonneesDA["date_decision"]:"-");
        //On met des valeurs par défaut dans ces deux champs
        $form->setVal("date_depot_DAACT", ($rowDonneesDA["date_achevement"]!=="")?$rowDonneesDA["date_achevement"]:"-");
        $form->setVal("date_depot_DOC", ($rowDonneesDA["date_chantier"]!=="")?$rowDonneesDA["date_chantier"]:"-");
        $form->setVal("da_description_projet", ($rowPrincDonneesTechniques["description_projet"]!=="")?$rowPrincDonneesTechniques["description_projet"]:"-");
        $form->setVal("surface",($rowPrincDonneesTechniques["surface"]!=="")?$rowPrincDonneesTechniques["surface"]:"-");
        $form->setVal("da_nombre_logement_crees_individuel", ($rowPrincDonneesTechniques["nombre_logement_crees_individuel"]!=="")?$rowPrincDonneesTechniques["nombre_logement_crees_individuel"]:"-");
        $form->setVal("da_nombre_logement_crees_collectif", ($rowPrincDonneesTechniques["nombre_logement_crees_collectif"]!=="")?$rowPrincDonneesTechniques["nombre_logement_crees_collectif"]:"-");
        //On met les bonnes valeurs dans les champs s'il y en a
        $val = "";
        while ($rowDonneesDateDossiersDA = &$resDonneesDateDossiersDA->fetchRow(DB_FETCHMODE_ASSOC)){
            $val = ($rowDonneesDateDossiersDA["date_depot"]!=="")?$rowDonneesDateDossiersDA["date_depot"]:"-";
            if ( strcmp($rowDonneesDateDossiersDA['code'], 'DOC') == 0 ){
                $form->setVal("date_depot_DOC", $val);
            }
            elseif(strcmp($rowDonneesDateDossiersDA['code'], 'DAACT') == 0){
                $form->setVal("date_depot_DAACT", $val);
            }
        }
        $form->setVal("date_validite", ($rowDonneesDA["date_validite"]!=="")?$rowDonneesDA["date_validite"]:"-");
        $form->setVal("da_liste_lots", $da_liste_lots);
        $form->setVal("histo_decisions", $histo_decisions);
        $form->setVal("donnees_techniques", $donnees_techniques);

        // Si un DI est en cours d'instruction
        if ($resDonneesDI->numrows() > 0 ) {
            //Le numéro de dossier d'instruction est caché, on affiche 
            //celui qui est formatté
            $form->setType('dossier', 'hidden');

            //
            $form->setType('liste_lots', 'htmlstatic');

            //Configuration des libellés
            $form->setLib("dossier", _("dossier"));
            $form->setLib("dossier_libelle", _("dossier_libelle"));
            $form->setLib("libelle_di", _("libelle_di"));
            $form->setLib("etat", _("etat"));
            $form->setLib("demandeur", _("Demandeur principal"));
            $form->setLib("date_limite", _("Date limite"));
            $form->setLib("description_projet", _("description_projet"));
            $form->setLib("surface", _("Surface creee"));
            $form->setLib("nombre_logement_crees_individuel", _("nombre_logement_crees_individuel"));
            $form->setLib("nombre_logement_crees_collectif", _("nombre_logement_crees_collectif"));
            $form->setLib("liste_lots", "");

            //Configuration des données
            $form->setVal("dossier", $rowDonneesDI["dossier"]);
            $form->setVal("dossier_libelle", $rowDonneesDI["dossier_libelle"]);
            $form->setVal("libelle_di", $rowDonneesDI["libelle_di"]);
            $form->setVal("etat", $rowDonneesDI["etat"]);
            $form->setVal("demandeur", $rowDonneesDI["demandeur"]);
            $form->setVal("date_limite", $rowDateImpDI["date_limite"]);
            $form->setVal("description_projet", ($rowPrincDonneesTechniquesDI["description_projet"]!=="")?$rowPrincDonneesTechniquesDI["description_projet"]:"-");
            $form->setVal("surface",($rowPrincDonneesTechniquesDI["surface"]!=="")?$rowPrincDonneesTechniquesDI["surface"]:"-");
            $form->setVal("nombre_logement_crees_individuel", ($rowPrincDonneesTechniquesDI["nombre_logement_crees_individuel"]!=="")?$rowPrincDonneesTechniquesDI["nombre_logement_crees_individuel"]:"-");
            $form->setVal("nombre_logement_crees_collectif", ($rowPrincDonneesTechniquesDI["nombre_logement_crees_collectif"]!=="")?$rowPrincDonneesTechniquesDI["nombre_logement_crees_collectif"]:"-");
            $form->setVal("liste_lots", $liste_lots);
        }

        //
        $form->setBloc("dossier_autorisation", "D", "", "col_6");

            // Fieldset des dossiers en cours de validité
            $form->setFieldset("dossier_autorisation", "D", _("En cours de validite"), "");

                //Données générales
                $form->setBloc("dossier_autorisation", "D", _("Donnees generales"), "col_12");;
                $form->setBloc("infos_localisation_terrain", "F");

                //Dates importantes
                $form->setBloc("depot_initial", "D", _("Dates importantes"), "col_12");
                $form->setBloc("date_depot_DOC", "F");

                //Principales données techniques
                $form->setBloc("da_description_projet", "D", _("Principales donnees techniques"), "col_12");
                $form->setBloc("da_nombre_logement_crees_collectif", "F");

                //
                $form->setBloc("da_liste_lots", "DF", _("Liste des lots"), "col_12");

                //
                $form->setBloc("histo_decisions", "DF", _("Historique des decisions"), "col_12");

                if ($rowPrincDonneesTechniques['donnees_techniques'] != ''
                    && $display_cerfa === true
                    && $this->f->isAccredited(array('donnees_techniques', 'donnees_techniques_consulter'), 'OR') === true) {
                    //
                    $form->setBloc("donnees_techniques", "DF", _("Toutes les donnees du CERFA"), "col_12");
                    // Ferme le fieldset sur ce champ
                    $form->setFieldset("donnees_techniques", "F", "");
                    //
                    $form->setBloc("donnees_techniques", "F");
                } else {
                    // Sinon ferme le fieldset sur le champ précédent
                    $form->setFieldset("histo_decisions", "F", "");
                    //
                    $form->setBloc("histo_decisions", "F");
                }

        // Si un DI est en cours d'instruction
        if ($resDonneesDI->numrows() > 0 ) {

            //
            $form->setBloc("dossier", "D", "", "col_6");

                //
                $form->setFieldset("dossier", "D", _("En cours d'instruction"), " ");

                    //Données générales
                    $form->setBloc("dossier", "D", _("Donnees generales"), "col_12");
                    $form->setBloc("demandeur", "F");

                    //Si statut dossier différent d'incomplet
                    if ($rowDateImpDI["etat_dossier"] != 'incomplet'
                        && $rowDateImpDI["etat_dossier"] != 'incomplet_notifie') {

                        //Dates importantes 
                        $form->setBloc("date_limite", "D", _("Dates importantes"), "col_12");
                        $form->setBloc("date_limite", "F");
                    }

                //Principales données techniques
                $form->setBloc("description_projet", "D", _("Principales données techniques"), "col_12");
                $form->setBloc("nombre_logement_crees_collectif", "F");

                //
                $form->setBloc("liste_lots", "DF", _("Liste des lots"), "col_12");

                //
                $form->setFieldset("liste_lots", "F", "");

            //
            $form->setBloc("liste_lots", "F");
        }

        //
        $form->afficher($champs, $validation, false, false);

        printf("</form>");

        if ($bouton_retour !== false) {
            //
            printf("%s</div>",$bouton_retour);
        }
    }


    /**
     * VIEW - view_consulter
     * 
     * Cette vue permet d'afficher l'interface spécifique de consultation
     * des dossiers d'autorisation.
     *
     * @return void
     */
    function view_consulter() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        $idx = $this->f->get_submitted_get_value('idx');
        $idz = $this->f->get_submitted_get_value('idz');
        $retour = $this->f->get_submitted_get_value('retour');
        $premier = $this->f->get_submitted_get_value('premier');
        $advs_id = $this->f->get_submitted_get_value('advs_id');
        $recherche = $this->f->get_submitted_get_value('recherche');
        $tricol = $this->f->get_submitted_get_value('tricol');
        $selectioncol = $this->f->get_submitted_get_value('selectioncol');
        $valide = $this->f->get_submitted_get_value('valide');
        $retourformulaire = $this->f->get_submitted_get_value('retourformulaire');
        
        $bouton_retour = "<a class=\"retour\" 
            href=\"../scr/tab.php?obj=dossier_autorisation&premier=".$premier."&tricol=".$tricol.
            "&recherche=".$recherche."&selectioncol=".$selectioncol."&retourformulaire=".$retourformulaire.
            "&advs_id=".$advs_id."\">"._("Retour")."</a>";
        // Si l'identifiant du dossier d'autorisation a été fourni
        if (!is_null($idx)) {
            // Affiche la fiche complète du dossier d'autorisation
            $this->display_dossier_autorisation_data($idx, $bouton_retour);
        }
    }


    /**
     * Affiche la fiche du dossier d'autorisation pour les utilisateurs anonymes.
     *
     * @param boolean $content_only Affiche le contenu seulement.
     *
     * @return void
     */
    public function view_consulter_anonym($content_only = false) {

        // Par défaut on considère qu'on va afficher le formulaire
        $idx = 0;
        // Flag d'erreur
        $error = false;
        // Message d'erreur
        $message = '';

        // Paramètres POST
        $validation = $this->f->get_submitted_post_value('validation');
        //
        $dossier = $this->f->get_submitted_post_value('dossier');
        $dossier = $this->f->db->escapeSimple($dossier);
        $dossier = preg_replace('/\s+/', '', $dossier);
        //
        $cle_acces_citoyen = $this->f->get_submitted_post_value('cle_acces_citoyen_complete');
        $cle_acces_citoyen = $this->f->db->escapeSimple($cle_acces_citoyen);
        //
        $timestamp_generation_formulaire = $this->f->get_submitted_post_value('timestamp_generation_formulaire');

        // Si au moins un des champs n'est pas renseignés
        if ($error !== true
            && $validation !== null
            && (($dossier === null || $dossier == '')
                || ($cle_acces_citoyen === null || $cle_acces_citoyen == ''))) {
            //
            $message = _("Tous les champs doivent etre remplis.");
            $error = true;
        }

        // Si le formulaire est expiré
        if ($error !== true
            && $validation !== null
            && time() >= strtotime('+5 minutes', $timestamp_generation_formulaire)) {
            //
            $message = _("Le formulaire a expire. Veuillez recharger la page.");
            $error = true;
        }

        // Si les valeurs renseignées semblent correctes
        if ($error !== true
            && $validation !== null
            && (strlen($dossier) < 15 || strlen($cle_acces_citoyen) != 19)) {
            //
            $message = _("Le numero de dossier ou la cle d'acces n'est pas valide.");
            $error = true;
        }

        // S'il n'y a pas eu d'erreur
        if ($error !== true
            && $validation !== null) {
            // Vérifie le couple numéro de dossier et clé d'accès citoyen
            $idx = $this->verify_citizen_access_portal_credentials($dossier, $cle_acces_citoyen);

            // Si le couple n'est pas correct
            if ($idx === false) {
                //
                $message = _("Le numero de dossier ou la cle d'acces n'est pas valide.");
                $error = true;
            }
        }

        // S'il n'y a pas d'erreur et que le formulaire a été validé
        if ($error !== true && $validation !== null) {
            // On affiche la fiche d'information du dossier d'autorisation
            $this->display_dossier_autorisation_data($idx, false, false);
        } else {
            // Sinon on affiche le formulaire d'accès au portail citoyen
            $this->display_citizen_access_portal_form($message, $content_only);
        }
    }


    /**
     * Vérifie le couple dossier/clé d'accès dans la base de données.
     *
     * @param string $dossier            Le numéro du DI ou DA, sans espaces.
     * @param string $citizen_access_key La clé d'accès.
     *
     * @return string Identifiant du DA sinon 0.
     */
    public function verify_citizen_access_portal_credentials($dossier, $citizen_access_key) {

        //
        $numero_da = "SELECT dossier_autorisation.dossier_autorisation
        FROM ".DB_PREFIXE."dossier LEFT JOIN ".DB_PREFIXE."dossier_autorisation ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
        WHERE (dossier.dossier = '".$dossier."' OR
            dossier_autorisation.dossier_autorisation = '".$dossier."') AND
                dossier_autorisation.cle_acces_citoyen = '".$citizen_access_key."'";
        $resDossier = $this->f->db->getOne($numero_da);
        $this->f->addToLog(__METHOD__."() : db->query(\"".$numero_da."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($resDossier);

        // Si aucun dossier n'existe avec la clé fournie, on renvoie faux
        if ($resDossier == null) {
            return false;
        }
        //
        return $resDossier;
    }


    /**
     * Affiche le formulaire d'accès au portail citoyen.
     *
     * @param string  $message      Message d'erreur.
     * @param boolean $content_only Affiche le contenu seulement.
     *
     * @return void
     */
    public function display_citizen_access_portal_form($message, $content_only) {

        // Ajoute le paramètre content_only à l'url permettant de ne pas afficher
        // le header et le footer
        $param_get_content_only = '';
        if ($content_only === true) {
            $param_get_content_only = '?content_only=true';
        }

        // Affichage du message d'erreur
        if (isset($message) && $message != "") {
            printf('<div class="alert alert-danger" role="alert">%s</div>', $message);
        }

        // Ouverture du formulaire
        printf("<div class=\"formulaire\"><form class=\"form-signin\" method=\"POST\" id=\"acces_portail_citoyen\" action=\"citizen.php%s\">", $param_get_content_only);

        $champs = array('dossier', 'cle_acces_citoyen_split', 'timestamp_generation_formulaire', 'cle_acces_citoyen_complete');

        //
        require_once "../obj/om_formulaire.class.php";
        $form = new om_formulaire(null, 0, 0, $champs);
        $form->setType('dossier', 'text');
        $form->setType('cle_acces_citoyen_split', 'citizen_access_key');
        $form->setType('cle_acces_citoyen_complete', 'hidden');
        $form->setType('timestamp_generation_formulaire', 'hidden');

        $form->setTaille("dossier", 30);
        $form->setTaille('cle_acces_citoyen_complete', 19);
        $form->setTaille('timestamp_generation_formulaire', 20);

        $form->setMax("dossier", 30);
        $form->setMax('cle_acces_citoyen_complete', 19);
        $form->setMax('timestamp_generation_formulaire', 20);

        $form->setLib('dossier', _('N° de dossier'));
        $form->setLib('cle_acces_citoyen_split', _('cle_acces'));
        $form->setLib('cle_acces_citoyen_complete', '');
        $form->setLib('timestamp_generation_formulaire', '');

        $form->setVal('timestamp_generation_formulaire', time());

        $form->setBloc("dossier", "D", "", "group");
        $form->setBloc("cle_acces_citoyen_split", "F");
        $form->afficher($champs, 0, false, false);

        // Bouton de validation
        echo "<div class=\"formControls\">";
        echo "<input type=\"submit\" class=\"btn btn-lg btn-primary btn-block\" value=\""._("Valider")."\" name=\"validation\" />";
        echo "</div>";
        printf("</form>");

        // Fermeture du div formulaire
        printf("</div>");

    }


    /**
     *  Assure que la date passee par reference soit en
     *  format attendu par la fonction dateDB du fichier 
     *  core/om_dbform.class.php. Change le format de la
     *  date si necessaire.
     *  @param $string $field Le date dans format DB, ou
     *  celui attendu par setvalF
     */
    private function changeDateFormat(&$field) {
        if (preg_match('/([0-9]{4})-([0-9]{2})-([0-9]{2})/',
                    $field, $matches)) {
            $field = $matches[3].'/'.$matches[2].'/'.$matches[1];
        }        
    }
    
    function setvalF($val = array()) {
        // verifie que les dates envoyes au parent::setvalF sont ont
        // bon format, et change le format si necessaire
        $this->changeDateFormat($val['erp_date_ouverture']);
        $this->changeDateFormat($val['erp_date_arrete_decision']);
        
        parent::setvalF($val);
        
        // si la valeur d'erp_arrete_decision n'etait pas set, laisse elle a null
        if ($val['erp_arrete_decision'] == null) {
            $this->valF['erp_arrete_decision'] = null;
        }
        // si la valeur d'erp_ouvert n'etait pas set, laisse elle a null
        if ($val['erp_ouvert'] == null) {
            $this->valF['erp_ouvert'] = null;
        }
    }

    /**
     * Méthode permettant de définir des valeurs à envoyer en base après
     * validation du formulaire d'ajout.
     * @param array $val tableau des valeurs retournées par le formulaire
     */
    function setValFAjout($val = array()) {

        // On récupère les paramètres de la collectivité concernée
        // par la demande.
        $collectivite_parameters = $this->f->getCollectivite($this->valF['om_collectivite']);

        // Le paramètre 'departement' est obligatoire si il n'est pas présent
        // dans le tableau des paramètres alors on stoppe le traitement.
        if (!isset($collectivite_parameters['departement'])) {
            $this->f->addToLog(
              __METHOD__."(): ERROR om_parametre 'departement' inexistant.",
                DEBUG_MODE
            );
            return false;
        }
        $departement = $collectivite_parameters['departement'];

        // Le paramètre 'commune' est obligatoire si il n'est pas présent
        // dans le tableau des paramètres alors on stoppe le traitement.
        if (!isset($collectivite_parameters['commune'])) {
            $this->f->addToLog(
              __METHOD__."(): ERROR om_parametre 'commune' inexistant.",
                DEBUG_MODE
            );
            return false;
        }
        $commune = $collectivite_parameters['commune'];

        // Récupération du type de dossier ou série
        $sql = "SELECT da_t.code
        FROM ".DB_PREFIXE."dossier_autorisation_type as da_t
        INNER JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille as da_t_d
        ON da_t.dossier_autorisation_type=da_t_d.dossier_autorisation_type
        WHERE da_t_d.dossier_autorisation_type_detaille=".
        $val['dossier_autorisation_type_detaille'].";";
        //
        $da_type_code = $this->db->getone($sql);
        $this->addToLog("setValFAjout(): db->getone(\"".$sql."\")", VERBOSE_MODE);
        database::isError($da_type_code);

        //
        $annee = date('y', strtotime($this->valF["depot_initial"]));

        // Récupération de la division de l'instructeur
        $division_instructeur = $this->get_instructeur_division_for_numero_dossier();
        // Récupération du numéro du dossier
        $numero_dossier = $this->createNumeroDossier(
            $da_type_code,
            $annee,
            $departement,
            $commune
        );
        if($numero_dossier === false) {
            return false;
        }

        /// IDENTIFIANT DU DOSSIER
        // PC 013 055 12 00001       
        $this->valF[$this->clePrimaire] =
            $da_type_code.$departement.$commune.$annee.$division_instructeur.$numero_dossier;

        // Identifiant du dossier d'autorisation lisible
        // Ex : DP 013055 13 00002
        $this->valF["dossier_autorisation_libelle"] = 
            $da_type_code." ".$departement.$commune." ".$annee." ".$division_instructeur.$numero_dossier;

    }

    /**
     * Récupère le code de la division pour le numéro du dossier.
     * Par défaut retourne 0.
     *
     * @return mixed Division de l'instructeur ou 0
     */
    function get_instructeur_division_for_numero_dossier() {

        // Récupère le paramètre numero_dossier_division_instructeur
        $option = $this->f->getParameter("option_instructeur_division_numero_dossier");

        // Si l'option n'est pas activée
        if ($option != 'true') {

            // Retourne la valeur par défaut 0
            return 0;
        }

        // Instancie la classe dossier pour utiliser les fonctions de
        // récupération de l'instructeur automatiquement
        require_once '../obj/dossier.class.php';
        $dossier = new dossier(0, $this->db, false);

        // Récupère le quartier et l'arrondissement depuis les ref cadastrales
        $quartierArrondissement = $dossier->getQuartierArrondissement($this->valF['terrain_references_cadastrales']);
        //
        $quartier = null;
        $arrondissement = null;
        if ($quartierArrondissement != NULL) {
            //
            $quartier = $quartierArrondissement['quartier'];
            $arrondissement = $quartierArrondissement['arrondissement'];
        }
        // Récupère la section depuis les ref cadastrales
        $section = $dossier->getSection($this->valF['terrain_references_cadastrales']);

        // Récupère l'instructeur et la division qui seront affectés
        // automatiquement
        $instructeurDivision = $dossier->getInstructeurDivision($quartier, $arrondissement, $section, $this->valF['dossier_autorisation_type_detaille'], $this->valF['om_collectivite']);

        // Si aucun instructeur est affecté automatiquement
        if ($instructeurDivision == null) {

            // Retourne la valeur par défaut 0
            return 0;
        }

        // Récupère la division
        $division = $instructeurDivision['division'];

        // Récupère le code de la division
        require_once '../obj/division.class.php';
        $division_instance = new division($division, $this->db, false);
        $division_code = $division_instance->getVal("code");

        // Retourne le libellé de la division
        return $division_code;
    }

    // {{{
    // getter
    function getValIdDemandeur() {
        return $this->valIdDemandeur;
    }
    // setter
    function setValIdDemandeur($valIdDemandeur) {
        $this->valIdDemandeur = $valIdDemandeur;
    }
    // }}}
    
    /**
     * Retourne le numéro suivant de la séquence des DA identifiées par les
     * quatre paramètres (PC, 15, 013, 055).
     *
     *
     * @param string $datc  Code du type de dossier d'autorisation.
     * @param string $annee Année de la date de dépôt initial.
     * @param string $dep   Code département.
     * @param string $com   Code commune.
     *
     * @return string numéro de dossier ou false.
     */
    function createNumeroDossier($datc, $annee, $dep, $com) {
        /**
         * On vérifie la validité des trois paramètres.
         */
        // Vérification du code du type de dossier d'autorisation.
        if (!is_string($datc)) {
            // Logger
            $this->addToLog(
                __METHOD__.'(): parametre datc = '.var_export($datc, true),
                DEBUG_MODE
            );
            // Stop le traitement
            return false;
        }
        // Vérification du code département.
        if ($dep == null
            || !is_numeric($dep)
            || intval($dep) == 0
            || count($dep) > 3) {
            // Logger
            $this->addToLog(
                __METHOD__.'(): om_parametre departement = '.var_export($dep, true),
                DEBUG_MODE
            );
            // Stop le traitement
            return false;
        }
        // Vérification du code commune.
        if ($com == null
            || !is_numeric($com)
            || intval($com) == 0
            || count($com) > 3) {
            // Logger
            $this->addToLog(
                __METHOD__.'(): om_parametre commune = '.var_export($com, true),
                DEBUG_MODE
            );
            // Stop le traitement
            return false;
        }

        /**
         * On compose les identifiants qui composent la séquence.
         */
        // Clé unique.
        // Exemple : pc_15_013_055
        $unique_key = sprintf('%s_%s_%s_%s', strtolower($datc), $annee, $dep, $com);
        // Nom de la table représentant la séquence pour appel via la méthode
        // database::nextId() qui prend un nom de séquence sans son suffixe
        // '_seq'.
        // Exemple : openads.dossier_pc_15_013_055
        $table_name = sprintf('%sdossier_%s', DB_PREFIXE, $unique_key);
        // Nom de la séquence avec son suffixe 'seq'.
        // Exemple : openads.dossier_pc_15_013_055_seq
        $sequence_name = sprintf('%s_seq', $table_name);

        /**
         * On interroge la base de données pour vérifier si la séquence existe
         * ou non. Si il y a un retour à l'exécution de la requête alors la
         * séquence existe et si il n'y en a pas alors la séquence n'existe
         * pas.
         *
         * Cette requête particulière (car sur la table pg_class) nécessite
         * d'être exécutée sur le schéma public pour fonctionner correctement.
         * En effet, par défaut postgresql positionne search_path avec la
         * valeur '"$user", public' ce qui peut causer des mauvais effets de
         * bord si l'utilisateur et le schéma sont identiques.
         * On force donc le schéma public sur le search_path pour être sûr que
         * la requête suivante s'exécute correctement.
         */
        $res_search_path = $this->f->db->query("set search_path=public;");
        $this->f->isDatabaseError($res_search_path);
        $query_sequence_exists = sprintf(
            'SELECT 
                * 
            FROM 
                pg_class 
            WHERE
                relkind = \'S\' 
                AND oid::regclass::text = \'%s\'
            ;',
            $sequence_name
        );
        $res_sequence_exists = $this->f->db->getone($query_sequence_exists);
        $this->addToLog(
            __METHOD__.'(): db->getone("'.$res_sequence_exists.'");',
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_sequence_exists, true) === true) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db(
                $res_sequence_exists->getDebugInfo(),
                $res_sequence_exists->getMessage(),
                'dossier_autorisation'
            );
            // Stop le traitement
            return false;
        }

        /**
         * Si la séquence n'existe pas, alors on la cré. Puis si des DA
         * existent déjà avec cette clé unique alors on initialise cette
         * séquence avec le numéro du dernier DA correcspondant.
         */
        if ($res_sequence_exists === null) {

            // Création de la sequence si elle n'existe pas
            $res = $this->db->createSequence($table_name);
            $this->f->addToLog(
                __METHOD__.'(): db->createSequence("'.$table_name.'");',
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res, true) === true) {
                // Appel de la methode de recuperation des erreurs
                $this->erreur_db(
                    $res->getDebugInfo(),
                    $res->getMessage(),
                    'dossier_autorisation'
                );
                // Stop le traitement
                return false;
            }

            // Récupération du dernier DA du typeDA passé en paramètre de
            // l'année de dépôt
            $sql_max_DA = sprintf(
                'SELECT
                    dossier_autorisation
                FROM 
                    %sdossier_autorisation
                WHERE
                    dossier_autorisation ILIKE \'%s%%\'
                ORDER BY
                    dossier_autorisation DESC
                ;',
                DB_PREFIXE,
                $datc.$dep.$com.$annee
            );
            $last_DA = $this->db->getone($sql_max_DA);
            $this->f->addToLog(
                __METHOD__.'(): db->getone("'.$sql_max_DA.'");',
                VERBOSE_MODE
            );
            // Si une erreur est levé pour la création.
            if ($this->f->isDatabaseError($last_DA, true) === true) {
                // Appel de la methode de recuperation des erreurs
                $this->erreur_db(
                    $nb_DA->getDebugInfo(),
                    $nb_DA->getMessage(),
                    'dossier_autorisation'
                );
                // Stop le traitement
                return false;
            }

            // Si au moins un dossier on récupère son id
            if ($last_DA != "") {
                //
                $id = intval(substr($last_DA, -4));
                // Mise à jour de la sequence avec l'id recalculé
                $sql_maj_seq = sprintf(
                    'SELECT setval(\'%s\',%s);',
                    $sequence_name,
                    $id
                );
                $res_seq = $this->db->query($sql_maj_seq);
                $this->f->addToLog(
                    __METHOD__.'(): db->query("'.$sql_maj_seq.'");',
                    VERBOSE_MODE
                );
                if ($this->f->isDatabaseError($res_seq, true) === true) {
                    // Appel de la methode de recuperation des erreurs
                    $this->erreur_db(
                        $res_seq->getDebugInfo(),
                        $res_seq->getMessage(),
                        'dossier_autorisation'
                    );
                    // Stop le traitement
                    return false;
                }
            }

        }

        /**
         * On récupère le prochain numéro de la séquence fraichement créée ou
         * créée de longue date.
         */
        $nextID = $this->db->nextId($table_name, false);
        $this->addToLog(
            __METHOD__.'(): db->nextId("'.$table_name.'", false);',
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($nextID, true) === true) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db(
                $res_seq->getDebugInfo(),
                $res_seq->getMessage(),
                'dossier_autorisation'
            );
            // Stop le traitement
            return false;
        }

        /**
         * On retourne le numéro du dossier sur quatre caractères complétés
         * par des zéros. Exemple : '0012'
         */
        $numero_dossier = str_pad($nextID, 4, "0", STR_PAD_LEFT);
        return $numero_dossier;
    }

    /**
     * Méthode permettant de récupérer les id des demandeurs liés à la table
     * liée passée en paramètre
     *
     * @param string $from Table liée : "demande", "dossier", dossier_autorisation"
     * @param string $id Identifiant (clé primaire de la table liée en question)
     */
    function listeDemandeur($from, $id) {
        // Récupération des demandeurs de la base
        $sql = "SELECT demandeur.demandeur,
                        demandeur.type_demandeur,
                        lien_".$from."_demandeur.petitionnaire_principal
            FROM ".DB_PREFIXE."lien_".$from."_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur 
            ON demandeur.demandeur=lien_".$from."_demandeur.demandeur 
            WHERE ".$from." = '".$id."'";
        $res = $this->f->db->query($sql);
        $this->f->addToLog("listeDemandeur(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        // Stockage du résultat dans un tableau
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
            $demandeur_type = $row['type_demandeur'];
            if ($row['petitionnaire_principal'] == 't'){
                $demandeur_type .= "_principal";
            }
            $this->valIdDemandeur[$demandeur_type][] = $row['demandeur'];
        }
    }

    /**
     * Ajout de la liste des demandeurs
     */
    function formSpecificContent($maj) {
        if(!$this->correct AND $maj != 0) {
            $this->listeDemandeur("dossier_autorisation", $this->val[array_search('dossier_autorisation', $this->champs)]);
        }
        if($maj < 2 AND !$this->correct) {
            $linkable = true;
        } else {
            $linkable = false;
        }

        // Conteneur de la listes des demandeurs
        echo "<div id=\"liste_demandeur\" class=\"demande_hidden_bloc col_12\">";
        echo "<fieldset class=\"cadre ui-corner-all ui-widget-content\">";
        echo "  <legend class=\"ui-corner-all ui-widget-content ui-state-active\">"
                ._("Petitionnaire")."</legend>";
        // Si des demandeurs sont liés à la demande
        require_once "../obj/petitionnaire.class.php";
        require_once "../obj/delegataire.class.php";

            // Affichage du bloc pétitionnaire principal / délégataire
            // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
            echo "<div id=\"petitionnaire_principal_delegataire\">";
            // Affichage de la synthèse
            if (isset ($this->valIdDemandeur["petitionnaire_principal"]) AND
                !empty($this->valIdDemandeur["petitionnaire_principal"])) {
                $demandeur = new petitionnaire(
                                    $this->valIdDemandeur["petitionnaire_principal"],
                                    $this->f->db,false);
                $demandeur -> afficherSynthese("petitionnaire_principal", $linkable);
                $demandeur -> __destruct();
            }
            // Si en édition de formulaire
            if($maj < 2) {
                // Bouton d'ajout du pétitionnaire principal
                // L'ID DE L'INPUT SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
                echo "<a href=\"#\" id=\"add_petitionnaire_principal\"
                    class=\"om-form-button add-16\">".
                    _("Saisir le petitionnaire principal").
                "</a>";
            }
            // Bouton d'ajout du delegataire
            // L'ID DU DIV ET DE L'INPUT SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
            echo "<div id=\"delegataire\">";
            if ($maj < 2 OR ($maj == 3 AND !empty($this->valIdDemandeur["delegataire"]))) {
                echo " <span class=\"om-icon om-icon-16 om-icon-fix arrow-right-16\">
                        <!-- -->
                    </span> ";
            }
            // Affichage de la synthèse
            if (isset ($this->valIdDemandeur["delegataire"]) AND
                !empty($this->valIdDemandeur["delegataire"])) {
                $demandeur = new delegataire($this->valIdDemandeur["delegataire"],
                                           $this->f->db,false);
                $demandeur -> afficherSynthese("delegataire", $linkable);
                $demandeur -> __destruct();
            }
            if($maj < 2) {
                echo "<a href=\"#\" id=\"add_delegataire\"
                        class=\"om-form-button add-16\">".
                        _("Saisir un autre correspondant").
                    "</a>";
            }
            echo "</div>";
            
            echo "</div>";
            // Bloc des pétitionnaires secondaires
            // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
            echo "<div id=\"listePetitionnaires\">";

             // Affichage de la synthèse
            if (isset ($this->valIdDemandeur["petitionnaire"]) AND
                !empty($this->valIdDemandeur["petitionnaire"])) {
                
                foreach ($this->valIdDemandeur["petitionnaire"] as $petitionnaire) {
                    $demandeur = new petitionnaire($petitionnaire,
                                           $this->f->db,false);
                    $demandeur -> afficherSynthese("petitionnaire", $linkable);
                    $demandeur -> __destruct();
                }
                
            }
            if ($maj < 2) {
                // L'ID DE L'INPUT SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
                echo "<a href=\"#\" id=\"add_petitionnaire\"
                        class=\"om-form-button add-16\">".
                        _("Ajouter un petitionnaire")
                    ."</a>";
            }
            echo "</div>";
        echo "</fieldset>";
        echo "</div>";
    }


    /**
     * Méthode permettant de recalculer à tout moment les données en cours de
     * validité d'un dossier d'autorisation
     */
    function majDossierAutorisation() {

        /*
         * Mise à jour des données (terrain, ref. cadastrales, demandeurs, lots)
         * si au moins un dossier a été accepté
         */
        // Initialisation des requêtes
        $sql_terrain = "SELECT dossier.terrain_references_cadastrales,
                            dossier.terrain_adresse_voie_numero,
                            dossier.terrain_adresse_voie,
                            dossier.terrain_adresse_lieu_dit,
                            dossier.terrain_adresse_localite,
                            dossier.terrain_adresse_code_postal,
                            dossier.terrain_adresse_bp,
                            dossier.terrain_adresse_cedex,
                            dossier.terrain_superficie
                        FROM ".DB_PREFIXE."dossier";

        $sql_lots = "SELECT lot.lot 
                    FROM ".DB_PREFIXE."lot
                    JOIN ".DB_PREFIXE."dossier ON dossier.dossier = lot.dossier";

        $sql_demandeurs = "SELECT lien_dossier_demandeur.demandeur, lien_dossier_demandeur.petitionnaire_principal,
                            lien_dossier_demandeur.lien_dossier_demandeur, lien_dossier_demandeur.dossier
                            FROM ".DB_PREFIXE."lien_dossier_demandeur
                            LEFT JOIN ".DB_PREFIXE."dossier ON
                            lien_dossier_demandeur.dossier = dossier.dossier";
        $sql_etat = "SELECT avis_decision.typeavis, avis_decision.avis_decision
                        FROM ".DB_PREFIXE."dossier
                        LEFT JOIN ".DB_PREFIXE."avis_decision ON
                            dossier.avis_decision = avis_decision.avis_decision";
        // Récupération du nombre de dossiers d'instruction liés au dossier d'autorisation
        $sql_count_di = "SELECT count(*) FROM ".DB_PREFIXE."dossier as di
                            LEFT JOIN ".DB_PREFIXE."avis_decision 
                                ON di.avis_decision = avis_decision.avis_decision
                            WHERE di.dossier_autorisation = '".$this->getVal("dossier_autorisation")."'
                                AND (di.version = 0
                                OR (di.date_decision IS NOT NULL
                                    AND (avis_decision.typeavis='F'
                                        OR avis_decision.typeavis='A'
                                    )
                                ))";

        $count_di = $this->db->getOne($sql_count_di);
        $this->addToLog("majDossierAutorisation(): db->getone(\"".$sql_count_di."\")", VERBOSE_MODE);
        if(database::isError($count_di)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($count_di->getDebugInfo(), $count_di->getMessage(), '');
            $this->addToLog('', 'majDossierAutorisation() : '.$count_di->getMessage(), __FUNCTION__);
            return false;
        }
        // Si pas de dossier : erreur
        if ($count_di == 0) {
            return false;
        } elseif ($count_di == 1) { // Cas P0 (en cours ou non) ou de plusieurs DI en cours
            $sql_where = " WHERE dossier.dossier_autorisation = '".$this->getVal("dossier_autorisation")."' AND version = 0";
        } else { // Cas ou un dossier d'instruction est soumis à arrété
            $sql_where = " WHERE dossier.dossier = (
                                        SELECT dossier.dossier
                                        FROM ".DB_PREFIXE."dossier
                                        JOIN ".DB_PREFIXE."avis_decision 
                                            ON dossier.avis_decision = avis_decision.avis_decision
                                        WHERE  dossier.dossier_autorisation = '".$this->getVal("dossier_autorisation")."'
                                            AND date_decision IS NOT NULL
                                            AND (avis_decision.typeavis='F'
                                                OR avis_decision.typeavis='A')
                                        ORDER BY version DESC
                                        LIMIT 1
                                    )";
        }

        //terrain
        $res_terrain = $this->db->query($sql_terrain.$sql_where);
        $this->addToLog("majDossierAutorisation(): db->query(\"".$sql_terrain.$sql_where."\")", VERBOSE_MODE);
        if(database::isError($res_terrain)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res_terrain->getDebugInfo(), $res_terrain->getMessage(), '');
            $this->addToLog('', 'majDossierAutorisation() : '.$res_terrain->getMessage(), __FUNCTION__);
            return false;
        }
        $row_terrain = $res_terrain->fetchRow(DB_FETCHMODE_ASSOC);
        if($row_terrain != null) {

            //On récupère l'arrondissement, si le code postal est renseigné dans
            //le dossier d'instruction
            if (isset($row_terrain['terrain_adresse_code_postal']) &&
                $row_terrain['terrain_adresse_code_postal'] !== "") {

                $sql_arrondissement = "SELECT arrondissement
                    FROM ".DB_PREFIXE."arrondissement
                    WHERE code_postal = '".$row_terrain['terrain_adresse_code_postal']."'";
                $row_arrondissement = $this->db->getOne($sql_arrondissement);
                $this->addToLog("majDossierAutorisation(): db->getone(\"".$sql_arrondissement."\")", VERBOSE_MODE);
                $this->f->isDatabaseError($row_arrondissement);

                $row_terrain['arrondissement'] = $row_arrondissement;
            }

            // Tous les champs vides sont mis à NULL pour éviter les erreurs de base lors de l'update
            foreach ($row_terrain as $key => $champ) {
                if ($champ == "") {
                    $row_terrain[$key] = NULL;
                }
            }
            $res_update_terrain = $this->db->autoExecute(DB_PREFIXE."dossier_autorisation",
                                                         $row_terrain,
                                                         DB_AUTOQUERY_UPDATE,
                                                         "dossier_autorisation = '".$this->getVal("dossier_autorisation")."'");
            if(database::isError($res_update_terrain)) {
                // Appel de la methode de recuperation des erreurs
                $this->erreur_db($res_update_terrain->getDebugInfo(), $res_update_terrain->getMessage(), '');
                $this->addToLog('', 'majDossierAutorisation() : '.$res_update_terrain->getMessage(), __FUNCTION__);
                return false;
            }
        }

        // Lots / demandeur
        // Suppression des anciens liens entre les lots et le DA
        $valLot['dossier_autorisation'] = NULL;
        $res_update_lots = $this->db->autoExecute(DB_PREFIXE."lot",
                                                  $valLot,
                                                  DB_AUTOQUERY_UPDATE,
                                                  "dossier_autorisation='".$this->getVal("dossier_autorisation")."'");
        $sql = "SELECT count(*) FROM ".DB_PREFIXE."lot WHERE dossier_autorisation='".$this->getVal("dossier_autorisation")."'";
        if(database::isError($res_update_lots)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res_update_terrain->getDebugInfo(), $res_update_terrain->getMessage(), '');
            $this->addToLog('', 'majDossierAutorisation() : '.$res_update_terrain->getMessage(), __FUNCTION__);
            return false;
        }
        // Suppression des anciens liens dossier_autorisation/demandeurs
        $sql_delete_liens_da_demandeur = "DELETE FROM ".DB_PREFIXE."lien_dossier_autorisation_demandeur
                                            WHERE dossier_autorisation='".$this->getVal("dossier_autorisation")."'";
        $res_delete_liens_da_demandeur = $this->db->query($sql_delete_liens_da_demandeur);
        $this->addToLog("majDossierAutorisation(): db->query(\"".$sql_delete_liens_da_demandeur."\")", VERBOSE_MODE);
        if(database::isError($res_delete_liens_da_demandeur)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res_delete_liens_da_demandeur->getDebugInfo(), $res_delete_liens_da_demandeur->getMessage(), '');
            $this->addToLog('', 'majDossierAutorisation() : '.$res_delete_liens_da_demandeur->getMessage(), __FUNCTION__);
            return false;
        }
        // Si il ya des lots liés on les remonte au DA et on omet les demandeurs
        // car ils sont liés aux  lots
        $res_lots = $this->db->query($sql_lots.$sql_where);
        $this->addToLog("majDossierAutorisation(): db->query(\"".$sql_lots.$sql_where."\")", VERBOSE_MODE);
        if(database::isError($res_lots)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res_lots->getDebugInfo(), $res_lots->getMessage(), '');
            $this->addToLog('', 'majDossierAutorisation() : '.$res_lots->getMessage(), __FUNCTION__);
            return false;
        }

        // XXX Si il existe des lots (auxquels les demandeurs sont lié) faut-il recupérer la liste des demandeurs liés au DI ?
        //if ($res_lots->numRows() > 0) {
            // Définition du lien entre lot et dossier_autorisation pour chaque lot
            $valLotUpdate['dossier_autorisation'] = $this->getVal("dossier_autorisation");
            // XXX Sauvegarde des id des lots pour traitement ultérieur 
            // les lots ne devraient pas être liés au DA mais une copie de ces lots
            // devraient l'être.
            $liste_lots = array();
            // On lie chaque lot en définissant l'id du dossier d'autorisation
            while ($rowlot = $res_lots->fetchRow(DB_FETCHMODE_ASSOC)) {
                $res_lots_update = $this->db->autoExecute(DB_PREFIXE."lot",
                                                           $valLotUpdate,
                                                           DB_AUTOQUERY_UPDATE,
                                                           "lot=".$rowlot['lot']);
                $liste_lots[] = $rowlot['lot'];
                if(database::isError($res_lots_update)) {
                    // Appel de la methode de recuperation des erreurs
                    $this->erreur_db($res_lots_update->getDebugInfo(), $res_lots_update->getMessage(), '');
                    $this->addToLog('', 'majDossierAutorisation() : '.$res_lots_update->getMessage(), __FUNCTION__);
                    return false;
                }
            }
        //} else { // Si pas de lot liaison avec les demandeurs
            // Récupération de la liste des demandeurs
            $res_demandeurs = $this->db->query($sql_demandeurs.$sql_where);
            $this->addToLog("majDossierAutorisation(): db->query(\"".$sql_demandeurs.$sql_where."\")", VERBOSE_MODE);
            if(database::isError($res_demandeurs)) {
                // Appel de la methode de recuperation des erreurs
                $this->erreur_db($res_demandeurs->getDebugInfo(), $res_demandeurs->getMessage(), '');
                $this->addToLog('', 'majDossierAutorisation() : '.$res_demandeurs->getMessage(), __FUNCTION__);
                return false;
            }
            // Définition de l'id du DA
            $valDemandeurUpdate["lien_dossier_autorisation_demandeur"] = NULL;
            $valDemandeurUpdate['dossier_autorisation'] = $this->getVal("dossier_autorisation");
            // Pour chaque demandeur on créer un lien avec le DA
            while ($rowDemandeur = $res_demandeurs->fetchRow(DB_FETCHMODE_ASSOC)) {
                // Ajout de l'id du demandeur et du flag petitionnaire_principal
                // aux données à insérer
                $valDemandeurUpdate["demandeur"] = $rowDemandeur["demandeur"];
                $valDemandeurUpdate["petitionnaire_principal"] =
                    $rowDemandeur["petitionnaire_principal"];
                // Instanciation d'un lien dossier_autorisation/demandeur en ajout
                require_once "../obj/lien_dossier_autorisation_demandeur.class.php";
                $ldad = new lien_dossier_autorisation_demandeur("]", $this->db, DEBUG);
                // Ajout d'un enregistrement avec les données des liens
                $ldad->ajouter($valDemandeurUpdate, $this->db, DEBUG);
            }
        //}


        /**
         * Mise à jour des dates
         */
        // Requêtes permettant de les recupérer
        $sql_date_depot_decision_validite = 
            "SELECT 
                MIN(dossier.date_depot) as date_depot,
                MIN(dossier.date_decision) as date_decision,
                MAX(dossier.date_validite) as date_validite
            FROM ".DB_PREFIXE."dossier 
            WHERE dossier.dossier_autorisation='".
                $this->getVal("dossier_autorisation")."'";
     
        $sql_date_doc_daact = 
            "SELECT 
                MAX(dossier.date_chantier) as date_chantier,
                MAX(dossier.date_achevement) as date_achevement
            FROM ".DB_PREFIXE."dossier 
            LEFT OUTER JOIN ".DB_PREFIXE."avis_decision ON
                dossier.avis_decision = avis_decision.avis_decision
            WHERE dossier.dossier_autorisation = '".
                    $this->getVal("dossier_autorisation")."'
                AND date_decision IS NOT NULL
                AND (avis_decision.typeavis='F'
                    OR avis_decision.typeavis='A')";
        // Récupération des infos avec la 1ere requête
        $res_date_depot_decision_validite =
            $this->db->query($sql_date_depot_decision_validite);
        $this->addToLog("majDossierAutorisation(): db->query(\"".
            $sql_date_depot_decision_validite."\")", VERBOSE_MODE);
        if(database::isError($res_date_depot_decision_validite)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db(
                $res_date_depot_decision_validite->getDebugInfo(),
                $res_date_depot_decision_validite->getMessage(),
                ''
            );
            $this->addToLog('', 'majDossierAutorisation() : '.$res_date_depot_decision_validite->getMessage(), __FUNCTION__);
            return false;
        }
        $row_date_depot_decision_validite =
            $res_date_depot_decision_validite->fetchRow(DB_FETCHMODE_ASSOC);
        // Récupération des infos avec la 2nd requête
        $res_date_doc_daact = $this->db->query($sql_date_doc_daact);
        $this->addToLog("majDossierAutorisation(): db->query(\"".
            $sql_date_doc_daact."\")", VERBOSE_MODE);
        if(database::isError($res_date_doc_daact)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res_date_doc_daact->getDebugInfo(),
                $res_date_doc_daact->getMessage(), '');
            $this->addToLog('', 'majDossierAutorisation() : '.$res_date_doc_daact->getMessage(), __FUNCTION__);
            return false;
        }
        $row_date_doc_daact = $res_date_doc_daact->fetchRow(DB_FETCHMODE_ASSOC);
        // Fusion des 2 résultats
        $row_date = array_merge($row_date_depot_decision_validite, $row_date_doc_daact);
        // Si pas de date on remplace "" par NULL pour éviter
        // les erreurs de base de données
        foreach($row_date as $key => $date) {
            if($date == "") {
                $row_date[$key] = null;
            }
        }
        // Mise à jour du DA avec ces nouvelles dates
        $res_update_date = $this->db->autoExecute(
            DB_PREFIXE."dossier_autorisation",
            $row_date,
            DB_AUTOQUERY_UPDATE,
            "dossier_autorisation = '".$this->getVal("dossier_autorisation")."'"
        );
        if(database::isError($res_update_date)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db(
                $res_update_date->getDebugInfo(),
                $res_update_date->getMessage(),
                ''
            );
            $this->addToLog('', 'majDossierAutorisation() : '.$res_update_date->getMessage(), __FUNCTION__);
            return false;
        }

        /**
         * Mise à jour de l'état
         */
        $sql_etat .= " ".$sql_where;
        $res_etat = $this->db->query($sql_etat);
        $this->addToLog(
            "majDossierAutorisation(): db->query(\"".$sql_etat."\")",
            VERBOSE_MODE
        );
        if(database::isError($res_etat)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res_etat->getDebugInfo(), $res_etat->getMessage(), '');
            $this->addToLog('', 'majDossierAutorisation() : '.$res_etat->getMessage(), __FUNCTION__);
            return false;
        }

        $row_etat = $res_etat->fetchRow(DB_FETCHMODE_ASSOC);
        $etatDA = array();

        // Cas initial : on défini les trois valeurs par défaut
        // (elles seront écrasées si $res_encours->numRows() > 1)

        // Correspondance entre typeavis et etat_dossier_autorisation
        switch ($row_etat['typeavis']) {
            case 'F':
                // typeavis F => Accordé
                $etatDA['etat_dernier_dossier_instruction_accepte'] = 2;
                $etatDA['etat_dossier_autorisation'] = 2;
                break;
            case 'D':
                // typeavis D => Refusé
                $etatDA['etat_dernier_dossier_instruction_accepte'] = 4;
                $etatDA['etat_dossier_autorisation'] = 4;
                break;
            case 'A':
                // typeavis A => Abandonné
                $etatDA['etat_dernier_dossier_instruction_accepte'] = 3;
                $etatDA['etat_dossier_autorisation'] = 3;
                break;
            default:
                // typeavis '' => En cours
                $etatDA['etat_dernier_dossier_instruction_accepte'] = null;
                $etatDA['etat_dossier_autorisation'] = 1;
                break;
        }
        $etatDA['avis_decision'] = $row_etat['avis_decision'];


        foreach($etatDA as $key=>$val) {
            if($val=="") {
                $etatDA[$key] = null;
            }
        }

        // Mise à jour du DA avec ces nouvelles dates
        $res_update_etat = $this->db->autoExecute(
            DB_PREFIXE."dossier_autorisation",
            $etatDA,
            DB_AUTOQUERY_UPDATE,
            "dossier_autorisation = '".$this->getVal("dossier_autorisation")."'"
        );
        if(database::isError($res_update_date)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db(
                $res_update_date->getDebugInfo(),
                $res_update_date->getMessage(),
                ''
            );
            $this->addToLog('', 'majDossierAutorisation() : '.$res_update_date->getMessage(), __FUNCTION__);
            return false;
        }

        // Si le champ des références cadastrales n'est pas vide
        if ($this->getVal('terrain_references_cadastrales') 
            != $row_terrain['terrain_references_cadastrales']) {

            // On supprime toutes les lignes de la table 
            // dossier_autorisation_parcelle qui font référence au 
            // dossier d'autorisation en cours de suppression
            $this->supprimer_dossier_autorisation_parcelle(
                $this->getVal('dossier_autorisation')
            );

            // Ajout des parcelles dans la table dossier_autorisation_parcelle
            $this->ajouter_dossier_autorisation_parcelle(
                $this->getVal('dossier_autorisation'), 
                $row_terrain['terrain_references_cadastrales']
            );

        }

        // Mise à jour des données techniques du dossier d'instruction
        $sql_donnees_techniques = "SELECT donnees_techniques
            FROM ".DB_PREFIXE."donnees_techniques
            INNER JOIN ".DB_PREFIXE."dossier ON
                dossier.dossier=donnees_techniques.dossier_instruction ";
        if ($count_di == 1) {
            $sql_donnees_techniques .= $sql_where;
        } else { // Cas ou un dossier d'instruction est soumis à arrété
            $sql_donnees_techniques .= " WHERE dossier.dossier = (
                                        SELECT dossier.dossier
                                        FROM ".DB_PREFIXE."dossier
                                        JOIN ".DB_PREFIXE."avis_decision 
                                            ON dossier.avis_decision = avis_decision.avis_decision
                                        JOIN ".DB_PREFIXE."donnees_techniques 
                                            ON donnees_techniques.dossier_instruction = dossier.dossier
                                        WHERE  dossier.dossier_autorisation = '".$this->getVal("dossier_autorisation")."'
                                            AND (date_decision IS NOT NULL
                                            AND (avis_decision.typeavis='F'
                                                OR avis_decision.typeavis='A')
                                            OR version=0)
                                        ORDER BY version DESC
                                        LIMIT 1
                                    )";
        }
        
        $donnees_techniques = $this->db->getOne($sql_donnees_techniques);
        $this->addToLog(
            "majDossierAutorisation(): db->query(\"".$sql_donnees_techniques."\")",
            VERBOSE_MODE
        );
        if(database::isError($donnees_techniques)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db(
                $donnees_techniques->getDebugInfo(),
                $donnees_techniques->getMessage(),
                ''
            );
            $this->addToLog('', 'majDossierAutorisation() : '.$donnees_techniques->getMessage(), __FUNCTION__);
            return false;
        }
        // Liaison des nouvelles données données techniques au DA 
        require_once "../obj/donnees_techniques.class.php";
        if($donnees_techniques === null) {
            $donnees_techniques = "]";
        }
        $dti = new donnees_techniques($donnees_techniques, $this->db, DEBUG);
        
        // Création du tableau de valeurs pour report des DT sur le DA
        $dti->setValFFromVal();
        unset($dti->valF["tab_surface"]);
        unset($dti->valF["tab_tax_su_princ"]);
        unset($dti->valF["tab_tax_su_heber"]);
        unset($dti->valF["tab_tax_su_secon"]);
        unset($dti->valF["tab_tax_su_tot"]);
        unset($dti->valF["tab_tax_su_non_habit_surf"]);
        unset($dti->valF["tab_tax_su_parc_statio_expl_comm"]);
        unset($dti->valF["tab_tax_am"]);
        unset($dti->valF["tab_erp_eff"]);
        // On récupère l'instance des DT du DA
        $sql = "SELECT donnees_techniques FROM ".DB_PREFIXE."donnees_techniques
                WHERE dossier_autorisation='".$this->getVal($this->clePrimaire)."'";

        $dt_id = $this->db->getOne($sql);
        $this->addToLog(
            "get_da_dt(): db->query(\"".$sql."\")",
            VERBOSE_MODE
        );
        if($this->f->isDatabaseError($dt_id, true)) {
            return false;
        }
        if($dt_id === null) {
            $dt_id = "]";
        }
        // On instancie les données techniques
        $dta = new donnees_techniques($dt_id, $this->db, DEBUG);
        // On lie se tableau de DT au DA
        $dti->valF["dossier_autorisation"] = $this->getVal($this->clePrimaire);
        // On délie les données du DI et lots
        $dti->valF["dossier_instruction"] = null;
        $dti->valF["lot"] = null;
        $dti->valF["donnees_techniques"] = $dta->getVal("donnees_techniques");
        if($dt_id != "]") {
            // On met à jour
            $dta->setParameter('maj',1);
            if($dta->modifier($dti->valF, $this->db, DEBUG) === false) {
                return false;
            }
        } else {
            // On ajoute
            $dta->setParameter('maj',0);
            if($dta->ajouter($dti->valF, $this->db, DEBUG) === false) {
                return false;
            }
        }

        return true;
    }


    /**
     * Ajoute les parcelles du dossier d'autorisation passé en paramètre.
     * 
     * @param string $dossier_autorisation           Identifiant du dossier
     * @param string $terrain_references_cadastrales Références cadastrales du 
     *                                                dossier
     */
    function ajouter_dossier_autorisation_parcelle(
        $dossier_autorisation,
        $terrain_references_cadastrales
    ) {

        // Parse les parcelles
        $list_parcelles = $this->f->parseParcelles($terrain_references_cadastrales, $this->getVal('om_collectivite'));

        // Fichier requis
        require_once "../obj/dossier_autorisation_parcelle.class.php";

        // A chaque parcelle une nouvelle ligne est créée dans la table
        // dossier_parcelle
        foreach ($list_parcelles as $parcelle) {

            // Instance de la classe dossier_parcelle
            $dossier_autorisation_parcelle =
                new dossier_autorisation_parcelle("]", $this->db, DEBUG);

            // Valeurs à sauvegarder
            $value = array(
                'dossier_autorisation_parcelle' => '',
                'dossier_autorisation' => $dossier_autorisation,
                'parcelle' => '',
                'libelle' => $parcelle['quartier']
                                .$parcelle['section']
                                .$parcelle['parcelle']
            );

            // Ajout de la ligne
            $dossier_autorisation_parcelle->ajouter($value, $this->db, DEBUG);
        }

    }

    /**
     * Supprime les parcelles du dossier d'autorisation passé en paramètre
     * @param  string $dossier_autorisation Identifiant du dossier
     */
    function supprimer_dossier_autorisation_parcelle($dossier_autorisation) {

        // Suppression des parcelles du dossier d'autorisation
        $sql = "DELETE FROM ".DB_PREFIXE."dossier_autorisation_parcelle
                WHERE dossier_autorisation='".$dossier_autorisation."'";
        $res = $this->db->query($sql);
        $this->addToLog("supprimer_dossier_autorisation_parcelle() db->query(\"".$sql."\");", VERBOSE_MODE);
        database::isError($res);

    }

    /**
     * Permet d’effectuer des actions après l’insertion des données dans la base
     */
    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {

        // Si le champ des références cadastrales n'est pas vide
        if ($this->valF['terrain_references_cadastrales'] != '') {

            // Ajout des parcelles dans la table dossier_autorisation_parcelle
            $this->ajouter_dossier_autorisation_parcelle($this->valF['dossier_autorisation'], 
                $this->valF['terrain_references_cadastrales']);

        }

    }

    /**
     * Surcharge du fil d'ariane en contexte formulaire.
     *
     * @param string $ent Chaîne initiale
     *
     * @return chaîne de sortie
     */
    function getFormTitle($ent) {
        //
        if (intval($this->getParameter("maj")) === 4) {
            // XXX - Manque demandeur
            $ent =  _("Autorisation")." -> "._("Dossier d'autorisation")." -> ".$this->getVal('dossier_autorisation_libelle');
        }
        //
        return $ent;
    }

    /**
     * Surcharge du fil d'ariane en contexte sous-formulaire.
     *
     * @param string $subent Chaîne initiale
     *
     * @return chaîne de sortie
     */
    function getSubFormTitle($subent) {
        //
        if (intval($this->getParameter("maj")) === 4) {
            // XXX - Manque demandeur
            $subent =  _("Autorisation")." -> "._("Dossier d'autorisation")." -> ".$this->getVal('dossier_autorisation_libelle');
        }
        //
        return $subent;
    }


    /**
     * Permet de générer une clé 4*4 chiffres, séparés par des "-".
     * Cette clé permet au citoyen de se connecter au portail pour consulter son
     * autorisation.
     * Exemple de clé générée : 0000-1111-2222-3333.
     *
     * @return string
     */
    public function generate_citizen_access_key() {
        // Initialisation d'un tableau
        $number_list = array();

        // Génération aléatoire d'un nombre sur 4 caractères, 4 fois
        for ($i = 0; $i < 4; $i++) { 
            $number_list[] = str_pad(mt_rand(0, 9999), 4, 0, STR_PAD_LEFT);
        }

        // Transformation en chaîne tout en séparant les nombres par un "-"
        $result = implode('-', $number_list);

        //
        return $result;
    }


    /**
     * Permet de modifier le dossier d'autorisation pour mettre à jour la clé
     * d'accès au portail citoyen, si celle-ci n'existe pas déjà.
     * Il est possible de forcer sa génération.
     *
     * @param boolean $force Force la génération de la clé.
     *
     * @return boolean
     */
    public function update_citizen_access_key($force = false) {
        // Si une clé d'accès citoyen existe déjà et que la génération n'est pas
        // forcée
        if ($force == false &&
            ($this->getVal('cle_acces_citoyen') != ""
            || $this->getVal('cle_acces_citoyen') != null)) {
            //
            return true;
        }

        // Génération de la clé d'accès au portail citoyen
        $citizen_access_key = $this->generate_citizen_access_key();

        // Valeurs à mettre à jour
        $valF = array();
        // Récupération la valeur des champs
        foreach($this->champs as $key => $champ) {
            //
            $valF[$champ] = $this->val[$key];
        }
        $valF["cle_acces_citoyen"] = $citizen_access_key;

        // Modification
        $update = $this->modifier($valF);

        // Si la modification échoue
        if ($update !== true) {
            //
            return false;
        }

        //
        return true;
    }

    /**
     * Récupère l'instance du type détaillé du dossier d'autorisation.
     *
     * @param integer $dossier_autorisation_type_detaille Identifiant
     *
     * @return object
     */
    function get_inst_dossier_autorisation_type_detaille($dossier_autorisation_type_detaille = null) {
        //
        if (is_null($this->inst_dossier_autorisation_type_detaille)) {
            //
            if (is_null($dossier_autorisation_type_detaille)) {
                //
                $dossier_autorisation_type_detaille = $this->getVal('dossier_autorisation_type_detaille');
            }
            //
            require_once "../obj/dossier_autorisation_type_detaille.class.php";
            $this->inst_dossier_autorisation_type_detaille = new dossier_autorisation_type_detaille($dossier_autorisation_type_detaille, $this->db, 0);
        }
        //
        return $this->inst_dossier_autorisation_type_detaille;
    }

    /**
     * Récupère l'instance du type du dossier d'autorisation.
     *
     * @param integer $dossier_autorisation_type Identifiant
     *
     * @return object
     */
    function get_inst_dossier_autorisation_type($dossier_autorisation_type = null) {
        //
        if (is_null($this->inst_dossier_autorisation_type)) {
            //
            if (is_null($dossier_autorisation_type)) {
                //
                $inst_datd = $this->get_inst_dossier_autorisation_type_detaille($this->getVal('dossier_autorisation_type_detaille'));
                $dossier_autorisation_type = $inst_datd->getVal('dossier_autorisation_type');
            }
            //
            require_once "../obj/dossier_autorisation_type.class.php";
            $this->inst_dossier_autorisation_type = new dossier_autorisation_type($dossier_autorisation_type, $this->db, 0);
        }
        //
        return $this->inst_dossier_autorisation_type;
    }

    /**
     * CONDITION - is_dossier_autorisation_visible
     *
     * Permet de savoir si le type de DA lié au dossier d'instruction courant est visible.
     *
     * @return boolean true si le DA est visible, sinon false
     */
    public function is_dossier_autorisation_visible() {

        $inst_dat = $this->get_inst_dossier_autorisation_type();
        //
        if ($inst_dat->getVal('cacher_da') === 't') {
            return false;
        }
        return true;
    }

    /**
     * VIEW - redirect.
     *
     * Cette vue est appelée lorsque l'on souhaite consulter un DI/DA dont on ne connaît pas le groupe.
     * Ce fonctionnement est nécessaire car :
     *  - on consule le DA pour les DI d'urbanisme ;
     *  - on consulte le DI pour les DI du contentieux ;
     *  - les classes métier filles de 'dossier' sont relatives au groupe.
     *
     * Par exemple, depuis l'onglet "Dossiers Liés" du DI, le listing ne permet pas d'instancier chaque résultat
     * et par conséquent on n'a pas accès au groupe du dossier. L'action tableau consulter y est surchargée afin
     * d'amener à cette vue qui se charge de faire la redirection adéquate.
     *
     * @return void
     */
    public function redirect() {
        $idx_da = $this->getVal($this->clePrimaire);
        // Redirection DA si visible
        if ($this->is_dossier_autorisation_visible() === true) {
            $link = '../scr/form.php?obj=dossier_autorisation&action=3&idx='.$idx_da;
            if ($this->f->get_submitted_get_value('retourformulaire') !== null
                && $this->f->get_submitted_get_value('idxformulaire') !== null) {
                $link .= '&premier=0&recherche=&tricol=&selectioncol=&retourformulaire='.$this->f->get_submitted_get_value('retourformulaire');
                $link .= '&retour='.$this->f->get_submitted_get_value('idxformulaire');
            }
            header('Location: '.$link);
            exit();
        }
        // Sinon redirection vers la classe métier adéquate
        // du premier DI récupéré en base
        $sql = " SELECT dossier FROM ".DB_PREFIXE."dossier ";
        $sql .= " WHERE dossier_autorisation = '".$idx_da."' ";
        $idx_di = $this->db->getOne($sql);
        $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
        require_once '../obj/dossier.class.php';
        $di = new dossier($idx_di);
        $di->redirect();
    }

    /**
     * CONDITION - is_confidentiel
     *
     * Permet de savoir si le type de dossier d'autorisation du dossier courant est
     * confidentiel.
     *
     * @return boolean true si le dossier est confidentiel, sinon false.
     * 
     */
    public function is_confidentiel() {
        //
        $inst_dossier_autorisation_type_detaille = $this->get_inst_dossier_autorisation_type_detaille();
        $inst_dossier_autorisation_type = $this->get_inst_dossier_autorisation_type($inst_dossier_autorisation_type_detaille->getVal('dossier_autorisation_type'));
        $confidentiel = $inst_dossier_autorisation_type->getVal('confidentiel');
        //
        if ($confidentiel === 't') {
            return true;
        }
        return false;
    }

    /**
     * Retourne le code du groupe du dossier d'instruction.
     *
     * @return string
     */
    public function get_groupe() {
        //
        if (isset($this->groupe) === true && $this->groupe !== null) {
            return $this->groupe;
        }

        // Récupère le code du groupe
        $inst_dossier_autorisation_type_detaille = $this->get_inst_dossier_autorisation_type_detaille();
        $inst_dossier_autorisation_type = $this->get_inst_dossier_autorisation_type($inst_dossier_autorisation_type_detaille->getVal('dossier_autorisation_type'));
        $inst_groupe = $this->get_inst_groupe($inst_dossier_autorisation_type->getVal('groupe'));
        $groupe = $inst_groupe->getVal('code');

        //
        $this->groupe = $groupe;
        //
        return $this->groupe;
    }

    /**
     * Récupère l'instance du groupe.
     *
     * @param string $groupe Identifiant du groupe.
     *
     * @return object
     */
    private function get_inst_groupe($groupe) {
        //
        return $this->get_inst_common("groupe", $groupe);
    }

    /**
     * CONDITION - can_user_access_dossier
     *
     * Effectue les vérifications suivantes :
     * - L'utilisateur doit avoir accès au groupe du dossier
     * - Si le dossier est confidentiel, l'utilisateur doit avoir accès aux dossiers
     * confidentiels de ce groupe
     *
     * @return boolean true si les conditions ci-dessus sont réunies, sinon false
     * 
     */
    public function can_user_access_dossier() {
        // Récupère le code du groupe
        $groupe_dossier = $this->get_groupe();

        // Le groupe doit être accessible par l'utilisateur ;
        if ($this->f->is_user_in_group($groupe_dossier) === false) {
            return false;
        }
        if ($this->is_confidentiel() === true) {
            //
            if ($this->f->can_user_access_dossiers_confidentiels_from_groupe($groupe_dossier) === false) {
                return false;
            }
        }
        return true;
    }

}

?>
