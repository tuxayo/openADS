<?php
//$Id: dossier.class.php 6912 2017-06-15 08:20:09Z tuxayo $ 
//gen openMairie le 10/02/2011 20:39

require_once "../gen/obj/dossier.class.php";
require_once "../obj/petitionnaire.class.php";
require_once "../obj/delegataire.class.php";
require_once "../obj/plaignant.class.php";
require_once "../obj/contrevenant.class.php";
require_once "../obj/requerant.class.php";
require_once "../obj/avocat.class.php";
require_once "../obj/bailleur.class.php";

class dossier extends dossier_gen {

    var $maj;
    var $dossier_instruction_type;
    var $is_incomplet_notifie = null;
    var $valIdDemandeur = array("petitionnaire_principal" => array(),
                                "delegataire" => array(),
                                "petitionnaire" => array(),
                                "plaignant_principal" => array(),
                                "plaignant" => array(),
                                "contrevenant_principal" => array(),
                                "contrevenant" => array(),
                                "requerant_principal" => array(),
                                "requerant" => array(),
                                "avocat_principal" => array(),
                                "avocat" => array(),
                                "bailleur_principal" => array(),
                                "bailleur" => array(),
                            );
    var $postedIdDemandeur = array("petitionnaire_principal" => array(),
                                "delegataire" => array(),
                                "petitionnaire" => array(),
                                "plaignant_principal" => array(),
                                "plaignant" => array(),
                                "contrevenant_principal" => array(),
                                "contrevenant" => array(),
                                "requerant_principal" => array(),
                                "requerant" => array(),
                                "avocat_principal" => array(),
                                "avocat" => array(),
                                "bailleur_principal" => array(),
                                "bailleur" => array(),
                            );
    /**
     * Instance de la classe taxe_amenagement.
     *
     * @var null
     */
    var $inst_taxe_amenagement = null;

    /**
     * Instance de la classe donnees_techniques.
     *
     * @var null
     */
    var $inst_donnees_techniques = null;

    /**
     * Instance de la classe dossier_autorisation.
     *
     * @var mixed (resource | null)
     */
    var $inst_dossier_autorisation = null;

    /**
     * Instance de la classe dossier_autorisation_type_detaille.
     *
     * @var null
     */
    var $inst_dossier_autorisation_type_detaille = null;

    /**
     * Instance de la classe cerfa.
     *
     * @var null
     */
    var $inst_cerfa = null;

    /**
     * Groupe du dossier d'instruction.
     *
     * @var null
     */
    var $groupe = null;

    /**
     * Instance de la classe groupe.
     */
    var $inst_groupe = null;

    /**
     * Instance de la classe dossier_autorisation_type.
     *
     * @var null
     */
    var $inst_dossier_autorisation_type = null;

    /**
     * Instance de la classe demande.
     *
     * @var mixed (resource | null)
     */
    var $inst_demande = null;


    function dossier($id, &$db = null, $DEBUG = null) {
        $this->constructeur($id,$db,$DEBUG);
    } // fin constructeur
    
    /*Mutateur pour ma variable dossier_instruction_type*/
    public function getDossierInstructionType(){
        return $this->dossier_instruction_type;
    }
    public function setDossierInstructionType($dossier_instruction_type){
        $this->dossier_instruction_type = $dossier_instruction_type;
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();

        // ACTION - 003 - consulter
        //
        $this->class_actions[3]["condition"] = array(
            "is_user_from_allowed_collectivite",
            "check_context",
        );

        // ACTION - 004 - contrainte
        //
        $this->class_actions[4] = array(
            "identifier" => "contrainte",
            "view" => "view_contrainte",
            "permission_suffix" => "contrainte_tab",
            "condition" => array(
                "is_user_from_allowed_collectivite",
            ),
        );

        // ACTION - 005 - view_document_numerise
        // Interface spécifique du tableau des pièces
        $this->class_actions[5] = array(
            "identifier" => "view_document_numerise",
            "view" => "view_document_numerise",
            "permission_suffix" => "document_numerise",
            "condition" => array(
                "is_user_from_allowed_collectivite",
            ),
        );
        
        // ACTION - 006 - view_sitadel
        //
        $this->class_actions[6] = array(
            "identifier" => "sitadel",
            "view" => "view_sitadel",
            "permission_suffix" => "export_sitadel",
        );

        // ACTION - 777 - Redirection vers la classe fille adéquate
        // 
        $this->class_actions[777] = array(
            "identifier" => "redirect",
            "view" => "redirect",
            "permission_suffix" => "consulter",
        );
    }

    /**
     * VIEW - view_sitadel.
     * 
     * @return void
     */
    function view_sitadel() {
        
        $post = $this->f->get_submitted_post_value();
        // 1ere étape : affichage du formulaire permettant le choix d'un interval de date
        // ainsi que le numéro de d'ordre qui est le numéro de la version de 
        // l'export
        if (empty($post)) {
            // Initialisation des variables
            $datedebut = "";
            $datefin = "";
            $numero = "";
            // Récupération des paramètres
            if ($this->f->get_submitted_get_value("datedebut") !== null AND $this->f->get_submitted_get_value("datedebut") != "") {
                $datedebut = substr($this->f->get_submitted_get_value("datedebut"),8,2)."/".substr($this->f->get_submitted_get_value("datedebut"),5,2)."/".substr($this->f->get_submitted_get_value("datedebut"),0,4);
            }
            if ($this->f->get_submitted_get_value("datefin") !== null AND $this->f->get_submitted_get_value("datefin") != "") {
                $datefin = substr($this->f->get_submitted_get_value("datefin"),8,2)."/".substr($this->f->get_submitted_get_value("datefin"),5,2)."/".substr($this->f->get_submitted_get_value("datefin"),0,4);
            }
            if ($this->f->get_submitted_get_value("numero") !== null AND $this->f->get_submitted_get_value("numero") != "") {
                $numero = $this->f->get_submitted_get_value("numero");
            }
            // Affichage du formulaire
            $this->affichageFormulaire($datedebut, $datefin, $numero);
        }
        else {
            // Initialisation des variables pour les messages de fin de traitement
            $correct=true;
            // Initialisation de la chaîne contenant le message d'erreur
            $erreur = "";

            // Initialisation des dates de début et de fin
            $datedebut ='';
            $datefin='';

            // Traitement des erreurs
            if ($this->f->get_submitted_post_value("datedebut") == "") {
                $correct=false;
            } else {
                $datedebut = substr($this->f->get_submitted_post_value("datedebut"),6,4).
                            "-".substr($this->f->get_submitted_post_value("datedebut"),3,2)."-".
                            substr($this->f->get_submitted_post_value("datedebut"),0,2);
            }
            if ($this->f->get_submitted_post_value("datefin") == "") {
                $correct=false;
            } else {
                $datefin = substr($this->f->get_submitted_post_value("datefin"),6,4).
                            "-".substr($this->f->get_submitted_post_value("datefin"),3,2)."-".
                            substr($this->f->get_submitted_post_value("datefin"),0,2);
            }
            $numero = $this->f->get_submitted_post_value("numero");
            // Composition du tableau de paramètres pour le bouton retour
            $params = array(
                "href" => "../scr/form.php?obj=sitadel&action=6&idx=0&datedebut=".$datedebut."&amp;datefin=".$datefin."&amp;numero=".$numero,
            );
            //
            if ($correct === true){ // ***
                // Affichage du bouton retour
                $this->f->layout->display_form_retour($params);
                // Requête permettant de lister tous les dossiers de l'export
                $sql= "SELECT   dossier.dossier,
                    dossier.om_collectivite as collectivite,
                    dossier.dossier_autorisation,
                    dossier_instruction_type.mouvement_sitadel,
                    dossier_instruction_type.code as dossier_instruction_type_code,
                    dossier.date_depot,
                    dossier.date_decision,
                    dossier.date_chantier,
                    dossier.date_achevement,
                    dossier.terrain_references_cadastrales as dossier_terrain_references_cadastrales,
                    dossier.terrain_adresse_voie_numero as dossier_terrain_adresse_voie_numero,
                    dossier.terrain_adresse_voie as dossier_terrain_adresse_voie,
                    dossier.terrain_adresse_lieu_dit as dossier_terrain_adresse_lieu_dit,
                    dossier.terrain_adresse_localite as dossier_terrain_adresse_localite,
                    dossier.terrain_adresse_code_postal as dossier_terrain_adresse_code_postal,
                    dossier.terrain_adresse_bp as dossier_terrain_adresse_bp,
                    dossier.terrain_adresse_cedex as dossier_terrain_adresse_cedex,
                    dossier_autorisation_type.code,
                    dossier.date_limite,
                    dossier.date_limite_incompletude,
                    dossier.date_notification_delai,
                    dossier.terrain_superficie as dossier_terrain_superficie,

                    arrondissement.code_impots as code_impots,

                    autorite_competente.autorite_competente_sitadel,
                    pp.type_demandeur,
                    pp.qualite,
                    civilite_pp.libelle as civilite_pp,
                    pp.particulier_nom as pp_particulier_nom,
                    pp.particulier_prenom as pp_particulier_prenom,
                    pp.personne_morale_denomination as pp_personne_morale_denomination,
                    pp.personne_morale_raison_sociale as pp_personne_morale_raison_sociale,
                    pp.personne_morale_siret as pp_personne_morale_siret,
                    pp.personne_morale_categorie_juridique as pp_personne_morale_categorie_juridique,
                    civilite_pm.libelle as civilite_pm_libelle,
                    pp.personne_morale_nom as pp_personne_morale_nom,
                    pp.personne_morale_prenom as pp_personne_morale_prenom,

                    pp.numero as pp_numero,
                    pp.voie as pp_voie,
                    pp.complement as pp_complement,
                    pp.lieu_dit as pp_lieu_dit,
                    pp.localite as pp_localite,
                    pp.code_postal as pp_code_postal,
                    pp.bp as pp_bp,
                    pp.cedex as pp_cedex,
                    pp.pays as pp_pays,
                    pp.division_territoriale as pp_division_territoriale,

                    pp.telephone_fixe as pp_telephone_fixe,
                    pp.courriel as pp_courriel,

                    donnees_techniques.co_archi_recours,
                    donnees_techniques.am_terr_surf,
                    donnees_techniques.am_lotiss,
                    donnees_techniques.terr_juri_zac,
                    donnees_techniques.terr_juri_afu,
                    donnees_techniques.co_projet_desc,
                    donnees_techniques.am_projet_desc,
                    donnees_techniques.dm_projet_desc,
                    donnees_techniques.co_cstr_nouv,
                    donnees_techniques.co_cstr_exist,
                    donnees_techniques.co_modif_aspect,
                    donnees_techniques.co_modif_struct,
                    donnees_techniques.co_cloture,
                    donnees_techniques.co_trx_exten,
                    donnees_techniques.co_trx_surelev,
                    donnees_techniques.co_trx_nivsup,
                    donnees_techniques.co_trx_amgt,
                    donnees_techniques.co_anx_pisc,
                    donnees_techniques.co_anx_gara,
                    donnees_techniques.co_anx_veran,
                    donnees_techniques.co_anx_abri,
                    donnees_techniques.co_anx_autr,
                    donnees_techniques.co_bat_niv_nb,

                    -- Tableau des destinations
                    donnees_techniques.su_avt_shon1,
                    donnees_techniques.su_avt_shon2,
                    donnees_techniques.su_avt_shon3,
                    donnees_techniques.su_avt_shon4,
                    donnees_techniques.su_avt_shon5,
                    donnees_techniques.su_avt_shon6,
                    donnees_techniques.su_avt_shon7,
                    donnees_techniques.su_avt_shon8,
                    donnees_techniques.su_avt_shon9,
                    donnees_techniques.su_demo_shon1,
                    donnees_techniques.su_demo_shon2,
                    donnees_techniques.su_demo_shon3,
                    donnees_techniques.su_demo_shon4,
                    donnees_techniques.su_demo_shon5,
                    donnees_techniques.su_demo_shon6,
                    donnees_techniques.su_demo_shon7,
                    donnees_techniques.su_demo_shon8,
                    donnees_techniques.su_demo_shon9,
                    donnees_techniques.su_chge_shon1,
                    donnees_techniques.su_chge_shon2,
                    donnees_techniques.su_chge_shon3,
                    donnees_techniques.su_chge_shon4,
                    donnees_techniques.su_chge_shon5,
                    donnees_techniques.su_chge_shon6,
                    donnees_techniques.su_chge_shon7,
                    donnees_techniques.su_chge_shon8,
                    donnees_techniques.su_chge_shon9,
                    donnees_techniques.su_sup_shon1,
                    donnees_techniques.su_sup_shon2,
                    donnees_techniques.su_sup_shon3,
                    donnees_techniques.su_sup_shon4,
                    donnees_techniques.su_sup_shon5,
                    donnees_techniques.su_sup_shon6,
                    donnees_techniques.su_sup_shon7,
                    donnees_techniques.su_sup_shon8,
                    donnees_techniques.su_sup_shon9,
                    donnees_techniques.su_cstr_shon1,
                    donnees_techniques.su_cstr_shon2,
                    donnees_techniques.su_cstr_shon3,
                    donnees_techniques.su_cstr_shon4,
                    donnees_techniques.su_cstr_shon5,
                    donnees_techniques.su_cstr_shon6,
                    donnees_techniques.su_cstr_shon7,
                    donnees_techniques.su_cstr_shon8,
                    donnees_techniques.su_cstr_shon9,
                    donnees_techniques.su_tot_shon1,
                    donnees_techniques.su_tot_shon2,
                    donnees_techniques.su_tot_shon3,
                    donnees_techniques.su_tot_shon4,
                    donnees_techniques.su_tot_shon5,
                    donnees_techniques.su_tot_shon6,
                    donnees_techniques.su_tot_shon7,
                    donnees_techniques.su_tot_shon8,
                    donnees_techniques.su_tot_shon9,
                    -- XXX valeurs obsolètes mais utilisées dans les conditions
                    -- pour afficher les messages d'incohérence
                    donnees_techniques.su_trsf_shon1,
                    donnees_techniques.su_trsf_shon2,
                    donnees_techniques.su_trsf_shon3,
                    donnees_techniques.su_trsf_shon4,
                    donnees_techniques.su_trsf_shon5,
                    donnees_techniques.su_trsf_shon6,
                    donnees_techniques.su_trsf_shon7,
                    donnees_techniques.su_trsf_shon8,
                    donnees_techniques.su_trsf_shon9,

                    -- Tableau des sous-destinations
                    donnees_techniques.su2_avt_shon1,
                    donnees_techniques.su2_avt_shon2,
                    donnees_techniques.su2_avt_shon3,
                    donnees_techniques.su2_avt_shon4,
                    donnees_techniques.su2_avt_shon5,
                    donnees_techniques.su2_avt_shon6,
                    donnees_techniques.su2_avt_shon7,
                    donnees_techniques.su2_avt_shon8,
                    donnees_techniques.su2_avt_shon9,
                    donnees_techniques.su2_avt_shon10,
                    donnees_techniques.su2_avt_shon11,
                    donnees_techniques.su2_avt_shon12,
                    donnees_techniques.su2_avt_shon13,
                    donnees_techniques.su2_avt_shon14,
                    donnees_techniques.su2_avt_shon15,
                    donnees_techniques.su2_avt_shon16,
                    donnees_techniques.su2_avt_shon17,
                    donnees_techniques.su2_avt_shon18,
                    donnees_techniques.su2_avt_shon19,
                    donnees_techniques.su2_avt_shon20,
                    donnees_techniques.su2_demo_shon1,
                    donnees_techniques.su2_demo_shon2,
                    donnees_techniques.su2_demo_shon3,
                    donnees_techniques.su2_demo_shon4,
                    donnees_techniques.su2_demo_shon5,
                    donnees_techniques.su2_demo_shon6,
                    donnees_techniques.su2_demo_shon7,
                    donnees_techniques.su2_demo_shon8,
                    donnees_techniques.su2_demo_shon9,
                    donnees_techniques.su2_demo_shon10,
                    donnees_techniques.su2_demo_shon11,
                    donnees_techniques.su2_demo_shon12,
                    donnees_techniques.su2_demo_shon13,
                    donnees_techniques.su2_demo_shon14,
                    donnees_techniques.su2_demo_shon15,
                    donnees_techniques.su2_demo_shon16,
                    donnees_techniques.su2_demo_shon17,
                    donnees_techniques.su2_demo_shon18,
                    donnees_techniques.su2_demo_shon19,
                    donnees_techniques.su2_demo_shon20,
                    donnees_techniques.su2_chge_shon1,
                    donnees_techniques.su2_chge_shon2,
                    donnees_techniques.su2_chge_shon3,
                    donnees_techniques.su2_chge_shon4,
                    donnees_techniques.su2_chge_shon5,
                    donnees_techniques.su2_chge_shon6,
                    donnees_techniques.su2_chge_shon7,
                    donnees_techniques.su2_chge_shon8,
                    donnees_techniques.su2_chge_shon9,
                    donnees_techniques.su2_chge_shon10,
                    donnees_techniques.su2_chge_shon11,
                    donnees_techniques.su2_chge_shon12,
                    donnees_techniques.su2_chge_shon13,
                    donnees_techniques.su2_chge_shon14,
                    donnees_techniques.su2_chge_shon15,
                    donnees_techniques.su2_chge_shon16,
                    donnees_techniques.su2_chge_shon17,
                    donnees_techniques.su2_chge_shon18,
                    donnees_techniques.su2_chge_shon19,
                    donnees_techniques.su2_chge_shon20,
                    donnees_techniques.su2_sup_shon1,
                    donnees_techniques.su2_sup_shon2,
                    donnees_techniques.su2_sup_shon3,
                    donnees_techniques.su2_sup_shon4,
                    donnees_techniques.su2_sup_shon5,
                    donnees_techniques.su2_sup_shon6,
                    donnees_techniques.su2_sup_shon7,
                    donnees_techniques.su2_sup_shon8,
                    donnees_techniques.su2_sup_shon9,
                    donnees_techniques.su2_sup_shon10,
                    donnees_techniques.su2_sup_shon11,
                    donnees_techniques.su2_sup_shon12,
                    donnees_techniques.su2_sup_shon13,
                    donnees_techniques.su2_sup_shon14,
                    donnees_techniques.su2_sup_shon15,
                    donnees_techniques.su2_sup_shon16,
                    donnees_techniques.su2_sup_shon17,
                    donnees_techniques.su2_sup_shon18,
                    donnees_techniques.su2_sup_shon19,
                    donnees_techniques.su2_sup_shon20,
                    donnees_techniques.su2_cstr_shon1,
                    donnees_techniques.su2_cstr_shon2,
                    donnees_techniques.su2_cstr_shon3,
                    donnees_techniques.su2_cstr_shon4,
                    donnees_techniques.su2_cstr_shon5,
                    donnees_techniques.su2_cstr_shon6,
                    donnees_techniques.su2_cstr_shon7,
                    donnees_techniques.su2_cstr_shon8,
                    donnees_techniques.su2_cstr_shon9,
                    donnees_techniques.su2_cstr_shon10,
                    donnees_techniques.su2_cstr_shon11,
                    donnees_techniques.su2_cstr_shon12,
                    donnees_techniques.su2_cstr_shon13,
                    donnees_techniques.su2_cstr_shon14,
                    donnees_techniques.su2_cstr_shon15,
                    donnees_techniques.su2_cstr_shon16,
                    donnees_techniques.su2_cstr_shon17,
                    donnees_techniques.su2_cstr_shon18,
                    donnees_techniques.su2_cstr_shon19,
                    donnees_techniques.su2_cstr_shon20,
                    donnees_techniques.su2_tot_shon1,
                    donnees_techniques.su2_tot_shon2,
                    donnees_techniques.su2_tot_shon3,
                    donnees_techniques.su2_tot_shon4,
                    donnees_techniques.su2_tot_shon5,
                    donnees_techniques.su2_tot_shon6,
                    donnees_techniques.su2_tot_shon7,
                    donnees_techniques.su2_tot_shon8,
                    donnees_techniques.su2_tot_shon9,
                    donnees_techniques.su2_tot_shon10,
                    donnees_techniques.su2_tot_shon11,
                    donnees_techniques.su2_tot_shon12,
                    donnees_techniques.su2_tot_shon13,
                    donnees_techniques.su2_tot_shon14,
                    donnees_techniques.su2_tot_shon15,
                    donnees_techniques.su2_tot_shon16,
                    donnees_techniques.su2_tot_shon17,
                    donnees_techniques.su2_tot_shon18,
                    donnees_techniques.su2_tot_shon19,
                    donnees_techniques.su2_tot_shon20,

                    donnees_techniques.co_sp_transport,
                    donnees_techniques.co_sp_enseign,
                    donnees_techniques.co_sp_sante,
                    donnees_techniques.co_sp_act_soc,
                    donnees_techniques.co_sp_ouvr_spe,
                    donnees_techniques.co_sp_culture,
                    donnees_techniques.dm_tot_log_nb,
                    donnees_techniques.co_tot_ind_nb,
                    donnees_techniques.co_tot_coll_nb,
                    donnees_techniques.co_tot_log_nb,
                    donnees_techniques.co_resid_agees,
                    donnees_techniques.co_resid_etud,
                    donnees_techniques.co_resid_tourism,
                    donnees_techniques.co_resid_hot_soc,
                    donnees_techniques.co_resid_hand,
                    donnees_techniques.co_resid_autr,
                    donnees_techniques.co_resid_autr_desc,
                    donnees_techniques.co_uti_pers,
                    donnees_techniques.co_uti_princ,
                    donnees_techniques.co_uti_secon,
                    donnees_techniques.co_uti_vente,
                    donnees_techniques.co_uti_loc,
                    donnees_techniques.co_foyer_chamb_nb,
                    donnees_techniques.co_fin_lls_nb,
                    donnees_techniques.co_fin_aa_nb,
                    donnees_techniques.co_fin_ptz_nb,
                    donnees_techniques.co_fin_autr_nb,
                    donnees_techniques.co_mais_piece_nb,
                    donnees_techniques.co_log_1p_nb,
                    donnees_techniques.co_log_2p_nb,
                    donnees_techniques.co_log_3p_nb,
                    donnees_techniques.co_log_4p_nb,
                    donnees_techniques.co_log_5p_nb,
                    donnees_techniques.co_log_6p_nb,
                    donnees_techniques.mod_desc,

                    donnees_techniques.doc_date,
                    donnees_techniques.terr_div_surf_av_div,
                    donnees_techniques.doc_tot_trav,
                    donnees_techniques.doc_tranche_trav,
                    donnees_techniques.doc_tranche_trav_desc,
                    donnees_techniques.doc_surf,
                    donnees_techniques.doc_nb_log,
                    donnees_techniques.doc_nb_log_indiv,
                    donnees_techniques.doc_nb_log_coll,
                    donnees_techniques.doc_nb_log_lls,
                    donnees_techniques.doc_nb_log_aa,
                    donnees_techniques.doc_nb_log_ptz,
                    donnees_techniques.doc_nb_log_autre,
                    donnees_techniques.daact_date,
                    donnees_techniques.daact_date_chgmt_dest,
                    donnees_techniques.daact_tot_trav,
                    donnees_techniques.daact_tranche_trav,
                    donnees_techniques.daact_tranche_trav_desc,
                    donnees_techniques.daact_surf,
                    donnees_techniques.daact_nb_log,
                    donnees_techniques.daact_nb_log_indiv,
                    donnees_techniques.daact_nb_log_coll,
                    donnees_techniques.daact_nb_log_lls,
                    donnees_techniques.daact_nb_log_aa,
                    donnees_techniques.daact_nb_log_ptz,
                    donnees_techniques.daact_nb_log_autre,

                    dossier_autorisation.date_depot as date_depot_da,
                    dossier_autorisation.date_decision as date_decision_da,
                    dossier_autorisation.date_validite as date_validite_da,
                    dossier_autorisation.date_chantier as date_chantier_da,
                    dossier_autorisation.date_achevement as date_achevement_da,
                    avis_decision.typeavis as typeavis_da,
                    avis_decision.sitadel,
                    avis_decision.sitadel_motif,
                    avis_decision.typeavis,
                    etat.statut as statut_di

                FROM ".DB_PREFIXE."dossier
                    
                INNER JOIN ".DB_PREFIXE."dossier_instruction_type 
                    ON dossier.dossier_instruction_type =
                    dossier_instruction_type.dossier_instruction_type
                INNER JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
                    ON dossier_instruction_type.dossier_autorisation_type_detaille
                    =dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
                INNER JOIN ".DB_PREFIXE."dossier_autorisation_type
                    ON dossier_autorisation_type.dossier_autorisation_type
                    =dossier_autorisation_type_detaille.dossier_autorisation_type
                INNER JOIN ".DB_PREFIXE."groupe
                    ON dossier_autorisation_type.groupe = groupe.groupe
                    AND groupe.code != 'CTX'
                INNER JOIN ".DB_PREFIXE."dossier_autorisation
                    ON dossier_autorisation.dossier_autorisation
                    =dossier.dossier_autorisation
                INNER JOIN ".DB_PREFIXE."autorite_competente
                    ON autorite_competente.autorite_competente
                    =dossier.autorite_competente

                LEFT JOIN ".DB_PREFIXE."donnees_techniques
                    ON donnees_techniques.dossier_instruction = dossier.dossier
                LEFT JOIN ".DB_PREFIXE."avis_decision
                    ON avis_decision.avis_decision = dossier.avis_decision
                LEFT JOIN ".DB_PREFIXE."lien_dossier_demandeur as ldd_pp 
                    ON ldd_pp.dossier = dossier.dossier
                LEFT JOIN ".DB_PREFIXE."demandeur as pp 
                    ON ldd_pp.demandeur = pp.demandeur
                LEFT JOIN ".DB_PREFIXE."civilite as civilite_pp
                    ON civilite_pp.civilite = pp.particulier_civilite
                LEFT JOIN ".DB_PREFIXE."civilite as civilite_pm
                    ON civilite_pm.civilite = pp.personne_morale_civilite
                LEFT JOIN ".DB_PREFIXE."etat
                    ON etat.etat = dossier.etat
                LEFT JOIN ".DB_PREFIXE."arrondissement 
                    ON dossier_autorisation.arrondissement=arrondissement.arrondissement
                    
                WHERE dossier_instruction_type.mouvement_sitadel IS NOT NULL
                    AND ldd_pp.petitionnaire_principal is TRUE
                    AND ((dossier.date_depot >='".$datedebut."' AND dossier.date_depot<='".$datefin."')
                    OR (dossier.date_decision>='".$datedebut."' AND dossier.date_decision<='".$datefin."')
                    OR (dossier.date_chantier>='".$datedebut."' AND dossier.date_chantier<='".$datefin."')
                    OR (dossier.date_achevement>='".$datedebut."' AND dossier.date_achevement<='".$datefin."'))
                ORDER by dossier_instruction_type.mouvement_sitadel, dossier.dossier";
                //Exécution de la requête
                $res = $this->f->db -> query ($sql);
                $this->f->addToLog("dossier.class.php : db->query(\"".$sql."\")", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $export="";
                while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
                    // initialisation de la classe permettant la mise en forme de chaque ligne de l'export
                    require_once "../obj/export_sitadel.class.php";
                    $export_sitadel = new export_sitadel($row['dossier'], $this->f);
                    $export_sitadel->setRow($row);
                    //Ajout du fichier de variable
                    if(file_exists ("../sql/".OM_DB_PHPTYPE."/export_sitadel.inc.php")) {
                        include ("../sql/".OM_DB_PHPTYPE."/export_sitadel.inc.php");
                    }
                    //
                    $export_sitadel->setVal($val);  
                    $departement = $export_sitadel->getDepartement($row["collectivite"]);
                    $commune = $export_sitadel->getCommune($row["collectivite"]);
                    $region = $this->f->getParameter("region");

                    // Initialisation des variables pour le tableau des
                    // surfaces en version 1
                    $prefix_su = 'su';
                    $count_su = 9;
                    // S'il faut utiliser le tableau des surfaces en
                    // version 2
                    if ($export_sitadel->get_tab_su_version() === 2) {
                        //
                        $prefix_su = 'su2';
                        $count_su = 20;
                    }

                    // Récupère la version du dossier d'instruction depuis son
                    // numéro
                    $version = 0;
                    $version = intval($this->get_dossier_instruction_version($row['dossier']));

                    // Mouvement de dépôt
                    $depot="";

                    // Tous les mouvements autres que transfert ont comme mouvement le code DEPOT
                    // les décisions devant êtres précédées par le dépôt correspondant,
                    // les dossiers avec date de décision comprise dans l'interval fourni sont
                    // réaffichés en tant que dépôts (mouvement DEPOT)
                    if (($row['mouvement_sitadel'] == 'DEPOT'||($row['mouvement_sitadel'] == 'MODIFICATIF' 
                        AND $row['statut_di']=='cloture' AND $row['typeavis']=='F'))
                        and (($row['date_depot'] >= $datedebut
                        and $row['date_depot'] <= $datefin)
                        or  ($row['date_decision'] >= $datedebut
                        and $row['date_decision'] <= $datefin))) {
                        $depot.=$export_sitadel->entete("DEPOT",$departement,$commune, $version);
                        $depot.=$export_sitadel->etatcivil();
                        $depot.=$export_sitadel->adresse();
                        $depot.=$export_sitadel->delegataire();
                        $depot.=$export_sitadel->meltel($row['mouvement_sitadel']);
                        $depot.=$export_sitadel->adresse_terrain();             
                        $depot.=$export_sitadel->parcelle();
                        // ===========================
                        // contrat maison individuelle
                        // ===========================
                        // sitadel : contrat|
                        // openads : non renseigne
                        $depot.= $export_sitadel->defaultValue('contrat')."|";
                        // ==========
                        // architecte
                        // ==========
                        // sitadel : architecte|
                        // openads : données techniques.co_archi_recours    
                        $depot.= ($row['co_archi_recours'] == "t")?"1|":"0|";
                        // =====================
                        // utilisation data cnil
                        // ======================
                        // sitadel : cnil
                        // openads : non renseigne
                        $depot.= $export_sitadel->defaultValue('cnil');
                        // fin d enregistrement depot         
                        $depot.="\n";
                    }

                    // Seuls les dossier de type transfert ont comme mouvement le code TRANSFERT
                    $transfert ="";
                    if($row['mouvement_sitadel']=='TRANSFERT'
                        and $row['date_depot'] >= $datedebut
                        and $row['date_depot']<=$datefin){
                        $transfert.=$export_sitadel->entete("TRANSFERT",$departement,$commune, $version);
                        $transfert.=$export_sitadel->etatcivil();
                        $transfert.=$export_sitadel->adresse(); 
                        $transfert.=$export_sitadel->meltel($row['mouvement_sitadel']);
                        $transfert.="\n";
                    }


                    // Une ligne de mouvement DECISION est insérée après chaque ligne
                    // de mouvement DEPOT
                    $decision="";
                    if ($row['mouvement_sitadel'] != 'TRANSFERT'
                        and $row['mouvement_sitadel'] != 'SUPPRESSION'
                        and $row['mouvement_sitadel'] == 'DEPOT'
                        and (($row['date_depot'] >= $datedebut and $row['date_depot']<=$datefin)
                        or  ($row['date_decision'] >= $datedebut and $row['date_decision']<=$datefin))) {

                        //Ajout de l'entête
                        $decision.=$export_sitadel->entete("DECISION",$departement,$commune, $version);

                        //Ajout du groupe 1
                        $decision.= $export_sitadel->decision_groupe1();
                        // Si la décision est favorable, on remplit le groupe 2
                        if ( $row['sitadel'] == 2 || $row['sitadel'] == 4 || $row['sitadel'] == 5
                            || $row['sitadel'] == 0 ){
                            //
                            $decision.= $export_sitadel->amenagement_terrain();
                            //Nature du projet
                            $natproj = 2;
                            $natprojlib= array(
                                1=>_("Nouvelle construction"),
                                2=>_("Travaux sur construction existante"),
                                3=>_("Nouvelle construction et travaux sur construction"),
                            );
                            if ( isset($row['co_cstr_nouv']) && isset($row['co_cstr_exist']) &&
                                $row['co_cstr_nouv'] == 't' && 
                                $row['co_cstr_exist'] == 't' ){
                                $natproj = 3;
                            } 
                            //Nouvelle construction
                            elseif ( isset($row['co_cstr_nouv']) && $row['co_cstr_nouv'] == 't' ) {
                                $natproj = 1;
                            }
                            //
                            $su_avt_shon = $export_sitadel->shon("avt");
                            //Si la SHON existante avant travaux est supérieur à la SHON 
                            //démolie alors la variable natproj est égale à 2
                            $shonExistante = 0;
                            $shonDemolie = 0;
                            // Pour chaque ligne du tableau
                            for ($i = 1; $i <= $count_su; $i++) {
                                //
                                $shonExistante += floor($row[$prefix_su.'_avt_shon'.$i]);
                                $shonDemolie += floor($row[$prefix_su.'_demo_shon'.$i]);
                            }
                            //Si la SHON existante avant travaux et la SHON démolie sont 
                            //égales alors la variable natproj est égale à 1
                            if ( $shonExistante == 0 && $shonDemolie == 0 && $natproj != 1 && 
                                    $row['code'] != 'DP' && $row['code'] != 'PA') {
                                $erreur .= _("Dossier ").$row['dossier']." \"".$natprojlib[$natproj]."\" "._("La SHON existante avant travaux et la SHON demolie sont nulles alors cela devrait être une nouvelle construction.")."\n";
                            } elseif ( $shonExistante > $shonDemolie && $natproj != 2 ){
                                $erreur .= _("Dossier ").$row['dossier']." \"".$natprojlib[$natproj]."\" "._("La SHON existante avant travaux ne doit pas être supérieure à la SHON démolie.")."\n";
                            }
                            $decision .= $su_avt_shon;

                            $su_demo_shon = $export_sitadel->shon("demo");
                            //La SHON démolie s'accompagne obligatoirement de la shon 
                            //existante avant travaux
                            if ( $shonDemolie != 0 && $shonExistante == 0 ){
                                $erreur .= _("Dossier ").$row['dossier']." "._("La SHON demolie s'accompagne obligatoirement de la SHON existante avant travaux.")."\n";
                            }
                            $decision .= $su_demo_shon;
                            //
                            $su_sup_shon = $export_sitadel->shon("sup");
                            $su_chge_shon = $export_sitadel->shon("chge");
                            if ( strcasecmp($su_sup_shon, $su_chge_shon) != 0){
                                //
                                $erreur .= _("Dossier ").$row['dossier']." "._("Les SHON globales supprimées par changement de destination ou de sous-destination et créées par le même changement doivent être égales.")."\n";
                            }
                            $decision .= $su_sup_shon;
                            $decision .= $su_chge_shon;
                            $decision .= $export_sitadel->shon("cstr");
                            $decision .= "0|0|0|0|0|0|0|0|0|";
                            // Les SHON créées par changement de destination ou
                            // de sous-destination s'accompagnent obligatoirement
                            // de SHON existante avant travaux non nulle
                            if (preg_match("/[0|]{7}/", $su_chge_shon) &&
                                preg_match("/[0|]{7}/", $su_avt_shon)){

                                $erreur .= _("Dossier ").$row['dossier']." "._("Les SHON créées par changement de destination ou de sous-destination s'accompagnent obligatoirement de SHON existante avant travaux non nulle.")."\n";
                            }
                            // Un nombre de logements démolis strictement positif doit
                            // s'accompagner obligatoirement de SHON démolie.
                            if($row['dm_tot_log_nb'] > 0) {
                                if($export_sitadel->get_shon_val('demo', 1) <= 0) {
                                    //
                                    $erreur .= _("Dossier ").$row['dossier']." "._("Un nombre de logements demolis strictement positif doit s'accompagner obligatoirement de SHON demolie.")."\n";
                                }
                            }
                            // Un nombre de logements créés strictement positif doit
                            // s'accompagner obligatoirement de SHON créée ou de SHON
                            // créée par changement de destination ou de sous-destination 
                            // ayant pour destination l'habitation.
                            if($row['co_tot_log_nb'] > 0 AND ($export_sitadel->get_shon_val('cstr', 1) <= 0 OR $export_sitadel->get_shon_val('chge', 1) <=0)) {
                                //
                                $erreur .= _("Dossier ").$row['dossier']." "._("Un nombre de logements créés strictement positif doit s'accompagner obligatoirement de SHON créée ou de SHON créée par changement de destination ou de sous-destination ayant pour destination l'habitation.")."\n";
                            }

                            // La SHON créée ou issue de la transformation 
                            // suffixée par 9 (intérêt collectif ou service public) doit
                            // obligatoirement s'accompagner de la décomposition
                            // en sous modalité renseignée par la variable cpublic et réciproquement.

                            // Test si une valeur est true
                            $cpublic = FALSE;
                            if (isset($row['co_sp_transport']) && $row['co_sp_transport'] == 't') {
                                $cpublic = TRUE;
                            }                    
                            if (isset($row['co_sp_enseign']) && $row['co_sp_enseign'] == 't') {
                                $cpublic = TRUE;
                            }
                            if (isset($row['co_sp_sante']) && $row['co_sp_sante'] == 't') {
                                $cpublic = TRUE;
                            }
                            if (isset($row['co_sp_act_soc']) && $row['co_sp_act_soc'] == 't') {
                                $cpublic = TRUE;
                            }
                            if (isset($row['co_sp_ouvr_spe']) && $row['co_sp_ouvr_spe'] == 't') {
                                $cpublic = TRUE;
                            }
                            if (isset($row['co_sp_culture']) && $row['co_sp_culture'] == 't') {
                                $cpublic = TRUE;
                            }
                            //
                            if($cpublic !== TRUE AND ($export_sitadel->get_shon_val('cstr', 9) > 0 OR $export_sitadel->get_shon_val('chge', 9) > 0)) {
                                $erreur .= _("Dossier ").$row['dossier']." "._("La SHON créée ou créée par changement de destination ou de sous-destination concernant le service public ou l'interet collectif doit obligatoirement s'accompagner du choix de destination des constructions.")."\n";
                            }

                            // La destination principale du logement mise à résidence
                            // principale ou résidence secondaire doit obligatoirement
                            // s'accompagner d'un mode d'utilisation à occupation personnelle
                            if($row['co_uti_princ'] == 't' OR $row['co_uti_secon'] == 't') {
                                if($row['co_uti_pers'] != 't') {

                                    $erreur .= _("Dossier ").$row['dossier']." "._("La destination principale du logement mise a residence principale ou residence secondaire doit obligatoirement s'accompagner d'un mode d'utilisation a occupation personnelle.")."\n";
                                }
                            }

                            $decision.= $export_sitadel->destination($row['mouvement_sitadel']);

                            // Le nombre total de logements créés (variable nbtotlog)
                            // doit être égal à la somme des nombres de logements créés
                            // ventilés par type de financement
                            if($row['co_tot_log_nb'] != ($row['co_fin_lls_nb'] + $row['co_fin_aa_nb'] +
                               $row['co_fin_ptz_nb'] + $row['co_fin_autr_nb'])) {

                                $erreur .= _("Dossier ").$row['dossier']." "._("Le nombre total de logements crees doit etre egal a la somme des nombres de logements crees ventiles par type de financement.")."\n";
                            }

                            $decision.= $export_sitadel->repartitionFinan();

                            // Le nombre total de logements créés (variable nbtotlog)
                            // doit être égal à la totalisation de la répartition des
                            // logements par nombre de pièces
                            if($row['co_tot_log_nb'] != ($row['co_log_1p_nb'] + $row['co_log_2p_nb'] +
                               $row['co_log_3p_nb'] + $row['co_log_4p_nb'] + $row['co_log_5p_nb'] +
                               $row['co_log_6p_nb'])) {

                                $erreur .= _("Dossier ").$row['dossier']." "._("Le nombre total de logements crees doit etre egal a la totalisation de la repartition des logements par nombre de pieces.")."\n";
                            }

                            $decision.= $export_sitadel->repartitionNbPiece($row['mouvement_sitadel']);
                        }
                        else {
                            //
                            $decision.= str_repeat("|", 6);
                            if($row['code']=='DP'){
                                $decision .= "00000|";
                            }else{
                                $decision .= "|";
                            }
                            $decision .= "0000|00000|";
                            $decision.= str_repeat("|", 74);
                        }
                        $decision.="\n";
                    }

                    // modificatif
                    $modificatif='';
                    if($row['mouvement_sitadel'] == 'MODIFICATIF' AND $row['statut_di']=='cloture' AND
                        $row['typeavis']=='F'
                        and (($row['date_depot'] >= $datedebut
                        and $row['date_depot'] <= $datefin) or 
                        ($row['date_decision'] >= $datedebut
                        and $row['date_decision'] <= $datefin))) {   
                        $modificatif.=$export_sitadel->entete("MODIFICATIF",$departement,$commune, $version);

                        $modificatif.= $export_sitadel->decision_groupe1();

                        if(isset($row['date_decision']) or $row['date_decision']==""){
                            // avis_sitadel et avis_sitadel_motif
                            // si la decision est favorable, envoi des informations statistiques
                            if($row["sitadel"] == 2  or $row["sitadel"] == 4
                                   or $row["sitadel"] == 5){
                                // si accordé : ajout du 2nd groupe d'informations
                                $modificatif .= $export_sitadel->adresse_terrain();    // adresse du terrain
                                $modificatif .= $export_sitadel->parcelle();    // 3 premières parcelles
                                $modificatif .= $export_sitadel->modificatif_terrain();    // Informations complémentaires

                                $modificatif.= $export_sitadel->shon("avt");
                                $modificatif.= $export_sitadel->shon("demo");
                                $modificatif.= $export_sitadel->shon("chge");
                                $modificatif.= $export_sitadel->shon("trsf");
                                $modificatif.= $export_sitadel->shon("cstr");

                                $modificatif.= "|||||||||";
                                $modificatif.= $export_sitadel->destination($row['mouvement_sitadel']);
                                $modificatif.= $export_sitadel->repartitionFinan();
                                $modificatif.= $export_sitadel->repartitionNbPiece($row['mouvement_sitadel']);
                           }
                           else {
                                $modificatif .= str_repeat("|", 90);
                            }
                        }
                        else {
                            $modificatif .= str_repeat("|", 90);
                        }

                        $modificatif.="\n";
                    }

                    // Mouvement suppression
                    $suppression = '';
                    if($row['mouvement_sitadel'] == 'SUPPRESSION') {
                        $suppression .= $export_sitadel->entete("SUPPRESSION",$departement,$commune, $version);
                        $suppression .= "\n";
                    }

                    // Règles sur le mouvement suivi
                    $suivi="";
                    if($row['mouvement_sitadel'] == 'SUIVI' and
                        ($row['date_chantier'] >= $datedebut and $row['date_chantier']<=$datefin) ||
                        ($row['date_achevement'] >= $datedebut and $row['date_achevement']<=$datefin)){
                        // Si le dossier est une DOC
                        if($row['dossier_instruction_type_code']=='DOC'){
                            // Une ouverture de chantier ne peut concerner qu'un permis autorisé
                            if($row['typeavis_da'] != 'F'&&$row['typeavis_da'] != '') {
                                $erreur .= _("Dossier ").$row['dossier']." "._("Une ouverture de chantier ne peut concerner qu'un permis autorise.")."\n";
                            }
                            // La date d'ouverture de chantier doit être supérieur à la date d'autorisation
                            if($row['doc_date'] > $row['date_decision_da']) {
                                $erreur .= _("Dossier ").$row['dossier']." "._("La date d'ouverture de chantier doit être superieur a la date d'autorisation.")."\n";
                            }
                            // Un achèvement de chantier ne peut concerner qu'un permis autorisé
                            if($row['typeavis_da'] != 'F'&&$row['typeavis_da'] != '') {
                                $erreur .= _("Dossier ").$row['dossier']." "._("Un achevement de chantier ne peut concerner qu'un permis autorise.")."\n";
                            }
                            if( $row['date_chantier_da'] == "" && $row['date_achevement']!="") {
                                $erreur .= _("Dossier ").$row['dossier']." "._("Un achevement de chantier ne peut concerner qu'un permis sur lequel un chantier a ete ouvert.")."\n";
                            }
                            // La date d'achevement de travaux doit être supérieur à la date d'ouverture des travaux
                            if($row['daact_date'] > $row['date_chantier_da']) {
                                $erreur .= _("Dossier ").$row['dossier']." "._("La date d'achevement de travaux doit etre superieur a la date d'ouverture des travaux.")."\n";
                            }
                            $suivi.=$export_sitadel->entete("SUIVI",$departement,$commune, $version);//8|
                            $suivi.=$export_sitadel->chantier($row);
                            //On récupère la DAACT si elle existe
                            $sqlDAACT = "SELECT
                                donnees_techniques.daact_date,
                                donnees_techniques.daact_date_chgmt_dest,
                                donnees_techniques.daact_tot_trav,
                                donnees_techniques.daact_tranche_trav,
                                donnees_techniques.daact_tranche_trav_desc,
                                donnees_techniques.daact_surf,
                                donnees_techniques.daact_nb_log,
                                donnees_techniques.daact_nb_log_indiv,
                                donnees_techniques.daact_nb_log_coll,
                                donnees_techniques.daact_nb_log_lls,
                                donnees_techniques.daact_nb_log_aa,
                                donnees_techniques.daact_nb_log_ptz,
                                donnees_techniques.daact_nb_log_autre,
                                etat.statut as statut_di

                                FROM ".DB_PREFIXE."dossier 
                                LEFT JOIN ".DB_PREFIXE."donnees_techniques 
                                    ON dossier.dossier=donnees_techniques.dossier_instruction
                                LEFT JOIN ".DB_PREFIXE."etat
                                    ON dossier.etat = etat.etat
                                LEFT JOIN ".DB_PREFIXE."dossier_instruction_type
                                    ON dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
                                WHERE dossier.dossier_autorisation ='".$row['dossier_autorisation']."' AND 
                                    dossier_instruction_type.code = 'DAACT' and mouvement_sitadel='SUIVI'";
                            $resDAACT = $this->f->db -> query ($sqlDAACT);
                            $this->f->addToLog("dossier.class.php : db->query(\"".$sqlDAACT."\")", VERBOSE_MODE);
                            $this->f->isDatabaseError($resDAACT);
                            $rowDAACT=& $resDAACT->fetchRow(DB_FETCHMODE_ASSOC);
                            $suivi.=$export_sitadel->achevement($rowDAACT);
                            $suivi.="\n";
                        }
                        elseif($row['dossier_instruction_type_code']=='DAACT'){

                            //On vérifie qu'une DOC existe
                            $sqlDOC = "SELECT dossier.dossier, 
                                    dossier.date_chantier,
                                    donnees_techniques.doc_date,
                                    donnees_techniques.terr_div_surf_av_div,
                                    donnees_techniques.doc_tot_trav,
                                    donnees_techniques.doc_tranche_trav,
                                    donnees_techniques.doc_tranche_trav_desc,
                                    donnees_techniques.doc_surf,
                                    donnees_techniques.doc_nb_log,
                                    donnees_techniques.doc_nb_log_indiv,
                                    donnees_techniques.doc_nb_log_coll,
                                    donnees_techniques.doc_nb_log_lls,
                                    donnees_techniques.doc_nb_log_aa,
                                    donnees_techniques.doc_nb_log_ptz,
                                    donnees_techniques.doc_nb_log_autre
                                FROM ".DB_PREFIXE."dossier 
                                LEFT JOIN ".DB_PREFIXE."donnees_techniques 
                                    ON dossier.dossier=donnees_techniques.dossier_instruction
                                LEFT JOIN ".DB_PREFIXE."dossier_instruction_type 
                                    ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
                                WHERE dossier.dossier_autorisation ='".$row['dossier_autorisation']."' AND 
                                    dossier_instruction_type.code = 'DOC' and mouvement_sitadel='SUIVI'";
                            $resDOC = $this->f->db -> query ($sqlDOC);
                            $this->f->addToLog("dossier.class.php : db->query(\"".$sqlDOC."\")", VERBOSE_MODE);
                            $this->f->isDatabaseError($resDOC);
                            $rowDOC=& $resDOC->fetchRow(DB_FETCHMODE_ASSOC);

                            if((isset($rowDOC['dossier']) && 
                                $rowDOC['date_chantier']<$datedebut || $rowDOC['date_chantier']>$datefin) ||
                                !isset($rowDOC['dossier'])) {
                                //
                                $suivi.=$export_sitadel->entete("SUIVI",$departement,$commune, $version);//8|
                                $suivi.=$export_sitadel->chantier($rowDOC);
                                $suivi.=$export_sitadel->achevement($row);
                                $suivi.="\n";                    
                            }
                        }
                    }          
                    // export       
                    $export.=$depot.$decision.$transfert.$modificatif.$suivi.$suppression;
                } // fin while  

                /**
                 *
                 */
                //
                if (DBCHARSET == 'UTF8') {
                    $export = utf8_decode($export);
                }

                /**
                 * En-tête de fichier.
                 *
                 * C'est la première ligne du fichier.
                 */
                // on éclate la chaîne export par ligne pour calculer le nombre 
                // d'enregistrements et la longueur maximale des enregistrements
                $export_by_line = explode("\n", $export);
                // longueur maximale des enregistrements
                // (Num)(6) longueur de l’enregistrement le plus long contenu dans le 
                // fichier (sans compter la fin d’enregistrement ou la fin de fichier)
                $longueur_maximale_des_enregistrements = 0;
                foreach ($export_by_line as $export_line) {
                    if ($longueur_maximale_des_enregistrements > strlen($export_line)) {
                        continue;
                    }
                    $longueur_maximale_des_enregistrements = strlen($export_line);
                }
                // nombre d'enregistrements
                // (Num)(6) nombre d’enregistrements présents dans le fichier en 
                // comptant l’en-tête du fichier
                // XXX Ne faut-il pas ajouter +1 pour la ligne d'en-tête ?
                $nombre_d_enregistrements = count($export_by_line);
                // code application 
                // (Alphanum)(7) = SITADEL
                $code_application = "SITADEL";
                // code département
                // (Alphanum)(3) département dans lequel se trouve le service instructeur
                // nomenclature : 001 à 095, 02A, 02B, 971...974
                $code_departement = $this->f->getParameter("departement");
                // service expéditeur
                // (Alphanum)(3) DDE ou commune (la plus grosse en cas d'EPCI) ou DGI
                // nomenclature : 'ADS', ‘DGI ou code commune INSEE
                $service_expediteur = $this->f->getParameter("commune");
                // service destinataire
                // (Alphanum)(2) DRE
                // nomenclature : code région INSEE (exemple : Ile-de-France=11)
                $service_destinataire = $this->f->getParameter("region");
                // code du fichier transmis
                // (AlphaNum)(12) AAMMjjdddccc
                // ddd = département du service instructeur
                // ccc = code du service expéditeur
                // AAMMjj = date
                // par exemple : 090531093ADS dans le cas de la transmission mensuelle 
                // des événements intervenus au mois de mai communiqués par la DDE de 
                // Seine-Saint-Denis.
                // XXX La date du jour correspond bien à la date demandée ?
                $code_du_fichier_transmis = sprintf(
                    "%s%s%s",
                    date('ymd'),
                    $code_departement,
                    $service_expediteur
                );
                // numéro d'ordre
                // (AlphaNum)(1) numéro d’ordre du fichier en cas de rectificatif
                // XXX Le formulaire propose jusqu'à la valeur 10 alors que la taille
                //     de la châine doit être (1) ?
                $numero_d_ordre = $this->f->get_submitted_post_value("numero");
                // date de création
                // (Alphanum)(6) AAMMjj date de création du fichier transmis
                $date_de_creation = date('ymd');
                // nom de l'applicatif expéditeur
                // (Alphanum)(20) Exemple : GESTIO
                $nom_de_l_applicatif_expediteur = "openADS";
                // version de l'applicatif expéditeur
                // (Alphanum)(8) Exemple : 2.05
                $version_de_l_applicatif_expediteur = substr($this->f->version, 0, 8);
                // Consititution de la ligne d'en-tête.
                $entete = sprintf(
                    "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
                    $code_application,
                    $code_departement,
                    $service_expediteur,
                    $service_destinataire,
                    $code_du_fichier_transmis,
                    $numero_d_ordre,
                    $longueur_maximale_des_enregistrements,
                    $date_de_creation,
                    $nombre_d_enregistrements,
                    $nom_de_l_applicatif_expediteur,
                    $version_de_l_applicatif_expediteur
                );

                /**
                 *
                 */
                //
                $export = $entete.$export;

                /**
                 * Écriture de l'export dans un fichier sur le disque et affichage du 
                 * lien de téléchargement.
                 */
                // Composition du nom du fichier
                $nom_fichier = "SITADEL".substr($this->f->get_submitted_post_value("datedebut"),3,2)."".substr($this->f->get_submitted_post_value("datedebut"),8,4).".txt";
                // Composition des métadonnées du fichier
                $metadata_fichier = array(
                    "filename" => $nom_fichier,
                    "size" => strlen($export),
                    "mimetype" => "application/vnd.ms-excel",
                );
                // Écriture du fichier
                $uid_fichier = $this->f->storage->create_temporary($export, $metadata_fichier);
                //
                $message = _("Fichier")." ".$nom_fichier." "._("sauvegarde");
                $message .= "<br/>";
                $message .= _("Pour telecharger le fichier, cliquer ici :");
                $message .= " ";
                $message .= "<a href=\"../spg/file.php?uid=".$uid_fichier."&amp;mode=temporary\" target=\"_blank\">";
                $message .= "<img src=\"../app/img/ico_trace.png\"";
                $message .= " alt=\""._("Telecharger le fichier Sitadel")."\"";
                $message .= " title=\""._("Telecharger le fichier Sitadel")."\"";
                $message .= " />";
                $message .= "</a>";
                $message .= _("avec le bouton droit et enregistrer la cible du lien sous.");
                //
                $this->f->displayMessage("ok", $message);

                /**
                 * Écriture d'une éventuelle erreur durant l'export dans un fichier sur
                 * le disque et affichage du lien de téléchargement.
                 */
                //
                if ($erreur != "") {
                    // Composition du nom du fichier
                    $nom_fichier_erreur = "probleme_".$nom_fichier;
                    // Composition des métadonnées du fichier
                    $metadata_fichier_erreur = array(
                        "filename" => $nom_fichier_erreur,
                        "size" => strlen($erreur),
                        "mimetype" => "text/plain",
                    );
                    // Écriture du fichier
                    $uid_fichier_erreur = $this->f->storage->create_temporary($erreur, $metadata_fichier_erreur);
                    //
                    $message = _("Un ou plusieurs problèmes de cohérence ont été détectés durant l'export, celles-ci sont listees dans le fichiers ci-dessous");
                    $message .= "<br/>";
                    $message .= _("Fichier")." ".$nom_fichier_erreur." "._("sauvegarde");
                    $message .= "<br/>";
                    $message .= _("Pour telecharger le fichier, cliquer ici :");
                    $message .= " ";
                    $message .= "<a href=\"../spg/file.php?uid=".$uid_fichier_erreur."&amp;mode=temporary\" target=\"_blank\">";
                    $message .= "<img src=\"../app/img/ico_trace.png\"";
                    $message .= " alt=\""._("Telecharger le fichier d'erreur")."\"";
                    $message .= " title=\""._("Telecharger le fichier d'erreur")."\"";
                    $message .= " />";
                    $message .= "</a>";
                    $message .= _("avec le bouton droit et enregistrer la cible du lien sous.");
                    //
                    $this->f->displayMessage("info", $message);
                }
                
                if (DEBUG > 0) {
                    printf($export);
                }
                // Appel de la méthode d'affichage du lien retour par le layout
                $this->f->layout->display_form_retour($params);

            } else {// correct = false
                $this->affichageFormulaire($this->f->get_submitted_post_value("datedebut"), $this->f->get_submitted_post_value("datefin"), $numero, _("Les champs dates sont obligatoires"));
            }
        }
    }


    function affichageFormulaire($datedebut="", $datefin="", $numero="", $msg=null){
        //Description de la page
        $description = _("Cette page vous permet de Transmettre les dossiers suivant la procedure SITADEL");
        $this->f->displayDescription($description);
        // Affichage du message d'erreur si existe
        if($msg!==null){
            $this->f->displayMessage("error", $msg);
        }
        //
        printf("<form method=\"POST\" action=\"form.php?obj=sitadel&action=6&idx=0\" name=f1>");
        //
        $input = "<input type=\"text\" name=\"%s\" id=\"%s\" value=\"%s\" size=\"15\" class=\"champFormulaire datepicker\" onchange=\"fdate(this)\"/>";
        // champ date debut
        printf(" "._("debut")." ");
        printf($input, "datedebut", "datedebut", $datedebut);
        // champ date fin
        printf(" "._("fin")." ");
        printf($input, "datefin", "datefin", $datefin);
        // numero d'ordre d'envoi
        printf(" "._("Numero d'ordre d'envoi")." : "."<select name=\"numero\">");
        for ($i = 1; $i < 11; $i++) {
            printf("<option value =\"%d\" ", $i);
            printf(($numero == $i )? "selected=\"selected\"":"");
            printf(">%d</option>", $i);
        }
        printf("</select>");
        printf("<br/><br/><input type=\"submit\" value=\"export SITADEL\" >");
        printf("</form>");
     }
    
    /**
     * VIEW - view_document_numerise.
     *
     * Vue du tableau des pièces du dossier d'autorisation.
     *
     * Cette vue permet de gérer le contenu de l'onglet "Pièce(s)" sur un 
     * dossier d'autorisation. Cette vue spécifique est nécessaire car
     * l'ergonomie standard du framework ne prend pas en charge ce cas.
     * C'est ici la vue spécifique des pièces liées au dossier qui est
     * affichée directement au clic de l'onglet au lieu du soustab.
     * 
     * L'idée est donc de simuler l'ergonomie standard en créant un container 
     * et d'appeler la méthode javascript 'ajaxit' pour charger le contenu 
     * de la vue visualisation de l'objet lié.
     * 
     * @return void
     */
    function view_document_numerise() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Récupération des variables GET
        ($this->f->get_submitted_get_value('idxformulaire')!==null ? $idxformulaire = 
            $this->f->get_submitted_get_value('idxformulaire') : $idxformulaire = "");
        ($this->f->get_submitted_get_value('retourformulaire')!==null ? $retourformulaire = 
            $this->f->get_submitted_get_value('retourformulaire') : $retourformulaire = "");
        // Objet à charger
        $obj = "document_numerise";
        $type_aff_form = $this->get_type_affichage_formulaire();
        if ($type_aff_form === 'CTX RE' OR $type_aff_form === 'CTX IN') {
            $obj = "document_numerise_contexte_ctx";
        }
        // Construction de l'url de sousformulaire à appeler
        $url = "../scr/sousform.php?obj=".$obj;
        $url .= "&idx=".$idxformulaire;
        $url .= "&action=4";
        $url .= "&retourformulaire=".$retourformulaire;
        $url .= "&idxformulaire=".$idxformulaire;
        $url .= "&retour=form";
        // Affichage du container permettant le reffraichissement du contenu
        // dans le cas des action-direct.
        printf('
            <div id="sousform-href" data-href="%s">
            </div>',
            $url
        );
        // Affichage du container permettant de charger le retour de la requête
        // ajax récupérant le sous formulaire.
        printf('
            <div id="sousform-%s">
            </div>
            <script>
            ajaxIt(\'%s\', \'%s\');
            </script>',
            $obj,
            $obj,
            $url
        );
    }


    /*
     * Définition de la version et du suffixe du DI lors de sa création.
     */
    function setValFAjout($val = array()) {

        //
        // GESTION DE LA VERSION DU DI
        // Elle est unique et basée sur celle du DA qui débute à -1.
        // Ainsi la version du DI initial est à 0.
        // 
        $numeroVersion = $this->getNumeroVersion($val['dossier_autorisation']);
        if (is_numeric($numeroVersion) or $numeroVersion == -1){
            $this->incrementNumeroVersion($val['dossier_autorisation'], ++$numeroVersion);
        }

        //
        // GESTION DU SUFFIXE
        // La version du suffixe est celle du type de DI : à ne pas confondre
        // avec celle du DI lui même.
        // Exemple chronologique :
        // DI n° PC0130551600004 -> version 0
        // DI n° PC0130551600004M01 -> version 1
        // DI n° PC0130551600004PRO01 -> version 2 !!
        // 
        $suffixe = "";
        // Si l'option suffixe de ce type de DI est activée
        if ( $this->getSuffixe($this->getDossierInstructionType()) === 't' ){
            // Récupération de la lettre associée au type de dossier d'instruction
            $code = $this->getCode($this->getDossierInstructionType());
            // Récupération du numéro de version en fonction du type de dossier d'instruction
            $numeroVersionDossierInstructionType = $this->getNumeroVersionDossierInstructionType($val['dossier_autorisation'], $val['dossier_instruction_type'], $numeroVersion);
            // Suffixe
            $suffixe = $code.$numeroVersionDossierInstructionType;
        }
        
        // Numéro de dossier
        $this->valF['dossier'] = $val['dossier_autorisation'].$suffixe;
        // Identifiant du dossier d'instruction lisible
        $this->valF['dossier_libelle'] = $this->get_dossier_autorisation_libelle($val['dossier_autorisation']).$suffixe;
        // Version du dossier
        $this->valF['version'] = $numeroVersion;
    }

    /*Récupère la valeur du suffixe d'un dossier_instruction_type*/
    function getSuffixe($dossierInstructionType){
        
        $suffixe = "";
        
        $sql = "SELECT 
                    suffixe
                FROM 
                    ".DB_PREFIXE."dossier_instruction_type 
                WHERE 
                    dossier_instruction_type = $dossierInstructionType";
                    
        $this->addToLog("getSuffixe(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $res = $this->db->query($sql);
        if (database :: isError($res))
            die($res->getMessage()."erreur ".$sql);
            
        if ( $res->numRows() > 0 ){
            
            $row = $res->fetchRow(DB_FETCHMODE_ASSOC);
            $suffixe =  $row['suffixe'];
        }
        
        return $suffixe;
    }
    
    /*Récupère dans la table de paramètrage la lettre correspondant 
     * au dossier_instruction_type
     */
    function getCode($dossierInstructionType){
        
        $code = "";
        
        $sql = "SELECT 
                    code 
                FROM 
                    ".DB_PREFIXE."dossier_instruction_type 
                WHERE 
                    dossier_instruction_type = $dossierInstructionType";
                    
        $this->addToLog("getCode(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $res = $this->db->query($sql);
        if (database :: isError($res))
            die($res->getMessage()."erreur ".$sql);
            
        if ( $res->numRows() > 0 ){
            
            $row = $res->fetchRow(DB_FETCHMODE_ASSOC);
            $code =  $row['code'];
        }
        
        return $code;
    }
    
    /*Récupère le numéro de version d'un dossier_autorisation*/
    function getNumeroVersion($dossierAutorisation){
        
        $numeroVersion = "";
        
        $sql = "SELECT 
                    numero_version
                FROM 
                    ".DB_PREFIXE."dossier_autorisation 
                WHERE 
                    dossier_autorisation = '$dossierAutorisation'";
                    
        $this->addToLog("getNumeroVersion(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $res = $this->db->query($sql);
        if (database :: isError($res))
            die($res->getMessage()."erreur ".$sql);
            
        if ( $res->numRows() > 0 ){
            
            $row = $res->fetchRow(DB_FETCHMODE_ASSOC);
            $numeroVersion =  $row['numero_version'];
        }
        
        return $numeroVersion;
    }

    /**
     * Retourne le numéro d'un dossier d'instruction et ses six parties.
     *
     * @param string $dossier_instruction Identifiant du dossier d'instruction (avec ou sans espace)
     *
     * @return array
     */
    function get_dossier_instruction_numero($dossier_instruction = null) {

        // Si le DI n'a pas été fourni
        if ($dossier_instruction === null) {
            // On le récupère
            $dossier_instruction = $this->getVal($this->clePrimaire);
        }

        /*
            Analyse du numéro du DI et extraction de ses parties

            Retour : tableau à 7 entrées, chacune étant elle-même un tableau à 1 entrée :

            - 0 : chaîne testée (identifiant du DI)
            - 1 : type du DA (2 ou 3 lettres)
            - 2 : code département + code communes(3 + 3 integer)
            - 3 : année (2 integer)
            - 4 : division + numéro (le premier peut être une lettre ou un
            - integer et le reste integer)
            - 5 : lettre(s) du type du dossier d'instruction (P, M, ANNUL, ...)
            - 6 : version du dossier d'instruction (1 ou 2 integer)
         */
        preg_match_all('|^([A-Z]{2,3})\s{0,1}(\d{6})\s{0,1}(\d{2})\s{0,1}([[:alnum:]]{5})([A-Z]{1,5})(\d{1,2})$|', $dossier_instruction, $return);
        //
        return $return;
    }

    /**
     * Récupère le numéro de version du dossier d'instruction.
     *
     * @return string
     */
    function get_dossier_instruction_version($dossier_instruction = null) {

        // Expression régulière qui découpe le numéro du dossier
        $return = $this->get_dossier_instruction_numero($dossier_instruction);

        // Si l'expression régulière retourne une erreur
        if ($return == false) {
            // Message dans le log
            $this->f->addToLog(__METHOD__."(): "._("Erreur lors de la recuperation du numero de version du dossier d'instruction"), DEBUG_MODE);
            //
            return false;
        }

        // Retourne seulement la version du dossier d'instruction.
        // Elle vaut 0 si le numéro du DI n'a pas de suffixe.
        if (isset($return[6][0])) {
            return $return[6][0];
        }
        return 0;
    }
    
    /*Incrémente le numéro de version du dossier*/
    function incrementNumeroVersion($dossierAutorisation, $nouveauNumeroVersion) {
        
        $valF = array (
                    "numero_version" => $nouveauNumeroVersion
                );
        
        $res = $this->db->autoExecute(DB_PREFIXE."dossier_autorisation", 
                                    $valF, 
                                    DB_AUTOQUERY_UPDATE, 
                                    "dossier_autorisation = '$dossierAutorisation'");
                                    
        if (database :: isError($res))
            die($res->getMessage()."erreur ".$sql);
            
    }

    /**
     * Retourne un numéro de version en fonction du type de dossier d'instruction
     * @param string $dossier_autorisation
     * @param integer $dossier_instruction_type
     * @return int
     */
    public function getNumeroVersionDossierInstructionType($dossier_autorisation, $dossier_instruction_type, $numero_version, $increment = true){
        
        $numeroVersionDossierInstructionType = $numero_version;
        
        //On récupère le code correspondant au type de dossier d'instruction passé
        //en paramètre
        $sql = "SELECT 
                code
            FROM
                ".DB_PREFIXE."dossier_instruction_type
            WHERE
                dossier_instruction_type = ".$dossier_instruction_type;
        $codeDossierInstructionType = $this->db->getOne($sql);       
        $this->f->addToLog("getNumeroVersionDossierInstructionType(): db->getOne(\"".$sql."\")", VERBOSE_MODE);
        if ( database::isError($codeDossierInstructionType)){
            $this->f->addToError("", $codeDossierInstructionType, $codeDossierInstructionType);
            return false;
        }
        
        
        // Si c'est un dossier d'instruction de type "Initial", code "P", on retourne 0
        if ( strcmp($codeDossierInstructionType, "P") == 0 ){
            return 0;
        }
        //Si c'est un modificatif ou transfert on retourne un nombre correspondant au
        //nombre de dossier d'instruction de ce type, rattaché au dossier 
        //d'autorisation complété par des 0 à gauche si besoin. Format du retour 
        //attendu : 01 ou 02, etc. 
        else {
            
            //On récupère le nombre de dossier d'instruction de ce type rattaché au 
            //dossier d'autorisation
            $sql = "SELECT 
                    count(dossier) 
                FROM 
                    ".DB_PREFIXE."dossier
                LEFT JOIN
                    ".DB_PREFIXE."dossier_autorisation
                    ON
                        dossier_autorisation.dossier_autorisation = dossier.dossier_autorisation
                WHERE
                    dossier_autorisation.dossier_autorisation = '".$dossier_autorisation."'
                    AND
                    dossier.dossier_instruction_type = ".$dossier_instruction_type;
            $numeroVersionDossierInstructionType = $this->db->getOne($sql);
            $this->f->addToLog("getNumeroVersionDossierInstructionType(): db->getOne(\"".$sql."\")", VERBOSE_MODE);
            if ( database::isError($numeroVersionDossierInstructionType)){
                $this->f->addToError("", $numeroVersionDossierInstructionType, $numeroVersionDossierInstructionType);
                return false;
            }
            
            // Requête SQL
            $sql = "SELECT
                        substring(dossier, '\d*$')::int as last_num_dossier
                    FROM ".DB_PREFIXE."dossier
                    WHERE dossier_instruction_type = ".$dossier_instruction_type."
                    AND dossier_autorisation = '".$dossier_autorisation."'
                    AND version = (
                        SELECT max(version) 
                        FROM ".DB_PREFIXE."dossier 
                        WHERE dossier_instruction_type = ".$dossier_instruction_type."
                        AND dossier_autorisation = '".$dossier_autorisation."' 
                        GROUP BY dossier_instruction_type, dossier_autorisation
                    )";
            $res = $this->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            $row =& $res->fetchRow(DB_FETCHMODE_ASSOC);

            $num_version_last_dossier = $row['last_num_dossier'];

            if (!empty($num_version_last_dossier)
                && $num_version_last_dossier >= $numeroVersionDossierInstructionType) {
                // Modifie le numéro suivant
                $numeroVersionDossierInstructionType = $num_version_last_dossier;
            }
            //
            if ($increment === true) {
                $numeroVersionDossierInstructionType = ++$numeroVersionDossierInstructionType;
            }
            //On compléte par des 0 à gauche
            $numeroVersionDossierInstructionType = str_pad($numeroVersionDossierInstructionType, 2, "0", STR_PAD_LEFT);
            
            return $numeroVersionDossierInstructionType;
        }
    }
    /**
     * Retourne le libellé du dossier d'autorisation
     * @param  string $dossier_autorisation Identifiant du dossier d'autorisation
     * @return string                       Libellé dossier d'autorisation
     */
    function get_dossier_autorisation_libelle($dossier_autorisation) {

        $dossier_autorisation_libelle = "";

        // Requête SQL
        $sql = "SELECT 
                    dossier_autorisation_libelle
                FROM 
                    ".DB_PREFIXE."dossier_autorisation 
                WHERE 
                    dossier_autorisation = '$dossier_autorisation'";

        $dossier_autorisation_libelle = $this->db->getOne($sql);       
        $this->addToLog("get_dossier_autorisation_libelle(): db->getOne(\"".$sql."\")", VERBOSE_MODE);
        database::isError($dossier_autorisation_libelle);
        
        // Retourne le résultat
        return $dossier_autorisation_libelle;
    }
    
    function setvalF($val = array()){
        parent::setvalF($val);

        // Récupération des id demandeurs postés
        $this->getPostedValues();

        // enlever les valeurs a ne pas saisir -> recherche en trigger ajouter et modifier
        unset ($this->valF['geom']);
        unset ($this->valF['geom1']);
        // valeurs hiddenstatic (calcule)
        if($this->maj==1){
            // par defaut
            unset ($this->valF['etat']);
            unset ($this->valF['delai']);
            unset ($this->valF['accord_tacite']);
        }
        unset ($this->valF['avis_decision']); // avis + libelle avis
        unset ($this->valF['terrain_surface_calcul']);
        unset ($this->valF['shon_calcul']);
        unset ($this->valF['date_notification_delai']);
        unset ($this->valF['date_decision']);
        unset ($this->valF['date_limite']);
        unset ($this->valF['date_validite']);
        unset ($this->valF['date_chantier']);
        unset ($this->valF['date_achevement']);
        unset ($this->valF['date_conformite']);
        // Ce champ est mis à jour uniquement par la gestion spécifique du log
        // et donc jamais par les actions ajouter/modifier
        unset ($this->valF['log_instructions']);

        // Durée de validité lors de la création du dossier d'instruction
        $this->valF['duree_validite'] = $this->get_duree_validite($this->valF['dossier_autorisation']);
    }


    /**
     * Retourne le type de formulaire : ADS, CTX RE, CTX IN ou DPC.
     *
     * @return mixed $type_aff_form Type de formulaire (string) ou false (bool) si erreur BDD.
     */
    function get_type_affichage_formulaire() {
        if (isset($this->type_aff_form) === true) {
            return $this->type_aff_form;
        }
        //
        if($this->getParameter('maj') === '0' OR $this->get_action_crud() === 'create') {
            $id_dossier_instruction_type = $this->valF["dossier_instruction_type"];
        } else {
            $id_dossier_instruction_type = $this->getVal("dossier_instruction_type");
        }
        $sql = "SELECT dossier_autorisation_type.affichage_form
                FROM " . DB_PREFIXE . "dossier_instruction_type
                INNER JOIN " . DB_PREFIXE . "dossier_autorisation_type_detaille
                    ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
                INNER JOIN " . DB_PREFIXE . "dossier_autorisation_type
                    ON dossier_autorisation_type.dossier_autorisation_type=dossier_autorisation_type_detaille.dossier_autorisation_type
            WHERE dossier_instruction_type.dossier_instruction_type=" . intval($id_dossier_instruction_type);
        $type_aff_form = $this->db->getOne($sql);
        $this->f->addToLog(__METHOD__ . " : db->getOne(\"" . $sql . "\")", VERBOSE_MODE);
        if($this->f->isDatabaseError($type_aff_form, true) === true) {
            return false;
        }
        $this->type_aff_form = $type_aff_form;
        //
        return $this->type_aff_form;
    }


    /**
     * Retourne le code du groupe du dossier d'instruction.
     *
     * @return string
     */
    public function get_groupe() {
        //
        if (isset($this->groupe) === true && $this->groupe !== null) {
            return $this->groupe;
        }

        // Récupère le code du groupe
        $inst_dossier_autorisation_type_detaille = $this->get_inst_dossier_autorisation_type_detaille();
        $inst_dossier_autorisation_type = $this->get_inst_dossier_autorisation_type($inst_dossier_autorisation_type_detaille->getVal('dossier_autorisation_type'));
        $inst_groupe = $this->get_inst_groupe($inst_dossier_autorisation_type->getVal('groupe'));
        $groupe = $inst_groupe->getVal('code');

        //
        $this->groupe = $groupe;
        //
        return $this->groupe;
    }


    /**
     * Méthode de verification du contenu
     */
    function verifier($val = array(), &$db = null, $DEBUG = null) {
        parent::verifier($val,$db,$DEBUG);

        // La date de dépôt est obligatoire
        if ($val['date_depot'] === '' || $val['date_depot'] === null) {
            //
            $this->correct = false;
            $this->addToMessage( _('Le champ').' <span class="bold">'.$this->getLibFromField('date_depot').'</span> '._('est obligatoire'));
        }

        $type_aff_form = $this->get_type_affichage_formulaire();
        if ($type_aff_form ===false) {
            $this->correct = false;
            $this->addToMessage(_("Une erreur s'est produite lors de l'ajout de ce dossier. Veuillez contacter votre administrateur."));
        }

        switch ($type_aff_form) {
            case 'ADS':
            case 'CTX RE':
                if (!isset($this->postedIdDemandeur["petitionnaire_principal"]) OR
                   empty($this->postedIdDemandeur["petitionnaire_principal"]) AND
                   !is_null($this->form)) {
                    $this->correct = false;
                    $this->addToMessage(_("La saisie d'un petitionnaire principal est obligatoire."));
                }
                break;
            case 'CTX IN':
                if (!isset($this->postedIdDemandeur["contrevenant_principal"]) OR
                   empty($this->postedIdDemandeur["contrevenant_principal"]) AND
                   !is_null($this->form)) {
                    $this->correct = false;
                    $this->addToMessage(_("La saisie d'un contrevenant principal est obligatoire."));
                }
                break;
            case 'DPC':
                if(!isset($this->postedIdDemandeur["petitionnaire_principal"]) OR
                   empty($this->postedIdDemandeur["petitionnaire_principal"]) AND
                   !is_null($this->form)) {
                    $this->correct = false;
                    $this->addToMessage(_("La saisie d'un petitionnaire principal est obligatoire."));
                }
                if(!isset($this->postedIdDemandeur["bailleur_principal"]) OR
                   empty($this->postedIdDemandeur["bailleur_principal"]) AND
                   !is_null($this->form)) {
                    $this->correct = false;
                    $this->addToMessage(_("La saisie d'un bailleur principal est obligatoire."));
                }
                break;
        }

        // Récupération du crud par rapport au mode du formulaire
        $crud = $this->get_action_crud($this->getParameter("maj"));

        // L'année de la date de dépot ne peut pas être modifiée
        if ($crud === 'update' && array_key_exists("date_depot", $val) === true && ($val["date_depot"] !== "" && $val["date_depot"] !== null)) {
            //
            $new_date = DateTime::createFromFormat('d/m/Y', $val["date_depot"]);
            $old_date = DateTime::createFromFormat('Y-m-d', $this->getVal("date_depot"));
            if ($new_date->format("Y") != $old_date->format("Y")) {
                $this->addToMessage(_("L'année de la date de dépôt n'est pas modifiable."));
                $this->correct = false;
            }
        }
    }


    function setType(&$form,$maj) {
        // Par défaut le type des champs est géré nativement par le framework
        parent::setType($form,$maj);

        // Récupération du contexte : groupe, CRUD et paramètres
        $groupe = $this->get_type_affichage_formulaire();
        $crud = $this->get_action_crud($maj);
        $parameters = $this->f->getCollectivite($this->getVal('om_collectivite'));

        //
        // Gestion du groupe
        //

        // INFRACTION
        $inf_fields = array(
            'a_qualifier' => 'hidden',
            'autorisation_contestee' => 'hidden',
            'autorite_competente' => 'hidden',
            'cle_acces_citoyen' => 'hidden',
            'contrevenants' => 'static',
            'date_ait' => 'hiddenstaticdate',
            'date_audience' => 'hiddenstaticdate',
            'date_cloture_instruction' => 'hidden',
            'date_complet' => 'hidden',
            'date_contradictoire' => 'hiddenstaticdate',
            'date_decision' => 'hiddenstaticdate',
            'date_depot' => 'hiddenstaticdate',
            'date_dernier_depot' => 'hidden',
            'date_derniere_visite' => 'hiddenstaticdate',
            'date_limite' => 'hidden',
            'date_limite_incompletude' => 'hidden',
            'date_premiere_visite' => 'hiddenstaticdate',
            'date_transmission_parquet' => 'hiddenstaticdate',
            'date_validite' => 'hidden',
            'delai' => 'hidden',
            'description_projet' => 'hidden',
            'dossier_autorisation_type_detaille' => 'hidden',
            'dossier_instruction_type' => 'hidden',
            'dossier_petitionnaire' => 'hidden',
            'dossier_petitionnaires' => 'hidden',
            'dt_ctx_infraction' => 'checkboxhiddenstatic',
            'dt_ctx_regularisable' => 'checkboxhiddenstatic',
            'dt_ctx_synthese_anr' => 'static',
            'dt_ctx_synthese_nti' => 'static',
            'enjeu_ctx' => 'hidden',
            'enjeu_erp' => 'hidden',
            'enjeu_urba' => 'hidden',
            'erp' => 'hidden',
            'evenement_suivant_tacite' => 'hidden',
            'evenement_suivant_tacite_incompletude' => 'hidden',
            'numero_versement_archive' => 'hidden',
            'requerants' => 'hidden',
            'tax_mtn_part_commu' => 'hidden',
            'tax_mtn_part_depart' => 'hidden',
            'tax_mtn_part_reg' => 'hidden',
            'tax_mtn_total' => 'hidden',
            'tax_mtn_rap' => 'hidden',
            'tax_secteur' => 'hidden',
            'tax_mtn_part_commu_sans_exo' => 'hidden',
            'tax_mtn_part_depart_sans_exo' => 'hidden',
            'tax_mtn_part_reg_sans_exo' => 'hidden',
            'tax_mtn_total_sans_exo' => 'hidden',
            'tax_mtn_rap_sans_exo' => 'hidden',
            'bailleur' => 'hidden',
        );
        // RECOURS
        $re_fields = array(
            'a_qualifier' => 'hidden',
            'autorite_competente' => 'hidden',
            'cle_acces_citoyen' => 'hidden',
            'contrevenants' => 'hidden',
            'date_ait' => 'hidden',
            'date_audience' => 'hidden',
            'date_cloture_instruction' => 'hiddenstaticdate',
            'date_complet' => 'hidden',
            'date_contradictoire' => 'hidden',
            'date_decision' => 'hiddenstaticdate',
            'date_depot' => 'hiddenstaticdate',
            'date_dernier_depot' => 'hidden',
            'date_derniere_visite' => 'hidden',
            'date_limite' => 'hiddenstaticdate',
            'date_limite_incompletude' => 'hidden',
            'date_premiere_visite' => 'hidden',
            'date_transmission_parquet' => 'hidden',
            'date_validite' => 'hidden',
            'delai' => 'hidden',
            'description_projet' => 'hidden',
            'dossier_autorisation_type_detaille' => 'static',
            'dossier_instruction_type' => 'hidden',
            'dossier_petitionnaire' => 'hidden',
            'dossier_petitionnaires' => 'static',
            'dt_ctx_infraction' => 'hidden',
            'dt_ctx_regularisable' => 'hidden',
            'dt_ctx_synthese_anr' => 'hidden',
            'dt_ctx_synthese_nti' => 'hidden',
            'enjeu_ctx' => 'hidden',
            'enjeu_erp' => 'hidden',
            'enjeu_urba' => 'hidden',
            'erp' => 'hidden',
            'evenement_suivant_tacite' =>'selecthiddenstatic',
            'evenement_suivant_tacite_incompletude' => 'hidden',
            'instructeur_2' => 'hidden',
            'numero_versement_archive' => 'hidden',
            'requerants' => 'static',
            'tax_mtn_part_commu' => 'hidden',
            'tax_mtn_part_depart' => 'hidden',
            'tax_mtn_part_reg' => 'hidden',
            'tax_mtn_total' => 'hidden',
            'tax_mtn_rap' => 'hidden',
            'tax_secteur' => 'hidden',
            'tax_mtn_part_commu_sans_exo' => 'hidden',
            'tax_mtn_part_depart_sans_exo' => 'hidden',
            'tax_mtn_part_reg_sans_exo' => 'hidden',
            'tax_mtn_total_sans_exo' => 'hidden',
            'tax_mtn_rap_sans_exo' => 'hidden',
            'bailleur' => 'hidden',
        );
        // ADS
        $ads_fields = array(
            'autorisation_contestee' => 'hidden',
            'cle_acces_citoyen' => 'static',
            'contrevenants' => 'hidden',
            'date_ait' => 'hidden',
            'date_audience' => 'hidden',
            'date_cloture_instruction' => 'hidden',
            'date_contradictoire' => 'hidden',
            'date_derniere_visite' => 'hidden',
            'date_premiere_visite' => 'hidden',
            'date_transmission_parquet' => 'hidden',
            'dossier_autorisation_type_detaille' => 'hidden',
            'dossier_instruction_type' => 'selecthiddenstatic',
            'dossier_petitionnaires' => 'hidden',
            'dt_ctx_infraction' => 'hidden',
            'dt_ctx_regularisable' => 'hidden',
            'dt_ctx_synthese_anr' => 'hidden',
            'dt_ctx_synthese_nti' => 'hidden',
            'enjeu_ctx' => 'static',
            'instructeur_2' => 'hidden',
            'requerants' => 'hidden',
            'bailleur' => 'hidden',
        );
        // DPC
        $dpc_fields = array(
            'autorisation_contestee' => 'hidden',
            'cle_acces_citoyen' => 'static',
            'contrevenants' => 'hidden',
            'date_ait' => 'hidden',
            'date_audience' => 'hidden',
            'date_cloture_instruction' => 'hidden',
            'date_contradictoire' => 'hidden',
            'date_derniere_visite' => 'hidden',
            'date_premiere_visite' => 'hidden',
            'date_transmission_parquet' => 'hidden',
            'dossier_autorisation_type_detaille' => 'hidden',
            'dossier_instruction_type' => 'selecthiddenstatic',
            'dossier_petitionnaires' => 'hidden',
            'dt_ctx_infraction' => 'hidden',
            'dt_ctx_regularisable' => 'hidden',
            'dt_ctx_synthese_anr' => 'hidden',
            'dt_ctx_synthese_nti' => 'hidden',
            'enjeu_ctx' => 'static',
            'instructeur_2' => 'hidden',
            'requerants' => 'hidden',
            'bailleur' => 'static',
        );
        // COMMUN
        $all_fields = array(
            'accord_tacite' => 'hidden',
            'annee' => 'hidden',
            'autres_demandeurs' => 'hidden',
            'date_achevement' => 'hidden',
            'date_chantier' => 'hidden',
            'date_conformite' => 'hidden',
            'date_notification_delai' => 'hidden',
            'date_rejet' => 'hidden',
            'date_retour_contradictoire' => 'hidden',
            'delai_incompletude' => 'hidden',
            'description' => 'hidden',
            'dossier' => 'hidden',
            'dossier_arrondissement' => 'hidden',
            'dossier_autorisation' => 'hidden',
            'dossier_autorisation_libelle' => 'hidden',
            'duree_validite' => 'hidden',
            'etat_pendant_incompletude' => 'hidden',
            'geom' => 'hiddenstatic',
            'geom1' => 'hidden',
            'incomplet_notifie' => 'hidden',
            'incompletude' => 'hidden',
            'interface_referentiel_erp' => 'hidden',
            'log_instructions' => 'nodisplay',
            'om_collectivite' => 'hidden',
            'quartier' => 'hidden',
            'version' => 'hidden',
        );
        if (isset($parameters['option_afficher_division'])
            && $parameters['option_afficher_division'] !== 'true') {
            $all_fields['division'] = 'hidden';
        }
        if (isset($parameters['option_sig'])
            && $parameters['option_sig'] !== 'sig_interne'
            && $parameters['option_sig'] !== 'sig_externe'){
            $all_fields['geom'] = 'hidden';
        }

        //
        // Gestion du contexte
        //

        // AJOUTER, MODIFIER
        if ($crud === 'create' OR $crud === 'update') {

            $all_fields['avis_decision'] = 'selecthiddenstatic';
            $all_fields['etat'] = 'hiddenstatic';
            $all_fields['terrain'] = 'hiddenstatic';
            //
            $ads_fields['a_qualifier'] = 'checkbox';
            $ads_fields['accord_tacite'] = 'hiddenstatic';
            $ads_fields['autorite_competente'] = 'selecthiddenstatic';
            $ads_fields['date_achevement'] = 'hiddenstaticdate';
            $ads_fields['date_chantier'] = 'hiddenstaticdate';
            $ads_fields['date_conformite'] = 'hiddenstaticdate';
            $ads_fields['date_decision'] = 'hiddenstaticdate';
            $ads_fields['date_depot'] = 'hiddenstaticdate';
            $ads_fields['date_dernier_depot'] = 'hiddenstaticdate';
            $ads_fields['date_notification_delai'] = 'hiddenstaticdate';
            $ads_fields['date_rejet'] = 'hiddenstaticdate';
            $ads_fields['date_validite'] = 'hiddenstaticdate';
            $ads_fields['delai'] = 'hiddenstatic';
            $ads_fields['description_projet'] = 'hiddenstatic';
            $ads_fields['dossier_autorisation_type_detaille'] = 'hiddenstatic';
            $ads_fields['shon_calcul'] = 'hiddenstatic';
            $ads_fields['tax_mtn_part_commu'] = 'hidden';
            $ads_fields['tax_mtn_part_depart'] = 'hidden';
            $ads_fields['tax_mtn_part_reg'] = 'hidden';
            $ads_fields['tax_mtn_total'] = 'hidden';
            $ads_fields['tax_mtn_rap'] = 'hidden';
            $ads_fields['tax_mtn_part_commu_sans_exo'] = 'hidden';
            $ads_fields['tax_mtn_part_depart_sans_exo'] = 'hidden';
            $ads_fields['tax_mtn_part_reg_sans_exo'] = 'hidden';
            $ads_fields['tax_mtn_total_sans_exo'] = 'hidden';
            $ads_fields['tax_mtn_rap_sans_exo'] = 'hidden';
            $ads_fields['terrain_surface_calcul'] = 'hiddenstatic';
            //
            $dpc_fields['a_qualifier'] = 'checkbox';
            $dpc_fields['accord_tacite'] = 'hiddenstatic';
            $dpc_fields['autorite_competente'] = 'selecthiddenstatic';
            $dpc_fields['date_achevement'] = 'hiddenstaticdate';
            $dpc_fields['date_chantier'] = 'hiddenstaticdate';
            $dpc_fields['date_conformite'] = 'hiddenstaticdate';
            $dpc_fields['date_decision'] = 'hiddenstaticdate';
            $dpc_fields['date_depot'] = 'hiddenstaticdate';
            $dpc_fields['date_dernier_depot'] = 'hiddenstaticdate';
            $dpc_fields['date_notification_delai'] = 'hiddenstaticdate';
            $dpc_fields['date_rejet'] = 'hiddenstaticdate';
            $dpc_fields['date_validite'] = 'hiddenstaticdate';
            $dpc_fields['delai'] = 'hiddenstatic';
            $dpc_fields['description_projet'] = 'hiddenstatic';
            $dpc_fields['dossier_autorisation_type_detaille'] = 'hiddenstatic';
            $dpc_fields['shon_calcul'] = 'hiddenstatic';
            $dpc_fields['tax_mtn_part_commu'] = 'hidden';
            $dpc_fields['tax_mtn_part_depart'] = 'hidden';
            $dpc_fields['tax_mtn_part_reg'] = 'hidden';
            $dpc_fields['tax_mtn_total'] = 'hidden';
            $dpc_fields['tax_mtn_rap'] = 'hidden';
            $dpc_fields['tax_mtn_part_commu_sans_exo'] = 'hidden';
            $dpc_fields['tax_mtn_part_depart_sans_exo'] = 'hidden';
            $dpc_fields['tax_mtn_part_reg_sans_exo'] = 'hidden';
            $dpc_fields['tax_mtn_total_sans_exo'] = 'hidden';
            $dpc_fields['tax_mtn_rap_sans_exo'] = 'hidden';
            $dpc_fields['terrain_surface_calcul'] = 'hiddenstatic';
            //
            $re_fields['dossier_autorisation_type_detaille'] = 'hiddenstatic';
            $re_fields['dossier_petitionnaires'] = 'hiddenstatic';
            $re_fields['requerants'] = 'hiddenstatic';
            $inf_fields['contrevenants'] = 'hiddenstatic';
            $inf_fields['dt_ctx_synthese_anr'] = 'hiddenstatic';
            $inf_fields['dt_ctx_synthese_nti'] = 'hiddenstatic';
            // Si l'état du dossier est incomplet
            if ($this->is_incomplet_notifie()) {
                // On cache les dates de complétude et de limite d'instruction
                $ads_fields['date_complet'] = 'hidden';
                $ads_fields['date_limite'] = 'hidden';
                $ads_fields['evenement_suivant_tacite_incompletude'] ='selecthiddenstatic';
                $ads_fields['evenement_suivant_tacite'] ='hidden';
                $ads_fields['date_limite_incompletude'] = 'hiddenstaticdate';
                //
                $dpc_fields['date_complet'] = 'hidden';
                $dpc_fields['date_limite'] = 'hidden';
                $dpc_fields['evenement_suivant_tacite_incompletude'] ='selecthiddenstatic';
                $dpc_fields['evenement_suivant_tacite'] ='hidden';
                $dpc_fields['date_limite_incompletude'] = 'hiddenstaticdate';
            } else {
                // Sinon on cache la date de limite d'incomplétude
                $ads_fields['date_limite_incompletude'] = 'hidden';
                $ads_fields['evenement_suivant_tacite_incompletude'] ='hidden';
                $ads_fields['evenement_suivant_tacite'] ='selecthiddenstatic';
                $ads_fields['date_complet'] = 'hiddenstaticdate';
                $ads_fields['date_limite'] = 'hiddenstaticdate';
                //
                $dpc_fields['date_limite_incompletude'] = 'hidden';
                $dpc_fields['evenement_suivant_tacite_incompletude'] ='hidden';
                $dpc_fields['evenement_suivant_tacite'] ='selecthiddenstatic';
                $dpc_fields['date_complet'] = 'hiddenstaticdate';
                $dpc_fields['date_limite'] = 'hiddenstaticdate';
            }

            // MODIFIER
            if ($crud ==='update') {
                $all_fields['dossier_libelle'] = 'hiddenstatic';
                $ads_fields['dossier_petitionnaire'] = 'hiddenstatic';
                $dpc_fields['dossier_petitionnaire'] = 'hiddenstatic';
                //
                if ($this->f->isAccredited("dossier_modifier_instructeur")) {
                    $all_fields['instructeur'] =  'select';
                    $inf_fields['instructeur_2'] =  'select';
                } else {
                    $all_fields['instructeur'] =  'selecthiddenstatic';
                    $inf_fields['instructeur_2'] =  'selecthiddenstatic';
                }
                //
                if (isset($parameters['option_afficher_division'])
                    && $parameters['option_afficher_division'] === 'true') {
                    $all_fields['division'] =  'selecthiddenstatic';
                    if ($this->f->isAccredited("dossier_modifier_division")) {
                        $all_fields['division'] =  'select';
                    }
                }
                // Si l'utilisateur ne peut pas qualifier un DI (guichet unique & guichet et suivi)
                if (!$this->f->isAccredited(array("dossier_instruction","dossier_instruction_qualifier"), "OR")) {
                    // Il ne peut pas modifier les champs suivants
                    $ads_fields['numero_versement_archive'] =  'hiddenstatic';
                    $ads_fields['enjeu_urba'] =  'checkboxhiddenstatic';
                    $ads_fields['enjeu_erp'] =  'checkboxhiddenstatic';
                    $ads_fields['erp'] =  'checkboxhiddenstatic';
                    $ads_fields['a_qualifier'] =  'checkboxhiddenstatic';
                    //
                    $dpc_fields['numero_versement_archive'] =  'hiddenstatic';
                    $dpc_fields['enjeu_urba'] =  'checkboxhiddenstatic';
                    $dpc_fields['enjeu_erp'] =  'checkboxhiddenstatic';
                    $dpc_fields['erp'] =  'checkboxhiddenstatic';
                    $dpc_fields['a_qualifier'] =  'checkboxhiddenstatic';
                }
                // Le profil Qualificateur peut modifier seulement les champs
                // autorite_competente, a_qualifier et erp
                if ($this->f->isUserQualificateur()) {
                    $ads_fields['numero_versement_archive'] =  'static';
                    $ads_fields['enjeu_urba'] =  'checkboxstatic';
                    $ads_fields['enjeu_erp'] =  'checkboxstatic';
                    //
                    $dpc_fields['numero_versement_archive'] =  'static';
                    $dpc_fields['enjeu_urba'] =  'checkboxstatic';
                    $dpc_fields['enjeu_erp'] =  'checkboxstatic';
                }
                // Le dossier ne doit pas être instruit
                if ($this->has_only_recepisse() === true
                    && $this->getStatut() !== 'cloture') {
                    //
                    $all_fields['date_depot'] = 'date';
                }
            }
        }
        // MODIFIER, SUPPRIMER, CONSULTER
        if ($crud !== 'create') {
            $re_fields['autorisation_contestee'] = 'selecthiddenstatic';
            $all_fields['date_demande'] = 'hidden';
            // La collectivité n'est jamais modifiable
            if ($_SESSION['niveau'] == 2) {
                $all_fields['om_collectivite'] = 'selecthiddenstatic';
            }
            // Instance du paramétrage des taxes
            $inst_taxe_amenagement = $this->get_inst_taxe_amenagement();
            // Instance de cerfa
            $inst_cerfa = $this->get_inst_cerfa();
            // Si l'option de simulation est activée pour la collectivité du
            // dossier, l'utilisateur connecté a la permissions de voir
            // la simulation des taxes, la collectivité à un paramétrage pour
            // les taxes et que le cerfa du dossier à les champs requis
            if ($this->f->is_option_simulation_taxes_enabled($this->getVal('om_collectivite')) === true
                && $this->f->isAccredited("dossier_instruction_simulation_taxes") === true
                && $inst_taxe_amenagement !== null
                && $inst_cerfa->can_simulate_taxe_amenagement() === true) {

                // MODIFIER
                if ($crud === 'update') {
                    //
                    if ($this->is_in_context_of_foreign_key("tax_secteur", $this->retourformulaire)) {
                        $ads_fields['tax_secteur'] = 'selecthiddenstatic';
                        $dpc_fields['tax_secteur'] = 'selecthiddenstatic';
                    } else {
                        $ads_fields['tax_secteur'] = 'select';
                        $dpc_fields['tax_secteur'] = 'select';
                    }
                }
                // SUPPRIMER
                if ($crud === 'delete') {
                    //
                    $ads_fields['tax_secteur'] = 'selectstatic';
                    $dpc_fields['tax_secteur'] = 'selectstatic';
                }
                // CONSULTER
                if ($crud === 'read') {
                    //
                    $ads_fields['tax_secteur'] = 'selectstatic';
                    $dpc_fields['tax_secteur'] = 'selectstatic';
                }
                // Si ce n'est pas une commune d'Île-de-France
                if ($inst_taxe_amenagement->getVal('en_ile_de_france') == 'f') {
                    //
                    $ads_fields['tax_mtn_part_reg'] = 'hidden';
                    $ads_fields['tax_mtn_part_reg_sans_exo'] = 'hidden';
                    $dpc_fields['tax_mtn_part_reg'] = 'hidden';
                    $dpc_fields['tax_mtn_part_reg_sans_exo'] = 'hidden';
                }
            } else {
                // MODIFER
                if ($crud === 'update') {
                    $ads_fields['tax_secteur'] = 'hidden';
                    $dpc_fields['tax_secteur'] = 'hidden';
                }
                // SUPPRIMER, CONSULTER
                if ($crud === 'delete' OR $crud ==='read') {
                    $ads_fields['tax_secteur'] = 'hidden';
                    $ads_fields['tax_mtn_part_commu'] = 'hidden';
                    $ads_fields['tax_mtn_part_depart'] = 'hidden';
                    $ads_fields['tax_mtn_part_reg'] = 'hidden';
                    $ads_fields['tax_mtn_total'] = 'hidden';
                    $ads_fields['tax_mtn_rap'] = 'hidden';
                    $ads_fields['tax_mtn_part_commu_sans_exo'] = 'hidden';
                    $ads_fields['tax_mtn_part_depart_sans_exo'] = 'hidden';
                    $ads_fields['tax_mtn_part_reg_sans_exo'] = 'hidden';
                    $ads_fields['tax_mtn_total_sans_exo'] = 'hidden';
                    $ads_fields['tax_mtn_rap_sans_exo'] = 'hidden';
                    //
                    $dpc_fields['tax_secteur'] = 'hidden';
                    $dpc_fields['tax_mtn_part_commu'] = 'hidden';
                    $dpc_fields['tax_mtn_part_depart'] = 'hidden';
                    $dpc_fields['tax_mtn_part_reg'] = 'hidden';
                    $dpc_fields['tax_mtn_total'] = 'hidden';
                    $dpc_fields['tax_mtn_rap'] = 'hidden';
                    $dpc_fields['tax_mtn_part_commu_sans_exo'] = 'hidden';
                    $dpc_fields['tax_mtn_part_depart_sans_exo'] = 'hidden';
                    $dpc_fields['tax_mtn_part_reg_sans_exo'] = 'hidden';
                    $dpc_fields['tax_mtn_total_sans_exo'] = 'hidden';
                    $dpc_fields['tax_mtn_rap_sans_exo'] = 'hidden';
                }
            }
        }
        // SUPPRIMER, CONSULTER
        if ($crud === 'delete' OR $crud ==='read') {

            $ads_fields['geom'] = 'static';
            $ads_fields['a_qualifier'] =  'checkboxstatic';
            $ads_fields['terrain_references_cadastrales'] = 'referencescadastralesstatic';
            //
            $dpc_fields['geom'] = 'static';
            $dpc_fields['a_qualifier'] =  'checkboxstatic';
            $dpc_fields['terrain_references_cadastrales'] = 'referencescadastralesstatic';
            // Si l'état du dossier est incomplet
            if ($this->is_incomplet_notifie()) {
                // on cache les dates de complétude et de limite d'instruction
                $ads_fields['date_complet'] =  'hidden';
                $ads_fields['date_limite'] =  'hidden';
                $ads_fields['evenement_suivant_tacite_incompletude'] = 'selecthiddenstatic';
                $ads_fields['evenement_suivant_tacite'] = 'hidden';
                //
                $dpc_fields['date_complet'] =  'hidden';
                $dpc_fields['date_limite'] =  'hidden';
                $dpc_fields['evenement_suivant_tacite_incompletude'] = 'selecthiddenstatic';
                $dpc_fields['evenement_suivant_tacite'] = 'hidden';
            } else {
                // sinon on cache la date de limite d'incomplétude
                $ads_fields['date_limite_incompletude'] =  'hidden';
                $ads_fields['evenement_suivant_tacite_incompletude'] = 'hidden';
                $ads_fields['evenement_suivant_tacite'] = 'selecthiddenstatic';
                //
                $dpc_fields['date_limite_incompletude'] =  'hidden';
                $dpc_fields['evenement_suivant_tacite_incompletude'] = 'hidden';
                $dpc_fields['evenement_suivant_tacite'] = 'selecthiddenstatic';
            }
            if (isset($parameters['option_arrondissement'])
                && $parameters['option_arrondissement'] === 'true') {
                $all_fields['dossier_arrondissement'] = 'static';
            }
        }

        //
        // Typage
        //

        switch ($groupe) {
            case 'CTX IN':
                $this->manage_type($form, $inf_fields);
                break;
            case 'CTX RE':
                $this->manage_type($form, $re_fields);
                break;
            case 'ADS':
                $this->manage_type($form, $ads_fields);
                break;
            case 'DPC':
                $this->manage_type($form, $dpc_fields);
                break;
        }
        $this->manage_type($form, $all_fields);
    }


    /**
     * Gestion du typage des champs
     *
     * @param  object  $form    formulaire instancié
     * @param  array   $fields  tableau associatif des champs avec leur widget de formulaire en valeur
     * @return void
     */
    protected function manage_type($form, $fields) {
        foreach ($this->champs as $key => $field) {
            if (array_key_exists($field, $fields) === true) {
                $form->setType($field, $fields[$field]);
            }
        }
    }


    /**
     * Retourne le nombre de parcelles qu'à en commun le dossier passé en
     * paramètre avec les dossiers contentieux en cours. Le nombre de parcelles
     * est groupé par type de dossier d'autorisation : RE ou IN.
     *
     * @param string $di identifiant du DI
     * @return array
     */
    function get_nb_parcelles_dossier_ciblees_par_contentieux($di) {
        $sql = "
            SELECT
              dossier_autorisation_type.code, COUNT(*) as nb
            FROM " . DB_PREFIXE . "dossier
                LEFT JOIN " . DB_PREFIXE . "dossier_parcelle
                    ON dossier.dossier = dossier_parcelle.dossier
                LEFT JOIN " . DB_PREFIXE . "dossier_parcelle as parcelle_ctx
                    ON dossier_parcelle.libelle = parcelle_ctx.libelle
                    AND dossier_parcelle.dossier != parcelle_ctx.dossier
                LEFT JOIN " . DB_PREFIXE . "dossier as dossier_ctx
                    ON dossier_ctx.dossier = parcelle_ctx.dossier
                INNER JOIN " . DB_PREFIXE . "etat
                    ON dossier_ctx.etat = etat.etat AND etat.statut != 'cloture'
                LEFT JOIN " . DB_PREFIXE . "dossier_autorisation
                    ON dossier_ctx.dossier_autorisation = dossier_autorisation.dossier_autorisation
                LEFT JOIN " . DB_PREFIXE . "dossier_autorisation_type_detaille
                    ON dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
                        = dossier_autorisation.dossier_autorisation_type_detaille
                LEFT JOIN " . DB_PREFIXE . "dossier_autorisation_type
                    ON dossier_autorisation_type_detaille.dossier_autorisation_type
                        = dossier_autorisation_type.dossier_autorisation_type
            WHERE
                dossier.dossier = '" . $di . "'
                AND (dossier_autorisation_type.code = 'RE'
                    OR dossier_autorisation_type.code = 'IN')
            GROUP BY dossier_autorisation_type.code
        ";
        $this->addToLog("get_nb_parcelles_dossier_ciblees_par_contentieux : db->query(\"$sql\")", VERBOSE_MODE);
        $res = $this->db->query($sql);
        $this->f->isDatabaseError($res);
        
        $nb_re_inf = array('re' => 0, 'inf' => 0);

        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
            if ($row["code"] == "RE"){
                $nb_re_inf['re'] += $row["nb"];
            }
            if ($row["code"] == "IN"){
                $nb_re_inf['inf'] += $row["nb"];
            }
        }

        return $nb_re_inf;
    }

    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        $this->maj=$maj;

        if($this->getVal('geom') != "" 
            && $this->f->getParameter('option_sig') == 'sig_externe' 
            && $this->f->issetSIGParameter($this->getVal('dossier')) === true) {
            $form->setVal('geom', $this->getGeolocalisationLink());
        }
        $affichage_form = $this->get_type_affichage_formulaire();
        if ($affichage_form === 'ADS') {
            if ($maj == 3) {
            
                $nb_re_inf = $this->get_nb_parcelles_dossier_ciblees_par_contentieux($this->getVal('dossier'));

                $message = "";
                if ($nb_re_inf['re'] > 0) {
                    $message .= "<span class=\"label label-warning\" ";
                    $message .= "title=\""._("Au moins un dossier de recours contentieux ou gracieux en cours concerne les références cadastrales du dossier courant.")."\">";
                    $message .= _("RE");
                    $message .= "</span>";
                    if ($nb_re_inf['inf'] > 0) {
                        $message .= " ";
                    }
                }
                if ($nb_re_inf['inf'] > 0) {
                    $message .= "<span class=\"label label-important\" ";
                    $message .= "title=\""._("Au moins un dossier d'infraction en cours concerne les références cadastrales du dossier courant.")."\">";
                    $message .= _("IN");
                    $message .= "</span>";
                }

                $form->setVal('enjeu_ctx', $message);
            }
        }
        if ($affichage_form === 'CTX RE') {
            // Récupération des demandeurs liés au dossier
            $this->listeDemandeur("dossier", $this->getVal('dossier'));
            //
            $requerants = '';
            if ($this->getVal('requerants') != '') {
                $requerants = $this->getVal('requerants');
                if (isset($this->valIdDemandeur['requerant']) === true
                    AND count($this->valIdDemandeur['requerant']) > 0) {
                    $requerants .= ' '._('et autres');
                }
            }
            $form->setVal('requerants', $requerants);
            //
            $dossier_petitionnaires = '';
            if ($this->getVal('dossier_petitionnaire') != '') {
                $dossier_petitionnaires = $this->getVal('dossier_petitionnaire');
            }
            if (isset($this->valIdDemandeur['petitionnaire']) === true
                AND count($this->valIdDemandeur['petitionnaire']) > 0) {
                $dossier_petitionnaires .= ' '._('et autres');
            }
            $form->setVal('dossier_petitionnaires', $dossier_petitionnaires);
        }
        if ($affichage_form === 'CTX IN') {
            // Récupération des demandeurs liés au dossier
            $this->listeDemandeur("dossier", $this->getVal('dossier'));
            //
            $contrevenants = '';
            if ($this->getVal('contrevenants') != '') {
                $contrevenants = $this->getVal('contrevenants');
                if (isset($this->valIdDemandeur['contrevenant']) === true
                    AND count($this->valIdDemandeur['contrevenant']) > 0) {
                    $contrevenants .= ' '._('et autres');
                }
            }
            $form->setVal('contrevenants', $contrevenants);
        }
        if ($affichage_form === 'DPC') {
            // Récupération des demandeurs liés au dossier
            $this->listeDemandeur("dossier", $this->getVal('dossier'));
            //
            $bailleurs = '';
            if ($this->getVal('bailleurs') != '') {
                $bailleurs = $this->getVal('bailleurs');
                if (isset($this->valIdDemandeur['bailleur']) === true
                    AND count($this->valIdDemandeur['bailleur']) > 0) {
                    $bailleurs .= ' '._('et autres');
                }
            }
            $form->setVal('bailleurs', $bailleurs);
        }
        if ($validation == 0) {
            if ($maj == 0){
              
                $form->setVal('annee', date('y'));
                $form->setVal('date_demande', date('Y-m-d'));
                $form->setVal('date_depot', date('Y-m-d'));
              
                //$form->setVal('demandeur_cp', $dossier_cp);
                //$form->setVal('demandeur_ville', $dossier_ville);
              
                //$form->setVal('delegataire_cp', $dossier_cp);
                //$form->setVal('delegataire_ville', $dossier_ville);
              
                //$form->setVal('terrain_cp', $dossier_cp);
                //$form->setVal('terrain_ville', $dossier_ville);
              
                $form->setVal('accord_tacite', 'Non');
                $form->setVal('etat', 'initialiser');
            }
        }
    }


    /**
     * getGeolocalisationLink retourne le code HTML affichant l'icone du globe, ainsi que
     * les coordonnées du centroide du dossier, le tout étant un lien vers le SIG.
     *
     * @return string Lien vers le SIG
     */
    function getGeolocalisationLink() {
        //
        $link = "<a id='action-form-localiser'".
                " target='_SIG' href='../scr/form.php?obj=dossier_instruction&action=140&idx=".$this->getVal("dossier")."'>".
                "<span class='om-icon om-icon-16 om-icon-fix sig-16' title='Localiser'>Localiser</span> ".
                $this->getVal('geom').
                " </a>";
        return $link;
    }


    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        // XXX Commenté pour patcher le problème de montée en charge de la base
        // de données en cas de reprise de données d'un gros volume de dossier
        // d'instruction
        //parent::setSelect($form, $maj, $db, $debug);

        //optimisation sur table importante parcelle -> pas d appel methode parent
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
                include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");

        // om_collectivite
        $this->init_select($form, $this->f->db, $maj, null, "om_collectivite", $sql_om_collectivite, $sql_om_collectivite_by_id, false);

        /* 
         *  Pour chaque init_select d'un select non modifiable on teste
         *  si l'on est en mode modifier : si c'est le cas alors on initialise le
         *  select en mode consulter (qui n'affiche rien s'il n'y a aucune valeur).
         */

        $collectivite_idx = $this->getVal("om_collectivite");
        $affichage_form_dat = "";
        // Si recherche avancée om_collectivite = collectivité utilisateur
        if($maj == 999) {
            $collectivite_idx = $this->f->getParameter("om_collectivite_idx");
        } else {
            $affichage_form_dat = $this->get_type_affichage_formulaire();
        }
        // Définition de la qualité et de la traduction de l'instructeur
        switch ($affichage_form_dat) {
            case 'DPC':
            case 'ADS':
                $affichage_instr = "AND instructeur_qualite.code = 'instr'";
                $lib_instructeur = _("l'instructeur");
                break;
            case 'CTX RE':
            case 'CTX IN':
                $affichage_instr = "AND instructeur_qualite.code = 'juri'";
                $lib_instructeur = _('le juriste');
                break;
            default:
                $affichage_instr = "";
                $lib_instructeur = _("l'instructeur");
                break;
        }


        // instructeur
        // on recupère les services des multicollectivités et de celle du DI
        if($this->f->getParameter('option_afficher_division')==='true') {
            // instructeur
            $sql_instructeur_div_by_di = str_replace('<collectivite_di>', $collectivite_idx, $sql_instructeur_div_by_di);
            $sql_instructeur_div_by_di = str_replace('<instructeur_qualite>', $affichage_instr, $sql_instructeur_div_by_di);
            $this->init_select($form, $db, $maj, $debug, "instructeur",
            $sql_instructeur_div_by_di, $sql_instructeur_div_by_id, true, false, $lib_instructeur);
            
            $sql_instructeur_2_div_by_di = str_replace('<collectivite_di>', $collectivite_idx, $sql_instructeur_2_div_by_di);
            $this->init_select($form, $db, $maj, $debug, "instructeur_2",
            $sql_instructeur_2_div_by_di, $sql_instructeur_2_div_by_id, true, false, _('le technicien'));
            
        } else {
            $sql_instructeur_by_di = str_replace('<collectivite_di>', $collectivite_idx, $sql_instructeur_by_di);
            $sql_instructeur_by_di = str_replace('<instructeur_qualite>', $affichage_instr, $sql_instructeur_by_di);
            $this->init_select($form, $db, $maj, $debug, "instructeur",
                                   $sql_instructeur_by_di, $sql_instructeur_by_id, true, false, $lib_instructeur);
            $sql_instructeur_2_by_di = str_replace('<collectivite_di>', $collectivite_idx, $sql_instructeur_2_by_di);
            $this->init_select($form, $db, $maj, $debug, "instructeur_2",
                                   $sql_instructeur_2_by_di, $sql_instructeur_2_by_id, true, false, _('le technicien'));
        }

        // etat
        if ($maj == 1) {
            $this->init_select($form, $db, 3, $debug, "etat",
                           $sql_etat, $sql_etat_by_id, false);
        } else {
            $this->init_select($form, $db, $maj, $debug, "etat",
                               $sql_etat, $sql_etat_by_id, false);
        }

        // dossier_instruction_type
        $this->init_select($form, $db, $maj, $debug, "dossier_instruction_type",
                           $sql_dossier_instruction_type, $sql_dossier_instruction_type_by_id, false);

        // division
        $sql_division_by_di = str_replace('<collectivite_di>', $collectivite_idx, $sql_division_by_di);
        $this->init_select($form, $db, $maj, $debug, "division",
                               $sql_division_by_di, $sql_division_by_id, false);

        // autorite_competente
        $this->init_select($form, $db, $maj, $debug, "autorite_competente",
                               $sql_autorite_competente, $sql_autorite_competente_by_id, false);

        // avis_decision
        if ($maj == 1) {
            $this->init_select($form, $db, 3, $debug, "avis_decision",
                           $sql_avis_decision, $sql_avis_decision_by_id, false);
        } else {
            $this->init_select($form, $db, $maj, $debug, "avis_decision",
                               $sql_avis_decision, $sql_avis_decision_by_id, false);
        }

        // autorisation_contestee
        if ($affichage_form_dat === 'CTX RE'
            && ($maj == 1 || $maj == 3)) {
            // À exécuter seulement en mode modifier ou consulter des recours
            // pour éviter le ralentissement de l'affichage des listings des DI
            $this->init_select($form, $db, $maj, $debug, "autorisation_contestee", $sql_autorisation_contestee, $sql_autorisation_contestee_by_id, false);
        }

        // evenement_suivant_tacite
        $this->init_select($form, $db, $maj, $debug, "evenement_suivant_tacite",
                               $sql_evenement_suivant_tacite, $sql_evenement_suivant_tacite_by_id, false);

        // evenement_suivant_tacite_incompletude
        $this->init_select($form, $db, $maj, $debug, "evenement_suivant_tacite_incompletude",
                               $sql_evenement_suivant_tacite_incompletude, $sql_evenement_suivant_tacite_incompletude_by_id, false);

        // Ajout, modification et recherche avancée
        if($maj == 0 || $maj == 1 || $maj == 999){
            // accord tacite
            $contenu=array();
            $contenu[0]=array('Non','Oui');
            $contenu[1]=array('Non','Oui');
            $form->setSelect("accord_tacite",$contenu);
            
            // geom *** a voir
            if($maj==1){ //modification
                $contenu=array();
                $contenu[0]=array("dossier",$this->getParameter("idx"));
                $form->setSelect('geom',$contenu);
            }
            // arrondissement recherche avancée
            $this->init_select($form, $db, $maj, $debug, "arrondissement",
                           $sql_arrondissement, $sql_arrondissement_by_id, false);
            // dossier_autorisation_type_detaille recherche avancée
            $this->init_select($form, $db, $maj, $debug, "dossier_autorisation_type_detaille",
                           $sql_dossier_autorisation_type_detaille, $sql_dossier_autorisation_type_detaille_by_id, false);
            
        }

        // Ce formulaire n'est pas accessible en ajout ni en recherche avancée
        // mais dans le cas où il le serait, rien ne doit être fait concernant
        // les taxes
        if ($maj != 0 && $maj != 999) {

            // Instance du parmétrage des taxes
            $inst_taxe_amenagement = $this->get_inst_taxe_amenagement();
            // Si la colletivité à un paramétrage pour la taxe d'aménagement
            if ($inst_taxe_amenagement !== null) {

                // Choix du secteur pour part communale
                $contenu = array();
                $contenu[0][0] = "";
                $contenu[1][0] = _('choisir')."&nbsp;"._("tax_secteur");

                // Il y a 20 secteurs maximum dans une commune de France
                for ($i=1; $i < 21; $i++) {

                    // Valeur du secteur
                    $value = $inst_taxe_amenagement->getVal('tx_comm_secteur_'.$i);

                    //
                    if ($value !== null && $value !== '') {
                        //
                        $contenu[0][$i] = $i;
                        $contenu[1][$i] = sprintf(_('Secteur %s'), $i);
                    }
                }
                //
                $form->setSelect("tax_secteur", $contenu);
            }
        }
    }

    function setLib(&$form,$maj) {
        parent::setLib($form,$maj);
        $affichage_form = $this->get_type_affichage_formulaire();
        if ($affichage_form === 'ADS') {
            $form->setLib('date_decision', _("date de la decision"));
            $form->setLib('date_limite', _("limite d'instruction"));
        }
        if ($affichage_form === 'CTX IN') {
            $form->setLib('avis_decision', _("Décision"));
            $form->setLib('date_cloture_instruction', _("Date de clôture d'instruction"));
            $form->setLib('date_decision', _("Date de décision"));
            $form->setLib('date_depot', _("Date de réception"));
            $form->setLib('date_limite', _("Tacicité"));
            $form->setLib('instructeur',_('Juriste'));
        }
        if ($affichage_form === 'CTX RE') {
            $form->setLib('autorisation_contestee', _("Autorisation contestée"));
            $form->setLib('avis_decision', _("Décision"));
            $form->setLib('date_cloture_instruction', _("Date de clôture d'instruction"));
            $form->setLib('date_decision', _("Date de décision"));
            $form->setLib('date_depot', _("Date de recours"));
            $form->setLib('date_limite', _("Tacicité"));
            $form->setLib('instructeur',_('Juriste'));
        }
        if ($affichage_form === 'DPC') {
            //
        }
        $form->setLib('accord_tacite',_("decision tacite"));
        $form->setLib('autorite_competente',_('competence'));
        $form->setLib('cle_acces_citoyen', _("cle_acces_citoyen"));
        $form->setLib('date_ait', _("Date d'AIT"));
        $form->setLib('date_audience', _("Date d'audience"));
        $form->setLib('date_complet', _("completude"));
        $form->setLib('date_contradictoire', _("Date de contradictoire"));
        $form->setLib('date_dernier_depot', _("dernier depot"));
        $form->setLib('date_derniere_visite', _("Date de dernière visite"));
        $form->setLib('date_limite_incompletude', _("limite d'instruction"));
        $form->setLib('date_premiere_visite', _("Date de 1ère visite"));
        $form->setLib('date_transmission_parquet', _('Date de transmission au Parquet'));
        $form->setLib('date_validite', _("fin de validite le"));
        $form->setLib('delai', _("delai (mois)"));
        $form->setLib('delai',_("delai d'instruction"));
        $form->setLib('description_projet',_('description du projet'));
        $form->setLib('dossier_arrondissement', _("Arrondissement"));
        $form->setLib('dossier_autorisation_libelle',_('dossier_autorisation_libelle'));
        $form->setLib('dossier_autorisation_type_detaille', _("Type"));
        $form->setLib('contrevenants', _("Contrevenant(s)"));
        $form->setLib('dossier_instruction_type',_('type de dossier'));
        $form->setLib('dossier_petitionnaire',_('demandeur'));
        $form->setLib('dossier_petitionnaires', _("Pétitionnaire(s)"));
        $form->setLib('requerants', _("Requérant(s)"));
        $form->setLib('dt_ctx_infraction', _("Infraction"));
        $form->setLib('dt_ctx_regularisable', _("Régularisable"));
        $form->setLib('dt_ctx_synthese_anr', _("Synthèse des ANR"));
        $form->setLib('dt_ctx_synthese_nti', _("Synthèse des NTI"));
        $form->setLib('enjeu_ctx', _("contentieux"));
        $form->setLib('enjeu_erp', _("ERP"));
        $form->setLib('enjeu_urba', _("urbanisme"));
        $form->setLib('erp', _("ERP"));
        $form->setLib('geom',_('geolocalisaion'));
        $form->setLib('instructeur_2', _('Technicien'));
        $form->setLib('numero_versement_archive', _("numero"));
        $form->setLib('bailleur', _("Bailleur(s)"));
        $form->setLib('terrain', _("Localisation"));
    }

    function setOnchange(&$form,$maj){
        parent::setOnchange($form,$maj);
        // mise en majuscule
        $form->setOnchange("demandeur_nom","this.value=this.value.toUpperCase()");
        $form->setOnchange("demandeur_societe","this.value=this.value.toUpperCase()");
        $form->setOnchange("delegataire_nom","this.value=this.value.toUpperCase()");
        $form->setOnchange("delegataire_societe","this.value=this.value.toUpperCase()");
        $form->setOnchange("architecte_nom","this.value=this.value.toUpperCase()");
        $form->setOnchange("terrain_adresse","this.value=this.value.toUpperCase()");
        $form->setOnchange('terrain_surface','VerifNumdec(this)');
        $form->setOnchange('tax_mtn_part_commu', 'VerifFloat(this, 0)');
        $form->setOnchange('tax_mtn_part_depart', 'VerifFloat(this, 0)');
        $form->setOnchange('tax_mtn_part_reg', 'VerifFloat(this, 0)');
        $form->setOnchange('tax_mtn_total', 'VerifFloat(this, 0)');
        $form->setOnchange('tax_mtn_rap', 'VerifFloat(this, 0)');
        $form->setOnchange('tax_mtn_part_commu_sans_exo', 'VerifFloat(this, 0)');
        $form->setOnchange('tax_mtn_part_depart_sans_exo', 'VerifFloat(this, 0)');
        $form->setOnchange('tax_mtn_part_reg_sans_exo', 'VerifFloat(this, 0)');
        $form->setOnchange('tax_mtn_total_sans_exo', 'VerifFloat(this, 0)');
        $form->setOnchange('tax_mtn_rap_sans_exo', 'VerifFloat(this, 0)');
    }

    function setLayout(&$form, $maj) {

        $affichage_form = $this->get_type_affichage_formulaire();
        if ($affichage_form === 'ADS' || $affichage_form === 'DPC') {
             // En-tête
            $form->setBloc('om_collectivite', 'D', '', ($maj == 3 ? 'col_9':'col_12'));

                // Col1 : Fieldset "Dossier d'Instruction"
                $form->setBloc('om_collectivite', 'D', '', 'col_9');

                    $form->setFieldset('om_collectivite', 'D', _("Dossier d'instruction"));
                    $form->setFieldset('geom', 'F');
                    

                $form->setBloc('geom', 'F');
            
                // Col2 : 3 fieldsets
                $form->setBloc('enjeu_urba', 'D', '', 'col_3');

                    // Fieldset "Enjeu"
                    $form->setFieldset('enjeu_urba', 'D', _("Enjeu"));
                    $form->setFieldset('enjeu_ctx', 'F');
                    // Fieldset "Qualification"
                    $form->setFieldset('erp', 'D', _("Qualification"));
                    $form->setFieldset('a_qualifier', 'F');
                    // Fieldset "Archive"
                    $form->setFieldset('numero_versement_archive', 'D', _("Archive"));
                    $form->setFieldset('date_demande', 'F');

                $form->setBloc('date_demande', 'F');
            $form->setBloc('date_demande', 'F');

            // Fieldset "Instruction"
            $form->setBloc('date_depot', 'D', '', 'col_12');
            $form->setFieldset('date_depot', 'D', _('Instruction'), 'col_12');

            // Fieldset "Suivi"
            $form->setBloc('date_depot', 'D', '', 'col_12');
            
                $form->setFieldset('date_depot', 'D', _('Suivi'), 'col_12');
                // Col 1
                $form->setBloc('date_depot', 'D', '', 'col_6');
                    $form->setBloc('date_depot', 'D');
                    $form->setBloc('date_dernier_depot', 'F');
                    $form->setBloc('date_limite', 'D', '', 'interligne');
                    $form->setBloc('date_limite_incompletude', 'F');
                $form->setBloc('date_limite_incompletude', 'F');
                // Col 2
                $form->setBloc('etat', 'D', '', 'col_6');
                    $form->setBloc('etat', 'D');
                    $form->setBloc('etat', 'F');
                    $form->setBloc('evenement_suivant_tacite', 'D', '', 'evmt_suivant_tacite_di');
                    $form->setBloc('evenement_suivant_tacite_incompletude', 'F');
                $form->setBloc('evenement_suivant_tacite_incompletude', 'F');
                $form->setFieldset('evenement_suivant_tacite_incompletude','F','');
            
            $form->setBloc('evenement_suivant_tacite_incompletude', 'F'); // Fin Suivi

            // Bloc 2 fieldsets
            $form->setBloc('date_decision', 'D', '', 'col_12');

            // Col 1 Fieldset "Décision"
            $form->setFieldset('date_decision', 'D', _('Decision'), 'col_6');
            $form->setFieldset('avis_decision','F',''); 
            // Col 2 Fieldset "Validité de l'autorisation"
            $form->setFieldset('date_validite', 'D', _("Validite de l'autorisation"), 'col_6');
            $form->setFieldset('date_validite','F',''); 

            $form->setBloc('date_validite', 'F'); // Fin bloc 2 fieldsets

            $form->setFieldset('date_conformite','F','');
            $form->setBloc('date_conformite', 'F'); // Fin Instruction

            // Fieldset "Simulation des taxes"
            $form->setBloc('tax_secteur', 'D', '', 'col_12');
                $form->setFieldset('tax_secteur', 'D', _("Simulation des taxes"), 'startClosed');
                //
                $form->setBloc('tax_secteur', 'D', '', 'col_12');
                    $form->setFieldset('tax_secteur', 'D', _("Taxe d'aménagement"), 'collapsible');
                    $form->setFieldset('tax_mtn_total_sans_exo', 'F', '');
                $form->setBloc('tax_mtn_total_sans_exo', 'F');
                //
                $form->setBloc('tax_mtn_rap', 'D', '', 'col_12');
                    $form->setFieldset('tax_mtn_rap', 'D', _("Redevance d'archéologie préventive"), 'collapsible');
                    $form->setFieldset('tax_mtn_rap_sans_exo', 'F', '');
                $form->setBloc('tax_mtn_rap_sans_exo', 'F');
                //
                $form->setFieldset('tax_mtn_rap_sans_exo', 'F', '');
            $form->setBloc('tax_mtn_rap_sans_exo', 'F');

            // Fieldset "Localisation"
            $form->setBloc('terrain_adresse_voie_numero', 'D', '', 'col_12');

                $form->setFieldset('terrain_adresse_voie_numero', 'D', _('Localisation'), 'startClosed');
                    // Col 1
                    $form->setBloc('terrain_adresse_voie_numero', 'D', "", "col_6");
                    $form->setBloc('terrain_references_cadastrales', 'F');
                    // Col 2
                    $form->setBloc('terrain_adresse_voie', 'D', "", "col_6");
                    $form->setBloc('terrain_superficie', 'F');

                $form->setFieldset('terrain_superficie', 'F', '');

            $form->setBloc('terrain_superficie', 'F');
        }
        // RECOURS
        if ($affichage_form === 'CTX RE') {
            // Fieldset "Dossier d'Instruction"
            $form->setBloc('om_collectivite', 'D', '', ($maj == 3 ? 'col_9':'col_12'));
                $form->setFieldset('om_collectivite', 'D', _("Dossier d'instruction"));
                $form->setFieldset('date_demande', 'F');
            $form->setBloc('date_demande', 'F');

            // Fieldset "Instruction"
            $form->setBloc('date_depot', 'D', '', 'col_12');
            $form->setFieldset('date_depot', 'D', _('Instruction'), 'col_12');

            // Fieldset "Suivi"
            $form->setBloc('date_depot', 'D', '', 'col_12');
            
                $form->setFieldset('date_depot', 'D', _('Suivi'), 'col_12');
                // Col 1
                $form->setBloc('date_depot', 'D', '', 'col_6');
                    // $form->setBloc('date_depot', 'D');
                    // $form->setBloc('date_dernier_depot', 'F');
                    // $form->setBloc('date_limite', 'D', '');
                    // $form->setBloc('date_limite_incompletude', 'F');
                $form->setBloc('date_cloture_instruction', 'F');
                // Col 2
                $form->setBloc('etat', 'D', '', 'col_6');
                    $form->setBloc('etat', 'D');
                    $form->setBloc('evenement_suivant_tacite_incompletude', 'F');
                    // $form->setBloc('evenement_suivant_tacite', 'D', '', 'evmt_suivant_tacite_di');
                    // $form->setBloc('evenement_suivant_tacite_incompletude', 'F');
                $form->setBloc('evenement_suivant_tacite_incompletude', 'F');
                $form->setFieldset('evenement_suivant_tacite_incompletude','F','');
            
            $form->setBloc('evenement_suivant_tacite_incompletude', 'F'); // Fin Suivi

            // Bloc 2 fieldsets
            $form->setBloc('date_decision', 'D', '', 'col_12');

            // Col 1 Fieldset "Décision"
            $form->setFieldset('date_decision', 'D', _('Decision'), 'col_12');
            $form->setFieldset('avis_decision','F',''); 
            // Col 2 Fieldset "Validité de l'autorisation"

            $form->setBloc('date_validite', 'F'); // Fin bloc 2 fieldsets

            $form->setFieldset('date_conformite','F','');
            $form->setBloc('date_conformite', 'F'); // Fin Instruction

            // Fieldset "Localisation"
            $form->setBloc('terrain_adresse_voie_numero', 'D', '', 'col_12');

                $form->setFieldset('terrain_adresse_voie_numero', 'D', _('Localisation'), 'startClosed');
                    // Col 1
                    $form->setBloc('terrain_adresse_voie_numero', 'D', "", "col_6");
                    $form->setBloc('terrain_references_cadastrales', 'F');
                    // Col 2
                    $form->setBloc('terrain_adresse_voie', 'D', "", "col_6");
                    $form->setBloc('terrain_superficie', 'F');

                $form->setFieldset('terrain_superficie', 'F', '');

            $form->setBloc('terrain_superficie', 'F');
        }

        // INFRACTION
        if ($affichage_form === 'CTX IN') {

            // Fieldset "Dossier d'Instruction"
            $form->setBloc('om_collectivite', 'D', '', ($maj == 3 ? 'col_9':'col_12'));
                $form->setFieldset('om_collectivite', 'D', _("Dossier d'instruction"));
                $form->setFieldset('date_demande', 'F');
            $form->setBloc('date_demande', 'F');

            // Fieldset "Instruction"
            $form->setBloc('date_depot', 'D', '', 'col_12');
            $form->setFieldset('date_depot', 'D', _('Instruction'), 'col_12');
                // Fieldset "Suivi"
                $form->setBloc('date_depot', 'D', '', 'col_12');
                    $form->setFieldset('date_depot', 'D', _('Suivi'), 'col_12');
                    // Col 1
                    $form->setBloc('date_depot', 'D', '', 'col_6');
                        $form->setBloc('date_depot', 'D');
                        $form->setBloc('date_dernier_depot', 'F');
                        $form->setBloc('date_limite', 'D', '', 'interligne');
                        $form->setBloc('date_limite_incompletude', 'F');
                    $form->setBloc('date_limite_incompletude', 'F');
                    // Col 2
                    $form->setBloc('etat', 'D', '', 'col_6');
                        $form->setBloc('etat', 'D');
                        $form->setBloc('etat', 'F');
                        $form->setBloc('evenement_suivant_tacite', 'D', '', 'evmt_suivant_tacite_di');
                        $form->setBloc('evenement_suivant_tacite_incompletude', 'F');
                    $form->setBloc('evenement_suivant_tacite_incompletude', 'F');
                    $form->setFieldset('evenement_suivant_tacite_incompletude','F','');
                $form->setBloc('evenement_suivant_tacite_incompletude', 'F'); // Fin Suivi
                // Fieldset "Décision"
                $form->setFieldset('date_decision', 'D', _('Decision'), 'col_12');
                $form->setFieldset('date_conformite','F',''); // Fin Décision
            $form->setFieldset('date_conformite','F','');
            $form->setBloc('date_conformite', 'F'); // Fin Instruction

            // Fieldset "Localisation"
            $form->setBloc('terrain_adresse_voie_numero', 'D', '', 'col_12');
                $form->setFieldset('terrain_adresse_voie_numero', 'D', _('Localisation'), 'startClosed');
                    // Col 1
                    $form->setBloc('terrain_adresse_voie_numero', 'D', "", "col_6");
                    $form->setBloc('terrain_references_cadastrales', 'F');
                    // Col 2
                    $form->setBloc('terrain_adresse_voie', 'D', "", "col_6");
                    $form->setBloc('terrain_superficie', 'F');
                $form->setFieldset('terrain_superficie', 'F', '');
            $form->setBloc('terrain_superficie', 'F');

            // Fieldset "Demandeurs"
            // → cf. formSpecificContent()
        }
    }

    /**
     * Permet de retourner si le dossier est incomplet notifié
     *
     * @return boolean true si incomplet notifié
     */
    function is_incomplet_notifie() {
        // Si le dossier est défini en tant qu'incomplet notifie
        if($this->getVal('incomplet_notifie') == 't' AND
            $this->getVal('incompletude') == 't') {
            return true;
        }
        return false;
    }


    /**
     * Vérifie que le dossier d'instruction en cours ne soit pas instruit.
     * Ne sont pas compté comme instruit les dossiers n'ayant que des événements
     * d'instruction de type 'affichage'.
     *
     * @return boolean
     */
    function has_only_recepisse() {

        // Récupère la liste des instructions du dossier
        $list_instructions = $this->get_list_instructions(true);

        // Si le dossier a pour seule instruction le récépissé de la demande
        if (count($list_instructions) === 1
            && $list_instructions[0] === $this->get_demande_instruction_recepisse()) {
            //
            return true;
        }

        //
        return false;
    }


    /**
     * TRIGGER - triggerajouterapres.
     *
     * - Interface avec le référentiel ERP [108]
     * - Gestion des données techniques liées
     * - Mise à jour du DA
     * - Gestion des références cadastrales / parcelles liées
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {

        /**
         * Interface avec le référentiel ERP.
         *
         * (WS->ERP)[108] Dépôt de dossier DAT -> AT
         * Déclencheur :
         *  - L'option ERP est activée
         *  - Validation du formulaire d'ajout d'une demande de nouveau dossier
         *    de type AT
         */
        //
        if ($this->f->is_option_referentiel_erp_enabled($this->valF['om_collectivite']) === true 
            && $this->f->getDATCode($this->valF['dossier']) == $this->f->getParameter('erp__dossier__nature__at')) {
            //
            $infos = array(
                "dossier_instruction" => $this->valF['dossier'],
            );
            //
            $ret = $this->f->send_message_to_referentiel_erp(108, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification (108) du référentiel ERP. Contactez votre administrateur."));
                return false;
            }
            $this->addToMessage(_("Notification (108) du référentiel ERP OK."));
        }

        /**
         * Gestion des données techniques liées.
         */
        // On ajoute les données techniques
        if ($this->ajoutDonneesTechniquesDI($id, $db, $val, $DEBUG) === false) {
            //
            $this->addToMessage(_("Erreur lors de l'enregistrement du dossier.")." "._("Contactez votre  administrateur."));
            $this->correct = false;
            return false;
        }

        /**
         * Mise à jour des données du DA.
         */
        //
        $inst_da = $this->get_inst_dossier_autorisation($this->valF["dossier_autorisation"]);
        //
        if ($inst_da->majDossierAutorisation() === false) {
            //
            $this->addToMessage(_("Erreur lors de la mise a jour des donnees du dossier d'autorisation. Contactez votre administrateur."));
            $this->correct = false;
            return false;
        }

        /**
         * Gestion des références cadastrales / parcelles liées.
         */
        // Si le champ des références cadastrales n'est pas vide
        if ($this->valF['terrain_references_cadastrales'] != '') {
            // Ajout des parcelles dans la table dossier_parcelle
            $this->ajouter_dossier_parcelle($this->valF['dossier'], 
                $this->valF['terrain_references_cadastrales']);
        }

        /**
         * Notification de l'éventuelle autorisation contestée
         */
        if ($this->valF['autorisation_contestee'] !== null) {
            // Instancie la classe dossier_message
            $dossier_message = $this->get_inst_dossier_message(']');
            // Ajoute le message de notification
            $dossier_message_val = array();
            $dossier_message_val['dossier'] = $this->valF['autorisation_contestee'];
            $dossier_message_val['type'] = _('Autorisation contestée');
            $dossier_message_val['emetteur'] = $this->f->get_connected_user_login_name();
            $dossier_message_val['login'] = $_SESSION['login'];
            $dossier_message_val['date_emission'] = date('Y-m-d H:i:s');
            $dossier_message_val['contenu'] = sprintf(_('Cette autorisation a été contestée par le recours %s.'), $this->valF['dossier']);
            // Si une erreur se produit lors de l'ajout
            if ($dossier_message->add_notification_message($dossier_message_val) !== true) {
                // Message d'erreur affiché à l'utilisateur
                $this->addToMessage(_("L'autorisation contestée n'a pas pu être notifiée du recours."));
                $this->correct = false;
                return false;
            }
        }

        //
        return true;
    }

    /**
     * Récupère l'instance de dossier message.
     *
     * @param string $dossier_message Identifiant du message.
     *
     * @return object
     */
    private function get_inst_dossier_message($dossier_message = null) {
        //
        return $this->get_inst_common("dossier_message", $dossier_message);
    }

    /**
     * Récupère l'identifiant du quartier et d'un arrondissement d'une référence 
     * cadastrale
     * @param string $reference_cadastrale
     * 
     * @return array
     */
    function getQuartierArrondissement($reference_cadastrale) {
             
        $reference_cadastrale = trim($reference_cadastrale);
        $quartier = '';
         
        //Récupère le code impot du quartier dans la référence cadastrale
        for ( $i = 0 ; $i < strlen($reference_cadastrale) ; $i++ ){
           
           //Si c'est un chiffre, c'est le code quartier
           if (is_numeric($reference_cadastrale[$i]) ){

               $quartier .= $reference_cadastrale[$i];
           }
           //Sinon c'est la section
           else{
               break;
           }
        }
       
        //Si le code impôt du quartier a bien été renseigné
        if ( $quartier !== '' ){
           
            //Requête de récupération de l'identifiantdu quartier et de 
            //l'arrondissement
            $sql = "SELECT
                        quartier, arrondissement
                    FROM
                        ".DB_PREFIXE."quartier
                    WHERE
                        code_impots='$quartier'";
            $this->addToLog("getQuartierArrondissement() : db->query(\"$sql\")", VERBOSE_MODE);
            $res = $this->db->query($sql);
            if (database::isError($res)) {
                die();
            }
           
            //Si on a un résultat
            if ( $res->numRows() == 1 ){
                //    
                return $res->fetchRow(DB_FETCHMODE_ASSOC);
            }
        }
       
        return NULL;
    }
    
    /**
     * Récupère la section d'une référence cadastrale
     * @param string $reference_cadastrale
     * 
     * @return string
     */
    function getSection($reference_cadastrale){
         
        $reference_cadastrale = trim($reference_cadastrale);
        $section = NULL;
         
        for ( $i = 0 ; $i < strlen($reference_cadastrale) ; $i++ )
            if ( !is_numeric($reference_cadastrale[$i]) && is_string($reference_cadastrale[$i]) && $reference_cadastrale[$i] !== ' ' )
                $section .= $reference_cadastrale[$i];
         
        return $section;
    }
    
    /*
     * Retourne l'intructeur correspondant le mieux à la parcelle
     * */
    /**
     * Récupère la section d'une référence cadastrale
     * @param string $quartier
     * @param string $arrondissement
     * @param string $section
     * @param string $dossier_autorisation
     * 
     * @return array
     */    
    function getInstructeurDivision( $quartier, $arrondissement, $section, $dossier_autorisation_type_detaille, $collectivite, $column = 'instructeur') {

        $quartier = ( $quartier == NULL ) ? -1 : $quartier;
        $arrondissement = ( $arrondissement == NULL ) ? -1 : $arrondissement;
        $collectivite = ( $collectivite == "" ) ? -1 : $collectivite;
 
        //Si le dossier d'autorisation a un type détaillé de dossier 
        //d'autorisation associé
        if ($dossier_autorisation_type_detaille != ''
            && $dossier_autorisation_type_detaille != null){

            //Requête de récupération de l'instructeur à affecter
            $sql = "
               SELECT
                   ".$column.", section, quartier, arrondissement, dossier_autorisation_type_detaille
               FROM
                   ".DB_PREFIXE."affectation_automatique l
               WHERE
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement IS NULL AND quartier IS NULL AND section IS NULL AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement IS NULL AND quartier IS NULL AND section IS NULL AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement IS NULL AND quartier IS NULL AND section = '$section' AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement IS NULL AND quartier IS NULL AND section = '$section' AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement IS NULL AND quartier = $quartier AND section IS NULL AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement IS NULL AND quartier = $quartier AND section IS NULL AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement IS NULL AND quartier = $quartier AND section = '$section' AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement IS NULL AND quartier = $quartier AND section = '$section' AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement = $arrondissement AND quartier IS NULL AND section IS NULL AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement = $arrondissement AND quartier IS NULL AND section IS NULL AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement = $arrondissement AND quartier IS NULL AND section = '$section' AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement = $arrondissement AND quartier IS NULL AND section = '$section' AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement = $arrondissement AND quartier = $quartier AND section IS NULL AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement = $arrondissement AND quartier = $quartier AND section IS NULL AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement = $arrondissement AND quartier = $quartier AND section = '$section' AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille IS NULL AND arrondissement = $arrondissement AND quartier = $quartier AND section = '$section' AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement IS NULL AND quartier IS NULL AND section IS NULL AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement IS NULL AND quartier IS NULL AND section IS NULL AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement IS NULL AND quartier IS NULL AND section = '$section' AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement IS NULL AND quartier IS NULL AND section = '$section' AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement IS NULL AND quartier = $quartier AND section IS NULL AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement IS NULL AND quartier = $quartier AND section IS NULL AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement IS NULL AND quartier = $quartier AND section = '$section' AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement IS NULL AND quartier = $quartier AND section = '$section' AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement = $arrondissement AND quartier IS NULL AND section IS NULL AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement = $arrondissement AND quartier IS NULL AND section IS NULL AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement = $arrondissement AND quartier IS NULL AND section = '$section' AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement = $arrondissement AND quartier IS NULL AND section = '$section' AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement = $arrondissement AND quartier = $quartier AND section IS NULL AND om_collectivite = $collectivite) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement = $arrondissement AND quartier = $quartier AND section IS NULL AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement = $arrondissement AND quartier = $quartier AND section = '$section' AND om_collectivite IS NULL) OR
                   ( dossier_autorisation_type_detaille = ".$dossier_autorisation_type_detaille." AND arrondissement = $arrondissement AND quartier = $quartier AND section = '$section' AND om_collectivite = $collectivite)
               ORDER BY dossier_autorisation_type_detaille, section, quartier, arrondissement
               LIMIT 1";
            $this->addToLog("getInstructeurDivision : db->query(\"$sql\")", VERBOSE_MODE);
            $res = $this->db->query($sql);
            if (database :: isError($res))
                die($res->getMessage()."erreur ".$sql);
            
            //Si on a récupéré un instructeur correspondant aux critères
            if ( $res->numRows() > 0 ){
                
                $row = $res->fetchRow(DB_FETCHMODE_ASSOC);
                // On vérifie que l'instructeur est paramétré
                if ($row[$column] !== '') {
                    //On récupère sa division
                    $sql = "SELECT division FROM ".DB_PREFIXE."instructeur WHERE instructeur = ".$row[$column];
                    $res = $this->db->query($sql);
                    if (database :: isError($res))
                        die($res->getMessage()."erreur ".$sql);
                    
                    $row['division'] = NULL;
                    //S'il a une division
                    if ( $res->numRows() > 0 ){
                        
                        $rowT=& $res->fetchRow(DB_FETCHMODE_ASSOC);
                        $row['division'] = $rowT['division'];
                    }
                    
                    return $row;
                }
            }
        }

        return NULL;
     }

     /**
      * Récupère le type détaillé d'une dossier d'autorisation.
      *
      * @param integer $dossier_autorisation DA
      *
      * @return mixed
      */
     function get_dossier_autorisation_da_type_detaille($dossier_autorisation) {

        //
        $resDATD = "";

        //Récupération du dossier_autorisation_type_detaille concerné par le 
        //$dossier_autorisation
        $sql = "
           SELECT
               dossier_autorisation_type_detaille
           FROM
               ".DB_PREFIXE."dossier_autorisation
           WHERE
               dossier_autorisation = '$dossier_autorisation'";
        $this->addToLog(__METHOD__." : db->query(\"$sql\")", VERBOSE_MODE);
        $resDATD = $this->db->getOne($sql);
        $this->f->isDatabaseError($resDATD);

        //
        return $resDATD;
     }
    
    /* =============================================================
    * ===============================================================
    */

    function triggerajouter($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        $this->addToLog("triggerajouter() : start", EXTRA_VERBOSE_MODE);
        
        // Initialisation des variables nécessaires à l'affectation automatique
        $quartier = NULL;
        $arrondissement = NULL;
        $section = NULL;
        $instructeur = NULL;

        // Si la référence cadastrale n'est pas vide alors on récupère la 
        //section, le quartier et l'arrondissement
        if ($this->valF['terrain_references_cadastrales'] != '') {
            
            // Cette méthode récupère l'arrondissement et le quartier de la
            //référence cadastrale saisie 
            $quartierArrondissement = $this->getQuartierArrondissement($this->valF['terrain_references_cadastrales']);
            if ( $quartierArrondissement!= NULL ){
                
                $quartier = $quartierArrondissement['quartier'];
                $arrondissement = $quartierArrondissement['arrondissement'];
            }
            // Si il n'y a pas d'arrondissement alors on vide le quartier
            if ( strcmp($arrondissement,'') == 0 )  {
                
                $arrondissement = NULL;
                $quartier = NULL;
            }
            // On récupère la section
            $section = $this->getSection($this->valF['terrain_references_cadastrales']);
        }

        // Si aucun instructeur n'est saisi et que la dossier_autorisation_type_detaille n'est pas vide
        // alors on récupère l'instructeur et la division depuis l'affectation
        if ( ( empty($this->valF['instructeur']) || $this->valF['instructeur'] == '' ) && $val['dossier_autorisation'] != '' ) {

            // Récupère le type détaillé du DA
            $dossier_autorisation_type_detaille = $this->get_dossier_autorisation_da_type_detaille($this->valF['dossier_autorisation']);
            
            //Récupération de l'instructeur ainsi que de sa division
            $instructeurDivision = $this->getInstructeurDivision($quartier, $arrondissement, $section, $dossier_autorisation_type_detaille, $this->valF['om_collectivite']);

            //Si un instructeur et sa division ont été récupérés
            if ( $instructeurDivision != NULL ){
                
                $instructeur = $instructeurDivision['instructeur'];
                $division = $instructeurDivision['division'];
            }
            //Si un instructeur et sa division n'ont pas été récupérés, on 
            //ajoute pas les données
            if ( $instructeur != NULL ){
                
                $this->valF['instructeur'] = $instructeur;
                $this->valF['division'] = $division;
            } else {
                //On affiche un message à l'utilisateur pour lui signifier 
                //qu'aucun instructeur n'a été assigné au dossier d'instruction 
                //créé
                if ($this->f->isAccredited("dossier_modifier_instructeur")) {
                    $this->addToMessage("<br/> "._("Pensez a assigner un instructeur a ce dossier.")." <br/>");
                } else {
                    $this->addToMessage("<br/> "._("Aucun instructeur compatible avec ce dossier trouve, contactez votre administrateur afin d'en assigner un a ce dossier.")." <br/>");
                }
            }

            // Gestion instructeur 2
            $instr2 = $this->getInstructeurDivision($quartier, $arrondissement, $section, $dossier_autorisation_type_detaille, $this->valF['om_collectivite'], 'instructeur_2');
            if ($instr2 != NULL){
                $this->valF['instructeur_2'] = $instr2['instructeur_2'];
            }
        } else {
            $this->addToMessage("<br/> "._("Aucun instructeur compatible avec ce dossier trouve, contactez votre administrateur afin d'en assigner un a ce dossier.")." <br/>");
        }
        //
        $this->addToLog("triggerajouter() : end", EXTRA_VERBOSE_MODE);
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - Interface avec le référentiel ERP [101]
     * - Interface avec le référentiel ERP [102][103]
     * - Interface avec le référentiel ERP [114]
     * - Gestion des demandeurs liés
     * - Gestion des références cadastrales / parcelles liées
     * - Gestion des taxes
     * 
     * @return boolean
     */
    function triggermodifierapres($id, &$db = null, $val = array(), $DEBUG = null) {
        // Mise à jour DA si miroir du DI
        $inst_da = $this->get_inst_dossier_autorisation($this->getVal('dossier_autorisation'));
        if ($inst_da->is_dossier_autorisation_visible() === false) {
            if ($inst_da->majDossierAutorisation() === false) {
                $this->addToMessage(_("Erreur lors de la mise a jour des donnees du dossier d'autorisation. Contactez votre administrateur."));
                return false;
            }
        }

        /**
         * Interface avec le référentiel ERP.
         *
         * (WS->ERP)[101] ERP Qualifié -> AT
         * Déclencheur :
         *  - L'option ERP est activée
         *  - Le dossier est de type AT
         *  - Le dossier est marqué comme "connecté au référentiel ERP"
         *  - Le formulaire de modification du dossier est validé avec le
         *    marqueur "à qualifier" à "NON" alors qu'il était précédemment à
         *    "OUI"
         */
        // 
        if ($this->f->is_option_referentiel_erp_enabled($this->getVal('om_collectivite')) === true
            && $this->is_connected_to_referentiel_erp() === true
            && $this->f->getDATCode($this->valF['dossier']) == $this->f->getParameter('erp__dossier__nature__at')
            && $this->getVal('a_qualifier') == 't' && $this->valF['a_qualifier'] === false) {
            // Récupère la liste des contraintes
            $contraintes_plu_list = $this->getListContrainte($this->valF['dossier'], false);
            // Extrait les libellés de chaque contraintes
            $contraintes_plu = array();
            $contraintes_plu_string = "";
            while($row = &$contraintes_plu_list->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $contraintes_plu[] = $row['contrainte_libelle'];
            }
            // Chaîne de caractère listant toutes les contraintes du dossier
            $contraintes_plu_string = implode(' ; ', $contraintes_plu);
            $competence = "";
            if ($this->valF['autorite_competente'] !== null) {
                $inst_ac = $this->get_inst_autorite_competente($this->valF['autorite_competente']);
                $competence = $inst_ac->getVal("libelle");
            }
            //
            $infos = array(
                "dossier_instruction" => $this->valF['dossier'],
                "competence" => $competence,
                "contraintes_plu" => $contraintes_plu_string,
                "references_cadastrales" => $this->getReferenceCadastrale($this->valF['dossier']),
            );
            //
            $ret = $this->f->send_message_to_referentiel_erp(101, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification (101) du référentiel ERP. Contactez votre administrateur."));
                return false;
            }
            $this->addToMessage(_("Notification (101) du référentiel ERP OK."));
        }

        /**
         * Interface avec le référentiel ERP.
         *
         * (WS->ERP)[102] Demande de complétude de dossier PC pour un ERP -> PC qui concerne un ERP
         * (WS->ERP)[103] Demande de qualification de dossier PC pour un ERP -> PC qui concerne un ERP
         * Déclencheur :
         *  - L'option ERP est activée
         *  - Le dossier est de type PC
         *  - Le formulaire de modification du dossier est validé avec le
         *    marqueur "à qualifier" à "NON" alors qu'il était précédemment à
         *    "OUI"
         *  - Le formulaire de modification du dossier est validé avec le
         *    marqueur "ERP" à "OUI"
         */
        //
        if ($this->f->is_option_referentiel_erp_enabled($this->getVal('om_collectivite')) === true
            && $this->f->getDATCode($this->valF['dossier']) == $this->f->getParameter('erp__dossier__nature__pc')
            && $this->getVal('a_qualifier') == 't' && $this->valF['a_qualifier'] === false
            && $this->valF['erp'] == true) {
            //
            $infos = array(
                "dossier_instruction" => $this->valF['dossier'],
            );
            // [102] Demande de complétude de dossier PC pour un ERP
            $ret = $this->f->send_message_to_referentiel_erp(102, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification (102) du référentiel ERP. Contactez votre administrateur."));
                return false;
            }
            $this->addToMessage(_("Notification (102) du référentiel ERP OK."));
            // [103] Demande de qualification de dossier PC pour un ERP
            $ret = $this->f->send_message_to_referentiel_erp(103, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification (103) du référentiel ERP. Contactez votre administrateur."));
                return false;
            }
            $this->addToMessage(_("Notification (103) du référentiel ERP OK."));
        }

        /**
         * Interface avec le référentiel ERP.
         *
         * (WS->ERP)[114] ERP Qualifié -> PC
         * Déclencheur :
         *  - l'option ERP est activée
         *  - ET le dossier est marqué comme "connecté au référentiel ERP"
         *  - ET le dossier est de type PC
         *  - ET
         *    - soit le formulaire de modification du dossier est validé avec le
         *    marqueur "enjeu_urba" qui change de statut
         *    - soit ce marqueur est vrai et le dossier passe à qualifié
         */
        // Étant donné que l'objet a été modifié en base après sa création,
        // il faut le ré-instancier pour récupérer ses informations.
        $dossier = new dossier($this->valF['dossier']);
        if ($dossier->f->is_option_referentiel_erp_enabled($this->getVal('om_collectivite')) === true
            && $dossier->is_connected_to_referentiel_erp() === true
            && $this->f->getDATCode($this->valF['dossier']) == $this->f->getParameter('erp__dossier__nature__pc')
            && (($this->getVal('enjeu_urba') == 't') != $this->valF['enjeu_urba'] 
                || ($this->getVal('a_qualifier') == 't' && $this->valF['a_qualifier'] === false
                    && $this->getVal('enjeu_urba') == 't'))) {

            $enjeu = "non";
            if ($this->valF['enjeu_urba']) {
                $enjeu = "oui";
            }

            $infos = array(
                "dossier_instruction" => $this->valF['dossier'],
                "Dossier à enjeu ADS" => $enjeu
            );
            //
            $ret = $this->f->send_message_to_referentiel_erp(114, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification (114) du référentiel ERP. Contactez votre administrateur."));
                return false;
            }
            $this->addToMessage(_("Notification (114) du référentiel ERP OK."));
        }

        /**
         * Gestion des demandeurs liés.
         */
        // Ajout ou modification des demandeurs
        $this->insertLinkDossierDemandeur($db, $DEBUG);

        /**
         * Gestion des références cadastrales / parcelles liées.
         */
        // Si le champ des références cadastrales n'est pas vide
        if ($this->getVal('terrain_references_cadastrales') 
            != $this->valF['terrain_references_cadastrales']) {

            // On supprime toutes les lignes de la table dossier_parcelle qui 
            // font référence le dossier en cours de modification
            $this->supprimer_dossier_parcelle($val['dossier']);

            // Ajout des parcelles dans la table dossier_parcelle
            $this->ajouter_dossier_parcelle($val['dossier'], 
                $val['terrain_references_cadastrales']);

        }

        /**
         * Gestion des taxes.
         */
        // Si le champ tax_secteur est modifié et que l'option de simulation des
        // taxes est activée
        if ($this->getVal('tax_secteur') != $this->valF['tax_secteur']
            && $this->f->is_option_simulation_taxes_enabled($this->getVal('om_collectivite')) === true) {

            // Valeurs pour le calcul de la taxe d'aménagement
            $values = array();
            // Instance de la classe donnees_techniques
            $donnees_techniques = $this->get_inst_donnees_techniques();
            // Récupère les valeurs des données techniques
            $values = $donnees_techniques->get_form_val();

            // Met à jour les montants du dossier
            $update_dossier_tax_mtn = $this->update_dossier_tax_mtn($this->valF['tax_secteur'], $values);
            if ($update_dossier_tax_mtn === false) {
                //
                $this->addToMessage(_("La mise a jour des montants de la simulation de la taxe d'amenagement a echouee."));
                //
                return false;
            }
        }

        /**
         * Gestion des métadonées des pièces liés.
         * Vérifie les méthodes à exécuter configurées dans le connecteur du
         * filestorage.
         */
        //
        $ret = $this->post_update_metadata($val);
        // 
        if ($ret === false) {
            //
            $this->cleanMessage();
            $this->addToMessage(_("La mise à jour des métadonnées des pièces liées à ce dossier a échouée."));
            return false;
        }

        /**
         * Gestion des du changement de date de dépôt.
         * Vérification préalable de la présence de la date et de sa
         * modification.
         */
        //
        if (array_key_exists("date_depot", $val) === true) {
            //
            $new_date = DateTime::createFromFormat('d/m/Y', $val["date_depot"]);
            $old_date = DateTime::createFromFormat('Y-m-d', $this->getVal("date_depot"));

            //
            if ($new_date !== $old_date) {
                //
                $status = $this->update_date_depot($val["date_depot"]);
                //
                if ($status === false) {
                    //
                    $this->addToMessage(_("Erreur de base de donnees. Contactez votre administrateur."));
                    return false;
                }

            }
        }
        //
        return true;
    }

    /**
     * Permet d’effectuer des actions avant la modification des données dans la base.
     * @param mixed   $id    Identifiant de l'enregistrement
     * @param object  &$db   Objet de la base de données
     * @param array   $val   Valeurs du formulaire
     * @param boolean $DEBUG Mode DEBUG
     */
    function triggermodifier($id, &$db = null, $val = array(), $DEBUG = null) {
        // Si la date de dépôt a changé et si elle valait celle du dernier dépôt
        // alors cette dernière prend également sa valeur
        if ($this->f->formatDate($this->getVal('date_depot')) !== $val['date_depot']
            && $this->f->formatDate($this->getVal('date_depot')) === $this->f->formatDate($this->getVal('date_dernier_depot'))) {
            $this->valF['date_dernier_depot'] = $this->valF['date_depot'];
        }

        //
        return true;
    }

    /**
     * Methode de traitement suite à la modification de la date de dépot.
     *
     * @param string $new_date_str Nouvelle date de dépot.
     *
     * @return boolean
     */
    function update_date_depot($new_date_str) {
        $demande = $this->get_inst_demande();
        if ($demande === false) {
            return false;
        }

        $retour = $this->majDateInstruction($demande->getVal("instruction_recepisse"), $new_date_str);
        if ($retour === false) {
            return false;
        }

        $valF = array();
        foreach ($demande->champs as $id => $champ) {
            $valF[$champ] = $demande->val[$id];
        }
        $valF['date_demande'] = $new_date_str;
        $modification = $demande->modifier($valF, $this->f->db, DEBUG);
        if ($modification === false) {
            return false;
        }

        $row_date = array("date_demande" => $new_date_str);
        $res = $this->db->autoExecute(
            DB_PREFIXE."dossier",
            $row_date,
            DB_AUTOQUERY_UPDATE,
            "dossier = '".$this->getVal("dossier")."'"
        );
        $this->f->addToLog(__METHOD__ . "() : db->autoExecute(" . $res . ")", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }

        $row_date = array("depot_initial" => $new_date_str);
        $res = $this->db->autoExecute(
            DB_PREFIXE."dossier_autorisation",
            $row_date,
            DB_AUTOQUERY_UPDATE,
            "dossier_autorisation = '" . $this->getVal("dossier_autorisation") . "'"
        );
        $this->f->addToLog(__METHOD__ . "() : db->autoExecute(" . $res . ")", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }
    }


    /**
     * Met à jour l'instruction en fonction de la nouvelle date 
     * ou retourne false si il ya une erreur.
     *
     * @param integer $instruction_id Identifiant de l'instruction.
     * @param string  $date_depot     Nouvelle date de dépôt.
     * 
     * @return boolean
     */
    public function majDateInstruction($instruction_id, $date_depot) {

        // Definalise l'instruction de récépissé si nécessaire
        require_once "../obj/instruction.class.php";
        $instruction = new instruction($instruction_id, $this->f->db, 0);
        $instruction->setParameter('maj', 110);
        //
        if ($instruction->is_unfinalizable_without_bypass() === true
            && $instruction->unfinalize($instruction->valF) === false) {
            return false;
        }

        // Modifie la date d'événement
        $instruction->setParameter('maj', 1);
        //
        $valF = array();
        foreach ($instruction->champs as $id => $champ) {
            $valF[$champ] = $instruction->getVal($champ);
        }
        //
        $valF['date_evenement'] = $date_depot;
        $valF['date_finalisation_courrier'] = null;
        //
        $modification = $instruction->modifier($valF, $this->f->db, DEBUG);
        //
        if ($modification === false) {
            return false;
        }

        // Finalise l'instruction
        $instruction->setParameter('maj', 100);
        if ($instruction->finalize($instruction->valF) === false) {
            return false;
        }

        //
        return true;
    }

    /**
     * Récupère l'instance de l'autorité compétente.
     *
     * @param string $autorite_competente Identifiant de l'autorité compétente.
     *
     * @return object
     */
    function get_inst_autorite_competente($autorite_competente = null) {
        //
        return $this->get_inst_common("autorite_competente", $autorite_competente);
    }


    /**
     * Met à jour les montants des taxes du dossier d'instruction.
     *
     * @param integer $tax_secteur Secteur communal.
     * @param array   $val         Valeurs des données techniques.
     *
     * @return boolean
     */
    public function update_dossier_tax_mtn($tax_secteur, $val = array()) {
        // Instance du paramétrage de la taxe d'aménagement
        $taxe_amenagement = $this->get_inst_taxe_amenagement();

        // Liste des montants à mettre à jour
        $valF = array();
        $valF['tax_mtn_part_commu'] = null;
        $valF['tax_mtn_part_depart'] = null;
        $valF['tax_mtn_part_reg'] = null;
        $valF['tax_mtn_total'] = null;
        $valF['tax_mtn_rap'] = null;
        $valF['tax_mtn_part_commu_sans_exo'] = null;
        $valF['tax_mtn_part_depart_sans_exo'] = null;
        $valF['tax_mtn_part_reg_sans_exo'] = null;
        $valF['tax_mtn_total_sans_exo'] = null;
        $valF['tax_mtn_rap_sans_exo'] = null;

        // Si le tableau des valeurs n'est pas vide
        if ($val !== array()) {

            // Si le taux communal est renseigné
            if ($taxe_amenagement->getVal('tx_comm_secteur_'.$tax_secteur) !== null
                && $taxe_amenagement->getVal('tx_comm_secteur_'.$tax_secteur) !== '') {

                // Calcul de la TA
                $calcul_ta = $taxe_amenagement->compute_ta($tax_secteur, $val);

                // Si chaque résultat est calculable
                if ($calcul_ta !== null && is_array($calcul_ta) === true) {

                    // Total des parts de la TA avec exonération
                    $total_ta = $calcul_ta['commu'] + $calcul_ta['depart'] + $calcul_ta['reg'];
                    $total_ta_ss_exo = $calcul_ta['commu_ss_exo'] + $calcul_ta['depart_ss_exo'] + $calcul_ta['reg_ss_exo'];

                    // Valeurs à mettre à jour, les montants doivent être à l'entier
                    // inférieur
                    $valF['tax_mtn_part_commu'] = floor($calcul_ta['commu']);
                    $valF['tax_mtn_part_depart'] = floor($calcul_ta['depart']);
                    $valF['tax_mtn_part_reg'] = floor($calcul_ta['reg']);
                    $valF['tax_mtn_total'] = floor($total_ta);
                    $valF['tax_mtn_part_commu_sans_exo'] = floor($calcul_ta['commu_ss_exo']);
                    $valF['tax_mtn_part_depart_sans_exo'] = floor($calcul_ta['depart_ss_exo']);
                    $valF['tax_mtn_part_reg_sans_exo'] = floor($calcul_ta['reg_ss_exo']);
                    $valF['tax_mtn_total_sans_exo'] = floor($total_ta_ss_exo);
                }
            }

            // Calcul de la RAP
            $calcul_rap = $taxe_amenagement->compute_rap($val);

            // Si chaque résultat est calculable
            if ($calcul_rap !== null && is_array($calcul_rap) === true) {

                // RAP avec exonération
                $mtn_rap = $calcul_rap['rap'];
                // RAP sans exonération
                $mtn_rap_ss_exo = $calcul_rap['rap_ss_exo'];

                // Valeurs à mettre à jour, les montants doivent être à l'entier
                // inférieur
                $valF['tax_mtn_rap'] = floor($mtn_rap);
                $valF['tax_mtn_rap_sans_exo'] = floor($mtn_rap_ss_exo);
            }
        }

        // Met à jour l'enregistrement de dossier
        $res = $this->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->clePrimaire ."='".$this->getVal($this->clePrimaire)."'"
        );
        // Log
        $this->f->addToLog(__METHOD__."() : db->autoExecute(".$res.")", VERBOSE_MODE);
        //
        if ($this->f->isDatabaseError($res, true)) {
            //
            $this->correct = false;
            return false;
        }

        //
        return true;
    }


    /**
     * Ne servira surement pas mais dans le doute autant recalculer les données du DA
     */
    function triggersupprimerapres($id, &$db = null, $val = array(), $DEBUG = null) {
        // Mise à jour des données du dossier d'autorisation
        require_once "../obj/dossier_autorisation.class.php";
        $da = new dossier_autorisation($this->valF["dossier_autorisation"], $this->db, DEBUG);
        $da->majDossierAutorisation();

        // On supprime toutes les lignes de la table dossier_parcelle qui 
        // font référence le dossier en cours de suppression
        $this->supprimer_dossier_parcelle($this->valF['dossier']);

    }
    
    
    /**
     * Retourne la reference cadastrale de la demande attache a un dossier ERP
     * specifique
     * @param string $dossier L'identifiant du dossier
     * @return string|null La reference cadastrale si elle est trouve,
     * sinon NULL. En cas d'erreur de la BD, l'execution s'arrete.
     */
    function getReferenceCadastrale($dossier) {
        $sql = "SELECT terrain_references_cadastrales FROM ".DB_PREFIXE."demande WHERE dossier_instruction = '" . $dossier . "'";
        $res = $this->db->limitquery($sql, 0, 1);
        $this->addToLog("getReferenceCadastrale(): db->limitquery(\"".
                        str_replace(",",", ",$sql)."\", 0, 1);", VERBOSE_MODE);
        // Si une erreur survient on die
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), 'demande');
        }
        // retourne la nature du dossier
        while ($row =& $res->fetchRow()) {
            return $row[0];
        }
        // la nature n'etait pas trouve, ce qui ne devrait pas se passer
        return NULL;
    }

    /**
     * Supprime puis recrée tous les liens entre dossier et demandeurs
     **/
    function insertLinkDossierDemandeur($db, $DEBUG) {
        //
        require_once "../obj/lien_dossier_demandeur.class.php";
        // Suppression des anciens demandeurs
        $this->deleteLinkDossierDemandeur($db, $DEBUG);
        $types_demandeur = array(
            "petitionnaire_principal",
            "delegataire",
            "petitionnaire",
            "plaignant_principal",
            "plaignant",
            "contrevenant_principal",
            "contrevenant",
            "requerant_principal",
            "requerant",
            "avocat_principal",
            "avocat",
            "bailleur_principal",
            "bailleur",
        );
        foreach ($types_demandeur as $type) {
            // Comparaison des autres demandeurs
            if(isset($this->postedIdDemandeur[$type]) === true) {
                // Ajout des nouveaux liens
                foreach ($this->postedIdDemandeur[$type] as $demandeur) {
                    //
                    $principal = false;
                    if (strpos($type, '_principal') !== false) {
                        $principal = true;
                    }
                    if ($this->addLinkDossierDemandeur($demandeur, $principal, $db, $DEBUG) === false) {
                        //
                        return false;
                    }
                }
            }
        }
    }


    /**
     * Fonction permettant d'ajouter un lien
     * entre la table dossier et demandeur
     **/
    function addLinkDossierDemandeur($id, $principal, $db, $DEBUG) {
        $lienAjout = new lien_dossier_demandeur(
                                        "]",
                                        $db,
                                        $DEBUG);
        $lien = array('lien_dossier_demandeur' => "",
                           'petitionnaire_principal' => (($principal)?"t":"f"),
                           'dossier' => $this->valF['dossier'],
                           'demandeur' => $id);
        $lienAjout->ajouter($lien, $db, $DEBUG);
        $lienAjout->__destruct();
    }

    /**
     * Fonction permettant de supprimer un lien
     * entre la table demande et demandeur
     **/
    function deleteLinkDossierDemandeur($db, $DEBUG) {
        // Suppression
        $sql = "DELETE FROM ".DB_PREFIXE."lien_dossier_demandeur ".
                "WHERE dossier='".$this->valF['dossier']."'";
        // Execution de la requete de suppression de l'objet
        $res = $db->query($sql);
        // Logger
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ( database::isError($res)){
            die();
        }

    }

    /**
     * Methode de recupération des valeurs postées
     **/
    function getPostedValues() {
        // Récupération des demandeurs dans POST
        $types_demandeur = array(
            "petitionnaire_principal",
            "delegataire",
            "petitionnaire",
            "plaignant_principal",
            "plaignant",
            "contrevenant_principal",
            "contrevenant",
            "requerant_principal",
            "requerant",
            "avocat_principal",
            "avocat",
            "bailleur_principal",
            "bailleur",
        );
        foreach ($types_demandeur as $type) {
            if($this->f->get_submitted_post_value($type) !== null AND
                    $this->f->get_submitted_post_value($type) != '') {
                $this->postedIdDemandeur[$type] = $this->f->get_submitted_post_value($type);
            }
        }
    }

    /**
     * Méthode permettant de récupérer les id des demandeurs liés à la table
     * liée passée en paramètre
     *
     * @param string $from Table liée : "demande", "dossier", dossier_autorisation"
     * @param string $id Identifiant (clé primaire de la table liée en question)
     */
    function listeDemandeur($from, $id) {
        
        // Si la donnée membre a déjà été remplie par un précédent appel à cette méthode,
        // on sort.
        if ($this->valIdDemandeur["petitionnaire_principal"] !== array() or
            $this->valIdDemandeur["delegataire"] !== array() or
            $this->valIdDemandeur["petitionnaire"] !== array() or
            $this->valIdDemandeur["plaignant_principal"] !== array() or
            $this->valIdDemandeur["plaignant"] !== array() or
            $this->valIdDemandeur["contrevenant_principal"] !== array() or
            $this->valIdDemandeur["contrevenant"] !== array() or
            $this->valIdDemandeur["requerant_principal"] !== array() or
            $this->valIdDemandeur["requerant"] !== array() or
            $this->valIdDemandeur["avocat_principal"] !== array() or
            $this->valIdDemandeur["avocat"] !== array() or 
            $this->valIdDemandeur["bailleur_principal"] !== array() or
            $this->valIdDemandeur["bailleur"] !== array()) {
            return;
        }

        // Récupération des demandeurs de la base
        $sql = "SELECT demandeur.demandeur,
                        demandeur.type_demandeur,
                        lien_".$from."_demandeur.petitionnaire_principal
            FROM ".DB_PREFIXE."lien_".$from."_demandeur
            INNER JOIN ".DB_PREFIXE."demandeur 
            ON demandeur.demandeur=lien_".$from."_demandeur.demandeur 
            WHERE ".$from." = '".$id."'";
        $res = $this->f->db->query($sql);
        $this->f->addToLog("listeDemandeur(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        
        // Stockage du résultat dans un tableau
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
            
            $demandeur_type = $row['type_demandeur'];
            if ($row['petitionnaire_principal'] == 't'){
                $demandeur_type .= "_principal";
            }
            $this->valIdDemandeur[$demandeur_type][] = $row['demandeur'];
        }
    }

    /**
     * Récupère la liste des contraintes d'un dossier.
     * 
     * @param string  $dossier     Identifiant du dossier.
     * @param boolean $for_di_view Liste avec condition affichage DI.
     *
     * @return object          Résultat de la requête
     */
    function getListContrainte($dossier, $for_di_view = true) {

        // Select
        $select = "SELECT dossier_contrainte.dossier_contrainte as dossier_contrainte_id,
                    dossier_contrainte.texte_complete as dossier_contrainte_texte,
                    dossier_contrainte.reference as dossier_contrainte_reference,
                    contrainte.libelle as contrainte_libelle,
                    contrainte.nature as contrainte_nature,
                    contrainte.texte as contrainte_texte,
                    contrainte.reference as contrainte_reference,
                    lower(contrainte.groupe) as contrainte_groupe,
                    lower(contrainte.sousgroupe) as contrainte_sousgroupe ";

        // From
        $from = " FROM ".DB_PREFIXE."contrainte 
                    LEFT JOIN ".DB_PREFIXE."dossier_contrainte
                        ON  dossier_contrainte.contrainte = contrainte.contrainte ";

        // Where
        $where = " WHERE dossier_contrainte.dossier = '".$dossier."' ";

        // Si les contraintes sont listées pour être affichées dans le DI
        if ($for_di_view === true) {
            // Si le paramètre "option_contrainte_di" est défini
            if ($this->f->getParameter('option_contrainte_di') != 'aucun') {
                // Ajoute la condition
                $where .= $this->f->traitement_condition_contrainte(
                    $this->f->getParameter('option_contrainte_di'));
            }
        }

        // Tri
        $tri = " ORDER BY contrainte_groupe DESC, contrainte_sousgroupe, 
                    contrainte.no_ordre, contrainte.libelle ";

        // Requête SQL
        $sql = $select.$from.$where.$tri;
        $res = $this->f->db->query($sql);
        $this->f->addToLog("listContrainte(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Retourne le résultat
        return $res;
    }

    /**
     * Ajout de la liste des contraintes et des demandeurs
     */
    function formSpecificContent($maj) {

        /**
         * Liste des contraintes
         */
        //
        $listContrainte = $this->getListContrainte($this->getVal('dossier'));

        // Si le dossier possède des contraintes
        if ($listContrainte->numRows() != 0) {

            // Affiche du fieldset
            printf("<div id=\"liste_contrainte\" class=\"demande_hidden_bloc\">");
            printf("<fieldset class=\"cadre ui-corner-all ui-widget-content col_12 startClosed\">");
            printf("  <legend class=\"ui-corner-all ui-widget-content ui-state-active\"
                    id =\"fieldset_contraintes_liees\">"
                    ._("dossier_contrainte")."</legend>");
            printf("<div class=\"fieldsetContent\" style=\"display: none;\">");

            // Entête pour le groupe
            $groupeHeader = "
            <div class='dossier_contrainte_groupe'>
                <div class='dossier_contrainte_groupe_header'>
                    <span class='name'>
                        %s
                    </span>
                </div>
            ";

            // Entête pour le sous-groupe
            $sousgroupeHeader = "
            <div class='dossier_contrainte_sousgroupe'>
                <div class='dossier_contrainte_sousgroupe_header'>
                    <span class='name'>
                        %s
                    </span>
                </div>
            ";

            // Titres des colonnes
            $tableHeader = "
            <thead>
                <tr class='ui-tabs-nav ui-accordion ui-state-default tab-title'>
                    <th class='title col-0 firstcol contrainte_th_texte_complete'>
                        <span class='name'>
                            "._('texte_complete')."
                        </span>
                    </th>
                    <th class='title col-1 contrainte_th_reference'>
                        <span class='name'>
                            "._('reference')."
                        </span>
                    </th>
                    <th class='title col-2 contrainte_th_nature'>
                        <span class='name'>
                            "._('nature')."
                        </span>
                    </th>
                </tr>
            </thead>
            ";

            // Ligne de données
            $line = "
            <tr class='tab-data %s'>
                <td class='col-0 firstcol contrainte_th_texte_complete'>
                    %s
                </td>
                <td class='col-1 contrainte_th_reference'>
                    %s
                </td>
                <td class='col-2 contrainte_th_nature'>
                    %s
                </td>
            ";

            // Sauvegarde des données pour les comparer
            $lastRow = array();
            $lastRow['contrainte_groupe'] = 'empty';
            $lastRow['contrainte_sousgroupe'] = 'empty';

            // Tant qu'il y a des résultats
            while($row = &$listContrainte->fetchRow(DB_FETCHMODE_ASSOC)) {
                // Si l'identifiant du groupe de la contrainte présente et 
                // celle d'avant est différent
                if ($row['contrainte_groupe'] != $lastRow['contrainte_groupe']) {

                    // Si l'identifiant du groupe d'avant est vide
                    if ($lastRow['contrainte_groupe'] != 'empty') {
                        // Ferme le tableau
                        printf("</table>");
                        // Ferme le div
                        printf("</div>");
                        // Ferme le div
                        printf("</div>");
                    }

                    // Affiche le header du groupe
                    printf($groupeHeader, $row['contrainte_groupe']);
                }

                // Si l'identifiant du sous-groupe de la contrainte présente et 
                // celle d'avant est différent
                // Ou qu'ils soient identique mais n'appartiennent pas au même groupe
                if ($row['contrainte_sousgroupe'] != $lastRow['contrainte_sousgroupe']
                    || ($row['contrainte_sousgroupe'] == $lastRow['contrainte_sousgroupe']
                        && $row['contrainte_groupe'] != $lastRow['contrainte_groupe'])) {

                    //
                    if($row['contrainte_groupe'] == $lastRow['contrainte_groupe']) {
                        // Si l'identifiant de la sous-groupe d'avant est vide
                        if ($lastRow['contrainte_sousgroupe'] != 'empty') {
                            // Ferme le tableau
                            printf("</table>");
                            // Ferme le div
                            printf("</div>");
                        }
                    }

                    // Affiche le header du sous-groupe
                    printf($sousgroupeHeader, $row['contrainte_sousgroupe']);

                    // Ouvre le tableau
                    printf("<table id='sousgroupe_".$row['contrainte_sousgroupe']."' class='tab-tab dossier_contrainte_view'>");

                    // Affiche le header des données
                    printf($tableHeader);

                    // Définis le style des lignes
                    $style = 'odd';
                }

                // Si toujours dans la même groupe et même sous-groupe, 
                // on change le style de la ligne
                if ($row['contrainte_groupe'] == $lastRow['contrainte_groupe']
                    && $row['contrainte_sousgroupe'] == $lastRow['contrainte_sousgroupe']) {
                    // Définis le style
                    $style = ($style=='even')?'odd':'even';
                }
                
                // Affiche "Oui" ou "Non" pour le bouléen
                if ($row['dossier_contrainte_reference'] == 1 
                    || $row['dossier_contrainte_reference'] == "t"
                    || $row['dossier_contrainte_reference'] == "Oui") {
                    //
                    $contrainte_reference = "Oui";
                } else {
                    //
                    $contrainte_reference = "Non";
                }

                // Affiche les données
                printf($line, $style, 
                    $row['dossier_contrainte_texte'], 
                    $contrainte_reference,
                    $row['contrainte_nature']
                );

                // Sauvegarde les données
                $lastRow['contrainte_groupe'] = $row['contrainte_groupe'];
                $lastRow['contrainte_sousgroupe'] = $row['contrainte_sousgroupe'];
                
            }
            // Ferme le tableau
            printf("</table>");
            // Ferme le sous-groupe
            printf("</div>");
            // Ferme le groupe
            printf("</div>");

            printf("</div>");

            printf("<div class=\"visualClear\"></div>");            
            // Ferme le fieldset content
            printf("</div>");
            printf("</fieldset>");
        } 
        /**
         * Fin Liste des contraintes
         */

        /**
         * Liste des demandeurs
         */
        // Tableau des demandeurs selon le contexte
        $listeDemandeur = $this->valIdDemandeur;
        /**
         * Gestion du bloc des demandeurs
         */
        // Si le mode est (modification ou suppression ou consultation) ET que
        // le formulaire n'est pas correct (c'est-à-dire que le formulaire est
        // actif) 
        if ($this->correct !== true AND
            $this->getParameter('validation') === 0 AND
            $this->getParameter("maj") > 0) {
            // Alors on récupère les demandeurs dans la table lien pour
            // affectation des résultats dans $this->valIdDemandeur
            $this->listeDemandeur("dossier", $this->getval($this->clePrimaire));
            $listeDemandeur = $this->valIdDemandeur;
        }

        // Récupération des valeurs postées
        if ($this->getParameter('validation') > 0) {
            $listeDemandeur = $this->postedIdDemandeur;
        }

        // Si le mode est (ajout ou modification) ET que le formulaire n'est pas
        // correct (c'est-à-dire que le formulaire est actif)
        if ($this->getParameter("maj") < 2 AND $this->correct !== true) {
            // Alors on positionne le marqueur linkable a true qui permet
            // d'afficher ou non les actions de gestion des demandeurs
            $linkable = true;
        } else {
            // Sinon on positionne le marqueur linkable a false qui permet
            // d'afficher ou non les actions de gestion des demandeurs
            $linkable = false;
        }
        $affichage_form = $this->get_type_affichage_formulaire();
        // Pour les dossiers contentieux, il faut un droit spécifique pour visualiser le
        // fieldset "Demandeurs"
        if (($affichage_form === 'ADS' || $affichage_form === 'DPC')
            OR ($affichage_form === 'CTX RE' AND $this->f->isAccredited('dossier_contentieux_recours_afficher_demandeurs') === true)
            OR ($affichage_form === 'CTX IN' AND $this->f->isAccredited('dossier_contentieux_infractions_afficher_demandeurs') === true)) {

            // Conteneur de la listes des demandeurs
            echo "<div id=\"liste_demandeur\" class=\"demande_hidden_bloc col_12\">";
            echo "<fieldset id=\"fieldset-form-dossier_instruction-demandeur\" class=\"cadre ui-corner-all ui-widget-content startClosed\">";
            echo "  <legend class=\"ui-corner-all ui-widget-content ui-state-active\">"
                    ._("Demandeurs")."</legend>";
            echo "<div class=\"fieldsetContent\" style=\"display: none;\">";


            // Pour les DI avec DA visible, dans tous les modes excepté en ajout et si l'option d'accès au
            // portail citoyen est activée
            $inst_da = $this->get_inst_dossier_autorisation();
            if ($this->getParameter("maj") != 0
                && $this->f->is_option_citizen_access_portal_enabled() === true
                && $inst_da->is_dossier_autorisation_visible() === true) {
                // Instance du dossier d'autorisation
                //
                printf('<div class="field field-type-static"><div class="form-libelle"><label id="lib-cle_acces_citoyen" class="libelle-cle_acces_citoyen" for="cle_acces_citoyen">%s</label></div><div class="form-content"><span id="cle_acces_citoyen" class="field_value">%s</span></div></div><br/>', _("cle_acces_citoyen"), $inst_da->getVal('cle_acces_citoyen'));
            }
            // Sélection des demandeur à afficher en fonction du paramétrage du type
            // du dossier d'autorisation.
            switch ($affichage_form) {
                case 'ADS':
                    $this->display_demandeur_petitionnaire_delegataire($listeDemandeur);
                    break;
                case 'CTX RE':
                    $this->display_demandeur_petitionnaire_delegataire($listeDemandeur);
                    $this->display_demandeur_requerant_avocat($listeDemandeur);
                    break;
                case 'CTX IN':
                    $this->display_demandeur_plaignant_contrevenant($listeDemandeur);
                    break;
                case 'DPC':
                    $this->display_demandeur_petitionnaire_delegataire($listeDemandeur);
                    $this->display_demandeur_petitionnaire_delegataire_bailleur($listeDemandeur);
                    break;
            }

        }

        echo "</fieldset>";
        echo "</div>";
        /**
         * Fin liste des demandeurs
         */

        /**
         * Interface avec le référentiel ERP.
         *
         * On affiche le message uniquement si le dossier est connecté.
         */
        if ($this->getParameter('maj') == 3 && $this->is_connected_to_referentiel_erp() === true) {
            //
            printf(
                '<div class="col_12">
                    Ce dossier est connecté au référentiel ERP.
                </div>'
            );
        }

    }

    /**
     * Affiche le bloc d'affichage des demandeurs pour dossiers ADS avec actions.
     *
     * @param array $listeDemandeur Liste des demandeurs.
     */
    function display_demandeur_petitionnaire_delegataire($listeDemandeur) {
        
        // Affichage du bloc pétitionnaire principal / délégataire
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"petitionnaire_principal_delegataire\">";
        // Affichage de la synthèse du pétitionnaire principal
        $this->displaySyntheseDemandeur($listeDemandeur, "petitionnaire_principal");
        // L'ID DU DIV ET DE L'INPUT SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"delegataire\">";
        // Affichage de la synthèse du délégataire
        $this->displaySyntheseDemandeur($listeDemandeur, "delegataire");
        echo "</div>";
        echo "<div class=\"both\"></div>";
        echo "</div>";
        // Bloc des pétitionnaires secondaires
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"listePetitionnaires\">";
        $this->displaySyntheseDemandeur($listeDemandeur, "petitionnaire");
        echo "</div>";
    }

    /**
     * Affiche le bloc d'affichage des demandeurs pour dossiers CTX recours
     * avec actions.
     *
     * @param array $listeDemandeur Liste des demandeurs.
     */
    function display_demandeur_plaignant_contrevenant($listeDemandeur) {
        
        echo "<div id=\"plaignant_contrevenant\">";
        // Affichage du bloc contrevenant
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"listeContrevenants\" class=\"col_12\">";
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"contrevenant_principal\">";
        // Affichage de la synthèse
        $this->displaySyntheseDemandeur($listeDemandeur, "contrevenant_principal");
        echo "</div>";
        echo "<div id=\"listeAutresContrevenants\">";
        // Affichage de la synthèse
        $this->displaySyntheseDemandeur($listeDemandeur, "contrevenant");
        echo "</div>";
        echo "</div>";
        // Affichage du bloc plaignant
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"listePlaignants\" class=\"col_12\">";
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"plaignant_principal\">";
        // Affichage de la synthèse
        $this->displaySyntheseDemandeur($listeDemandeur, "plaignant_principal");
        echo "</div>";
        echo "<div id=\"listeAutresPlaignants\">";
        $this->displaySyntheseDemandeur($listeDemandeur, "plaignant");
        echo "</div>";
        echo "</div>";
        echo "</div>";
        
    }

    /**
     * Affiche le bloc d'affichage des demandeurs pour dossiers CTX infraction
     * avec actions.
     *
     * @param array $listeDemandeur Liste des demandeurs.
     */
    function display_demandeur_requerant_avocat($listeDemandeur) {
        echo "<div id=\"requerant_avocat\">";
        // Affichage du bloc requérant
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"listeRequerants\" class=\"col_12\">";
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"requerant_principal\">";
        // Affichage de la synthèse
        $this->displaySyntheseDemandeur($listeDemandeur, "requerant_principal");
        echo "</div>";
        echo "<div id=\"listeAutresRequerants\">";
        $this->displaySyntheseDemandeur($listeDemandeur, "requerant");
        echo "</div>";
        echo "</div>";
        // Affichage du bloc avocat
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"listeAvocat\" class=\"col_12\">";
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"avocat_principal\">";
        $this->displaySyntheseDemandeur($listeDemandeur, "avocat_principal");
        echo "</div>";
        echo "<div id=\"listeAutresAvocats\">";
        $this->displaySyntheseDemandeur($listeDemandeur, "avocat");
        echo "</div>";
        echo "</div>";
        echo "</div>";
        echo "</fieldset>";
        // Champ flag permettant de récupérer la valeur de l'option sig pour
        // l'utiliser en javascript, notamment lors du chargement de l'interface
        // pour les références cadastrales
        // XXX Si un widget pour les références cadastrales existait, il n'y
        // aurait pas besoin de faire cela
        echo "<input id='option_sig' type='hidden' value='".$this->f->getParameter("option_sig")."' name='option_sig'>";
        echo "</div>";
    }


    /**
     * Affiche le bloc d'affichage des demandeurs pour dossiers DPC avec actions.
     *
     * @param array $listeDemandeur Liste des demandeurs.
     */
    function display_demandeur_petitionnaire_delegataire_bailleur($listeDemandeur) {
        
        // Affichage du bloc pétitionnaire principal / délégataire / bailleur
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"petitionnaire_principal_delegataire_bailleur\">";
        // Doit être utilisé avec la div petitionnaire_principal_delegataire
        echo "<div id=\"listeBailleurs\" class=\"col_12\">";
        // L'ID DU DIV SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
        echo "<div id=\"bailleur_principal\">";
        // Affichage de la synthèse
        $this->displaySyntheseDemandeur($listeDemandeur, "bailleur_principal");
        echo "</div>";
        echo "<div id=\"listeAutresBailleurs\">";
        $this->displaySyntheseDemandeur($listeDemandeur, "bailleur");
        echo "</div>";
        echo "</div>";
        echo "</div>";
    }


    function displaySyntheseDemandeur($listeDemandeur, $type) {
        // Si le mode est (ajout ou modification) ET que le formulaire n'est pas
        // correct (c'est-à-dire que le formulaire est actif)
        if ($this->getParameter("maj") < 2 AND $this->correct !== true) {
            // Alors on positionne le marqueur linkable a true qui permet
            // d'afficher ou non les actions de gestion des demandeurs
            $linkable = true;
        } else {
            // Sinon on positionne le marqueur linkable a false qui permet
            // d'afficher ou non les actions de gestion des demandeurs
            $linkable = false;
        }
        // Récupération du type de demandeur pour l'affichage
        switch ($type) {
            case 'petitionnaire_principal':
                $legend = _("Petitionnaire principal");
                break;

            case 'delegataire':
                $legend = _("Autre correspondant");
                break;
            
            case 'petitionnaire':
                $legend = _("Petitionnaire");
                break;
                
            case 'contrevenant_principal':
                $legend = _("Contrevenant principal");
                break;
                
            case 'contrevenant':
                $legend = _("Autre contrevenant");
                break;
                
            case 'plaignant_principal':
                $legend = _("Plaignant principal");
                break;
            
            case 'plaignant':
                $legend = _("Autre plaignant");
                break;
            
            case 'requerant_principal':
                $legend = _("Requérant principal");
                break;
            
            case 'requerant':
                $legend = _("Autre requérant");
                break;
            
            case 'avocat_principal':
                $legend = _("Avocat principal");
                break;
            
            case 'avocat':
                $legend = _("Autre avocat");
                break;

            case 'bailleur_principal':
                $legend = _("Bailleur principal");
                break;
            
            case 'bailleur':
                $legend = _("Bailleur");
                break;
        }
        foreach ($listeDemandeur[$type] as $demandeur_id) {
            $obj = str_replace('_principal', '', $type);
            $demandeur = new $obj(
                                $demandeur_id,
                                $this->f->db,false);
            $demandeur -> afficherSynthese($type, $linkable);
            $demandeur -> __destruct();
        }
        // Si en édition de formulaire
        if ($this->getParameter("maj") < 2 AND $this->correct !== true) {
            // Bouton d'ajout du avocat
            // L'ID DE L'INPUT SUIVANT EST NECESSAIRE AU BON FONCTIONNEMENT DU JS
            echo "<a id=\"add_".$type."\"
                class=\"om-form-button add-16\">".
                $legend.
            "</a>";
        }
    }
    /**
     * Retourne le statut du dossier
     * @return string Le statut du dossier d'instruction
     */
    function getStatut(){
        
        $statut = '';
        
        $etat = $this->getVal("etat");
        //Si l'état du dossier d'instruction n'est pas vide
        if ( $etat != '' ){
            
            $sql = "SELECT statut
                FROM ".DB_PREFIXE."etat
                WHERE libelle ='".$etat."'";
            $statut = $this->db->getOne($sql);
            $this->f->addToLog("getStatut() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            if ( database::isError($statut, true)){
                die();
            }
        }
        return $statut;
    }

    /**
     * Retourne le dernier événement lié au dossier instancié
     * 
     * @return [string] ID du dernier événement
     */
    function get_dernier_evenement() {
        $sql = "SELECT max(instruction)
            FROM ".DB_PREFIXE."instruction
            WHERE dossier = '".$this->getVal($this->clePrimaire)."'";
        $id_dernier_evenement = $this->db->getOne($sql);
        $this->f->addToLog(__METHOD__."(): db->getOne(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($id_dernier_evenement);
        return $id_dernier_evenement;
    }

    /**
     * Retourne l'identifiant du rapport d'instruction lié du dossier
     * @return string L'identifiant du rapport d'instruction lié du dossier
     */
    function getRapportInstruction() {
        
        $rapport_instruction = null;

        $sql = "SELECT rapport_instruction
            FROM ".DB_PREFIXE."rapport_instruction
            WHERE dossier_instruction ='".$this->getVal($this->clePrimaire)."'";
        $rapport_instruction = $this->db->getOne($sql);
        $this->f->addToLog(__METHOD__."() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($rapport_instruction);

        return $rapport_instruction;
    }
    
    /**
     * Retourne l'identifiant des données techniques liées du dossier
     * @return string L'identifiant des données techniques liées du dossier
     */
    function getDonneesTechniques() {
        
        $donnees_techniques = '';

        $sql = "SELECT donnees_techniques
            FROM ".DB_PREFIXE."donnees_techniques
            WHERE dossier_instruction ='".$this->getVal($this->clePrimaire)."'";
        $donnees_techniques = $this->db->getOne($sql);
        $this->f->addToLog("getStatut() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
        if ( database::isError($donnees_techniques)){
            die();
        }

        return $donnees_techniques;
    }

    /**
     * Surcharge du bouton retour afin de retourner sur la recherche de dossiers
     * d'instruction existant
     */
    function retour($premier = 0, $recherche = "", $tricol = "") {

        echo "\n<a class=\"retour\" ";
        echo "href=\"";
        //
        if($this->getParameter("idx_dossier") != "") {
            echo "tab.php?";
            echo "obj=recherche_dossier";

        } else {
            if($this->getParameter("retour")=="form" AND !($this->getParameter("validation")>0 AND $this->getParameter("maj")==2 AND $this->correct)) {
                echo "form.php?";
            } else {
                echo "tab.php?";
            }
            echo "obj=".get_class($this);
            if ($this->f->get_submitted_get_value('retourformulaire2') !== null && $this->f->get_submitted_get_value('retourformulaire2') != '') {
                echo "&retourformulaire=".$this->f->get_submitted_get_value('retourformulaire2');
            }
            if($this->getParameter("retour")=="form") {
                $idx = $this->getParameter("idx");
                echo "&amp;idx=".$idx;
                echo "&amp;idz=".$this->getParameter("idz");
                echo "&amp;action=3";
            }
        }
        echo "&amp;premier=".$this->getParameter("premier");
        echo "&amp;tricol=".$this->getParameter("tricol");
        echo "&amp;recherche=".$this->getParameter("recherche");
        echo "&amp;selectioncol=".$this->getParameter("selectioncol");
        echo "&amp;advs_id=".$this->getParameter("advs_id");
        echo "&amp;valide=".$this->getParameter("valide");
        //
        echo "\"";
        echo ">";
        //
        echo _("Retour");
        //
        echo "</a>\n";

    }

    /**
     * Permet de modifier le fil d'Ariane
     * @param string $ent Fil d'Ariane
     * @param array  $val Valeurs de l'objet
     * @param intger $maj Mode du formulaire
     */
    function getFormTitle($ent) {

        // Fil d'Ariane
        $type_aff_form = $this->get_type_affichage_formulaire();
        switch ($type_aff_form) {
            case 'DPC':
            case 'ADS':
                $ent = _("instruction")." -> "._("dossiers d'instruction");
                break;
            case 'CTX IN':
                $ent = _("contentieux")." -> "._("infraction");
                break;
            case 'CTX RE':
                $ent = _("contentieux")." -> "._("recours");
                break;
        }

        // Si différent de l'ajout
        if($this->getParameter("maj") != 0) {
            // Si le champ dossier_libelle existe
            if (trim($this->getVal("dossier_libelle")) != '') {
                $ent .= " -> ".strtoupper($this->getVal("dossier_libelle"));
            }
            // Si contexte ADS
            if ($type_aff_form ==='ADS'
                && trim($this->getVal("dossier")) != '') {
                $demandeur = $this->get_demandeur($this->getVal("dossier"));
                // Si le demandeur existe
                if (isset($demandeur) && trim($demandeur) != '') {
                    $ent .= " ".mb_strtoupper($demandeur, "UTF-8");
                }
            }
        }

        // Change le fil d'Ariane
        return $ent;
    }

    /**
     * Récupère le demandeur du dossier
     * @return string Identifiant du dossier
     */
    private function get_demandeur($dossier) {

        // init de la variable de retour
        $demandeur = '';

        // Requête SQL
        $sql = "SELECT 
                    CASE WHEN demandeur.qualite='particulier' 
                        THEN TRIM(CONCAT(demandeur.particulier_nom, ' ', demandeur.particulier_prenom)) 
                        ELSE TRIM(CONCAT(demandeur.personne_morale_raison_sociale, ' ', demandeur.personne_morale_denomination)) 
                    END as demandeur
                FROM ".DB_PREFIXE."dossier
                    LEFT JOIN ".DB_PREFIXE."lien_dossier_demandeur 
                            ON lien_dossier_demandeur.dossier=dossier.dossier
                                AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
                    LEFT JOIN ".DB_PREFIXE."demandeur
                        ON lien_dossier_demandeur.demandeur=demandeur.demandeur
            WHERE dossier.dossier ='".$dossier."'";
        $demandeur = $this->db->getOne($sql);
        $this->f->addToLog("get_demandeur() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
        database::isError($demandeur);

        // Résultat retourné
        return $demandeur;
    }

    /**
     * Récupère la durée de validité
     * @param  string $dossier_autorisation Identifiant dossier d'autorisation
     * @return intger                       Durée de validité
     */
    function get_duree_validite($dossier_autorisation) {

        // init de la variable de retour
        $duree_validite = "";

        // Récupère le numéro de version
        $numeroVersion = $this->getNumeroVersion($dossier_autorisation);

        // Si c'est l'ajout du dossier initial
        if ($numeroVersion < 0) {

            // Récupération de la duree de validite depuis la table 
            // "dossier_autorisation_type_detaille"
            $sql = "SELECT duree_validite_parametrage
                    FROM ".DB_PREFIXE."dossier_autorisation_type_detaille
                    LEFT JOIN ".DB_PREFIXE."dossier_autorisation
                        ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
                    WHERE dossier_autorisation.dossier_autorisation='".$dossier_autorisation."'";
            $duree_validite = $this->db->getOne($sql);
            $this->f->addToLog("get_duree_validite(): db->getOne(\"".$sql."\");", VERBOSE_MODE);
            database::isError($duree_validite);

        } else {

            // Récupération de la duree de validite depuis le P0
            $sql = "SELECT duree_validite
                    FROM ".DB_PREFIXE."dossier
                    LEFT JOIN ".DB_PREFIXE."dossier_autorisation
                        ON dossier_autorisation.dossier_autorisation = dossier.dossier_autorisation
                    WHERE dossier_autorisation.dossier_autorisation='".$dossier_autorisation."'
                    AND dossier.version = 0";
            $duree_validite = $this->db->getOne($sql);
            $this->f->addToLog("get_duree_validite(): db->getOne(\"".$sql."\");", VERBOSE_MODE);
            database::isError($duree_validite);
        }

        // retourne le résultat
        return $duree_validite;

    }

    /**
     * Ajoute les parcelles du dossier passé en paramètre et met à jour le 
     * quartier du dossier.
     * @param string $dossier                        Identifiant du dossier
     * @param string $terrain_references_cadastrales Références cadastrales du 
     *                                                dossier
     */
    function ajouter_dossier_parcelle($dossier, $terrain_references_cadastrales) {

        // Parse les parcelles
        $list_parcelles = $this->f->parseParcelles($terrain_references_cadastrales, $this->getVal('om_collectivite'));

        // Fichier requis
        require_once "../obj/dossier_parcelle.class.php";

        // A chaque parcelle une nouvelle ligne est créée dans la table
        // dossier_parcelle
        foreach ($list_parcelles as $parcelle) {

            // Instance de la classe dossier_parcelle
            $dossier_parcelle = new dossier_parcelle("]", $this->db, DEBUG);

            // Valeurs à sauvegarder
            $value = array(
                'dossier_parcelle' => '',
                'dossier' => $dossier,
                'parcelle' => '',
                'libelle' => $parcelle['quartier']
                                .$parcelle['section']
                                .$parcelle['parcelle']
            );

            // Ajout de la ligne
            $dossier_parcelle->ajouter($value, $this->db, DEBUG);
        }

        // Si la liste des parcelles n'est pas vide
        if (count($list_parcelles) > 0) {

            // Récupère le code impôt de la première référence cadastrale
            $quartier_code_impots = $list_parcelles[0]['quartier'];
            // Récupère l'identifiant du quartier
            $quartier = $this->get_quartier_by_code_impot($quartier_code_impots);

            // Ajoute le quartier au dossier
            $this->modifier_quartier_dossier($dossier, $quartier);
        }
    }

    /**
     * Supprime les parcelles du dossier passé en paramètre et met à jour le 
     * quartier du dossier.
     * @param string $dossier Identifiant du dossier
     */
    function supprimer_dossier_parcelle($dossier) {

        // Suppression des parcelles du dossier
        $sql = "DELETE FROM ".DB_PREFIXE."dossier_parcelle
                WHERE dossier='".$dossier."'";
        $res = $this->db->query($sql);
        $this->addToLog("supprimer_dossier_parcelle() db->query(\"".$sql."\");",
            VERBOSE_MODE);
        database::isError($res);

        // Supprime le quartier dans dossier
        $this->modifier_quartier_dossier($dossier);
    }

    /**
     * Modifie le quartier au dossier.
     * @param string  $dossier  Numéro du dossier
     * @param integer $quartier Identifiant du quartier
     */
    function modifier_quartier_dossier($dossier, $quartier = null) {

        // Valeurs à mettre à jour
        $valF = array();
        $valF['quartier'] = $quartier;

        // Met à jour le quartier du dossier
        $cle = " dossier='".$dossier."'";
        $res = $this->db->autoExecute(
            DB_PREFIXE.'dossier', $valF, DB_AUTOQUERY_UPDATE, $cle);
        $this->addToLog("ajouter_quartier_dossier(): db->autoexecute(\""
            .DB_PREFIXE."dossier\", ".print_r($valF, true)
            .", DB_AUTOQUERY_UPDATE, \"".$cle."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
    }

    /**
     * Récupère le quartier par rapport au code impôts.
     * @param string $code_impots Code impôts du quartier
     * 
     * @return integer            Identifiant du quartier
     */
    function get_quartier_by_code_impot($code_impots) {

        // Initialisation résultat
        $quartier = null;

        // Si la condition n'est pas vide
        if ($code_impots != "" 
            && $code_impots != null) {

            // Requête SQL
            $sql = "SELECT quartier
                    FROM ".DB_PREFIXE."quartier
                    WHERE code_impots = '".$code_impots."'";
            $this->f->addToLog(
                "get_quartier_by_code_impots() : db->getOne(\"".$sql."\")", 
                VERBOSE_MODE);
            $quartier = $this->db->getOne($sql);
            $this->f->isDatabaseError($quartier);
        }

        // Retourne résultat
        return $quartier;
    }
    
    /**
     * Méthode permettant d'ajouter les données techniques d'un DI.
     *
     * @param integer  $id    identifiant de la demande
     * @param database &$db   handler de la base de donnée
     * @param array    $val   tableau de valeurs postées via le formulaire
     * @param boolean  $DEBUG debug
     *
     * @return boolean false si erreur
     */
    function ajoutDonneesTechniquesDI($id, &$db, $val, $DEBUG) {

        //On vérifie que le dossier d'autorisation a des données techniques
        $sql = "SELECT * "
            ."FROM ".DB_PREFIXE."donnees_techniques "
            ."WHERE dossier_autorisation = '".$this->valF["dossier_autorisation"]."'";
        $dtda = $this->db->query($sql);
        $this->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
        if($this->f->isDatabaseError($dtda, true)){
            $this->f->addToLog(__METHOD__."() : ERROR - Erreur de base de données. Impossible d'ajouter les données techniques du dossier d'instruction.", DEBUG_MODE);
            return false;
        }
        
        //Si le dossier d'autorisation a des données techniques
        if ( $dtda->numrows() == 1 ){
            //
            require_once '../obj/donnees_techniques.class.php';
            $dtdi = new donnees_techniques(']', $db, $DEBUG);
            //Préparation des données
            $valF = $dtda->fetchRow(DB_FETCHMODE_ASSOC);
            //Suppression de l'identifiant
            $valF["donnees_techniques"] = null;
            // Ajout du numéro de dossier d'instruction
            $valF['dossier_instruction'] = $this->valF['dossier'];
            // Suppression du numéro de dossier d'autorisation
            $valF['dossier_autorisation'] = null;
            // Ajout des données techniques
            if($dtdi->ajouter($valF, $db, $DEBUG) === false) {
                $this->f->addToLog(__METHOD__."() : ERROR - Impossible d'ajouter les données techniques du dossier d'instruction.", DEBUG_MODE);
                return false;
            }
        }
        else {
            //Le dossier d'autorisation n'a pas de données techniques
            $this->f->addToLog(__METHOD__."() : ERROR - le DA n'a pas de données techniques.", DEBUG_MODE);
            return -1;
        }

        //
        return true;
    }

    /**
     * VIEW - contrainte.
     *
     * Vue des contraintes du dossier
     *
     * Cette vue permet de gérer le contenu de l'onglet "Contrainte(s)" sur un 
     * dossier. Cette vue spécifique est nécessaire car l'ergonomie standard du
     * framework ne prend pas en charge ce cas.
     * C'est ici la vue spécifique des contraintes liées au dossier qui est
     * affichée directement au clic de l'onglet au lieu du soustab.
     * 
     * L'idée est donc de simuler l'ergonomie standard en créant un container 
     * et d'appeler la méthode javascript 'ajaxit' pour charger le contenu 
     * de la vue visualisation de l'objet lié.
     * 
     * @return void
     */
    function view_contrainte() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Récupération des variables GET
        ($this->f->get_submitted_get_value('idxformulaire')!==null ? $idxformulaire = 
            $this->f->get_submitted_get_value('idxformulaire') : $idxformulaire = "");
        ($this->f->get_submitted_get_value('retourformulaire')!==null ? $retourformulaire = 
            $this->f->get_submitted_get_value('retourformulaire') : $retourformulaire = "");
        $obj = "dossier_contrainte";
        $type_aff_form = $this->get_type_affichage_formulaire();
        if ($type_aff_form === 'CTX RE' OR $type_aff_form === 'CTX IN') {
            $obj = "dossier_contrainte_contexte_ctx";
        }
        // Objet à charger
        // Construction de l'url de sousformulaire à appeler
        $url = "../scr/sousform.php?obj=".$obj;
        $url .= "&action=4";
        $url .= "&idx=".$idxformulaire;
        $url .= "&retourformulaire=".$retourformulaire;
        $url .= "&idxformulaire=".$idxformulaire;
        $url .= "&retour=form";
        // Affichage du container permettant le reffraichissement du contenu
        // dans le cas des action-direct.
        printf('
            <div id="sousform-href" data-href="%s">
            </div>',
            $url
        );
        // Affichage du container permettant de charger le retour de la requête
        // ajax récupérant le sous formulaire.
        printf('
            <div id="sousform-%s">
            </div>
            <script>
            ajaxIt(\'%s\', \'%s\');
            </script>',
            $obj,
            $obj,
            $url
        );
    }

    /**
     * Cette methode permet d'afficher le bouton de validation du formulaire
     *
     * @param integer $maj Mode de mise a jour
     * @return void
     */
    function bouton($maj) {
        
        if (!$this->correct
            && $this->checkActionAvailability() == true) {
            //
            switch($maj) {
                case 0 :
                    $bouton = _("Ajouter");
                    break;
                case 1 :
                    $bouton = _("Modifier");
                    break;
                case 2 :
                    $bouton = _("Supprimer");
                    break;
                default :
                    // Actions specifiques
                    if ($this->get_action_param($maj, "button") != null) {
                        //
                        $bouton = $this->get_action_param($maj, "button");
                    } else {
                        //
                        $bouton = _("Valider");
                    }
                    break;
            }
            //
            $params = array(
                "value" => $bouton,
                "name" => "submit",
                "onclick"=>"return getDataFieldReferenceCadastrale();",
            );
            //
            $this->f->layout->display_form_button($params);
        }

    }

    /**
     * Récupère l'instance de paramétrage des taxes.
     *
     * @param integer $taxe_amenagement Identifiant
     *
     * @return object
     */
    function get_inst_taxe_amenagement($taxe_amenagement = null) {
        //
        if ($this->inst_taxe_amenagement === null) {
            //
            if ($taxe_amenagement === null) {
                //
                $taxe_amenagement = $this->get_taxe_amenagement_by_om_collectivite($this->getVal('om_collectivite'));

                // Si aucun paramétrage de taxe trouvé et que la collectivité
                // est mono
                if ($taxe_amenagement === null
                    && $this->f->isCollectiviteMono($this->getVal('om_collectivite')) === true) {
                    // Récupère la collectivité multi
                    $om_collectivite_multi = $this->f->get_idx_collectivite_multi();
                    //
                    $taxe_amenagement = $this->get_taxe_amenagement_by_om_collectivite($om_collectivite_multi);
                }

                //
                if ($taxe_amenagement === null) {
                    //
                    return null;
                }
            }
            //
            require_once "../obj/taxe_amenagement.class.php";
            $this->inst_taxe_amenagement = new taxe_amenagement($taxe_amenagement);
        }
        //
        return $this->inst_taxe_amenagement;
    }

    /**
     * Récupère l'identifiant de la taxe d'aménagement par rapport à la collectivité.
     *
     * @param integer $om_collectivite La collectivité
     *
     * @return integer
     */
    function get_taxe_amenagement_by_om_collectivite($om_collectivite) {
        //
        $taxe_amenagement = null;

        // Si la collectivité n'est pas renseigné
        if ($om_collectivite !== '' && $om_collectivite !== null) {

            // SQL
            $sql = "SELECT taxe_amenagement
                    FROM ".DB_PREFIXE."taxe_amenagement
                    WHERE om_collectivite = ".intval($om_collectivite);
            $taxe_amenagement = $this->f->db->getOne($sql);
            $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($taxe_amenagement);
        }

        //
        return $taxe_amenagement;
    }

    /**
     * [get_inst_donnees_techniques description]
     *
     * @param [type] $donnees_techniques [description]
     *
     * @return [type] [description]
     */
    function get_inst_donnees_techniques($donnees_techniques = null) {
        //
        if (is_null($this->inst_donnees_techniques)) {
            //
            if (is_null($donnees_techniques)) {
                $donnees_techniques = $this->getDonneesTechniques();
            }
            //
            require_once "../obj/donnees_techniques.class.php";
            $this->inst_donnees_techniques = new donnees_techniques($donnees_techniques);
        }
        //
        return $this->inst_donnees_techniques;
    }


    /**
     * Récupère l'instance du dossier d'autorisation.
     *
     * @param string $dossier_autorisation Identifiant du dossier d'autorisation.
     *
     * @return object
     */
    function get_inst_dossier_autorisation($dossier_autorisation = null) {
        //
        return $this->get_inst_common("dossier_autorisation", $dossier_autorisation);
    }


    /**
     * Récupère l'instance du dossier d'autorisation, puis la clé d'accès au portail 
     * citoyen associée à ce DA.
     *
     * @param string $dossier_autorisation Identifiant du dossier d'autorisation.
     *
     * @return string $cle_acces_citoyen si la clé d'accès existe
     *         boolean false             si la clé n'existe pas
     */
    protected function get_citizen_access_key($dossier_autorisation = null) {
        //
        $inst_da = $this->get_inst_dossier_autorisation($dossier_autorisation);
        // Récupération de la valeur de la clé d'accès
        $cle_acces_citoyen = $inst_da->getVal('cle_acces_citoyen');
        if ($cle_acces_citoyen === '' OR $cle_acces_citoyen === null) {
            return false;
        }
        return $cle_acces_citoyen;
    }


    /**
     * Récupère l'instance du type détaillé du dossier d'autorisation.
     *
     * @param integer $dossier_autorisation_type_detaille Identifiant
     *
     * @return object
     */
    function get_inst_dossier_autorisation_type_detaille($dossier_autorisation_type_detaille = null) {
        //
        if (is_null($this->inst_dossier_autorisation_type_detaille)) {
            //
            if (is_null($dossier_autorisation_type_detaille)) {
                //
                $dossier_autorisation = $this->get_inst_dossier_autorisation();
                //
                $dossier_autorisation_type_detaille = $dossier_autorisation->getVal('dossier_autorisation_type_detaille');
            }
            //
            require_once "../obj/dossier_autorisation_type_detaille.class.php";
            $this->inst_dossier_autorisation_type_detaille = new dossier_autorisation_type_detaille($dossier_autorisation_type_detaille, $this->db, 0);
        }
        //
        return $this->inst_dossier_autorisation_type_detaille;
    }

    /**
     * Récupère l'instance du cerfa
     *
     * @param integer $cerfa Identifiant du cerfa
     *
     * @return object
     */
    function get_inst_cerfa($cerfa = null) {
        //
        if (is_null($this->inst_cerfa)) {
            //
            if (is_null($cerfa)) {
                //
                $dossier_autorisation_type_detaille = $this->get_inst_dossier_autorisation_type_detaille();
                //
                $cerfa = $dossier_autorisation_type_detaille->getVal('cerfa');
            }
            //
            require_once "../obj/cerfa.class.php";
            $this->inst_cerfa = new cerfa($cerfa, $this->db, 0);
        }
        //
        return $this->inst_cerfa;
    }

    /**
     * CONDITION - is_user_from_allowed_collectivite.
     *
     * Cette condition permet de vérifier si l'utilisateur connecté appartient
     * à une collectivité autorisée : c'est-à-dire de niveau 2 ou identique à
     * la collectivité de l'enregistrement sur lequel on se trouve.
     *
     * @return boolean
     */
    function is_user_from_allowed_collectivite() {

        // Si l'utilisateur est de niveau 2
        if ($_SESSION["niveau"] == "2") {
            // Alors l'utilisateur fait partie d'une collectivité autorisée
            return true;
        }

        // L'utilisateur est donc de niveau 1
        // On vérifie donc si la collectivité de l'utilisateur est la même
        // que la collectivité de l'élément sur lequel on se trouve
        if ($_SESSION["collectivite"] === $this->getVal("om_collectivite")) {
            // Alors l'utilisateur fait partie d'une collectivité autorisée
            return true;
        }

        // L'utilisateur ne fait pas partie d'une collectivité autorisée
        return false;
    }

    /**
     * Création ou mise à jour du répertoire de numérisation.
     *
     * L'objet de cette méthode est la création ou la mise à jour de la date de
     * modification du répertoire de numérisation destiné à recevoir les pièces
     * numérisées pour un import automatique.
     * À chaque saisie d'une nouvelle demande dans openADS, le répertoire est
     * soit créé soit mis à jour pour être disponible en dehors d'openADS
     * (point de montage sur le serveur) pour permettre de déposer les pièces
     * numérisées directement depuis le copieur. À intervalle régulier, un
     * service vérifie le contenu de ces répertoire pour importer
     * automatiquement ces fichiers dans l'onglet 'Pièce(s)' du dossier
     * concerné.
     * La mise à jour de la date de modification est importante pour réaliser
     * la purge des répertoires vides sur la base de la date de la dernière
     * demande qui concerne le dossier.
     *
     * @return boolean
     */
    function create_or_touch_digitalization_folder() {

        // Nom du répertoire
        // Le répertoire créé possède comme nom le libellé du dossier avec
        // le suffixe séparé par un '.'. Exemple : PC0130551601234.P0
        $separateur = '';
        if ($this->getSuffixe($this->getVal('dossier_instruction_type')) === 't') {
            $separateur = '.';
        }

        $digitalization_folder_name = str_replace(
            $this->getVal("dossier_autorisation"),
            $this->getVal("dossier_autorisation").$separateur,
            $this->getVal($this->clePrimaire)
        );

        // Vérifie que l'option de numérisation des dossiers est désactivée
        if ($this->f->is_option_digitalization_folder_enabled() !== true) {
            //
            $this->addToLog(
                _("L'option de numerisation des dossiers n'est pas activee").".",
                DEBUG_MODE
            );
            return false;
        }

        // Vérifie le paramétrage du répertoire de numérisation
        if ($this->f->getParameter("digitalization_folder_path") === null) {
            //
            $this->addToLog(
                "Configuration du répertoire de numérisation incorrecte.",
                DEBUG_MODE
            );
            return false;
        }

        // Répertoire cible
        $root_folder_path = $this->f->getParameter("digitalization_folder_path");

        // Vérifie que le répertoire existe
        if (is_dir($root_folder_path) !== true) {
            //
            $this->addToLog(
                sprintf(
                    "Le répertoire '%s' n'existe pas.",
                    $root_folder_path
                ),
                DEBUG_MODE
            );
            return false;
        }

        // Répertoire des "à traiter"
        $todo_folder_path = $root_folder_path."Todo/";

        // Vérifie que le répertoire existe
        if (is_dir($todo_folder_path) !== true) {
            //
            $this->addToLog(
                sprintf(
                    "Le répertoire '%s' n'existe pas.",
                    $todo_folder_path
                ),
                DEBUG_MODE
            );
            return false;
        }

        // Répertoire de numérisation.
        $digitalization_folder_path = $todo_folder_path.$digitalization_folder_name;
 
        // Si le répertore existe déjà le répertoire n'est pas créé
        if (file_exists($digitalization_folder_path) == true) {
            // Mise à jour du répertoire
            if (touch($digitalization_folder_path) !== true) {
                // Si une erreur survient
                $this->addToLog(
                    sprintf(
                        "Erreur lors de la mise à jour du répertoire '%s'.",
                        $digitalization_folder_path
                    ),
                    DEBUG_MODE
                );
                return false;
            }
            //
            return true;
        } else {
            // Création du répertoire
            if (mkdir($digitalization_folder_path) !== true) {
                //
                $this->addToLog(
                    sprintf(
                        "Erreur lors de la création du répertoire '%s'.",
                        $digitalization_folder_path
                    ),
                    DEBUG_MODE
                );
                return false;
            }
            //
            return true;
        }
    }

    /**
     * Récupère, convertit et retourne les logs de toutes les instructions
     * 
     * @return array tableau indexé de logs
     */
    public function get_log_instructions() {
        $log_instructions = $this->getVal('log_instructions');
        // Gestion du premier log
        if ($log_instructions === '') {
            $log_instructions = json_encode(array());
        }
        // Gestion du log invalide
        if(!$this->isJson($log_instructions)) {
            return false;
        }
        return json_decode($log_instructions, true);
    }

    /**
     * Ajoute un log d'instruction aux logs existants
     * 
     * @param  array $log valeurs de l'instruction
     * @return bool       vrai si traitement effectué avec succès
     */
    public function add_log_instructions($log) {
        // Begin
        $this->begin_treatment(__METHOD__);
        // Ajout du log
        $log_instructions = $this->get_log_instructions();
        if ($log_instructions === false) {
            $this->addToMessage(_("Erreur de base de donnees. Contactez votre administrateur."));
            return $this->end_treatment(__METHOD__, false);
        }
        array_push($log_instructions, $log);
        $log_instructions = json_encode($log_instructions);
        // Mise à jour du DI
        $val = array("log_instructions"=>$log_instructions);
        $ret = $this->f->db->autoExecute(
            DB_PREFIXE."dossier",
            $val,
            DB_AUTOQUERY_UPDATE,
            "dossier = '".$this->getVal('dossier')."'");
        if (database::isError($ret, true)) {
            $this->erreur_db($ret->getDebugInfo(), $ret->getMessage(), 'dossier');
            $this->addToMessage(_("Erreur de base de donnees. Contactez votre administrateur."));
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *  Vérifie qu'une chaîne est au format json
     * 
     * @param  string  $text chaîne à analyser
     * @return boolean       vrai si formatée json
     */
    function isJson($text) {
         json_decode($text);
         return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * TREATMENT - mark_as_connected_to_referentiel_erp.
     * 
     * Cette méthode permet de positionner le marqueur 
     * 'interface_referentiel_erp' à 'true'. Cela signifie que le dossier est
     * connecté au référentiel ERP.
     *
     * @return boolean
     */
    function mark_as_connected_to_referentiel_erp() {
        //
        $this->begin_treatment(__METHOD__);
        //
        $data = array("interface_referentiel_erp" => true, );
        // Exécution de la requête
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $data,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($this->getVal($this->clePrimaire))
        );
        // Logger
        $this->addToLog(
            __METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($data, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\");",
            VERBOSE_MODE
        );
        //
        if ($this->f->isDatabaseError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            $this->addToLog(
                __METHOD__."(): Problème erreur lors de la mise à jour du dossier",
                DEBUG_MODE
            );
            // Termine le traitement
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $this->addToMessage(_("Le dossier est désormais 'connecté avec le référentiel ERP'."));
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * CONDITION - is_connected_to_referentiel_erp.
     *
     * @return boolean
     */
    function is_connected_to_referentiel_erp() {
        //
        if ($this->getVal("interface_referentiel_erp") !== "t") {
            return false;
        }
        //
        return true;
    }


    /**
     * Retourne les données techniques applicables au dossier courant, càd les données
     * techniques liées au dossier avec seulement les champs du CERFA associé au type de
     * dossier.
     * 
     * @return array $donnees_techniques_applicables  Tableau associatif contenant
     *                                                seulement les données techniques
     *                                                applicables au dossier.
     */
    public function get_donnees_techniques_applicables() {

        // Récupération de l'identifiant des données techniques liées au dossier
        $donnees_techniques = $this->getDonneesTechniques();

        $inst_donnees_techniques = $this->get_inst_common('donnees_techniques', $donnees_techniques);
        $donnees_techniques_applicables = $inst_donnees_techniques->get_donnees_techniques_applicables();
        //
        return $donnees_techniques_applicables;

    }


    /**
     * Retourne un tableau avec les données du dossier d'instruction.
     *
     * L'objectif est de mettre à disposition via un WS REST un ensemble
     * de données exploitable par une autre application.
     */
    function get_datas() {

        /**
         *
         */

        $om_collectivite = $this->get_inst_common('om_collectivite', $this->getVal('om_collectivite'));
        $instructeur = $this->get_inst_common('instructeur', $this->getVal('instructeur'));
        $division = $this->get_inst_common('division', $this->getVal('division'));
        $dossier_autorisation_type_detaille = $this->get_inst_dossier_autorisation_type_detaille();
        $dossier_autorisation_type = $this->get_inst_common('dossier_autorisation_type', $dossier_autorisation_type_detaille->getVal('dossier_autorisation_type'));
        $donnees_techniques = $this->get_donnees_techniques_applicables();

        //
        $datas = array(
            //
            "dossier_instruction" => $this->getVal($this->clePrimaire),
            //
            "dossier_autorisation" => $this->getVal("dossier_autorisation"),
            //
            "terrain_adresse_voie_numero" => $this->getVal("terrain_adresse_voie_numero"),
            "terrain_adresse_lieu_dit" => $this->getVal("terrain_adresse_lieu_dit"),
            "terrain_adresse_code_postal" => $this->getVal("terrain_adresse_code_postal"),
            "terrain_adresse_cedex" => $this->getVal("terrain_adresse_cedex"),
            "terrain_adresse_voie" => $this->getVal("terrain_adresse_voie"),
            "terrain_adresse_bp" => $this->getVal("terrain_adresse_bp"),
            "terrain_adresse_localite" => $this->getVal("terrain_adresse_localite"),
            "terrain_superficie" => $this->getVal("terrain_superficie"),
            //
            "references_cadastrales" => $this->f->parseParcelles($this->getVal("terrain_references_cadastrales"), $this->getVal('om_collectivite')),
            "dossier_autorisation_type" => $dossier_autorisation_type->getVal("libelle"),
            "dossier_autorisation_type_detaille" => $dossier_autorisation_type_detaille->getVal("libelle"),
            "collectivite" => $om_collectivite->getVal("libelle"),
            "instructeur" => $instructeur->getVal("nom"),
            "division" => $division->getVal("libelle"),
            "etat_dossier" => $this->getVal("etat"),
            "statut_dossier" => $this->getStatut(),
            "date_depot_initial" => $this->getVal("date_depot"),
            "date_limite_instruction" => $this->getVal("date_limite"),
            "date_decision" => $this->getVal("date_decision"),
            "enjeu_urbanisme" => $this->getVal("enjeu_urba") == 't' ? 'true' : 'false',
            "enjeu_erp" => $this->getVal("enjeu_erp") == 't' ? 'true' : 'false',
        );

        // Gestion des demandeurs.
        $this->listeDemandeur('dossier', $this->getVal($this->clePrimaire));
        //
        if (isset($this->valIdDemandeur["petitionnaire_principal"]) AND !empty($this->valIdDemandeur["petitionnaire_principal"])) {
            $demandeur = new petitionnaire($this->valIdDemandeur["petitionnaire_principal"][0], $this->f->db, false);
            $datas["petitionnaire_principal"] = $demandeur->get_datas();
            $demandeur->__destruct();
        }
        //
        if (isset($this->valIdDemandeur["delegataire"]) && !empty($this->valIdDemandeur["delegataire"])) {
            $demandeur = new delegataire($this->valIdDemandeur["delegataire"][0], $this->f->db, false);
            $datas["delegataire"] = $demandeur->get_datas();
            $demandeur->__destruct();
        }
        //
        if (isset($this->valIdDemandeur["petitionnaire"]) AND !empty($this->valIdDemandeur["petitionnaire"])) {
            $datas["autres_petitionnaires"] = array();
            foreach ($this->valIdDemandeur["petitionnaire"] as $petitionnaire) {
                $demandeur = new petitionnaire($petitionnaire, $this->f->db, false);
                $datas["autres_petitionnaires"][] = $demandeur->get_datas();
                $demandeur->__destruct();
            }
        }

        // Tableau contenant le nom de chaque champ de données techniques à retourner
        $dt_a_afficher = array(
              "co_tot_log_nb",
              "co_cstr_exist",
              "co_uti_pers",
              "co_uti_vente",
              "co_uti_loc",
              "su_tot_shon_tot",
              "su_avt_shon_tot",
              "am_lot_max_nb",
              "am_empl_nb",
        );

        // Tableau associatif contenant les données techniques voulues
        $tab_donnees_techniques = array();
        foreach ($dt_a_afficher as $key) {
            // On ajoute le champ de données techniques dans le retour seulement s'il
            // existe dans $donnees_techniques (s'il est applicable au dossier)
            if (array_key_exists($key, $donnees_techniques) === true) {
                if ($donnees_techniques[$key] === 't') {
                    $donnees_techniques[$key] = 'true';
                };
                if ($donnees_techniques[$key] === 'f') {
                    $donnees_techniques[$key] = 'false';
                };
                $tab_donnees_techniques[$key] = $donnees_techniques[$key];
            };
        };

        $datas['donnees_techniques'] = $tab_donnees_techniques;
        /**
         *
         */
        //
        return $datas;
    }

    /**
     * CONDITION - is_class_dossier_corresponding_to_his_groupe
     * 
     * Vérifie la correspondance groupe/classe du dossier instancié.
     *
     * @param  string  $classe
     * @return boolean
     */
    protected function is_class_dossier_corresponding_to_type_form($classe) {
        $type_form = $this->get_type_affichage_formulaire();
        switch ($type_form) {
            case 'DPC';
            case 'ADS':
                if ($this->f->starts_with($classe, 'dossier_instruction') === true) {
                    return true;
                }
                return false;
            case 'CTX RE':
                if ($this->f->ends_with($classe, '_recours') === true) {
                    return true;
                }
                return false;
            case 'CTX IN':
                if ($this->f->ends_with($classe, '_infractions') === true) {
                    return true;
                }
                return false;
            default:
                return false;
        }
    }


    /**
     * CONDITION - check_context
     *
     * Vérifie la correspondance groupes dossier/utilisateur.
     * Vérifie l'accès aux dossiers confidentiels.
     * Vérifie la correspondance groupe/classe.
     *
     * @return boolean
     */
    public function check_context() {
        // Le dossier doit être un objet valide
        $id = $this->getVal($this->clePrimaire);
        if ($id === 0 OR $id === '0' OR $id === '' OR $id === ']') {
            return false;
        }

        // Vérification que l'utilisateur a accès au dossier
        if ($this->can_user_access_dossier() === false) {
            return false;
        }
        // Vérification que la classe métier instanciée est adéquate.
        return $this->is_class_dossier_corresponding_to_type_form(get_class($this));
    }

    /**
     * VIEW - redirect.
     *
     * Cette vue est appelée lorsque l'on souhaite consulter un dossier dont on ne connaît pas le groupe.
     * Ce fonctionnement est nécessaire car les classes métier filles de 'dossier' sont relatives à ce groupe.
     *
     * Par exemple, depuis l'onglet "Dossiers Liés" du DI, le listing ne permet pas d'instancier chaque résultat
     * et par conséquent on n'a pas accès au groupe du dossier. L'action tableau consulter y est surchargée afin
     * d'amener à cette vue qui se charge de faire la redirection adéquate.
     *
     * @return void
     */
    public function redirect() {
        // Redirection vers la classe métier adéquate
        $context = $this->get_type_affichage_formulaire();
        switch ($context) {
            case 'ADS':
                $obj = 'dossier_instruction';
                break;
            case 'CTX RE':
                $obj = 'dossier_contentieux_tous_recours';
                break;
            case 'CTX IN':
                $obj = 'dossier_contentieux_toutes_infractions';
                break;
            default:
                return;
        }
        $idx = $this->getVal($this->clePrimaire);
        $link = '../scr/form.php?obj='.$obj.'&action=3&idx='.$idx;
        if ($this->f->get_submitted_get_value('retourformulaire') !== null
            && $this->f->get_submitted_get_value('idxformulaire') !== null) {
            $link .= '&premier=0&recherche=&tricol=&selectioncol=&retourformulaire='.$this->f->get_submitted_get_value('retourformulaire');
            $link .= '&idxformulaire='.$this->f->get_submitted_get_value('idxformulaire');
        }
        
        header('Location: '.$link);
        exit();
    }


    /**
     * CONDITION - is_confidentiel
     *
     * Permet de savoir si le type de dossier d'autorisation du dossier courant est
     * confidentiel.
     *
     * @return boolean true si le dossier est confidentiel, sinon false.
     * 
     */
    public function is_confidentiel() {
        //
        $inst_dossier_autorisation_type_detaille = $this->get_inst_dossier_autorisation_type_detaille();
        $inst_dossier_autorisation_type = $this->get_inst_dossier_autorisation_type($inst_dossier_autorisation_type_detaille->getVal('dossier_autorisation_type'));
        $confidentiel = $inst_dossier_autorisation_type->getVal('confidentiel');
        //
        if ($confidentiel === 't') {
            return true;
        }
        return false;
    }


    /**
     * CONDITION - can_user_access_dossier
     *
     * Effectue les vérifications suivantes :
     * - L'utilisateur doit avoir accès au groupe du dossier
     * - Si le dossier est confidentiel, l'utilisateur doit avoir accès aux dossiers
     * confidentiels de ce groupe
     *
     * @return boolean true si les conditions ci-dessus sont réunies, sinon false
     * 
     */
    public function can_user_access_dossier() {
        // Récupère le code du groupe
        $groupe_dossier = $this->get_groupe();

        // Le groupe doit être accessible par l'utilisateur ;
        if ($this->f->is_user_in_group($groupe_dossier) === false) {
            return false;
        }
        if ($this->is_confidentiel() === true) {
            //
            if ($this->f->can_user_access_dossiers_confidentiels_from_groupe($groupe_dossier) === false) {
                return false;
            }
        }
        return true;
    }


    /**
     * Met à jour une métadonnée sur tous les fichiers liés au dossier.
     *
     * @param string $metadata       Nom de la métadonnée.
     * @param string $metadata_value Nouvelle valeur de la métadonnée.
     *
     * @return boolean
     */
    public function update_metadata_by_dossier($metadata, $metadata_value) {
        // Rècupère la liste des fichiers stockés liés au dossier
        $sql = sprintf('
            SELECT uid AS uid
            FROM %1$sdocument_numerise
            WHERE dossier = \'%2$s\'
              AND document_numerise IS NOT NULL
            UNION
            SELECT fichier AS uid
            FROM %1$sconsultation
            WHERE dossier = \'%2$s\'
              AND fichier IS NOT NULL
            UNION
            SELECT om_fichier_consultation AS uid
            FROM %1$sconsultation
            WHERE dossier = \'%2$s\'
              AND om_fichier_consultation IS NOT NULL
            UNION
            SELECT om_fichier_instruction AS uid
            FROM %1$sinstruction
            WHERE dossier = \'%2$s\'
              AND om_fichier_instruction IS NOT NULL
            UNION
            SELECT om_fichier_rapport_instruction AS uid
            FROM %1$srapport_instruction
            WHERE dossier_instruction = \'%2$s\'
              AND om_fichier_rapport_instruction IS NOT NULL;
        ',
        DB_PREFIXE,
        $this->getVal($this->clePrimaire)
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Définit la métadonnée à mettre à jour
        $metadata_update = array();
        $metadata_update[$metadata] = $metadata_value;

        // Pour chaque résultat
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // Met à jour les métadonnées du fichier
            $uid_update = $this->f->storage->storage->update_metadata($row['uid'], $metadata_update);

            // Si la méthode ne retourne pas l'uid du fichier alors la mise
            // à jour ne s'est pas réalisée
            if ($uid_update !== $row['uid']) {
                //
                return false;
            }
        }
        $res->free();

        //
        return true;
    }


    /**
     * Traitement pour les ERP.
     * Si la valeur du champ 'erp' a été modifié, modifie la valeur de la
     * métadonnée concernceERP sur tous les fichiers liés au dossier.
     *
     * @param array $values Liste des nouvelles valeurs.
     *
     * @return boolean
     */
    public function update_concerneERP(array $values) {
        // Définit le champ et la métadonnée
        $champ = 'erp';
        $metadata = 'concerneERP';

        // Définit les valeurs à comparer
        $value_after = $this->get_boolean_from_view_value($values[$champ]);
        $value_before = $this->get_boolean_from_pgsql_value($this->getVal($champ));

        // Vérifie si la valeur du champ a été modifié
        if ($value_after !== $value_before) {
            // Transforme la valeur booléenne en string
            $metadata_value = 'false';
            if ($value_after === true) {
                $metadata_value = 'true';
            }

            // Met à jour les métadonnées des fichiers liés au dossier
            $update = $this->update_metadata_by_dossier($metadata, $metadata_value);
            //
            if ($update !== true) {
                //
                return false;
            }
        }

        //
        return true;
    }


    /**
     * Récupère l'instance du groupe.
     *
     * @param string $groupe Identifiant du groupe.
     *
     * @return object
     */
    private function get_inst_groupe($groupe) {
        //
        return $this->get_inst_common("groupe", $groupe);
    }


    /**
     * Récupère l'instance du type de dossier d'autorisation.
     *
     * @param string $dossier_autorisation_type Identifiant du type de dossier
     * d'autorisation.
     *
     * @return object
     */
    private function get_inst_dossier_autorisation_type($dossier_autorisation_type) {
        //
        return $this->get_inst_common("dossier_autorisation_type", $dossier_autorisation_type);
    }


    /**
     * Récupère l'instance de la demande du dossier
     *
     * @param mixed Identifiant de la demande
     *
     * @return object
     */
    function get_inst_demande($demande = null) {
        //
        if (is_null($this->inst_demande)) {
            //
            if (is_null($demande)) {
                $demande = $this->get_demande_by_dossier_instruction();
            }
            //
            return $this->get_inst_common("demande", $demande);
        }
        //
        return $this->inst_demande;
    }


    /**
     * Récupère l'identifiant de la demande par le dossier d'instruction.
     *
     * @return integer
     */
    function get_demande_by_dossier_instruction() {
        // Initialisation de la variable de retour
        $res = null;

        // SQL
        $sql = "SELECT demande
                FROM ".DB_PREFIXE."demande
                WHERE dossier_instruction = '".$this->getVal($this->clePrimaire)."'";
        $res = $this->f->db->getOne($sql);
        $this->f->addToLog(__METHOD__."(): db->getOne(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        //
        return $res;
    }


}

?>
