<?php
/**
 * DBFORM - 'contrainte' - Surcharge gen.
 *
 * Ce script permet de définir la classe 'contrainte'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../gen/obj/contrainte.class.php";

class contrainte extends contrainte_gen {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 100 - synchronisation_contrainte
        // Permet de synchroniser les contraintes de l'application
        $this->class_actions[100] = array(
            "identifier" => "synchronisation_contrainte",
            "view" => "view_synchronisation_contrainte",
            "permission_suffix" => "contrainte_synchronisation",
        );
    }

    /**
     * Permet de définir le type des champs.
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);

        // Champs cachés
        $form->setType('contrainte', 'hidden');
        $form->setType('numero', 'hidden');

        // En mode ajouter et modifier
        if ($maj < 2) {
            $form->setType('nature', 'select');
            $form->setType('reference', 'hidden');
        }
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     * @param object  $db    Objet de la base de données
     * @param boolean $debug Mode DEBUG
     */
    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        //
        parent::setSelect($form, $maj, $db, $debug);

        //
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        }
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");
        }

        // nature
        $form->setSelect("nature", $nature);
    }

    /**
     * Méthode de mise en page.
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLayout(&$form, $maj) {

        //
        $form->setFieldset("libelle", "D", _("Contrainte"));
        $form->setFieldset("no_ordre", "F");
        //
        $form->setFieldset("groupe", "D", _("Categorie"));
        $form->setFieldset("sousgroupe", "F");
        //
        $form->setFieldset("texte", "D", _("Detail"));
        $form->setFieldset("om_validite_fin", "F");
        
    }

    function verifier($val = array(), &$db = null, $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $db, $DEBUG);

        // S'il y a une erreur
        if ($this->correct == false) {
            // Ajoute l'erreur au log
            $this->addToLog("verifier() : ".$this->msg, DEBUG_MODE);
        }
    }

    /**
     * Permet de synchroniser les contraintes du SIG depuis une interface.
     */
    function view_synchronisation_contrainte() {
        // Description de la page
        $description = _("Cette page permet de synchroniser les contraintes de l'application avec celles du SIG.");
        // Affichage de la description
        $this->f->displayDescription($description);
        require_once "../obj/synchronisationContrainte.class.php";

        $sync = new SynchronisationContrainte($this->f);
        // Affichage du formulaire (bouton de validation)
        $sync->view_form_sync();
    }

}

?>
