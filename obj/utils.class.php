<?php
/**
 * Ce fichier est destine a permettre la surcharge de certaines methodes de
 * la classe om_application pour des besoins specifiques de l'application
 *
 * @package openmairie_exemple
 * @version SVN : $Id: utils.class.php 6132 2016-03-09 09:18:18Z stimezouaght $
 */

/**
 *
 */
require_once "../dyn/locales.inc.php";

/**
 *
 */
require_once "../dyn/include.inc.php";

/**
 *
 */
require_once "../dyn/debug.inc.php";

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));

/**
 *
 */
require_once PATH_OPENMAIRIE."om_application.class.php";

/**
 *
 */
class utils extends application {

    /**
     * Gestion du favicon de l'application.
     *
     * @var mixed Configuration niveau application.
     */
    var $html_head_favicon = '../app/img/favicon.ico';

    // {{{

    /**
     * SURCHARGE DE LA CLASSE OM_APPLICATION.
     *
     * @see Documentation sur la méthode parent 'om_application:getCollectivite'.
     */
    function getCollectivite($om_collectivite_idx = null) {
        // On vérifie si une valeur a été passée en paramètre ou non.
        if ($om_collectivite_idx === null) {
            // Cas d'utilisation n°1 : nous sommes dans le cas où on 
            // veut récupérer les informations de la collectivité de
            // l'utilisateur et on stocke l'info dans un flag.
            $is_get_collectivite_from_user = true;
            // On initialise l'identifiant de la collectivité
            // à partir de la variable de session de l'utilisateur.
            $om_collectivite_idx = $_SESSION['collectivite'];
        } else {
            // Cas d'utilisation n°2 : nous sommes dans le cas où on
            // veut récupérer les informations de la collectivité 
            // passée en paramètre et on stocke l'info dans le flag.
            $is_get_collectivite_from_user = false;
        }
        //
        $collectivite_parameters = parent::getCollectivite($om_collectivite_idx);

        //// BEGIN - SURCHARGE OPENADS

        // Ajout du paramétrage du sig pour la collectivité
        if (file_exists("../dyn/var.inc")) {
            include "../dyn/var.inc";
        }
        if (file_exists("../dyn/sig.inc.php")) {
            include "../dyn/sig.inc.php";
        }
        if (!isset($sig_externe)) {
            $sig_externe = "sig-default";
        }
        $idx_multi = $this->get_idx_collectivite_multi();

        if (isset($collectivite_parameters['om_collectivite_idx'])
            && isset($conf[$sig_externe][$collectivite_parameters['om_collectivite_idx']])
            && isset($conf[$sig_externe]["sig_treatment_mod"])
            && isset($collectivite_parameters["option_sig"]) 
            && $collectivite_parameters["option_sig"] == "sig_externe"
            ) {

            // Cas numéro 1 : conf sig définie sur la collectivité et option sig active
            $collectivite_parameters["sig"] = $conf[$sig_externe][$collectivite_parameters['om_collectivite_idx']];
            $collectivite_parameters["sig"]["sig_treatment_mod"] = $conf[$sig_externe]["sig_treatment_mod"];

        } elseif($idx_multi != ''
                && isset($conf[$sig_externe][$idx_multi])
                && isset($conf[$sig_externe]["sig_treatment_mod"])
                && isset($collectivite_parameters["option_sig"])
                && $collectivite_parameters["option_sig"] == "sig_externe") {
            // Cas numéro  : conf sig définie sur la collectivité multi et 
            // option_sig activé pour la collectivité mono
            $collectivite_parameters["sig"] = $conf[$sig_externe][$idx_multi];
            $collectivite_parameters["sig"]["sig_treatment_mod"] = $conf[$sig_externe]["sig_treatment_mod"];
        }

        //// END - SURCHARGE OPENADS

        // Si on se trouve dans le cas d'utilisation n°1
        if ($is_get_collectivite_from_user === true) {
            // Alors on stocke dans l'attribut collectivite le tableau de 
            // paramètres pour utilisation depuis la méthode 'getParameter'.
            $this->collectivite = $collectivite_parameters;
        }
        // On retourne le tableau de paramètres.
        return $collectivite_parameters;
    }

    /**
     * Retourne l'identifiant de la collectivité multi ou l'identifiant de la
     * seule collectivité dans le cas d'une installation mono.
     *
     * @return integer Identifiant de la collectivité multi.
     */
    public function get_idx_collectivite_multi() {
        // Récupère l'identifiant de la collectivité de niveau 2
        $sql = "SELECT om_collectivite FROM ".DB_PREFIXE."om_collectivite WHERE niveau = '2'";
        $idx = $this->db->getOne($sql);
        $this->addToLog(__METHOD__.": db->getOne(\"".$sql."\");", VERBOSE_MODE);
        $this->isDatabaseError($idx);

        // S'il n'y a pas de collectivité de niveau 2
        if ($idx == null || $idx == '') {

            // Compte le nombre de collectivité
            $sql = "SELECT count(om_collectivite) FROM ".DB_PREFIXE."om_collectivite";
            $count = $this->db->getOne($sql);
            $this->addToLog(__METHOD__.": db->getOne(\"".$sql."\");", VERBOSE_MODE);
            $this->isDatabaseError($count);

            // S'il y qu'une collectivité
            if ($count == 1) {

                // Récupère l'identifiant de la seule collectivité
                $sql = "SELECT om_collectivite FROM ".DB_PREFIXE."om_collectivite WHERE niveau = '1'";
                $idx = $this->db->getOne($sql);
                $this->addToLog(__METHOD__.": db->getOne(\"".$sql."\");", VERBOSE_MODE);
                $this->isDatabaseError($idx);
            }

        }

        // Retourne l'identifiant
        return $idx;
    }


    /**
     * Retourne  l'identifiant de la collectivité de l'element de la table passée
     * en paramètre.
     *
     * @param string $table Table de l'element.
     * @param mixed  $id    Identifiant de l'element.
     *
     * @return string identifiant de la collectivite ou false si l'element n'existe pas.
     */
    public function get_collectivite_of_element($table, $id) {
        require_once '../obj/'.$table.'.class.php';
        $instance = new $table($id, $this->db, null);
        if($instance->getVal($instance->clePrimaire) != '') {
            return $instance->getVal('om_collectivite');
        }
        return false;
    }


    /**
     * Retourne vrai si la collectivité passée en paramètre ou la collectivité
     * de l'utilisateur connecté est mono.
     *
     * @param string $id Identifiant de la collectivité.
     *
     * @return boolean
     */
    public function isCollectiviteMono($id = null) {
        // Si on ne passe pas de collectivité en argument
        if ($id == null) {
            // On vérifie la collectivité stockée en session de l'utilisateur
            // connecté
            $res = false;
            if ($_SESSION['niveau'] === '1') {
                //
                $res = true;
            }
            //
            return $res;
        }

        // Requête SQL
        $sql = "SELECT niveau FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = ".$id;
        $niveau = $this->db->getOne($sql);
        $this->addToLog(__METHOD__.": db->getOne(\"".$sql."\");", VERBOSE_MODE);
        $this->isDatabaseError($niveau);

        //
        if ($niveau === '1') {
            //
            return true;
        }
        //
        return false;
    }


    // }}}

    // {{{

    var $om_utilisateur = array();
    var $user_is_instr = NULL;
    var $user_is_service = NULL;
    var $user_is_admin = NULL;
    var $user_is_service_ext = NULL;
    var $user_is_qualificateur = NULL;
    var $user_is_chef = NULL;
    var $user_is_divisionnaire = NULL;
    var $user_is_service_int = NULL;

    /**
     * Méthode de récupération des informations de l'utilisateur connecté.
     */
    function getUserInfos() {
        
        // Si l'utilisateur est loggé $_SESSION existe
        if(isset($_SESSION['login']) AND !empty($_SESSION['login'])) {
            
            // Récupération des infos utilisateur
            $sqlUser = "SELECT om_utilisateur, nom, email, login, om_collectivite, om_profil ".
            "FROM ".DB_PREFIXE."om_utilisateur WHERE login = '".$_SESSION['login']."'";
            $resUser=$this->db->query($sqlUser);
            $this->addToLog("getUserInfos(): db->query(\"".$sqlUser."\");", VERBOSE_MODE);
            if ( database::isError($resUser)){
                die();
            }
            $this->om_utilisateur=&$resUser->fetchRow(DB_FETCHMODE_ASSOC);

            // Récupère le profil et test si c'est un 
            $sqlProfil = "SELECT libelle FROM ".DB_PREFIXE."om_profil WHERE om_profil = ".$this->om_utilisateur['om_profil'];
            $resProfil=$this->db->getOne($sqlProfil);
            $this->addToLog("getUserInfos(): db->getOne(\"".$sqlProfil."\");", VERBOSE_MODE);
            if (database::isError($resProfil)){
                die();
            }
            // Sauvegarde le libelle du profil
            $this->om_utilisateur["libelle_profil"] = $resProfil;

            // si c'est un administrateur technique
            // XXX Mauvaise méthode, il faut utiliser isAccredited
            if ($resProfil == "ADMINISTRATEUR TECHNIQUE"
                || $resProfil == "ADMINISTRATEUR FONCTIONNEL") {
                $this->user_is_admin = true;
            } else {
                $this->user_is_admin = false;
            }

            //si c'est un service externe
            if ($resProfil == "SERVICE CONSULTÉ") {
                $this->user_is_service_ext = true;
            } else {
                $this->user_is_service_ext = false;
            }

            //si c'est un service interne
            if ($resProfil == "SERVICE CONSULTÉ INTERNE") {
                $this->user_is_service_int = true;
            } else {
                $this->user_is_service_int = false;
            }

            // si c'est un qualificateur
            if ($resProfil == "QUALIFICATEUR") {
                $this->user_is_qualificateur = true;
            } else {
                $this->user_is_qualificateur = false;
            }

            // si c'est un chef de service
            if ($resProfil == "CHEF DE SERVICE") {
                $this->user_is_chef = true;
            } else {
                $this->user_is_chef = false;
            }

            // si c'est un divisionnaire
            if ($resProfil == "DIVISIONNAIRE") {
                $this->user_is_divisionnaire = true;
            } else {
                $this->user_is_divisionnaire = false;
            }
            
            // Récupération des infos instructeur
            $sqlInstr = "SELECT instructeur.instructeur, instructeur.nom, instructeur.telephone,
            division.division, division.code, division.libelle ".
            "FROM ".DB_PREFIXE."instructeur INNER JOIN ".DB_PREFIXE."division ON division.division=instructeur.division ".
            "WHERE instructeur.om_utilisateur = ".$this->om_utilisateur['om_utilisateur'];
            $resInstr=$this->db->query($sqlInstr);
            $this->addToLog("getUserInfos(): db->query(\"".$sqlInstr."\");", VERBOSE_MODE);
            if ( database::isError($resInstr)){
                die();
            }
            $tempInstr=&$resInstr->fetchRow(DB_FETCHMODE_ASSOC);
            // Si il y a un resultat c'est un instructeur
            if(count($tempInstr)>0) {
                $this->user_is_instr=true;
                $this->om_utilisateur = array_merge($this->om_utilisateur,$tempInstr);
            } else {
                $this->user_is_instr=false;
            }
            
            // Récupération des infos de services consultés
            $sqlServ = "SELECT service.service, service.abrege, service.libelle ".
            "FROM ".DB_PREFIXE."service ".
            "INNER JOIN ".DB_PREFIXE."lien_service_om_utilisateur ON lien_service_om_utilisateur.service=service.service ".
            "WHERE lien_service_om_utilisateur.om_utilisateur = ".$this->om_utilisateur['om_utilisateur'];
            $resServ=$this->db->query($sqlServ);
            $this->addToLog("getUserInfos(): db->query(\"".$sqlServ."\");", VERBOSE_MODE);
            if ( database::isError($resServ)){
                die();
            }
            
            while ($tempServ=&$resServ->fetchRow(DB_FETCHMODE_ASSOC)) {
                $this->om_utilisateur['service'][]=$tempServ;
            }
            // Si il y a un resultat c'est un utilisateur de service
            if(isset($this->om_utilisateur['service'])) {
                $this->user_is_service=true;
            } else {
                $this->user_is_service=false;
            }
        }
    }

    /**
     * getter user_is_service
     */
    function isUserService() {
        //
        if (is_null($this->user_is_service)) {
            //
            $this->getUserInfos();
        }
        //
        return $this->user_is_service;
    }

    /**
     * getter user_is_instr
     */
    function isUserInstructeur() {
        //
        if (is_null($this->user_is_instr)) {
            //
            $this->getUserInfos();
        }
        //
        return $this->user_is_instr;
    }

    function isUserAdministrateur() {
        //
        if (is_null($this->user_is_admin)) {
            //
            $this->getUserInfos();
        }
        //
        return $this->user_is_admin;
    }

    /**
     * getter user_is_service_ext
     */
    function isUserServiceExt() {
        //
        if (is_null($this->user_is_service_ext)) {
            //
            $this->getUserInfos();
        }
        //
        return $this->user_is_service_ext;
    }

    /**
     * getter user_is_service_int
     */
    function isUserServiceInt() {
        //
        if (is_null($this->user_is_service_int)) {
            //
            $this->getUserInfos();
        }
        //
        return $this->user_is_service_int;
    }

    /**
     * getter user_is_qualificateur
     */
    function isUserQualificateur() {
        //
        if (is_null($this->user_is_qualificateur)) {
            //
            $this->getUserInfos();
        }
        //
        return $this->user_is_qualificateur;
    }

    /**
     * getter user_is_chef
     */
    function isUserChef() {
        //
        if (is_null($this->user_is_chef)) {
            //
            $this->getUserInfos();
        }
        //
        return $this->user_is_chef;
    }

    /**
     * getter user_is_divisionnaire
     */
    function isUserDivisionnaire() {
        //
        if (is_null($this->user_is_divisionnaire)) {
            //
            $this->getUserInfos();
        }
        //
        return $this->user_is_divisionnaire;
    }

    /**
     * Méthode permettant de définir si l'utilisateur connecté peut ajouter un
     * événement d'instruction
     *
     * @param integer $idx identifiant du dossier
     * @param string  $obj objet
     *
     * @return boolean true si il peut false sinon
     */
    function isUserCanAddObj($idx, $obj) {
        // Si il à le droit "bypass" il peut ajouter
        if($this->isAccredited($obj."_ajouter_bypass") === true) {
            return true;
        }
        if($this->isAccredited(array($obj."_ajouter", $obj), "OR") === false) {
            return false;
        }
        
        
        // On inclut le script qui déclare la classe à instancier
        require_once "../obj/".$obj.".class.php";
        $return = false;
        
        $object_instance = new $obj(
            ']',
            $this->db,
            0
        );
        // Si il n'est pas dans la même division on défini le retour comme faux
        // à moins qu'il ai un droit de changement de decision
        if($this->isUserInstructeur() === true &&
            ($object_instance->getDivisionFromDossier($idx) == $_SESSION["division"] or
            ($obj == "instruction" &&
            $object_instance->isInstrCanChangeDecision($idx) === true))) {

            $return = true;
        }

        return $return;
    }


    /**
     * Ajout de variables de session contenant la division pour permettre une
     * utilisation plus efficace dans les requetes.
     *
     * @param array $utilisateur Tableau d'informations de l'utilisateur.
     */
    function triggerAfterLogin($utilisateur = NULL) {
        // Récupération de la division de l'utilisateur.
        $sql = "SELECT instructeur.division, division.code
            FROM ".DB_PREFIXE."instructeur
            LEFT JOIN ".DB_PREFIXE."division
            ON instructeur.division = division.division
            WHERE instructeur.om_utilisateur='".$utilisateur["om_utilisateur"]."'";
        $res = $this->db->query($sql);
        $this->addToLog(
            "triggerAfterLogin(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        if (database::isError($res)) {
            die();
        }
        $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
        // Enregistrement de la division en session
        if (isset($row["division"]) && $row["division"] != NULL) {
            $_SESSION["division"] = $row["division"];
            $_SESSION["division_code"] = $row["code"];
        } else {
            $_SESSION["division"] = "0";
            $_SESSION["division_code"] = "";
        }
        // Récupération du paramétrage des groupes de l'utilisateur.
        $sqlGroupes = "SELECT groupe.code, lien_om_utilisateur_groupe.confidentiel, lien_om_utilisateur_groupe.enregistrement_demande, groupe.libelle
            FROM ".DB_PREFIXE."groupe
            RIGHT JOIN ".DB_PREFIXE."lien_om_utilisateur_groupe ON
                lien_om_utilisateur_groupe.groupe = groupe.groupe
            WHERE lien_om_utilisateur_groupe.login = '".$utilisateur["login"] ."'
            ORDER BY libelle";
        $resGroupes = $this->db->query($sqlGroupes);
        $this->addToLog(
            "triggerAfterLogin(): db->query(\"".$sqlGroupes."\");",
            VERBOSE_MODE
        );
        if (database::isError($resGroupes)) {
            die();
        }
        // Si aucun résultat alors récupération du paramétrage des groupes du profil
        if ($resGroupes->numRows() === 0) {
            $sqlGroupes = "SELECT groupe.code, lien_om_profil_groupe.confidentiel, lien_om_profil_groupe.enregistrement_demande, groupe.libelle
                FROM ".DB_PREFIXE."groupe
                RIGHT JOIN ".DB_PREFIXE."lien_om_profil_groupe ON
                    lien_om_profil_groupe.groupe = groupe.groupe
                    AND om_profil = ".$utilisateur["om_profil"] ."
                ORDER BY libelle";
            $resGroupes = $this->db->query($sqlGroupes);
            $this->addToLog(
                "triggerAfterLogin(): db->query(\"".$sqlGroupes."\");",
                VERBOSE_MODE
            );
            if (database::isError($resGroupes)) {
                die();
            }
        }
        $_SESSION["groupe"] = array();
        // Enregistrement des groupe en session
        while ($row = $resGroupes->fetchrow(DB_FETCHMODE_ASSOC)) {
            if ($row["confidentiel"] === 't') {
                $row["confidentiel"] = true;
            } else {
                $row["confidentiel"] = false;
            }
            if ($row["enregistrement_demande"] === 't') {
                $row["enregistrement_demande"] = true;
            } else {
                $row["enregistrement_demande"] = false;
            }
            $_SESSION["groupe"][$row["code"]] = array(
                "confidentiel" => $row["confidentiel"],
                "enregistrement_demande" => $row["enregistrement_demande"],
                "libelle" => $row["libelle"],
            );
        }
    }

    // Affichage des actions supplémentaires
    function displayActionExtras() {
        // Affichage de la division si l'utilisateur en possède une
        if ($_SESSION["division"] != 0) { 
            echo "\t\t\t<li class=\"action-division\">";
            echo "(".$_SESSION['division_code'].")";
            echo "</li>\n";
        }
    }

    // }}}


    // {{{ GESTION DES FICHIERS
    
    /**
     *
     */
    function notExistsError ($explanation = NULL) {
        // message
        $message_class = "error";
        $message = _("Cette page n'existe pas.");
        $this->addToMessage ($message_class, $message);
        //
        $this->setFlag(NULL);
        $this->display();
        
        //
        die();
    }
    
     // }}}
    /**
     * Retourne le statut du dossier d'instruction
     * @param string $idx Identifiant du dossier d'instruction
     * @return string Le statut du dossier d'instruction
     */
    function getStatutDossier($idx){
        
        $statut = '';
        
        //Si l'identifiant du dossier d'instruction fourni est correct
        if ( $idx != '' ){
            
            //On récupère le statut de l'état du dossier à partir de l'identifiant du
            //dossier d'instruction
            $sql = "SELECT etat.statut
                FROM ".DB_PREFIXE."dossier
                LEFT JOIN
                    ".DB_PREFIXE."etat
                    ON
                        dossier.etat = etat.etat
                WHERE dossier ='".$idx."'";
            $statut = $this->db->getOne($sql);
            $this->addToLog("getStatutDossier() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            if ( database::isError($statut)){
                die();
            }
        }
        return $statut;
    }

    /**
     * Formate le champ pour le type Timestamp
     * @param  date  $date_str 		Date
     * @param  boolean $show     	Format pour l'affichage
     * @return mixed            	False si le traitement échoue ou la date formatée
     */
    function formatTimestamp ($date_str, $show = true) {

        // Sépare la date et l'heure
        $date = explode(" ", $date_str);
        if (count($date) != 2) {
            return false;
        }

        // Date en BDD
        $date_db = explode ('-', $date[0]);
		// Date en affichage
        $date_show = explode ('/', $date[0]);

        // Contrôle la composition de la date
        if (count ($date_db) != 3 and count ($date_show) != 3) {
            return false;
        }

        if (count ($date_db) == 3) {
            // Vérifie que c'est une date valide
            if (!checkdate($date_db[1], $date_db[2], $date_db[0])) {
                return false;
            }
            // Si c'est pour l'affichage de la date
            if ($show == true) {
                return $date_db [2]."/".$date_db [1]."/".$date_db [0]." ".$date[1];
            } else {
                return $date[0];
            }
        }

        //
        if (count ($date_show) == 3) {
            // Vérifie que c'est une date valide
            if (!checkdate($date_show[1], $date_show[0], $date_show[2])) {
                return false;
            }
            // Si c'est pour l'affichage de la date
            if ($show == true) {
                return $date[0];
            } else {
                return $date_show [2]."-".$date_show [1]."-".$date_show [0]." ".$date[1];
            }

        }
        return false;

    }

    /**
     * Permet de calculer la liste des parcelles à partir de la chaîne passée en paramètre
     * et la retourner sous forme d'un tableau associatif
     * 
     * @param  string $strParcelles     Chaîne de la parcelles.
     * @param  string $collectivite_idx Collectivite de la parcelle.
     * 
     * @return array (array(prefixe, quartier, section, parcelle), ...)
     */
    function parseParcelles($strParcelles, $collectivite_idx = null) {
        
        // Séparation des lignes
        $references = explode(";", $strParcelles);
        $liste_parcelles = array();
        
        // On boucle sur chaque ligne pour ajouter la liste des parcelles de chaque ligne
        foreach ($references as $parcelles) {
            
            // On transforme la chaîne de la ligne de parcelles en tableau
            $ref = str_split($parcelles);
            // Les 1er caractères sont numériques
            $num = true;
            
            // Tableau des champs de la ligne de références cadastrales
            $reference_tab = array();
            $temp = "";
            foreach ($ref as $carac) {
                
                // Permet de tester si le caractère courant est de même type que le précédent
                if(is_numeric($carac) === $num) {
                    $temp .= $carac;
                } else {
                    // Bascule
                    $num = !$num;
                    // On stock le champ
                    $reference_tab[] = $temp;
                    // re-init de la valeur temporaire pour le champ suivant
                    $temp = $carac;
                }
            }
            // Stockage du dernier champ sur lequel il n'y a pas eu de bascule
            $reference_tab[] = $temp;
            // Calcul des parcelles
            $quartier = $reference_tab[0];
            $sect = $reference_tab[1];

            $ancien_ref_parc = "";
            for ($i=2; $i < count($reference_tab); $i+=2) {
                if($collectivite_idx != null) {
                    // Récupération du code impot de l'arrondissement
                    $collectivite = $this->getCollectivite($collectivite_idx);
                    $parc["prefixe"] = $this->get_arrondissement_code_impot($quartier);
                }
                $parc["quartier"] = $quartier;
                // Met en majuscule si besoin
                $parc["section"] = strtoupper($sect);
                if( $ancien_ref_parc == "" OR $reference_tab[$i-1] == "/") {
                    // 1ere parcelle ou parcelle individuelle
                    // Compléte par des "0" le début de la chaîne si besoin
                    $parc["parcelle"] = str_pad($reference_tab[$i], 4, "0", STR_PAD_LEFT);
                    // Ajout d'une parcelle à la liste
                    $liste_parcelles[] = $parc;
                } elseif ($reference_tab[$i-1] == "A") {
                    // Interval de parcelles
                    for ($j=$ancien_ref_parc+1; $j <= $reference_tab[$i]; $j++) {
                        // Compléte par des "0" le début de la chaîne si besoin
                        $parc["parcelle"] = str_pad($j, 4, "0", STR_PAD_LEFT);
                        // Ajout d'une parcelle à la liste
                        $liste_parcelles[] = $parc;
                    }
                }
                //Gestion des erreurs
                else{
                    
                    echo _("Une erreur de formattage a ete detecte dans la reference cadastrale du dossier ").$this->row['dossier'];
                }
                // Sauvegarde de la référence courante de parcelle
                $ancien_ref_parc = $reference_tab[$i];
            }
        }

        return $liste_parcelles;
    }


    /**
     * Récupère le code impôt par rapport au quartier.
     * 
     * @param string $quartier Numéro de quartier.
     * @return string Code impôts.
     */
    protected function get_arrondissement_code_impot($quartier) {
        // Initialisation
        $code_impots = "";
        // Si le quartier fournis est correct
        if ($quartier != "") {
            // Requête SQL
            $sql = "SELECT
                        arrondissement.code_impots
                    FROM
                        ".DB_PREFIXE."arrondissement
                    LEFT JOIN
                        ".DB_PREFIXE."quartier
                        ON
                            quartier.arrondissement = arrondissement.arrondissement 
                    WHERE
                        quartier.code_impots = '".$quartier."'";

        }
        $code_impots = $this->db->getOne($sql);
        if ($code_impots === null) {
            $code_impots = "";
        }
        $this->isDatabaseError($code_impots); 
        // Retour
        return $code_impots;
    }


    /**
     * Formate les parcelles en ajoutant le code impôt
     * @param  array    $liste_parcelles   Tableau des parcelles
     * @return string                      Liste des parcelles formatées
     */
    function formatParcelleToSend($liste_parcelles) {

        //
        $wParcelle = array();

        //Formatage des références cadastrales pour l'envoi
        foreach ($liste_parcelles as $value) {
                
            // On ajoute les données dans le tableau que si quartier + section + parcelle
            // a été fourni
            if ($value["quartier"] !== ""
                && $value["section"] !== ""
                && $value["parcelle"] !== ""){
                
                //On récupère le code impôt de l'arrondissement
                $arrondissement = $this->getCodeImpotByQuartier($value["quartier"]);
                
                //On ajoute la parcelle, si un arrondissement a été trouvé
                if ($arrondissement!=="") {
                    //
                    $wParcelle[] = $arrondissement.$value["quartier"].
                        str_pad($value["section"], 2, " ", STR_PAD_LEFT).
                        $value["parcelle"];
                }
            }
        }

        //
        return $wParcelle;
    }

    /**
     * Récupère le code impôt par rapport au quartier
     * @param  string $quartier Numéro de quartier
     * @return string           Code impôt
     */
    function getCodeImpotByQuartier($quartier) {

        $arrondissement = "";

        // Si le quartier fournis est correct
        if ($quartier != "") {

            // Requête SQL
            $sql = "SELECT
                        arrondissement.code_impots
                    FROM
                        ".DB_PREFIXE."arrondissement
                    LEFT JOIN
                        ".DB_PREFIXE."quartier
                        ON
                            quartier.arrondissement = arrondissement.arrondissement 
                    WHERE
                        quartier.code_impots = '".$quartier."'";
            $this->addToLog("getCodeImpotByQuartier() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $arrondissement = $this->db->getOne($sql);
            $this->isDatabaseError($arrondissement);
        }

        // Retour
        return $arrondissement;
    }


    /**
     * Retourne true si tous les paramètres du SIG externe ont bien été définis
     * @return bool true/false
     */
    public function issetSIGParameter($idx) {
        $collectivite_idx = $this->get_collectivite_of_element("dossier", $idx);
        $collectivite = $this->getCollectivite($collectivite_idx);
        if(isset($collectivite["sig"])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de vérifier que des champs existe dans une table
     * @param  array  $list_fields Liste des champs à tester
     * @param  string $table       Table où les champs doivent exister
     * @return mixed               Retourne les champs qui n'existent pas
     *                             ou true
     */
    public function check_field_exist($list_fields, $table) {

        // Instance de la classe en paramètre
        require_once "../obj/".$table.".class.php";
        $object = new $table("]", $this->db, DEBUG);

        // Récupère les champs de la table
        foreach ($object->champs as $champ) {
            $list_column[] = $champ;
        }

        // Tableau des champs en erreur
        $error_fields = array();

        // Pour chaque champ à tester
        foreach ($list_fields as $value) {
            
            // S'il n'apparaît pas dans la liste des champs possible
            if (!in_array($value, $list_column)) {

                // Alors le champ est ajouté au tableau des erreurs
                $error_fields[] = $value;
            }
        }

        // Si le tableau des erreurs n'est pas vide on le retourne
        if (count($error_fields) > 0) {
            return $error_fields;
        }

        // Sinon on retourne le booléen true
        return true;

    }

    /*
     * 
     */
    /**
     * Récupère la lettre type lié à un événement
     * @param  integer  $evenement L'identifiant de l'événement
     * @return integer             Retourne l'idenfiant de la lettre-type                             ou true
     */
    function getLettreType($evenement){
         
         $lettretype = NULL;
         
         $sql = 
            "SELECT
                lettretype
            FROM
                ".DB_PREFIXE."evenement
            WHERE
                evenement = $evenement";
            
        $this->addToLog("getLettreType() : db->query(\"".$sql."\")", VERBOSE_MODE);
        $res = $this->db->query($sql);
        if ( database::isError($res)){
            die();
        }
        
        if ( $res->numrows() > 0 ){
                
            $row=& $res->fetchRow(DB_FETCHMODE_ASSOC);
            $lettretype = $row['lettretype'];
        }
       
        return $lettretype;
    }
    
    /**
     * Retourne le type de dossier d'autorisation du dossier courant :
     * @param $idxDossier Le numéro du dossier d'instruction
     * @return le code du type détaillée de dossier d'autorisation
     **/
    function getDATDCode($idxDossier) {
        $sql = "SELECT dossier_autorisation_type_detaille.code
                FROM ".DB_PREFIXE."dossier_autorisation_type_detaille
                INNER JOIN ".DB_PREFIXE."dossier_autorisation
                    ON dossier_autorisation_type_detaille.dossier_autorisation_type_detaille =
                       dossier_autorisation.dossier_autorisation_type_detaille
                INNER JOIN ".DB_PREFIXE."dossier ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
                WHERE dossier.dossier = '".$idxDossier."'";
        $res = $this->db->getOne($sql);
        $this->addToLog("getDATDCode() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
        if ( database::isError($res)){
            die();
        }
        return $res;
    }

    /**
     * Retourne le type de dossier d'autorisation du dossier courant :
     * @param $idxDossier Le numéro du dossier d'instruction
     * @return le code du type de dossier d'autorisation
     **/
    function getDATCode($idxDossier) {
        $sql = "
        SELECT 
            dossier_autorisation_type.code
        FROM 
            ".DB_PREFIXE."dossier_autorisation_type
            INNER JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
                ON dossier_autorisation_type.dossier_autorisation_type=dossier_autorisation_type_detaille.dossier_autorisation_type
            INNER JOIN ".DB_PREFIXE."dossier_autorisation
                ON dossier_autorisation_type_detaille.dossier_autorisation_type_detaille=dossier_autorisation.dossier_autorisation_type_detaille
            INNER JOIN ".DB_PREFIXE."dossier 
                ON dossier.dossier_autorisation=dossier_autorisation.dossier_autorisation
        WHERE 
            dossier.dossier = '".$idxDossier."'
        ";
        $res = $this->db->getOne($sql);
        $this->addToLog(__METHOD__."(): db->getOne(\"".$sql."\")", VERBOSE_MODE);
        if ( database::isError($res)){
            die();
        }
        return $res;
    }

    /**
     * Permet de copier un enregistrement
     * @param  mixed $idx   Identifiant de l'enregistrment
     * @param  string $obj   Objet de l'enregistrment
     * @param  string $objsf Objets associés
     * @return array        Tableau des nouveaux id et du message
     */
    function copier($idx, $obj, $objsf) {

        // Tableau de résultat
        $resArray = array();
        // Message retourné à l'utilisateur
        $message = "";
        // Type du message (valid ou error)
        $message_type = "valid";

        // Requête SQL permettant de récupérer les informations sur l'objet métier
        $sql = "SELECT * 
                FROM ".DB_PREFIXE.$obj." 
                WHERE ".$obj." = ".$idx;
        $res = $this->db->query($sql);
        $this->isDatabaseError($res);

        // Valeurs clonées
        $valF = array();
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // Recupère la valeur
            $valF = $row;
        }

        // Valeurs non clonées
        // Identifiant modifié pour que ça soit un ajout
        $valF[$obj] = "]";

        // Inclus la classe de l'objet métier
        require_once "../obj/".$obj.".class.php";

        // Instance de l'objet métier
        $clone_obj = new $obj("]", $this->db, DEBUG);
        // Si dans l'objet métier la fonction "copier" existe
        if (method_exists($clone_obj, "update_for_copy")) {
            // Traitement sur les valeurs du duplicata
            $valF = $clone_obj->update_for_copy($valF, $objsf, DEBUG);
            // Recupère les messages retourné par la fonction
            $message .= $valF['message'];
            // Supprime les messages de la liste des valeurs
            unset($valF['message']);
        }
        // Ajoute le duplicata
        $clone_obj->ajouter($valF, $this->db, DEBUG);
        // Si aucune erreur se produit dans la classe instanciée
        if ($clone_obj->correct === true) {
            // Récupère l'identifiant de l'objet créé
            $clone_obj_id = $clone_obj->valF[$obj];

            // Message
            $message .= sprintf(_("La copie de l'enregistrement %s avec l'identifiant %s s'est effectuee avec succes"), "<span class='bold'>"._($obj)."</span>", "<span class='bold'>".$idx."</span>")."<br />";

            // Ajout de l'identifant au tableau des résultat
            $resArray[$obj.'_'.$idx] = $clone_obj_id;

            // S'il y a au moins un objet metier associé
            if ($objsf != "") {
                // Liste des objet métier associés
                $list_objsf = explode(",", $objsf);
                // Pour chaque objet associé
                foreach ($list_objsf as $key => $objsf) {
                    // Inclus la classe de l'objet métier associé
                    require_once "../obj/".$objsf.".class.php";

                    // Requête SQL permettant de récupérer les informations sur 
                    // l'objet métier associé
                    $sql = "SELECT * 
                            FROM ".DB_PREFIXE.$objsf." 
                            WHERE ".$obj." = ".$idx;
                    $res = $this->db->query($sql);
                    $this->isDatabaseError($res);

                    // Pour chaque élément associé
                    while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
                        // Identifiant de l'objet associé à copier
                        $idxsf = $row[$objsf];

                        // Valeurs clonées
                        $valF = $row;
                        // Valeurs non clonées
                        $valF[$obj] = $clone_obj_id;
                        // Identifiant modifié pour que ça soit un ajout
                        $valF[$objsf] = "]";
                        // Instance de l'objet métier associé
                        $clone_objsf = new $objsf("]", $this->db, DEBUG);
                        // Si dans l'objet métier associé 
                        // la fonction "copier" existe
                        if (method_exists($clone_objsf, "update_for_copy")) {
                            // Traitement sur les valeurs du duplicata
                            $valF = $clone_objsf->update_for_copy($valF, $objsf, DEBUG);
                            // Recupère les messages retourné par la fonction
                            $message .= $valF['message'];
                            // Supprime les messages de la liste des valeurs
                            unset($valF['message']);
                        }
                        // Ajoute le duplicata
                        $clone_objsf->ajouter($valF, $this->db, DEBUG);
                        // Si aucune erreur se produit dans la classe instanciée
                        if ($clone_objsf->correct === true) {
                            // Récupère l'identifiant de l'objet créé
                            $clone_objsf_id = $clone_objsf->valF[$objsf];

                            // Message
                            $message .= sprintf(_("La copie de l'enregistrement %s avec l'identifiant %s s'est effectuee avec succes"), "<span class='bold'>"._($objsf)."</span>", "<span class='bold'>".$idxsf."</span>")."<br />";

                            // Ajout de l'identifant au tableau des résultat
                            $resArray[$objsf.'_'.$row[$objsf]] = $clone_objsf_id;
                        } else {

                            // Message d'erreur récupéré depuis la classe
                            $message .= $clone_objsf->msg;
                            // Type du message 
                            $message_type = "error";
                        }
                    }
                }
            }
        //    
        } else {

            // Message d'erreur récupéré depuis la classe
            $message .= $clone_obj->msg;
            // Type du message 
            $message_type = "error";
        }

        // Ajout du message au tableau des résultats
        $resArray['message'] = $message;
        // Ajout du type de message au tableau des résultats
        $resArray['message_type'] = $message_type;

        // Retourne le tableau des résultats
        return $resArray;
    }

    /**
     * Cette fonction prend en entrée le ou les paramètres du &contrainte qui sont entre
     * parenthèses (un ensemble de paramètres séparés par des points-virgules). Elle
     * sépare les paramètres et leurs valeurs puis construit et retourne un tableau 
     * associatif qui contient pour les groupes et sous-groupes :
     * - un tableau de valeurs, avec un nom de groupe ou sous-groupe par ligne
     * pour les autres options :
     * - la valeur de l'option
     * 
     * @param string $contraintes_param    Chaîne contenant tous les paramètres
     * 
     * @return array  Tableau associatif avec paramètres et valeurs séparés
     */
    function explodeConditionContrainte($contraintes_param) {

        // Initialisation des variables
        $return = array();
        $listGroupes = "";
        $listSousgroupes = "";
        $service_consulte = "";
        $affichage_sans_arborescence = "";

        // Sépare toutes les conditions avec leurs valeurs et les met dans un tableau
        $contraintes_params = explode(";", $contraintes_param);
        
        // Pour chaque paramètre de &contraintes
        foreach ($contraintes_params as $value) {
            // Récupère le mot-clé "liste_groupe" et les valeurs du paramètre
            if (strstr($value, "liste_groupe=")) { 
                // On enlève le mots-clé "liste_groupe=", on garde les valeurs
                $listGroupes = str_replace("liste_groupe=", "", $value);
            }
            // Récupère le mot-clé "liste_ssgroupe" et les valeurs du paramètre
            if (strstr($value, "liste_ssgroupe=")) { 
                // On enlève le mots-clé "liste_ssgroupe=", on garde les valeurs
                $listSousgroupes = str_replace("liste_ssgroupe=", "", $value);
            }
            // Récupère le mot-clé "service_consulte" et la valeur du paramètre
            if (strstr($value, "service_consulte=")) { 
                // On enlève le mots-clé "service_consulte=", on garde sa valeur
                $service_consulte = str_replace("service_consulte=", "", $value);
            }
            // Récupère le mot-clé "affichage_sans_arborescence" et la valeur du
            // paramètre
            if (strstr($value, "affichage_sans_arborescence=")) { 
                // On enlève le mots-clé "affichage_sans_arborescence=", on garde la valeur
                $affichage_sans_arborescence = str_replace("affichage_sans_arborescence=", "", $value);
            }
        }

        // Récupère dans des tableaux la liste des groupes et sous-groupes qui  
        // doivent être utilisés lors du traitement de la condition
        if ($listGroupes != "") {
            $listGroupes = array_map('trim', explode(",", $listGroupes));
        }
        if ($listSousgroupes != "") {
            $listSousgroupes = array_map('trim', explode(",", $listSousgroupes));
        }

        // Tableau à retourner
        $return['groupes'] = $listGroupes;
        $return['sousgroupes'] = $listSousgroupes;
        $return['service_consulte'] = $service_consulte;
        $return['affichage_sans_arborescence'] = $affichage_sans_arborescence;
        return $return;
    }

    /**
     * Méthode qui complète la clause WHERE de la requête SQL de récupération des
     * contraintes, selon les paramètres fournis. Elle permet d'ajouter une condition sur
     * les groupes, sous-groupes et les services consultés.
     * 
     * @param $string  $part  Contient tous les paramètres fournis à &contraintes séparés
     * par des points-virgules, tel que définis dans l'état.
     * array[]  $conditions  Paramètre optionnel, contient les conditions déjà explosées
     * par la fonction explodeConditionContrainte()
     * 
     * @return string    Contient les clauses WHERE à ajouter à la requête SQL principale.
     */
    function traitement_condition_contrainte($part, $conditions = NULL) {

        // Initialisation de la condition
        $whereContraintes = "";
        // Lorsqu'on a déjà les conditions explosées dans le paramètre $conditions, on
        // utilise ces données. Sinon, on appelle la méthode qui explose la chaîne de 
        // caractères contenant l'ensemble des paramètres.
        if (is_array($conditions)){
            $explodeConditionContrainte = $conditions;
        }
        else {
            $explodeConditionContrainte = $this->explodeConditionContrainte($part);
        }
        // Récupère les groupes, sous-groupes et service_consulte pour la condition
        $groupes = $explodeConditionContrainte['groupes'];
        $sousgroupes = $explodeConditionContrainte['sousgroupes'];
        $service_consulte = $explodeConditionContrainte['service_consulte'];

        // Pour chaque groupe
        if ($groupes != "") {
            foreach ($groupes as $key => $groupe) {
                // Si le groupe n'est pas vide
                if (!empty($groupe)) {
                    // Choisit l'opérateur logique
                    $op_logique = $key > 0 ? 'OR' : 'AND (';
                    // Ajoute la condition
                    $whereContraintes .= " ".$op_logique." lower(trim(both E'\n\r\t' from contrainte.groupe)) = lower('"
                        .pg_escape_string($groupe)."')";
                }
            }
            // S'il y a des valeurs dans groupe
            if (count($groupe) > 0) {
                // Ferme la parenthèse
                $whereContraintes .= " ) ";
            }
        }

        // Pour chaque sous-groupe
        if ($sousgroupes != "") {
            foreach ($sousgroupes as $key => $sousgroupe) {
                // Si le sous-groupe n'est pas vide
                if (!empty($sousgroupe)) {
                    // Choisit l'opérateur logique
                    $op_logique = $key > 0 ? 'OR' : 'AND (';
                    // Ajoute la condition
                    $whereContraintes .= " ".$op_logique." lower(trim(both E'\n\r\t' from contrainte.sousgroupe)) = lower('"
                        .pg_escape_string($sousgroupe)."')";
                }
            }
            // S'il y a des valeurs dans sous-groupe
            if (count($sousgroupes) > 0) {
                // Ferme la parenthèse
                $whereContraintes .= " ) ";
            }
        }

        // Si l'option service_consulte n'est pas vide
        if ($service_consulte != "") {
            // Ajoute la condition
            $whereContraintes .= " AND service_consulte = cast(lower('".$service_consulte."') as boolean) ";
        }

        // Condition retournée
        return $whereContraintes;
    }

    /**
     * Calcule une date par l'ajout ou la soustraction de mois ou de jours.
     *
     * @param date    $date     Date de base (format dd-mm-yyyy)
     * @param integer $delay    Délais à ajouter
     * @param string  $operator Opérateur pour le calcul ("-" ou "+")
     * @param string  $type     Type de calcul (mois ou jour)
     * 
     * @return date             Date calculée
     */
    function mois_date($date, $delay, $operator = "+", $type = "mois") {

        // Si un type n'est pas définit
        if ($type != "mois" && $type != "jour") {
            //
            return null;
        }

        // Si aucune date n'a été fournie ou si ce n'est pas une date correctement 
        // formatée
        if ( is_null($date) || $date == "" ||
            preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $date) == 0 ){
            return null;
        }

        // Si l'opérateur n'est pas définit
        if ($operator != "+" && $operator != "-") {
            //
            return null;
        }

        // Découpage de la date
        $temp = explode("-", $date);
        $day = (int) $temp[2];
        $month = (int) $temp[1];
        $year = (int) $temp[0];

        // Si c'est un calcul sur des mois
        // Le calcul par mois ne se fait pas comme le calcul par jour car
        // les fonctions PHP ne réalisent pas les calculs réglementaires
        if ($type == "mois") {

            // Si c'est une addition
            if ($operator == '+') {
                // Année à ajouter
                $year += floor($delay / 12);
                // Mois restant
                $nb_month = ($delay % 12);
                // S'il y a des mois restant
                if ($nb_month != 0) {
                    // Ajout des mois restant
                    $month += $nb_month;
                    // Si ça dépasse le mois 12 (décembre)
                    if ($month > 12) {
                        // Soustrait 12 au mois
                        $month -= 12;
                        // Ajoute 1 à l'année
                        $year += 1;
                    }
                }
            }

            // Si c'est une soustraction
            if ($operator == "-") {
                // Année à soustraire
                $year -= floor($delay / 12);
                // Mois restant
                $nb_month = ($delay % 12);
                // S'il y a des mois restant
                if ($nb_month != 0) {
                    // Soustrait le délais
                    $month -= $nb_month;
                    // Si ça dépasse le mois 1 (janvier)
                    if ($month < 1) {
                        // Soustrait 12 au mois
                        $month += 12;
                        // Ajoute 1 à l'année
                        $year -= 1;
                    }
                }
            }

            // Calcul du nombre de jours dans le mois sélectionné
            switch($month) {
                // Mois de février
                case "2":
                    if ($year % 4 == 0 && $year % 100 != 0 || $year % 400 == 0) {
                        $day_max = 29;
                    } else {
                        $day_max = 28;
                    }
                break;
                // Mois d'avril, juin, septembre et novembre
                case "4":
                case "6":
                case "9":
                case "11":
                    $day_max = 30;
                break;
                // Mois de janvier, mars, mai, juillet, août, octobre et décembre 
                default:
                    $day_max = 31;
            }

            // Si le jour est supérieur au jour maximum du mois
            if ($day > $day_max) {
                // Le jour devient le jour maximum
                $day = $day_max;
            }

            // Compléte le mois et le jour par un 0 à gauche si c'est un chiffre
            $month = str_pad($month, 2, "0", STR_PAD_LEFT);
            $day = str_pad($day, 2, "0", STR_PAD_LEFT);

            // Résultat du calcul
            $date_result = $year."-".$month."-".$day;
        }

        // Si c'est un calcul sur des jours
        if ($type == "jour") {
            //
            $datetime = new DateTime($date);
            // Si le délai est un numérique
            if (is_numeric($delay)) {
                // Modifie la date
                $datetime->modify($operator.$delay.' days');
            }
            // Résultat du calcul
            $date_result = $datetime->format('Y-m-d');
        }

        // Retourne la date calculée
        return $date_result;
    }

    /**
     * Vérifie la valididité d'une date.
     * 
     * @param string $pDate Date à vérifier
     * 
     * @return boolean
     */
    function check_date($pDate) {

        // Vérifie si c'est une date valide
        if (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $pDate, $date) 
            && checkdate($date[2], $date[3], $date[1]) 
            && $date[1] >= 1900) {
            //
            return true;
        }

        //
        return false;
    }
    
    /**
     * Permet de tester le bypass
     * 
     * @param string $obj le nom de l'objet
     * @param string $permission_suffix  
     * @return boolean
     */
    function can_bypass($obj="", $permission_suffix=""){
        //On teste le droit bypass
        if ($permission_suffix!=""&&$obj!=""&&
            $this->isAccredited($obj."_".$permission_suffix."_bypass")){
            return true;
        }
        return false;
    }


    /**
     * Vérifie l'option de numérisation.
     *
     * @return boolean
     */
    public function is_option_digitalization_folder_enabled() {
        //
        if ($this->getParameter("option_digitalization_folder") !== true) {
            //
            return false;
        }
        //
        return true;
    }


    /**
     * Vérifie que l'option d'accès au portail citoyen est activée.
     *
     * @param integer $om_collectivite Identifiant de la collectivité.
     *
     * @return boolean
     */
    public function is_option_citizen_access_portal_enabled($om_collectivite=null) {
        //
        $parameters = $this->getCollectivite($om_collectivite);
        //
        if (isset($parameters['option_portail_acces_citoyen']) === true
            && $parameters['option_portail_acces_citoyen'] === 'true') {
            //
            return true;
        }

        //
        return false;
    }


    /**
     * Vérifie que l'option du SIG est activée.
     *
     * @return boolean
     */
    public function is_option_sig_enabled() {
        //
        $option = $this->getParameter("option_sig");
        //
        if ($option !== 'sig_externe' && $option !== 'sig_interne') {
            //
            return false;
        }

        //
        return true;
    }


    /**
     * Vérifie que l'option de simulation des taxes est activée.
     *
     * @param integer $om_collectivite Identifiant de la collectivité.
     *
     * @return boolean
     */
    public function is_option_simulation_taxes_enabled($om_collectivite=null) {
        //
        $parameters = $this->getCollectivite($om_collectivite);
        //
        if (isset($parameters['option_simulation_taxes']) === true
            && $parameters['option_simulation_taxes'] === 'true') {
            //
            return true;
        }

        //
        return false;
    }


    /**
     * Vérifie le niveau de la collectivité de l'utilisateur connecté
     * 
     * @return boolean
     */
    function has_collectivite_multi() {
        $idx_multi = $this->get_idx_collectivite_multi();
        if (intval($_SESSION['collectivite']) === intval($idx_multi)) {
            return true;
        }
        return false;
    }


    /**
     * Pour un path absolu donné, retourne le relatif à la racine de
     * l'application.
     *
     * @param string $absolute Chemin absolu.
     *
     * @return mixed Faux si échec sinon chemin relatif.
     */
    public function get_relative_path($absolute) {
        if ($this->get_path_app() === false) {
            return false;
        }
        $path_app = $this->get_path_app();
        return str_replace($path_app, '', $absolute);
    }


    /**
     * Retourne le path absolu de la racine de l'application
     * 
     * @return mixed Faux si échec sinon chemin absolu
     */
    public function get_path_app() {
        $match = array();
        preg_match( '/(.*)\/[a-zA-Z0-9]+\/\.\.\/core\/$/', PATH_OPENMAIRIE, $match);
        // On vérifie qu'il n'y a pas d'erreur
        if (isset($match[1]) === false) {
            return false;
        }
        return $match[1];
    }

    /**
     * Compose un tableau et retourne son code HTML
     * 
     * @param   string  $id       ID CSS du conteneur
     * @param   array   $headers  entêtes de colonnes
     * @param   array   $rows     lignes
     * @return  string            code HTML
     */
    public function compose_generate_table($id, $headers, $rows) {
        //
        $html = '';
        // Début conteneur
        $html .= '<div id="'.$id.'">';
        // Début tableau
        $html .= '<table class="tab-tab">';
        // Début entête
        $html .= '<thead>';
        $html .= '<tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">';
        // Colonnes
        $nb_colonnes = count($headers);
        $index_last_col = $nb_colonnes - 1;
        foreach ($headers as $i => $header) {
            if ($i === 0) {
                $col = ' firstcol';
            }
            if ($i === $index_last_col) {
                $col = ' lastcol';
            }
            $html .= '<th class="title col-'.$i.$col.'">';
                $html .= '<span class="name">';
                    $html .= $header;
                $html .= '</span>';
            $html .= '</th>';
        }
        // Fin entête
        $html .= '</tr>';
        $html .= '</thead>';
        // Début corps
        $html .= '<tbody>';
        // Lignes
        foreach ($rows as $cells) {
            // Début ligne
            $html .= '<tr class="tab-data">';
            // Cellules
            foreach ($cells as $j => $cell) {
                if ($j === 0) {
                $col = ' firstcol';
                }
                if ($j === $index_last_col) {
                    $col = ' lastcol';
                }
                $html .= '<td class="title col-'.$j.$col.'">';
                    $html .= '<span class="name">';
                        $html .= $cell;
                    $html .= '</span>';
                $html .= '</td>';
            }
            // Fin ligne
            $html .= "</tr>";
        }
        // Fin corps
        $html .= '</tbody>';
        // Fin tableau
        $html .= '</table>';
        // Fin conteneur
        $html .= '</div>';
        //
        return $html;
    }

    /**
     * Retourne le login de l'utilisateur connecté + entre parenthèses son nom
     * s'il en a un.
     * 
     * @return  string  myLogin OU myLogin (myName)
     */
    public function get_connected_user_login_name() {
        // Requête et stockage des informations de l'user connecté
        $this->getUserInfos();
        // Si le nom existe et est défini on le récupère
        $nom = "";
        if (isset($this->om_utilisateur["nom"])
            && !empty($this->om_utilisateur["nom"])) {
            $nom = trim($this->om_utilisateur["nom"]);
        }
        // Définition de l'émetteur : obligatoirement son login
        $emetteur = $_SESSION['login'];
        // Définition de l'émetteur : + éventuellement son nom
        if ($nom != "") {
            $emetteur .= " (".$nom.")";
        }
        // Retour
        return $emetteur;
    }

    /**
     * Cette méthode permet d'interfacer le module 'Settings'.
     */
    function view_module_settings() {
        //
        require_once "../obj/settings.class.php";
        $settings = new settings();
        $settings->view_main();
    }


    /**
     * Vérifie que l'option référentiel ERP est activée.
     *
     * @param integer $om_collectivite Identifiant de la collectivité.
     *
     * @return boolean
     */
    public function is_option_referentiel_erp_enabled($om_collectivite=null) {
        //
        $parameters = $this->getCollectivite($om_collectivite);
        //
        if (isset($parameters['option_referentiel_erp']) === true
            && $parameters['option_referentiel_erp'] === 'true') {
            //
            return true;
        }

        //
        return false;
    }


    /**
     * Interface avec le référentiel ERP.
     */
    function send_message_to_referentiel_erp($code, $infos) {
        //
        require_once "../obj/interface_referentiel_erp.class.php";
        $interface_referentiel_erp = new interface_referentiel_erp();
        $ret = $interface_referentiel_erp->send_message_to_referentiel_erp($code, $infos);
        return $ret;
    }

    /**
     * Récupère la liste des identifiants des collectivités
     *
     * @param  string $return_type 'string' ou 'array' selon que l'on retourne
     *                             respectivement une chaîne ou un tableau
     * @param  string $separator   caractère(s) séparateur(s) employé(s) lorsque
     *                             l'on retourne une chaîne, inutilisé si tableau
     * @return mixed               possibilité de boolean/string/array :
     *                             false si erreur BDD sinon résultat
     */
    public function get_list_id_collectivites($return_type = 'string', $separator = ',') {
        $sql = "
        SELECT
        array_to_string(
            array_agg(om_collectivite),
            '".$separator."'
        ) as list_id_collectivites
        FROM ".DB_PREFIXE."om_collectivite";
        $list = $this->db->getone($sql);
        $this->addTolog(
            __FILE__." - ".__METHOD__." : db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        if ($this->isDatabaseError($list, true)) {
            return false;
        }
        if ($return_type === 'array') {
            return explode($separator, $list);
        }
        return $list;
    }

    /**
     * Teste si l'utilisateur connecté appartient au groupe indiqué en paramètre
     * ou s'il a le goupe bypass.
     *
     * @param  string  $groupe Code du groupe : ADS / CTX / CU / RU / ERP.
     * @return boolean         vrai si utilisateur appartient au groupe fourni
     */
    public function is_user_in_group($groupe) {
        if (isset($_SESSION['groupe']) === true
            && (array_key_exists($groupe, $_SESSION['groupe']) === true
                || array_key_exists("bypass", $_SESSION['groupe']) === true)) {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - can_user_access_dossiers_confidentiels_from_groupe
     *
     * Permet de savoir si le type de dossier d'autorisation du dossier courant est
     * considéré comme confidentiel ou si l'utilisateur a le groupe bypass.
     *
     * @param string $groupe Code du groupe : ADS / CTX / CU / RU / ERP.
     * @return boolean true si l'utilisateur à accès aux dossiers confidentiels du groupe
     * passé en paramètre, sinon false.
     *
     */
    public function can_user_access_dossiers_confidentiels_from_groupe($groupe) {
        if ((isset($_SESSION['groupe'][$groupe]['confidentiel']) === true
            AND $_SESSION['groupe'][$groupe]['confidentiel'] === true)
            || array_key_exists("bypass", $_SESSION['groupe']) === true) {
            return true;
        }
        return false;
    }

    public function starts_with($haystack, $needle) {
         $length = strlen($needle);
         return (substr($haystack, 0, $length) === $needle);
    }

    public function ends_with($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (substr($haystack, -$length) === $needle);
    }

     /**
      * Récupère le type définit dans la base de données des champs d'une table
      * entière ou d'un champs si celui-ci est précisé.
      *
      * Liste des types BDD :
      * - int4
      * - varchar
      * - bool
      * - numeric
      * - text
      *
      * @param string $table  Nom de la table.
      * @param string $column Nom de la colonne (facultatif).
      *
      * @return array
      */
     public function get_type_from_db($table, $column = null) {
         // Composition de la requête
         $sql_select = ' SELECT column_name, udt_name ';
         $sql_from = ' FROM information_schema.columns ';
         $sql_where = ' WHERE table_schema = \''.str_replace('.', '', DB_PREFIXE).'\' AND table_name = \''.$table.'\' ';
         $sql_order = ' ORDER BY ordinal_position ';
 
         // Si une colonne est précisé
         if ($column !== null || $column !== '') {
             //
             $sql_where .= ' AND column_name = \''.$column.'\' ';
         }
 
         // Requête SQL
         $sql = $sql_select.$sql_from.$sql_where.$sql_order;
         // Exécution de la requête
         $res = $this->db->query($sql);
         // Log
         $this->addToLog(__METHOD__."() : db->query(\"".$sql."\");", VERBOSE_MODE);
         // Erreur BDD
         $this->isDatabaseError($res);
         //
         $list_type = array();
         while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
             $list_type[$row['column_name']] = $row['udt_name'];
         }
 
         // Retourne la liste des codes
         return $list_type;
     }


    /**
     * Cette méthode permet de récupérer le code de division correspondant
     * au dossier sur lequel on se trouve.
     *
     * Méthode identique à la méthode getDivisionFromDossier() de la classe
     * om_dbform à l'exception d'un cas de récupération du numéro de dossier par
     * la méthode getVal(). Cette exception permet d'utiliser cette méthode dans
     * les scripts instanciant seulement la classe utils tel que les *.inc.php.
     *
     * @param string $dossier Identifiant du dossier d'instruction.
     *
     * @return string Code de la division du dossier en cours
     */
    public function get_division_from_dossier_without_inst($dossier = null) {

        // Cette méthode peut être appelée plusieurs fois lors d'une requête.
        // Pour éviter de refaire le traitement de recherche de la division
        // alors on vérifie si nous ne l'avons pas déjà calculé.
        if (isset($this->_division_from_dossier) === true
            && $this->_division_from_dossier !== null) {
            // Log
            $this->addToLog(__METHOD__."() : retour de la valeur déjà calculée - '".$this->_division_from_dossier."'", EXTRA_VERBOSE_MODE);
            // On retourne la valeur déjà calculée
            return $this->_division_from_dossier;
        }

        // Récupère le paramétre retourformulaire présent dans l'URL
        $retourformulaire = $this->getParameter("retourformulaire");
        // Récupère le paramétre idxformulaire présent dans l'URL
        $idxformulaire = $this->getParameter("idxformulaire");

        // Si le dossier n'est pas passé en paramètre de la méthode
        if ($dossier === null) {

            // La méthode de récupération du dossier diffère selon le contexte
            // du formulaire
            if ($retourformulaire === "dossier"
                || $retourformulaire === "dossier_instruction"
                || $retourformulaire === "dossier_instruction_mes_encours"
                || $retourformulaire === "dossier_instruction_tous_encours"
                || $retourformulaire === "dossier_instruction_mes_clotures"
                || $retourformulaire === "dossier_instruction_tous_clotures"
                || $retourformulaire === "dossier_contentieux_mes_infractions"
                || $retourformulaire === "dossier_contentieux_toutes_infractions"
                || $retourformulaire === "dossier_contentieux_mes_recours"
                || $retourformulaire === "dossier_contentieux_tous_recours") {

                // Récupère le numéro du dossier depuis le paramètre
                // idxformulaire présent dans l'URL
                $dossier = $idxformulaire;
            }
            //
            if ($retourformulaire === "lot") {

                // Requête SQL
                $sql = sprintf("SELECT dossier FROM ".DB_PREFIXE."lot WHERE lot = %s", $idxformulaire);
                // Récupère l'identifiant du dossier
                $dossier = $this->db->getone($sql);
                // Log
                $this->addToLog(__METHOD__."() : db->query(\"".$dossier."\");", VERBOSE_MODE);
                // Erreur BDD
                $this->isDatabaseError($dossier);
            }
        }

        // À cette étape si le dossier n'est toujours pas récupéré alors la
        // division ne pourra pas être récupérée
        if ($dossier === null) {
            //
            return null;
        }

        // Requête SQL
        $sql = sprintf("SELECT division FROM ".DB_PREFIXE."dossier WHERE dossier = '%s'", $dossier);
        // Récupère l'identifiant de la division du dossier
        $this->_division_from_dossier = $this->db->getOne($sql);
        // Log
        $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
        // Erreur BDD
        $this->isDatabaseError($this->_division_from_dossier);

        //
        return $this->_division_from_dossier;

    }


}

?>
