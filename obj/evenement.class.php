<?php
/**
 * DBFORM - 'evenement' - Surcharge gen.
 *
 * Ce script permet de définir la classe 'evenement'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../gen/obj/evenement.class.php";

class evenement extends evenement_gen {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    public function init_class_actions() {

        parent::init_class_actions();

        // ACTION - 000 - ajouter
        // Modifie la condition d'affichage du bouton ajouter
        $this->class_actions[0]["condition"] = "is_addable_editable_and_deletable";

        // ACTION - 001 - modifier
        // Modifie la condition et le libellé du bouton modifier
        $this->class_actions[1]["condition"] = "is_addable_editable_and_deletable";

        // ACTION - 002 - supprimer
        // Modifie la condition et le libellé du bouton supprimer
        $this->class_actions[2]["condition"] = "is_addable_editable_and_deletable";
    }


    /**
     * CONDITION - is_addable_editable_and_deletable.
     *
     * Condition pour l'affichage de l'action d'ajout, de modification et de
     * suppression.
     *
     * @return boolean
     */
    public function is_addable_editable_and_deletable() {

        // Si evenement est ouvert en sous-formulaire
        if ($this->getParameter("retourformulaire") !== null) {

            //
            return false;
        }

        //
        return true;
    }


    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        // MODE AJOUTER et MODE MODIFIER
        if ($maj == 0 || $maj == 1) {
            //
            $form->setType('accord_tacite', 'select');
            $form->setType('delai_notification', 'select');
            $form->setType('delai', 'select');
            $form->setType('lettretype', 'select');
            $form->setType('consultation', 'checkbox');
            $form->setType('dossier_instruction_type','select_multiple');
            $form->setType('type','select');
            $form->setType('etats_depuis_lequel_l_evenement_est_disponible','select_multiple');
        }
        // MODE SUPPRIMER et MODE CONSULTER
        if ($maj == 2 || $maj == 3) {
            //
            $form->setType('dossier_instruction_type','select_multiple_static');
            $form->setType('etats_depuis_lequel_l_evenement_est_disponible','select_multiple_static');
        }

        // Mode modifier
        if ($maj == 1) {
            // Champ non modifiable pour éviter un déréglement du paramètrage
            // des événements
            $form->setType('retour', 'checkboxhiddenstatic');
        }

        // Cache les champs en fonction de la valeur de 'retour'
        if ($this->getVal("retour") == 't') {

            // Cache les champs "evenement_retour_ar" et 
            // "evenement_retour_signature"
            $form->setType('evenement_retour_ar', 'hidden');
            $form->setType('evenement_retour_signature', 'hidden');

            // En mode Ajouter et Modifier
            if ($maj < 2) {
                $form->setType('restriction', 'hiddenstatic');
                $form->setType('delai', 'hiddenstatic');
                $form->setType('accord_tacite', 'hiddenstatic');
                $form->setType('delai_notification', 'hiddenstatic');
                $form->setType('avis_decision', 'hiddenstatic');
            }
        }
    }

    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        //
        parent::setSelect($form, $maj, $db, $debug);
        //
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        }
        //
        if($maj<2){
            // lettretype
            
            $contenu=array();
            $res = $db->query($sql_om_lettretype);
            if (database::isError($res))
                die($res->getMessage().$sql_om_lettretype);
            else{
                if ($debug == 1)
                    echo " la requete ".$sql_om_lettretype." est executee<br>";
                $contenu[0][0]='';
                $contenu[1][0]=_('choisir')."&nbsp;"._('lettretype');
                $k=1;
                while ($row=& $res->fetchRow()){
                    $contenu[0][$k]=$row[0];
                    $contenu[1][$k]=$row[1];
                    $k++;
                }
                $form->setSelect('lettretype',$contenu);
            }// fin error db
        }
        // accord_tacite
        $contenu=array();
        $contenu[0]=array('Non','Oui');
        $contenu[1]=array(_('Non'), _('Oui'));
        $form->setSelect("accord_tacite",$contenu);
        // delai_notification
        $contenu=array();
        $contenu[0]=array('0','1');
        $contenu[1]=array('sans','1 '._("mois"));
        $form->setSelect("delai_notification",$contenu);
        // delai
        $contenu=array();
        $contenu[0]=array('0','1','2','3','4','5','6','7','8','9','10','11','12','18','24');
        $contenu[1]=array('sans',
                          '1 '._("mois"),
                          '2 '._("mois"),
                          '3 '._("mois"),
                          '4 '._("mois"),
                          '5 '._("mois"),
                          '6 '._("mois"),
                          '7 '._("mois"),
                          '8 '._("mois"),
                          '9 '._("mois"),
                          '10 '._("mois"),
                          '11 '._("mois"),
                          '12 '._("mois"),
                          '18 '._("mois"),
                          '24 '._("mois")
                          );
        $form->setSelect("delai",$contenu);

        // type de l'événement
        $contenu=array();
        $contenu[0]=array(
            '',
            'arrete',
            'incompletude',
            'majoration_delai',
            'retour',
            'changement_decision',
            'affichage',
            'ait',
            'annul_contradictoire'
            );
        $contenu[1]=array(
            _('choisir type'),
            _('arrete'),
            _('incompletude'),
            _('majoration_delai'),
            _('retour de pieces'),
            _('changement de decision'),
            _('affichage'),
            _('arrêté interruptif des travaux'),
            _('annulation de contradictoire'),
            );
        $form->setSelect("type",$contenu);

        // dossier_instruction_type
        $this->init_select($form, $db, $maj, $debug, "dossier_instruction_type",
                           $sql_dossier_instruction_type, $sql_dossier_instruction_type_by_id, false, true);
        // dossier_instruction_type
        $this->init_select($form, $db, $maj, $debug, "etats_depuis_lequel_l_evenement_est_disponible",
                           $sql_etats_depuis_lequel_l_evenement_est_disponible, $sql_etats_depuis_lequel_l_evenement_est_disponible_by_id, false, true);

        // evenement_retour_ar filtre seulement les evenements dont le booléen 
        // retour est à true
        $this->init_select($form, $db, $maj, $debug, "evenement_retour_ar",
                           $sql_evenement_retour_ar, $sql_evenement_retour_ar_by_id, false);

        // evenement_retour_signature filtre seulement les evenements dont le 
        // booléen retour est à true
        $this->init_select($form, $db, $maj, $debug, "evenement_retour_signature",
                           $sql_evenement_retour_signature, $sql_evenement_retour_signature_by_id, false);

        // evenement_suivant_tacite filtre seulement les evenements dont le 
        // booléen retour est à false
        $this->init_select($form, $db, $maj, $debug, "evenement_suivant_tacite",
                           $sql_evenement_suivant_tacite, $sql_evenement_suivant_tacite_by_id, false);

    }

    function setTaille(&$form, $maj) {
        //
        parent::setTaille($form, $maj);
        //
        $form->setTaille("dossier_instruction_type", 10);
        $form->setTaille("etats_depuis_lequel_l_evenement_est_disponible", 10);
    }

    function setMax(&$form, $maj) {
        //
        parent::setMax($form, $maj);
        //
        $form->setMax("dossier_instruction_type", 5);
        $form->setMax("etats_depuis_lequel_l_evenement_est_disponible", 5);
    }

    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $maj);
        //
        $form->setLib("dossier_instruction_type", _("type(s) de DI concerne(s)"));
        $form->setLib("etats_depuis_lequel_l_evenement_est_disponible", _("etat(s) source(s)"));
        // Change le libellé de retour pour pas qu'il soit confondus avec le
        // bouton
        $form->setLib("retour", _("retour_evenement"));
        // En ajout et en modification 
        if ($maj < 2) {
            $form->setLib("retour", _("retour_evenement (parametrage non modifiable)"));
        }

        // Message d'aide à l'utilisateur concernant les événements liés
        $message_help = _("Les champs suivants seront copies vers l'evenement choisi :");
        $champs_copy = _('delai') . ", " . _('accord_tacite') . ", " . _('delai_notification') . ", " . _('avis_decision') . ", " . _('restriction');
        $form->setLib("evenement_retour_ar", _('evenement_retour_ar') . "<br> (" . $message_help . " " . $champs_copy . ")");
        $form->setLib("evenement_retour_signature", _('evenement_retour_signature') . "<br> (" . $message_help . " " . $champs_copy . ")");
    }

    function setLayout(&$form, $maj) {
        //
        parent::setLayout($form, $maj);
        //
        $form->setFieldset("evenement", "D", _("Evenement"));
        $form->setFieldset("retour", "F");
        //
        $form->setFieldset("etats_depuis_lequel_l_evenement_est_disponible", "D", _("Filtre de selection"));
            $form->setBloc("etats_depuis_lequel_l_evenement_est_disponible", "D", _("Filtres pour la possibilite de selection a l'ajout d'un evenement d'instruction"));
            $form->setBloc("dossier_instruction_type", "F");
            $form->setBloc("restriction", "DF", _("Filtre supplementaire a l'enregistrement de l'evenement d'instruction"));
        $form->setFieldset("restriction", "F");
        //
        $form->setFieldset("action", "D", _("Action"));
            $form->setBloc("action", "DF");
            $form->setBloc("etat", "D", _("Parametres de l'action"));
            $form->setBloc("autorite_competente", "F");
        $form->setFieldset("autorite_competente", "F");
        //
        $form->setFieldset("lettretype", "D", _("Edition"));
        $form->setFieldset("phase", "F");
        //
        $form->setFieldset("evenement_suivant_tacite", "D", _("Evenements lies"), "evenements_lies");
        $form->setFieldset("evenement_retour_signature", "F");
    }

    //Ajoute autant de lien_dossier_instruction_type_evenement que de dossier_instruction_type
    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        parent::triggerajouterapres($id,$db,$val,$DEBUG);

        /**
         * LIEN ETAT
         */
        // Récupération des données du select multiple
        $etats_depuis_lequel_l_evenement_est_disponible = $this->getEvenementLinks('etats_depuis_lequel_l_evenement_est_disponible', 
            'transition', 'etat');
        // Ne traite les données que s'il y en a et qu'elles sont correctes
        if (is_array($etats_depuis_lequel_l_evenement_est_disponible)
            && count($etats_depuis_lequel_l_evenement_est_disponible) > 0 ){
            // Initialisation
            $nb_liens_etat = 0;
            // Boucle sur la liste des états sélectionnés
            foreach ($etats_depuis_lequel_l_evenement_est_disponible as $value) {
                // Test si la valeur par défaut est sélectionnée
                if ($value != "") {
                    // 
                    $data = array(
                        'evenement' => $this->valF['evenement'],
                        'etat' => $value
                    );
                    // On ajoute l'enregistrement
                    $this->addEvenementLinks($data, 'transition');
                    // On compte le nombre d'éléments ajoutés
                    $nb_liens_etat++;
                }
            }
            // Message de confirmation
            if ($nb_liens_etat > 0) {
                if ($nb_liens_etat == 1 ){
                    $this->addToMessage(_("Creation de ").$nb_liens_etat._(" nouvelle liaison realisee avec succes."));
                } else{
                    $this->addToMessage(_("Creation de ").$nb_liens_etat._(" nouvelles liaisons realisees avec succes."));
                }
            }
        }

        /**
         * LIEN DI TYPE
         */
        //Récupère les données du select multiple
        $dossier_instruction_type = $this->getEvenementLinks('dossier_instruction_type', 
            'lien_dossier_instruction_type_evenement', 'dossier_instruction_type');
        //Ne traite les données que s'il y en a et qu'elles sont correctes
        if ( is_array($dossier_instruction_type) && count($dossier_instruction_type) > 0 ){
            
            $nb_tr = 0;    
            /* Va créer autant de lien_dossier_instruction_type_evenement 
             * que de dossier_instruction_type choisis */
            foreach( $dossier_instruction_type as $value ){
            
                //Test si la valeur par défaut est sélectionnée
                if ( $value != "" ) {
                    
                    //Données
                    $data = array(
                        'evenement' => $this->valF['evenement'],
                        'dossier_instruction_type' => $value
                    );
                    
                    //Ajoute un nouveau lien_dossier_instruction_type_evenement
                    $this->addEvenementLinks($data, 'lien_dossier_instruction_type_evenement');

                    $nb_tr++;
                }
            }
            
            //Message de confirmation de création de(s) lien_dossier_instruction_type_evenement(s).
            if ( $nb_tr > 0 ){
                
                if ( $nb_tr == 1 ){
                    
                    $this->addToMessage(_("Creation de ").$nb_tr._(" nouvelle liaison 
                        realisee avec succes."));
                }
                else{
                    
                    $this->addToMessage(_("Creation de ").$nb_tr._(" nouvelles liaisions 
                        realisee avec succes."));
                }
            }
        }

        // Copie les paramètres vers l'événement lié
        $this->copyParametersToEvenementLink($db, $DEBUG);
        
    }
        
    /**
     * Récupère les liens de la variable POST ou de la base de données selon le
     * contexte
     * 
     * @param $champ Le champ POST à récupérer
     * @return mixed Les liens
     */
    function getEvenementLinks($champ, $table, $champLie){
            
        $liens = array();

        // Si on est dans le contexte d'un formulaire
        if ( isset($this->form) && !is_null($this)){
            // On récupère les données post
            if ($this->f->get_submitted_post_value($champ) !== null) {
            
                return $this->f->get_submitted_post_value($champ);
            }
        }
        //Si on n'est pas dans le contexte d'un formulaire
        else {

            //Requête
            $sql = "SELECT ".$champLie." 
                FROM ".DB_PREFIXE.$table." 
                WHERE evenement = ".$this->valF['evenement'];
            $res = $this->db->query($sql);
            $this->addToLog("getLiensEvenement(): db->query(\"".$sql."\");", VERBOSE_MODE);
            if (database::isError($res, true)) {
                // Appel de la methode de recuperation des erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), 'evenement');
            }
            
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                
                $liens[] = $row[$champLie];
            }
        }
        
        return $liens;
    }
    
    //Modification des liens
    function triggermodifierapres($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        parent::triggermodifierapres($id, $db, $val, $DEBUG);

        /**
         * LIEN ETAT
         */
        // On récupère les liens selon le contexte : POST ou base de données
        $etats_depuis_lequel_l_evenement_est_disponible = $this->getEvenementLinks('etats_depuis_lequel_l_evenement_est_disponible', 
            'transition', 'etat');

        // Suppression de tous les liens de la table transition (table lien
        // entre etat et evenement)
        $this->deleteEvenementLinks($this->valF['evenement'], 'transition');
        // Ne traite les données que s'il y en a et qu'elles sont correctes
        if (is_array($etats_depuis_lequel_l_evenement_est_disponible)
            && count($etats_depuis_lequel_l_evenement_est_disponible) > 0 ){
            // Initialisation
            $nb_liens_etat = 0;
            // Boucle sur la liste des états sélectionnés
            foreach ($etats_depuis_lequel_l_evenement_est_disponible as $value) {
                // Test si la valeur par défaut est sélectionnée
                if ($value != "") {
                    // 
                    $data = array(
                        'evenement' => $this->valF['evenement'],
                        'etat' => $value
                    );
                    // On ajoute l'enregistrement
                    $this->addEvenementLinks($data, 'transition');
                    // On compte le nombre d'éléments ajoutés
                    $nb_liens_etat++;
                }
            }
            // Message de confirmation
            if ($nb_liens_etat > 0) {
                $this->addToMessage(_("Mise a jour des liaisons realisee avec succes."));
            }
        }

        /**
         * LIEN DI TYPE
         */
        // On récupère les liens selon le contexte : POST ou base de données
        $dossier_instruction_type = $this->getEvenementLinks('dossier_instruction_type', 
            'lien_dossier_instruction_type_evenement', 'dossier_instruction_type');
        //Supprime toutes les liaisions liées à l'événement
        $this->deleteEvenementLinks($this->valF['evenement'], 'lien_dossier_instruction_type_evenement');
        
        //Ne traite les données que s'il y en a et qu'elles sont correctes
        if ( is_array($dossier_instruction_type) && count($dossier_instruction_type) > 0 ){
            
            $nb_tr = 0;    
            //Va créer autant de lien_dossier_instruction_type_evenement que de dossier_instruction_type choisis
            foreach( $dossier_instruction_type as $value ){
            
                //Test si la valeur par défaut est sélectionnée
                if ( $value != "" ) {
                    
                    //Données
                    $data = array(
                        'evenement' => $this->valF['evenement'],
                        'dossier_instruction_type' => $value
                    );
                    
                    //Ajoute un nouveau lien_dossier_instruction_type_evenement
                    $this->addEvenementLinks($data, 'lien_dossier_instruction_type_evenement');

                    $nb_tr++;
                }
            }
            
            //Message de confirmation de création de(s) lien_dossier_instruction_type_evenement.
            if ( $nb_tr > 0 ){
                
                $this->addToMessage(_("Mise a jour des liaisons realisee avec succes."));
            }
        }

        // Copie les paramètres vers l'événement lié
        $this->copyParametersToEvenementLink($db, $DEBUG);
    }

    /**
     * Ajout d'un lien dans la table passée en paramètre
     * @param $data Les données à ajouter
     * @param $table La table à populer
     */
    function addEvenementLinks($data, $table){
        
        //Fichier requis
        require_once '../obj/'.$table.'.class.php';
        
        $linksEvenement = new $table("]", $this->db, false);
        
        $linksEvenement->valF = "";
        $val[$table] = NULL;
        //
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $val[$key]=$value;
            }
        }
        //
        $linksEvenement->ajouter($val, $this->db, false);
    }

    /**
     * Suppression des liens de la table passé en paramètre
     * @param $id L'identifiant de l'événement
     * @param $table La table à vider
     */
    function deleteEvenementLinks($id, $table){
        
        // Suppression de tous les enregistrements correspondants à l'id de
        // l'événement
        $sql = "DELETE
            FROM ".DB_PREFIXE.$table."
            WHERE evenement = ".$id;
        $res = $this->db->query($sql);
        $this->addToLog("deleteLiensEvenement(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), 'evenement');
        } 
    }

    function triggersupprimer($id, &$db = null, $val = array(), $DEBUG = null) {
        // Suppression de tous les liens de la table transition (table lien
        // entre etat et evenement)
        $this->deleteEvenementLinks($this->getVal('evenement'), 'transition');
        //Supprime toutes les lien_dossier_instruction_type_evenement liées à l'evenement
        $this->deleteEvenementLinks($this->getVal('evenement'), 'lien_dossier_instruction_type_evenement');
    }
    
    /* Surcharge de la fonction cleSecondaire pour qu'elle ne vérifie pas le lien avec 
     * lien_dossier_instruction_type_evenement qui sera supprimé juste après ni avec la table transition*/ 
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {

        // Verification de la cle secondaire : bible
        $this->rechercheTable($db, "bible", "evenement", $id);
        // Verification de la cle secondaire : demande_type
        $this->rechercheTable($db, "demande_type", "evenement", $id);
        // Verification de la cle secondaire : evenement
        $this->rechercheTable($db, "evenement", "evenement_retour_ar", $id);
        // Verification de la cle secondaire : evenement
        $this->rechercheTable($db, "evenement", "evenement_suivant_tacite", $id);
        // Verification de la cle secondaire : instruction
        $this->rechercheTable($db, "instruction", "evenement", $id);
        //// Verification de la cle secondaire : transition
        //$this->rechercheTable($db, "transition", "evenement", $id);
    }
    
    //Affichage des dossier_instruction_type anciennement liés
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        
        parent::setVal($form, $maj, $validation, $db, $DEBUG);
        
        if($maj == 2 && $validation == 1 ) {
             $form->setVal("dossier_instruction_type",$this->val[count($this->val)-1]);
        }
    }

    function verifier($val = array(), &$db = null, $DEBUG = null) {
        parent::verifier($val, $db, $DEBUG);
    
        //Test qu'une restriction est présente
        if (isset($this->valF['restriction']) && $this->valF['restriction'] != ""){

            $restriction = $this->valF['restriction'];

            // Liste des opérateurs possible
            $operateurs = array(">=", "<=", "+", "-", "&&", "||", "==", "!=");

            // Supprime tous les espaces de la chaîne de caractère
            $restriction = str_replace(' ', '', $restriction);
            
            // Met des espace avant et après les opérateurs puis transforme la 
            // chaine en un tableau 
            $tabRestriction = str_replace($operateurs, " ", $restriction);
            // Tableau des champ
            $tabRestriction = explode(" ", $tabRestriction);
            // Supprime les numériques du tableau
            foreach ($tabRestriction as $key => $value) {
                if (is_numeric($value)) {
                    unset($tabRestriction[$key]);
                }
            }

            // Vérifie les champs utilisés pour la restriction
            $check_field_exist = $this->f->check_field_exist($tabRestriction, 
                'instruction');
            if ($check_field_exist !== true) {

                // Liste des champs en erreur
                $string_error_fields = implode(", ", $check_field_exist);

                // Message d'erreur
                $error_message = _("Le champ %s n'est pas utilisable pour le champ %s");
                if (count($check_field_exist) > 1) {
                    $error_message = _("Les champs %s ne sont pas utilisable pour le champ %s");
                }

                // Affiche l'erreur
                $this->correct=false;
                $this->addToMessage(sprintf($error_message, $string_error_fields, _("restriction")));
            }
        }

        // Identifiant de l'évenement en cours
        $evenement_main = "";
        // Si pas en mode "Ajouter"
        if ($this->getParameter("maj") != 0) {
            $evenement_main = $this->valF['evenement'];
        }

        //
        $error_retour = false;

        // Si le même événement retour est sélectionné pour le retour ar et le
        // retour signature
        if (isset($this->valF['evenement_retour_ar']) 
            && $this->valF['evenement_retour_ar'] != ""
            && isset($this->valF['evenement_retour_signature']) 
            && $this->valF['evenement_retour_signature'] != "") {

            //
            if ($this->valF['evenement_retour_ar'] == $this->valF['evenement_retour_signature']) {

                // Récupère l'événement
                $evenement_retour = $this->valF['evenement_retour_ar'];

                // Récupère le libelle de l'événement
                $evenement_retour_libelle = $this->getEvenementLibelle($evenement_retour);

                // Message d'erreur
                $error_message = _("L'evenement \"%s\" ne peut pas etre utilise en tant qu'evenement d'accuse de reception et evenement de retour de signature.");

                // Le formulaire n'est pas validé
                $this->correct=false;
                $this->addToMessage(sprintf($error_message, $evenement_retour_libelle));
                $error_retour = true;
            }
        }

        // Si l'erreur concernant la double utilisation d'une événement retour 
        // sur le même formulaire n'est pas activé
        if ($error_retour === false) {

            // Vérifie que l'événement "evenement_retour_signature" n'est pas
            // utilisé en evenement retour 
            $this->checkEvenementRetour('evenement_retour_signature', $evenement_main);
            // Vérifie que l'événement n'est pas déjà utilisé en tant que 
            // "evenement_retour_ar"
            $this->checkEvenementRetour('evenement_retour_ar', $evenement_main);
        }

        // Si c'est un événement retour
        if (isset($this->valF['retour']) 
            && $this->valF['retour'] === true) {

            // Supprime les valeurs des champs
            unset($this->valF['evenement_retour_ar']);
            unset($this->valF['evenement_retour_signature']);
        }
    }

    /**
     * Vérifie que l'événement $champ n'est pas déjà utilisé en événement 
     * 'evenement_retour_ar' et 'evenement_retour_signature'
     * @param  string   $champ          Champ à tester
     * @param  integer  $evenement_main Identifiant de l'événement en cours
     */
    function checkEvenementRetour($champ, $evenement_main) {

        // Si un l'évenement est renseigné
        if (isset($this->valF[$champ]) 
            && $this->valF[$champ] != "") {

            // Récupère l'événement
            $evenement_retour = $this->valF[$champ];

            // Récupère le libelle de l'événement
            $evenement_libelle = $this->getEvenementLibelle($evenement_retour);

            // Si l'événement est utilisé en tant que "evenement_retour_ar"
            if ($this->checkEvenementIsUse($evenement_retour, 'evenement_retour_ar', $evenement_main)) {

                // Message d'erreur
                $error_message = _("L'evenement \"%s\" est deja utilise en tant qu'evenement d'accuse de reception.");

                // Le formulaire n'est pas validé
                $this->correct=false;
                $this->addToMessage(sprintf($error_message, $evenement_libelle));
            }

            // Si l'événement est utilisé en tant que 
            // "evenement_retour_signature"
            if ($this->checkEvenementIsUse($evenement_retour, 'evenement_retour_signature', $evenement_main)) {

                // Message d'erreur
                $error_message = _("L'evenement \"%s\" est deja utilise en tant qu'evenement de retour de signature.");

                // Le formulaire n'est pas validé
                $this->correct=false;
                $this->addToMessage(sprintf($error_message, $evenement_libelle));
            }
        }

    }

    /**
     * Vérifie si l'événement est déjà utilisé dans un autre champ
     * @param  integer $evenement Identifiant de l'événement
     * @return boolean            
     */
    function checkEvenementIsUse($evenement_link, $champ, $evenement_main) {

        // Initialisation du retour de la fonction
        $return = false;

        // Si les paramètres ne sont pas vide
        if ($evenement_link != "" && $champ != "") {

            // Requête SQL
            $sql = "SELECT evenement
                    FROM ".DB_PREFIXE."evenement
                    WHERE $champ = $evenement_link";

            // Si l'événement principal est déjà crée
            if ($evenement_main != "") {
                // Il ne faut pas que l'événement principal soit pris en compte
                $sql .= " AND evenement != $evenement_main";
            }

            $this->f->addToLog("checkEvenementIsUse() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->db->query($sql);
            $this->f->isDatabaseError($res);
            
            // Si il y a un résultat à la requête
            if ($res->numrows() > 0) {

                // Change la valeur de retour
                $return = true;
            }
        }

        // Retourne le résultat de la fonction
        return $return;

    }

    /**
     * Récupère le libellé de l'evénement passé en paramètre
     * @param  integer $evenement Identifiant de l'événement
     * @return string             Libellé de l'événement
     */
    function getEvenementLibelle($evenement) {

        // Initialisation du résultat
        $libelle = '';

        // Si la condition n'est pas vide
        if ($evenement != "") {

            // Requête SQL
            $sql = "SELECT libelle
                    FROM ".DB_PREFIXE."evenement
                    WHERE evenement = $evenement";
            $this->f->addToLog("getEvenementLibelle() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $libelle = $this->db->getOne($sql);
            $this->f->isDatabaseError($libelle);
        }

        // Retourne résultat
        return $libelle;
    }

    /**
     * Copie les paramétres de l'événement principal vers l'évévenement lié
     * @param  object   $db     [description]
     * @param  mixed    $DEBUG  [description]
     */
    function copyParametersToEvenementLink($db, $DEBUG) {

        // Si un évenement retour de signature est renseigné
        if (isset($this->valF['evenement_retour_signature']) 
            && $this->valF['evenement_retour_signature'] != "") {

            // Instanciation de la classe evenement
            $evenement_retour_signature = new evenement($this->valF['evenement_retour_signature'], $db, $DEBUG);
            $evenement_retour_signature->setParameter("maj",1);

            // Valeurs de l'enregistrement
            $value_evenement_retour_signature = array();
            foreach($evenement_retour_signature->champs as $key => $champ) {
                //
                $value_evenement_retour_signature[$champ] = $evenement_retour_signature->val[$key];
            }

            // Valeurs à modifier
            $value_evenement_retour_signature['delai'] = $this->valF['delai'];
            $value_evenement_retour_signature['accord_tacite'] = $this->valF['accord_tacite'];
            $value_evenement_retour_signature['delai_notification'] = $this->valF['delai_notification'];
            $value_evenement_retour_signature['avis_decision'] = $this->valF['avis_decision'];
            $value_evenement_retour_signature['restriction'] = $this->valF['restriction'];

            // Récupère le libelle de l'événement
            $evenement_retour_signature_libelle = $this->getEvenementLibelle($value_evenement_retour_signature['evenement']);

            // Message de validation
            $valid_message = _("Mise a jour de l'evenement lie \"%s\" realisee avec succes.");

            // Modifie l'événement lié pour qu'il ait les mêmes paramètres
            // que l'événement principal
            if ($evenement_retour_signature->modifier($value_evenement_retour_signature, $db, $DEBUG)) {
                
                //
                $this->addToMessage(sprintf($valid_message, $evenement_retour_signature_libelle));
            }

        }

        // Si un évenement retour d'accusé de réception est renseigné
        if (isset($this->valF['evenement_retour_ar']) 
            && $this->valF['evenement_retour_ar'] != "") {

            // Instanciation de la classe evenement
            $evenement_retour_ar = new evenement($this->valF['evenement_retour_ar'], $db, $DEBUG);
            $evenement_retour_ar->setParameter("maj",1);

            // Valeurs de l'enregistrment
            $value_evenement_retour_ar = array();
            foreach($evenement_retour_ar->champs as $key => $champ) {
                //
                $value_evenement_retour_ar[$champ] = $evenement_retour_ar->val[$key];
            }

            // Valeurs à modifier
            $value_evenement_retour_ar['delai'] = $this->valF['delai'];
            $value_evenement_retour_ar['accord_tacite'] = $this->valF['accord_tacite'];
            $value_evenement_retour_ar['delai_notification'] = $this->valF['delai_notification'];
            $value_evenement_retour_ar['avis_decision'] = $this->valF['avis_decision'];
            $value_evenement_retour_ar['restriction'] = $this->valF['restriction'];

            // Récupère le libelle de l'événement
            $evenement_retour_ar_libelle = $this->getEvenementLibelle($value_evenement_retour_ar['evenement']);

            // Message de validation
            $valid_message = _("Mise a jour de l'evenement lie \"%s\" realisee avec succes.");
            // Modifie l'événement lié pour qu'il ait les mêmes paramètres
            // que l'événement principal
            if ($evenement_retour_ar->modifier($value_evenement_retour_ar, $db, $DEBUG)) {

                //
                $this->addToMessage(sprintf($valid_message, $evenement_retour_ar_libelle));
            }

        }
    }

    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);

        //
        $form->setOnchange('retour','retourOnchangeEvenement(this)');
    }

    /**
     * Fonction appelée lors de la copie d'un enregistrement
     * @param  array    $valCopy    Liste des valeurs de l'enregistrement
     * @param  string   $objsf      Liste des objets associés
     * @param  mixed    $DEBUG      Type du DEBUG
     * @return array                Liste des valeurs après traitement
     */
    function update_for_copy($valCopy, $objsf, $DEBUG) {

        // Libellé du duplicata
        $libelle = _("Copie de %s du %s");
        $valCopy['libelle'] = sprintf($libelle, $valCopy['libelle'], date('d/m/Y H:i:s'));
        // Tronque le libellé si celui est trop long
        $valCopy['libelle'] = mb_substr($valCopy['libelle'], 0, 70, "UTF8");

        // Message à retourner
        $valCopy['message'] = "";

        // S'il y a un événement retour_ar sur l'événement copié
        if ($valCopy['evenement_retour_ar'] != '') {
            // Copie l'événement retour_ar
            $copie = $this->f->copier($valCopy['evenement_retour_ar'], 'evenement', $objsf);
            $evenement_retour_ar = $copie['evenement_'.$valCopy['evenement_retour_ar']];
            $valCopy['message'] .= $copie['message'];
            $valCopy['message_type'] = $copie['message_type'];
            $valCopy['evenement_retour_ar'] = $evenement_retour_ar;
        }

        // S'il y a un événement evenement_retour_signature sur l'événement copié
        if ($valCopy['evenement_retour_signature'] != '') {
            // Copie l'événement retour_signature
            $copie = $this->f->copier($valCopy['evenement_retour_signature'], 'evenement', $objsf);
            $evenement_retour_signature = $copie['evenement_'.$valCopy['evenement_retour_signature']];
            $valCopy['message'] .= $copie['message'];
            $valCopy['message_type'] = $copie['message_type'];
            $valCopy['evenement_retour_signature'] = $evenement_retour_signature;
        }

        // Retourne les valeurs
        return $valCopy;
    }

}

?>
