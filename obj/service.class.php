<?php
/**
 * DBFORM - 'service' - Surcharge gen.
 *
 * Ce script permet de définir la classe 'service'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../gen/obj/service.class.php";

class service extends service_gen {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    function setType(&$form,$maj) {
        parent::setType($form,$maj);
        if ($maj < 2) {
            $form->setType('type_consultation', 'select');
            $form->setType('edition', 'select');
            $form->setType('delai_type', 'select');
        }
    }

    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        parent::setSelect($form, $maj, $db, $debug) ;

        $contenu=array();

        $contenu[0]=array(
          'avec_avis_attendu',
          'pour_conformite',
          'pour_information',
          );

        $contenu[1]=array(
          _('Avec avis attendu'),
          _('Pour conformite'),
          _('Pour information'),
          
          );
        $form->setSelect("type_consultation",$contenu);

        //
        $contenu = array();
        $contenu[0] = array('mois','jour');
        $contenu[1] = array(_('mois'), _('jour'));
        $form->setSelect("delai_type", $contenu);
    }
    
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        parent::setVal($form, $maj, $validation, $db, $DEBUG);

        if ($maj > 1){
            //Traitement des données pour l'affichage du select
            $temp = $this->val[array_search('type_consultation', array_keys($form->val))];
            
            if ( strcmp($temp, 'pour_information') == 0 ){
            
                $temp = _('Pour information');
            
            }elseif( strcmp($temp, 'avec_avis_attendu') == 0 ) {
                
                $temp = _('Avec avis attendu');
                
            }elseif( strcmp($temp, 'pour_conformite') == 0 ) {
                
                $temp = _('Pour conformite');
                
            }
            
            $form->setVal('type_consultation', $temp);
        }
    }
    
    function setLib(&$form,$maj) {
        //
        parent::setLib($form, $maj);
        $form->setLib("edition",_("type d'edition de la consultation"));
    }

}

?>
