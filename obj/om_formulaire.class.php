<?php
/**
 * Ce fichier est destine a permettre la surcharge de certaines methodes de
 * la classe om_formulaire pour des besoins specifiques de l'application
 *
 * @package openmairie_exemple
 * @version SVN : $Id: om_formulaire.class.php 6161 2016-03-14 14:05:13Z nhaye $
 */

/**
 *
 */
require_once PATH_OPENMAIRIE."om_formulaire.class.php";

/**
 *
 */
class om_formulaire extends formulaire {
   
    /**
     * La valeur du champ est passe par le controle hidden
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referencescadastralesstatic($champ, $validation, $DEBUG = false) {

        //
        foreach (explode(';', $this->val[$champ]) as $key => $ref) {
            echo "<span class=\"reference-cadastrale-".$key."\">";
            echo $ref;
            echo "</span> ";
        }
    }
    
    function tableau($champ,$validation,$DEBUG = false) {
        // Ouverture du tableau
        echo "<table class='om-form-tab-table'>";

            // Affichage des entêtes de colonnes
            echo "<tr class='om-form-tab-tr'>";
            foreach ($this->select[$champ]["column_header"] as $value) {
                echo "<th";
                // Si l'entête de colonne n'est pas vide
                if($value != "") {
                    // Ajoute la classe à l'élément
                    echo " class='om-form-tab-th'";
                }
                echo ">";
                //echo "<th class='om-form-tab-th'>";
                echo $value;
                echo "</th>";
            }
            echo "</tr>";
            
            // Initialisation des variables
            $k=0;
            $keys = array_keys($this->select[$champ]["values"]);
            $values = array_values($this->select[$champ]["values"]);

            // Affichage de chaque ligne
            for ($i=0; $i<count($this->select[$champ]["row_header"]); $i++) {
                echo "<tr class='om-form-tab-tr'>";

                // Affichage de l'entête de ligne
                echo "<th";
                // Si l'entête de ligne n'est pas vide
                if($this->select[$champ]["row_header"][$i] != "") {
                    // Ajoute la classe à l'élément
                    echo " class='om-form-tab-th'";
                }
                echo ">";
                echo $this->select[$champ]["row_header"][$i];
                echo "</th>";

                // Affichage des champs
                for ($j=0; $j<count($this->select[$champ]["column_header"])-1; $j++) {
                    echo "<td class='om-form-tab-td'>";

                    // Appel de la méthode définie dans setType
                    if (method_exists($this, $this->type[$keys[$i+$j*count($this->select[$champ]["row_header"])]])) {
                        $function=$this->type[$keys[$i+$j*count($this->select[$champ]["row_header"])]];

                        $this->$function($keys[$i+$j*count($this->select[$champ]["row_header"])], $validation);
                    } else {
                        $this->statiq($keys[$i+$j*count($this->select[$champ]["row_header"])], $validation);
                    }
                    echo "</td>";
                    $k++;
                }
                echo "</tr>";
            }
        echo "</table>";
    }

    /**
     * Au clique affiche un pop-up contenant le formulaire en ajout de l'objet $champ
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function manage_with_popup($champ,$validation,$DEBUG = false){
        
        //Si on a pas de valeur on affiche un bouton d'ajout
        if ($this->val[$champ]==''){
            printf ("<span class=\"om-form-button add-16 add_".$champ."\"
                onclick=\"popupIt('".$champ."',
                '../scr/sousform.php?obj=".$champ."&action=0'+
                '&retourformulaire=".$this->getParameter('obj')."', 860, 'auto',
                getObjId, '".$this->select[$champ]["obj"]."');\"".
                ">");
            printf(_("Saisir un(e) %s"),$champ);
            printf ("</span>");
        }
        //On affiche les données enregistrées et un bouton de suppression
        else {
            //
            printf ("<span class=\"om-form-button delete-16 add_".$champ."\"
                onclick=\"setDataFrequent('','".$champ."');\"".
                "title=\"");
            printf(_("Supprimer"));
            printf("\">");
            printf("&nbsp;");
            printf("</span>");
            //
            
            //
            printf ("<span class=\"om-form-button edit-16 add_".$champ."\"
                onclick=\"popupIt('".$champ."',
                '../scr/sousform.php?obj=".$champ."&action=1&idx=".$this->val[$champ]."'+
                '&retourformulaire=".$this->getParameter('obj')."', 860, 'auto',
                getObjId, '".$this->select[$champ]["obj"]."');\"".
                "title=\"");
            printf(_("editer"));
            printf("\">");
            printf(isset($this->select[$champ]["data"])?$this->select[$champ]["data"]:_("Aucun libelle pour la donnee"));
            printf("</span>");
        } 
        //
        printf ("<input");
        printf (" type=\"hidden\"");
        printf (" name=\"".$champ."\"");
        printf (" id=\"".$champ."\" ");
        printf (" value=\"".$this->val[$champ]."\"");
        printf (" class=\"champFormulaire\"");
        if (!$this->correct) {
            if (isset($this->onchange) and $this->onchange[$champ] != "") {
                printf (" onchange=\"".$this->onchange[$champ]."\"");
            }
            if (isset($this->onkeyup) and $this->onkeyup[$champ] != "") {
                printf (" onkeyup=\"".$this->onkeyup[$champ]."\"");
            }
            if (isset($this->onclick) and $this->onclick[$champ] != "") {
                printf (" onclick=\"".$this->onclick[$champ]."\"");
            }
        } else {
            printf (" disabled=\"disabled\"");
        }
        printf (" />\n");
        //
    }

    /**
     * La valeur du champ est passé par le champ input hidden
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function checkboxhiddenstatic($champ, $validation, $DEBUG = false) {

        // Input de type hidden pour passé la valeur à la validation du 
        // formulaire
        echo "<input";
        echo " type=\"hidden\"";
        echo " id=\"".$champ."\"";
        echo " name=\"".$champ."\"";
        echo " value=\"".$this->val[$champ]."\"";
        echo " class=\"champFormulaire\"";
        echo " />\n";

        // Affichage de la valeur 'Oui' ou 'Non'
        if ($this->val[$champ] == 1 || $this->val[$champ] == "t"
            || $this->val[$champ] == "Oui") {
            $value = "Oui";
        } else {
            $value = "Non";
        }
        echo "<span id=\"".$champ."\" class=\"field_value\">$value</span>";
    }
    
    /**
     * La valeur du champ est passé par le champ input hidden.
     * 
     * @param string  $champ      Nom du champ.
     * @param integer $validation Indicateur de validation du formulaire.
     * @param boolean $DEBUG      Parametre inutilise.
     */
    function regle_donnees_techniques($champ, $validation, $DEBUG = false) {

       $params = array(
        "type" => "text",
        "name" => "cible_".$champ,
        "id" => "cible_".$champ,
        "value" => $this->val["cible_".$champ],
        "size" => $this->taille["cible_".$champ],
        "maxlength" => $this->max["cible_".$champ],
        "correct" => $this->correct,
        "onchange" => "",
        "onkeyup" => "",
        "onclick" => ""
        );
        echo _("Champ : ");
        $this->f->layout->display_formulaire_text($params);
        echo " = ";
        
       $params = array(
        "type" => "text",
        "name" => $champ,
        "id" => $champ,
        "value" => $this->val[$champ],
        "size" => $this->taille[$champ],
        "maxlength" => $this->max[$champ],
        "correct" => $this->correct,
        "onchange" => "",
        "onkeyup" => "",
        "onclick" => ""
        );
        $this->f->layout->display_formulaire_text($params);
    }
    
    /**
     * La valeur du champ est passé par le champ input hidden.
     * 
     * @param string  $champ      Nom du champ.
     * @param integer $validation Indicateur de validation du formulaire.
     * @param boolean $DEBUG      Parametre inutilise.
     */
    function regle_donnees_techniques_static($champ, $validation, $DEBUG = false) {
        //
        if ($this->val["cible_".$champ] !== '') {
            echo "<span id=\"".$champ."\" class=\"regle_donnees_techniques\">";
            printf("%s = %s", $this->val["cible_".$champ], $this->val[$champ]);
            echo "</span>";
        }
    }
    
    /**
     * La valeur du champ est passé par le champ input hidden.
     * 
     * @param string  $champ      Nom du champ.
     * @param integer $validation Indicateur de validation du formulaire.
     * @param boolean $DEBUG      Parametre inutilise.
     */
    function cible_regle_donnees_techniques($champ, $validation, $DEBUG = false) {
        
    }


    /**
     * Widget - Type de champ composé de 4 inputs de 4 caractère chacun, dédié à la saisie
     * de la clé d'accès au portail citoyen.
     *
     * @param string  $champ      Nom du champ.
     * @param integer $validation Etat de la validation du formulaire.
     * @param boolean $DEBUG      Parametre inutilise.
     *
     * @return void
     */
    function citizen_access_key($champ, $validation, $DEBUG = false) {

        // Stockage de l'identifiant du champ suivant
        for ($i = 1; $i < 5; $i++) {
            if ($i < 4) {
                $next_value = 1 + $i;
                $next_value = $champ.$next_value;
            }
            echo "<input";
            echo " type=\"text\"";
            echo " name=\"".$champ."\"";
            echo " id=\"".$champ.$i."\"";
            echo " class=\"champFormulaire citizen_access_key\"";
            echo " maxlength=\"4\"";
            echo " size=\"4\"";
            echo " onKeyUp=\"autojump(this.id,".$next_value.", event);concatenate_citizen_access_key();\"";
            echo " onchange=\"concatenate_citizen_access_key()\"";
            echo " autocomplete=\"off\"";
            echo " />";
            if ($i < 4) {
                echo " - ";
            }
        }
    }

    function autorisation_contestee($champ, $validation, $DEBUG = false) {
        // Champ recherche DI
        echo "<input";
        echo " type=\"text\"";
        echo " name=\"".$champ."\"";
        echo " id=\"".$champ."\"";
        echo " class=\"champFormulaire\"";
        echo " maxlength=\"30\"";
        echo " size=\"20\"";
        echo " value=\"".$this->val[$champ]."\" ";
        echo " />&nbsp;";
        // Bouton chercher
        echo "<input";
        echo " id=\"autorisation_contestee_search_button\"";
        echo " type=\"button\"";
        echo " value=\""._('Chercher')."\" ";
        echo " name=\"looking_for_autorisation_contestee\"";
        echo " onclick=\"lookingForAutorisationContestee();\"";
        echo " class=\"om-button\"";
        echo " />&nbsp;";
        // Bouton annuler
        echo "<input";
        echo " id=\"autorisation_contestee_cancel_button\"";
        echo " type=\"button\"";
        echo " value=\""._('Annuler')."\" ";
        echo " name=\"erase_autorisation_contestee\"";
        echo " onclick=\"eraseAutorisationContestee();\"";
        echo " class=\"om-button\"";
        echo " />";
    }
}

?>
