<?php
/**
 * OM_UTILISATEUR - Surcharge du core
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../core/obj/om_utilisateur.class.php";

class om_utilisateur extends om_utilisateur_core {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Recherche si le login a supprimer est identique au login de
        // l'utilisateur connecte
        $sql = "select * from ".DB_PREFIXE."om_utilisateur where om_utilisateur='".$id."'";
        $res = $db->query($sql);
        if (database::isError($res)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), "");
        } else {
            //
            $row =& $res->fetchRow(DB_FETCHMODE_ASSOC);
            if ($row['login'] == $_SESSION ['login']) {
                $this->msg .= _("Vous ne pouvez pas supprimer votre utilisateur.")."<br/>";
                $this->correct = false;
            }
        }
        // Si la suppression n'est pas possible, on ajoute un message clair
        // pour l'utilisateur
        if ($this->correct == false) {
            $this->msg .= _("SUPPRESSION IMPOSSIBLE")."<br />";
        }
    }

    /**
     * Permet d’effectuer des actions avant la modification des données dans la base
     * @param  mixed    $id     Identifiant de l'enregistrement
     * @param  mixed    $db     Connecteur BDD
     * @param  array    $val    Valeurs
     * @param  boolean  $DEBUG  Mode debug
     */
    function triggersupprimer($id, &$db = null, $val = array(), $DEBUG = null) {

        // Si l'utilisateur est un instructeur
        $get_instructeur_by_om_utilisateur = $this->get_instructeur_by_om_utilisateur($id);
        if ($get_instructeur_by_om_utilisateur != '') {

            // Instanciation de la classe instructeur
            require_once '../obj/instructeur.class.php';
            $instructeur = new instructeur($get_instructeur_by_om_utilisateur, $db, $DEBUG);
            // Valeurs de l'enregistrement
            $value_instructeur = array();
            foreach($instructeur->champs as $key => $champ) {
                //
                $value_instructeur[$champ] = $instructeur->val[$key];
            }
            // Valeur à modifier
            $value_instructeur['om_utilisateur'] = NULL;
            // Modifie l'enregistrement
            $instructeur->modifier($value_instructeur, $db, $DEBUG);
        }

        // Si l'utilisateur est associé à des services 
        $get_lien_service_om_utilisateur_by_om_utilisateur = $this->get_lien_service_om_utilisateur_by_om_utilisateur($id);
        if ($get_lien_service_om_utilisateur_by_om_utilisateur != '') {

            //Instanciation de la classe lien_service_om_utilisateur
            require_once '../obj/lien_service_om_utilisateur.class.php';

            //
            while ($row = &$get_lien_service_om_utilisateur_by_om_utilisateur->fetchRow(DB_FETCHMODE_ASSOC)) {

                $lien_service_om_utilisateur = new lien_service_om_utilisateur($row['lien_service_om_utilisateur'], $db, $DEBUG);
                // Valeurs de l'enregistrement
                $value_lien_service_om_utilisateur = array();
                foreach($lien_service_om_utilisateur->champs as $key => $champ) {
                    //
                    $value_lien_service_om_utilisateur[$champ] = $lien_service_om_utilisateur->val[$key];
                }
                // Valeur à modifier
                $value_lien_service_om_utilisateur['om_utilisateur'] = NULL;
                // Modifie l'enregistrement
                $lien_service_om_utilisateur->modifier($value_lien_service_om_utilisateur, $db, $DEBUG);
            }

            
        }

    }

    /**
     * Récupère l'identifiant de l'instructeur par rapport à l'utilisateur
     * @param  integer $om_utilisateur Identifiant de l'utilisateur
     * @return integer                 Identifiant de l'instructeur
     */
    function get_instructeur_by_om_utilisateur($om_utilisateur) {

        // Initialisation résultat
        $instructeur = '';

        // Si la condition n'est pas vide
        if ($om_utilisateur != "" 
            && $om_utilisateur != null
            && is_numeric($om_utilisateur)) {

            // Requête SQL
            $sql = "SELECT instructeur
                    FROM ".DB_PREFIXE."instructeur
                    WHERE om_utilisateur = $om_utilisateur";
            $this->f->addToLog("get_instructeur_by_om_utilisateur() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $instructeur = $this->db->getOne($sql);
            $this->f->isDatabaseError($instructeur);
        }

        // Retourne résultat
        return $instructeur;

    }


    /**
     * Récupère l'identifiant de l'instructeur par rapport au login de 
     * l'utilisateur.
     *
     * @param  integer $om_utilisateur_login Login de l'utilisateur.
     *
     * @return integer
     */
    function get_instructeur_by_om_utilisateur_login($om_utilisateur_login) {

        // Initialisation résultat
        $instructeur = '';

        // Si la condition n'est pas vide
        if ($om_utilisateur_login != "" 
            && $om_utilisateur_login != null) {

            // Requête SQL
            $sql = "SELECT instructeur
                    FROM ".DB_PREFIXE."instructeur
                    LEFT JOIN ".DB_PREFIXE."om_utilisateur
                        ON instructeur.om_utilisateur = om_utilisateur.om_utilisateur
                    WHERE om_utilisateur.login = '".$om_utilisateur_login."'";
            $this->f->addToLog("get_instructeur_by_om_utilisateur() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $instructeur = $this->db->getOne($sql);
            $this->f->isDatabaseError($instructeur);
        }

        // Retourne résultat
        return $instructeur;

    }


    /**
     * Récupère l'identifiant de lien_service_om_utilisateur par rapport à 
     * l'utilisateur
     * @param  integer $om_utilisateur Identifiant de l'utilisateur
     * @return object                  Résultat de la requête
     */
    function get_lien_service_om_utilisateur_by_om_utilisateur($om_utilisateur) {

        // Initialisation résultat
        $res = '';

        // Si la condition n'est pas vide
        if ($om_utilisateur != "" 
            && $om_utilisateur != null
            && is_numeric($om_utilisateur)) {

            // Requête SQL
            $sql = "SELECT lien_service_om_utilisateur
                    FROM ".DB_PREFIXE."lien_service_om_utilisateur
                    WHERE om_utilisateur = $om_utilisateur";
            $this->f->addToLog("get_lien_service_om_utilisateur_by_om_utilisateur() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->db->query($sql);
            $this->f->isDatabaseError($res);
        }

        // Retourne résultat
        return $res;

    }

}

?>
