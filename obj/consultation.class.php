<?php
//$Id: consultation.class.php 6046 2016-02-26 15:27:06Z fmichon $ 
//gen openMairie le 10/02/2011 20:32 
require_once ("../gen/obj/consultation.class.php");

class consultation extends consultation_gen {
    
    // Champs contenant les UID des fichiers
    var $abstract_type = array(
        "fichier" => "file",
        "om_fichier_consultation" => "file",
    );

    var $metadata = array(
        "om_fichier_consultation" => array(
            "dossier" => "getDossier",
            "dossier_version" => "getDossierVersion",
            "numDemandeAutor" => "getNumDemandeAutor",
            "anneemoisDemandeAutor" => "getAnneemoisDemandeAutor",
            "typeInstruction" => "getTypeInstruction",
            "statutAutorisation" => "getStatutAutorisation",
            "typeAutorisation" => "getTypeAutorisation",
            "dateEvenementDocument" => "getDateEvenementDocument",
            "groupeInstruction" => 'getGroupeInstruction',
            "title" => 'getTitle',
            'concerneERP' => 'get_concerne_erp',
        ),
        "fichier" => array(
            "filename" => "getFichierFilename",
            "dossier" => "getDossier",
            "dossier_version" => "getDossierVersion",
            "numDemandeAutor" => "getNumDemandeAutor",
            "anneemoisDemandeAutor" => "getAnneemoisDemandeAutor",
            "typeInstruction" => "getTypeInstruction",
            "statutAutorisation" => "getStatutAutorisation",
            "typeAutorisation" => "getTypeAutorisation",
            "dateEvenementDocument" => "getDateEvenementDocument",
            "groupeInstruction" => 'getGroupeInstruction',
            "title" => 'getTitle',
            'concerneERP' => 'get_concerne_erp',
        ),
    );

    /**
     * Cette variable permet de stocker le résultat de la méthode
     * getDivisionFromDossier() afin de ne pas effectuer le recalcul à chacun de
     * ces appels.
     * @var string Code de la division du dossier en cours
     */
    var $_division_from_dossier = NULL;

    /**
     * Instance de la classe dossier
     *
     * @var mixed
     */
    var $inst_dossier = null;

    /**
     * Constructeur.
     *
     * @param integer  $id    identifiant de l'enregistrement
     * @param database $db    handler de la base de données
     * @param boolean  $debug debug
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id, $dnu1, $dnu2);
    }// fin constructeur

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();

        // ACTION - 000 - ajouter
        // Modifie la condition d'affichage du bouton ajouter
        $this->class_actions[0]["condition"] = array("can_user_access_dossier_contexte_ajout");

        // ACTION - 001 - modifier
        // 
        $this->class_actions[1]["condition"] = array("is_editable", "can_user_access_dossier_contexte_modification");
        
        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = array("is_deletable", "can_user_access_dossier_contexte_modification");

        // ACTION - 040 - ajout_multiple
        // Ajout de consultations multiples
        $this->class_actions[40] = array(
            "identifier" => "ajout_multiple",
            "view" => "view_ajout_multiple",
            "method" => "ajouter_multiple",
            "button" => "valider",
            "permission_suffix" => "ajouter",
            "condition" => array("is_multiaddable", "can_user_access_dossier_contexte_ajout"),
        );
        // ACTION - 050 - marquer_comme_lu
        $this->class_actions[50] = array(
            "identifier" => "marquer_comme_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("Marquer comme lu"),
                "order" => 50,
                "class" => "lu-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_comme_lu",
            "permission_suffix" => "modifier_lu",
            "condition" => array(
                "is_markable",
                "show_marquer_comme_lu_portlet_action",
                "can_user_access_dossier_contexte_modification",
            ),
        );
        // ACTION - 060 - finaliser
        $this->class_actions[60] = array(
            "identifier" => "finalise",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("Finaliser le document"),
                "order" => 60,
                "class" => "finalise",
            ),
            "view" => "formulaire",
            "method" => "finalize",
            "permission_suffix" => "finaliser",
            "condition" => array(
                "show_consultation_finaliser_portlet_action",
                "is_finalizable",
                "can_user_access_dossier_contexte_modification",
            ),
        );

        // ACTION - 070 - unfinaliser
        $this->class_actions[70] = array(
            "identifier" => "unfinalise",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("Reprendre la redaction du document"),
                "order" => 70,
                "class" => "definalise",
            ),
            "view" => "formulaire",
            "method" => "unfinalize",
            "permission_suffix" => "definaliser",
            "condition" => array(
                "show_unfinalize_portlet_action",
                "is_unfinalizable",
                "can_user_access_dossier_contexte_modification",
            ),
        );

        // ACTION - 080 - consulter_pdf
        $this->class_actions[80] = array(
            "identifier" => "consulter_pdf",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => _("Editer la consultation PDF"),
                "order" => 45,
                "class" => "pdf-16",
            ),
            "view" => "view_consulter_pdf",
            "permission_suffix" => "edition",
            "condition" => "can_user_access_dossier_contexte_modification",
        );

        // ACTION - 090 - Générer l'édition PDF d'une consultation multiple
        // 
        $this->class_actions[90] = array(
            "identifier" => "generate_pdf_consultation_multiple",
            "view" => "generate_pdf_consultation_multiple",
            "permission_suffix" => "ajouter",
        );

        // ACTION - 100 - retour_consultation
        // Lors de la saisie de retour d'avis par le profil suivi des dates
        $this->class_actions[100] = array(
            "identifier" => "retour_consultation",
            "view" => "formulaire",
            "method" => "modifier",
            "button" => _("Modifier"),
            "permission_suffix" => "modifier",
            "condition" => array("is_suivi_retours_de_consultation, can_user_access_dossier_contexte_modification"),
            
        );

        $this->class_actions[110] = array(
            "identifier" => "suivi_mise_a_jour_des_dates",
            "view" => "view_suivi_mise_a_jour_des_dates",
            "permission_suffix" => "suivi_mise_a_jour_des_dates",
        );

        $this->class_actions[120] = array(
            "identifier" => "suivi_retours_de_consultation",
            "view" => "view_suivi_retours_de_consultation",
            "permission_suffix" => "suivi_retours_de_consultation",
        );

        // ACTION - 130 - Afficher la consultation dans les éditions
        $this->class_actions[130] = array(
            "identifier" => "afficher_dans_edition",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("Afficher dans les éditions"),
                "order" => 80,
                "class" => "watch-16"
            ),
            "method" => "manage_visibilite_consultation",
            "permission_suffix" => "visibilite_dans_edition",
            "condition" => array(
                "is_not_visible",
                "can_show_or_hide_in_edition"),
        );

        // ACTION - 140 - Masquer la consultation dans les éditions
        $this->class_actions[140] = array(
            "identifier" => "masquer_dans_edition",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("Masquer dans les éditions"),
                "order" => 80,
                "class" => "unwatch-16"
            ),
            "method" => "manage_visibilite_consultation",
            "permission_suffix" => "visibilite_dans_edition",
            "condition" => array(
                "is_visible",
                "can_show_or_hide_in_edition"),
        );
    }

    /**
     * Permet de modifier le fil d'Ariane depuis l'objet pour un formulaire
     * @param string    $ent    Fil d'Ariane récupéréré 
     * @return                  Fil d'Ariane
     */
    function getFormTitle($ent) {
        //
        if ($this->getParameter('maj') == 120) {
            //
            return _("suivi")." -> "._("demandes d'avis")." -> "._("retours de consultation");
        }
        //
        if ($this->getParameter('maj') == 110) {
            //
            return _("suivi")." -> "._("demandes d'avis")." -> "._("mise a jour des dates");
        }
        //
        if ($this->getParameter('maj') == 100) {
            //
            return _("suivi")." -> "._("demandes d'avis")." -> "._("retours de consultation")." -> ".$this->getVal($this->clePrimaire);
        }
        //
        return $ent;
    }


    /**
     * Ce script permet de gérer l'interface de saisie rapide des retours de
     * consultation par la cellule suivi l'aide d'un code barre.
     */
    function view_suivi_retours_de_consultation() {
        //
        $this->checkAccessibility();
        /**
         * Validation du formulaire
         */
        // Si le formulaire a été validé
        if ($this->f->get_submitted_post_value('code_barres') !== null) {
            // Si la valeur transmise est correcte
            if ($this->f->get_submitted_post_value('code_barres') != ""
                && is_numeric($this->f->get_submitted_post_value('code_barres'))) {
                // Vérification de l'existence de la consultation
                $sql = "SELECT consultation FROM ".DB_PREFIXE."consultation ";
                $sql .= " WHERE code_barres='".$this->f->db->escapesimple($this->f->get_submitted_post_value('code_barres'))."'";
                $res = $this->f->db->query($sql);
                // Logger
                $this->f->addToLog(
                    __METHOD__."(): db->query(\"".$sql."\")",
                    VERBOSE_MODE
                );
                // Gestion des erreurs de base de données
                $this->f->isDatabaseError($res);
                // En fonction du nombre de consultations correspondantes
                // on affiche un message d"erreur ou on redirige vers le formulaire de
                // saisie du retour
                if ($res->numrows()==0) {
                    // Si
                    $message_class = "error";
                    $message = _("Ce code barres de consultation n'existe pas.");
                } elseif ($res->numrows() > 1) {
                    // Si
                    $message_class = "error";
                    $message = _("Plusieurs consultations avec ce code barres.");
                } else {
                    $row =& $res->fetchRow(DB_FETCHMODE_ASSOC);
                    header("Location: ../scr/form.php?obj=consultation&action=100&retour=suivi_retours_de_consultation&idx=".$row['consultation']);
                }
            } elseif ($this->f->get_submitted_post_value('code_barres')!==null && $this->f->get_submitted_post_value('code_barres') == "") {
                // Si aucune valeur n'a été saisie dans le champs consultation
                $message_class = "error";
                $message = _("Veuiller saisir un code barres de consultation.");
            } else {
                // Si
                $message_class = "error";
                $message = _("Cette consultation n'existe pas.");
            }
        }

        /**
         * Affichage des messages et du formulaire
         */
        // Affichage du message de validation ou d'erreur
        if (isset($message) && isset($message_class) && $message != "") {
            $this->f->displayMessage($message_class, $message);
        }
        //
        $datasubmit = $this->getDataSubmit();
        // Inclusion de la classe de gestion des formulaires
        require_once "../obj/om_formulaire.class.php";
        // Ouverture du formulaire
        echo "\t<form";
        echo " method=\"post\"";
        echo " id=\"suivi_retours_de_consultation_form\"";
        echo " action=\"".$datasubmit."\"";
        echo ">\n";
        // Paramétrage des champs du formulaire
        $champs = array("code_barres");
        // Création d'un nouvel objet de type formulaire
        $form = new formulaire(null, 0, 0, $champs);
        // Paramétrage des champs du formulaire
        $form->setLib("code_barres", _("No de code barres de consultation"));
        $form->setType("code_barres", "text");
        $form->setTaille("code_barres", 25);
        $form->setMax("code_barres", 25);
        // Affichage du formulaire
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        // Affichage du bouton
        echo "\t<div class=\"formControls\">\n";
        $this->f->layout->display_form_button(array("value" => _("Valider")));
        echo "\t</div>\n";
        // Fermeture du formulaire
        echo "\t</form>\n";
    }

    /**
     * Ce script permet de mettre à jour les dates de suivi du traitement
     * des consultations.
     */
    function view_suivi_mise_a_jour_des_dates() {
        //
        $this->checkAccessibility();

        // Récupération des valeur passées en POST ou GET
        $date = "";
        if($this->f->get_submitted_post_value('date') !== null) {
            $date = $this->f->get_submitted_post_value('date');
        } elseif($this->f->get_submitted_get_value('date') !== null) {
            $date = $this->f->get_submitted_get_value('date');
        }
        $code_barres = "";
        if($this->f->get_submitted_post_value('code_barres') !== null) {
            $code_barres = $this->f->get_submitted_post_value('code_barres');
        } elseif($this->f->get_submitted_get_value('code_barres')!==null) {
            $code_barres = $this->f->get_submitted_get_value('code_barres');
        }

        // Booléen permettant de définir si un enregistrement à eu lieu
        $correct = false;
        // Booléen permettant de définir si les dates peuvent êtres enregistrées
        $date_error = false;

        // Si le formulaire a été validé
        if ($this->f->get_submitted_post_value('validation') !== null) {
            //Tous les champs doivent obligatoirement être remplis
            if ( !empty($date) && !empty($code_barres) ){
                
                //Vérification de l'existence de la consultation
                $sql = "SELECT consultation, type_consultation 
                    FROM ".DB_PREFIXE."consultation
                    LEFT JOIN ".DB_PREFIXE."service
                    ON service.service = consultation.service
                    WHERE code_barres = '".$this->f->db->escapesimple($code_barres)."'";
                $res = $this->f->db->query($sql);
                $this->f->addToLog("app/demandes_avis_mise_a_jour_des_dates.php : db->query(\"".$sql."\")", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                
                //Si le code barres est correct
                if($res->numrows() == 1) {
                    
                    //Un retour de demande d'avis ne peut être saisie que si le type de 
                    //consultation est "avec_avis_attendu"
                    $row =& $res->fetchRow(DB_FETCHMODE_ASSOC);
                    if ( strcasecmp($row['type_consultation'], "avec_avis_attendu") === 0 ){
                        
                        //On met à jour la date après l'écran de vérification
                        if($this->f->get_submitted_post_value("is_valid") != null and $this->f->get_submitted_post_value("is_valid") == "true") {
                            require_once '../obj/consultation.class.php';
                            $consultation = new consultation($row['consultation'], $this->f->db, DEBUG);
                            $consultation->setParameter("maj", 1);
                            $valF = array();
                            foreach($consultation->champs as $id => $champ) {
                                $valF[$champ] = $consultation->val[$id];
                            }
                            
                            $valF['date_reception']=$date;

                            $consultation->modifier($valF, $this->f->db, DEBUG);
                            // Vérification de la finalisation du document
                            // correspondant au code barres
                            if($consultation->valF["om_final_consultation"] === true) {
                                $message_class = "valid";
                                $message = _("Saisie enregistree");
                                $code_barres = "";
                            } else {
                                //
                                $message_class = "error";
                                $message = sprintf(_("Le document correspondant au 
                                    code barres %s n'est pas finalise, 
                                    la date ne sera pas mise a jour."),
                                    $code_barres);
                            }
                            
                        }
                        //Sinon on récupère les infos du dossier pour les afficher
                        else {
                            // Récupération des infos du dossier
                            $sqlInfo = "SELECT dossier_libelle, libelle, 
                                date_reception,
                                TO_CHAR(date_envoi ,'DD/MM/YYYY') as date_envoi
                                FROM ".DB_PREFIXE."consultation
                                LEFT JOIN ".DB_PREFIXE."dossier 
                                ON dossier.dossier = consultation.dossier
                                LEFT JOIN ".DB_PREFIXE."service
                                ON service.service = consultation.service
                                WHERE code_barres='".$code_barres."'";
                            $resInfo = $this->f->db->query($sqlInfo);
                            $this->f->isDatabaseError($resInfo);
                            $infos = $resInfo->fetchRow(DB_FETCHMODE_ASSOC);
                        }
                    }
                    //C'est un autre type de consultation
                    else{
                        $message_class = "error";
                        $message = _("Cette consultation n'a pas d'avis attendu.");
                    }
                }
                else {
                    $message_class = "error";
                    $message = _("Le numero saisi ne correspond a aucun code barres de consultation.");
                }
            } else {
                $message_class = "error";
                $message = _("Tous les champs doivent etre remplis.");
            }
        }

        /**
         * Affichage des messages et du formulaire
         */
        // Affichage du message de validation ou d'erreur
        if (isset($message) && isset($message_class) && $message != "") {
            $this->f->displayMessage($message_class, $message);
        }
        //
        $datasubmit = $this->getDataSubmit();
        // Inclusion de la classe de gestion des formulaires
        require_once "../obj/om_formulaire.class.php";
        // Ouverture du formulaire
        printf("\t<form");
        printf(" method=\"post\"");
        printf(" id=\"demandes_avis_mise_a_jour_des_dates_form\"");
        printf(" action=\"".$datasubmit."\"");
        printf(">\n");
        // Paramétrage des champs du formulaire
        $champs = array("date", "code_barres");
        if (isset($infos)) {
            array_push(
                $champs,
                "dossier_libelle",
                "service",
                "date_envoi",
                "date_reception",
                "is_valid"
            );
        }
        // Création d'un nouvel objet de type formulaire
        $form = new formulaire(null, 0, 0, $champs);
        // Paramétrage des champs du formulaire
        // Parametrage du champ date
        $form->setLib("date", _("Date")."* :");
        if (isset($infos)) {
            $form->setType("date", "hiddenstaticdate");
        } else {
            $form->setType("date", "date");
        }
        $form->setVal("date", $date);
        $form->setTaille("date", 10);
        $form->setMax("date", 10);

        // Parametrage du champ code_barres
        $form->setLib("code_barres", _("Code barres de consultation")."* :");
        if (isset($infos)) {
            $form->setType("code_barres", "hiddenstatic");
        } else {
            $form->setType("code_barres", "text");
        }
        $form->setVal("code_barres", $code_barres);
        $form->setTaille("code_barres", 20);
        $form->setMax("code_barres", 20);

        // Ajout des infos du dossier correspondantes à la consultation séléctionnée
        if (isset($infos)) {

            // Tous les champs sont défini par defaut à static
            foreach ($infos as $key => $value) {
                $form->setType($key, "static");
                $form->setVal($key, $value);
            }

            // Les champs dont on vient de définir la valeur sont en gras
            $form->setBloc("date_reception", 'DF', "", 'bold');

            // Parametrage du champ dossier
            $form->setLib("dossier_libelle", _("dossier_libelle")." :");
            $form->setType("dossier_libelle", "static");
            $form->setVal("dossier_libelle", $infos['dossier_libelle']);

            // Parametrage du champ service
            $form->setLib("service", _("service")." :");
            $form->setType("service", "static");
            $form->setVal("service", $infos['libelle']);

            // Parametrage des libellés d'envoi avec RAR
            $form->setLib("date_envoi", _("Envoi demande d'avis")." :");
            $form->setLib("date_reception", _("Retour demande d'avis")." :");
            $form->setVal("date_reception", $date);

            // Ajout d'un champ hidden permettant de savoir que le formulaire précédant est celui de vérification
            $form->setLib("is_valid", _("Valide")." :");
            $form->setType("is_valid", "hidden");
            $form->setVal("is_valid", 'true');

            $form->setFieldset('dossier_libelle', 'D', _('Synthese'));
            $form->setFieldset('is_valid', 'F');
        }


        // Création du fieldset regroupant les champs permettant la mise à jour des date
        $form->setFieldset('date', 'D', _('Mise a jour'));
        $form->setFieldset('code_barres', 'F');
        // Affichage du formulaire
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        // Affichage du bouton
        printf("\t<div class=\"formControls\">\n");
        //
        if (!$date_error) {
            $this->f->layout->display_form_button(
                array("value" => _("Valider"), "name" => "validation")
            );
        }
        // Si pas sur l'écran de validation
        if (isset($infos)) {
            printf(
                '<a class="retour" href="%s&amp;date=%s&amp;code_barres=%s">Retour</a>',
                $datasubmit,
                $date,
                $code_barres
            );
        }
        printf("\t</div>\n");
        // Fermeture du formulaire
        printf("\t</form>\n");

    }


    /**
     * Défini si l'utilisateur est de la cellule suivi.
     *
     * @return boolean true si correspond false sinon
     */
    function is_suivi_retours_de_consultation() {
        
        if($this->f->can_bypass("consultation", "modifier")){
            return true;
        }
        
        return $this->f->isAccredited("consultation_suivi_retours_de_consultation");
    }

    /**
     * Si le champ lu est à true l'action "Marquer comme lu" n'est pas affichée
     *
     * @return boolean true sinon lu false sinon
     */
    function show_marquer_comme_lu_portlet_action() {
        if (isset($this->val[array_search("lu", $this->champs)])
            && $this->val[array_search("lu", $this->champs)]== "t") {
            return false;
        }
        return true;
    }

    /**
     * Si le document est finalisé l'action "finaliser" n'est pas affichée
     *
     * @return boolean true sinon lu false sinon
     */
    function show_consultation_finaliser_portlet_action() {
        if ($this->is_document_finalized("om_final_consultation")) {
            return false;
        }
        return true;
    }

    /**
     * Retourne is_document_finalized("om_final_consultation")
     *
     * @return boolean true si finalisé false sinon
     */
    function show_unfinalize_portlet_action() {
        return $this->is_document_finalized("om_final_consultation");
    }

    /**
     * Permet de savoir si le document passé en paramètre est finalisé
     *
     * @param string $field flag finalisé
     *
     * @return boolean true si finalisé false sinon
     */
    function is_document_finalized($field) {
        if($this->getVal($field) == 't') {
            return true;
        }
        return false;
    }


    /**
     *
     */
    var $inst_service = null;

    /**
     *
     */
    function get_inst_service($service = null) {
        //
        if ($service !== null) {
            require_once "../obj/service.class.php";
            return new service($service, $this->f->db, null);
        }
        //
        if (isset($this->inst_service) === false or
            $this->inst_service === null) {
            require_once "../obj/service.class.php";
            $this->inst_service = new service($this->getVal('service'), $this->f->db, null);
        }
        return $this->inst_service;
    }

    /**
     * TREATMENT - marquer_comme_lu.
     * 
     * Cette methode permet de passer la consultation en "lu"
     *
     * @return boolean true si maj effectué false sinon
     */
    function marquer_comme_lu() {
        // Cette méthode permet d'exécuter une routine en début des méthodes
        // dites de TREATMENT.
        $this->begin_treatment(__METHOD__);

        if($this->getVal("lu") == 'f') {
            $this->correct = true;
            $this->valF["lu"] = true;
        
            $res = $this->f->db->autoExecute(
                        DB_PREFIXE.$this->table, 
                        $this->valF, 
                        DB_AUTOQUERY_UPDATE,
                        $this->clePrimaire."=".$this->getVal($this->clePrimaire)
                    );
            if ($this->f->isDatabaseError($res, true)) {
                // Appel de la methode de recuperation des erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                $this->correct = false;
                // Termine le traitement
                return $this->end_treatment(__METHOD__, false);
            } else {
                $this->addToMessage(_("Mise a jour effectue avec succes"));
                return $this->end_treatment(__METHOD__, true);
            }

        } else {
            $this->addToMessage(_("Element deja marque comme lu"));
        }

        // Termine le traitement
        return $this->end_treatment(__METHOD__, false);
    }

    // }}}

    /**
     * TREATMENT - ajouter_multiple.
     * 
     * Cette methode permet d'ajouter plusieurs consultations.
     *
     * @return boolean true si ajouts effectués false sinon
     */
    function ajouter_multiple() {
        // Cette méthode permet d'exécuter une routine en début des méthodes
        // dites de TREATMENT.
        $this->begin_treatment(__METHOD__);

        // Identifiant de l'objet metier a copier
        ($this->f->get_submitted_get_value('idxformulaire') !== null ? $idx = $this->f->get_submitted_get_value('idxformulaire') : $idx = "");
        // Nom de l'objet metier
        ($this->f->get_submitted_get_value('obj') !== null ? $obj = $this->f->get_submitted_get_value('obj') : $obj = "");
        //formulaire retour
        ($this->f->get_submitted_get_value('ret') !== null ? $retourformulaire = $this->f->get_submitted_get_value('ret') : $retourformulaire = "");
        ($this->f->get_submitted_get_value('date_envoi') !== null ? $date_envoi = $this->f->get_submitted_get_value('date_envoi') : $date_envoi = "");
        /*Récupération des données et formatage.*/
        $donnees_temp = explode(';', $this->f->get_submitted_get_value('data'));
        for ( $i = 1 ; $i < count($donnees_temp) ; $i++ )
            $donnees[] = explode('_', $donnees_temp[$i]);
        /* Nombre de consultations papier à générer */
        $nbConsPap = 0;
        
        /* Ajout des données en base de données 
         * 0 : l'ID du service
         * 1 : consultation papier {0,1}
         * */
        if ( isset($donnees) && count($donnees) > 0 ) {
            
            foreach ($donnees as $value) {
                
                $sql = "SELECT delai, id, delai_type
                    FROM ".DB_PREFIXE."service 
                    LEFT JOIN ".DB_PREFIXE."om_etat
                        ON service.edition = om_etat.om_etat
                    WHERE service = $value[0]";
                $res = $this->f->db->query($sql);
                // Si la récupération de la description de l'avis échoue
                if ($this->f->isDatabaseError($res, true)) {
                    // Appel de la methode de recuperation des erreurs
                    $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                    $this->correct = false;
                    // Termine le traitement
                    $this->end_treatment(__METHOD__, false);
                }
                $row=& $res->fetchRow();
                $delai = $row[0];
                $type_edition = $row[1];
                $delai_type = $row[2];

                /*Calcul du delai de retour*/
                $date_envoi_temp = $this->datePHP($date_envoi);
                $delai = $this->dateDB($this->f->mois_date($date_envoi_temp, $delai, "+", $delai_type));

                /*Les données à ajouter*/  
                $arrayVal = array(
                    'consultation' => "]",
                    'dossier' => $idx,
                    'date_envoi' => $date_envoi,
                    'date_retour' => NULL,
                    'date_limite' => $delai,
                    'service' => $value[0],
                    'avis_consultation' => NULL,
                    'date_reception' => NULL,
                    'motivation' => "",
                    'fichier' => NULL,
                    'lu' => TRUE,
                    'code_barres' => NULL,
                    'om_final_consultation' => false,
                    'om_fichier_consultation' => '',
                    'marque' => false,
                    'visible' => true
                );

                $res_ajout = $this->ajouter($arrayVal, $this->f->db, DEBUG);
                if($res_ajout != true) {
                    // Termine le traitement
                    $this->end_treatment(__METHOD__, false);
                }

                /*Comptage du nombre de consultations papier demandées et récupération des ids des PDFs à éditer*/
                if ($value[1]==1){
                    $idxConsultations[] = $this->valF['consultation'];
                    $objConsultations[] = $type_edition;
                    $nbConsPap++;
                }
            }

            /*Génération du PDF*/
            if (isset($idxConsultations) && count($idxConsultations) > 0 ){

                // Stockage de l'identifiant de chaque consultation dont on veut éditer la
                // version papier, séparés par un point-virgule
                $textIdsConsultations = "";
                foreach ($idxConsultations as $value) {
                    if ($textIdsConsultations != "") {
                            $textIdsConsultations .= ";"; 
                    }
                    $textIdsConsultations .= $value; 
                }
                // Stockage de l'objet de chaque consultation dont on veut éditer la
                // version papier, séparés par un point-virgule
                $textObjConsultations = "";
                foreach ($objConsultations as $value) {
                    if ($textObjConsultations != "") {
                        $textObjConsultations .= ";";
                    }
                    $textObjConsultations .= $value;
                }

                // Ouverture du PDF dans une nouvelle fenêtre
                printf("
                    <script language='javascript' type='text/javascript'>
                        window.open('%s','_blank')
                    </script>
                    ",
                    "../scr/form.php?obj=consultation&action=90&idx=0&textobj=".$textObjConsultations."&"."textids=".$textIdsConsultations
                );
            }
            $return_url = "../scr/soustab.php?";
            $return_url .= "obj=consultation";
            $return_url .= "&retourformulaire=".$this->getParameter("retourformulaire");
            $return_url .= "&idxformulaire=".$this->getParameter("idxformulaire");
            $return_url .= "&premier=".$this->getParameter("premiersf");
            $return_url .= "&tricol=".$this->getParameter("tricolsf");

            /*Affichage du message d'information*/
            $this->f->displayMessage("valid", count($donnees)._(' service(s) selectionne(s) dont ').$nbConsPap._(' consultation(s) papier.'));


            // Termine le traitement
            return $this->end_treatment(__METHOD__, true);
        }
    }


    /**
     * VIEW - view_ajout_multiple.
     *
     * Formulaire specifique
     * 
     * @return void
     */
    function view_ajout_multiple() {
        
        if (count($this->f->get_submitted_get_value()) > 0 
        && $this->f->get_submitted_get_value('data') !== null
        && $this->f->get_submitted_get_value('data') != "" ) {
            $this->f->disableLog();
            $this->ajouter_multiple();

        } else {

            // Vérification de l'accessibilité sur l'élément
            $this->checkAccessibility();
            //
            $datasubmit = $this->getDataSubmitSousForm();
            $return_url = "../scr/soustab.php?";
            $return_url .= "obj=consultation";
            $return_url .= "&retourformulaire=".$this->getParameter("retourformulaire");
            $return_url .= "&idxformulaire=".$this->getParameter("idxformulaire");
            $return_url .= "&premier=".$this->getParameter("premiersf");
            $return_url .= "&tricol=".$this->getParameter("tricolsf");
            // Légende du fieldset
            $title = _("Objet");
            /*Requête qui récupère les services qui sont dans des thématiques*/
            $sql = 
                "SELECT
                    ser_cat.service_categorie,
                    ser_cat.libelle AS them_lib,
                    ser.service,
                    CONCAT(ser.abrege, ' - ', ser.libelle) AS ser_lib,
                    ser.consultation_papier
                FROM
                    ".DB_PREFIXE."lien_service_service_categorie lie,
                    ".DB_PREFIXE."service_categorie ser_cat,
                    ".DB_PREFIXE."service ser
                LEFT JOIN ".DB_PREFIXE."om_collectivite 
                    ON ser.om_collectivite = om_collectivite.om_collectivite
                WHERE
                    ser_cat.service_categorie = lie.service_categorie AND
                    ser.service = lie.service AND
                    (
                     (ser.om_validite_debut IS NULL
                      AND (ser.om_validite_fin IS NULL
                           OR ser.om_validite_fin > CURRENT_DATE))
                     OR
                     (ser.om_validite_debut <= CURRENT_DATE
                      AND (ser.om_validite_fin IS NULL
                           OR ser.om_validite_fin > CURRENT_DATE))
                    )";

            // Si c'est un sous-formulaire de dossier d'instruction ou une de ses surcharges
            // mes encours, mes clôtures...
            $is_in_context_of_foreign_key = $this->is_in_context_of_foreign_key("dossier", $this->getParameter('retourformulaire'));
            if ($is_in_context_of_foreign_key == true) {

                // on recupère les services des multicollectivités et de celle
                // du DI
                require_once "../obj/dossier_instruction.class.php";
                $di = new dossier_instruction($this->getParameter('idxformulaire'), $this->db, false);

                //
                $sql .= sprintf(" AND (om_collectivite.niveau = '2' OR ser.om_collectivite = %s) ", $di->getVal('om_collectivite'));
            }
            //
            $sql .= " ORDER BY them_lib, ser.abrege, ser.libelle";

            $res = $this->f->db->query($sql);
            $this->f->addToLog(
                "app/consultation_multiple.php: db->query(\"".$sql."\")", VERBOSE_MODE
                );
            $this->f->isDatabaseError($res);
            
            $temp_ser_cat = 0;
            $liste_gauche = "";

            while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $name = $row['service_categorie'].'_'.
                        $row['service'].'_'.
                        (($row['consultation_papier'] == '' || $row['consultation_papier'] == 'f' ) ? '0' : '1' ).'_';
                // On change de thématique, donc rajoute le nom de la thématique
                if ( $temp_ser_cat != $row['service_categorie'] ){
                    
                    $temp_ser_cat = $row['service_categorie'];
                    $liste_gauche .= '
                    <div id="them_'.$row['service_categorie'].'" class="liste_gauche_them" >'.
                        $row['them_lib'].
                    '</div>
                        <div 
                            class="liste_gauche_service t'.$name.'" 
                            id="t'.$name.'" >
                            '.$row['ser_lib'].'
                            <input class="t'.$name.'" type="checkbox" '.(($row['consultation_papier'] == '' || $row['consultation_papier'] == 'f' ) ? '' : 'checked="checked"' ).'/>
                        </div>
                    ';
                }
                
                /*On est dans la même thématique*/
                else {
                    
                    $liste_gauche .= '
                        <div 
                            class="liste_gauche_service t'.$name.'" 
                            id="t'.$name.'" >
                            '.$row['ser_lib'].'
                            <input class="t'.$name.'" type="checkbox" '.(($row['consultation_papier'] == '' || $row['consultation_papier'] == 'f' ) ? '' : 'checked="checked"' ).'/>
                        </div>
                    ';
                }
                
            }
            
            /*Requête qui récupère les services qui ne sont pas dans une thématique*/
            $sql = 
                "SELECT service.service,
                CONCAT(service.abrege, ' - ', service.libelle) AS ser_lib,
                service.consultation_papier
                FROM ".DB_PREFIXE."service
                LEFT JOIN ".DB_PREFIXE."om_collectivite
                    ON service.om_collectivite = om_collectivite.om_collectivite
                WHERE service NOT IN 
                    ( 
                        SELECT service
                        FROM ".DB_PREFIXE."lien_service_service_categorie
                    ) 
                     AND 
                    ( 
                        om_validite_fin <= CURRENT_DATE OR 
                        om_validite_fin IS NULL 
                    )
                ";

            // Si c'est un sous-formulaire de dossier d'instruction ou une de ses surcharges
            // mes encours, mes clôtures...
            $is_in_context_of_foreign_key = $this->is_in_context_of_foreign_key("dossier", $this->getParameter('retourformulaire'));
            if ($is_in_context_of_foreign_key == true) {

                // on recupère les services des multicollectivités et de celle
                // du DI
                require_once "../obj/dossier_instruction.class.php";
                $di = new dossier_instruction($this->getParameter('idxformulaire'), $this->db, false);

                //
                $sql .= sprintf(" AND (om_collectivite.niveau = '2' OR service.om_collectivite = %s) ", $di->getVal('om_collectivite'));
            }

            // Tri des services qui ne sont pas dans une thématique par ordre alphabétique
            $sql .= " ORDER BY service.abrege, service.libelle";

            
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);
            
            if ($res->numrows() > 0) {
                $liste_gauche .= '
                    <div id="them_0" class="liste_gauche_them">Autres</div>';
            }
            
            while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                
                /*Ajout de tous les services qui n'ont pas de thématique*/
                $name = '0_'. 
                        $row['service'].'_'.
                        (($row['consultation_papier'] == '' || $row['consultation_papier'] == 'f'  ) ? '0' : '1' ).'_';
                $liste_gauche .= '
                    <div 
                        class="liste_gauche_service t'.$name.'" 
                        id="t'.$name.'" >
                        '.$row['ser_lib'].'&nbsp;
                        <input class="t'.$name.'" type="checkbox" '.(($row['consultation_papier'] == '' || $row['consultation_papier'] == 'f' ) ? '' : 'checked="checked"' ).'/>
                    </div>
                ';
            }
            
            /*Affichage du formulaire*/
            echo "\n<!-- ########## START DBFORM ########## -->\n";
            echo "<form";
            echo " method=\"post\"";
            echo " name=\"f2\"";
            echo " action=\"\"";
            echo " id=\"form_val\"";
            //echo " onsubmit=\"ajaxIt('consultation', '');return false;\"";

            //echo " onsubmit=\"affichersform('".$this->getParameter("objsf")."', '".$datasubmit."', this);\"";
            echo ">\n";
                echo '<div class="formEntete ui-corner-all">';
                    echo "<div>";
                        echo '<div class="bloc">';
                            echo "<fieldset class='cadre ui-corner-all ui-widget-content'>\n";
                                echo "\t<legend class='ui-corner-all ui-widget-content ui-state-active'>".
                                    _("Consultation par thematique ")."</legend>";
                                echo "<div class='fieldsetContent' style='width:100%'>";
                                    echo '<div class="field-ser-them field-type-hiddenstatic">';
                                        echo '<div class="form-libelle">';
                                            echo '<label class="libelle-dossier" for="dossier">';
                                                echo _('dossier');
                                            echo '<span class="not-null-tag">*</span>';
                                            echo '</label>';
                                        echo '</div>';
                                        echo '<div class="form-content">';
                                            echo '<input class="champFormulaire" type="hidden" value="'.$this->getParameter("idxformulaire").'" name="dossier"/>';
                                            echo $this->getParameter("idxformulaire");
                                        echo '</div>';
                                    echo '</div>';
                                    /*Code du nouveau champ*/
                                    echo '<div class="field-ser-them ser-them">';
                                        echo '<div class="list-ser-them">';
                                            echo $liste_gauche;
                                        echo '</div>';
                                        echo '<div class="button-ser-them">';
                                            echo '<ul>';
                                                echo '<li>';
                                                    echo '<input type="button" value="'._("Ajouter").' >>" id="add-ser-them"/>';
                                                echo '</li>';
                                                echo '<li>';
                                                    echo '<input type="button" value="<< '._("Supprimer").'" id="del-ser-them"/>';
                                                echo '</li>';
                                            echo '</ul>';
                                        echo '</div>';
                                        echo '<div class="list-sel-ser-them">';
                                            echo '<div class=\'row row_title\'>';
                                                echo '<div class=\'cell1 liste_droite_title list-sel-ser-them-title\'>'._('Service a consulter').'</div>';
                                                echo '<div class=\'cell2 liste_droite_title list-sel-ser-them-title\'>'._('Version papier').'</div>';
                                            echo '</div>';
                                        echo '</div>';
                                    echo '</div>';
                                    /* -- FIN --*/
                                    // Le champ de date d'envoi est visible seulement aux ayant-droits
                                    if($this->f->isAccredited('consultation_saisir_date_envoi')) {
                                        echo '<div class="field-ser-them field-type-date2">';
                                    }
                                    else {
                                        echo '<div class="field field-type-hiddendate">';
                                    }
                                        echo '<div  class="form-libelle">';
                                            echo '<label class="libelle-date_envoi" for="date_envoi">';
                                                echo _('date_envoi');
                                                echo '<span class="not-null-tag">*</span>';
                                            echo '</label>';
                                        echo '</div>';
                                        echo '<div class="form-content">';
                                            echo '<input id="date_envoi" class="champFormulaire datepicker" 
                                                  type="text"  onkeyup="" onchange="fdate(this);" 
                                                  maxlength="10" size="12" value="'.date("d/m/Y").'" 
                                                   name="date_envoi">';
                                        echo '</div>';
                                    echo '</div>';
                                echo "</div>";
                            echo "</fieldset>";
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
                echo '<div class="formControls">';

                echo '<div class="formControls">';
                        echo "<input class=\"om-button ui-button ui-widget ui-state-default ui-corner-all\" 
                              type=\"button\" 
                              
                              value=\"Ajouter le(s) enregistrement(s) de la table : 'Consultation'\" 
                              id=\"button_val\">";
                $this->retoursousformulaire(
                    $this->getParameter("idxformulaire"),
                    $this->getParameter("retourformulaire"),
                    null,
                    $this->getParameter("objsf"),
                    $this->getParameter("premiersf"),
                    $this->getParameter("tricolsf"),
                    $this->getParameter("validation"),
                    $this->getParameter("idx"),
                    $this->getParameter("maj"),
                    $this->getParameter("retour")
                );
                    echo '</div>';
                echo '</div>';
            echo '</form>';
            
            echo "
            <script language='javascript' type='text/javascript'>
                $(function(){
                    changeActionVal('');
                    /*
                        Sélectionne tous les services d'un thème au clique sur celui ci.
                    */
                    $('.liste_gauche_them').click(
                        function(){
                            
                            var id = $(this).attr('id').split('_')[1];
                            var numSer = 0;
                            var numSerWithClass = 0;
                            
                            $('.list-ser-them div').each(
                                function() {
                                            
                                    if ( $(this).attr('id').indexOf('them') == -1 &&
                                         $(this).attr('id').indexOf(id) == 1  &&
                                         $(this).hasClass('liste_gauche_service_selected') )
                                         
                                         numSerWithClass++;
                                    
                                    if ( $(this).attr('id').indexOf('them') == -1 &&
                                         $(this).attr('id').indexOf(id) == 1  )
                                         
                                        numSer++;
                                }
                            );
                            
                            if ( numSerWithClass < numSer && numSerWithClass >= 0 ){
                            
                                 $('.list-ser-them div').each(
                                    function() {
                                        
                                        if ( $(this).attr('id').indexOf('them') == -1 &&
                                         $(this).attr('id').indexOf(id) == 1 &&
                                         !$(this).hasClass('liste_gauche_service_selected') )
                                         
                                            $(this).addClass('liste_gauche_service_selected');
                                    }
                                );
                            }
                            
                            else {
                                
                                $('.list-ser-them div').each(
                                    function() {
                                        
                                       if ( $(this).attr('id').indexOf('them') == -1 &&
                                        $(this).attr('id').indexOf(id) == 1  &&
                                        $(this).hasClass('liste_gauche_service_selected') )
                                         
                                           $(this).removeClass('liste_gauche_service_selected');
                                    }
                               );
                            }
                        }
                    );
                    
                    /*
                        Change la class CSS d'un service sur lequel on clique dans la liste de gauche.
                    */
                    $('.liste_gauche_service').click(
                        function(){
                            $(this).toggleClass('liste_gauche_service_selected');
                        }
                    );
                    
                    /*
                        Change la class CSS d'un service sur lequel on clique dans la liste de droite.
                    */
                    $('.field-ser-them').on( 
                        'click',
                        '.cell1',
                        function(){
                            if ( !$(this).hasClass('liste_droite_title') )
                                $(this).parent().toggleClass('liste_droite_service_selected');
                        }
                    );
                    
                    $('.liste_droite_service input[type=checkbox]').live( 
                        'click',
                        'input[type=checkbox]',
                        function(){

                            old_id = $(this).attr('class');
                            
                            tab_don = old_id.split('_');
                            
                            new_id = tab_don[0] + '_' + tab_don[1] + '_' + ((tab_don[2] == 0 ) ? 1 : 0 ) + '_';

                            changeOneData( ';' + tab_don[1] + '_' + tab_don[2], ';' + tab_don[1] + '_' + ((tab_don[2] == 0) ? 1 : 0) );
                            $('div[class=\"' + old_id + '\"]').attr('class', new_id);
                            $(this).attr('class', new_id);

                        }
                    );
                    
                    $('#date_envoi').change(
                        function (){
                            
                            var listServ = new Array();
                            var data = '';
                    
                            $('.liste_gauche_service_selected').each(
                                function(i) {
                                    
                                    var id = $(this).attr('id');
                                    
                                    if ( listServ.length > 0 && listServ.indexOf(id.split('_')[1]) != -1 )
                                        return;
                                    listServ[i] = id.split('_')[1];
                                    data += ';' + id.split('_')[1] + '_' + id.split('_')[2] ;
                                    
                                }
                            );
                            
                            changeActionVal(data);
                        }
                    );
                    
                    /*
                        Passe les services sélectionnés dans la liste de gauche dans celle de droite.
                    */
                    $('#add-ser-them').click(
                        function() {
                            
                            changeDataLeftColumn();
                        }
                    );    
                    
                    /*
                        Passe les services sélectionnés dans la liste de droite dans celle de gauche.
                    */
                    $('#del-ser-them').click(
                        function() {
                            
                            var data = '';
                            
                            //Supprime les éléments de la liste de droite
                            $('.liste_droite_service_selected').each(
                                function() {

                                    var name = $('#'+ $(this).attr('id') + ' .cell1 div').attr('name');
                                    
                                    manageListServ('.list-ser-them div', name, 1);
                                    
                                    $(this).remove();
                                }
                            );
                            
                            //Change les valeurs qui vont être renvoyées à la validation du formulaire
                            $('.liste_droite_service').each(
                                function(){
                                    
                                    var name = $('#'+ $(this).attr('id') + ' .cell1 div').attr('name');
                                    data += ';' + name.split('_')[1] + '_' + name.split('_')[2] ;
                                }
                            );
                            
                            changeActionVal(data);
                        }
                    );
                });
                
                /*
                    Vérifie que l'objet n'est pas une thématique et que son identifiant correspond.
                */
                function isNotthemIsOneServ( objet, id ){
                    return ( $(objet).attr('id').indexOf('them') == -1 && 
                             $(objet).attr('id').indexOf('_' + id.split('_')[1] + '_') != -1 );
                }
                
                /*
                    Affiche ou cache un élément qui n'est pas une thématique et dont son identifiant correspond.
                */
                function manageListServ( objet , name, type){
        
                    $(objet).each(
                        function() {
                            
                            if ( isNotthemIsOneServ(this, name) ){
                                if ( type == 0 ) 
                                
                                    $(this).hide() ;
                                
                                else {
                                    
                                    if ( $(this).hasClass('liste_gauche_service_selected') )
                                    
                                        $(this).toggleClass('liste_gauche_service_selected');
                                    
                                    $(this).show() ;
                                    
                                }
                            }
                        }
                    );
                }
                
                /*
                    Change les actions qui sont réalisées lors de la soumission du formulaire
                */
                function changeActionVal(data){
                    date = $('#date_envoi').val();
                    


                    $('#button_val').attr(
                        'onclick',
                        'if ( $(\'.liste_gauche_service_selected\').length > 0 && $(\'#date_envoi\').val() != \'\' ) { messageIt(\'consultation\', \'".html_entity_decode($datasubmit)."&data='+data+'&date_envoi='+date+'\',true);' +
                        'messageIt(\'consultation\', \'".html_entity_decode($return_url)."\',false);} else alert(\'Veuillez choisir au moins un service et une date d envoi\');'
                        
                    );
                    
                }
                
                /*
                    Change les actions qui sont réalisées lors de la soumission du formulaire
                */
                function changeOneData( oldData, newData) {
                    
                    date = $('#date_envoi').val();

                    $('#button_val').attr(
                        'onclick',
                        $('#button_val').attr('onclick').replace(oldData,newData)
                    );

                }
                
                function changeDataLeftColumn(){
                    
                    $('.list-sel-ser-them').empty();
                    $('.list-sel-ser-them').html(
                        '<div class=\"row row_title\">' +
                            '<div class=\"cell1 liste_droite_title list-sel-ser-them-title\">"._("Service a consulter")."</div>' +
                            '<div class=\"cell2 liste_droite_title list-sel-ser-them-title\">"._("Version papier")."</div>' +
                        '</div>'
                    );
                    
                    var listServ = new Array();
                    var data = '';
                                    
                    $('.liste_gauche_service_selected').each(
                        function(i) {
                            
                            var id = $(this).attr('id');
                            
                            if ( $.inArray(id.split('_')[1], listServ) != -1 )                            
                                return;
                            
                            data += ';' + id.split('_')[1] + '_' + id.split('_')[2] ;
                            listServ[i] = id.split('_')[1];
                            
                            $('.list-sel-ser-them').append(
                                '<div id=\'s' + i + '\' class=\'row liste_droite_service\'>'+
                                    '<div class=\'cell1\'>'+
                                        '<div class=\'' + $(this).attr('id') + '\' name=\'' + $(this).attr('id') + '\'>'+
                                            $(this).html().split('<')[0]+
                                        '</div>'+
                                    '</div>' + 
                                    '<div class=\'cell2\'>'+
                                        '<div>'+
                                            '<input class=\'' + $(this).attr('id') + '\''+$(this).html().split('<input')[1]+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            );
                            
                            $(this).hide();
                            
                            manageListServ('.list-ser-them div', id, 0);
                            
                        }
                    );
                    changeActionVal(data);
                }
            </script>";
        }
    }


    /**
     * TREATMENT - view_bordereau_envoi_maire.
     * 
     * Génère et affiche l'édition PDF contenant une ou plusieurs consultations.
     * 
     * @return [void]
     */
    function generate_pdf_consultation_multiple() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Récupération de la collectivité du dossier d'instruction
        $collectivite = $this->f->getCollectivite($this->get_dossier_collectivite());
        // Identifiants des consultations à afficher
        $idsConsultations = $this->f->get_submitted_get_value('textids');
        // Type de chaque consultations (avec_avis_attendu, ...)
        $objConsultations = $this->f->get_submitted_get_value('textobj');
        // Génération du PDF
        $result = $this->compute_pdf_output('etat', $objConsultations, $collectivite, $idsConsultations);
        // Affichage du PDF
        $this->expose_pdf_output(
            $result['pdf_output'], 
            $result['filename']
        );
    }


    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        // ajout du retourformulaire aux attributs de l'objet
        $this->retourformulaire = $retourformulaire;
        if ($validation==0) {
            if ($maj == 0 or $maj == 40){
                $form->setVal("dossier", $idxformulaire);
                $form->setVal("date_envoi",date('d/m/Y'));
            }
            if(($maj == 1 or $maj == 90 or $maj == 100) AND $this->getVal("date_retour") == "") {
                if($this->f->isAccredited('consultation_retour_avis_suivi') OR 
                    $this->f->isAccredited('consultation_retour_avis_service')) {
                    $form->setVal("date_retour",date('d/m/Y'));
                }
            }
        }
    }
    
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        if(($maj == 1 or $maj == 90 or $maj == 100) AND $this->getVal("date_retour") == "") {
             $form->setVal("date_retour",date('d/m/Y'));
        }
    }

    function setvalF($val = array()) {
        //
        parent::setValF($val);
        //Si on crée une consultation on la met a visible
        if ($this->getParameter('maj') == 0) {
            $this->valF["visible"] = true;
        }

        if ($this->getParameter('maj') == 0 or $this->getParameter('maj') == 40) {
            //
            if (isset($this->valF["date_envoi"])) {
                $this->valF["date_reception"] = $this->valF["date_envoi"];
            }
            //
            $this->valF["lu"] = true;
        }

        // Si un retour d'avis est modifie on passe "lu" a false
        if(($this->getParameter('maj')==90 or $this->getParameter('maj')==100 ) and (
            $this->val[array_search("avis_consultation",$this->champs)] != $val["avis_consultation"] OR
            $this->val[array_search("date_retour",$this->champs)] != $val["date_retour"] OR
            $this->val[array_search("motivation",$this->champs)] != $val["motivation"] OR
            $this->val[array_search("fichier",$this->champs)] != $val["fichier"])
        ) {
            $this->valF["lu"]=false;
        }
    }

    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, _("id"));
        $form->setLib('visible', _("visible dans les éditions"));
    }

    function setType(&$form,$maj) {
        // Appel du parent
        parent::setType($form,$maj);
        $form->setType('dossier', 'hidden');
        $form->setType('marque', 'hidden');
        // MODE - AJOUTER
        if ($maj == 0) {
            // On cache alors tous les champs que nous ne voulons pas voir
            // apparaître dans le formulaire d'ajout (principalement les
            // informations sur le retour d'avis)
            $form->setType('date_retour', 'hiddendate');
            $form->setType('date_reception', 'hiddendate');
            $form->setType('date_limite', 'hiddendate');
            $form->setType('avis_consultation', 'hidden');
            $form->setType('visible', 'hidden');
            $form->setType('motivation', 'hidden');
            $form->setType('fichier', 'hidden');
            $form->setType('lu', 'hidden');
            // On permet la modification de certains champs
            $form->setType('dossier_libelle', 'hiddenstatic');
            $form->setType('service', 'select');
            // Le champ "date d'envoi" est affiché seulement si l'utilisateur a la 
            // permission
            if($this->f->isAccredited('consultation_saisir_date_envoi')) {
                $form->setType('date_envoi', 'date2');
            }
            else {
                $form->setType('date_envoi', 'hiddendate');
            }
        }
        // MODE - MODIFIER
        if ($maj == 1) {

            // On affiche en statique les informations qui ne sont plus
            // modifiables
            $form->setType('dossier_libelle', 'hiddenstatic');
            $form->setType('date_envoi', 'hiddenstaticdate');
            $form->setType('date_limite', 'hiddenstaticdate');
            $form->setType('service', 'selecthiddenstatic');
            $form->setType('visible', 'checkboxhiddenstatic');
            
            // La date de réception ne peut être modifiée que par un
            // utilisateur en ayant spécifiquement la permission
            if($this->f->isAccredited(array('consultation','consultation_modifier_date_reception'), 'OR')) {
                $form->setType('date_reception', 'date2');
            } else {
                $form->setType('date_reception', 'hiddenstaticdate');
            }

            // Le marqueur lu/non lu ne peut être modifié que par un
            // utilisateur en ayant spécifiquement la permission
            if ($this->f->isAccredited(array('consultation','consultation_modifier_lu'), 'OR')) {
                $form->setType('lu', 'checkbox');
            } else {
                $form->setType('lu', 'hidden');
            }

            // Gestion du type du widget sur le champ fichier
            if($this->getVal("fichier") == "" OR
               $this->f->isAccredited(array('consultation', 'consultation_modifier_fichier'), 'OR')) {
                // Si il n'y a jamais eu de fichier enregistré ou que
                // l'utilisateur a spécifiquement les droits pour modifier
                // un fichier déjà enregistré alors on positionne un type
                // de widget modifiable
                if ($this->retourformulaire == "demande_avis_encours"
                    || $this->retourformulaire == "dossier_qualifier"
                    || $this->retourformulaire == "dossier"
                    || $this->retourformulaire == "dossier_instruction"
                    || $this->retourformulaire == "dossier_instruction_mes_encours"
                    || $this->retourformulaire == "dossier_instruction_tous_encours"
                    || $this->retourformulaire == "dossier_instruction_mes_clotures"
                    || $this->retourformulaire == "dossier_instruction_tous_clotures"
                    || $this->retourformulaire == "dossier_contentieux_mes_infractions"
                    || $this->retourformulaire == "dossier_contentieux_toutes_infractions"
                    || $this->retourformulaire == "dossier_contentieux_mes_recours"
                    || $this->retourformulaire == "dossier_contentieux_tous_recours") {
                    $form->setType('fichier', 'upload2');
                } else {
                    $form->setType('fichier', 'upload');
                }
            } else {
                // Si non on affiche uniquement le nom du fichier 
                $form->setType('fichier', 'filestaticedit');
            }

        }
        // Mode supprimer
        if ($maj == 2) {
            $form->setType('fichier', 'filestatic');
        }
        // MODE - CONSULTER
        if ( $maj == 3 ) {
            $form->setType('fichier', 'file');
        }

        // MODE - AJOUT MULTIPLE
        if ($maj == 40) {
            $form->setType('visible', 'hidden');
        }

        // Mode - retour d'avis
        // Modification layout : écran de retour d'avis permettant
        // uniquement la saisie des trois champs : avis, motivation et fichier
        if ( $maj == 90 ) {

            $form->setType("consultation", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("avis_consultation", $this->retourformulaire)) {
                $form->setType("avis_consultation", "selecthiddenstatic");
            } else {
                $form->setType("avis_consultation", "select");
            }
            $form->setType("motivation", "textarea");
            $form->setType('fichier', 'upload2');

            // On cache alors tous les champs que nous ne voulons pas voir
            $form->setType('dossier_libelle', 'hidden');
            $form->setType('service', 'hidden');
            $form->setType('date_envoi', 'hiddendate');
            $form->setType('date_retour', 'hiddendate');
            $form->setType('date_reception', 'hiddendate');
            $form->setType('date_limite', 'hiddendate');
            $form->setType('lu', 'hidden');
            $form->setType('visible', 'hidden');
        }

        // MODE - retour de consultation par suivi des date
        if ($maj == 100) {
            $form->setType("consultation", "hiddenstatic");
            $form->setType('dossier_libelle', 'hiddenstatic');
            $form->setType('date_envoi', 'hiddenstaticdate');
            $form->setType('date_limite', 'hiddenstaticdate');
            $form->setType('date_reception', 'hiddenstaticdate');
            $form->setType('service', 'selecthiddenstatic');
            $form->setType('date_retour', 'date');
            $form->setType('lu', 'hidden');
            $form->setType('visible', 'hidden');
            $form->setType("avis_consultation", "select");
            $form->setType("motivation", "textarea");
            $form->setType('fichier', 'upload');
            // Gestion du type du widget sur le champ fichier
            if($this->getVal("fichier") == "" OR
               $this->f->isAccredited(array('consultation', 'consultation_modifier_fichier'), 'OR')) {
                // Si il n'y a jamais eu de fichier enregistré ou que
                // l'utilisateur a spécifiquement les droits pour modifier
                // un fichier déjà enregistré alors on positionne un type
                // de widget modifiable
                if ($this->retourformulaire == "demande_avis_encours"
                    || $this->retourformulaire == "dossier_qualifier"
                    || $this->retourformulaire == "dossier"
                    || $this->retourformulaire == "dossier_instruction"
                    || $this->retourformulaire == "dossier_instruction_mes_encours"
                    || $this->retourformulaire == "dossier_instruction_tous_encours"
                    || $this->retourformulaire == "dossier_instruction_mes_clotures"
                    || $this->retourformulaire == "dossier_instruction_tous_clotures"
                    || $this->retourformulaire == "dossier_contentieux_mes_infractions"
                    || $this->retourformulaire == "dossier_contentieux_toutes_infractions"
                    || $this->retourformulaire == "dossier_contentieux_mes_recours"
                    || $this->retourformulaire == "dossier_contentieux_tous_recours") {
                    $form->setType('fichier', 'upload2');
                } else {
                    $form->setType('fichier', 'upload');
                }
            } else {
                // Si non on affiche uniquement le nom du fichier 
                $form->setType('fichier', 'filestaticedit');
            }
        }
        //// On cache la clé primaire
        //$form->setType('consultation', 'hidden');
        //
        if ($this->getParameter("retourformulaire") == "dossier"
            || $this->getParameter("retourformulaire") == "dossier_instruction"
            || $this->getParameter("retourformulaire") == "dossier_instruction_mes_encours"
            || $this->getParameter("retourformulaire") == "dossier_instruction_tous_encours"
            || $this->getParameter("retourformulaire") == "dossier_instruction_mes_clotures"
            || $this->getParameter("retourformulaire") == "dossier_instruction_tous_clotures"
            || $this->getParameter("retourformulaire") == "dossier_contentieux_mes_infractions"
            || $this->getParameter("retourformulaire") == "dossier_contentieux_toutes_infractions"
            || $this->getParameter("retourformulaire") == "dossier_contentieux_mes_recours"
            || $this->getParameter("retourformulaire") == "dossier_contentieux_tous_recours") {
            //
            $form->setType('dossier_libelle', 'hidden');
        }
            
        $form->setType('code_barres', 'hidden');
        
        //Cache les champs pour la finalisation
        $form->setType('om_fichier_consultation', 'hidden');
        $form->setType('om_final_consultation', 'hidden');

        if($maj == 50 OR $maj == 60 OR $maj == 70 OR $maj == 120 OR $maj == 130 OR $maj == 140) {
            foreach ($this->champs as $value) {
                $form->setType($value, 'hidden');
            }
        }
    }

    // Cette méthode permet de calculer la date limite en fonction de la date
    // de réception et du délai de consultation du service consulté
    function calculDateLimite() {
        // mise a jour instruction avec evenement [return delay]
        if ($this->valF["date_reception"] != "") {
            //
            $sql = " select delai from ".DB_PREFIXE."service ";
            $sql .= " where service='".$this->valF["service"]."' ";
            $delai = $this->db->getOne($sql);
            $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            //
            $sql = " select delai_type from ".DB_PREFIXE."service ";
            $sql .= " where service='".$this->valF["service"]."' ";
            $delai_type = $this->db->getOne($sql);
            $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            //
            $this->valF["date_limite"] = $this->f->mois_date($this->valF["date_reception"], $delai, "+", $delai_type);
            //
            if ($delai_type == "mois") {
                $delai_type_trad = _("mois");
            } else {
                $delai_type_trad = _("jour(s)");
            }
            //
            $this->addToMessage(_("delai")." ".
               _("retour")." ".$delai." ".$delai_type_trad." -> ".
               _("retour")." ".date("d/m/Y", strtotime($this->valF["date_limite"])));
        }
    }

    // TRIGGER AVANT MODIFICATION DE DONNEES
    // trigger before modification data
    function triggerajouter($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        $this->calculDateLimite();
        
        // Identifiant du type de courrier
        $idTypeCourrier = '12';
        $idCourrier = str_pad($this->valF["consultation"], 10, "0", STR_PAD_LEFT);
        // Code barres
        $this->valF["code_barres"] = $idTypeCourrier . $idCourrier;
    }

    //
    function triggermodifier($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        $this->calculDateLimite();
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - Notification par courrier du service consulté
     * - Interface avec le référentiel ERP [104]
     * - Interface avec le référentiel ERP [106]
     * - Finalisation du document PDF de consultation
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {

        // On a besoin de l'instance du dossier lié à la consultation
        $inst_di = $this->get_inst_dossier($this->valF['dossier']);

        // On a besoin de l'instance du service lié à la consultation
        $inst_service = $this->get_inst_service($this->valF['service']);

        /**
         * Notification par courriel du service consulté.
         * 
         * Si il y a un problème lors de l'envoi du courriel, on prévient l'utilisateur
         * mais on ne bloque pas le traitement. Le courriel de notification n'a pas de 
         * de caractère critique.
         */
        if ($inst_service->getVal('notification_email') == 't') {
            // Composition du titre du courriel
            $title = sprintf(
                '%s %s',
                _("Consultation de services : dossier no"),
                $inst_di->getVal($inst_di->clePrimaire)
            );
            $title = iconv("UTF-8", "CP1252", $title);
            // Composition du corps du courriel
            $corps = sprintf(
                '%s %s<br/>%s %s %s %s %s<br/>%s <a href=\'%s\'>%s</a> %s <a href=\'%s\'>%s</a>',
                _("Votre service est consulte concernant le dossier no"),
                $inst_di->getVal($inst_di->clePrimaire),
                _("Il concerne le terrain situe a l'adresse :"),
                $inst_di->getVal('terrain_adresse_voie_numero'),
                $inst_di->getVal('terrain_adresse_voie'),
                $inst_di->getVal('terrain_adresse_code_postal'),
                $inst_di->getVal('terrain_adresse_localite'),
                _("Vous pouvez y acceder et rendre votre avis a l'adresse"),
                // Adresse interne, si l'adresse termine par &idx= alors on ajoute l'identifiant de la consultation
                $this->f->getParameter('services_consultes_lien_interne').((substr($this->f->getParameter('services_consultes_lien_interne'), -5) == "&idx=") ? $this->valF['consultation'] : ""),
                _("Lien interne"),
                _("ou"),
                // Adresse externe, si l'adresse termine par &idx= alors on ajoute l'identifiant de la consultation
                $this->f->getParameter('services_consultes_lien_externe').((substr($this->f->getParameter('services_consultes_lien_externe'), -5) == "&idx=") ? $this->valF['consultation'] : ""),
                _("Lien externe")
            );
            $corps = iconv("UTF-8", "CP1252", $corps);
            // Envoi du mail avec message de retour
            if ($this->f->sendMail($title, $corps, iconv("UTF-8", "CP1252", $inst_service->getVal('email')))) {
                $this->addToMessage(_("Envoi d'un mail de notification au service")." \"(".$inst_service->getVal('abrege').") ".$inst_service->getVal("libelle")."\".");
            } else {
                $this->addToMessage(_("L'envoi du mail de notification a échoué.")
                    .' '._("Veuillez contacter votre administrateur."));
            }
        }

        // Récupère la collectivité du dossier d'instruction lié à la
        // consultation
        $om_collectivite = $this->get_dossier_collectivite();

        /**
         * Interface avec le référentiel ERP.
         *
         * (WS->ERP)[104] Demande d'instruction de dossier PC pour un ERP -> PC
         * Déclencheur :
         *  - L'option ERP est activée
         *  - Le dossier est de type PC
         *  - Le formulaire d'ajout de consultation est validé avec un service
         *    correspondant à un des services ERP pour avis
         */
        //
        if ($this->f->is_option_referentiel_erp_enabled($om_collectivite) === true
            && $this->f->getDATCode($inst_di->getVal($inst_di->clePrimaire)) == $this->f->getParameter('erp__dossier__nature__pc')
            && in_array($inst_service->getVal($inst_service->clePrimaire), explode(";", $this->f->getParameter('erp__services__avis__pc')))) {
            //
            $infos = array(
                "dossier_instruction" => $inst_di->getVal($inst_di->clePrimaire),
                "consultation" => $this->valF['consultation'],
                "date_envoi" => $this->valF['date_envoi'],
                "service_abrege" => $inst_service->getVal('abrege'),
                "service_libelle" => $inst_service->getVal('libelle'),
                "date_limite" => $this->valF['date_limite'],
            );
            //
            $ret = $this->f->send_message_to_referentiel_erp(104, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification (104) du référentiel ERP. Contactez votre administrateur."));
                return false;
            }
            $this->addToMessage(_("Notification (104) du référentiel ERP OK."));
        }

        /**
         * Interface avec le référentiel ERP.
         *
         * (WS->ERP)[106] Consultation ERP pour conformité -> PC
         * Déclencheur :
         *  - L'option ERP est activée
         *  - Le dossier est de type PC
         *  - Le formulaire d'ajout de consultation est validé avec un service
         *    correspondant à un des services ERP pour conformité
         */
        //
        if ($this->f->is_option_referentiel_erp_enabled($om_collectivite) === true
            && $this->f->getDATCode($inst_di->getVal($inst_di->clePrimaire)) == $this->f->getParameter('erp__dossier__nature__pc')
            && in_array($inst_service->getVal($inst_service->clePrimaire), explode(";", $this->f->getParameter('erp__services__conformite__pc')))) {
            //
            $infos = array(
                "dossier_instruction" => $inst_di->getVal($inst_di->clePrimaire),
                "consultation" => $this->valF['consultation'],
                "date_envoi" => $this->valF['date_envoi'],
                "service_abrege" => $inst_service->getVal('abrege'),
                "service_libelle" => $inst_service->getVal('libelle'),
                "date_limite" => $this->valF['date_limite'],
            );
            //
            $ret = $this->f->send_message_to_referentiel_erp(106, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification (106) du référentiel ERP. Contactez votre administrateur."));
                return false;
            }
            $this->addToMessage(_("Notification (106) du référentiel ERP OK."));
        }

        /**
         * Finalisation du document PDF de consultation.
         */
        //
        return $this->finaliserAjouter($id);
    }

    // =============================================
    // Ajout du fielset
    // Add fieldset
    // =============================================
    function setLayout(&$form, $maj){

        //Champs sur lequel s'ouvre le fieldset
        $form->setBloc('dossier','D',"");
        $form->setFieldset('dossier','D',_('Consultation'));

        //Champs sur lequel se ferme le fieldset
        $form->setFieldset('visible','F','');
        $form->setBloc('visible','F');
            
            // MODE - autre que AJOUTER alors on affiche un fieldset retour
            // d'avis
            if ($maj != 0) {
                
                //Champs sur lequel s'ouvre le fieldset
                $form->setBloc('date_reception','D',"");
                $form->setFieldset('date_reception','D',_('Retour d\'avis'));
    
                //Champs sur lequel se ferme le fieldset
                $form->setFieldset('lu','F','');
                $form->setBloc('lu','F');
                
            }
    }

    /** Surcharge de la methode retour afin de retourner sur la page de saisie de
    * code barre si besoin
    **/
    function retour($premier = 0, $recherche = "", $tricol = "") {
        $params ="obj=".get_class($this);
        if($this->getParameter("retour")=="form") {
            $params .= "&amp;idx=".$this->getParameter("idx");
            $params .= "&amp;action=3";
        }
        $params .= "&amp;premier=".$this->getParameter("premier");
        $params .= "&amp;tricol=".$this->getParameter("tricol");
        $params .= "&amp;recherche=".$this->getParameter("recherche");
        $params .= "&amp;selectioncol=".$this->getParameter("selectioncol");
        $params .= "&amp;advs_id=".$this->getParameter("advs_id");
        $params .= "&amp;valide=".$this->getParameter("valide");
        echo "\n<a class=\"retour\" ";
        echo "href=\"";
        //

        if($this->getParameter("retour")=="form" AND !($this->getParameter("validation")>0 AND $this->getParameter("maj")==2 AND $this->correct)) {
            echo "form.php?".$params;
        } elseif($this->getParameter("retour")=="suivi_retours_de_consultation") {
            echo "../scr/form.php?obj=consultation&idx=0&action=120";
        } else {
            echo "tab.php?".$params;
        }
        //
        echo "\"";
        echo ">";
        //
        echo _("Retour");
        //
        echo "</a>\n";
    }

    /**
     * Surcharge du bouton retour pour popup
     */
    function retoursousformulaire($idxformulaire = NULL, $retourformulaire = NULL, $val = NULL,
                                  $objsf = NULL, $premiersf = NULL, $tricolsf = NULL, $validation = NULL,
                                  $idx = NULL, $maj = NULL, $retour = NULL) {
        if($retourformulaire === "demande_avis_encours") {
            echo "\n<a class=\"retour\" ";
            echo "href=\"";
            echo "#";
            echo  "\" ";
            echo ">";
            //
            echo _("Retour");
            //
            echo "</a>\n";
        } else {
            parent::retoursousformulaire($idxformulaire, $retourformulaire, $val,
                                  $objsf, $premiersf, $tricolsf, $validation,
                                  $idx, $maj, $retour);
        }
    }
    
    /**
     * Ajout des contraintes spécifiques pour l'ajout d'un fichier en retour de 
     * consultation
     */
    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        // avis_consultation
        $this->init_select($form, $db, $maj, $debug, "avis_consultation",
                           $sql_avis_consultation, $sql_avis_consultation_by_id, true);

        // service
        // si contexte DI, ou surcharge (mes_encours, mes_clotures...)
        $is_in_context_of_foreign_key = $this->is_in_context_of_foreign_key("dossier", $this->getParameter('retourformulaire'));
        if ($is_in_context_of_foreign_key == true) {
            // on recupÃšre les services des multicollectivitÃ©s et de celle du DI
            require_once "../obj/dossier_instruction.class.php";
            $di = new dossier_instruction($this->getParameter('idxformulaire'), $db, $debug);
            $sql_service_by_di = str_replace('<collectivite_di>', $di->getVal("om_collectivite"), $sql_service_by_di);

            $this->init_select($form, $db, $maj, $debug, "service",
                           $sql_service_by_di, $sql_service_by_id, true);
        } else {
            $this->init_select($form, $db, $maj, $debug, "service", $sql_service, $sql_service_by_id, true);
        }

        //Seulement dans le cas d'un retour d'avis
        if($this->retourformulaire == "demande_avis_encours" or $this->getParameter("maj") == 100 or $this->getParameter("maj") == 90) {
            // avis_consultation
            $this->init_select($form, $db, 1, $debug, "avis_consultation",
                           $sql_avis_consultation, $sql_avis_consultation_by_id, true);
            
            //Tableau des contraintes spécifiques
            $params = array(
                "constraint" => array(
                    "size_max" => 2,
                    "extension" => ".pdf"
                ),
            );
            
            $form->setSelect("fichier", $params);
        }
    }

    /**
     * Finalisation du document lors de l'ajout d'une consultation.
     *
     * @param integer $id_consult indentifiant de l'objet
     */
    function finaliserAjouter($id_consult){
        //
        $this->begin_treatment(__METHOD__);

        //
        $admin_msg_error = _("Veuillez contacter votre administrateur.");
        $file_msg_error = _("La finalisation du document a échoué.")
            ." ".$admin_msg_error;
        $bdd_msg_error = _("Erreur de base de données.")
            ." ".$admin_msg_error;
        $log_msg_error = "Finalisation non enregistrée - id consultation = %s - uid fichier = %s";

        $uid = $this->file_finalizing($id_consult);
        // Comme pour le fonctionnement de l'envoi de mail,
        // Si échec cela ne stoppe pas la création de la consultation.
        // L'utilisateur en est tout de même informé dans l'IHM
        // et l'erreur est loguée.
        if ($uid == '' || $uid == 'OP_FAILURE' ) {
            $this->addToMessage($file_msg_error);
            $this->addToLog(sprintf($log_msg_error, $id_consult, $uid));
            return $this->end_treatment(__METHOD__, true);
        }

        // Si succès mise à jour de la consultation
        $valF = array(
            "om_final_consultation" => true,
            "om_fichier_consultation" => $uid
        );
        //
        $res = $this->db->autoExecute(DB_PREFIXE.$this->table, $valF, 
            DB_AUTOQUERY_UPDATE, $this->getCle($id_consult));
        //
        $this->addToLog(
            "finaliserAjouter() : db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($id_consult)."\")",
            VERBOSE_MODE
        );
        //
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->correct = false;
            // Remplacement du message de validation
            $this->msg = '';
            $this->addToMessage($bdd_msg_error);
            return $this->end_treatment(__METHOD__, false);
        }
        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Finalisation du fichier.
     *
     * @param integer $id indentifiant de l'objet
     *
     * @return  string uid du fichier finalisé
     */
    function file_finalizing($id){
        $pdf = $this->generate_pdf_consultation();

        if(isset($this->valF["om_final_consultation"])) {
            $finalized = $this->valF["om_final_consultation"];
        } else {
            $finalized = $this->getVal("om_final_consultation");
        }

        // Métadonnées du document
        $metadata = array(
            'filename' => $pdf["filename"],
            'mimetype' => 'application/pdf',
            'size' => strlen($pdf["pdf_output"])
        );

        // Récupération des métadonnées calculées après validation
        $spe_metadata = $this->getMetadata("om_fichier_consultation");

        $metadata = array_merge($metadata, $spe_metadata);

        // Si le document a déjà été finalisé
        if ( $finalized != '' ){

            // Met à jour le document mais pas son uid
            $uid = $this->f->storage->update(
                $this->getVal("om_fichier_consultation"), $pdf["pdf_output"], $metadata);
        }
        // Sinon, ajoute le document et récupère son uid
        else {

            // Stockage du PDF
            $uid = $this->f->storage->create($pdf["pdf_output"], $metadata);
        }

        return $uid;

    }


    /**
     * TREATMENT - finalize.
     * 
     * Permet de finaliser un enregistrement
     *
     * @param array $val  valeurs soumises par le formulaire
     * @param null  $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null  $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return boolean
     */
    function finalize($val = array(), &$dnu1 = null, $dnu2 = null) {

        // Cette méthode permet d'exécuter une routine en début des méthodes
        // dites de TREATMENT.
        $this->begin_treatment(__METHOD__);

        // Traitement de la finalisation
        $ret = $this->manage_finalizing("finalize", $val);

        // Si le traitement retourne une erreur
        if ($ret !== true) {

            // Termine le traitement
            $this->end_treatment(__METHOD__, false);
        }

        // Termine le traitement
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - unfinalize.
     * 
     * Permet de definaliser un enregistrement
     *
     * @param array $val  valeurs soumises par le formulaire
     * @param null  $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null  $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return boolean
     */
    function unfinalize($val = array(), &$dnu1 = null, $dnu2 = null) {

        // Cette méthode permet d'exécuter une routine en début des méthodes
        // dites de TREATMENT.
        $this->begin_treatment(__METHOD__);

        // Traitement de la finalisation
        $ret = $this->manage_finalizing("unfinalize", $val);

        // Si le traitement retourne une erreur
        if ($ret !== true) {

            // Termine le traitement
            $this->end_treatment(__METHOD__, false);
        }

        // Termine le traitement
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - manage_visibilite_consultation.
     * 
     * Permet de masquer la consultation dans les éditions si elle est actuellement
     * affichée, ou de l'afficher si elle est actuellement masquée.
     * 
     *
     * @param array $val  valeurs soumises par le formulaire
     * @param null  $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null  $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return boolean
     */
    function manage_visibilite_consultation($val = array(), &$dnu1 = null, $dnu2 = null) {

        // Cette méthode permet d'exécuter une routine en début des méthodes
        // dites de TREATMENT.
        $this->begin_treatment(__METHOD__);

        // Recuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);

        if($this->getVal("visible") == 'f') {
            $valF["visible"] = true;
        }
        else {
            $valF["visible"] = false;
        }

        // Execution de la requête de modification des donnees de l'attribut
        $res = $this->db->autoExecute(DB_PREFIXE.$this->table, $valF, 
            DB_AUTOQUERY_UPDATE, $this->getCle($id));
        $this->addToLog(
            "manage_visibilite_consultation() : db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($id)."\")",
            VERBOSE_MODE
        );
        
        // Si une erreur survient
        if (database::isError($res, true)) {

            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            $this->addToMessage(_("Erreur de base de donnees. Contactez votre administrateur."));
            // Termine le traitement
            return $this->end_treatment(__METHOD__, false);
        }

        if ($valF["visible"] === false) {
            $this->addToMessage(_("La consultation est masquée dans les éditions."));
        }
        else {
            $this->addToMessage(_("La consultation est affichée dans les éditions."));
        }
        // Termine le traitement
        return $this->end_treatment(__METHOD__, true);
    }


    /**
     * Finalisation des documents.
     * 
     * @param string $mode finalize/unfinalize
     * @param array  $val  valeurs soumises par le formulaire
     */
    function manage_finalizing($mode = null, $val = array()) {
        //
        $this->begin_treatment(__METHOD__);

        // Recuperation de la valeur de la cle primaire de l'objet
        $id_consult = $this->getVal($this->clePrimaire);

        //
        $admin_msg_error = _("Veuillez contacter votre administrateur.");
        $file_msg_error = _("Erreur de traitement de fichier.")
            ." ".$admin_msg_error;
        $bdd_msg_error = _("Erreur de base de données.")
            ." ".$admin_msg_error;
        $log_msg_error = "Finalisation non enregistrée - id consultation = %s - uid fichier = %s";

        // Si on finalise le document
        if ($mode == "finalize") {
            //
            $etat = _('finalisation');
            // Finalisation du fichier
            $uid = $this->file_finalizing($id_consult);
        }
        //
        else {
            //
            $etat = _('définalisation');
            //Récupération de l'uid du document finalisé
            $uid = $this->getVal("om_fichier_consultation");
        }

        // Si on définalise l'UID doit être défini
        // Si on finalise la création/modification du fichier doit avoir réussi
        if ($uid == '' || $uid == 'OP_FAILURE' ) {
            $this->correct = false;
            $this->addToMessage($file_msg_error);
            $this->addToLog(sprintf($log_msg_error, $id_consult, $uid));
            return $this->end_treatment(__METHOD__, false);
        }

        foreach ($this->champs as $key => $value) {
            //
            $val[$value] = $this->val[$key];
        }
        $this->setvalF($val);

        // Verification de la validite des donnees
        $this->verifier($this->val, $this->db, DEBUG);
        // Si les verifications precedentes sont correctes, on procede a
        // la modification, sinon on ne fait rien et on retourne une erreur
        if ($this->correct === true) {

            // Execution du trigger 'before' specifique au MODE 'update'
            $this->triggermodifier($id_consult, $this->db, $this->val, DEBUG);
            // Suppression de son message de validation
            $this->msg = '';

            //
            $valF = '';
            if ($mode == "finalize") {
                $valF["om_final_consultation"] = true;
            } else {
                $valF["om_final_consultation"] = false;
            }
            $valF["om_fichier_consultation"] = $uid;
                
            // Execution de la requête de modification des donnees de l'attribut
            // valF de l'objet dans l'attribut table de l'objet
            $res = $this->db->autoExecute(DB_PREFIXE.$this->table, $valF, 
                DB_AUTOQUERY_UPDATE, $this->getCle($id_consult));
            $this->addToLog(
                "finaliser() : db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($id_consult)."\")",
                VERBOSE_MODE
            );
            //
            if ($this->f->isDatabaseError($res, true) === true) {
                $this->correct = false;
                $this->addToMessage($bdd_msg_error);
                return $this->end_treatment(__METHOD__, false);
            }
            //
            $this->addToMessage(sprintf(_("La %s du document s'est effectuee avec succes."), $etat));
            return $this->end_treatment(__METHOD__, true);
        }
        // L'appel de verifier() a déjà positionné correct à false
        // et défini un message d'erreur.
        $this->addToLog(sprintf($log_msg_error, $id_consult, $uid));
        return $this->end_treatment(__METHOD__, false);
    }

    /**
     * Permet de récupérer l'édition de la consultation.
     *
     * @param string $output type de sortie
     *
     * @return string si le type de sortie est string, le contenu du pdf est retourné.
     */
    function generate_pdf_consultation() {
        if(isset($this->valF[$this->clePrimaire])) {
            $id = $this->valF[$this->clePrimaire];
        } else {
            $id = $this->getVal($this->clePrimaire);
        }

        // Requête qui récupère le type de consultation
        $sql = " SELECT id ";
        $sql .= " FROM ".DB_PREFIXE."service ";
        $sql .= " LEFT JOIN ".DB_PREFIXE."om_etat ";
        $sql .= " ON service.edition = om_etat.om_etat ";
        $sql .= " WHERE service=(";
            $sql .= " SELECT service ";
            $sql .= " FROM ".DB_PREFIXE."consultation ";
            $sql .= " WHERE consultation=".$id."";
        $sql .= ") "; 
        $obj = $this->f->db->getone($sql);
        $this->f->addTolog(
            __FILE__." - ".__METHOD__." : db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        if($this->f->isDatabaseError($obj, true)) {
            $this->addToMessage(_("Aucun document genere."));
            $this->addToMessage(_("Finalisation non enregistree"));
            return false;
        }
        $collectivite = $this->f->getCollectivite($this->get_dossier_collectivite());

        $pdf_output = $this->compute_pdf_output("etat", $obj, $collectivite, $id);

        return $pdf_output;
    }

    function get_dossier_collectivite() {
        if(isset($this->valF["dossier"])) {
            $dossier = $this->valF["dossier"];
        } else {
            $dossier = $this->getVal("dossier");
        }

        $sql = "SELECT om_collectivite FROM ".DB_PREFIXE."dossier WHERE dossier.dossier='".$dossier."'";
        $collectivite = $this->f->db->getone($sql);
        $this->f->addTolog(
            __FILE__." - ".__METHOD__." : db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        if($this->f->isDatabaseError($collectivite, true)) {
            $this->addToMessage(_("Aucun document genere."));
            $this->addToMessage(_("Finalisation non enregistree"));
            return false;
        }
        return $collectivite;
    }

    function view_consulter_pdf(){
        if($this->getVal("om_final_consultation") == 't') {
            $lien = '../spg/file.php?obj=consultation&'.
                    'champ=om_fichier_consultation&id='.$this->getVal($this->clePrimaire);
            header("Location: ".$lien);
        } else {
            
            $output = $this->generate_pdf_consultation();
            $this->expose_pdf_output($output["pdf_output"], $output["filename"]);
        }
        exit();
    }


    /**
     * Création du nom de fichier
     * @return string numéro de dossier d'instruction
     */
    protected function getFichierFilename() {
        return "consultation_avis_".$this->valF[$this->clePrimaire].".pdf";
    }

    /**
     * Récupération du numéro de dossier d'instruction à ajouter aux métadonnées
     * @return string numéro de dossier d'instruction
     */
    protected function getDossier() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->dossier;
    }
    /**
     * Récupération la version du dossier d'instruction à ajouter aux métadonnées
     * @return int Version
     */
    protected function getDossierVersion() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->version;
    }
    /**
     * Récupération du numéro de dossier d'autorisation à ajouter aux métadonnées
     * @return string numéro de dossier d'autorisation
     */
    protected function getNumDemandeAutor() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->dossier_autorisation;
    }
    /**
     * Récupération de la date de demande initiale du dossier à ajouter aux métadonnées
     * @return date demande initiale
     */
    protected function getAnneemoisDemandeAutor() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->date_demande_initiale;
    }
    /**
     * Récupération du type de dossier d'instruction à ajouter aux métadonnées
     * @return string type de dossier d'instruction
     */
    protected function getTypeInstruction() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->dossier_instruction_type;
    }
    /**
     * Récupération du statut du dossier d'autorisation à ajouter aux métadonnées
     * @return string avis
     */
    protected function getStatutAutorisation() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->statut;
    }
    /**
     * Récupération du type de dossier d'autorisation à ajouter aux métadonnées
     * @return string type d'autorisation
     */
    protected function getTypeAutorisation() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->dossier_autorisation_type;
    }
    /**
     * Récupération de la date d'ajout de document à ajouter aux métadonnées
     * @return date de l'évènement
     */
    protected function getDateEvenementDocument() {
        return date("Y-m-d");
    }
    /**
     * Récupération du groupe d'instruction à ajouter aux métadonnées
     * @return string Groupe d'instruction
     */
    protected function getGroupeInstruction() {
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        return $this->specificMetadata->groupe_instruction;
    }
    /**
     * Récupération du type de document à ajouter aux métadonnées
     * @return string Type de document
     */
    protected function getTitle() {
        if ($this->retourformulaire == "demande_avis_encours") {
            return 'Retour de consultation';
        } else {
            return 'Demande de consultation';
        }
    }


    /**
     * Récupération du champ ERP du dossier d'instruction.
     *
     * @return boolean
     */
    public function get_concerne_erp() {
        //
        if(empty($this->specificMetadata)) {
            $this->getSpecificMetadata();
        }
        //
        return $this->specificMetadata->erp;
    }


    /**
     * Cette méthode permet de stocker en attribut toutes les métadonnées
     * nécessaire à l'ajout d'un document.
     */
    public function getSpecificMetadata() {
        if (isset($this->valF["dossier"]) AND $this->valF["dossier"] != "") {
            $dossier = $this->valF["dossier"];
        } else {
            $dossier = $this->getVal("dossier");
        }
        //Requête pour récupérer les informations essentiels sur le dossier d'instruction
        $sql = "SELECT dossier.dossier as dossier,
                        dossier_autorisation.dossier_autorisation as dossier_autorisation, 
                        to_char(dossier.date_demande, 'YYYY/MM') as date_demande_initiale,
                        dossier_instruction_type.code as dossier_instruction_type, 
                        etat_dossier_autorisation.libelle as statut,
                        dossier_autorisation_type.code as dossier_autorisation_type,
                        groupe.code as groupe_instruction,
                        CASE WHEN dossier.erp IS TRUE
                            THEN 'true'
                            ELSE 'false'
                        END as erp
                FROM ".DB_PREFIXE."dossier 
                    LEFT JOIN ".DB_PREFIXE."dossier_instruction_type  
                        ON dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
                    LEFT JOIN ".DB_PREFIXE."dossier_autorisation 
                        ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation 
                    LEFT JOIN ".DB_PREFIXE."etat_dossier_autorisation
                        ON  dossier_autorisation.etat_dossier_autorisation = etat_dossier_autorisation.etat_dossier_autorisation
                    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
                        ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
                    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
                        ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
                    LEFT JOIN ".DB_PREFIXE."groupe
                        ON dossier_autorisation_type.groupe = groupe.groupe
                WHERE dossier.dossier = '".$dossier."'";
        $res = $this->db->query($sql);
        $this->f->addToLog("obj/document_numerise.class.php : db->query(".$sql.")", VERBOSE_MODE);
        if ( database::isError($res)){
            die();
        }
        
        //Le résultat est récupéré dans un objet
        $row =& $res->fetchRow(DB_FETCHMODE_OBJECT);

        //Si il y a un résultat
        if ($row !== null) {

            // Instrance de la classe dossier
            $inst_dossier = $this->get_inst_dossier($dossier);

            // Insère l'attribut version à l'objet
            $row->version = $inst_dossier->get_dossier_instruction_version();

            //Alors on créé l'objet dossier_instruction
            $this->specificMetadata = $row;

        }
    }
    
    /**
     * 
     * @return boolean
     */
    function is_instructeur(){
        
        //Si l'utilisateur est un instructeur
        if ($this->f->isUserInstructeur()){
            return true;
        }
        return false;
    }
    
    function is_editable(){
        // Si bypass
        if ($this->f->can_bypass("consultation", "modifier")){
            return true;
        }
        
        // Tester si le dossier est cloturé ,
        // et si l'instructeur est de la même division
        if ($this->is_instructeur_from_division_dossier() === true and
            $this->is_dossier_instruction_not_closed() === true) {
            return true;
        }
        return false;
    }
    
    function is_deletable(){
        // Suppression possible pour l'instructeur de la division si pas d'avis
        if ($this->is_dossier_instruction_not_closed() === true &&
            $this->is_instructeur_from_division_dossier() === true &&
            $this->getVal("avis_consultation") == "" &&
            $this->getVal('fichier') == ""
            ){
            return true;
        }
        
        // Si un avis a été rendu il faut que l'utilisateur ai le droit
        // specifique de suppression avec avis rendu et le bypass de division
        if ($this->getVal("avis_consultation") != "" or $this->getVal('fichier') != "") {
            
            if ($this->f->can_bypass("consultation", "supprimer") and
                $this->f->isAccredited("consultation_supprimer_avec_avis_rendu") == true){
                return true;
            }
        } else {
            // Si pas d'avis rendu, le bypass suffit
            if ($this->f->can_bypass("consultation", "supprimer")){
                return true;
            }
        }
        
        return false;
    }
    
    function is_multiaddable(){
        
        if ($this->f->can_bypass("consultation", "ajouter")){
            return true;
        }
        
        if ($this->is_instructeur_from_division_dossier() === true){
            return true;
        }
        return false;
    }
    
    function is_markable(){
        
        if($this->f->can_bypass("consultation", "modifier_lu")){
            return true;
        }
        
        if ($this->is_instructeur_from_division_dossier() === true){
            return true;
        }
        return false;
    }
    
    // Si la consultation est visible alors on peut afficher le lien pour la masquer
    function is_visible() {
        if($this->getVal("visible") == 't') {
            return true;
        }
        return false;
    }
    
    // Si la consultation n'est pas visible alors on peut afficher le lien pour l'afficher
    function is_not_visible() {
        if($this->getVal("visible") == 'f') {
            return true;
        }
        return false;
    }
    
    function is_finalizable(){
        
        if($this->f->can_bypass("consultation", "finaliser")){
            return true;
        }
        
        if ($this->is_instructeur_from_division_dossier() === true &&
            $this->is_dossier_instruction_not_closed() === true){
            return true;
        }
        
        return false;
    }
    
    function is_unfinalizable(){
        
        if($this->f->can_bypass("consultation", "definaliser")){
            return true;
        }
        
        if ($this->is_instructeur_from_division_dossier() === true &&
            $this->is_dossier_instruction_not_closed() === true){
            return true;
        }
        
        return false;
    }

    /*
     * CONDITION - can_show_or_hide_in_edition
     *
     * Vérifie que l'utilisateur possède la permission bypass.
     * Vérifie que l'utilisateur est un instructeur, qu'il fait partie de la
     * division du dossier et que le dossier n'est pas clôturé.
     *
     * Cette méthode est identique aux conditions dans consultation.inc.php
     * permettant l'affichage des actions depuis le listing des consultations.
     * Il est nécessaire que cette méthode ainsi que la condition if restent
     * concordants en tout point afin que le comportement des actions soit
     * identique depuis le formulaire et depuis le listing.
     *
     * @return boolean
     *
     */
    function can_show_or_hide_in_edition() {

        //
        if ($this->f->can_bypass("consultation", "visibilite_dans_edition") === true){
            return true;
        }
        
        // 
        if ($this->is_instructeur_from_division_dossier() === true and
            $this->is_dossier_instruction_not_closed() === true) {
            return true;
        }
        return false;
    }

    /*
     * CONDITION - can_user_access_dossier_contexte_ajout
     *
     * Vérifie que l'utilisateur a bien accès au dossier d'instruction passé dans le
     * formulaire d'ajout.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_ajout() {

        ($this->f->get_submitted_get_value('idxformulaire') !== null ? $id_dossier = 
            $this->f->get_submitted_get_value('idxformulaire') : $id_dossier = "");
        //
        if ($id_dossier !== "") {
            require_once "../obj/dossier_instruction.class.php";
            $dossier = new dossier_instruction($id_dossier, $f->db, DEBUG);
            //
            return $dossier->can_user_access_dossier();
        }
        return false;
    }

    /*
     * CONDITION - can_user_access_dossier_contexte_modification
     *
     * Vérifie que l'utilisateur a bien accès au dossier lié à la consultation.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_modification() {

        ($this->f->get_submitted_get_value('idxformulaire') !== null ? $id_dossier = 
            $this->f->get_submitted_get_value('idxformulaire') : $id_dossier = "");

        if ($id_dossier != "") {
            require_once "../obj/dossier.class.php";
            $dossier = new dossier($id_dossier, $f->db, DEBUG);
            //
            return $dossier->can_user_access_dossier();
        }
        return false;
    }

}// fin classe

?>
