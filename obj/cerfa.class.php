<?php
//$Id: cerfa.class.php 5856 2016-02-03 11:35:25Z stimezouaght $ 
//gen openMairie le 13/02/2013 14:41

require_once ("../gen/obj/cerfa.class.php");

class cerfa extends cerfa_gen {

    function cerfa($id, &$db = null, $debug = null) {
        $this->constructeur($id,$db,$debug);
    }// fin constructeur


    function setType(&$form,$maj) {
        parent::setType($form,$maj);


        //Ajout des select sur les tableaux
        if($maj == 0) {
            
            $form->setType('tab_surface', 'select');
            $form->setType('tab_surface2', 'select');
            $form->setType('tab_tax_su_princ', 'select');
            $form->setType('tab_tax_su_heber', 'select');
            $form->setType('tab_tax_su_secon', 'select');
            $form->setType('tab_tax_su_tot', 'select');
            $form->setType('tab_tax_su_non_habit_surf', 'select');
            $form->setType('tab_tax_su_parc_statio_expl_comm', 'select');
            $form->setType('tab_tax_am', 'select');
            $form->setType('tab_erp_eff', 'select');
        }elseif($maj == 1) {
                
            $form->setType('tab_surface', 'select');
            $form->setType('tab_surface2', 'select');
            $form->setType('tab_tax_su_princ', 'select');
            $form->setType('tab_tax_su_heber', 'select');
            $form->setType('tab_tax_su_secon', 'select');
            $form->setType('tab_tax_su_tot', 'select');
            $form->setType('tab_tax_su_non_habit_surf', 'select');
            $form->setType('tab_tax_su_parc_statio_expl_comm', 'select');
            $form->setType('tab_tax_am', 'select');
            $form->setType('tab_erp_eff', 'select');
        }elseif($maj == 2) {
                
            $form->setType('tab_surface', 'selecthiddenstatic');
            $form->setType('tab_surface2', 'selecthiddenstatic');
            $form->setType('tab_tax_su_princ', 'selecthiddenstatic');
            $form->setType('tab_tax_su_heber', 'selecthiddenstatic');
            $form->setType('tab_tax_su_secon', 'selecthiddenstatic');
            $form->setType('tab_tax_su_tot', 'selecthiddenstatic');
            $form->setType('tab_tax_su_non_habit_surf', 'selecthiddenstatic');
            $form->setType('tab_tax_su_parc_statio_expl_comm', 'selecthiddenstatic');
            $form->setType('tab_tax_am', 'selecthiddenstatic');
            $form->setType('tab_erp_eff', 'selecthiddenstatic');
        }elseif($maj == 3) {
                
            $form->setType('tab_surface', 'selecthiddenstatic');
            $form->setType('tab_surface2', 'selecthiddenstatic');
            $form->setType('tab_tax_su_princ', 'selecthiddenstatic');
            $form->setType('tab_tax_su_heber', 'selecthiddenstatic');
            $form->setType('tab_tax_su_secon', 'selecthiddenstatic');
            $form->setType('tab_tax_su_tot', 'selecthiddenstatic');
            $form->setType('tab_tax_su_non_habit_surf', 'selecthiddenstatic');
            $form->setType('tab_tax_su_parc_statio_expl_comm', 'selecthiddenstatic');
            $form->setType('tab_tax_am', 'selecthiddenstatic');
            $form->setType('tab_erp_eff', 'selecthiddenstatic');
        }
        $form->setType('co_statio_avt_shob', 'hidden');
        $form->setType('co_statio_apr_shob', 'hidden');
        $form->setType('co_statio_avt_surf', 'hidden');
        $form->setType('co_statio_apr_surf', 'hidden');
        $form->setType('co_trx_amgt', 'hidden');
        $form->setType('co_modif_aspect', 'hidden');
        $form->setType('co_modif_struct', 'hidden');
        $form->setType('co_trx_imm', 'hidden');
        $form->setType('co_cstr_shob', 'hidden');
        $form->setType('am_voyage_deb', 'hidden');
        $form->setType('am_voyage_fin', 'hidden');
        $form->setType('am_modif_amgt', 'hidden');
        $form->setType('am_lot_max_shob', 'hidden');
        $form->setType('mod_desc', 'hidden');
        $form->setType('tr_total', 'hidden');
        $form->setType('tr_partiel', 'hidden');
        $form->setType('tr_desc', 'hidden');
        $form->setType('avap_co_clot', 'hidden');
        $form->setType('avap_aut_coup_aba_arb', 'hidden');
        $form->setType('avap_ouv_infra', 'hidden');
        $form->setType('avap_aut_inst_mob', 'hidden');
        $form->setType('avap_aut_plant', 'hidden');
        $form->setType('avap_aut_auv_elec', 'hidden');
        $form->setType('tax_dest_loc_tr', 'hidden');
    }

    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        parent::setSelect($form, $maj,$db,$debug);

        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");
        
        // parametrage du tableau de surface
        $contenu=array();
        $contenu[0]=array_keys($tab_surface);
        $contenu[1]=array_values($tab_surface);
        $form->setSelect("tab_surface",$contenu);

        $contenu=array();
        $contenu[0]=array_keys($tab_surface2);
        $contenu[1]=array_values($tab_surface2);
        $form->setSelect("tab_surface2",$contenu);

        $contenu[0]=array_keys($tab_tax_su_princ);
        $contenu[1]=array_values($tab_tax_su_princ);
        $form->setSelect("tab_tax_su_princ",$contenu);

        $contenu[0]=array_keys($tab_tax_su_heber);
        $contenu[1]=array_values($tab_tax_su_heber);
        $form->setSelect("tab_tax_su_heber",$contenu);

        $contenu[0]=array_keys($tab_tax_su_secon);
        $contenu[1]=array_values($tab_tax_su_secon);
        $form->setSelect("tab_tax_su_secon",$contenu);

        $contenu[0]=array_keys($tab_tax_su_tot);
        $contenu[1]=array_values($tab_tax_su_tot);
        $form->setSelect("tab_tax_su_tot",$contenu);

        $contenu[0]=array_keys($tab_tax_su_non_habit_surf);
        $contenu[1]=array_values($tab_tax_su_non_habit_surf);
        $form->setSelect("tab_tax_su_non_habit_surf",$contenu);

        $contenu=array();
        $contenu[0]=array_keys($tab_tax_su_parc_statio_expl_comm);
        $contenu[1]=array_values($tab_tax_su_parc_statio_expl_comm);
        $form->setSelect("tab_tax_su_parc_statio_expl_comm",$contenu);

        $contenu[0]=array_keys($tab_tax_am);
        $contenu[1]=array_values($tab_tax_am);
        $form->setSelect("tab_tax_am",$contenu);

        $contenu=array();
        $contenu[0]=array_keys($tab_erp_eff);
        $contenu[1]=array_values($tab_erp_eff);
        $form->setSelect("tab_erp_eff", $contenu);
    }

    function setLayout(&$form, $maj) {

        /*Fieldset Parametrage du cerfa */
        $form->setBloc('cerfa','D',"","col_12");
            $form->setFieldset('cerfa','D'
                               ,_("Parametrage du cerfa"), "col_12");

            $form->setFieldset('om_validite_fin','F','');
        $form->setBloc('om_validite_fin','F');

        // Terrain

        $form->setBloc('terr_juri_titul','D',"","col_12 alignFormSpec");
            $form->setFieldset('terr_juri_titul','D'
                               ,_("Terrain"), "startClosed");

                $form->setBloc('terr_juri_titul','DF',_("Situation juridique du terrain"), "group");
                $form->setBloc('terr_juri_lot','DF',"", "group");
                $form->setBloc('terr_juri_zac','DF',"", "group");
                $form->setBloc('terr_juri_afu','DF',"", "group");
                $form->setBloc('terr_juri_pup','DF',"", "group");
                $form->setBloc('terr_juri_oin','DF',"", "group");
                $form->setBloc('terr_juri_desc','DF',"", "group");
                $form->setBloc('terr_div_surf_etab','DF',_("Terrain issu d'une division de propriete"), "group");
                $form->setBloc('terr_div_surf_av_div','DF',"", "group");

            $form->setFieldset('terr_div_surf_av_div','F','');
            
        $form->setBloc('terr_div_surf_av_div','F');

        // Description de la demande / du projet
        $form->setFieldset('ope_proj_desc', 'D',
            _("Description de la demande / du projet"), "col_12 startClosed");
            $form->setBloc('ope_proj_desc', 'DF', "", "group");
            $form->setBloc('ope_proj_div_co', 'DF', "", "group");
            $form->setBloc('ope_proj_div_contr', 'DF', "", "group");
        $form->setFieldset('ope_proj_div_contr', 'F');

        // Construire, aménager ou modifier un ERP
        $form->setBloc('erp_class_cat','D',"","col_12");
            $form->setFieldset('erp_class_cat','D'
                               ,_("Construire, amenager ou modifier un ERP"), "startClosed alignFormSpec");

            $form->setBloc('erp_class_cat','DF', _("Activite"),"group");
            $form->setBloc('erp_class_type','DF', "","group");

            $form->setBloc('erp_cstr_neuve','DF', _("Nature des travaux (plusieurs cases possibles)"),"group");
            $form->setBloc('erp_trvx_acc','DF', "","group");
            $form->setBloc('erp_extension','DF', "","group");
            $form->setBloc('erp_rehab','DF', "","group");
            $form->setBloc('erp_trvx_am','DF', "","group");
            $form->setBloc('erp_vol_nouv_exist','DF', "","group");

            $form->setBloc('tab_erp_eff','DF', _("Effectif"),"group");

            $form->setFieldset('tab_erp_eff','F', '');
        $form->setBloc('tab_erp_eff','F');

        // Aménager
        $form->setBloc('am_lotiss','D',"","col_12");
            $form->setFieldset('am_lotiss','D'
                                       ,_("Amenager"), "startClosed");
                $form->setBloc('am_lotiss','D',"","col_12");
                    $form->setFieldset('am_lotiss','D'
                                       ,_("Projet d'amenagement"), "startClosed alignFormSpec");
                        // bloc 4.1
                        $form->setBloc('am_lotiss','DF',_("Nature des travaux, instalations
                                       ou amenagements envisages"), "group");
                        $form->setBloc('am_div_mun','DF',"", "group");
                        $form->setBloc('am_autre_div','DF',"", "group");
                        $form->setBloc('am_camping','DF',"", "group");
                        $form->setBloc('am_parc_resid_loi','DF',"", "group");
                        $form->setBloc('am_sport_moto','DF',"", "group");
                        $form->setBloc('am_sport_attrac','DF',"", "group");
                        $form->setBloc('am_sport_golf','DF',"", "group");
                        $form->setBloc('am_caravane','DF',"", "group");
                        $form->setBloc('am_carav_duree','DF',"", "group");
                        $form->setBloc('am_statio','DF',"", "group");
                        $form->setBloc('am_statio_cont','DF',"", "group");
                        $form->setBloc('am_affou_exhau','DF',"", "group");
                        $form->setBloc('am_affou_exhau_sup','DF',"", "group");
                        $form->setBloc('am_affou_prof','DF',"", "group");
                        $form->setBloc('am_exhau_haut','DF',"", "group");
                        $form->setBloc('am_terr_res_demon','DF',"", "group");
                        $form->setBloc('am_air_terr_res_mob','DF',"", "group");
                        
                        $form->setBloc('am_chem_ouv_esp','D',_("Dans les secteurs proteges :"),"col_12");
                            $form->setBloc('am_chem_ouv_esp','DF',_("Amenagement situe dans un espace remarquable :"), "group");
                            $form->setBloc('am_agri_peche','DF',"", "group");
                            
                            $form->setBloc('am_crea_voie','DF',_("Amenagement situe dans un secteur sauvegarde :"), "group");
                            $form->setBloc('am_modif_voie_exist','DF',"", "group");
                            $form->setBloc('am_crea_esp_sauv','DF',"", "group");
                            
                            $form->setBloc('am_crea_esp_class','DF',_("Amenagement situe dans un site classe ou une reserve naturelle 1 :"), "group");
                            $form->setBloc('am_coupe_abat','DF',"", "group");
                            $form->setBloc('am_prot_plu','DF',"", "group");
                            $form->setBloc('am_prot_muni','DF',"", "group");
                            $form->setBloc('am_mobil_voyage','DF',"", "group");
                            $form->setBloc('am_aire_voyage','DF',"", "group");
                            $form->setBloc('am_rememb_afu','DF',"", "group");
                            $form->setBloc('co_ouvr_infra','DF',"", "group");
                        $form->setBloc('co_ouvr_infra','F');

                        $form->setBloc('am_mob_art','DF',_("Dans un secteur sauvegarde, site classe ou reserve naturelle :"), "group");
                        $form->setBloc('am_modif_voie_esp','DF',"", "group");
                        $form->setBloc('am_plant_voie_esp','DF',"", "group");
                        $form->setBloc('co_ouvr_elec','DF',"", "group");
                    $form->setFieldset('co_ouvr_elec','F','');
                $form->setBloc('co_ouvr_elec','F');
                $form->setBloc('am_projet_desc','D',"","col_12");
                    $form->setFieldset('am_projet_desc','D'
                                       ,_("Description amenagement"), "startClosed alignFormSpec");
                                       
                        $form->setBloc('am_projet_desc','DF',"", "group");
                        $form->setBloc('am_terr_surf','DF',"", "group");
                        $form->setBloc('am_tranche_desc','DF',"", "group");
                        
                    $form->setFieldset('am_tranche_desc','F','');
                $form->setBloc('am_tranche_desc','F');
                $form->setBloc('am_lot_max_nb','D',"","col_12");
                    $form->setFieldset('am_lot_max_nb','D'
                                       ,_("Complement d'amenagement"), "startClosed");
                        // bloc 4.2
                        $form->setBloc('am_lot_max_nb','D',_("Demande concernant un lotissement"),"col_12 alignFormSpec");
                        
                            $form->setBloc('am_lot_max_nb','DF',"", "group");
                            $form->setBloc('am_lot_max_shon','DF',"", "group");
                        
                            $form->setBloc('am_lot_cstr_cos','DF',_("Comment la constructibilite globale sera-t-elle repartie ?"), "group");
                            $form->setBloc('am_lot_cstr_plan','DF',"", "group");
                            $form->setBloc('am_lot_cstr_vente','DF',"", "group");
                            $form->setBloc('am_lot_fin_diff','DF',"", "group");
                            
                            $form->setBloc('am_lot_consign','DF',_("si oui, quelle garantie sera utilisee ?"), "group");
                            $form->setBloc('am_lot_gar_achev','DF',"", "group");
                            $form->setBloc('am_lot_vente_ant','DF',"", "group");
                        $form->setBloc('am_lot_vente_ant','F');

                        // bloc 4.3
                        $form->setBloc('am_exist_agrand','D',_("Amenagement d'un camping ou
                                       d'un terrain amenage en vue de l'hebergement
                                       touristique"),"col_12");
                            $form->setBloc('am_exist_agrand','D',"", "alignFormSpec");
                                $form->setBloc('am_exist_agrand','DF',"", "group");
                                $form->setBloc('am_exist_date','DF',"", "group");
                                $form->setBloc('am_exist_num','DF',"", "group");
                                $form->setBloc('am_exist_nb_avant','DF',"", "group");
                                $form->setBloc('am_exist_nb_apres','DF',"", "group");
                                $form->setBloc('am_empl_nb','DF',"", "group");
                            $form->setBloc('am_empl_nb','F',"", "");

                            $form->setBloc('am_tente_nb','D',_("Nombre maximum d’emplacements reserves aux :"), "alignForm");
                            $form->setBloc('am_mobil_nb','F',"", "");
                            
                            $form->setBloc('am_pers_nb','DF',"", "alignFormSpec group");
                            
                            $form->setBloc('am_empl_hll_nb','D',_("Implantation d’habitations legeres de loisirs (HLL) :"), "alignFormSpec");
                                $form->setBloc('am_empl_hll_nb','DF',"", "group");
                                $form->setBloc('am_hll_shon','DF',"", "group");
                                $form->setBloc('am_periode_exploit','DF',"", "group");
                            $form->setBloc('am_periode_exploit','F',"", "");

                            $form->setBloc('am_coupe_bois','D',_("Declaration de coupe et/ou abattage d’arbres :"),"col_12 cerfasubtitle");
                                
                                $form->setBloc('am_coupe_bois','D',_("Courte description du lieu :"), "cerfasubtitle alignForm");
                                $form->setBloc('am_coupe_align','F',"", "");
                                
                                $form->setBloc('am_coupe_ess','D',_("Nature du boisement :"),"col_12 cerfasubtitle alignForm");
                                $form->setBloc('am_coupe_autr','F');
                            $form->setBloc('am_coupe_autr','F');
                        $form->setBloc('am_coupe_autr','F');


                    $form->setFieldset('am_coupe_autr','F','');
                
                $form->setBloc('am_coupe_autr','F');
            $form->setFieldset('am_coupe_autr','F','');
        $form->setBloc('am_coupe_autr','F');
        // Fin amménager
        // Construire
        $form->setBloc('co_archi_recours','D',"","col_12");
            $form->setFieldset('co_archi_recours','D'
                               ,_("Construire"), "startClosed");
                $form->setBloc('co_archi_recours','D', "","col_12");
                    $form->setFieldset('co_archi_recours','D'
                               ,_("Projet construction"), "startClosed alignFormSpec");
                         
                        $form->setBloc('co_archi_recours','DF',_("Architecte"), "group");
                        $form->setBloc('architecte','DF',"", "group");
                        
                        $form->setBloc('co_cstr_nouv','DF',_("Nature du projet"), "group");
                        $form->setBloc('avap_co_elt_pro','DF',"", "group");
                        $form->setBloc('avap_nouv_haut_surf','DF',"", "group");
                        $form->setBloc('co_cstr_exist','DF',"", "group");
                        $form->setBloc('co_div_terr','DF',"", "group");
                        $form->setBloc('co_cloture','DF',"", "group");
                        $form->setBloc('co_projet_desc','DF',"", "group");
                        $form->setBloc('co_elec_tension','DF',"", "group");
                    $form->setFieldset('co_elec_tension','F','');
                    $form->setFieldset('co_tot_log_nb','D'
                               ,_("Complement construction"), "startClosed");

                        $form->setBloc('co_tot_log_nb','D',"", "alignForm");
                        $form->setBloc('co_tot_coll_nb','F',"", "");
                        
                        $form->setBloc('co_mais_piece_nb','D',"", "alignForm");
                        $form->setBloc('co_mais_niv_nb','F',"", "");
                        
                        $form->setBloc('co_fin_lls_nb','D', _("Repartition du nombre total de logement crees par type de financement :"),"col_12");
                            $form->setBloc('co_fin_lls_nb','D',"", "alignForm");
                            $form->setBloc('co_fin_autr_nb','F',"", "");
                            
                            $form->setBloc('co_fin_autr_desc','DF',"", "alignFormSpec group");
                            $form->setBloc('co_mais_contrat_ind','DF',"", "alignFormSpec group");
                        $form->setBloc('co_mais_contrat_ind','F');
                        
                        $form->setBloc('co_uti_pers','D',_("Mode d'utilisation principale des logements :"), "col_12");
                            $form->setBloc('co_uti_pers','D', "", "alignForm");
                            $form->setBloc('co_uti_loc','F',"", "");
                        $form->setBloc('co_uti_loc','F',"", "");
                        
                        $form->setBloc('co_uti_princ','D',_("S’il s’agit d’une occupation personnelle, veuillez preciser :"), "col_12");
                            $form->setBloc('co_uti_princ','D',"", "alignForm");
                            $form->setBloc('co_uti_secon','F',"", "");
                        $form->setBloc('co_uti_secon','F',"", "group");

                        $form->setBloc('co_anx_pisc','D',_("Si le projet porte sur une annexe a l’habitation, veuillez preciser :"), "col_12");
                            $form->setBloc('co_anx_pisc','D',"", "alignForm");
                            $form->setBloc('co_anx_autr_desc','F',"", "");
                        $form->setBloc('co_anx_autr_desc','F',"", "group");
                        
                        $form->setBloc('co_resid_agees','D',_("Si le projet est un foyer ou une residence, a quel titre :"), "col_12");
                            $form->setBloc('co_resid_agees','D',"", "alignForm");
                            $form->setBloc('co_resid_hand','F',"", "");
                        $form->setBloc('co_resid_hand','F',"", "group");
                        
                        $form->setBloc('co_resid_autr','D',"", "alignFormSpec");
                        $form->setBloc('co_foyer_chamb_nb','F',"", "");
                        
                        $form->setBloc('co_log_1p_nb','D',_("Repartition du nombre de logements crees selon le nombre de pieces :"), "col_12");
                            $form->setBloc('co_log_1p_nb','D', "", "alignForm");
                            $form->setBloc('co_log_6p_nb','F',"", "group");
                        $form->setBloc('co_log_6p_nb','F',"", "group");
                        
                        $form->setBloc('co_bat_niv_nb','DF',"", "alignFormSpec");
                        
                        $form->setBloc('co_trx_exten','D',_("Indiquez si vos travaux comprennent notamment :"), "col_12");
                            $form->setBloc('co_trx_exten','D',"", "alignForm");
                            $form->setBloc('co_trx_nivsup','F',"", "group");

                            $form->setBloc('co_demont_periode','DF', _("Construction periodiquement demontee et re-installee :"),"alignFormSpec group");

                        $form->setBloc('co_demont_periode','F',"", "group");
                    $form->setFieldset('co_demont_periode','F','');

                    $form->setFieldset('tab_surface','D'
                               ,_("Destinations et surfaces des constructions"), "startClosed");
                        $form->setBloc('tab_surface','DF', _("Destination des constructions et tableau des surfaces (uniquement à remplir si votre projet de construction est situé dans une commune couverte par un plan local d’urbanisme ou un document en tenant lieu appliquant l’article R.123-9 du code de l’urbanisme dans sa rédaction antérieure au 1er janvier 2016)."),"alignFormSpec group");

                        $form->setBloc('co_sp_transport','D', _("Destination des constructions futures en cas de realisation au benefice d'un service public ou d'interet collectif :"),"col_12");
                            $form->setBloc('co_sp_transport','D', "","alignForm");
                            $form->setBloc('co_sp_culture','F', "","");
                        $form->setBloc('co_sp_culture','F', "","");

                        $form->setBloc('tab_surface2','DF', _("Destination, sous-destination des constructions et tableau des surfaces (uniquement à remplir si votre projet de construction est situé dans une commune couverte par le règlement national d’urbanisme, une carte communale ou dans une commune non visée à la rubrique précédente"),"alignFormSpec group");
                    $form->setFieldset('tab_surface2','F','');

                    $form->setFieldset('co_statio_avt_nb','D'
                               ,_("Divers construction"), "startClosed");
                        $form->setBloc('co_statio_avt_nb','D', _("Nombre de places de stationnement"),"col_12");
                            $form->setBloc('co_statio_avt_nb','D', "","alignForm");
                            $form->setBloc('co_statio_apr_nb','F', "","");
                        $form->setBloc('co_statio_apr_nb','F', "","");
                        
                        $form->setBloc('co_statio_adr','D', _("Places de stationnement affectees au projet, amenagees ou reservees en dehors du terrain sur lequel est situe le projet"),"col_12");
                            $form->setBloc('co_statio_adr','DF', "","alignFormSpec");
                            
                            $form->setBloc('co_statio_place_nb','D', "","col_12");
                                $form->setBloc('co_statio_place_nb','D', "","alignForm");
                                $form->setBloc('co_statio_tot_shob','F', "","");
                            $form->setBloc('co_statio_tot_shob','F', "","");
                        $form->setBloc('co_statio_tot_shob','F');
                        $form->setBloc('co_statio_comm_cin_surf','D', _("Pour les commerces et cinemas :"),"col_12 alignFormSpec");
                        $form->setBloc('co_perf_energ','F',"", "");
                    $form->setFieldset('co_perf_energ','F','');
                $form->setBloc('co_perf_energ','F');

            $form->setFieldset('co_perf_energ','F','');
        
        $form->setBloc('co_perf_energ','F');
        // Fin construire

        /*Fieldset n°6 Projet necessitant demolitions */
        $form->setBloc('dm_constr_dates','D',"","col_12");
            $form->setFieldset('dm_constr_dates','D'
                               ,_("Demolir"), "startClosed alignFormSpec");
                
                $form->setBloc('dm_constr_dates','DF', "","group");
                $form->setBloc('dm_total','DF', "","group");
                $form->setBloc('dm_partiel','DF', "","group");
                $form->setBloc('dm_projet_desc','DF', "","group");
                $form->setBloc('dm_tot_log_nb','DF', "","group");
            $form->setFieldset('dm_tot_log_nb','F','');
        
        $form->setBloc('dm_tot_log_nb','F');

        /*Fieldset n°4 Ouverture de chantier */
        $form->setBloc('doc_date','D',"","col_12");
            $form->setFieldset('doc_date','D'
                               ,_("Ouverture de chantier"), "startClosed alignFormSpec");
                $form->setBloc('doc_date','DF', "","group");
                $form->setBloc('doc_tot_trav','DF', "","group");
                $form->setBloc('doc_tranche_trav','DF', "","group");
                $form->setBloc('doc_tranche_trav_desc','DF', "","group");
                $form->setBloc('doc_surf','DF', "","group");
                $form->setBloc('doc_nb_log','DF', "","group");
                $form->setBloc('doc_nb_log_indiv','DF', "","group");
                $form->setBloc('doc_nb_log_coll','DF', "","group");
                //
                $form->setBloc('doc_nb_log_lls','DF', _("Repartition du nombre de logements commences par type de financement"), "group");
                $form->setBloc('doc_nb_log_aa','DF', "","group");
                $form->setBloc('doc_nb_log_ptz','DF', "","group");
                $form->setBloc('doc_nb_log_autre','DF', "","group");
            $form->setFieldset('doc_nb_log_autre','F','');
        $form->setBloc('doc_nb_log_autre','F');

        /*Fieldset n°4  Achèvement des travaux */
        $form->setBloc('daact_date','D',"","col_12");
            $form->setFieldset('daact_date','D'
                               ,_("Achevement des travaux") , "startClosed alignFormSpec");
                $form->setBloc('daact_date','DF', "","group");
                $form->setBloc('daact_date_chgmt_dest','DF', "","group");
                $form->setBloc('daact_tot_trav','DF', "","group");
                $form->setBloc('daact_tranche_trav','DF', "","group");
                $form->setBloc('daact_tranche_trav_desc','DF', "","group");
                $form->setBloc('daact_surf','DF', "","group");
                $form->setBloc('daact_nb_log','DF', "","group");
                $form->setBloc('daact_nb_log_indiv','DF', "","group");
                $form->setBloc('daact_nb_log_coll','DF', "","group");
                //
                $form->setBloc('daact_nb_log_lls','DF', _("Repartition du nombre de logements commences par type de financement"), "group");
                $form->setBloc('daact_nb_log_aa','DF', "","group");
                $form->setBloc('daact_nb_log_ptz','DF', "","group");
                $form->setBloc('daact_nb_log_autre','DF', "","group");
            $form->setFieldset('daact_nb_log_autre','F','');
        $form->setBloc('daact_nb_log_autre','F');

        // Début DIA
        $form->setBloc('dia_imm_non_bati','D',"","col_12");
        $form->setFieldset('dia_imm_non_bati','D',_("Déclaration d’intention d’aliéner un bien"), "startClosed");

            // Désignation du bien
            $form->setFieldset('dia_imm_non_bati','D',_("Désignation du bien"), "startClosed");

                // Immeuble
                $form->setBloc('dia_imm_non_bati','D', _("Immeuble"),"alignForm group");
                $form->setBloc('dia_imm_bati_terr_autr_desc','F', "","");

                // Occupation du sol en superficie
                $form->setBloc('dia_occ_sol_su_terre','D', _("Occupation du sol en superficie (m²)"),"alignForm group");
                $form->setBloc('dia_occ_sol_su_sol','F', "","");

                //
                $form->setBloc('dia_bati_vend_tot','D', " ","alignForm group");
                $form->setBloc('dia_bati_vend_tot_txt','F', "","");
                $form->setBloc('dia_su_co_sol','D', "","alignForm group");
                $form->setBloc('dia_su_util_hab','F', "","");
                $form->setBloc('dia_nb_niv','D', _("Nombre de"),"alignForm group");
                $form->setBloc('dia_nb_autre_loc','F', "","");
                $form->setBloc('dia_vente_lot_volume','D', " ","alignForm group");
                $form->setBloc('dia_vente_lot_volume_txt','F', "","");

                // Copropriété
                $form->setBloc('dia_bat_copro','D', " ","alignForm group");
                $form->setBloc('dia_bat_copro_desc','F', "","");
                // Tableau lot
                $form->setBloc('dia_lot_numero','D', "","alignForm group");
                $form->setBloc('dia_lot_nat_su','F', "","");
                $form->setBloc('dia_lot_bat_achv_plus_10','D', _("Le bâtiment est achevé depuis"),"alignForm group");
                $form->setBloc('dia_lot_bat_achv_moins_10','F', "","");
                $form->setBloc('dia_lot_regl_copro_publ_hypo_plus_10','D', _("Le réglement de copropriété a été publié aux hypothèques depuis"),"alignForm group");
                $form->setBloc('dia_lot_regl_copro_publ_hypo_moins_10','F', "","");

                //
                $form->setBloc('dia_indivi_quote_part','DF', " ","alignFormSpec");
                $form->setBloc('dia_design_societe','DF', _("Droits sociaux"),"alignFormSpec group");
                $form->setBloc('dia_design_droit','DF', "","alignFormSpec");
                $form->setBloc('dia_droit_soc_nat','D', "","alignForm group");
                $form->setBloc('dia_droit_soc_num_part','F', "","");

            $form->setFieldset('dia_droit_soc_num_part','F','');

            // Usage et occupation
            $form->setFieldset('dia_us_hab','D',_("Usage et occupation"), "startClosed");

                // Usage
                $form->setBloc('dia_us_hab','D', _("Usage"),"alignForm group");
                $form->setBloc('dia_us_autre_prec','F', "","");

                // Occupation
                $form->setBloc('dia_occ_prop','D', _("Occupation"),"alignForm group");
                $form->setBloc('dia_occ_autre_prec','F', "","");

            $form->setFieldset('dia_occ_autre_prec','F','');

            // Droits réels ou personnels
            $form->setFieldset('dia_droit_reel_perso_grevant_bien_oui','D',_("Droits réels ou personnels"), "startClosed");

                //
                $form->setBloc('dia_droit_reel_perso_grevant_bien_oui','D', _("Grevant les biens"),"alignForm group");
                $form->setBloc('dia_droit_reel_perso_grevant_bien_non','F', "","");

                //
                $form->setBloc('dia_droit_reel_perso_nat','D', " ","alignForm group");
                $form->setBloc('dia_droit_reel_perso_viag','F', "","");

            $form->setFieldset('dia_droit_reel_perso_viag','F','');

            // Modalités de la cession
            $form->setFieldset('dia_mod_cess_prix_vente','D',_("Modalités de la cession"), "startClosed");

                // Vente amiable
                $form->setFieldset('dia_mod_cess_prix_vente','D', _("Vente amiable"), "startClosed");

                $form->setBloc('dia_mod_cess_prix_vente','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_prix_vente_mob','D', _("Dont éventuellement inclus"),"alignForm group");
                $form->setBloc('dia_mod_cess_prix_vente_autre','F', "","");

                //
                $form->setBloc('dia_mod_cess_adr','DF', _("Si vente indissociable d'autres biens"),"alignFormSpec");

                // Modalité de paiement
                $form->setBloc('dia_mod_cess_sign_act_auth','D', _("Modalités de paiement"),"alignForm group");
                $form->setBloc('dia_mod_cess_terme_prec','F', "","");
                $form->setBloc('dia_mod_cess_commi','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_commi_ht','F', "","");
                $form->setBloc('dia_mod_cess_bene_acquereur','D', _("Bénéficiaire"),"alignForm group");
                $form->setBloc('dia_mod_cess_bene_vendeur','F', "","");
                $form->setBloc('dia_mod_cess_paie_nat','DF', " ","alignFormSpec group");
                $form->setBloc('dia_mod_cess_design_contr_alien','DF', "","alignFormSpec group");
                $form->setBloc('dia_mod_cess_eval_contr','DF', "","alignFormSpec group");
                $form->setBloc('dia_mod_cess_rente_viag','DF', "","alignFormSpec group");
                $form->setBloc('dia_mod_cess_mnt_an','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_mnt_compt','F', "","");
                $form->setBloc('dia_mod_cess_bene_rente','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_droit_usa_hab','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_droit_usa_hab_prec','F', "","");
                $form->setBloc('dia_mod_cess_eval_usa_usufruit','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_vente_nue_prop','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_vente_nue_prop_prec','F', "","");
                $form->setBloc('dia_mod_cess_echange','DF', "","alignFormSpec group");
                $form->setBloc('dia_mod_cess_design_bien_recus_ech','DF', "","alignFormSpec group");
                $form->setBloc('dia_mod_cess_mnt_soulte','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_prop_contre_echan','F', "","");
                $form->setBloc('dia_mod_cess_apport_societe','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_bene','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_esti_bien','F', "","");
                $form->setBloc('dia_mod_cess_cess_terr_loc_co','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_esti_terr','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_esti_loc','F', "","");
                $form->setBloc('dia_mod_cess_esti_imm_loca','DF', "","alignFormSpec");

                $form->setFieldset('dia_mod_cess_esti_imm_loca','F','');

                // Adjudication
                $form->setFieldset('dia_mod_cess_adju_vol','D', _("Adjudication"), "startClosed");

                $form->setBloc('dia_mod_cess_adju_vol','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_adju_obl','F', "","");
                $form->setBloc('dia_mod_cess_adju_fin_indivi','DF', "","alignFormSpec");
                $form->setBloc('dia_mod_cess_adju_date_lieu','D', "","alignForm group");
                $form->setBloc('dia_mod_cess_mnt_mise_prix','F', "","");

                $form->setFieldset('dia_mod_cess_mnt_mise_prix','F','');

            $form->setFieldset('dia_mod_cess_mnt_mise_prix','F','');

            // Les soussignés déclarent
            $form->setFieldset('dia_prop_titu_prix_indique','D',_("Les soussignés déclarent"), "startClosed");

                //
                $form->setBloc('dia_prop_titu_prix_indique','DF', _("Que le(s) propriétaire(s) nommé(s) à la rubrique 1"),"alignFormSpec group");
                $form->setBloc('dia_prop_recherche_acqu_prix_indique','DF', "","alignFormSpec group");
                $form->setBloc('dia_acquereur_nom_prenom','DF', "","alignFormSpec group");
                $form->setBloc('dia_acquereur_prof','DF', "","alignFormSpec group");

                // Adresse
                $form->setBloc('dia_acquereur_adr_num_voie','D', _("Adresse"),"alignForm group");
                $form->setBloc('dia_acquereur_adr_localite','F', "","");

                //
                $form->setBloc('dia_indic_compl_ope','DF', " ","alignFormSpec group");
                $form->setBloc('dia_vente_adju','DF', "","alignFormSpec groupe");

            $form->setFieldset('dia_vente_adju','F','');

            // Observation
            $form->setFieldset('dia_observation','D',_("Observations"), "startClosed");

                //
                $form->setBloc('dia_observation','DF', "","alignFormSpec group");

            $form->setFieldset('dia_observation','F','');

        $form->setFieldset('dia_observation','F','');
        $form->setBloc('dia_observation','F',"","");
        // Fin DIA

        $form->setBloc('code_cnil','D',"","col_12");
            $form->setFieldset('code_cnil','D'
                               ,_("cnil (opposition à l’utilisation des informations du formulaire à des fins commerciales)") , "startClosed alignFormSpec");
                $form->setBloc('code_cnil','DF', "","group");
            $form->setFieldset('code_cnil','F','');
        $form->setBloc('code_cnil','F');

        $form->setBloc('tax_surf_tot_cstr','D',"","col_12");
            $form->setFieldset('tax_surf_tot_cstr','D'
                               ,_("Declaration des elements necessaires au calcul des impositions"), "startClosed alignFormSpec");
                
                $form->setBloc('tax_surf_tot_cstr','DF', _("Renseignement"),"group");
                $form->setBloc('tax_surf_tot','DF', "","group");
                $form->setBloc('tax_surf','DF', "","group");
                $form->setBloc('tax_surf_suppr_mod','DF', "","group");
                
                $form->setBloc('tab_tax_su_princ','DF', _("Creation de locaux destines a l’habitation :"),"group");
                $form->setBloc('tab_tax_su_secon','DF', "","group");
                $form->setBloc('tab_tax_su_heber','DF', "","group");
                $form->setBloc('tab_tax_su_tot','DF', "","group");
                //
                $form->setBloc('tax_ext_pret','DF', _("Extension de l’habitation principale, creation d’un batiment annexe a cette habitation ou d’un garage clos et couvert."), "group");
                $form->setBloc('tax_ext_desc','DF', "","group");
                $form->setBloc('tax_surf_tax_exist_cons','DF', "","group");
                $form->setBloc('tax_log_exist_nb','DF', "","group");
                $form->setBloc('tax_log_ap_trvx_nb','DF', "", "group");

                //
                $form->setBloc('tax_surf_abr_jard_pig_colom','DF', _("Creation d’abris de jardin, de pigeonniers et colombiers"), "group");

                $form->setBloc('tax_comm_nb','DF', _("Creation ou extension de locaux non destines a l'habitation :"),"group");
                $form->setBloc('tab_tax_su_non_habit_surf','DF', "","group");
                $form->setBloc('tab_tax_am','DF', "","group");
                $form->setBloc('tab_tax_su_parc_statio_expl_comm', 'DF', "", "group");
                $form->setBloc('tax_su_non_habit_abr_jard_pig_colom', 'DF', "", "group");

                $form->setBloc('tax_am_statio_ext_cr','D', _("Autres elements crees soumis à la taxe d’amenagement :"),"col_12");
                $form->setBloc('tax_pann_volt_sup_cr','F');

                $form->setBloc('tax_surf_loc_arch','D', _("Redevance d’archeologie preventive"),"col_12 alignFormSpec");
                    $form->setBloc('tax_surf_loc_arch','D', _("Veuillez preciser la profondeur du(des) terrassement(s) necessaire(s) a la realisation de votre projet (en mètre)"),"");
                    $form->setBloc('tax_eol_haut_nb_arch','F');
                $form->setBloc('tax_eol_haut_nb_arch','F');

                $form->setBloc('tax_trx_presc_ppr','DF', _("Cas particuliers"),"group");
                $form->setBloc('tax_monu_hist','DF', "","group");

                $form->setBloc('vsd_surf_planch_smd','DF', _("Versement pour sous-densite (VSD)"),"group");
                $form->setBloc('vsd_unit_fonc_sup','DF', "","group");
                $form->setBloc('vsd_unit_fonc_constr_sup','DF', "","group");
                $form->setBloc('vsd_val_terr','DF', "","group");
                $form->setBloc('vsd_const_sxist_non_dem_surf','DF', "","group");
                $form->setBloc('vsd_rescr_fisc','DF', "","group");
                
                $form->setBloc('pld_val_terr','DF', _("Plafond legal de densite (PLD)"),"group");
                $form->setBloc('pld_const_exist_dem','DF', "","group");
                $form->setBloc('pld_const_exist_dem_surf','DF', "","group");

                $form->setBloc('tax_desc','DF', _("Autres renseignements"),"col_12 alignFormSpec");

                $form->setBloc('exo_ta_1','D',"","col_12");
                    $form->setFieldset('exo_ta_1','D',_("Exonérations"), "startClosed alignFormSpec");

                    $form->setBloc('exo_ta_1','D',"","col_12");
                        $form->setFieldset('exo_ta_1','D',_("Taxe d'aménagement"), "collapsible alignFormSpec");

                        $form->setBloc('exo_ta_1','DF', _("Exonérations de plein droit"),"col_12 group");
                        $form->setBloc('exo_ta_2','DF', "","group");
                        $form->setBloc('exo_ta_3','DF', "","group");
                        $form->setBloc('exo_ta_4','DF', "","group");
                        $form->setBloc('exo_ta_5','DF', "","group");
                        $form->setBloc('exo_ta_6','DF', "","group");
                        $form->setBloc('exo_ta_7','DF', "","group");
                        $form->setBloc('exo_ta_8','DF', "","group");
                        $form->setBloc('exo_ta_9','DF', "","group");

                        $form->setBloc('exo_facul_1','DF', _("Exonérations facultatives"),"col_12 group");
                        $form->setBloc('exo_facul_2','DF', "","group");
                        $form->setBloc('exo_facul_3','DF', "","group");
                        $form->setBloc('exo_facul_4','DF', "","group");
                        $form->setBloc('exo_facul_5','DF', "","group");
                        $form->setBloc('exo_facul_6','DF', "","group");
                        $form->setBloc('exo_facul_7','DF', "","group");
                        $form->setBloc('exo_facul_8','DF', "","group");
                        $form->setBloc('exo_facul_9','DF', "","group");

                        $form->setBloc('mtn_exo_ta_part_commu','DF', _("Montants des exonérations"),"col_12 group");
                        $form->setBloc('mtn_exo_ta_part_depart','DF', "","group");
                        $form->setBloc('mtn_exo_ta_part_reg','DF', "","group");

                        $form->setFieldset('mtn_exo_ta_part_reg','F','');
                    $form->setBloc('mtn_exo_ta_part_reg','F');

                    $form->setBloc('exo_rap_1','D',"","col_12");
                        $form->setFieldset('exo_rap_1','D',_("Redevance d'archéologie préventive"), "collapsible alignFormSpec");

                        $form->setBloc('exo_rap_1','DF', _("Exonérations"),"col_12 group");
                        $form->setBloc('exo_rap_2','DF', "","group");
                        $form->setBloc('exo_rap_3','DF', "","group");
                        $form->setBloc('exo_rap_4','DF', "","group");
                        $form->setBloc('exo_rap_5','DF', "","group");
                        $form->setBloc('exo_rap_6','DF', "","group");
                        $form->setBloc('exo_rap_7','DF', "","group");
                        $form->setBloc('exo_rap_8','DF', "","group");

                        $form->setBloc('mtn_exo_rap','DF', _("Montant de l'exonération"),"col_12 group");

                        $form->setFieldset('mtn_exo_rap','F','');
                    $form->setBloc('mtn_exo_rap','F');

                $form->setFieldset('mtn_exo_rap','F','');
            $form->setBloc('mtn_exo_rap','F');

            $form->setFieldset('mtn_exo_rap','F','');
        $form->setBloc('mtn_exo_rap','F');

        // Début contentieux
        $form->setBloc('ctx_objet_recours','D',"","col_12");
        $form->setFieldset('ctx_objet_recours','D',_("Contentieux"), "startClosed alignFormSpec");

                //
                $form->setBloc('ctx_objet_recours','DF', "","group");
                $form->setBloc('ctx_moyen_souleve','DF', "","group");
                $form->setBloc('ctx_moyen_retenu_juge','DF', "","group");
                $form->setBloc('ctx_reference_sagace','DF', "","group");
                $form->setBloc('ctx_nature_travaux_infra_om_html','DF', "","group");
                $form->setBloc('ctx_synthese_nti','DF', "","group");
                $form->setBloc('ctx_article_non_resp_om_html','DF', "","group");
                $form->setBloc('ctx_synthese_anr','DF', "","group");
                $form->setBloc('ctx_reference_parquet','DF', "","group");
                $form->setBloc('ctx_element_taxation','DF', "","group");
                $form->setBloc('ctx_infraction','DF', "","group");
                $form->setBloc('ctx_regularisable','DF', "","group");
                $form->setBloc('ctx_reference_courrier','DF', "","group");
                $form->setBloc('ctx_date_audience','DF', "","group");
                $form->setBloc('ctx_date_ajournement','DF', "","group");

        $form->setFieldset('ctx_date_ajournement','F','');
        $form->setBloc('ctx_date_ajournement','F');
        // Fin contentieux

        // Début droit de préemption commercial
        $form->setBloc('dpc_type','D',"","col_12");
        $form->setFieldset('dpc_type','D',_("Droit de préemption commercial"), "startClosed alignFormSpec");

            // Description du bien
            $form->setBloc('dpc_type','D',"","col_12");
            $form->setFieldset('dpc_type','D',_("Description du bien"), "startClosed alignFormSpec");
                //
                $form->setBloc('dpc_type','DF', "","group");
                //
                $form->setBloc('dpc_desc_actv_ex','D', _("Description du fonds artisanal, du fonds de commerce ou du bail commercial"),"col_12");
                $form->setBloc('dpc_desc_aut_prec','F');
                //
                $form->setBloc('dpc_desig_comm_arti','D', _("Désignation du fonds artisanal, du fonds de commerce, ou du bail commercial ou du terrain"),"col_12");
                $form->setBloc('dpc_desig_loc_ann_prec','F');
                //
                $form->setBloc('dpc_bail_comm_date','D', _("S'il s'agit d'un bail commercial"),"col_12 group");
                $form->setBloc('dpc_bail_comm_loyer','F');
                //
                $form->setBloc('dpc_actv_acqu','DF',  _("Activité de l'acquéreur pressenti"),"group");
                //
                $form->setBloc('dpc_nb_sala_di','D', _("Nombre de salariés et nature de leur contrat de travail"),"col_12 group");
                $form->setBloc('dpc_nb_sala_td','F');
                //
                $form->setBloc('dpc_nb_sala_tc','D', "","col_12 group");
                $form->setBloc('dpc_nb_sala_tp','F');

            $form->setFieldset('dpc_nb_sala_tp','F','');
            $form->setBloc('dpc_nb_sala_tp','F');

            // Modalité de la cession
            $form->setBloc('dpc_moda_cess_vente_am','D',"","col_12");
            $form->setFieldset('dpc_moda_cess_vente_am','D',_("Modalité de la cession"), "startClosed alignFormSpec");
                //
                $form->setBloc('dpc_moda_cess_vente_am','D', "","col_12 group");
                $form->setBloc('dpc_moda_cess_prix','F');
                //
                $form->setBloc('dpc_moda_cess_adj_date','D', "","col_12 group");
                $form->setBloc('dpc_moda_cess_adj_prec','F');
                //
                $form->setBloc('dpc_moda_cess_paie_comp','DF', _("Modalités de paiement"),"col_12");

            $form->setFieldset('dpc_moda_cess_paie_aut_prec','F','');
            $form->setBloc('dpc_moda_cess_paie_aut_prec','F');

            // Le(s) soussigné(s) déclare(nt) que le bailleur
            $form->setBloc('dpc_ss_signe_demande_acqu','D',"","col_12");
            $form->setFieldset('dpc_ss_signe_demande_acqu','D',_("Le(s) soussigné(s) déclare(nt) que le bailleur"), "startClosed alignFormSpec");

            $form->setFieldset('dpc_ss_signe_recher_trouv_acqu','F','');
            $form->setBloc('dpc_ss_signe_recher_trouv_acqu','F');

            // Notification des décisions du titulaire du droit de préemption
            $form->setBloc('dpc_notif_adr_prop','D',"","col_12");
            $form->setFieldset('dpc_notif_adr_prop','D',_("Notification des décisions du titulaire du droit de préemption"), "startClosed alignFormSpec");

            $form->setFieldset('dpc_notif_adr_manda','F','');
            $form->setBloc('dpc_notif_adr_manda','F');

            //
            $form->setBloc('dpc_obs','D',"","col_12");
            $form->setFieldset('dpc_obs','D',_("Observations éventuelles"), "startClosed alignFormSpec");

            $form->setFieldset('dpc_obs','F','');
            $form->setBloc('dpc_obs','F');

        $form->setFieldset('dpc_obs','F','');
        $form->setBloc('dpc_obs','F');
        // Fin droit de préemption commercial
    }
    
    function setLib(&$form,$maj) {
        parent::setLib($form,$maj);
        //libelle des champs
        $form->setLib('architecte', _("coordonnees de l'architecte"));
        $form->setLib('ope_proj_desc', _("description"));
    }

    /**
     * Permet de savoir si le cerfa peut calculer la taxe d'aménagement.
     *
     * @return boolean
     */
    function can_simulate_taxe_amenagement() {
        // Liste des champs necessaires à la simulation de la taxe d'aménagement
        require_once "../obj/taxe_amenagement.class.php";
        $inst_taxe_amenagement = new taxe_amenagement(0);
        $list_fields = $inst_taxe_amenagement->get_list_fields_simulation();

        // Pour chaque champ
        foreach ($list_fields as $field) {
            // Si un seul des champs requis n'est pas dans le cerfa
            if ($this->getVal($field) == 'f') {
                // Retourne faux
                return false;
            }
        }

        // Retourne vrai
        return true;
    }


    /**
     * Récupère les champs du CERFA ainsi que leurs valeurs.
     *
     * @return array $result Tableau associatif
     */
    function get_form_val() {

        // Initialisation du tableau des résultats
        $result = array();

        // Pour chaque champ
        foreach ($this->champs as $champ) {
            // Récupère sa valeur
            $result[$champ] = $this->getVal($champ);
        }
        
        // Retourne le résultat
        return $result;
    }


}// fin classe
?>
