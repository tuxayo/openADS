<?php
/**
 * DBFORM - 'demande_avis_encours' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'demande_avis_encours'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/demande_avis.class.php";

class demande_avis_encours extends demande_avis {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();

        // ACTION - 090 - rendre_avis
        // Pour qu'un service rende l'avis
        $this->class_actions[90] = array(
            "identifier" => "rendre_avis",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => _("Rendre un avis"),
                "order" => 40,
                "class" => "edit-16",
            ),
            "view" => "formulaire",
            "method" => "modifier",
            "permission_suffix" => "modifier",
            "condition" => array("is_consultation_retour_avis_service"),
            "button" => _("Modifier"),
        );

        // ACTION - 120 - marquer
        // 
        $this->class_actions[120] = array(
            "identifier" => "marquer",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("Marquer le dossier"),
                "order" => 80,
                "class" => "marque-16",
            ),
            "method" => "marquer",
            "permission_suffix" => "marquer",
            "condition" => array("is_markable"),
        );

        // ACTION - 130 - démarquer
        // 
        $this->class_actions[130] = array(
            "identifier" => "demarquer",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("Dé-marquer le dossier"),
                "order" => 80,
                "class" => "demarque-16",
            ),
            "method" => "demarquer",
            "permission_suffix" => "demarquer",
            "condition" => array("is_unmarkable"),
        );
    }

    function setType(&$form, $maj) {
        parent::setType($form, $maj);

        if($this->getParameter("maj") == 3) {
            // Cache le fieldset avis rendu
            $form->setType('fichier', 'hidden');
            $form->setType('avis_consultation', 'hidden');
            $form->setType("motivation", "hidden");
        }
        // On cache la visibilité de la consultation car ça n'a rien a faire ici
        $form->setType('visible', 'hidden');

        // On définit le type des champs pour les actions direct
        // utilisant la vue formulaire
        if ($maj == 120  || $maj == 130) {
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
        }
    }

    function is_markable() {
        if ($this->getVal("marque") == 'f') {
            return true;
        }
        return false;
    }

    function is_unmarkable() {
        return !$this->is_markable();
    }

    function marquer($val = array(), &$dnu1 = null, $dnu2 = null) {
        return $this->manage_marque(true);
    }

    function demarquer($val = array(), &$dnu1 = null, $dnu2 = null) {
        return $this->manage_marque(false);
    }

    function manage_marque($bool) {
        // Begin
        $this->begin_treatment(__METHOD__);
        // Mise à jour
        $val = array("marque"=>$bool);
        $ret = $this->f->db->autoExecute(
            DB_PREFIXE."consultation",
            $val,
            DB_AUTOQUERY_UPDATE,
            "consultation = ".$this->getVal('consultation'));
        if (database::isError($ret, true)) {
            $this->erreur_db($ret->getDebugInfo(), $ret->getMessage(), 'consultation');
            $this->correct = false;
            $this->addToMessage(_("Erreur de base de donnees. Contactez votre administrateur."));
            return $this->end_treatment(__METHOD__, false);
        }
        $state = ($bool) ? _("marqué") : _("dé-marqué");
        $this->addToMessage(_("Dossier").' '.$state.' '._("avec succès."));
        $this->correct = true;
        return $this->end_treatment(__METHOD__, true);
    }


    /**
     * CONDITION - Défini si l'utilisateur est de service interne.
     *
     * @return boolean true si correspond false sinon
     */
    public function is_consultation_retour_avis_service() {

        return $this->f->isAccredited("consultation_retour_avis_service");
    }

}

?>
