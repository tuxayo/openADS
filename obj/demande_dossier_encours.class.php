<?php
/**
 * DBFORM - 'demande_dossier_encours' - Surcharge obj.
 *
 * Ce script permet de définir la classe 'demande_dossier_encours'.
 *
 * @package openads
 * @version SVN : $Id: demande_dossier_encours.class.php 5984 2016-02-19 08:41:12Z fmichon $
 */

require_once "../obj/demande.class.php";

/**
 * Définition de la classe 'demande_dossier_encours'.
 *
 * Cette classe permet d'interfacer l'ajout de demande sur un dossier existant.
 */
class demande_dossier_encours extends demande {

    /**
     * Surcharge du bouton retour afin de retourner sur le dashboard
     */
    function retour($premier = 0, $recherche = "", $tricol = "") {

        echo "\n<a class=\"retour\" ";
        echo "href=\"".$this->f->url_dashboard;
        //
        echo "\"";
        echo ">";
        //
        echo _("Retour");
        //
        echo "</a>\n";

    }

    /**
     * Permet de modifier le fil d'Ariane
     * @param string $ent Fil d'Ariane
     * @param array  $val Valeurs de l'objet
     * @param intger $maj Mode du formulaire
     */
    function getFormTitle($ent) {

        // Si l'identifiant du dossier est renseigné
        $dossier = $this->getParameter("idx_dossier");
        if (isset($dossier) && trim($dossier) != '') {
            //
            $dossier_libelle = $this->get_dossier_libelle($dossier);
            if (isset($dossier_libelle) && trim($dossier_libelle) != '') {
                $ent .= " -> "."<span class='color-num-di'>".mb_strtoupper($dossier_libelle, "UTF-8")."</span>";
            }
        } 

        // Change le fil d'Ariane
        return $ent;

    }

}

?>
