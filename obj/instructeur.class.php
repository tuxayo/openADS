<?php
//$Id: instructeur.class.php 5839 2016-01-29 08:50:12Z fmichon $ 
//gen openMairie le 29/01/2016 09:34

require_once "../gen/obj/instructeur.class.php";

class instructeur extends instructeur_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }


    function setOnchange(&$form, $maj){
        parent::setOnchange($form, $maj);

        // Si on n'est pas dans le contexte d'un sous-formulaire
        if ($this->getParameter('retourformulaire') === null) {
            //
            $form->setOnchange("division", "filterSelect(this.value, 'om_utilisateur', 'division', 'instructeur');");
        }
    }

    function setSelect(&$form, $maj, &$db = null, $debug = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/" . OM_DB_PHPTYPE . "/" . $this->table . ".form.inc.php")) {
            include "../sql/" . OM_DB_PHPTYPE . "/" . $this->table . ".form.inc.php";
        } elseif (file_exists("../sql/" . OM_DB_PHPTYPE . "/" . $this->table . ".form.inc")) {
            include "../sql/" . OM_DB_PHPTYPE . "/" . $this->table . ".form.inc";
        }

        // On recupere l'objet en cours et son identifiant
        $idx = $this->getParameter('idxformulaire');
        $retourformulaire = $this->f->get_submitted_get_value('retourformulaire');
        $obj = $this->f->get_submitted_get_value('obj');
        // Si on est dans le contexte d' "instructeur" en ajout ou en filter select
        if (($obj === "instructeur" && $retourformulaire === null)
            || $obj === null) {
            // Ne fait pas appel au parent car une requête SQL doit être modifiée 
            // dans le cas d'un ajout afin de permettre le bon tri du champ
            // om_utilisateur

            // En mode ajout, on modifie la requête permettant de récupérer la liste
            // des utilisateurs afin qu'elle ne retourne raucun résultat
            if ($maj == 0) {
                //
                $sql_om_utilisateur="SELECT om_utilisateur.om_utilisateur, om_utilisateur.nom FROM ".DB_PREFIXE."om_utilisateur WHERE om_utilisateur = 0";
            }

            // L'initialisation des select récupérée depuis la classe générée
            // instructeur_qualite
            $this->init_select($form, $this->f->db, $maj, null, "instructeur_qualite", $sql_instructeur_qualite, $sql_instructeur_qualite_by_id, false);
            // om_utilisateur
            $this->init_select($form, $this->f->db, $maj, null, "om_utilisateur", $sql_om_utilisateur, $sql_om_utilisateur_by_id, false);

            // Filtre des division en fonction des niveaux
            if ($this->f->isCollectiviteMono($_SESSION['collectivite']) === true) {

                // Bloque l'accès aux division autre que celles de sa collectivité
                $sql_division_by_collectivite = str_replace('<id_collectivite>',
                    $_SESSION['collectivite'], $sql_division_by_collectivite);
                $this->init_select($form, $this->f->db, $maj, null, "division", $sql_division_by_collectivite, $sql_division_by_id, true);
            }
            else {
                $this->init_select($form, $this->f->db, $maj, null, "division", $sql_division, $sql_division_by_id, true);
            }

            /**
             * Gestion du filtre sur les utilisateurs en fonction de la division
             * sélectionnée.
             */
            if ($maj == 0 || $maj == 1) {

                // Récupère l'identifiant de la division
                $id_division = "";
                if ($this->f->get_submitted_post_value('division') !== null) {
                    $id_division = $this->f->get_submitted_post_value('division');
                } elseif($this->getParameter('division') != "") {
                    $id_division = $this->getParameter('division');
                } elseif(isset($form->val['division'])) {
                    $id_division = $form->val['division'];
                }

                //
                if ($id_division !== "") {
                    // Tri les utilisateurs par la collectivité de la division
                    $sql_utilisateur_by_division = str_replace('<id_division>', $id_division,
                        $sql_utilisateur_by_division);
                    //
                    $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "om_utilisateur",
                        $sql_utilisateur_by_division, $sql_om_utilisateur_by_id, true);
                }
            }
        }
        // Si on est dans un autre context que "instructeur"
        else {
            
            parent::setSelect($form, $maj);

            // Si on est dans les utilisateurs
            if ($idx != null && $retourformulaire !== null && $retourformulaire === "om_utilisateur") {
                $sql_division_by_utilisateur = str_replace('<id_utilisateur>',
                    $idx, $sql_division_by_utilisateur);
                $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "division",
                    $sql_division_by_utilisateur, $sql_division_by_id, true);
            }
            // Si on est dans la division
            elseif ($idx != null && $retourformulaire !== null && $retourformulaire === "division") {
                $sql_utilisateur_by_division = str_replace('<id_division>', $idx,
                    $sql_utilisateur_by_division);
                $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "om_utilisateur",
                    $sql_utilisateur_by_division, $sql_om_utilisateur_by_id, true);
            }
        }
    }


}

?>
