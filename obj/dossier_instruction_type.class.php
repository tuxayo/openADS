<?php
/**
 * DBFORM - 'dossier_instruction_type' - Surcharge gen.
 *
 * Ce script permet de définir la classe 'dossier_instruction_type'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../gen/obj/dossier_instruction_type.class.php";

class dossier_instruction_type extends dossier_instruction_type_gen {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    function setType(&$form,$maj) {
        parent::setType($form,$maj);
        if($maj < 2) {
            $form->setType('mouvement_sitadel','select');
        } else {
            $form->setType('mouvement_sitadel','selectstatic');
        }
    }

    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        parent::setSelect($form, $maj,$db,$debug);
        // mouvement_sitadel
        $contenu=array();
        $contenu[0]=array(
            '',
            'DEPOT',
            'MODIFICATIF',
            'SUIVI',
            'SUPPRESSION',
            'TRANSFERT',
            );
        $contenu[1]=array(
            _('choisir')." "._("mouvement_sitadel"),
            _('DEPOT'),
            _('MODIFICATIF'),
            _('SUIVI'),
            _('SUPPRESSION'),
            _('TRANSFERT'),
            );
        $form->setSelect("mouvement_sitadel",$contenu);
    }

}

?>
