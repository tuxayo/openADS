<?php
/**
 * DBFORM - 'dossier_contrainte' - Surcharge gen.
 *
 * Ce script permet de définir la classe 'dossier_contrainte'.
 *
 * @package openads
 * @version SVN : $Id: dossier_contrainte.class.php 6565 2017-04-21 16:14:15Z softime $
 */

require_once "../gen/obj/dossier_contrainte.class.php";

class dossier_contrainte extends dossier_contrainte_gen {

    /**
     * Constructeur.
     * ...
     *
     * @param string $id_dossier Identifiant du dossier.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null, $id_dossier = 0) {
        $this->constructeur($id);
        //On active les nouvelles actions
        ($this->f->get_submitted_get_value('idxformulaire')!==null ? $idxformulaire = 
        $this->f->get_submitted_get_value('idxformulaire') : $idxformulaire = "");

        // Liste des contraintes
        $getListContraintes = $this->getListContraintes($idxformulaire);

        // Tant qu'il y a des contraintes
        while ($rowListContraintes = &$getListContraintes->fetchRow(DB_FETCHMODE_ASSOC)) {
            // Identifiant du champ
            $id_champ = 'contrainte_'.$rowListContraintes['contrainte_id'];
            // Ajoute les informations de la contrainte au tableau
            $this->listContraintes[$id_champ] = $rowListContraintes;
            // Ajoute la contrainte en tant que champ
            $this->setChamp($id_champ);
        }
    }

    /**
     * Cette variable permet de stocker le résultat de la méthode
     * getDivisionFromDossier() afin de ne pas effectuer le recalcul à chacun de
     * ces appels.
     * @var string Code de la division du dossier en cours
     */
    var $_division_from_dossier = NULL;
    
    /**
     * Liste des contraintes
     * @var array
     */
    var $listContraintes = array();

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();
        
        // ACTION - 000 - ajouter
        // Modifie la condition d'affichage du bouton ajouter
        $this->class_actions[0]["condition"] = array("is_addable", "can_user_access_dossier_contexte_ajout");

        // ACTION - 001 - modifier
        // Modifie la condition et le libellé du bouton modifier
        $this->class_actions[1]["condition"] = array("is_editable", "can_user_access_dossier_contexte_modification");
        
        // ACTION - 002 - supprimer
        // Modifie la condition et le libellé du bouton supprimer
        $this->class_actions[2]["condition"] = array("is_deletable", "can_user_access_dossier_contexte_modification");

        // ACTION - 003 - consulter
        // 
        $this->class_actions[3]["condition"] = "can_user_access_dossier_contexte_modification";


        // ACTION - 004 - view_tab
        // Interface spécifique de la liste des contraintes
        $this->class_actions[4] = array(
            "identifier" => "view_tab",
            "view" => "view_tab",
            "permission_suffix" => "tab",
            "condition" => "can_user_access_dossier_contexte_ajout",
        );

        // ACTION - 005 - view_tab
        // Interface spécifique de la vue du traitement d'ajout de contraintes
        $this->class_actions[5] = array(
            "identifier" => "view_add",
            "view" => "view_add",
            "permission_suffix" => "ajouter",
            "condition" => "can_user_access_dossier_contexte_ajout",
        );
    }

    /**
     * VIEW - view_tab
     * 
     * Cette vue permet d'afficher les contraintes rattachées à un dossier
     * dans un tableau organisé par les groupes et sous-groupes.
     *
     * @return void
     */
    function view_tab() {
        // Début affichage tableau
        printf("\n<div id=\"sousform-dossier_contrainte\">\n");
        printf("\n<!-- ########## START FORMULAIRE ########## -->\n");
        printf("<div class=\"formEntete ui-corner-all\">\n");

        // Récupération des variables GET
        ($this->f->get_submitted_get_value('idxformulaire')!==null ? $idxformulaire = 
            $this->f->get_submitted_get_value('idxformulaire') : $idxformulaire = "");
        ($this->f->get_submitted_get_value('retourformulaire')!==null ? $retourformulaire = 
            $this->f->get_submitted_get_value('retourformulaire') : $retourformulaire = "");
        ($this->f->get_submitted_get_value('obj') !== null ? $obj = $this->f->get_submitted_get_value('obj') : $obj = "");
        // Récupèration du numéro du dossier
        $dossier = $idxformulaire;

        // Initialisation des affichages des bouton modifier et supprimer
        $show_btn_edit = false;
        $show_btn_delete = false;
        // Si toutes les conditions pour afficher le bouton modifier sont remplis
        if ($this->is_editable() === true) {
            // Affiche le bouton
            $show_btn_edit = true;
        }
        // Si toutes les conditions pour afficher le bouton supprimer sont remplis
        if ($this->is_deletable() === true) {
            // Affiche le bouton
            $show_btn_delete = true;
        }

        // Champs des contraintes à afficher
        $select = "SELECT dossier_contrainte.dossier_contrainte as dossier_contrainte_id,
                        dossier_contrainte.texte_complete as dossier_contrainte_texte,
                        dossier_contrainte.reference as dossier_contrainte_reference,
                        contrainte.libelle as contrainte_libelle,
                        contrainte.nature as contrainte_nature,
                        contrainte.texte as contrainte_texte,
                        contrainte.reference as contrainte_reference,
                        lower(contrainte.groupe) as contrainte_groupe,
                        lower(contrainte.sousgroupe) as contrainte_sousgroupe ";

        // Table pour le select
        $from = " FROM ".DB_PREFIXE."contrainte 
                    LEFT JOIN ".DB_PREFIXE."dossier_contrainte
                        ON  dossier_contrainte.contrainte = contrainte.contrainte ";

        // Condition de la requête
        $where = " WHERE dossier_contrainte.dossier = '$dossier' ";

        // Le tri sur la requête
        $tri = " ORDER BY contrainte_groupe DESC, contrainte_sousgroupe, contrainte.no_ordre, contrainte.libelle ";

        // Requête
        $sql = $select.$from.$where.$tri;
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // bouton pour gérer les contraintes
        $ajouter = "
        <p>
            <a id=\"action-soustab-dossier_contrainte-corner-ajouter\" onclick=\"ajaxIt('" . $obj . "','../scr/sousform.php?obj=" . $obj . "&action=0&tri=&objsf=dossier_contrainte&premiersf=0&retourformulaire=".$retourformulaire."&idxformulaire=".$dossier."&trisf=&retour=tab');\" href='#'>
                <span class=\"om-prev-icon om-icon-16 add-16\" title=\""._("Ajouter des contraintes")."\">
                    "._("Ajouter des contraintes")."
                </span>
            </a>
        </p>
        ";

        // Entête pour le groupe
        $groupeHeader = "
        <div class='dossier_contrainte_groupe'>
            <div class='dossier_contrainte_groupe_header'>
                <span class='name'>
                    %s
                </span>
            </div>
        ";

        // Entête pour le sous-groupe
        $sousgroupeHeader = "
        <div class='dossier_contrainte_sousgroupe'>
            <div class='dossier_contrainte_sousgroupe_header'>
                <span class='name'>
                    %s
                </span>
            </div>
        ";
        // Titres des colonnes
        $tableHeader = "
        <thead>
            <tr class='ui-tabs-nav ui-accordion ui-state-default tab-title'>
                <th class='icons actions-max-1'>
                    <span class='name'>
                        %s
                    </span>
                </th>";
        // Si le retour formulaire est bon
        if (($retourformulaire == 'dossier'
            || $retourformulaire == 'dossier_instruction'
            || $retourformulaire == 'dossier_instruction_mes_encours'
            || $retourformulaire == 'dossier_instruction_tous_encours'
            || $retourformulaire == 'dossier_instruction_mes_clotures'
            || $retourformulaire == 'dossier_instruction_tous_clotures'
            || $retourformulaire == 'dossier_contentieux_mes_infractions'
            || $retourformulaire == 'dossier_contentieux_toutes_infractions'
            || $retourformulaire == 'dossier_contentieux_mes_recours'
            || $retourformulaire == 'dossier_contentieux_tous_recours')) {
            //
            if ($show_btn_edit == true) {
                // Affiche le header de la colonne pour le bouton
                $tableHeader .= "
                <th class='icons actions-max-1'>
                    <span class='name'>
                        %s
                    </span>
                </th>";
            }
            //
            if ($show_btn_delete == true) {
                // Affiche le header de la colonne pour le bouton
                $tableHeader .= "
                <th class='icons actions-max-1'>
                    <span class='name'>
                        %s
                    </span>
                </th>";
            }
        }
        $tableHeader .= "
                <th class='title col-0 firstcol'>
                    <span class='name'>
                        "._('libelle')."
                    </span>
                </th>
                <th class='title col-1'>
                    <span class='name'>
                        "._('texte_complete')."
                    </span>
                </th>
                <th class='title col-2'>
                    <span class='name'>
                        "._('reference')."
                    </span>
                </th>
                <th class='title col-3'>
                    <span class='name'>
                        "._('nature')."
                    </span>
                </th>
            </tr>
        </thead>
        ";

        // Ligne de données
        $line = "
        <tr class='tab-data %s'>
            <td class='icons'>
                %s
            </td>";
        //      
        if (($retourformulaire == 'dossier'
            || $retourformulaire == 'dossier_instruction'
            || $retourformulaire == 'dossier_instruction_mes_encours'
            || $retourformulaire == 'dossier_instruction_tous_encours'
            || $retourformulaire == 'dossier_instruction_mes_clotures'
            || $retourformulaire == 'dossier_instruction_tous_clotures'
            || $retourformulaire == 'dossier_contentieux_mes_infractions'
            || $retourformulaire == 'dossier_contentieux_toutes_infractions'
            || $retourformulaire == 'dossier_contentieux_mes_recours'
            || $retourformulaire == 'dossier_contentieux_tous_recours')) {
            //
            if ($show_btn_edit == true) {
                // Affiche la cellule pour le bouton
                $line .= "
                <td class='icons'>
                    %s
                </td>";
            }
            //
            if ($show_btn_delete == true) {
                // Affiche la cellule pour le bouton
                $line .= "
                <td class='icons'>
                    %s
                </td>";
            }
        }
        $line .= "    
            <td class='col-0 firstcol'>
                %s
            </td>
            <td class='col-1'>
                %s
            </td>
            <td class='col-2'>
                %s
            </td>
            <td class='col-3'>
                %s
            </td>
        </tr>
        ";

        // Lien des données
        $link = "
        <a class='lienTable' onclick=\"ajaxIt('" . $obj . "','../scr/sousform.php?obj=" . $obj . "&action=3&idx=%s&tri=&premier=0&recherche=&objsf=dossier_contrainte&premiersf=0&retourformulaire=%s&idxformulaire=%s&trisf=&retour=tab');\" href='#'>
            %s
        </a>
        ";

        // 
        $button = '
        <a onclick=\'ajaxIt("' . $obj . '","../scr/sousform.php?obj=' . $obj . '&amp;action=%1$s&amp;idx=%2$s&amp;tri=&amp;premier=0&amp;recherche=&amp;objsf=dossier_contrainte&amp;premiersf=0&amp;retourformulaire=%3$s&amp;idxformulaire=%4$s&amp;trisf=&amp;retour=tab");\' id="action-soustab-dossier_contrainte-corner-left-%5$s-%2$s" href="#">
            <span title="%5$s" class="om-icon om-icon-16 om-icon-fix %6$s-16">
                %5$s
            </span>
        </a>
        ';

        //Vérification des droits sur l'ajout
        // et que l'instructeur est de la bonne division
        if ($this->is_addable() === true) {

            // Affiche le bouton pour gérer les contraintes
            printf($ajouter);
        }

        // Sauvegarde des données pour les comparer
        $lastRow = array();
        $lastRow['contrainte_groupe'] = 'empty';
        $lastRow['contrainte_sousgroupe'] = 'empty';

        // Tant qu'il y a des résultats
        while($row = &$res->fetchRow(DB_FETCHMODE_ASSOC)) {

            // Si l'identifiant du groupe de la contrainte présente et 
            // celle d'avant est différent
            if ($row['contrainte_groupe'] != $lastRow['contrainte_groupe']) {

                // Si l'identifiant du groupe d'avant est vide
                if ($lastRow['contrainte_groupe'] != 'empty') {
                    // Ferme le tableau
                    printf("</table>");
                    //
                    printf("</div>");
                    //
                    printf("</div>");
                }

                // Affiche le header du groupe
                printf($groupeHeader, $row['contrainte_groupe']);
            }

            // Si l'identifiant du sous-groupe de la contrainte présente et 
            // celle d'avant est différent
            // Ou qu'ils soient identique mais n'appartiennent pas au même groupe
            if ($row['contrainte_sousgroupe'] != $lastRow['contrainte_sousgroupe']
                || ($row['contrainte_sousgroupe'] == $lastRow['contrainte_sousgroupe']
                    && $row['contrainte_groupe'] != $lastRow['contrainte_groupe'])) {

                //
                if($row['contrainte_groupe'] == $lastRow['contrainte_groupe']) {
                    // Si l'identifiant de la sous-groupe d'avant est vide
                    if ($lastRow['contrainte_sousgroupe'] != 'empty') {
                        // Ferme le tableau
                        printf("</table>");
                        //
                        printf("</div>");
                    }
                }

                // Affiche le header du sous-groupe
                printf($sousgroupeHeader, $row['contrainte_sousgroupe']);

                // Ouvre le tableau
                printf("<table id='sousgroupe_".$row['contrainte_sousgroupe']
                    ."' class='tab-tab dossier_contrainte_view'>");

                // Affiche le header des données
                if ($show_btn_edit == false
                    && $show_btn_delete == false) {
                    //
                    printf($tableHeader, '');
                }
                //
                if ($show_btn_edit == true
                    && $show_btn_delete == false) {
                    //
                    printf($tableHeader, '', '');
                }
                //
                if ($show_btn_edit == false
                    && $show_btn_delete == true) {
                    //
                    printf($tableHeader, '', '');
                }
                //
                if ($show_btn_edit == true
                    && $show_btn_delete == true) {
                    //
                    printf($tableHeader, '', '', '');
                }

                // Définis le style des lignes
                $style = 'odd';
            }

            // Si toujours dans la même groupe et même sous-groupe, 
            // on change le style de la ligne
            if ($row['contrainte_groupe'] == $lastRow['contrainte_groupe']
                && $row['contrainte_sousgroupe'] == $lastRow['contrainte_sousgroupe']) {
                //
                $style = ($style=='even')?'odd':'even';
            }
            
            // Affiche "Oui" ou "Non" pour le bouléen
            if ($row['dossier_contrainte_reference'] == 1 
                || $row['dossier_contrainte_reference'] == "t"
                || $row['dossier_contrainte_reference'] == "Oui") {
                //
                $contrainte_reference = "Oui";
            } else {
                //
                $contrainte_reference = "Non";
            }

            // Affiche les données
            if (($retourformulaire == 'dossier'
                || $retourformulaire == 'dossier_instruction'
                || $retourformulaire == 'dossier_instruction_mes_encours'
                || $retourformulaire == 'dossier_instruction_tous_encours'
                || $retourformulaire == 'dossier_instruction_mes_clotures'
                || $retourformulaire == 'dossier_instruction_tous_clotures'
                || $retourformulaire == 'dossier_contentieux_mes_infractions'
                || $retourformulaire == 'dossier_contentieux_toutes_infractions'
                || $retourformulaire == 'dossier_contentieux_mes_recours'
                || $retourformulaire == 'dossier_contentieux_tous_recours')) {
                //
                if ($show_btn_edit == false
                    && $show_btn_delete == false) {
                    //
                    printf($line, $style, 
                        sprintf($button, '3', $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, _('Consulter'), 'consult'),
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['contrainte_libelle']), 
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['dossier_contrainte_texte']), 
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $contrainte_reference),
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['contrainte_nature']));
                }
                //
                if ($show_btn_edit == true
                    && $show_btn_delete == false) {
                    //
                    printf($line, $style, 
                        sprintf($button, '3', $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, _('Consulter'), 'consult'),
                        sprintf($button, '1', $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, _('Modifier'), 'edit'),
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['contrainte_libelle']), 
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['dossier_contrainte_texte']), 
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $contrainte_reference),
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['contrainte_nature']));
                }
                //
                if ($show_btn_edit == false
                    && $show_btn_delete == true) {
                    //
                    printf($line, $style, 
                        sprintf($button, '3', $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, _('Consulter'), 'consult'),
                        sprintf($button, '2', $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, _('Supprimer'), 'delete'),
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['contrainte_libelle']), 
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['dossier_contrainte_texte']), 
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $contrainte_reference),
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['contrainte_nature']));
                }
                //
                if ($show_btn_edit == true
                    && $show_btn_delete == true) {
                    //
                    printf($line, $style, 
                        sprintf($button, '3', $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, _('Consulter'), 'consult'),
                        sprintf($button, '1', $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, _('Modifier'), 'edit'),
                        sprintf($button, '2', $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, _('Supprimer'), 'delete'),
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['contrainte_libelle']), 
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['dossier_contrainte_texte']), 
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $contrainte_reference),
                        sprintf($link, $row['dossier_contrainte_id'], $retourformulaire, 
                            $idxformulaire, $row['contrainte_nature']));
                }
            }

            // Sauvegarde les données
            $lastRow['contrainte_groupe'] = $row['contrainte_groupe'];
            $lastRow['contrainte_sousgroupe'] = $row['contrainte_sousgroupe'];
            
        }
        //
        if ($res->numrows() != 0) {
            // Ferme le tableau
            printf("</table>");
            // Ferme le sous-groupe
            printf("</div>");
            // Ferme le groupe
            printf("</div>");
        }

        // Si il n'y a pas de résultat on affiche "Aucun enregistrements"
        if ($res->numrows() == 0) {
            //
            printf ("<p class='noData'>"._("Aucun enregistrement")."<p>");
        }

        // Ferme la div
        printf("</div>");
        // Ferme la div
        printf("</div>");
    }

    /**
     * Permet d'ajouter des champs au formulaire.
     * @param string $champ Nom du champ
     */
    function setChamp($champ) {
        // Ajoute un champ
        $this->champs[] = $champ;
    }

    /**
     * Permet de définir le type des champs.
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setType(&$form,$maj) {
        parent::setType($form, $maj);
        
        // Les champs à cacher
        $form->setType('dossier_contrainte', 'hidden');
        $form->setType('dossier', 'hidden');
        $form->setType('contrainte', 'hidden');
        $form->setType('reference', 'hidden');

        // En mode ajouté
        if ($maj == 0) {
            //
            $form->setType('texte_complete', 'hidden');
            // Pour chaque champ
            foreach ($this->champs as $key => $value) {
                // 
                $search_field = 'contrainte_';
                // Qui contient le mot 'contrainte_'
                if (strpos($value, $search_field) !== false) {
                    //
                    $form->setType($value, 'checkbox');
                }
            }
        }
    }

    /**
     * Permet de définir la taille des champs.
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setTaille(&$form, $maj) {
        parent::setTaille($form, $maj);

        // En mode ajouté
        if ($maj == 0) {
            // Pour chaque champ
            foreach ($this->champs as $key => $value) {
                //
                $search_field = 'contrainte_';
                // Qui contient le mot 'contrainte_'
                if (strpos($value, $search_field) !== false) {
                    //
                    $form->setTaille($value, 1);
                }
            }
        }
    }

    /**
     * Permet de définir le nombre de caractères maximum des champs.
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setMax(&$form, $maj) {
        //
        parent::setMax($form, $maj);
        // En mode ajouté
        if ($maj == 0) {
            // Pour chaque champ
            foreach ($this->champs as $key => $value) {
                //
                $search_field = 'contrainte_';
                // Qui contient le mot 'contrainte_'
                if (strpos($value, $search_field) !== false) {
                    //
                    $form->setMax($value, 1);
                }
            }
        }
    }

    /**
     * Permet de définir le libellé des champs.
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLib(&$form,$maj) {
        //
        parent::setLib($form, $maj);
        // En mode ajouté
        if ($maj == 0) {
            // Pour chaque champ
            foreach ($this->champs as $key => $value) {
                //
                $search_field = 'contrainte_';
                // Qui contient le mot 'contrainte_'
                if (strpos($value, $search_field) !== false) {
                    //
                    $form->setLib($value, 
                        $this->listContraintes[$value]['contrainte_lib']);
                }
            }
        }
    }

    /**
     * Permet de définir les valeurs des champs.
     * @param object  &$form            Objet du formulaire
     * @param integer $maj              Mode du formulaire
     * @param integer $validation       Validation du formulaire
     * @param mixed   $idxformulaire    Identifiant du formulaire parent
     * @param string  $retourformulaire Formulaire parent
     * @param string  $typeformulaire   ???
     * @param object  &$db              Objet de la base de données
     * @param boolean $DEBUG            Mode DEBUG
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $db, $DEBUG);

        // En mode ajout
        if ($maj == 0) {
            // Si le formulaire principal est 'dossier_instruction'
            if($retourformulaire === "dossier_instruction" 
            || $retourformulaire==='dossier_instruction_tous_clotures' 
            || $retourformulaire==='dossier_instruction_mes_clotures' 
            || $retourformulaire==='dossier_instruction_tous_encours' 
            || $retourformulaire==='dossier_instruction_mes_encours'
            || $retourformulaire==='dossier_contentieux_mes_infractions'
            || $retourformulaire==='dossier_contentieux_toutes_infractions'
            || $retourformulaire==='dossier_contentieux_mes_recours'
            || $retourformulaire==='dossier_contentieux_tous_recours') {
                // Valeur du dossier en cours
                $form->setVal("dossier", $idxformulaire);
            }
        }
        // Pas en mode ajout
        if ($maj != 0) {
            // Pour chaque champ
            foreach ($this->champs as $key => $value) {
                //
                $search_field = 'contrainte_';
                // Qui contient le mot 'contrainte_'
                if (strpos($value, $search_field) !== false) {
                    //
                    unset($this->champs[$key]);
                }
            }
        }
    }

    /**
     * Récupère la liste des contraintes de la collectivité du dossier
     * et de la multicollectivité.
     * @return object Résultat de la requête
     */
    function getListContraintes($id_dossier = "") {
        $dossier_collectivite = "";
        if ($id_dossier != "") {
            // Récupération de la collectivité du dossier
            require_once "../obj/dossier.class.php";
            $dossier = new dossier($id_dossier, $f->db, DEBUG);
            $dossier_collectivite = $dossier->getVal("om_collectivite");
        }
        

        // Requête SQL
        $sql = "SELECT 
                    contrainte.contrainte as contrainte_id,
                    lower(contrainte.groupe) as contrainte_groupe, 
                    lower(contrainte.sousgroupe) as contrainte_sousgroupe,
                    contrainte.libelle as contrainte_lib,
                    contrainte.no_ordre as contrainte_ordre
                FROM ".DB_PREFIXE."contrainte
                LEFT JOIN ".DB_PREFIXE."om_collectivite
                ON contrainte.om_collectivite = om_collectivite.om_collectivite
                WHERE (contrainte.om_validite_fin IS NULL
                       OR contrainte.om_validite_fin > CURRENT_DATE)";
        if ($dossier_collectivite != "") {
            $sql .= "AND (om_collectivite.niveau = '2' OR contrainte.om_collectivite = ".$dossier_collectivite.")";
        }
        $sql .= "ORDER BY contrainte_groupe DESC, contrainte_sousgroupe, 
                    contrainte.no_ordre, contrainte.libelle";
        $this->f->addToLog("getListContraintes(".$id_dossier.") : db->query(\"".$sql."\")", 
            VERBOSE_MODE);
        $res = $this->db->query($sql);
        $this->f->isDatabaseError($res);

        // Résultat retourné
        return $res;
    }

    /**
     * Méthode de mise en page.
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLayout(&$form, $maj) {

        // En mode ajouté
        if ($maj == 0 ) {

            // Si la liste des contraintes n'est pas vide
            if (!empty($this->listContraintes)) {

                // Sauvegarde des données des contraintes pour les comparer
                $contrainte_before = array();
                $contrainte_before['contrainte_groupe'] = '';
                $contrainte_before['contrainte_sousgroupe'] = '';
                $contrainte_before['key'] = '';

                // Pour chaque contrainte
                foreach ($this->listContraintes as $key => $contrainte) {
                 
                    // Si l'identifiant du groupe de la contrainte présente et 
                    // celle d'avant est différent
                    if ($contrainte['contrainte_groupe'] 
                        != $contrainte_before['contrainte_groupe']) {
                        // Si l'identifiant du groupe d'avant est vide
                        if ($contrainte_before['contrainte_groupe'] != '') {
                            // Ferme le fieldset
                            $form->setFieldset($contrainte_before['key'], 'F');
                        }
                        // Ouvre le fieldset
                        $form->setFieldset($key, 'D', $contrainte['contrainte_groupe'], 
                            "startClosed text_capitalize");
                    }

                    // Si l'identifiant de la sous-catégorie de la contrainte présente 
                    // et celle d'avant est différent
                    if ($contrainte['contrainte_sousgroupe'] 
                        != $contrainte_before['contrainte_sousgroupe']) {
                        // Si l'identifiant de la sous-catégorie d'avant est vide
                        if ($contrainte_before['contrainte_sousgroupe'] != '') {
                            // Ferme le fieldset
                            $form->setFieldset($contrainte_before['key'], 'F');
                        }

                        // Si la contrainte a un sous-groupe
                        if ($contrainte['contrainte_sousgroupe'] != null) {
                            // Ouvre le fieldset
                            $form->setFieldset($key, 'D', 
                                $contrainte['contrainte_sousgroupe'], 
                                "startClosed text_capitalize");
                        }
                    }
                    //
                    $form->setBloc($key, 'DF', "", "");

                    // Sauvegarde les données de la contrainte
                    $contrainte_before['contrainte_groupe'] = 
                        $contrainte['contrainte_groupe'];
                    $contrainte_before['contrainte_sousgroupe'] = 
                        $contrainte['contrainte_sousgroupe'];
                    $contrainte_before['key'] = $key;
                }
                // Ferme le dernier fieldset
                $form->setFieldset($contrainte_before['key'], 'F');

            } 

            // Si la liste des contraintes est vide
            if (empty($this->listContraintes)) {
                // On affiche un message
                $form->setBloc('dossier_contrainte', 'D', _("Aucune contraintes."), 
                    "noDataForm");
            }

        }

        // Pas en mode ajout
        if ($maj != 0) {
            // Ouvre le fieldset
            $form->setFieldset('texte_complete', 'DF', _("dossier_contrainte"), "");
        }
        
    }

    /**
     * Permet de modifier l'affichage des boutons dans le sousformulaire.
     * @param string  $datasubmit Données a transmettre
     * @param integer $maj        Mode du formulaire
     * @param array   $val        Valeur du formulaire
     */
    function boutonsousformulaire($datasubmit, $maj, $val = null) {

        (isset($_GET['obj']) ? $obj = $this->f->get_submitted_get_value('obj') : $obj = "");
        ($this->f->get_submitted_get_value('idxformulaire') !== null ? $id_dossier = 
            $this->f->get_submitted_get_value('idxformulaire') : $id_dossier = "");
        //
        if (!$this->correct) {
            // Action par défaut
            $onclick =  "affichersform('".get_class($this)."', 
                '$datasubmit', this.form);return false;";
            //
            switch ($maj) {
                case 0:
                    $bouton = _("Appliquer les changements");
                    // Action en mode ajouter
                    $onclick = "dossierContrainteValidationForm(".get_class($this).", 
                        '../scr/form.php?obj=" . $obj
                        . "&action=5"
                        . "&idx=0"
                        . "&idxformulaire=" . $id_dossier . "', 
                        this.form);return false;";
                    break;
                case 1:
                    $bouton = _("Modifier");
                    break;
                case 2:
                    $bouton = _("Supprimer");
                    break;
            }
            //
            $params = array(
                "value" => $bouton,
                "onclick" => $onclick,
            );
            //
            $this->f->layout->display_form_button($params);
        }
    }

    /**
     * Permet de modifier le bouton retour du sousformulaire.
     * @param mixed   $idxformulaire    Identifiant du formulaire parent
     * @param string  $retourformulaire Formulaire parent
     * @param array   $val              Valeurs du formulaire
     * @param string  $objsf            Objet du sousformulaire
     * @param integer $premiersf        Premier enregistrement affiché
     * @param string  $tricolsf         Colonne triée
     * @param integer $validation       Validation du formulaire
     * @param mixed   $idx              Identifiant de l'enregistrement
     * @param integer $maj              Mode du formulaire
     * @param srting  $retour           Retour du formulaire
     */
    function retoursousformulaire($idxformulaire = NULL, $retourformulaire = NULL, $val = NULL,
                                  $objsf = NULL, $premiersf = NULL, $tricolsf = NULL, $validation = NULL,
                                  $idx = NULL, $maj = NULL, $retour = NULL) {

        // Si le formulaire parent est dossier
        if($retourformulaire === "dossier_instruction" 
            || $retourformulaire==='dossier_instruction_tous_clotures' 
            || $retourformulaire==='dossier_instruction_mes_clotures' 
            || $retourformulaire==='dossier_instruction_tous_encours' 
            || $retourformulaire==='dossier_instruction_mes_encours'
            || $retourformulaire==='dossier_contentieux_mes_infractions'
            || $retourformulaire==='dossier_contentieux_toutes_infractions'
            || $retourformulaire==='dossier_contentieux_mes_recours'
            || $retourformulaire==='dossier_contentieux_tous_recours') {
            
            //
            echo "\n<a class=\"retour\" ";
            echo "href=\"#\" ";
            
            echo "onclick=\"ajaxIt('".$objsf."', '"; 
            echo "../scr/sousform.php"
                    ."?obj=" . $objsf
                    ."&action=4"
                    ."&idx=$idxformulaire"
                    ."&retourformulaire=$retourformulaire"
                    ."&idxformulaire=$idxformulaire');\"";
            echo  "\" ";
            echo ">";
            //
            echo _("Retour");
            //
            echo "</a>\n";

        } else {
            //
            parent::retoursousformulaire($idxformulaire, $retourformulaire, $val,
                                  $objsf, $premiersf, $tricolsf, $validation,
                                  $idx, $maj, $retour);
        }
    }

    /**
     * Permet d’effectuer des actions avant la modification des données dans la base.
     * @param mixed   $id    Identifiant de l'enregistrement
     * @param object  &$db   Objet de la base de données
     * @param array   $val   Valeurs du formulaire
     * @param boolean $DEBUG Mode DEBUG
     */
    function triggermodifier($id, &$db = null, $val = array(), $DEBUG = null) {
        // Si le texte complété de la liaison venait du SIG et qu'il a été 
        // modifié
        if ($this->valF['reference'] == 't'
            && $this->getVal('texte_complete') != $this->valF['texte_complete']) {
            // Indique que ce n'est pas le texte complété du SIG
            $this->valF['reference'] = 'f';
        }
    }

    /**
     * Recupère le libellé de la contrainte.
     * @param integer $contrainte Identifiant de la contrainte
     * 
     * @return string             Libellé de la contrainte
     */
    function getLibelleFromContrainte($contrainte) {

        // Initialisation résultat
        $libelle = '';

        // Si la condition n'est pas vide
        if ($contrainte != "" 
            && $contrainte != null
            && is_numeric($contrainte)) {

            // Requête SQL
            $sql = "SELECT libelle
                    FROM ".DB_PREFIXE."contrainte
                    WHERE contrainte = ".$contrainte;
            $this->f->addToLog(__METHOD__."(".$contrainte.") : db->getOne(\"".$sql."\")", 
                VERBOSE_MODE);
            $libelle = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($libelle);
        }

        // Retourne résultat
        return $libelle;
    }

    /**
     * Récupère le texte de la contrainte.
     * @param integer $contrainte Identifiant de la contrainte
     * 
     * @return string             Texte de la contrainte
     */
    function getTexteFromContrainte($contrainte) {

        // Initialisation résultat
        $texte = '';

        // Si la condition n'est pas vide
        if ($contrainte != "" 
            && $contrainte != null
            && is_numeric($contrainte)) {

            // Requête SQL
            $sql = "SELECT texte
                    FROM ".DB_PREFIXE."contrainte
                    WHERE contrainte = ".$contrainte;
            $this->f->addToLog(__METHOD__."(".$contrainte.") : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $texte = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($texte);
        }

        // Retourne résultat
        return $texte;
    }

    /**
     * Récupère l'identifiant de la liaison.
     * @param string  $dossier    Identifiant du dossier
     * @param integer $contrainte Identifiant de la contrainte
     * @param object  $f          Objet de la classe utils
     * 
     * @return object             Résultat de la requête
     */
    function getDossierContrainte($dossier, $contrainte, $f) {

        // Requête SQL
        $sql = "SELECT dossier_contrainte
                FROM ".DB_PREFIXE."dossier_contrainte
                WHERE dossier = '".$dossier."'
                    AND contrainte = ".$contrainte;
        $f->addToLog("getDossierContrainte() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
        $res = $f->db->getOne($sql);
        $f->isDatabaseError($res);

        // Retourne résultat
        return $res;
    }

    /**
     * VIEW - view_add
     * 
     * Cette vue permet de traiter les contraintes postées et d'afficher
     * le résultat de ce traitement en AJAX.
     *
     * @return void
     */
    function view_add() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Désactivation des logs cause AJAX
        $this->f->disableLog();
        // Récupération des POST
        $postedValue = $this->f->get_submitted_post_value();
        $decodedPost = array();
        foreach ($postedValue as $key => $value) {
            $decodedPost[$key] = utf8_decode($value);
        }

        // Identifiant du dossier
        $dossier = $decodedPost['dossier'];

        // Définition des variables pour le message retourné
        $listAddContrainte = array();
        // $listDeleteContrainte = array();
        $message = "";
        // Message pour les ajouts
        $messageAdd = _("La contrainte %s a ete ajoutee au dossier.");

        // Pour chaque champ récupéré
        foreach ($decodedPost as $key => $value) {
            // Mot-clés à rechercher
            $search_field = 'contrainte_';
            // Si le mot-clés est présent dans l'identifiant du champ
            if (strpos($key, $search_field) !== false) {
                // Récupération de l'identifiant de la contrainte
                $id_contrainte = str_replace('contrainte_', '', $key);
                // Si la valeur du champ est 'Oui'
                if ($value === 'Oui') {
                    // Instancie la classe dossier_contrainte
                    $dossier_contrainte = new dossier_contrainte("]", $this->f->db, DEBUG);
                    // Définit les valeurs
                    $val = array(
                        'dossier_contrainte' => ']',
                        'dossier' => $dossier,
                        'contrainte' => $id_contrainte,
                        'texte_complete' => $this->getTexteFromContrainte($id_contrainte),
                        'reference' => false
                    );
                    // Ajoute l'enregistrement
                    $ajouter = $dossier_contrainte->ajouter($val, $this->f->db, DEBUG);
                    // Si la contrainte est ajouté
                    if ($ajouter == true) {
                        // Ajoute le libellé de la contrainte au tableau
                        $listAddContrainte[] = $this->getLibelleFromContrainte($id_contrainte);
                    }
                }
            }
        }

        // Pour chaque libellé sauvegardé dans les tableaux on compose un message
        foreach ($listAddContrainte as $key => $value) {
            $message .= sprintf($messageAdd, "<b>".$value."</b>")."<br/>";
        }

        // Si le message à retourner est vide
        if ($message == "") {
            // Message par défaut
            $message = _("Aucune action effectuee.");
        }

        // Retourne le message
        echo json_encode($message);
    }
    
    /**
     * Si le dossier d'instruction auquel est rattachée la consultation est 
     * cloturé, on affiche pas les liens du portlet.
     *
     * @return boolean true si non cloturé false sinon
     */
    function is_dossier_instruction_not_closed() {
        $idxformulaire = $this->getParameter("idxformulaire");
        $retourformulaire = $this->getParameter("retourformulaire");
        //Si le dossier d'instruction auquel est rattachée la consultation est 
        //cloturé, on affiche pas les liens du portlet
        if ( $idxformulaire != '' && 
            (
                $retourformulaire == 'dossier' ||
                $retourformulaire == 'dossier_instruction' ||
                $retourformulaire == 'dossier_instruction_mes_encours' ||
                $retourformulaire == 'dossier_instruction_tous_encours' ||
                $retourformulaire == 'dossier_instruction_mes_clotures' ||
                $retourformulaire == 'dossier_instruction_tous_clotures'
                || $retourformulaire == 'dossier_contentieux_mes_infractions'
                || $retourformulaire == 'dossier_contentieux_toutes_infractions'
                || $retourformulaire == 'dossier_contentieux_mes_recours'
                || $retourformulaire == 'dossier_contentieux_tous_recours'
            )){
                
            //On récuppère le statut du dossier d'instruction        
            $statut = $this->f->getStatutDossier($idxformulaire);
            if ( $this->f->isUserInstructeur() && $statut == "cloture" ){
                return false;
            }
        }
        return true;
    }


    function is_addable(){
        if ($this->f->can_bypass(get_class($this), "ajouter")){
            return true;
        }
        if ($this->is_dossier_instruction_not_closed() === true &&
            $this->is_instructeur_from_division_dossier() === true) {
            return true;
        }
        return false;
    }

    function is_editable(){
        
        if ($this->f->can_bypass(get_class($this), "modifier")){
            return true;
        }
        
        if ($this->is_dossier_instruction_not_closed() === true &&
            $this->is_instructeur_from_division_dossier() === true) {
            return true;
        }
        return false;
    }
    
    function is_deletable(){
        
        if ($this->f->can_bypass(get_class($this), "supprimer")){
            return true;
        }
        
        if ($this->is_dossier_instruction_not_closed() === true &&
            $this->is_instructeur_from_division_dossier() === true) {
            return true;
        }
        return false;
    }

    /*
     * CONDITION - can_user_access_dossier_contexte_ajout
     *
     * Vérifie que l'utilisateur a bien accès au dossier d'instruction passé dans le
     * formulaire d'ajout.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_ajout() {

        ($this->f->get_submitted_get_value('idxformulaire') !== null ? $id_dossier = 
            $this->f->get_submitted_get_value('idxformulaire') : $id_dossier = "");
        //
        if ($id_dossier !== "") {
            require_once "../obj/dossier_instruction.class.php";
            $dossier = new dossier_instruction($id_dossier, $f->db, DEBUG);
            //
            return $dossier->can_user_access_dossier();
        }
        return false;
    }

   /*
     * CONDITION - can_user_access_dossier
     *
     * Vérifie que l'utilisateur a bien accès au dossier lié à la contrainte instanciée.
     * Cette méthode vérifie que l'utilisateur est lié au groupe du dossier, et si le
     * dossier est confidentiel qu'il a accès aux confidentiels de ce groupe.
     * 
     */
    function can_user_access_dossier_contexte_modification() {

        $id_dossier = $this->getVal('dossier');
        //
        if ($id_dossier !== "" && $id_dossier !== null) {
            require_once "../obj/dossier_instruction.class.php";
            $dossier = new dossier_instruction($id_dossier, $f->db, DEBUG);
            //
            return $dossier->can_user_access_dossier();
        }
        return false;
    }

}

?>
