<?php
//$Id: demandeur.class.php 5056 2015-08-19 10:25:20Z nhaye $ 
//gen openMairie le 08/11/2012 14:59

require_once ("../gen/obj/demandeur.class.php");

class demandeur extends demandeur_gen {

    var $required_tag = array("particulier_nom",
                                "personne_morale_denomination",
                                "personne_morale_raison_sociale"
                            );

    /**
     * Constructeur.
     *
     * @param integer  $id   Identifiant de l'enregistrement.
     * @param database $dnu1 Handler de la base de données (deprecated).
     * @param boolean  $dnu2 Debug (deprecated).
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id, $dnu1, $dnu2);
    } // end __construct()

    /**
     * Mise en page.
     *
     * @param formulaire $form Instance de la classe om_formulaire.
     * @param integer    $maj  Identifiant de l'action.
     */
    function setLayout(&$form, $maj){
        // Affichage multi
        $col_coll = 'nolabel col_12';
        if ($_SESSION["niveau"] == 2) {
            $col_coll = 'nolabel col_6';
        }

        // Gestion recherche pétitionnaire fréquent
        $search_fields = '';
        if ($maj == 0 || $maj == 1) {
            if ($form->val['type_demandeur'] === 'petitionnaire' ||
                $form->val['type_demandeur'] === 'avocat' ||
                $form->val['type_demandeur'] === 'bailleur') {
                $search_fields = ' search_fields';
            }
        }

        // Qualité
        $form->setBloc(
            'qualite',
            'D',
            '',
            $col_coll
        );
        
            $form->setFieldset(
                'qualite',
                'D',
                _("Qualité")
            );
            $form->setFieldset(
                'qualite',
                'F'
            );
            
        $form->setBloc('qualite', 'F');

        // Collectivité
        $form->setBloc(
            'om_collectivite',
            'D',
            '',
            $col_coll.$search_fields
        );
        
            $form->setFieldset(
                'om_collectivite',
                'DF',
                _("Collectivité")
        );

        $form->setBloc('om_collectivite', 'F');

        // Etat civil
        $form->setBloc(
            'particulier_civilite',
            'D',
            "",
            "particulier_fields"
        );
        
            $form->setFieldset(
                'particulier_civilite',
                'D',
                _("Etat civil"),
                "group"
            );
                $form->setBloc(
                    'particulier_nom',
                    'D',
                    "",
                    "group".$search_fields
                );
                $form->setBloc('particulier_prenom', 'F');
                $form->setBloc(
                    'particulier_date_naissance',
                    'D',
                    "",
                    "group"
                );
                $form->setBloc('particulier_commune_naissance', 'F');
            $form->setFieldset(
                'particulier_departement_naissance',
                'F'
            );
            
        $form->setBloc('particulier_departement_naissance', 'F');
        
        $form->setBloc(
            'personne_morale_denomination',
            'D',
            "",
            "personne_morale_fields"
        );

            $form->setFieldset(
                'personne_morale_denomination',
                'D',
                _("Personne morale"),
                $search_fields
            );

                $form->setBloc(
                    'personne_morale_denomination',
                    'D',
                    "",
                    "group"
                );
                $form->setBloc('personne_morale_raison_sociale', 'F');

                $form->setBloc(
                    'personne_morale_siret',
                    'D',
                    "",
                    "group"
                );
                $form->setBloc(
                    'personne_morale_categorie_juridique',
                    'F'
                );
                
                $form->setBloc('personne_morale_civilite', 'D', "");
                $form->setBloc('personne_morale_civilite', 'F');
                
                $form->setBloc('personne_morale_nom', 'D', "", "group");
                $form->setBloc('personne_morale_prenom', 'F');

            $form->setFieldset('personne_morale_prenom', 'F');
            
        $form->setBloc('personne_morale_prenom', 'F');
    
        // Adresse
        $form->setFieldset('numero', 'D', _("Adresse"), "");
        
            $form->setBloc('numero', 'D', "", "group");
            $form->setBloc('voie', 'F');
            
            $form->setBloc('complement', 'D', "");
            $form->setBloc('complement', 'F');
            
            $form->setBloc('lieu_dit', 'D', "", "group");
            $form->setBloc('localite', 'F');
            
            $form->setBloc('code_postal', 'D', "", "group");
            $form->setBloc('cedex', 'F');
            
            $form->setBloc('pays', 'D', "", "", "group");
            $form->setBloc('division_territoriale', 'F');
        $form->setFieldset('division_territoriale', 'F');

        // Coordonnées
        $form->setFieldset('telephone_fixe', 'D', _("Coordonnees"), "");
            $form->setBloc('telephone_fixe', 'D', "", "group");
            $form->setBloc('indicatif', 'F');

            $form->setBloc('courriel', 'D', "", "group");
            $form->setBloc('notification', 'F');

        $form->setFieldset('notification', 'F');
    }

    /*
    * Pays par défaut : France
    */
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        parent::setVal($form, $maj, $validation, $db, $DEBUG);
        if($maj == 0) {
             $form->setVal("pays",'France');
        }
    }

    /**
     * Lbellé des champs
     */
    function setLib(&$form,$maj) {
        parent::setLib($form,$maj);
        $form->setLib('qualite', '');
        $form->setLib('om_collectivite', '');
    }

    /*
    * Pays par défaut : France
    */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
       parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);
        if ($maj == 0) {
             $form->setVal("pays",'France');
        }
        // Message d'information concernant la modification des demandeurs
        // fréquents
        if ($maj == 1 and $this->getVal('frequent') == 't') {
            //
            switch ($this->getVal('type_demandeur')) {
                case 'petitionnaire':
                    $type_demandeur = _("Pétitionnaire");
                    break;

                case 'avocat':
                    $type_demandeur = _("Avocat");
                    break;

                case 'bailleur':
                    $type_demandeur = _("Bailleur");
                    break;
            }
            //
            $message = sprintf(_("%s fréquent non modifiable"), $type_demandeur);
            //
            $this->f->displayMessage("info", $message);
        }
    }

    /**
     * Surcharge de la méthode de vérification
     **/
    function verifier($val = array(), &$db = null, $DEBUG = null) {
        parent::verifier($val, $db, $DEBUG);
        // le nom du particulier est obligatoire
        if($this->valF['qualite'] == "particulier" AND $this->valF['particulier_nom'] == "") {
            $this->correct = false;
            $this->addToMessage(_("Le champ")." <span class=\"bold\">".
                                _("particulier_nom")."</span> "._("est obligatoire."));
        }

        // la dénomination ou la raison sociale est obligatoire pour une personne morale
        if($this->valF['qualite'] == "personne_morale" 
           AND $this->valF['personne_morale_denomination'] == "" 
                AND $this->valF['personne_morale_raison_sociale'] == "") {
            $this->correct = false;
            $this->addToMessage(_("Un des champs")." <span class=\"bold\">".
                                _("personne_morale_denomination")."</span> ou <span class=\"bold\">".
                                _("personne_morale_raison_sociale")."</span> "._("doit etre rempli."));
        }

    }

    /*
     * Select pour les champs qualite et type_demandeur
     */
    function setType(&$form,$maj) {
        parent::setType($form,$maj);
        if ($maj < 2) { //ajouter et modifier
            
            $form->setType('type_demandeur', 'select');
            $form->setType('qualite', 'select');
            if($maj == 0 AND $this->getParameter("idx_demandeur") != "") {
                $form->setType('frequent','hidden');
                $form->setType('qualite','selectdisabled');
                $form->setType('particulier_nom','textdisabled');
                $form->setType('particulier_prenom','textdisabled');
                $form->setType('particulier_date_naissance','datedisabled');
                $form->setType('particulier_commune_naissance','textdisabled');
                $form->setType('particulier_departement_naissance','textdisabled');
                $form->setType('personne_morale_denomination','textdisabled');
                $form->setType('personne_morale_raison_sociale','textdisabled');
                $form->setType('personne_morale_siret','textdisabled');
                $form->setType('personne_morale_categorie_juridique','textdisabled');
                $form->setType('personne_morale_nom','textdisabled');
                $form->setType('personne_morale_prenom','textdisabled');
                $form->setType('particulier_civilite','selectdisabled');
                $form->setType('personne_morale_civilite','selectdisabled');
                $form->setType('telephone_fixe','textdisabled');
                $form->setType('telephone_mobile','textdisabled');
                $form->setType('fax','textdisabled');
                $form->setType('indicatif','textdisabled');
                $form->setType('courriel','textdisabled');

            }
        }
        $form->setType('demandeur', 'hidden');

    }
    
    /**
     * Select pour le type_demandeur et qualite
     *
     * @param om_formulaire $form  Instance de formulaire.
     * @param integer       $maj   Numéro de l'action.
     * @param om_database   $db    Instance de l'abstracteur de base de données.
     * @param boolean       $debug Flag d'activation du debug.
     */
    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        parent::setSelect($form, $maj,$db,$debug);

        // Type du demandeur
        $contenu=array();

        $contenu[0][0]="";
        $contenu[1][0]=_('choisir')." "._('type_demandeur');
        $contenu[0][2]="petitionnaire";
        $contenu[1][2]=_('petitionnaire');
        $contenu[0][1]="delegataire";
        $contenu[1][1]=_('autre correspondant');
        $contenu[0][3]="plaignant";
        $contenu[1][3]=_('Plaignant');
        $contenu[0][4]="contrevenant";
        $contenu[1][4]=_('Contrevenant');
        $contenu[0][5]="requerant";
        $contenu[1][5]=_('Requérant');
        $contenu[0][6]="avocat";
        $contenu[1][6]=_('Avocat');
        $contenu[0][7]="bailleur";
        $contenu[1][7]=_('Bailleur');
        
        $form->setSelect("type_demandeur", $contenu);
        
        // Qualité du demandeur
        $contenu=array();

        $contenu[0][0]="particulier";
        $contenu[1][0]=_('particulier');
        $contenu[0][1]="personne_morale";
        $contenu[1][1]=_('personne morale');
        
        $form->setSelect("qualite", $contenu);
    }
    
    /*
     * Ajoute l'action javascript sur le select de la qualité
     */
    function setOnchange(&$form,$maj){
        parent::setOnchange($form,$maj);
        
        $form->setOnchange("qualite","changeDemandeurType('qualite');");
    }

    /**
     * Ajout d'un champs caché permettant de linker l'id du demandeur
     * recement ajouté
     **/
    function sousFormSpecificContent($maj) {
        $id_demandeur = $this->getVal("demandeur");
        if(isset($this->valF["demandeur"]) AND !empty($this->valF["demandeur"])) {
            echo "<input id=\"id_retour\" name=\"idRetour\" type=\"hidden\" value=\"".
                    $this->valF["demandeur"]."\" />";
        } elseif (isset($id_demandeur) AND !empty($id_demandeur) AND $maj == 110) {
            echo "<input id=\"id_retour\" name=\"idRetour\" type=\"hidden\" value=\"".
                    $this->getVal("demandeur")."\" />";
        }
    }
    /**
     * Surcharge du lien de retour permettant de linker l'id du demandeur
     * recement ajouté
     **/
    function retoursousformulaire($idxformulaire = NULL, $retourformulaire = NULL, $val = NULL,
                                  $objsf = NULL, $premiersf = NULL, $tricolsf = NULL, $validation = NULL,
                                  $idx = NULL, $maj = NULL, $retour = NULL) {
        if($retourformulaire === "demande") {
            echo "\n<a class=\"retour\" ";
            echo "href=\"#\">";
            //
            echo _("Retour");
            //
            echo "</a>\n";
        } else {
            parent::retoursousformulaire($idxformulaire, $retourformulaire, $val,
                                  $objsf, $premiersf, $tricolsf, $validation,
                                  $idx, $maj, $retour);
        }
    }

    /**
     * Ajout du paramètre principal
     */
    function getDataSubmitSousForm() {
        /*Création du lien de validation du sous-formulaire*/
        $datasubmit = "";
        $datasubmit .= "../scr/sousform.php";
        $datasubmit .= "?";
        $datasubmit .= "obj=".get_class($this);
        $datasubmit .= "&amp;validation=".$this->getParameter("validation");
        if ($this->getParameter("idx") != "]") {
            //
            if ($this->getParameter("maj") == 1) { // modifier
                $datasubmit .= "&amp;action=1";
                $datasubmit .= "&amp;idx=".$this->getParameter("idx");
            } else { // supprimer
                $datasubmit .= "&amp;action=2";
                $datasubmit .= "&amp;idx=".$this->getParameter("idx");
            }
        }
        $datasubmit .= "&amp;premiersf=".$this->getParameter("premiersf");
        $datasubmit .= "&amp;retourformulaire=".$this->getParameter("retourformulaire");
        $datasubmit .= "&amp;trisf=".$this->getParameter("tricolsf");
        $datasubmit .= "&amp;idxformulaire=".$this->getParameter("idxformulaire");
        $datasubmit .= "&amp;principal=".$this->getParameter("principal");
        //
        return $datasubmit;
    }

    /**
     * Synthèse des demandeurs pour le formulaire de la demande.
     *
     * @param string  $type     Type de demandeur.
     * @param boolean $linkable Affiche le lien d'édition.
     */
    function afficherSynthese($type, $linkable) {
        // Récupération du type de demandeur pour l'affichage
        switch ($type) {
            case 'petitionnaire_principal':
                $legend = _("Petitionnaire principal");
                break;

            case 'delegataire':
                $legend = _("Autre correspondant");
                break;
            
            case 'petitionnaire':
                $legend = _("Petitionnaire");
                break;
                
            case 'contrevenant_principal':
                $legend = _("Contrevenant principal");
                break;
                
            case 'contrevenant':
                $legend = _("Contrevenant");
                break;
                
            case 'plaignant_principal':
                $legend = _("Plaignant principal");
                break;
            
            case 'plaignant':
                $legend = _("Plaignant");
                break;
                
            case 'requerant_principal':
                $legend = _("Requérant principal");
                break;
            
            case 'requerant':
                $legend = _("Requérant");
                break;
            
            case 'avocat_principal':
                $legend = _("Avocat principal");
                break;
            
            case 'avocat':
                $legend = _("Avocat");
                break;

            case 'bailleur_principal':
                $legend = _("Bailleur principal");
                break;
            
            case 'bailleur':
                $legend = _("Bailleur");
                break;
        }
        
        // Conteneur du demandeur
        echo "<div class=\"".$type." col_3\" id=\"".
                $type."_".$this->val[array_search('demandeur', $this->champs)].
            "\">\n";
        echo "<div class=\"legend_synthese_demandeur\">\n";
        echo $legend;
        echo "</div>\n";
        echo "<div class=\"synthese_demandeur\">\n";
        // Si le paramètre linkable est défini à true on ajoute les balises
        // de lien
        if ($linkable === true) {
            echo "<a href=\"#\" onclick=\"removeDemandeur('".
                $type."_".$this->val[array_search('demandeur', $this->champs)].
                "'); return false;\">
                <span
                class=\"demandeur_del om-icon om-icon-16 om-icon-fix delete-16\"
                title=\""._("Supprimer le demandeur")."\"
                >"._("Supprimer le demandeur")."</span></a>";
        }
        $input_name = $type.'[]';

        // Valeur de formulaire à retourner
        printf(
            "<input type=\"hidden\" class=\"demandeur_id\" name=\"%s\" value=\"%s\" />\n",
            $input_name,
            $this->val[array_search('demandeur', $this->champs)]
        );

        // Lien de modification du demandeur
        if ($linkable === true) {
            echo "<a class=\"edit_demandeur\" href=\"#\" onclick=\"
                editDemandeur('".
                $this->val[array_search('type_demandeur', $this->champs)]."',".
                $this->val[array_search('demandeur', $this->champs)].",'".
                $type."',".
                $type."_".$this->val[array_search('demandeur', $this->champs)].
                ");return false;\">\n";
        }
        
        // Affichage des infos du demandeur
        if ($this->val[array_search('qualite', $this->champs)] == 'particulier') {
            if (!empty($this->val[array_search('particulier_civilite', $this->champs)])) {
                $sql = "SELECT libelle FROM ".DB_PREFIXE."civilite WHERE civilite=".
                            $this->val[array_search('particulier_civilite', $this->champs)];
                $civilite = $this->db->getone($sql);
                $this->f->addToLog("afficherSynthese() : db->getone(\"".$sql."\");", VERBOSE_MODE);
                if ( database::isError($civilite)) {
                    die();
                }
                echo $civilite." ";
            }
            echo $this->val[array_search('particulier_nom', $this->champs)]." ".
                $this->val[array_search('particulier_prenom', $this->champs)]."<br/>\n";
        } else {
            echo $this->val[array_search('personne_morale_raison_sociale', $this->champs)]." ".
                $this->val[array_search('personne_morale_denomination', $this->champs)]."<br/>\n";
                if (!empty($this->val[array_search('personne_morale_civilite', $this->champs)])) {
                    $sql = "SELECT libelle FROM ".DB_PREFIXE."civilite WHERE civilite=".
                                $this->val[array_search('personne_morale_civilite', $this->champs)];
                    $civilite = $this->f->db->getone($sql);
                    $this->f->addToLog("afficherSynthese() : db->getone(\"".$sql."\");", VERBOSE_MODE);
                    if ( database::isError($civilite)) {
                        die();
                    }
                    echo $civilite." ";
                }
                echo $this->val[array_search('personne_morale_nom', $this->champs)]." ".
                $this->val[array_search('personne_morale_prenom', $this->champs)]."<br/>\n";
        }
        echo (($this->val[array_search('numero', $this->champs)] != "")? 
            $this->val[array_search('numero', $this->champs)]." " : "").
        (($this->val[array_search('voie', $this->champs)] != "")? 
            $this->val[array_search('voie', $this->champs)]." " : "").
        (($this->val[array_search('complement', $this->champs)] != "")? 
            $this->val[array_search('complement', $this->champs)]." " : "")."<br/>\n".
        (($this->val[array_search('code_postal', $this->champs)] != "")? 
            $this->val[array_search('code_postal', $this->champs)]." " : "").
        (($this->val[array_search('localite', $this->champs)] != "")? 
            $this->val[array_search('localite', $this->champs)]." " : "").
        (($this->val[array_search('bp', $this->champs)] != "")? 
            $this->val[array_search('bp', $this->champs)]." " : "").
        (($this->val[array_search('cedex', $this->champs)] != "")? 
            $this->val[array_search('cedex', $this->champs)]." " : "")."<br/>\n".
        (($this->val[array_search('telephone_fixe', $this->champs)] != "")? 
            $this->val[array_search('telephone_fixe', $this->champs)]." " : "").
        (($this->val[array_search('telephone_mobile', $this->champs)] != "")? 
            $this->val[array_search('telephone_mobile', $this->champs)]." " : "");
        if ($linkable === true) {
            echo "</a>\n";
        }
        echo "</div>\n";
        echo "</div>\n";
    }

    /**
     *
     */
    function get_inst_civilite($civilite) {
        return $this->get_inst_common("civilite", $civilite);
    }

    /**
     * Retourne un tableau avec les données principales du demandeur.
     *
     * L'objectif est de mettre à disposition via un WS Reste un ensemble
     * de données exploitable par une autre application.
     */
    function get_datas() {

        /**
         *
         */
        $particulier_civilite = "";
        $personne_morale_civilite = "";
        if ($this->getVal('qualite') == 'particulier'
            && $this->getVal('particulier_civilite') !== '') {
            //
            $inst_civilite = $this->get_inst_civilite($this->getVal('particulier_civilite'));
            $particulier_civilite = $inst_civilite->getVal("libelle");
        } elseif ($this->getVal('qualite') == 'personne_morale'
            && $this->getVal('personne_morale_civilite') !== '') {
            //
            $inst_civilite = $this->get_inst_civilite($this->getVal('personne_morale_civilite'));
            $personne_morale_civilite = $inst_civilite->getVal("libelle");
        }

        /**
         *
         */
        //
        $datas = array(
            "demandeur" => $this->getVal($this->clePrimaire),
            "qualite" => $this->getVal("qualite"),
        );

        if ($this->getVal('qualite') == 'particulier') {
            $datas["particulier_civilite"] = $particulier_civilite;
            $datas["particulier_nom"] = $this->getVal("particulier_nom");
            $datas["particulier_prenom"] = $this->getVal("particulier_prenom");
            $datas["particulier_date_naissance"] = $this->getVal("particulier_date_naissance");
            $datas["particulier_commune_naissance"] = $this->getVal("particulier_commune_naissance");
            $datas["particulier_departement_naissance"] = $this->getVal("particulier_departement_naissance");
        }
        if ($this->getVal('qualite') == 'personne_morale') {
            $datas["personne_morale_civilite"] = $personne_morale_civilite;
            $datas["personne_morale_denomination"] = $this->getVal("personne_morale_denomination");
            $datas["personne_morale_raison_sociale"] = $this->getVal("personne_morale_raison_sociale");
            $datas["personne_morale_siret"] = $this->getVal("personne_morale_siret");
            $datas["personne_morale_categorie_juridique"] = $this->getVal("personne_morale_categorie_juridique");
            $datas["personne_morale_nom"] = $this->getVal("personne_morale_nom");
            $datas["personne_morale_prenom"] = $this->getVal("personne_morale_prenom");
        }
        //
        $datas["numero"] = $this->getVal("numero");
        $datas["voie"] = $this->getVal("voie");
        $datas["complement"] = $this->getVal("complement");
        $datas["lieu_dit"] = $this->getVal("lieu_dit");
        $datas["localite"] = $this->getVal("localite");
        $datas["code_postal"] = $this->getVal("code_postal");
        $datas["bp"] = $this->getVal("bp");
        $datas["cedex"] = $this->getVal("cedex");
        $datas["pays"] = $this->getVal("pays");
        $datas["division_territoriale"] = $this->getVal("division_territoriale");
        $datas["telephone_fixe"] = $this->getVal("telephone_fixe");
        $datas["telephone_mobile"] = $this->getVal("telephone_mobile");
        $datas["indicatif"] = $this->getVal("indicatif");
        $datas["courriel"] = $this->getVal("courriel");
        $datas["fax"] = $this->getVal("fax");

        /**
         *
         */
        return $datas;
    }

}// fin classe
?>
