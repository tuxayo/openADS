<?php
/**
 * DBFORM - 'etat' - Surcharge gen.
 *
 * Ce script permet de définir la classe 'etat'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../gen/obj/etat.class.php";

class etat extends etat_gen {

    /**
     * Constructeur.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        //type
        if ($maj==0){ //ajout
            $form->setType('statut', 'select');
            $form->setType('evenement','select_multiple');
        }// fin ajout
        if ($maj==1){ //modifier
            $form->setType('statut', 'select');
            $form->setType('evenement','select_multiple');
        }// fin modifier
        if ($maj==2){ //supprimer
            $form->setType('statut', 'selectstatic');
            $form->setType('evenement','select_multiple_static');
        }//fin supprimer
        if ($maj==3){ //consulter
            $form->setType('statut', 'selectstatic');
            $form->setType('evenement','select_multiple_static');
        }//fin consulter
    }

    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        //
        parent::setSelect($form, $maj, $db, $debug);
        //
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");
        // evenement
        $this->init_select($form, $db, $maj, $debug, "evenement",
                           $sql_evenement, $sql_evenement_by_id, false, true);
        // Statut
        $contenu = array(
            0 => array('cloture', 'encours',),
            1 => array(_('Cloture'), _('En cours'),)
        );
        $form->setSelect("statut",$contenu);
    }
    
    //Nombre de evenement affiché
    function setTaille(&$form, $maj) {
        
        parent::setTaille($form, $maj);
        $form->setTaille("evenement", 5);
    }
    
    //Nombre de evenement maximum
    function setMax(&$form, $maj) {
        
        parent::setMax($form, $maj);
        $form->setMax("evenement", 5);
    }
    
    //Ajoute autant de transitions que d'événements
    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {
        
        parent::triggerajouterapres($id,$db,$val,$DEBUG);

        //Récupère les données du select multiple
        $evenements = $this->getPostedValues('evenement');
        
        //Ne traite les données que s'il y en a et qu'elles sont correctes
        if ( is_array($evenements) && count($evenements) > 0 ){
            
            $nb_tr = 0;    
            //Va créer autant de transition que d'événements choisis
            foreach( $evenements as $value ){
            
                //Test si la valeur par défaut est sélectionnée
                if ( $value != "" ) {
                    
                    //Données
                    $donnees = array(
                        'etat' => $this->valF['etat'],
                        'evenement' => $value
                    );
                    
                    //Ajoute une nouvelle transition
                    $this->addTransition($donnees, $db, $DEBUG);

                    $nb_tr++;
                }
            }
            
            //Message de confirmation de création de(s) transition(s).
            if ( $nb_tr > 0 ){
                
                if ( $nb_tr == 1 ){
                    
                    $this->addToMessage(_("Creation de ").$nb_tr._(" nouvelle transition 
                        realisee avec succes."));
                }
                else{
                    
                    $this->addToMessage(_("Creation de ").$nb_tr._(" nouvelles transitions 
                        realisee avec succes."));
                }
            }
        }
        
    }
    
    //Fonction générique permettant de récupérer les données d'un champ postées
    function getPostedValues($champ) {
        
        // Récupération des demandeurs dans POST
        if ($this->f->get_submitted_post_value($champ) !== null) {
            
            return $this->f->get_submitted_post_value($champ);
        }
    }
    
    //Modification des liens
    function triggermodifierapres($id, &$db = null, $val = array(), $DEBUG = null) {
        
        parent::triggermodifierapres($id, $db, $val, $DEBUG);
        
        //Supprime toutes les transitions liées à l'état
        $this->deleteAllTransitionEtat($this->valF['etat'], $db, $DEBUG);
        
        //Récupère les données du select multiple
        $evenements = $this->getPostedValues('evenement');
        
        //Ne traite les données que s'il y en a et qu'elles sont correctes
        if ( is_array($evenements) && count($evenements) > 0 ){
            
            $nb_tr = 0;    
            //Va créer autant de transition que d'événements choisis
            foreach( $evenements as $value ){
            
                //Test si la valeur par défaut est sélectionnée
                if ( $value != "" ) {
                    
                    //Données
                    $donnees = array(
                        'etat' => $this->valF['etat'],
                        'evenement' => $value
                    );
                    
                    //Ajoute une nouvelle transition
                    $this->addTransition($donnees, $db, $DEBUG);

                    $nb_tr++;
                }
            }
            
            //Message de confirmation de création de(s) transition(s).
            if ( $nb_tr > 0 ){
                
                $this->addToMessage(_("Mise a jour des liaisons avec transition realisee avec succes."));
            }
        }
    }

    //Ajoute une nouvelle transition
    // $data array de données
    function addTransition($data, $db, $DEBUG){
        
        require_once '../obj/transition.class.php';
        
        $transition = new transition("]",$db,$DEBUG);
        $transition->valF = "";
    
        //Données
        $valTransi['transition']=NULL;
    
        if ( is_array($data) ){
            
            foreach ($data as $key => $value) {
                
                $valTransi[$key]=$value;
            }            
        }
    
        $transition->ajouter($valTransi, $db, $DEBUG);
    }

    //Supprime toutes les transitions liées à un état
    function deleteAllTransitionEtat($id, $db, $DEBUG){
            
        //Création de la requête
        $sql = "DELETE FROM ".DB_PREFIXE."transition WHERE etat like '$id'";
        
        //Exécution de la requête
        $res = $db->query($sql);
        
        //Ajout au log
        $this->f->addToLog("deleteAllTransitionEtat(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ( database::isError($res)){
            die();
        }
    }

    //Suppression de toutes les liaisons avec transition
    function triggersupprimer($id, &$db = null, $val = array(), $DEBUG = null) {
                
        //Supprime toutes les transitions liées à l'état
        $this->deleteAllTransitionEtat($id, $db, $DEBUG);
    }
    
    /* Surcharge de la fonction cleSecondaire pour qu'elle ne vérifie pas le lien avec 
     * transition qui sera supprimé juste après*/ 
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {

        // Verification de la cle secondaire : dossier
        $this->rechercheTable($db, "dossier", "etat", $id);
        // Verification de la cle secondaire : evenement
        $this->rechercheTable($db, "evenement", "etat", $id);
        // Verification de la cle secondaire : instruction
        $this->rechercheTable($db, "instruction", "etat", $id);
    }
    
    //Affichage des evenement anciennement liés
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        
        parent::setVal($form, $maj, $validation, $db, $DEBUG);
        
        if($maj == 2 && $validation == 1 ) {
             $form->setVal("evenement",$this->val[3]);
        }
    }

}

?>
