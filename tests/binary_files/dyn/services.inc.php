<?php
/**
 * Contient les paramètres de connexion aux réferentiels.
 *
 * @package openads
 * @version SVN : $Id$
 */

/**
 * Référentiel ERP
 */
// L'URL du référentiel ERP qui reçoit les messages sortants
$ERP_URL_MESSAGES = 'http://localhost/openads/tests_services/rest_entry.php/referentiel_erp_test/';

?>
