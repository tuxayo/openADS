*** Settings ***
Documentation  Rubrique "Export / Import".

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Effectuer un export SITADEL
    [Documentation]  Vérifie que les utilisateurs avec différents profils
    ...  peuvent effectuer un export SITADEL

    # Profil CELLULE SUIVI
    Effectuer un export SITADEL avec l'utilisateur  suivi  suivi
    # Profil ADMINISTRATEUR GENERAL
    Effectuer un export SITADEL avec l'utilisateur  admingen  admingen
    # Profil INSTRUCTEUR POLYVALENT
    Effectuer un export SITADEL avec l'utilisateur  instrpoly  instrpoly
    # Profil INSTRUCTEUR POLYVALENT COMMUNE
    Effectuer un export SITADEL avec l'utilisateur  instrpolycomm  instrpolycomm
    # Profil GUICHET ET SUIVI
    Effectuer un export SITADEL avec l'utilisateur  guichetsuivi  guichetsuivi


Vérification de l'export SITADEL, des problèmes de cohérence et des champs de fusion

    [Documentation]  Vérifie le contenu du fichier SITADEL généré, le fichier
    ...  des problèmes de cohérence et les champs de fusion en rapport avec les
    ...  champs requis par SITADEL (évite de dupliquer les tests).

    ##
    ## Concernant le nouveau tableau des SHON
    ##

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Neville
    ...  particulier_prenom=Artois
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    # On modifie les valeurs du premier tableau des surfaces dans les données
    # techniques
    Depuis la page d'accueil  instr   instr
    &{donnees_techniques_values} =  Create Dictionary
    ...  su_avt_shon1=214
    ...  su_avt_shon2=213
    ...  su_avt_shon3=219
    ...  su_avt_shon4=224
    ...  su_avt_shon5=217
    ...  su_avt_shon6=218
    ...  su_avt_shon7=218
    ...  su_avt_shon8=218
    ...  su_avt_shon9=218
    ...  su_cstr_shon1=563
    ...  su_cstr_shon2=218
    ...  su_cstr_shon3=218
    ...  su_cstr_shon4=218
    ...  su_cstr_shon5=218
    ...  su_cstr_shon6=218
    ...  su_cstr_shon7=218
    ...  su_cstr_shon8=218
    ...  su_cstr_shon9=218
    ...  su_chge_shon1=218
    ...  su_chge_shon2=218
    ...  su_chge_shon3=218
    ...  su_chge_shon4=218
    ...  su_chge_shon5=218
    ...  su_chge_shon6=218
    ...  su_chge_shon7=218
    ...  su_chge_shon8=218
    ...  su_chge_shon9=218
    ...  su_demo_shon1=318
    ...  su_demo_shon2=218
    ...  su_demo_shon3=218
    ...  su_demo_shon4=218
    ...  su_demo_shon5=218
    ...  su_demo_shon6=218
    ...  su_demo_shon7=218
    ...  su_demo_shon8=218
    ...  su_demo_shon9=218
    ...  su_sup_shon1=218
    ...  su_sup_shon2=218
    ...  su_sup_shon3=218
    ...  su_sup_shon4=218
    ...  su_sup_shon5=218
    ...  su_sup_shon6=218
    ...  su_sup_shon7=218
    ...  su_sup_shon8=218
    ...  su_sup_shon9=218
    Modifier les données techniques pour le calcul des surfaces  ${di}  ${donnees_techniques_values}

    # On génère le fichier SITADEL
    Depuis la page d'accueil  guichetsuivi   guichetsuivi
    Depuis le formulaire de génération de l'export SITADEL
    Input Text  datedebut  ${DATE_FORMAT_DD/MM/YYYY}
    Input Text  datefin  ${DATE_FORMAT_DD/MM/YYYY}
    Cliquer sur le bouton export SITADEL
    Valid Message Should Contain  Fichier SITADEL
    Valid Message Should Contain  sauvegarde
    # On télécharge le fichier CSV SITADEL
    ${link_SITADEL} =  Get Element Attribute  css=.ui-state-valid span.text a@href
    ${output_dir}  ${output_name} =  Télécharger un fichier  ${SESSION_COOKIE}  ${link_SITADEL}  ${EXECDIR}${/}binary_files${/}
    ${full_path_to_file} =  Catenate  SEPARATOR=  ${output_dir}  ${output_name}
    # On vérifie que dans le fichier téléchargé les valeurs concernant le SHON
    # sont issues de la colonne "avt" du premier tableau des surfaces
    ${content_file} =  Get File  ${full_path_to_file}
    ${expected_content_file} =  Set Variable  214|213|219|224|217|218|
    Should Contain  ${content_file}  ${expected_content_file}

    # On vérifie l'affichage des champs de fusion qui devraient avoir le même
    # comportement que dans SITADEL
    Depuis la page d'accueil  admin  admin
    # On ajoute une lettre type
    &{args_lettretype} =  Create Dictionary
    ...  id=lettre_info_su
    ...  libelle=Lettre d information des surfaces
    ...  sql=Récapitulatif du dossier d'instruction / instruction
    ...  titre=<p>Total surface créée : [su_cstr_shon_tot_donnees_techniques]</p><p>Total surface supprimée : [su_demo_shon_tot_donnees_techniques]</p><p>Total : [su_tot_shon_tot_donnees_techniques]</p><p>[tab_surface_donnees_techniques]</p>
    ...  corps=<p><br pagebreak="true" /></p><p>Total surface créée : [su_cstr_shon_tot_donnees_techniques]</p><p>Total surface supprimée : [su_demo_shon_tot_donnees_techniques]</p><p>Total : [su_tot_shon_tot_donnees_techniques]</p><p>[tab_surface_donnees_techniques]</p>
    ...  actif=true
    ...  collectivite=agglo
    Ajouter la lettre-type depuis le menu  &{args_lettretype}
    # On ajoute un événement
    @{etat_source} =  Create List  delai de notification envoye
    @{type_di} =  Create List  PCI - P - Initial
    &{args_evenement} =  Create Dictionary
    ...  libelle=Lettre information surfaces
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  lettretype=lettre_info_su Lettre d information des surfaces
    Ajouter l'événement depuis le menu  ${args_evenement}
    # On ajoute l'événement d'instruction au dossier
    Depuis la page d'accueil  instr  instr
    Depuis le contexte du dossier d'instruction  ${di}
    Ajouter une instruction au DI  ${di}  Lettre information surfaces
    # On contrôle le contenu du PDF
    Depuis l'instruction du dossier d'instruction  ${di}  Lettre information surfaces
    Click On SubForm Portlet Action  instruction  edition
    Open PDF  sousform
    # On contrôle le titre
    PDF Page Number Should Contain  1  Total surface créée : 2307
    PDF Page Number Should Contain  1  Total surface supprimée : 2062
    PDF Page Number Should Contain  1  Total : 2204
    PDF Page Number Should Contain  1  Habitation - 563 m²
    # On contrôle le corps
    PDF Page Number Should Contain  2  Total surface créée : 2307
    PDF Page Number Should Contain  2  Total surface supprimée : 2062
    PDF Page Number Should Contain  2  Total : 2204
    PDF Page Number Should Contain  2  Habitation - 563 m²
    # On ferme le PDF
    Close PDF

    # On ajoute des valeurs sur le second tableau des surfaces pour qu'il soit
    # récupéré pour le fichier SITADEL à la place des valeurs du tableau 1
    Depuis la page d'accueil  instr   instr
    &{donnees_techniques_values} =  Create Dictionary
    ...  su2_avt_shon1=14
    ...  su2_avt_shon2=14
    ...  su2_avt_shon3=87
    ...  su2_avt_shon4=4
    ...  su2_avt_shon5=2
    ...  su2_avt_shon6=22
    ...  su2_avt_shon7=33
    ...  su2_avt_shon8=10
    ...  su2_avt_shon9=44
    ...  su2_avt_shon10=10
    ...  su2_avt_shon11=10
    ...  su2_avt_shon12=10
    ...  su2_avt_shon13=10
    ...  su2_avt_shon14=10
    ...  su2_avt_shon15=10
    ...  su2_avt_shon16=10
    ...  su2_avt_shon17=123
    ...  su2_avt_shon18=47
    ...  su2_avt_shon19=69
    ...  su2_avt_shon20=10
    ...  su2_cstr_shon1=14
    ...  su2_cstr_shon2=6
    ...  su2_cstr_shon3=51
    ...  su2_cstr_shon4=8
    ...  su2_cstr_shon5=8
    ...  su2_cstr_shon6=8
    ...  su2_cstr_shon7=8
    ...  su2_cstr_shon8=8
    ...  su2_cstr_shon9=8
    ...  su2_cstr_shon10=8
    ...  su2_cstr_shon11=8
    ...  su2_cstr_shon12=8
    ...  su2_cstr_shon13=8
    ...  su2_cstr_shon14=8
    ...  su2_cstr_shon15=8
    ...  su2_cstr_shon16=8
    ...  su2_cstr_shon17=8
    ...  su2_cstr_shon18=8
    ...  su2_cstr_shon19=8
    ...  su2_cstr_shon20=8
    ...  su2_chge_shon1=8
    ...  su2_chge_shon2=8
    ...  su2_chge_shon3=8
    ...  su2_chge_shon4=8
    ...  su2_chge_shon5=8
    ...  su2_chge_shon6=8
    ...  su2_chge_shon7=8
    ...  su2_chge_shon8=8
    ...  su2_chge_shon9=8
    ...  su2_chge_shon10=8
    ...  su2_chge_shon11=8
    ...  su2_chge_shon12=8
    ...  su2_chge_shon13=8
    ...  su2_chge_shon14=8
    ...  su2_chge_shon15=8
    ...  su2_chge_shon16=8
    ...  su2_chge_shon17=8
    ...  su2_chge_shon18=8
    ...  su2_chge_shon19=8
    ...  su2_chge_shon20=8
    ...  su2_demo_shon1=19
    ...  su2_demo_shon2=63
    ...  su2_demo_shon3=8
    ...  su2_demo_shon4=8
    ...  su2_demo_shon5=8
    ...  su2_demo_shon6=8
    ...  su2_demo_shon7=8
    ...  su2_demo_shon8=8
    ...  su2_demo_shon9=8
    ...  su2_demo_shon10=8
    ...  su2_demo_shon11=8
    ...  su2_demo_shon12=8
    ...  su2_demo_shon13=8
    ...  su2_demo_shon14=8
    ...  su2_demo_shon15=8
    ...  su2_demo_shon16=8
    ...  su2_demo_shon17=8
    ...  su2_demo_shon18=8
    ...  su2_demo_shon19=8
    ...  su2_demo_shon20=8
    ...  su2_sup_shon1=8
    ...  su2_sup_shon2=8
    ...  su2_sup_shon3=8
    ...  su2_sup_shon4=8
    ...  su2_sup_shon5=8
    ...  su2_sup_shon6=8
    ...  su2_sup_shon7=8
    ...  su2_sup_shon8=8
    ...  su2_sup_shon9=8
    ...  su2_sup_shon10=8
    ...  su2_sup_shon11=8
    ...  su2_sup_shon12=8
    ...  su2_sup_shon13=8
    ...  su2_sup_shon14=8
    ...  su2_sup_shon15=8
    ...  su2_sup_shon16=8
    ...  su2_sup_shon17=8
    ...  su2_sup_shon18=8
    ...  su2_sup_shon19=8
    ...  su2_sup_shon20=8
    Modifier les données techniques pour le calcul des surfaces  ${di}  ${donnees_techniques_values}

    # On génère le fichier SITADEL
    Depuis la page d'accueil  guichetsuivi   guichetsuivi
    Depuis le formulaire de génération de l'export SITADEL
    Input Text  datedebut  ${DATE_FORMAT_DD/MM/YYYY}
    Input Text  datefin  ${DATE_FORMAT_DD/MM/YYYY}
    Cliquer sur le bouton export SITADEL
    Valid Message Should Contain  Fichier SITADEL
    Valid Message Should Contain  sauvegarde
    # On télécharge le fichier CSV SITADEL
    ${link_SITADEL} =  Get Element Attribute  css=.ui-state-valid span.text a@href
    ${output_dir}  ${output_name} =  Télécharger un fichier  ${SESSION_COOKIE}  ${link_SITADEL}  ${EXECDIR}${/}binary_files${/}
    ${full_path_to_file} =  Catenate  SEPARATOR=  ${output_dir}  ${output_name}
    # On vérifie que dans le fichier téléchargé les valeurs concernant le SHON
    # ont changées
    # Correspondance d'après la ligne "avt" testée :
    # 91 = shon3+shon4
    # 44 = shon9
    # 69 = shon19
    # 67 = shon5+shon6+shon7+shon8
    # 0 = pas de correspondance
    # 123 = shon17
    # 28 = shon1+shon2
    # 47 = shon18
    # 80 = shon10+shon11+shon12+shon13+shon14+shon15+shon16+shon20
    ${content_file} =  Get File  ${full_path_to_file}
    ${expected_content_file} =  Set Variable  91|44|69|67|0|123|28|47|80|
    Should Contain  ${content_file}  ${expected_content_file}

    # On vérifie que les champs de fusion utilisent les valeurs du second
    # tableau
    # On contrôle le contenu du PDF
    Depuis l'instruction du dossier d'instruction  ${di}  Lettre information surfaces
    Click On SubForm Portlet Action  instruction  edition
    Open PDF  sousform
    # On contrôle le titre
    PDF Page Number Should Contain  1  Total surface créée : 207
    PDF Page Number Should Contain  1  Total surface supprimée : 226
    PDF Page Number Should Contain  1  Total : 530
    PDF Page Number Should Contain  1  Exploitation agricole - 14 m²
    # On contrôle le corps
    PDF Page Number Should Contain  2  Total surface créée : 207
    PDF Page Number Should Contain  2  Total surface supprimée : 226
    PDF Page Number Should Contain  2  Total : 530
    PDF Page Number Should Contain  2  Exploitation agricole - 14 m²
    # On ferme le PDF
    Close PDF

    ##
    ## Contrôle la colonne de la version du dossier d'instruction (colonne 8)
    ##

    # On ajoute une demande sur laquelle plusieurs modificatifs seront ajoutés
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Lejeune
    ...  particulier_prenom=Nicolas
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    # On clôture le dossier pour ajouter un modificatif
    Depuis la page d'accueil  instrpoly   instrpoly
    Ajouter une instruction au DI et la finaliser  ${di}  accepter un dossier sans réserve

    # On ajoute un modificatif sur le dossier d'instruction
    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande de modification
    ...  dossier_instruction=${di}
    ${di_modif} =  Ajouter la demande par WS  ${args_demande}
    # On clôture le modificatif
    Ajouter une instruction au DI et la finaliser  ${di_modif}  accepter un dossier sans réserve

    # On ajoute un modificatif sur le dossier d'instruction
    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande de modification
    ...  dossier_instruction=${di}
    ${di_modif_2} =  Ajouter la demande par WS  ${args_demande}
    # On clôture le modificatif
    Ajouter une instruction au DI et la finaliser  ${di_modif_2}  accepter un dossier sans réserve

    # On ajoute une DAACT sur le dossier d'instruction pour que le prochain
    # modificatif ait le champ dossier.version à 4
    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande de modification
    ...  dossier_instruction=${di}
    ${di_daact} =  Ajouter la demande par WS  ${args_demande}
    # On clôture le modificatif
    Ajouter une instruction au DI et la finaliser  ${di_daact}  accepter un dossier sans réserve

    # On ajoute un modificatif sur le dossier d'instruction qui doit avoir son
    # indice dans SITADEL (la colonne 8 dde l'export) à 3 malgré sa version à 4
    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande de modification
    ...  dossier_instruction=${di}
    ${di_modif_3} =  Ajouter la demande par WS  ${args_demande}
    # On clôture le modificatif
    Ajouter une instruction au DI et la finaliser  ${di_modif_3}  accepter un dossier sans réserve

    # On génère le fichier SITADEL
    Depuis la page d'accueil  guichetsuivi   guichetsuivi
    Depuis le formulaire de génération de l'export SITADEL
    Input Text  datedebut  ${DATE_FORMAT_DD/MM/YYYY}
    Input Text  datefin  ${DATE_FORMAT_DD/MM/YYYY}
    Cliquer sur le bouton export SITADEL
    Valid Message Should Contain  Fichier SITADEL
    Valid Message Should Contain  sauvegarde
    # On télécharge le fichier CSV SITADEL
    ${link_SITADEL} =  Get Element Attribute  css=.ui-state-valid span.text a@href
    ${output_dir}  ${output_name} =  Télécharger un fichier  ${SESSION_COOKIE}  ${link_SITADEL}  ${EXECDIR}${/}binary_files${/}
    ${full_path_to_file} =  Catenate  SEPARATOR=  ${output_dir}  ${output_name}
    # On vérifie que dans le fichier téléchargé la colonne correspondant à la
    # version ait la bonne valeur pour chaque DI
    ${numero_di} =  Get Substring  ${di}  13  18
    ${year} =  Get Time  year
    ${yy} =  Get Substring  ${year}  2  4
    ${content_file} =  Get File  ${full_path_to_file}
    ${expected_content_file_1} =  Set Variable  DEPOT|PC||||${yy}|${numero_di}||1||NICOLAS|LEJEUNE
    ${expected_content_file_2} =  Set Variable  DECISION|PC||||${yy}|${numero_di}||1|4
    ${expected_content_file_3} =  Set Variable  DEPOT|PC||||${yy}|${numero_di}|1|1||NICOLAS|LEJEUNE
    ${expected_content_file_4} =  Set Variable  MODIFICATIF|PC||||${yy}|${numero_di}|1|1|4
    ${expected_content_file_5} =  Set Variable  DEPOT|PC||||${yy}|${numero_di}|2|1||NICOLAS|LEJEUNE
    ${expected_content_file_6} =  Set Variable  MODIFICATIF|PC||||${yy}|${numero_di}|2|1|4
    ${expected_content_file_7} =  Set Variable  DEPOT|PC||||${yy}|${numero_di}|3|1||NICOLAS|LEJEUNE
    ${expected_content_file_8} =  Set Variable  MODIFICATIF|PC||||${yy}|${numero_di}|3|1|4
    Should Contain  ${content_file}  ${expected_content_file_1}
    Should Contain  ${content_file}  ${expected_content_file_2}
    Should Contain  ${content_file}  ${expected_content_file_3}
    Should Contain  ${content_file}  ${expected_content_file_4}
    Should Contain  ${content_file}  ${expected_content_file_5}
    Should Contain  ${content_file}  ${expected_content_file_6}
    Should Contain  ${content_file}  ${expected_content_file_7}
    Should Contain  ${content_file}  ${expected_content_file_8}


Versement aux archives

    [Documentation]  'Export / Import > Versement Aux Archives'. Cet écran
    ...  permet de mettre à jour le champ 'numéro archive' d'une liste de
    ...  dossiers d'instruction grâce à un fichier CSV.

    ##
    ## Étape n°1
    ##
    ## En tant qu'in profil SUIVI, on accède à l'écran dédié à l'import de CSV
    ## de numéro d'archive, et on vérifie les particularités du formulaire :
    ## - fichier obligatoires
    ## - fichier avec un extension .csv
    ## Ensuite on importe un fichier CSV de test qui met à jour des données
    ## existantes.
    ##
    # On se connecte avec un profil 'SUIVI'
    Depuis la page d'accueil  suivi  suivi
    # On accède à l'acran dédié pour réaliser l'import
    Go To Submenu In Menu  edition  versement_archives
    Page Title Should Be  Export / Import > Versement Aux Archives
    # On remplit le champ "insee"
    Input Text  css=#insee  01234
    # On tente d'ajouter un fichier avec une mauvais extension
    Add File and Expect Error Message Be  fichier  lettre_rar16042013124515.pdf  Le fichier n'est pas conforme à la liste des extension(s) autorisée(s) (.csv). [lettre_rar16042013124515.pdf]
    # On clic sur le bouton "Importer"
    Click Element  css=div.formControls input
    # On vérifie le message d'erreur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Error Message Should Be  Vous n'avez pas sélectionné de fichier à importer.
    # On vérifie que le code insee est toujours celui indiqué par
    # l'utilisateur
    Form Value Should Be  css=#insee  01234
    # On ajoute un fichier correct
    Add File  fichier  versement_archives.csv
    # On clic sur le bouton "Importer"
    Click Element  css=div.formControls input
    # On vérifie qu'il y a le message de validation d'import du csv
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Il y a eu 6 ligne(s) lue(s), 1 ligne(s) acceptée(s), 3 ligne(s) rejetée(s) et 2 ligne(s) ignorée(s)

    # Télécharge le fichier d'export CSV sur le disque
    ${link} =  Get Element Attribute  css=div.message a@href
    ${output_dir}  ${output_name} =  Télécharger un fichier  ${SESSION_COOKIE}  ${link}  ${EXECDIR}${/}binary_files${/}
    ${full_path_to_file} =  Catenate  SEPARATOR=  ${output_dir}  ${output_name}
    # On vérifie dans le fichier téléchargé que l'entête correspond à ce qui est attendu
    ${content_file} =  Get File  ${full_path_to_file}
    ${firstline_csv_file} =  Set Variable  03185;08;PC;1;0;1025W;111111;"ligne ignorée : code insee différent de celui indiqué dans le formulaire."
    Should Contain  ${content_file}  ${firstline_csv_file}


    ##
    ## Étape 2
    ##
    ## En tant que profil 'INSTRUCTEUR', on accède à un dossier concerné pour
    ## vérifier que le numéro d'archive a été mis à jour.
    ##
    # On se connecte en tant que "instr"
    Depuis la page d'accueil  instr  instr
    # On clique sur le dossier d'instruction ("PC 013055 12 00001P0")
    Depuis le contexte du dossier d'instruction  PC 013055 12 00001P0
    # On vérifie le numéro de versement aux archives
    Element Should Contain  css=#numero_versement_archive  1025W 444444


Statistiques d'usage

    [Documentation]  Vérifie l'export mono et multi des statistiques d'usage
    ...  ainsi que la prise en compte des dates

    # Nouvelle collectivité mono NICE
    Depuis la page d'accueil  admin  admin
    Ajouter la collectivité depuis le menu  NICE  mono

    # Création d'un DI de NICE
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Missy
    ...  particulier_prenom=Julien
    ...  om_collectivite=NICE
    &{args_demande} =  Create Dictionary
    ...  date_demande=25/06/2009
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=NICE
    ${di_allauch} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    #
    # MONO
    #
    Depuis la page d'accueil  guichet   guichet
    Depuis le menu des statistiques à la demande
    Click On Link  statistiques_usage
    Choix du format de sortie CSV
    Exécuter la reqmo
    ${link} =  Lien téléchargement CSV
    ${content_file} =  Contenu CSV  ${link}
    # On vérifie dans le fichier téléchargé que l'entête correspond à ce qui est attendu
    ${header_csv_file} =  Set Variable  référence dossier instruction;référence dossier autorisation;commune;division dossier;code type da détaillé;libellé type da détaillé;code type di;libellé type di;identifiant instructeur;nom instructeur;division instructeur;direction instructeur;date dépôt initial;date limite instruction;date décision;état di;total instructions;total consultations;simulation taxes part communale;simulation taxes part départementale;simulation taxes total;description du projet;
    Should Contain  ${content_file}  ${header_csv_file}
    # On vérifie qu'il y a des dossiers mono de collectivité unique
    Should Contain  ${content_file}  MARSEILLE
    Should Not Contain  ${content_file}  NICE

    #
    # MULTI
    #
    Depuis la page d'accueil  admingen   admingen
    Depuis le menu des statistiques à la demande
    Click On Link  statistiques_usage
    Choix du format de sortie CSV
    Exécuter la reqmo
    ${link} =  Lien téléchargement CSV
    ${content_file} =  Contenu CSV  ${link}
    # On vérifie dans le fichier téléchargé que l'entête correspond à ce qui est attendu
    ${header_csv_file} =  Set Variable  référence dossier instruction;référence dossier autorisation;commune;division dossier;code type da détaillé;libellé type da détaillé;code type di;libellé type di;identifiant instructeur;nom instructeur;division instructeur;direction instructeur;date dépôt initial;date limite instruction;date décision;état di;total instructions;total consultations;simulation taxes part communale;simulation taxes part départementale;simulation taxes total;description du projet;
    Should Contain  ${content_file}  ${header_csv_file}
    # On vérifie qu'il y a des dossiers mono de collectivité différente
    Should Contain  ${content_file}  MARSEILLE
    Should Contain  ${content_file}  NICE

    #
    # DATE DE DEPOT
    #
    Click On Link  Retour
    Choix du format de sortie CSV
    Input Text  css=input[name='date_depot_debut']  25/06/2009
    Input Text  css=input[name='date_depot_fin']  25/06/2009
    Exécuter la reqmo
    ${link} =  Lien téléchargement CSV
    ${content_file} =  Contenu CSV  ${link}
    # On vérifie qu'il n'y a que le dossier de nice
    Should Not Contain  ${content_file}  MARSEILLE
    Should Contain  ${content_file}  NICE


Vérification des listes simplifiées
    [Documentation]  On verifie les retours des ReqMo concernant les listes simplifiées


    @{colonne_valeur_dossier_simplifiee} =    Create List
    ...  numéro de dossier
    ...  date de dépôt
    ...  petitionnaire principal
    ...  localisation
    ...  shon
    ...  libellé de la destination

    &{args_export_dossier_simplifiee} =  Create Dictionary
    ...  reqmo=dossier_simplifiee
    ...  dossier_autorisation_type=PC
    ...  nom_champ_debut=date_depot_debut
    ...  date_debut=01/12/2012
    ...  nom_champ_fin=date_depot_fin
    ...  date_fin=01/01/2013
    ...  colonne_valeurs=${colonne_valeur_dossier_simplifiee}
    ...  content=PC 013055 12 00001P0

    @{colonne_valeur_dossier_simplifiee_accordes} =    Create List
    ...  numéro de dossier
    ...  date de décision
    ...  petitionnaire principal
    ...  localisation
    ...  shon
    ...  libellé de la destination

    &{args_export_dossier_simplifiee_accordes} =  Create Dictionary
    ...  reqmo=dossier_simplifiee_accordes
    ...  dossier_autorisation_type=PA
    ...  nom_champ_debut=date_decision_debut
    ...  date_debut=01/08/2010
    ...  nom_champ_fin=date_decision_fin
    ...  date_fin=01/09/2010
    ...  colonne_valeurs=${colonne_valeur_dossier_simplifiee_accordes}
    ...  content=PA 013055 12 00001

    @{colonne_valeur_dossier_simplifiee_deposes} =    Create List
    ...  numéro de dossier
    ...  date de dépôt
    ...  petitionnaire principal
    ...  localisation
    ...  shon
    ...  libellé de la destination

    &{args_export_dossier_simplifiee_deposes} =  Create Dictionary
    ...  reqmo=dossier_simplifiee_deposes
    ...  dossier_autorisation_type=AT
    ...  nom_champ_debut=date_depot_debut
    ...  date_debut=01/12/2012
    ...  nom_champ_fin=date_depot_fin
    ...  date_fin=01/01/2013
    ...  colonne_valeurs=${colonne_valeur_dossier_simplifiee_deposes}
    ...  content=AT 013055 12 00001P0

    @{colonne_valeur_dossier_simplifiee_refuses} =    Create List
    ...  numéro de dossier
    ...  date de décision
    ...  petitionnaire principal
    ...  localisation
    ...  shon
    ...  libellé de la destination

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Vadeboncoeur
    ...  particulier_prenom=Théodore
    ...  om_collectivite=agglo

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=27/11/1994
    ...  om_collectivite=agglo

    Depuis la page d'accueil  admin  admin
    Vérifier List Dans Export Tableau  ${args_export_dossier_simplifiee}
    Vérifier List Dans Export Tableau  ${args_export_dossier_simplifiee_accordes}
    Vérifier List Dans Export Tableau  ${args_export_dossier_simplifiee_deposes}
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    Set Suite Variable  ${di}



    &{args_export_dossier_simplifiee_refuses} =  Create Dictionary
    ...  reqmo=dossier_simplifiee_refuses
    ...  dossier_autorisation_type=PC
    ...  nom_champ_debut=date_decision_debut
    ...  date_debut=27/10/1994
    ...  nom_champ_fin=date_decision_fin
    ...  date_fin=27/12/1994
    ...  colonne_valeurs=${colonne_valeur_dossier_simplifiee_refuses}
    ...  content=${di}

    Ajouter une instruction au DI  ${di}  refus HS  28/11/1994
    Vérifier List Dans Export Tableau  ${args_export_dossier_simplifiee_refuses}

Vérification Listes Détaillées
  [Documentation]  On verifie les retours des ReqMo concernant les listes detaillées
    @{colonne_valeur_dossier_detaillee} =    Create List
    ...  numéro de dossier
    ...  date de dépôt
    ...  date d'ouverture de chantier
    ...  date de demande
    ...  date achèvement
    ...  date prévue de recevabilité
    ...  destination des surfaces
    ...  petitionnaire principal
    ...  localisation
    ...  référence cadastrale
    ...  date de décision
    ...  shon
    ...  architecte
    ...  affectation_surface
    ...  nature des travaux
    ...  nature du financement
    ...  nombre de logements
    ...  autorité compétente
    ...  décision

    &{args_export_dossier_detaillee} =  Create Dictionary
    ...  reqmo=dossier_detaillee
    ...  dossier_autorisation_type=PC
    ...  nom_champ_debut=date_depot_debut
    ...  date_debut=01/12/2012
    ...  nom_champ_fin=date_depot_fin
    ...  date_fin=01/01/2013
    ...  colonne_valeurs=${colonne_valeur_dossier_detaillee}
    ...  content=PC 013055 12 00001P0

    @{colonne_valeur_dossier_detaillee_accordes} =  Create List
    ...  numéro de dossier
    ...  date de dépôt
    ...  date d'ouverture de chantier
    ...  date de demande
    ...  date achèvement
    ...  date prévue de recevabilité
    ...  destination des surfaces
    ...  petitionnaire principal
    ...  localisation
    ...  référence cadastrale
    ...  date de décision
    ...  shon
    ...  affectation_surface
    ...  nature du financement
    ...  nombre de logements
    ...  autorité compétente
    ...  décision

    &{args_export_dossier_detaillee_accordes} =  Create Dictionary
    ...  reqmo=dossier_detaillee_accordes
    ...  dossier_autorisation_type=PA
    ...  nom_champ_debut=date_decision_debut
    ...  date_debut=01/08/2010
    ...  nom_champ_fin=date_decision_fin
    ...  date_fin=01/09/2010
    ...  colonne_valeurs=${colonne_valeur_dossier_detaillee_accordes}
    ...  content=PA 013055 12 00001

    @{colonne_valeur_dossier_detaillee_detail} =    Create List
    ...  numéro de dossier
    ...  date de dépôt
    ...  date d'ouverture de chantier
    ...  date de demande
    ...  date achèvement
    ...  date prévue de recevabilité
    ...  destination des surfaces
    ...  petitionnaire principal
    ...  localisation
    ...  référence cadastrale
    ...  date de décision
    ...  shon
    ...  architecte
    ...  affectation_surface
    ...  nature des travaux
    ...  nature du financement
    ...  nombre de logements
    ...  autorité compétente
    ...  décision

    &{args_export_dossier_detaillee_detail} =  Create Dictionary
    ...  reqmo=dossier_detaillee_detail
    ...  nom_champ_debut=date_decision_debut
    ...  date_debut=01/08/2010
    ...  nom_champ_fin=date_decision_fin
    ...  date_fin=01/09/2010
    ...  colonne_valeurs=${colonne_valeur_dossier_detaillee_detail}
    ...  content=aucun enregistrement

    @{colonne_valeur_dossier_detaillee_refuses} =    Create List
    ...  numéro de dossier
    ...  date de dépôt
    ...  date d'ouverture de chantier
    ...  date de demande
    ...  date achèvement
    ...  date prévue de recevabilité
    ...  destination des surfaces
    ...  petitionnaire principal
    ...  localisation
    ...  référence cadastrale
    ...  date de décision
    ...  shon
    ...  affectation_surface
    ...  nature du financement
    ...  nombre de logements
    ...  autorité compétente
    ...  décision

    &{args_export_dossier_detaillee_refuses} =  Create Dictionary
    ...  reqmo=dossier_detaillee_refuses
    ...  dossier_autorisation_type=PC
    ...  nom_champ_debut=date_decision_debut
    ...  date_debut=27/10/1994
    ...  nom_champ_fin=date_decision_fin
    ...  date_fin=27/12/1994
    ...  colonne_valeurs=${colonne_valeur_dossier_detaillee_refuses}
    ...  content=${di}

    Depuis la page d'accueil  admin  admin
    Vérifier List Dans Export Tableau  ${args_export_dossier_detaillee}
    Vérifier List Dans Export Tableau  ${args_export_dossier_detaillee_accordes}
    Vérifier List Dans Export Tableau  ${args_export_dossier_detaillee_detail}
    Vérifier List Dans Export Tableau  ${args_export_dossier_detaillee_refuses}


Vérification des éditions du module pilotage
    [Documentation]  Test les 3 éditions PDF du module de pilotage


    @{colonne_valeur_dossier_premiers_depots_dttm} =    Create List
    ...  N° DE DOSSIER
    ...  DATE DE DÉPÔT
    ...  PÉTITIONNAIRE PRINCIPAL
    ...  LOCALISATION

    &{args_export_dossier_premiers_depots_dttm} =  Create Dictionary
    ...  reqmo=dossier_premiers_depots_dttm
    ...  dossier_instruction_type=PCI - P - Initial
    ...  nom_champ_debut=date_depot_debut
    ...  date_debut=01/12/2012
    ...  nom_champ_fin=date_depot_fin
    ...  date_fin=01/01/2013
    ...  colonne_valeurs=${colonne_valeur_dossier_premiers_depots_dttm}
    ...  content=PC 013055 12 00001P0

    @{colonne_valeur_dossier_depots_division} =    Create List
    ...  N° DE DOSSIER
    ...  DIVISION
    ...  PÉTITIONNAIRE PRINCIPAL
    ...  LOCALISATION

    &{args_export_dossier_depots_division} =  Create Dictionary
    ...  reqmo=dossier_depots_division
    ...  nom_champ_debut=date_depot_debut
    ...  date_debut=01/12/2012
    ...  nom_champ_fin=date_depot_fin
    ...  date_fin=01/01/2013
    ...  colonne_valeurs=${colonne_valeur_dossier_depots_division}
    ...  content=PC 013055 12 00001P0

    @{colonne_valeur_dossier_transmission_dttm_signature_prefet} =    Create List
    ...  N° DE DOSSIER
    ...  DATE DE RETOUR SIGNATURE
    ...  PÉTITIONNAIRE PRINCIPAL
    ...  LOCALISATION

    &{args_export_dossier_transmission_dttm_signature_prefet} =  Create Dictionary
    ...  reqmo=dossier_transmission_dttm_signature_prefet
    ...  dossier_instruction_type=AT - P - Initiale
    ...  nom_champ_debut=date_retour_signature_debut
    ...  date_debut=07/10/2016
    ...  nom_champ_fin=date_retour_signature_fin
    ...  date_fin=07/12/2016
    ...  colonne_valeurs=${colonne_valeur_dossier_transmission_dttm_signature_prefet}
    ...  not_content=AT 013055 13 00001P0

    Depuis la page d'accueil  admin  admin
    Vérifier List Dans Export PDF  ${args_export_dossier_premiers_depots_dttm}
    Vérifier List Dans Export PDF  ${args_export_dossier_depots_division}
    Vérifier List Dans Export PDF  ${args_export_dossier_transmission_dttm_signature_prefet}
