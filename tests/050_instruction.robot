*** Settings ***
Documentation  Test des événements d'instruction.

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Création du jeu de données

    [Documentation]  Constitue le jeu de données.

    #
    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=Notaire&Co
    ...  personne_morale_raison_sociale=Société
    ...  personne_morale_civilite=Monsieur
    ...  personne_morale_nom=Martin
    ...  personne_morale_prenom=Nicolas
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE

    ${di_ok} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}


    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_civilite=Monsieur
    ...  particulier_nom=Odo
    ...  particulier_prenom=Laurent
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE

    ${di_bible_consultation} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Set Suite Variable  ${di_bible_consultation}

    Depuis la page d'accueil  admin  admin

    Ajouter une consultation depuis un dossier  ${di_bible_consultation}  59.01 - Direction de l'Eau et de l'Assainissement
    Ajouter une consultation depuis un dossier  ${di_bible_consultation}  59.01 - SERAM


    Depuis la page d'accueil  consu  consu
    &{args_avis_consultation} =  Create Dictionary
    ...  avis_consultation=Favorable
    ...  motivation=Test
    Rendre l'avis sur la consultation du dossier  ${di_bible_consultation}  ${args_avis_consultation}

     Depuis la page d'accueil  admin  admin

    # Liste des valeurs pour le tableau des surfaces des données techniques
    &{donnees_techniques_values} =  Create Dictionary
    ...  su_avt_shon1=10
    ...  su_avt_shon2=10
    ...  su_avt_shon3=10
    ...  su_avt_shon4=10
    ...  su_avt_shon5=10
    ...  su_avt_shon6=10
    ...  su_avt_shon7=10
    ...  su_avt_shon8=10
    ...  su_avt_shon9=10
    ...  su_cstr_shon1=10
    ...  su_cstr_shon2=10
    ...  su_cstr_shon3=10
    ...  su_cstr_shon4=10
    ...  su_cstr_shon5=10
    ...  su_cstr_shon6=10
    ...  su_cstr_shon7=10
    ...  su_cstr_shon8=10
    ...  su_cstr_shon9=10
    ...  su_chge_shon1=10
    ...  su_chge_shon2=10
    ...  su_chge_shon3=10
    ...  su_chge_shon4=10
    ...  su_chge_shon5=10
    ...  su_chge_shon6=10
    ...  su_chge_shon7=10
    ...  su_chge_shon8=10
    ...  su_chge_shon9=10
    ...  su_demo_shon1=10
    ...  su_demo_shon2=10
    ...  su_demo_shon3=10
    ...  su_demo_shon4=10
    ...  su_demo_shon5=10
    ...  su_demo_shon6=10
    ...  su_demo_shon7=10
    ...  su_demo_shon8=10
    ...  su_demo_shon9=10
    ...  su_sup_shon1=10
    ...  su_sup_shon2=10
    ...  su_sup_shon3=10
    ...  su_sup_shon4=10
    ...  su_sup_shon5=10
    ...  su_sup_shon6=10
    ...  su_sup_shon7=10
    ...  su_sup_shon8=10
    ...  su_sup_shon9=10
    Modifier les données techniques pour le calcul des surfaces  ${di_ok}  ${donnees_techniques_values}

    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_civilite=Monsieur
    ...  particulier_nom=Smith
    ...  particulier_prenom=John
    ...  om_collectivite=MARSEILLE

    ${di_ko} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    #
    #
    Ajouter une instruction au DI  ${di_ko}  Consultation ERP ET IGH
    # Liste des valeurs pour le tableau des surfaces des données techniques
    &{donnees_techniques_values} =  Create Dictionary
    ...  su_avt_shon1=10
    ...  su_avt_shon2=10
    ...  su_avt_shon3=10
    ...  su_avt_shon4=10
    ...  su_avt_shon5=10
    ...  su_avt_shon6=10
    ...  su_avt_shon7=10
    ...  su_avt_shon8=10
    ...  su_avt_shon9=10
    ...  su_cstr_shon1=10
    ...  su_cstr_shon2=10
    ...  su_cstr_shon3=10
    ...  su_cstr_shon4=10
    ...  su_cstr_shon5=10
    ...  su_cstr_shon6=10
    ...  su_cstr_shon7=10
    ...  su_cstr_shon8=10
    ...  su_cstr_shon9=10
    ...  su_chge_shon1=10
    ...  su_chge_shon2=10
    ...  su_chge_shon3=10
    ...  su_chge_shon4=10
    ...  su_chge_shon5=10
    ...  su_chge_shon6=10
    ...  su_chge_shon7=10
    ...  su_chge_shon8=10
    ...  su_chge_shon9=10
    ...  su_demo_shon1=10
    ...  su_demo_shon2=10
    ...  su_demo_shon3=10
    ...  su_demo_shon4=10
    ...  su_demo_shon5=10
    ...  su_demo_shon6=10
    ...  su_demo_shon7=10
    ...  su_demo_shon8=10
    ...  su_demo_shon9=10
    ...  su_sup_shon1=10
    ...  su_sup_shon2=10
    ...  su_sup_shon3=10
    ...  su_sup_shon4=10
    ...  su_sup_shon5=10
    ...  su_sup_shon6=10
    ...  su_sup_shon7=10
    ...  su_sup_shon8=10
    ...  su_sup_shon9=10
    Modifier les données techniques pour le calcul des surfaces  ${di_ko}  ${donnees_techniques_values}
    #
    Set Suite Variable  ${di_ok}
    Set Suite Variable  ${di_ko}

Verification du menu
    [Documentation]  Le but est de verifier si on a acces a toute les pages.

    Depuis la page d'accueil  instr  instr
    Go To Submenu In Menu  instruction  dossier_instruction_mes_encours
    Page Title Should Be  Instruction > Dossiers D'instruction
    Go To Submenu In Menu  instruction  dossier_instruction_tous_encours
    Page Title Should Be  Instruction > Dossiers D'instruction
    Go To Submenu In Menu  instruction  dossier_instruction_mes_clotures
    Page Title Should Be  Instruction > Dossiers D'instruction
    Go To Submenu In Menu  instruction  dossier_instruction_tous_clotures
    Page Title Should Be  Instruction > Dossiers D'instruction
    Go To Submenu In Menu  instruction  dossier_instruction_recherche
    Page Title Should Be  Instruction > Dossiers D'instruction

    Go To Submenu In Menu  instruction  dossier_qualifier
    Page Title Should Be  Instruction > Qualification > Dossiers À Qualifier
    Go To Submenu In Menu  instruction  architecte_frequent
    Page Title Should Be  Instruction > Qualification > Architecte Fréquent

    Go To Submenu In Menu  instruction  consultation_mes_retours
    Page Title Should Be  Instruction > Consultations > Mes Retours
    Go To Submenu In Menu  instruction  consultation_tous_retours
    Page Title Should Be  Instruction > Consultations > Tous Les Retours

    Go To Submenu In Menu  instruction  messages_mes_retours
    Page Title Should Be  Instruction > Messages > Mes Messages
    Go To Submenu In Menu  instruction  messages_tous_retours
    Page Title Should Be  Instruction > Messages > Tous Les Messages


    Go To Submenu In Menu  instruction  commission_mes_retours
    Page Title Should Be  Instruction > Commissions > Mes Retours
    Go To Submenu In Menu  instruction  commission_tous_retours
    Page Title Should Be  Instruction > Commissions > Tous Les Retours


Visualisation de DI et DA
    [Documentation]  On vérifie que le les DI et DA sont consultable par
    ...  l'instructeur en charge

    Depuis la page d'accueil  instr  instr
    Go To Submenu In Menu  instruction  dossier_instruction_mes_encours
    Page Title Should Be  Instruction > Dossiers D'instruction
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  AZ 013055 12 00001P0
    Element Should Contain  dossier_libelle  AZ 013055 12 00001P0
    On clique sur l'onglet  instruction  Instruction
    On clique sur l'onglet  consultation  Consultation(s)
    On clique sur l'onglet  lot  Lot(s)
    On clique sur l'onglet  dossier_message  Message(s)
    On clique sur l'onglet  dossier_commission  Commission(s)
    On clique sur l'onglet  blocnote  Bloc-note
    On clique sur l'onglet  lien_dossier_dossier  Dossiers Liés
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  AZ 013055 12 00001
    Element Should Contain  css=.form-content>#dossier_autorisation_libelle  AZ 013055 12 00001
    On clique sur l'onglet  dossier_instruction  Dossiers D'instruction
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  AZ 013055 12 00001P0
    Element Should Contain  dossier_libelle  AZ 013055 12 00001P0


Verification de restriction d'instruction
    [Documentation]  Ajout d'instructions par l'instructeur,
    ...  modification des restriction un événement

    # ici on test la modification des restriction des evenements d'instruction

    ${evenement} =  Set Variable  CDEC majoration appel decision

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  parametrage-dossier  evenement
    Use Simple Search  Tous  ${evenement}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  ${evenement}
    Click On Form Portlet Action  evenement  modifier
    # On emule une erreur de champ non existant
    Input Text  css=#restriction  date_evenement >= champ_errone + 1
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=div.ui-state-error p span.text  SAISIE NON ENREGISTRÉE
    # On remplace par une valeur qui marche
    Input Text  css=#restriction  date_evenement >= date_evenement + 1
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.


    # On vérifie que les restrictions fonctionne en essayant de créer une instruction

    Depuis la page d'accueil  instr  instr
    Depuis l'onglet instruction du dossier d'instruction  ${di_ok}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction-corner-ajouter
    Saisir instruction  ${evenement}
    Click On Submit Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=div.ui-state-error p span.text  SAISIE NON ENREGISTRÉE

    # On remet d'aplon l'événement

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  parametrage-dossier  evenement
    Use Simple Search  Tous  ${evenement}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  ${evenement}
    Click On Form Portlet Action  evenement  modifier
    Input Text  css=#restriction  date_evenement <= date_evenement + 1
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI  ${di_ok}  ${evenement}
    Click On Back Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  ${evenement}

    # On vérifie l'absence d'Element
    Wait Until Element Is Not Visible  css=#date_envoi_controle_legalite
    Wait Until Element Is Not Visible  css=#date_retour_controle_legalite


Suivi des dates

    [Documentation]  Cette action, directement disponible depuis la fiche d'un
    ...  événement d'instruction, permet d'éviter de passer par l'entrée menu.
    ...  L'objet de ce test case est de vérifier son comportement selon le contexte.

    # Jeu de données
    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Clavet
    ...  particulier_prenom=Sandrine
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    #
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    #
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI  ${di}  Notification de pieces manquante
    Depuis l'instruction du dossier d'instruction  ${di}  Notification de pieces manquante
    Portlet Action Should Be In SubForm  instruction  supprimer
    Click On SubForm Portlet Action  instruction  finaliser
    # L'instruction doit être finalisée et l'instructeur ne peut pas suivre les dates
    Portlet Action Should Be In SubForm  instruction  edition
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Portlet Action Should Be In SubForm  instruction  definaliser
    Portlet Action Should Not Be In SubForm  instruction  modifier_suivi
    Portlet Action Should Not Be In SubForm  instruction  modifier
    # On clique sur l'action "Suivi des dates"
    Depuis la page d'accueil  admingen  admingen
    Depuis l'instruction du dossier d'instruction  ${di}  Notification de pieces manquante
    Click On SubForm Portlet Action  instruction  modifier_suivi
    # On saisit les dates
    Input Datepicker  date_finalisation_courrier  ${date_ddmmyyyy}
    Input Datepicker  date_envoi_signature  ${date_ddmmyyyy}
    Input Datepicker  date_envoi_rar  ${date_ddmmyyyy}
    Input Datepicker  date_envoi_controle_legalite  ${date_ddmmyyyy}
    Input Datepicker  date_retour_signature  ${date_ddmmyyyy}
    Input Datepicker  date_retour_rar  ${date_ddmmyyyy}
    Input Datepicker  date_retour_controle_legalite  ${date_ddmmyyyy}
    Click On Submit Button In Subform
    # On contrôle les dates saisies
    Click On Back Button In Subform
    Element Text Should Be  date_finalisation_courrier  ${date_ddmmyyyy}
    Element Text Should Be  date_envoi_signature  ${date_ddmmyyyy}
    Element Text Should Be  date_envoi_rar  ${date_ddmmyyyy}
    Element Text Should Be  date_envoi_controle_legalite  ${date_ddmmyyyy}
    Element Text Should Be  date_retour_signature  ${date_ddmmyyyy}
    Element Text Should Be  date_retour_rar  ${date_ddmmyyyy}
    Element Text Should Be  date_retour_controle_legalite  ${date_ddmmyyyy}
    # On doit pouvoir modifier mais pas suivre les dates si l'on est instructeur
    # Cas 1/3 : INSTRUCTEUR
    Depuis la page d'accueil  instr  instr
    Depuis l'instruction du dossier d'instruction  ${di}  Notification de pieces manquante
    Portlet Action Should Not Be In SubForm  instruction  modifier_suivi
    Click On SubForm Portlet Action  instruction  definaliser
    Click On SubForm Portlet Action  instruction  modifier

    # Si le click du portlet ne fonctionne pas on essaie encore
    ${status} =  Run Keyword And Return Status  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Must Be Disabled  date_finalisation_courrier
    Run Keyword If  ${status} == False  Click On SubForm Portlet Action  instruction  modifier

    Element Must Be Disabled  date_finalisation_courrier
    Element Must Be Disabled  date_envoi_signature
    Element Must Be Disabled  date_envoi_rar
    Element Must Be Disabled  date_retour_signature
    Element Must Be Disabled  date_retour_rar
    Click On Back Button In Subform
    Click On SubForm Portlet Action  instruction  finaliser
    Portlet Action Should Not Be In SubForm  instruction  modifier_suivi
    # Cas 2/3 : GUICHET SUIVI
    Depuis la page d'accueil  guichetsuivi  guichetsuivi
    Depuis l'instruction du dossier d'instruction  ${di}  Notification de pieces manquante
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Element Must Be Enabled  date_finalisation_courrier
    Element Must Be Enabled  date_envoi_signature
    Element Must Be Enabled  date_envoi_rar
    Element Must Be Enabled  date_retour_signature
    Element Must Be Enabled  date_retour_rar
    # Cas 3/3 : ADMIN
    Depuis la page d'accueil  admingen  admingen
    Depuis l'instruction du dossier d'instruction  ${di}  Notification de pieces manquante
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Element Must Be Enabled  date_finalisation_courrier
    Element Must Be Enabled  date_envoi_signature
    Element Must Be Enabled  date_envoi_rar
    Element Must Be Enabled  date_retour_signature
    Element Must Be Enabled  date_retour_rar
    # L'instructeur polyvalent commune doit pouvoir suivre les dates d'un DI
    # dont l'instruction a été déléguée à la communauté.
    # Cas 1/2 : réaffectation
    Depuis la page d'accueil  admin  admin
    # Ajoute un instructeur polyvalent affecté à la collevtivité de niveau 2
    Ajouter l'utilisateur  LaGarde Armand  support@atreal.fr  instrpolyagglo  instrpolyagglo  INSTRUCTEUR POLYVALENT  agglo
    Ajouter la direction depuis le menu  ADS-AGGLO  Direction ADS-AGGLO  null  Chef ADS  null  null  agglo
    Ajouter la division depuis le menu  X  subdivision X-AGGLO  null  Chef X  null  null  Direction ADS-AGGLO
    Ajouter l'instructeur depuis le menu  LaGarde Armand  subdivision X-AGGLO  instructeur  LaGarde Armand
    # Permet le même comportement du test éxécuter seul ou avec tous les autres
    # tests
    Modifier le paramètre   option_afficher_division  true  agglo
    Depuis le contexte du dossier d'instruction  ${di}
    Click On Form Portlet Action  dossier_instruction  modifier
    Select From List By Label  instructeur  LaGarde Armand (X)
    Click On Submit Button
    #
    Depuis la page d'accueil  instrpolycomm2  instrpolycomm2
    Depuis l'instruction du dossier d'instruction  ${di}  Notification de pieces manquante
    Portlet Action Should Be In SubForm  instruction  modifier_suivi
    # Cas 2/2 : affectation automatique
    Depuis la page d'accueil  admin  admin
    &{args_affectation} =  Create Dictionary
    ...  instructeur=LaGarde Armand (X)
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=DECLARATION PREALABLE SIMPLE
    Ajouter l'affectation depuis le menu  ${args_affectation}

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Torri
    ...  particulier_prenom=Renato
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=DECLARATION PREALABLE SIMPLE
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    # Instructeur polyvalent commune de la même collectivité que celle du dossier
    Depuis la page d'accueil  instrpolycomm2  instrpolycomm2
    Depuis l'instruction du dossier d'instruction  ${di}  Notification du delai legal maison individuelle
    Portlet Action Should Be In SubForm  instruction  modifier_suivi
    # On ne peut pas modifier les dates si le dossier est clos
    Depuis la page d'accueil  instrpolyagglo  instrpolyagglo
    Ajouter une instruction au DI  ${di}  accepter un dossier sans réserve
    Click On Back Button In Subform
    Click On Link  accepter un dossier sans réserve
    Click On SubForm Portlet Action  instruction  finaliser
    Depuis l'instruction du dossier d'instruction  ${di}  Notification du delai legal maison individuelle
    Portlet Action Should Not Be In SubForm  instruction  modifier_suivi
    Depuis la page d'accueil  admin  admin
    Supprimer l'affectation depuis le menu  LaGarde Armand (X)  DECLARATION PREALABLE SIMPLE


Lien vers le di dans le message de validation de la demande

    [Documentation]  Vérifie si le lien dans le message de validation est
    ...  fonctionnel.

    #
    Depuis la page d'accueil  guichet  guichet
    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=DUPONT
    ...  particulier_prenom=Geralt


    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial

    ${libelle_di} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}
    # On clique sur le lien vers le DI du message de validation
    Click Link  css=#link_demande_dossier_instruction
    # On vérifie le fil d'Ariane
    Page Title Should Be    Instruction > Dossiers D'instruction > ${libelle_di} DUPONT GERALT

Finalisation
    [Documentation]  L'objet de ce 'Test Case' est de vérifier le log de
    ...    l'utilisateur qui a finalisé l'événement.

    # Constitution du jeu de données : deux utilisateurs dont un est instructeur
    # car si tel est le cas son nom d'instructeur surcharge son nom d'utilisateur.
    # En tant qu'administrateur
    Depuis la page d'accueil  admin  admin
    # Donnée 2/3 : instructeur
    Ajouter l'utilisateur  Marois Alain -UTIL-  support@atreal.fr  instrmars  instrmars  INSTRUCTEUR  MARSEILLE
    Ajouter la direction depuis le menu  MRS  Direction MRS  null  Chef MRS  null  null  MARSEILLE
    Ajouter la division depuis le menu  MRS  subdivision MRS  null  Chef MRS  null  null  Direction MRS
    Ajouter l'instructeur depuis le menu  Marois Alain -INSTR-  subdivision MRS  instructeur  Marois Alain -UTIL-
    # Donnée 3/3 : affectation automatique du nouvel instructeur
    &{args_affectation} =  Create Dictionary
    ...  instructeur=Marois Alain -INSTR- (MRS)
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    Ajouter l'affectation depuis le menu  ${args_affectation}

    # On crée une nouvelle demande via le tableau de bord
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Perrault
    ...  particulier_prenom=Sophie
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    ...  demande_type=Dépôt Initial
    # On crée une nouvelle demande via le tableau de bord
    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    # En tant qu'instructeur de Martigues
    Depuis la page d'accueil  instrmars  instrmars
    # On ouvre l'onglet instruction du nouveau DI
    Depuis l'onglet instruction du dossier d'instruction  ${di_libelle}
    # On accède à l'instruction
    Click On Link  Notification du delai legal maison individuelle
    # On vérifie qu'elle a été finalisée par le guichetier automatiquement
    # lors de la création
    Wait Until Element Is Visible  om_final_instruction_utilisateur
    Element Text Should Be  om_final_instruction_utilisateur  admin (Administrateur)
    # On reprend la rédaction
    Click On SubForm Portlet Action  instruction  definaliser
    # On vérifie qu'il n'y a pas le champ "finalisé par"
    Element Should Not Be Visible  om_final_instruction_utilisateur
    # On finalise
    Click On SubForm Portlet Action  instruction  finaliser
    # On vérifie le log
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Text Should Be  om_final_instruction_utilisateur  instrmars (Marois Alain -INSTR-)

    Depuis la page d'accueil  admin  admin
    Supprimer l'affectation depuis le menu  Marois Alain -INSTR- (MRS)  Permis de construire comprenant ou non des démolitions

Définalisation d'instruction

    [Documentation]  Permet de vérifier qu'un utilisateur hors division ne peut
    ...  définaliser un événement d'instruction.

    Depuis la page d'accueil  instr2  instr
    Depuis le contexte du dossier d'instruction  ${di_ko}
    # On clique sur le lien Instruction
    Click On Link  css=#instruction
    # On clique sur la 1ere instruction
    Click On Link  Notification du delai legal maison individuelle
    # Vérification que l'instructeur ne peut pas definaliser
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=#sousform-instruction div#portlet-actions  Reprendre la rédaction du document


Vérification du récapitulatif du dossier d'instruction

    [Documentation]  Vérifie l'affichage des champs de fusion sur un dossier
    ...  d'instruction.

    Depuis la page d'accueil  instr  instr
    Depuis le contexte du dossier d'instruction  ${di_ko}
    # On clique sur l'action édition
    Click On Form Portlet Action  dossier_instruction  edition
    # On ouvre le PDF
    Open PDF  form
    # On vérifie le pétitionnaire principal
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Monsieur Smith John
    # On vérifie le résultat total du tableau des surface
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Surface totale : 90
    # On ferme le PDF
    Close PDF


Vérification de l'édition de l'instruction

    [Documentation]  Vérifie l'affichage des champs de fusion sur une
    ...  instruction, et que le portail d'actions contextuelles contient les bonnes actions
    ...  de finalisation et de définalisation et modification dans les bons contextes.

    Depuis la page d'accueil  instr  instr
    Depuis l'instruction du dossier d'instruction  ${di_ok}  Notification du delai legal maison individuelle
    # On régénère le récépissé
    Click On SubForm Portlet Action  instruction  definaliser
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain In Subform  La définalisation du document s'est effectuée avec succès.
    Portlet Action Should Not Be In SubForm  instruction  definaliser
    Portlet Action Should Be In SubForm  instruction  modifier
    # On clique sur l'action édition
    Click On SubForm Portlet Action  instruction  edition
    # on verifie le premier nom de PDF
    Open PDF  sousform
    # On ferme le PDF
    Close PDF

    Click On SubForm Portlet Action  instruction  finaliser
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain In Subform  La finalisation du document s'est effectuée avec succès.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Portlet Action Should Not Be In SubForm  instruction  finaliser
    Portlet Action Should Not Be In SubForm  instruction  modifier
    # On clique sur l'action édition
    Click On SubForm Portlet Action  instruction  edition
    # On ouvre le PDF
    Open PDF  file
    # On vérifie le pétitionnaire principal
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Société Notaire&Co représenté(e) par Monsieur Martin Nicolas
    # On vérifie le résultat total du tableau des surface
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Surface totale : 90
    # On ferme le PDF
    Close PDF


Vérification de l'édition du rapport d'instruction

    [Documentation]  Vérifie l'affichage des champs de fusion sur un rapport
    ...  d'instruction. On vérifie ensuite qu'après que le rapport d'instruction soit
    ...  finalisé, la consultation de l'édition récupère le PDF directement en utilisant
    ...  spg/file.php sans le regénérer à la volée.

    #
    Depuis la page d'accueil  instr  instr
    #
    Depuis le contexte du rapport d'instruction  ${di_ok}
    # On valide le rapport d'instruction
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On clique sur le bouton retour
    Click On Back Button In Subform
    #
    Depuis le contexte du rapport d'instruction  ${di_ok}
    # On clique sur l'action de finaliser
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  rapport_instruction  finalise
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain In Subform  La finalisation du document s'est effectuée avec succès.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Portlet Action Should Not Be In SubForm  rapport_instruction  finalise
    Portlet Action Should Not Be In SubForm  rapport_instruction  modifier
    Click On SubForm Portlet Action  rapport_instruction  edition
    Open PDF  file
    # On vérifie le pétitionnaire principal
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Société Notaire&Co représenté(e) par Monsieur Martin Nicolas
    # On vérifie le résultat total du tableau des surface
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Surface totale : 90
    Close PDF

    Click On SubForm Portlet Action  rapport_instruction  definalise
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain In Subform  La définalisation du document s'est effectuée avec succès.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Portlet Action Should Not Be In SubForm  rapport_instruction  definalise
    Portlet Action Should Be In SubForm  rapport_instruction  modifier
    Portlet Action Should Be In SubForm  rapport_instruction  finalise

    Click On SubForm Portlet Action  rapport_instruction  edition
    Open PDF  sousform
    Close PDF

    # On modifie le rapport d'instruction pour contrôler le comportement de
    # l'overlay
    &{args_rapport_instruction} =  Create Dictionary
    ...  description_projet_om_html=À vérifier pour les tests
    Modifier le rapport d'instruction  ${di_ok}  ${args_rapport_instruction}
    # On clic sur le bouton retour et on vérifie que l'overlay est fermé
    Click On Back Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Be Visible  css=div#sousform-rapport_instruction
    Depuis le contexte du rapport d'instruction  ${di_ok}
    # On clique sur l'action de finaliser
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  rapport_instruction  finalise
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain In Subform  La finalisation du document s'est effectuée avec succès.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Portlet Action Should Not Be In SubForm  rapport_instruction  finalise
    Portlet Action Should Not Be In SubForm  rapport_instruction  modifier
    Click On SubForm Portlet Action  rapport_instruction  edition
    Open PDF  file
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  À vérifier pour les tests
    Close PDF

Changement de décision par commune
    [Documentation]  L'objet de ce 'Test Case' est de vérifier le changement de
    ...  décision par un instructeur polyvalent commune

    Depuis la page d'accueil  admin  admin
    # Création de l'action de workflow "changement de décision"
    &{args_action} =  Create Dictionary
    ...  action=changer_decision
    ...  libelle=Changer la décision
    ...  regle_etat=etat

    Ajouter l'action depuis le menu  ${args_action}

    # Création d'un événement de workflow de changement de décision
    @{etat_source} =  Create List  dossier accepter  dossier accepté tacitement  dossier rejeter manque de pieces  delai de notification envoye
    @{type_di} =  Create List  PA - P - Initial  PCI - P - Initial

    &{args_evenement} =  Create Dictionary
    ...  libelle=Modification décision
    ...  type=changement de décision
    ...  etats_depuis_lequel_l_evenement_est_disponible=${etat_source}
    ...  dossier_instruction_type=${type_di}
    ...  action=Changer la décision
    ...  etat=delai de notification envoye

    Ajouter l'événement depuis le menu  ${args_evenement}

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Halliwell
    ...  particulier_prenom=Geri
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_change_decision} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  admin  admin
    Modifier le paramètre   option_afficher_division  true  agglo

    &{args_di} =  Create Dictionary
    ...  instructeur=Poly (H)
    Modifier le dossier d'instruction  ${di_change_decision}  ${args_di}

    Depuis la page d'accueil  instrpolycomm  instrpolycomm
    ${widget_content} =  Get Text  view_widget_dossiers_evenement_retour_finalise
    Should Not Contain  ${widget_content}  ${di_change_decision}

    Depuis la page d'accueil  instrpoly  instrpoly

    # Ajout au DI une décision que l'utilisateur instructeur polyvalent commune changera
    Ajouter une instruction au DI  ${di_change_decision}  ARRÊTÉ DE REFUS
    Click On Back Button In Subform
    Click On Link  ARRÊTÉ DE REFUS
    Click On SubForm Portlet Action  instruction  finaliser

    # Connexion avec l'instructeur polyvalent commune
    Depuis la page d'accueil  instrpolycomm  instrpolycomm
    ${widget_content} =  Get Text  view_widget_dossiers_evenement_retour_finalise
    Should Contain  ${widget_content}  ${di_change_decision}
    # On clic pour voir tous les dossiers
    Click On Link  Voir les dossiers auxquels on peut proposer une autre décision
    # On clic sur le DI
    Click On Link  ${di_change_decision}
    #
    On clique sur l'onglet  instruction  Instruction
    # Ajout de l'événement d'instruction de modification de décision
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction-corner-ajouter
    #
    Saisir instruction  Modification décision
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.

    Depuis la page d'accueil  instrpolycomm2  instrpolycomm2
    # Changement de la décision
    Ajouter une instruction au DI  ${di_change_decision}  ARRÊTÉ DE REFUS 2

    Depuis la page d'accueil  instrpolycomm  instrpolycomm
    # On finalise avec un autre instructeur polyvalent commune
    Depuis l'onglet instruction du dossier d'instruction  ${di_change_decision}
    Click On Link  ARRÊTÉ DE REFUS 2
    ${id_instruction} =  Get Text  css=div#form-content span#instruction
    Click On SubForm Portlet Action  instruction  finaliser

    # En tant qu'instructeur
    Depuis la page d'accueil  instrpoly  instrpoly

    # On en déduit le code-barres
    ${code_barres} =  STR_PAD_LEFT  ${id_instruction}  10  0
    ${code_barres} =  Catenate  11${code_barres}
    Go To Submenu In Menu    suivi    suivi_mise_a_jour_des_dates
    Select From List By Label  css=#type_mise_a_jour  date de retour de l'AR
    Input Text  date  ${date_ddmmyyyy}
    Input Text  code_barres  ${code_barres}
    # On valide le formulaire
    Click Element  css=#formulaire div.formControls input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#formulaire div.formControls input[type="submit"]
    Click Element  css=#formulaire div.formControls input[type="submit"]

    Depuis la page d'accueil  instrpolycomm  instrpolycomm
    ${widget_content} =  Get Text  view_widget_dossiers_evenement_retour_finalise
    Should Not Contain  ${widget_content}  ${di_change_decision}


TNR Bug instructeur commune modifier finaliser définaliser instruction

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Dupont
    ...  particulier_prenom=Marc
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=DECLARATION PREALABLE SIMPLE
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instrpolycomm  instrpolycomm

    Depuis l'onglet instruction du dossier d'instruction  ${di}
    Click On Link  Notification du delai legal maison individuelle
    Click On SubForm Portlet Action  instruction  definaliser
    Click On SubForm Portlet Action  instruction  modifier
    Click On Submit Button In Subform
    Click On Back Button In Subform
    Click On SubForm Portlet Action  instruction  finaliser

TNR Bug instructeur commune ajout d'evenements autre que décision

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Dupont
    ...  particulier_prenom=Francis
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Certificat d'urbanisme
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instrpolycomm2  instrpolycomm2
    Ajouter une instruction au DI  ${di}  Commission Communale de Sécurité


TNR Bug instructeur commune ajout d'evenements sur dossier cloturé

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Dupont
    ...  particulier_prenom=Albert
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instrpoly  instrpoly
    Ajouter une instruction au DI  ${di}  accepter un dossier avec reserve
    Click On Back Button In Subform
    Click Link  accepter un dossier avec reserve
    # On finalise
    Click On SubForm Portlet Action  instruction  finaliser

    Depuis la page d'accueil  instrpolycomm  instrpolycomm
    ${widget_content} =  Get Text  view_widget_dossiers_evenement_retour_finalise
    Should Not Contain  ${widget_content}  ${di}

    Depuis l'onglet instruction du dossier d'instruction  ${di}
    Should Not Contain  css=#sousform-instruction  action-soustab-instruction-corner-ajouter


TNR Bug type de dossiers auxquels un instructeur commune peut changer la décision
    [Documentation]    Les types de demande DOC DAACT et PRO ne doivent pas
    ...    apparaître dans le widget des dossiers auxquels on peut changer la décision


    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Beckham
    ...  particulier_prenom=Victoria
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_change_decision} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instrpoly  instrpoly
    # Ajout au DI une décision que l'utilisateur instructeur polyvalent commune changera
    Ajouter une instruction au DI  ${di_change_decision}  accepter un dossier sans réserve
    Click On Back Button In Subform
    Click On Link  accepter un dossier sans réserve
    Click On SubForm Portlet Action  instruction  finaliser

    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande d'ouverture de chantier
    ...  om_collectivite=MARSEILLE
    ${di_change_decision_2} =  Ajouter la demande sur existant depuis le menu    ${di_change_decision}    ${args_demande}

    Depuis la page d'accueil  admin  admin
    Modifier le paramètre   option_afficher_division  true  agglo
    &{args_di} =  Create Dictionary
    ...  instructeur=Poly (H)
    Modifier le dossier d'instruction  ${di_change_decision_2}  ${args_di}

    # Ajout au DI une décision que l'utilisateur instructeur polyvalent commune changera
    Ajouter une instruction au DI  ${di_change_decision_2}  ARRÊTÉ DE REFUS
    Click On Back Button In Subform
    Click On Link  ARRÊTÉ DE REFUS
    Click On SubForm Portlet Action  instruction  finaliser

    Depuis la page d'accueil  instrpolycomm  instrpolycomm
    # Vérification widget
    ${widget_content} =  Get Text  view_widget_dossiers_evenement_retour_finalise
    Should Not Contain  ${widget_content}  ${di_change_decision_2}
    # Vérification tableau
    Go To Tab  dossier_instruction&decision=true
    Page Should Not Contain  ${di_change_decision_2}

TNR Nature des travaux dans la description du DI
    [Documentation]  Concernant les données techniques sur la nature des travaux,
    ...  lorsque les cases sont cochées alors la description du projet les affiche.

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Hasselhoff
    ...  particulier_prenom=David
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Demande d'autorisation de construire, d'aménager ou de modifier un ERP
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instrpoly  instrpoly
    # On coche les cases du CERFA sur la nature des travaux
    Depuis le contexte du dossier d'instruction  ${di}
    Click On Form Portlet Action  dossier_instruction  donnees_techniques
    Click On SubForm Portlet Action  donnees_techniques  modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=fieldset[id*='amenager-ou-modifier-un-erp'] > legend
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=fieldset[id*='amenager-ou-modifier-un-erp'] > div.fieldsetContent
    Select Checkbox  erp_cstr_neuve
    Select Checkbox  erp_trvx_acc
    Select Checkbox  erp_extension
    Select Checkbox  erp_rehab
    Select Checkbox  erp_trvx_am
    Select Checkbox  erp_vol_nouv_exist
    Click On Submit Button In Subform
    Click On Back Button In Subform
    # On contrôle la description du projet
    Reload Page
    Element Should Contain  description_projet  Construction neuve
    Element Should Contain  description_projet  Travaux de mise en conformité totale aux règles d’accessibilité
    Element Should Contain  description_projet  Extension
    Element Should Contain  description_projet  Réhabilitation
    Element Should Contain  description_projet  Travaux d’aménagement (remplacement de revêtements, rénovation électrique, création d’une rampe, par exemple)
    Element Should Contain  description_projet  Création de volumes nouveaux dans des volumes existants (modification du cloisonnement, par exemple)


Vérifie la restriction de modifier le DI et de régénérer le récépissé

    [Documentation]  Vérifie que l'instructeur peut toujours modifier le dossier
    ...  d'instruction, même si la restriction imposée pour le guichet unique
    ...  et pour l'instructeur commune n'est pas respectée. Ces deux profils ne
    ...  peuvent modifier le dossier d'instruction qu'a condition que sa seule
    ...  instruction soit son récépissé ou que les instructions qui suivent
    ...  soient du type "affichage".

    # On modifie l'affectation automatique pour ce test
    Depuis la page d'accueil  admin  admin
    &{args_affectation} =  Create Dictionary
    ...  instructeur=Poly (H)
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    Ajouter l'affectation depuis le menu  ${args_affectation}

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=DUPONT
    ...  particulier_prenom=Geralt
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${libelle_di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  guichetsuivi  guichetsuivi
    # On vérifie pour le guichet et suivi que les actions modifier et régénérer
    # le récépissé sont disponibles
    Depuis le contexte du dossier d'instruction  ${libelle_di}
    # Vérifie l'action modifier pour le guichet et suivi
    Portlet Action Should Be In Form  dossier_instruction  modifier
    # Vérifie l'action de régénérer le récépissé pour le guichet et suivi
    Portlet Action Should Be In Form  dossier_instruction  recepisse

    # On vérifie aussi pour l'instructeur commune que les actions modifier et
    # régénérer le récépissé sont disponibles
    Depuis la page d'accueil  instrpolycomm  instrpolycomm
    Depuis le contexte du dossier d'instruction  ${libelle_di}
    # Vérifie l'action modifier pour le guichet et suivi
    Portlet Action Should Be In Form  dossier_instruction  modifier
    # Vérifie l'action de régénérer le récépissé pour le guichet et suivi
    Portlet Action Should Be In Form  dossier_instruction  recepisse

    # On ajoute une instruction de type affichage au dossier
    Depuis la page d'accueil  instrpoly  instrpoly
    Depuis le contexte du dossier d'instruction de mes encours  ${libelle_di}
    # Vérifie les actions modifier et régénérer le récépissé pour l'instructeur
    # polyvalent
    Portlet Action Should Be In Form  dossier_instruction_mes_encours  modifier
    Ajouter une instruction au DI  ${libelle_di}  affichage_obligatoire
    Depuis le contexte du dossier d'instruction de mes encours  ${libelle_di}
    # Vérifie l'action modifier pour l'instructeur polyvalent
    Portlet Action Should Be In Form  dossier_instruction_mes_encours  modifier

    # On vérifie que les actions soient toujours visibles pour le guichet et
    # suivi
    Depuis la page d'accueil  guichetsuivi  guichetsuivi
    Depuis le contexte du dossier d'instruction  ${libelle_di}
    # Vérifie l'action modifier pour le guichet et suivi
    Portlet Action Should Be In Form  dossier_instruction  modifier
    # Vérifie l'action de régénérer le récépissé pour le guichet et suivi
    Portlet Action Should Be In Form  dossier_instruction  recepisse

    # On vérifie aussi pour l'instructeur commune que les actions modifier et
    # régénérer le récépissé sont disponibles
    Depuis la page d'accueil  instrpolycomm  instrpolycomm
    Depuis le contexte du dossier d'instruction  ${libelle_di}
    # Vérifie l'action modifier pour le guichet et suivi
    Portlet Action Should Be In Form  dossier_instruction  modifier
    # Vérifie l'action de régénérer le récépissé pour le guichet et suivi
    Portlet Action Should Be In Form  dossier_instruction  recepisse

    # On ajoute une instruction qui doit bloquer les actions aux autres
    # utilisateurs
    Depuis la page d'accueil  instrpoly  instrpoly
    Depuis le contexte du dossier d'instruction de mes encours  ${libelle_di}
    Ajouter une instruction au DI  ${libelle_di}  majoration_IGH
    Depuis le contexte du dossier d'instruction de mes encours  ${libelle_di}
    # Vérifie l'action modifier pour l'instructeur polyvalent
    Portlet Action Should Be In Form  dossier_instruction_mes_encours  modifier

    # On vérifie que les actions ne soient plus visibles pour le guichet et
    # suivi
    Depuis la page d'accueil  guichetsuivi  guichetsuivi
    Depuis le contexte du dossier d'instruction  ${libelle_di}
    # Vérifie l'action modifier pour le guichet et suivi
    Portlet Action Should Not Be In Form  dossier_instruction  modifier
    # Vérifie l'action de régénérer le récépissé pour le guichet et suivi
    Portlet Action Should Not Be In Form  dossier_instruction  recepisse

    # On vérifie aussi pour l'instructeur commune que les actions modifier et
    # régénérer le récépissé soient indisponibles
    Depuis la page d'accueil  instrpolycomm  instrpolycomm
    Depuis le contexte du dossier d'instruction  ${libelle_di}
    # Vérifie l'action modifier pour le guichet et suivi
    Portlet Action Should Not Be In Form  dossier_instruction  modifier
    # Vérifie l'action de régénérer le récépissé pour le guichet et suivi
    Portlet Action Should Not Be In Form  dossier_instruction  recepisse

    #
    Depuis la page d'accueil  admin  admin
    #
    Supprimer l'affectation depuis le menu  Poly (H)


TNR Modification des paramètres de la variable de remplacement &contrainte

    [Documentation]  Vérifie que les 3 paramètres de &contrainte liste_groupe,
    ...  liste_ssgroupe, affichage_sans_arborescence modifient l'affichage des
    ...  contraintes sans erreurs.

    # Le contenu de la nouvelle lettre-type de test, avec &contraintes sans paramètres
    &{args_lettretype} =  Create Dictionary
    ...  id=test_contraintes
    ...  libelle=Test des nouveaux paramètres &CONTRAINTES
    ...  sql=Aucune REQUÊTE
    ...  titre=&contraintes
    ...  corps=&contraintes
    ...  actif=true
    ...  collectivite=MARSEILLE

    &{args_evenement} =  Create Dictionary
    ...  libelle=Notification du delai legal maison individuelle
    ...  lettretype=test_contraintes Test des nouveaux paramètres &CONTRAINTES

    Depuis la page d'accueil  admin  admin
    Ajouter la lettre-type depuis le menu  &{args_lettretype}
    # On change la lettre-type de l'événement de création d'une nouvelle demande, en
    # définissant notre nouvelle lettre-type comme modèle
    Modifier l'événement  ${args_evenement}
    ${id_contrainte1} =  Ajouter la contrainte depuis le menu  Contrainte TNR instruction 1  PLU  MARSEILLE  TNR instr  sousgroupe  1ère contrainte instr
    ${id_contrainte2} =  Ajouter la contrainte depuis le menu  Contrainte TNR instruction 2  PLU  MARSEILLE  TNR instr  sousgroupe  2ème contrainte instr
    ${id_contrainte3} =  Ajouter la contrainte depuis le menu  Contrainte TNR instruction 3  PLU  MARSEILLE  TNR instr2  null  3ème contrainte instr2

    # Création d'une nouvelle demande pour notre test
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Saville
    ...  particulier_prenom=Lazure
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${libelle_di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    # Ajout de contraintes à notre dossier de test
    Depuis la page d'accueil  instr  instr
    Ajouter une contrainte depuis l'onglet du dossier d'instruction  ${libelle_di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  fieldset-sousform-dossier_contrainte-contraintes-openads
    Open Fieldset In Subform  dossier_contrainte  tnr-instr
    Open Fieldset In Subform  dossier_contrainte  sousgroupe
    Open Fieldset In Subform  dossier_contrainte  tnr-instr2
    Select Checkbox  css=#contrainte_${id_contrainte1}
    Select Checkbox  css=#contrainte_${id_contrainte2}
    Select Checkbox  css=#contrainte_${id_contrainte3}
    # On clique sur Appliquer les changements
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#sformulaire div.formControls input[type="submit"]
    # Vérification des messages
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#sousform-dossier_contrainte div.message.ui-state-valid p span.text  Contrainte TNR instruction 1 a été ajoutée au dossier.
    Element Should Contain  css=#sousform-dossier_contrainte div.message.ui-state-valid p span.text  La contrainte Contrainte TNR instruction 2 a été ajoutée au dossier.
    Element Should Contain  css=#sousform-dossier_contrainte div.message.ui-state-valid p span.text  La contrainte Contrainte TNR instruction 3 a été ajoutée au dossier.

    # On régénère l'édition
    Depuis l'instruction du dossier d'instruction  ${libelle_di}  Notification du delai legal maison individuelle
    Click On SubForm Portlet Action  instruction  definaliser
    Click On SubForm Portlet Action  instruction  finaliser
    Click On SubForm Portlet Action  instruction  edition
    # On ouvre le PDF
    Open PDF  file
    # On vérifie que la lettre-type contient toutes les contraintes
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  TNR INSTR
    Page Should Contain  SOUSGROUPE
    Page Should Contain  1ère contrainte instr
    Page Should Contain  2ème contrainte instr
    Page Should Contain  TNR INSTR2
    Page Should Contain  3ème contrainte instr2
    Close PDF

    # On ajoute le paramètre liste_groupe à la variable &contraintes dans la lettre-type
    &{args_lettretype} =  Create Dictionary
    ...  id=test_contraintes
    ...  libelle=Test des nouveaux paramètres &CONTRAINTES
    ...  sql=Aucune REQUÊTE
    ...  titre=&contraintes(liste_groupe=TNR INSTR)
    ...  corps=&contraintes(liste_groupe=TNR INSTR)
    ...  actif=true
    ...  collectivite=MARSEILLE

    Depuis la page d'accueil  admin  admin
    # XXX Appel à la surcharge de Modifier la lettre-type du core, à virer !!
    # Redéfinit la lettre-type avec les arguments passés
    Modifier la lettre-type XXX  &{args_lettretype}

    # On régénère l'édition
    Depuis l'instruction du dossier d'instruction  ${libelle_di}  Notification du delai legal maison individuelle
    Click On SubForm Portlet Action  instruction  definaliser
    Click On SubForm Portlet Action  instruction  finaliser
    Click On SubForm Portlet Action  instruction  edition
    # On ouvre le PDF
    Open PDF  file
    # On doit avoir seulement les contraintes du groupe Zones du PLU
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  TNR INSTR
    Page Should Contain  1ère contrainte instr
    Page Should Contain  2ème contrainte instr
    Page Should Not Contain  TNR INSTR2
    Page Should Not Contain  3ème contrainte instr2
    Close PDF

    # On active l'affichage sans arborescence, avec les paramètres liste_groupe et
    # listess_groupe activés
    &{args_lettretype} =  Create Dictionary
    ...  id=test_contraintes
    ...  libelle=Test des nouveaux paramètres &CONTRAINTES
    ...  sql=Aucune REQUÊTE
    ...  titre=&contraintes(liste_groupe=TNR INSTR;liste_ssgroupe=sousgroupe;affichage_sans_arborescence=t)
    ...  corps=&contraintes(liste_groupe=TNR INSTR;liste_ssgroupe=sousgroupe;affichage_sans_arborescence=t)
    ...  actif=true
    ...  collectivite=MARSEILLE

    # XXX Appel à la surcharge de Modifier la lettre-type du core, à virer !!
    # Redéfinit la lettre-type avec les arguments passés
    Modifier la lettre-type XXX  &{args_lettretype}

    # On régénère l'édition
    Depuis l'instruction du dossier d'instruction  ${libelle_di}  Notification du delai legal maison individuelle
    Click On SubForm Portlet Action  instruction  definaliser
    Click On SubForm Portlet Action  instruction  finaliser
    Click On SubForm Portlet Action  instruction  edition
    # On ouvre le PDF
    Open PDF  file
    # Le PDF doit contenir les 2 contraintes "Zones du PLU", sans groupes
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1ère contrainte instr
    Page Should Contain  2ème contrainte instr
    Page Should Not Contain  TNR INSTR
    Page Should Not Contain  TNR INSTR2
    Page Should Not Contain  3ème contrainte instr2
    Close PDF

    &{args_evenement} =  Create Dictionary
    ...  libelle=Notification du delai legal maison individuelle
    ...  lettretype=recepisse_1 RECEPISSE DE DEPOT

    # On remet la lettre-type de récépissé de dépôt initiale pour les tests suivants
    Modifier l'événement  ${args_evenement}


Les dossiers liés

    [Documentation]  Vérifie l'onglet "Dossiers liés" des dossiers
    ...  d'instruction. Celui-ci doit être composé de 4 tableaux, un pour le
    ...  dossier d'autorisation lié, le deuxième pour les dossiers d'instruction
    ...  liés manuellement ou implicitement, le 3ème listant les dossiers ayant
    ...  un lien pointant sur le dossier courant et le dernier pour les dossiers
    ...  d'autorisation liés géographiquement.

    &{args_petitionnaire_autre_commune} =  Create Dictionary
    ...  particulier_nom=Beauchamps
    ...  particulier_prenom=Maurissette
    ...  om_collectivite=ALLAUCH

    @{ref_cad_autre_commune} =  Create List  806  AB  25

    &{args_demande_autre_commune} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad_autre_commune}
    ...  om_collectivite=ALLAUCH
    ${libelle_di_autre_commune} =  Ajouter la demande par WS  ${args_demande_autre_commune}  ${args_petitionnaire_autre_commune}

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Beauchamps
    ...  particulier_prenom=Jeanette
    ...  om_collectivite=MARSEILLE

    @{ref_cad} =  Create List  806  AB  25  A  30

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ...  om_collectivite=MARSEILLE
    ${libelle_di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    ${libelle_di_spaceless} =  Sans espace  ${libelle_di}

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Forest
    ...  particulier_prenom=David
    ...  om_collectivite=MARSEILLE

    @{ref_cad} =  Create List  806  AB  01  A  50

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ...  om_collectivite=MARSEILLE

    ${libelle_di2} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    ${libelle_di2_spaceless} =  Sans espace  ${libelle_di2}
    ${libelle_da} =  Get Substring  ${libelle_di}  0  -2
    ${libelle_da_spaceless} =  Sans espace  ${libelle_da}
    ${libelle_da2} =  Get Substring  ${libelle_di2}  0  -2
    ${libelle_da_autre_commune} =  Get Substring  ${libelle_di_autre_commune}  0  -2
    ${libelle_di_autre_commune_spaceless} =  Sans espace  ${libelle_di_autre_commune}

    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI et la finaliser  ${libelle_di}  accepter un dossier sans réserve

    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande de modification

    Depuis la page d'accueil  guichet  guichet
    ${libelle_di_modification} =  Ajouter la demande sur existant depuis le tableau de bord  ${libelle_di}  ${args_demande}
    ${libelle_di_modification_spaceless} =  Sans espace  ${libelle_di_modification}


    Depuis la page d'accueil  admin  admin
    Depuis le contexte de nouvelle demande via l'URL
    Select From List By Label    dossier_autorisation_type_detaille    Recours contentieux
    Select From List By Label    om_collectivite    MARSEILLE
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input Text    autorisation_contestee    ${libelle_di}
    Click Button    css=#autorisation_contestee_search_button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain    css=#petitionnaire_principal_delegataire    Beauchamps Jeanette
    Sleep  1
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Page Should Not Contain Errors
    ${libelle_di_re} =  Get Text  id=new_di
    ${libelle_di_re_spaceless} =  Sans espace  ${libelle_di_re}

    Depuis le contexte de nouvelle demande via l'URL
    Select From List By Label    dossier_autorisation_type_detaille    Recours contentieux
    Select From List By Label    om_collectivite    MARSEILLE
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input Text    autorisation_contestee    ${libelle_di2}
    Click Button    css=#autorisation_contestee_search_button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain    css=#petitionnaire_principal_delegataire    Forest David
    Sleep  1
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Page Should Not Contain Errors
    ${libelle_di_re2} =  Get Text  id=new_di
    ${libelle_di_re_2spaceless} =  Sans espace  ${libelle_di_re2}

    # Vérification de la notification sur l'autorisation contestée
    Depuis l'onglet des messages du dossier d'instruction  ${libelle_di}
    Click On Link  Autorisation contestée
    Element Text Should Be  contenu  Cette autorisation a été contestée par le recours ${libelle_di_re_spaceless}.

    ##
    ## Le dossier d'autorisation lié
    ##

    Depuis la page d'accueil  instrpoly  instrpoly
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di}

    Element Should Contain X Times  sousform-dossier_autorisation  ${libelle_da}  1

    ##
    ## Les dossiers d'instruction liés
    ##

    # En premier lieu on vérifie que le dossier courant n'apparaît pas dans la liste
    Element Should Not Contain  sousform-dossier_lies  ${libelle_di}

    #
    # Ajout de liens : vérification des cas de succès
    #
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di_modification}
    # Si utilisateur multi on peut lier le DI d'une autre collectivité
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di_autre_commune}
    Click On Submit Button In SubForm
    Valid Message Should Contain In Subform  Le dossier ${libelle_di_autre_commune_spaceless} a été lié.
    # Vérification de la redirection vers le DI cible
    Click On Link  link_dossier_instruction_lie
    Page Title Should Be    Instruction > Dossiers D'instruction > ${libelle_di_autre_commune} BEAUCHAMPS MAURISSETTE
    # Vérification de la présence du nouveau lien si utilisateur multi
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di_modification}
    Element Should Contain  sousform-dossier_lies  ${libelle_di_autre_commune}
    # Vérification de l'absence du nouveau lien si utilisateur mono
    Depuis la page d'accueil  instr  instr
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di_modification}
    Element Should Not Contain  sousform-dossier_lies  ${libelle_di_autre_commune}

    # Si utilisateur mono on peut lier le DI de la même collectivité
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di2}
    Click On Submit Button In SubForm
    Valid Message Should Contain In Subform  Le dossier ${libelle_di2_spaceless} a été lié.
    # Vérification de la redirection vers le DI cible
    Click On Link  link_dossier_instruction_lie
    Page Title Should Be    Instruction > Dossiers D'instruction > ${libelle_di2} FOREST DAVID
    # Vérification de l'absence de lien (pour rappel il est directionnel)
    On clique sur l'onglet  lien_dossier_dossier  Dossiers Liés
    Element Should Contain  sousform-dossier_lies  Aucun enregistrement.
    # Pour la même raison on peut ajouter le DI source sur le DI cible
    # ainsi les DI seront liés dans chacun des deux sens
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di_modification_spaceless}
    Click On Submit Button In SubForm
    Valid Message Should Contain In Subform  Le dossier ${libelle_di_modification_spaceless} a été lié.
    Click On Back Button In SubForm
    Element Should Contain  sousform-dossier_lies  ${libelle_di_modification}

    # Ajout d'une liaison manuelle vers le dossier recours qui conteste le dossier courant
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di2}
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di_re2}
    Click On Submit Button In SubForm
    Valid Message Should Contain In Subform  Le dossier ${libelle_di_re2_spaceless} a été lié.
    Click On Back Button In SubForm
    Element Should Contain  sousform-dossier_lies  ${libelle_di_re2}

    # On vérifie que les dossiers auxquels on n'a pas accès sont visibles mais
    # que leur consultation produit une erreur de droits insuffisants
    Depuis la page d'accueil  visudadi  visudadi
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di2}
    Element Should Contain  sousform-dossier_lies  ${libelle_di_modification}
    Element Should Contain  sousform-dossier_lies  ${libelle_di_re2}
    Click Link  ${libelle_di_re2}
    Error Message Should Contain  Droits insuffisants

    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di2}
    Element Should Contain  sousform-dossier_lies_retour  ${libelle_di_modification}
    Element Should Contain  sousform-dossier_lies_retour  ${libelle_di_re2}
    Click Link  ${libelle_di_re2}
    Error Message Should Contain  Droits insuffisants

    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di2}
    Element Should Contain  sousform-dossier_lies_geographiquement  ${libelle_da}
    Element Should Contain  sousform-dossier_lies_geographiquement  ${libelle_di_re2}
    Click Link  ${libelle_di_re2}
    Error Message Should Contain  Droits insuffisants

    #
    # Ajout de liens : vérification des cas d'échec
    #

    # On ne peut pas ajouter de liaison si lien automatique existant
    Depuis la page d'accueil  juriste  juriste
    Depuis l'onglet Dossiers Liés du dossier recours  ${libelle_di_re}
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di}
    Click On Submit Button In SubForm
    Error Message Should Be In Subform  Le dossier ${libelle_di_spaceless} est déjà lié au dossier courant (lien automatique).

    # Le dossier cible est un champ obligatoire
    Depuis la page d'accueil  instr  instr
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di_modification}
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Click On Submit Button In SubForm
    Error Message Should Be In Subform  Le champ dossier cible est obligatoire
    # Le dossier cible peut ne pas exister tout court...
    ${ac_fail} =  Set Variable  '*#(('';;'
    ${ac_fail_escaped} =  Set Variable  ''*#(('''';;''
    Input Text  dossier_cible  ${ac_fail}
    Click On Submit Button In SubForm
    Error Message Should Be In Subform  Il n'existe aucun dossier correspondant au numéro ${ac_fail_escaped}. Saisissez un nouveau numéro puis recommencez.
    # ... ou ne pas exister parce qu'il est d'une collectivité différente de l'utilisateur mno
    Click On Back Button In SubForm
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di_autre_commune}
    Click On Submit Button In SubForm
    Error Message Should Be In Subform  Il n'existe aucun dossier correspondant au numéro ${libelle_di_autre_commune_spaceless}. Saisissez un nouveau numéro puis recommencez.
    # On ne peut pas lier un DI à lui-même
    Input Text  dossier_cible  ${libelle_di_modification}
    Click On Submit Button In SubForm
    Error Message Should Be In Subform  Vous ne pouvez pas lier un dossier à lui-même. Saisissez un nouveau numéro puis recommencez.
    # On ne peut pas ajouter de liaison si lien implicite par le DA
    Click On Back Button In SubForm
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di_spaceless}
    Click On Submit Button In SubForm
    Error Message Should Be In Subform  Le dossier ${libelle_di_spaceless} est déjà lié au dossier courant (même dossier d'autorisation).    # On ne peut pas ajouter de liaison si lien manuel existant
    Click On Back Button In SubForm
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di2_spaceless}
    Click On Submit Button In SubForm
    Error Message Should Be In Subform  Le dossier ${libelle_di2_spaceless} est déjà lié au dossier courant.

    #
    # Suppression de liens : vérification des cas de succès
    #

    # On peut supprimer un lien créé manuellement
    Click On Back Button In SubForm
    Page Should Contain  ${libelle_di2}
    Click Element  action-soustab-dossier_lies-left-supprimer-${libelle_di2_spaceless}
    Valid Message Should Be In Subtab  Le dossier ${libelle_di2_spaceless} a été délié.
    Element Should Not Contain  sousform-dossier_lies  ${libelle_di2}

    #
    # Suppression de liens : vérification des cas d'échec
    #

    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di}

    # On ne peut pas supprimer un lien implicite (même DA)
    Element Should Be Visible  action-soustab-dossier_lies-left-consulter-${libelle_da_spaceless}M01
    Element Should Not Be Visible  action-soustab-dossier_lies-left-supprimer-${libelle_da_spaceless}M01
    # On ne peut pas supprimer les liens automatiques si on n'est pas administrateur
    Depuis la page d'accueil  juriste  juriste
    Depuis l'onglet Dossiers Liés du dossier recours  ${libelle_di_re}
    Element Should Be Visible  action-soustab-dossier_lies-left-consulter-${libelle_di_spaceless}
    Element Should Not Be Visible  action-soustab-dossier_lies-left-supprimer-${libelle_di_spaceless}
    # On peut supprimer les liens automatiques si on est administrateur
    Depuis la page d'accueil  admin  admin
    Depuis l'onglet Dossiers Liés du dossier recours  ${libelle_di_re}
    Element Should Be Visible  action-soustab-dossier_lies-left-supprimer-${libelle_di_spaceless}

    ##
    ## Les dossiers d'autorisation liés géographiquement
    ##

    # Ajoute 2 nouvelles demandes avec une parcelle en commun,
    # puis affiche le tableau des dossiers liés géographiquement pour les 2
    # dossiers.
    # L'autre DA avec la même parcelle doit être présent, mais pas le DA lié
    # au DI courant.
    # On ajoute un troisième dossier avec les mêmes parcelles sur une autre
    # commune qui ne doit pas apparaitre dans la liste

    Depuis la page d'accueil  instr  instr
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di}
    # Le tableau des dossiers liés géographiquement ne doit pas contenir le DA lié au DI courant
    Element Should Not Contain  sousform-dossier_lies_geographiquement  ${libelle_da}
    # Le 2ème dossier avec la même parcelle doit apparaître
    Element Should Contain X Times  sousform-dossier_lies_geographiquement  ${libelle_da2}  1

    Depuis la page d'accueil  instr  instr
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di2}
    # Le tableau doit contenir une seule fois le DA qui a 2 DI avec une parcelle en commun
    Element Should Contain X Times  sousform-dossier_lies_geographiquement  ${libelle_da}  1
    # Le tableau des dossiers liés géographiquement ne doit pas contenir le DA lié au DI courant
    Element Should Not Contain  sousform-dossier_lies_geographiquement  ${libelle_da2}
    # Le tableau des dossiers liés géographiquement ne doit pas contenir le DI d'une autre commune
    Element Should Not Contain  sousform-dossier_lies_geographiquement  ${libelle_da_autre_commune}


Restriction d'événement
    [Documentation]  Teste une double condition dans la restriction :
    ...  date événement <= date limite de notification au pétitionnaire
    ...  date de dépôt == date de complétude

    ${date_valid} =  Set Variable  01/01/2015
    ${date_invalid} =  Set Variable  01/04/2016

    #
    # Cas 1 : opérateur logique || sur P0
    # La condition est satisfaite
    #

    # Création du DI sur lequel nous allons faire l'incomplétude
    &{case1_evenement} =  Create Dictionary
    ...  libelle=Notification de pieces manquante
    ...  restriction=date_evenement <= archive_date_notification_delai || archive_date_complet == date_depot
     &{case1_petitionnaire} =  Create Dictionary
    ...  particulier_civilite=Madame
    ...  particulier_nom=Déziel
    ...  particulier_prenom=Audrey
    ...  om_collectivite=MARSEILLE
    &{case1_demande} =  Create Dictionary
    ...  date_demande=${date_valid}
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    #
    Depuis la page d'accueil  admin  admin
    # On récupère la restriction avant de la modifier
    Depuis le contexte de l'événement  Notification de pieces manquante
    ${restriction_before} =  Get Text  css=#restriction
    #
    Modifier l'événement  ${case1_evenement}
    #
    ${case1_di} =  Ajouter la demande par WS
    ...  ${case1_demande}
    ...  ${case1_petitionnaire}
    # Vu le || la condition est satisfaite
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI  ${case1_di}  Notification de pieces manquante  ${date_invalid}
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.

    #
    # Cas 2 : opérateur logique && sur DOC01
    # La condition n'est pas satisfaite
    #

    # Création du DI sur lequel nous allons faire l'incomplétude
    &{case2_evenement} =  Create Dictionary
    ...  libelle=Notification de pieces manquante
    ...  restriction=date_evenement <= archive_date_notification_delai && archive_date_complet == date_depot
    #
    &{case2_petitionnaire} =  Create Dictionary
    ...  particulier_civilite=Madame
    ...  particulier_nom=Bourgeau
    ...  particulier_prenom=Corinne
    ...  om_collectivite=MARSEILLE
    &{case2_demande_temp} =  Create Dictionary
    ...  date_demande=${date_valid}
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    &{case2_demande} =  Create Dictionary
    ...  date_demande=${date_valid}
    ...  demande_type=Demande d'ouverture de chantier
    #
    Depuis la page d'accueil  admin  admin
    Modifier l'événement  ${case2_evenement}
    #
    ${case2_di_temp} =  Ajouter la demande par WS
    ...  ${case2_demande_temp}
    ...  ${case2_petitionnaire}
    #
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI  ${case2_di_temp}  accepter un dossier sans réserve  ${date_valid}
    #
    Depuis la page d'accueil  guichet  guichet
    ${case2_di} =  Ajouter la demande sur existant
    ...  ${case2_di_temp}
    ...  ${case2_demande}
    # Vu le && la condition est non satisfaite
    Depuis la page d'accueil  instr  instr
    Depuis l'onglet instruction du dossier d'instruction  ${case2_di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction-corner-ajouter
    Saisir instruction  Notification de pieces manquante  ${date_invalid}
    Click On Submit Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=div.ui-state-error p span.text  SAISIE NON ENREGISTRÉE
    Element Should Contain  css=div.ui-state-error p span.text  date d'événement <= date limite de notification au pétitionnaire && date de complétude archivé == date de dépôt

    #
    # Restauration de la restriction
    #
    &{old_evenement} =  Create Dictionary
    ...  libelle=Notification de pieces manquante
    ...  restriction=${restriction_before}
    Depuis la page d'accueil  admin  admin
    Modifier l'événement  ${old_evenement}


TNR Vérifie que le fichier est supprimé à la suppression de l'instruction

    [Documentation]  Vérifie dans le filestorage si le fichier de l'édition de
    ...  l'instruction est correctement supprimé lors de la suppression de
    ...  l'instruction.


    # Liste des arguments pour la demande
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    # Liste des arguments pour le pétitionnaire
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Batard
    ...  particulier_prenom=Laurene
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    #
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI  ${di}  accepter un dossier sans réserve
    #
    Depuis l'instruction du dossier d'instruction  ${di}  accepter un dossier sans réserve
    # On clique sur l'action de finalisation
    Click On SubForm Portlet Action  instruction  finaliser
    # On vérifie le message de validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  La finalisation du document s'est effectuée avec succès.
    # Récupération de l'UID
    Depuis l'instruction du dossier d'instruction  ${di}  accepter un dossier sans réserve
    ${uid} =  Get Value  om_fichier_instruction
    ${path_1} =  Get Substring  ${uid}  0  2
    ${path_2} =  Get Substring  ${uid}  0  4
    # Vérification dans le filestorage
    File Should Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}
    File Should Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}.info
    #
    Depuis la page d'accueil  admin  admin
    Depuis l'instruction du dossier d'instruction  ${di}  accepter un dossier sans réserve
    # On clique sur l'action de définalisation
    Click On SubForm Portlet Action  instruction  definaliser
    # On vérifie le message de validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  La définalisation du document s'est effectuée avec succès.
    #
    Supprimer l'instruction  ${di}  accepter un dossier sans réserve
    # Vérification dans le filestorage
    File Should Not Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}
    File Should Not Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}.info

Mail aux communes
    [Documentation]  Test de l'action sur l'instruction permettant à l'instructeur,
    ...  une fois son courrier finalisé, de déclencher l'envoi d'un mail aux communes.

    # Création du DI
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Gareau
    ...  particulier_prenom=Élisabeth
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    # Création de l'instruction finalisée
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI  ${di}  Notification de pieces manquante
    Click On Back Button In Subform
    Click On Link  Notification de pieces manquante
    Click On SubForm Portlet Action  instruction  finaliser
    # Saisie du paramétrage commune en sus du multi par fourni par défaut
    Depuis la page d'accueil  admingen  admingen
    Ajouter le paramètre depuis le menu  param_courriel_de_notification_commune  support@atreal.fr  MARSEILLE
    # Succès de la notification
    Depuis l'instruction du dossier d'instruction  ${di}  Notification de pieces manquante
    Click On SubForm Portlet Action  instruction  notifier_commune
    Click Element  css=div.ui-dialog-buttonset button
    Valid Message Should Be In Subform  La commune a été notifiée.
    # Échec de la notification si objet, modèle ou courriel indéfini
    Modifier le paramètre  param_courriel_de_notification_commune_objet_depuis_instruction  ${SPACE}
    Modifier le paramètre  param_courriel_de_notification_commune_modele_depuis_instruction  ${SPACE}
    Modifier le paramètre  param_courriel_de_notification_commune  ${SPACE}
    Depuis l'instruction du dossier d'instruction  ${di}  Notification de pieces manquante
    Click On SubForm Portlet Action  instruction  notifier_commune
    Click Element  css=div.ui-dialog-buttonset button
    Error Message Should Contain In Subform  l'objet du courriel envoyé aux communes est vide
    Error Message Should Contain In Subform  le modèle du courriel envoyé aux communes est vide
    Error Message Should Contain In Subform  aucun courriel valide de destinataire de la commune


Dossier sans suffixe

    [Documentation]  Teste le workflow des DI lorsque l'initial n'a pas le suffixe P0

    ${date_jour} =  Date du jour FR

    # Désactivation du suffixe pour les PCI initiaux
    Depuis la page d'accueil  admin  admin
    Go To Tab  dossier_instruction_type
    Use Simple Search  type de dossier d'autorisation détaillé  PCI (Permis de construire pour une maison individuelle et / ou ses annexes)
    Click On Link  Initial
    Click On Form Portlet Action  dossier_instruction_type  modifier
    Unselect Checkbox  suffixe
    Click On Submit Button

    # Nouveau DI initial sans le suffixe P0
        &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Netton
    ...  particulier_prenom=Valérie
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    Should Not Contain  ${di}  P0

    # Nouveau dossier sur existant
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI et la finaliser  ${di}  accepter un dossier sans réserve  ${date_jour}
    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande de modification
    ...  dossier_instruction=${di}
    ${di_M01} =  Ajouter la demande par WS  ${args_demande}
    Should Contain  ${di_M01}  M01

    # Nouvel événement d'instruction sans création de dossier
    Ajouter une instruction au DI et la finaliser  ${di_M01}  Notification de pieces manquante  ${date_jour}
    Depuis la page d'accueil  guichet  guichet
    &{args_demande} =  Create Dictionary
    ...  demande_type=Dépôt de pièces complémentaire
    ...  dossier_instruction=${di_M01}
    Ajouter la demande par WS  ${args_demande}

    # Ré-activation du suffixe pour les PCI initiaux
    Depuis la page d'accueil  admin  admin
    Go To Tab  dossier_instruction_type
    Use Simple Search  type de dossier d'autorisation détaillé  PCI (Permis de construire pour une maison individuelle et / ou ses annexes)
    Click On Link  Initial
    Click On Form Portlet Action  dossier_instruction_type  modifier
    Select Checkbox  suffixe
    Click On Submit Button


Vérification de retour d'instruction
    [Documentation]  Controle des date de retour d'une instruction

    &{args_action} =  Create Dictionary
    ...  action=retour signature
    ...  libelle=retour signature
    ...  regle_etat=etat
    ...  regle_date_validite=date_retour_signature + duree_validite

    @{etat_evenment_dispo} =  Create List  dossier accepter
    @{type_di} =  Create List  PCI - P - Initial

    &{args_evenement_creation} =  Create Dictionary
    ...  libelle=retour signature
    ...  etats_depuis_lequel_l_evenement_est_disponible=@{etat_evenment_dispo}
    ...  retour=true
    ...  dossier_instruction_type=${type_di}
    ...  action=retour signature

    &{args_evenement_modification} =  Create Dictionary
    ...  libelle=accepter un dossier sans réserve
    ...  evenement_retour_signature=retour signature

    &{args_type_DA_detaille_modification} =  Create Dictionary
    ...  duree_validite_parametrage=12

    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=particulier
    ...  particulier_nom=DURAND
    ...  particulier_prenom=MICKAEL
    ...  particulier_date_naissance=03/01/1956
    ...  particulier_commune_naissance=LILLE
    ...  particulier_departement_naissance=NORD
    ...  numero=12
    ...  voie=RUE DE LA LOI
    ...  complement=APPT 12
    ...  localite=MARSEILLE
    ...  code_postal=13012
    ...  telephone_fixe=0404040404
    ...  om_collectivite=agglo

    &{args_demande} =  Create Dictionary
    ...  om_collectivite=agglo
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial

    ${date_retour_signature} =  Date du jour FR
    ${date_retour_signature} =  Add Time To Date  ${date_retour_signature}  5 days  %d/%m/%Y  True  %d/%m/%Y
    ${dd} =  Convert Date  ${date_retour_signature}  %d  True  %d/%m/%Y
    ${mm} =  Convert Date  ${date_retour_signature}  %m  True  %d/%m/%Y
    ${yyyy} =  Convert Date  ${date_retour_signature}  %Y  True  %d/%m/%Y
    ${yyyy} =  Evaluate  ${yyyy}+1
    ${date_validite} =  Catenate  SEPARATOR=/  ${dd}  ${mm}  ${yyyy}
    # On créer une action et un evenement d'instruction retour de signature
    Depuis la page d'accueil  admin  admin
    Ajouter l'action depuis le menu  ${args_action}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Ajouter l'événement depuis le menu  ${args_evenement_creation}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Modifier l'événement  ${args_evenement_modification}
    Go To Tab  dossier_autorisation_type_detaille
    Modifier type de dossier d'autorisation détaillé  PCI  ${args_type_DA_detaille_modification}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # On Créé un DI avec une instruction retour de signature
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    ${code_barres} =  Ajouter une instruction au DI et la finaliser  ${di}  accepter un dossier sans réserve  null  Albert Dupont

    Go To Submenu In Menu  suivi  suivi_mise_a_jour_des_dates
    Select From List By Label  css=#type_mise_a_jour  date de retour de signature + Envoi contrôle légalite
    Input Text  date  ${date_retour_signature}
    Input Text  code_barres  ${code_barres}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#formulaire div.formControls input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#dossier_libelle  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#evenement  accepter un dossier sans réserve
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#suivi_mise_a_jour_des_dates_form div.formControls input.om-button

    Depuis le contexte du dossier d'instruction  ${di}
    Wait Until Element Contains  css=#date_validite  ${date_validite}
    On clique sur l'onglet  instruction  Instruction
    Click On Link  retour signature
    Wait Until Element Contains  css=#date_retour_signature  ${date_retour_signature}


Suppression evenement demande
    [Documentation]  Teste la suppression d'un événement d'instruction lié à une demande qui ne
    ...  crée pas de nouveau dossier d'instruction

    @{etats_autorises} =  Create List  delai de notification envoye

    &{args_demande_type} =  Create Dictionary
    ...  code=test_09_suppression
    ...  libelle=test_09_suppression
    ...  groupe=Autorisation ADS
    ...  dossier_autorisation_type_detaille=AZ (Demande d'autorisation spéciale de travaux dans le périmètre d'une AVAP)
    ...  demande_nature=Dossier existant
    ...  etats_autorises=${etats_autorises}
    ...  contraintes=Avec récupération demandeur
    ...  evenement=Defrichement soumis a enquete publique

    # Création du type de damande pour le DI
    Depuis la page d'accueil  admin  admin
    Ajouter un nouveau type de demande depuis le menu  ${args_demande_type}

    # Ajout de la demande au DI
    Depuis la page d'accueil  guichet  guichet
    Go To Tab  demande_dossier_encours
    Input Text  css=#adv-search-classic-fields input  AZ0130551200001P0
    Click Element  adv-search-submit
    Wait Until Page Contains Element  css=#action-tab-demande_dossier_encours-left-consulter-AZ0130551200001P0
    Click Element  css=#action-tab-demande_dossier_encours-left-consulter-AZ0130551200001P0
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  css=#fieldset-form-demande_dossier_encours-type-de-dossier-demande #etat
    ${etat} =  Get Value  etat
    Should Be Equal  ${etat}  delai de notification envoye
    Select From List By Label  css=#demande_type  test_09_suppression
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # Suppression de la demande
    Depuis la page d'accueil  admin  admin
    Depuis l'onglet instruction du dossier d'instruction  AZ 013055 12 00001P0
    Click On Link  Defrichement soumis a enquete publique
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  instruction  definaliser
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  instruction  supprimer
    Click On Submit Button In Subform
    Valid Message Should Contain  La suppression a été correctement effectuée.

Copie des donnees DA vers nouveau DI
    [Documentation]  Ce test case vérifie que les données du dossier d'autorisation sont
    ...  recopiés dans les champs 'archive_' de l'événement d'instruction de la création
    ...  du nouveau dossier d'instruction.

    &{args_action} =  Create Dictionary
    ...  action=test_12_recopie_donnees
    ...  libelle=test_12_recopie_donnees
    ...  regle_date_validite=archive_date_validite+12

    @{etat_evenement_dispo} =  Create List  dossier accepter
    @{type_di} =  Create List  PCI - P - Initial
    &{args_evenement_creation} =  Create Dictionary
    ...  libelle=test_12_recopie_donnees
    ...  etats_depuis_lequel_l_evenement_est_disponible=@{etat_evenement_dispo}
    ...  dossier_instruction_type=${type_di}
    ...  action=test_12_recopie_donnees

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=test recopie
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE

    &{args_type_instr} =  Create Dictionary
    ...  code=DT
    ...  libelle=test_12_recopie_donnees
    ...  dossier_autorisation_type_detaille=PCI (Permis de construire pour une maison individuelle et / ou ses annexes)
    ...  suffixe=true

    @{etats_autorises} =  Create List  dossier accepter
    &{args_type} =  Create Dictionary
    ...  code=test_12_recopie_donnees
    ...  libelle=test_12_recopie_donnees
    ...  groupe=Autorisation ADS
    ...  dossier_autorisation_type_detaille=PCI (Permis de construire pour une maison individuelle et / ou ses annexes)
    ...  demande_nature=Dossier existant
    ...  etats_autorises=${etats_autorises}
    ...  dossier_instruction_type=PCI - test_12_recopie_donnees
    ...  evenement=test_12_recopie_donnees

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI  ${di}  accepter un dossier sans réserve
    Depuis le contexte du dossier d'instruction  ${di}
    Wait Until Element Contains  css=#avis_decision  Favorable

    Depuis la page d'accueil  admin  admin
    Ajouter l'action depuis le menu  ${args_action}
    Ajouter l'événement depuis le menu  ${args_evenement_creation}
    Ajouter type de dossier d'instruction  ${args_type_instr}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Ajouter un nouveau type de demande depuis le menu  ${args_type}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Click On Back Button

    &{args_demande_modification} =  Create Dictionary
    ...  demande_type=test_12_recopie_donnees

    Depuis la page d'accueil  guichet  guichet

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=test validité
    ${di} =  Ajouter la demande sur dossier en cours depuis le menu  ${di}  ${args_demande_modification}  ${args_petitionnaire}

    ${yyyy} =  Get Time  year
    ${mm} =  Get Time  month
    ${dd} =  Get Time  day
    ${yyyy} =  Evaluate  ${yyyy}+1
    ${date_validite} =  Catenate  SEPARATOR=/  ${dd}  ${mm}  ${yyyy}
    Depuis le contexte du dossier d'instruction  ${di}
    Wait Until Element Contains  css=#date_validite  ${date_validite}

Verification numerotation DI successif
    [Documentation]  Vérifie que la numérotation des DI est successive, et que l'option
    ...  suffixe fonctionne.

    &{args_type_instr} =  Create Dictionary
    ...  code=TN
    ...  libelle=Test numérotation
    ...  dossier_autorisation_type_detaille=PCI (Permis de construire pour une maison individuelle et / ou ses annexes)
    ...  suffixe=true

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Lafontaine
    ...  particulier_prenom=Isaac
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE

    @{etats_autorises} =  Create List  dossier accepter
    &{args_type} =  Create Dictionary
    ...  code=TN
    ...  libelle=Test numérotation
    ...  groupe=Autorisation ADS
    ...  dossier_autorisation_type_detaille=PCI (Permis de construire pour une maison individuelle et / ou ses annexes)
    ...  demande_nature=Dossier existant
    ...  etats_autorises=${etats_autorises}
    ...  dossier_instruction_type=PCI - ${args_type_instr.libelle}
    ...  evenement=Notification du delai legal maison individuelle

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI  ${di}  accepter un dossier sans réserve

    Depuis la page d'accueil  admin  admin
    Ajouter type de dossier d'instruction  ${args_type_instr}
    Valid Message Should Contain  Vos modifications ont bien été enregistrée
    Ajouter un nouveau type de demande depuis le menu  ${args_type}
    Depuis le contexte du type de demande avec libellé unique  ${args_type.libelle}


    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=particulier
    ...  particulier_nom=Test nouveau di

    &{args_demande} =  Create Dictionary
    ...  demande_type=${args_type.libelle}
    ...  dossier_instruction=${di}

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    ${di_se} =  Sans espace  ${di}
    Should Match Regexp  ${di_se}  (PC)[0-9 ]*(TN01)


Recalcul données DI vers DA
    [Documentation]  Ce test case vérifie la copie des données techniques du DI vers le DA
    ...  après la clôture du DI. Il vérifie également qu'à la clôture du DI, l'état du DA
    ...  passe bien aussi en clôturé.

    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=particulier
    ...  particulier_nom=Vaillancour
    ...  particulier_prenom=Alphonse
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE

    &{donnees_techniques_values} =  Create Dictionary
    ...  su_cstr_shon1=120

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    Depuis la page d'accueil  instr  instr
    Modifier les données techniques pour le calcul des surfaces  ${di}  ${donnees_techniques_values}
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#sousform-dossier_autorisation  En cours
    Ajouter une instruction au DI  ${di}  accepter un dossier sans réserve
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#sousform-dossier_autorisation  Accordé

    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${di}
    Click Element  css=#sousform-dossier_autorisation .consult-16
    Wait Until Element Is Visible  donnees_techniques_da
    Click Element  css=#donnees_techniques_da
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#fieldset-sousform-donnees_techniques-construire legend
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#fieldset-sousform-donnees_techniques-destinations-et-surfaces-des-constructions legend
    Element Should Contain  css=#su_cstr_shon1  120

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Guédry
    ...  particulier_prenom=Paul
    ...  om_collectivite=MARSEILLE

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instr  instr
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#sousform-dossier_autorisation  En cours
    Ajouter une instruction au DI  ${di}  refuser un dossier
    On clique sur l'onglet  lien_dossier_dossier  Dossiers Liés
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#sousform-dossier_autorisation  Refusé


Annulation d'un DA
    [Documentation]  On vérifie que l'ajout d'une demande sur dossier en cours "ANNUL"
    ...  passe bien l'état du DA du dossier ciblé en annulé.

    @{etats_autorises} =  Create List  delai de notification envoye
    &{args_type} =  Create Dictionary
    ...  code=ANNUL
    ...  libelle=Demande d'annulation
    ...  groupe=Autorisation ADS
    ...  dossier_autorisation_type_detaille=PCI (Permis de construire pour une maison individuelle et / ou ses annexes)
    ...  demande_nature=Dossier existant
    ...  etats_autorises=${etats_autorises}
    ...  contraintes=Avec récupération demandeur
    ...  dossier_instruction_type=PCI - Demande d'annulation
    ...  evenement=Abandonner les travaux

    &{args_type_instr} =  Create Dictionary
    ...  code=ANNUL
    ...  libelle=Demande d'annulation
    ...  dossier_autorisation_type_detaille=PCI (Permis de construire pour une maison individuelle et / ou ses annexes)
    ...  suffixe=true
    ...  mouvement_sitadel=SUPPRESSION

    &{args_action_modif} =  Create Dictionary
    ...  regle_avis=avis_decision
    ...  regle_date_decision=date_evenement

    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=particulier
    ...  particulier_nom=test annulation
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE


    Depuis la page d'accueil  admin  admin
    Ajouter type de dossier d'instruction  ${args_type_instr}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Click On Back Button
    Use Simple Search  code  ANNUL
    Click On Link  ANNUL
    Ajouter un nouveau type de demande depuis le menu  ${args_type}
    Depuis le contexte du type de demande avec libellé unique  ${args_type.libelle}
    Go To Tab  action
    Modifier Action  abandon  ${args_action_modif}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    Depuis la page d'accueil  instr  instr
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${di}
    Element Should Contain  css=#sousform-dossier_autorisation  En cours

    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande d'annulation
    ...  dossier_instruction=${di}

    ${di} =  Ajouter la demande par WS  ${args_demande}

    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${di}
    Element Should Contain  css=#sousform-dossier_autorisation  Abandonné


Vérification de l'auto-complement des bibles
    [Documentation]  Ajout de bibles
    ...  remplissage automatique des complement
    ...  et remplissage du premier complement par les consultations

    # Arguments de creations de bible pour l'auto-complement
    &{args_bible1} =  Create Dictionary
    ...  evenement=accepter un dossier sans réserve
    ...  libelle=test 1
    ...  contenu=test contenu 1
    ...  complement=complément 1
    ...  automatique=Oui
    ...  collectivite=agglo
    &{args_bible2} =  Create Dictionary
    ...  evenement=accepter un dossier sans réserve
    ...  libelle=test 2
    ...  contenu=test contenu 2
    ...  complement=complément 2
    ...  automatique=Oui
    ...  collectivite=agglo
    &{args_bible3} =  Create Dictionary
    ...  evenement=accepter un dossier sans réserve
    ...  libelle=test 3
    ...  contenu=test contenu 3
    ...  complement=complément 3
    ...  automatique=Oui
    ...  collectivite=agglo
    &{args_bible4} =  Create Dictionary
    ...  evenement=accepter un dossier sans réserve
    ...  libelle=test 4
    ...  contenu=test contenu 4
    ...  complement=complément 4
    ...  automatique=Oui
    ...  collectivite=agglo

    Depuis la page d'accueil  admin  admin
    Ajouter une bible depuis l'onglet de l'événement  &{args_bible1}
    Click On Back Button In Subform
    Ajouter une bible depuis l'onglet de l'événement  &{args_bible2}
    Click On Back Button In Subform
    Ajouter une bible depuis l'onglet de l'événement  &{args_bible3}
    Click On Back Button In Subform
    Ajouter une bible depuis l'onglet de l'événement  &{args_bible4}
    Click On Back Button In Subform
    Ajouter une bible depuis le paramétrage dossiers  null  test multi 1  test multi contenu 1  complément 1  Oui  null  agglo
    Ajouter une bible depuis le paramétrage dossiers  null  test multi 2  test multi contenu 2  complément 1  Non  null  agglo

    # On test le remplissage automatique et la bible

    Depuis la page d'accueil  instr  instr
    Depuis l'onglet instruction du dossier d'instruction  ${di_ok}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction-corner-ajouter
    Saisir instruction  accepter un dossier sans réserve  null
    Click On Link  automatique
    Click On Link  bible
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=.ui-dialog  V56
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=.ui-dialog  V64
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=.ui-dialog  test multi 1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=.ui-dialog  test multi 2
    Click Element  css=#checkbox5
    Click Element  css=#checkbox8
    Click Element  css=#checkbox9
    Click Element  css=div.ui-dialog>div#upload-container>div>form>div.formControls input[type="submit"]
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    Click On Link  accepter un dossier sans réserve
    Element Should Contain  css=#complement_om_html  contenu 1
    Element Should Contain  css=#complement_om_html  test multi contenu 1
    Element Should Contain  css=#complement_om_html  test multi contenu 2
    Element Should Contain  css=#complement_om_html  Vu l'avis favorable de l'Architecte des Bâtiments de France en date du
    Element Should Contain  css=#complement_om_html  Vu l'avis favorable avec prescriptions de la Direction Régionale des Affaires Culturelles en date du
    Element Should Contain  css=#complement2_om_html  contenu 2
    Element Should Contain  css=#complement3_om_html  contenu 3
    Element Should Contain  css=#complement4_om_html  contenu 4
    Page Should Contain Element  css=.libelle-date_envoi_controle_legalite


    Depuis la page d'accueil  instr  instr
    # Vérification du complement basé sur les consultations. Il vérifie
    # la présence, l'avis et la date.
    Depuis l'onglet instruction du dossier d'instruction  ${di_bible_consultation}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction-corner-ajouter
    Saisir instruction  accepter un dossier avec reserve  null
    Click On Link  automatique
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    Click On Link  accepter un dossier avec reserve
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#complement_om_html  Vu l'avis du service SERAM${\n}${\n}Vu l'avis Favorable du service Direction de l'Eau et de l'Assainissement du ${DATE_FORMAT_DD/MM/YYYY}

    # Lecture de la consultation pour la suite des tests
    Depuis l'onglet consultation(s) du dossier d'instruction  ${di_bible_consultation}
    Click Link  59.01 - Direction de l'Eau et de l'Assainissement
    ${status} =  Run Keyword And Return Status  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Be Visible  css=div > table
    Run Keyword If  ${status} == False  Click Link  59.01 - Direction de l'Eau et de l'Assainissement
    Click On SubForm Portlet Action  consultation  marquer_comme_lu


Modification d'autorité compétente
    [Documentation]    Test du lien automatique entre l'ajout de l'événement d'instruction
    ...  'Changer l'autorité compétente 'commune état'' et la mise à jour de l'autorité
    ...  compétente du dossier.

    ${di} =  Set Variable  AZ 013055 12 00001P0
    Depuis la page d'accueil  instr  instr

    # Vérification de l'autorité compétente de base
    Depuis le contexte du dossier d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#autorite_competente  Commune

    Ajouter une instruction au DI  ${di}  Changer l'autorité compétente 'commune état'
    Click On Back Button In Subform
    Click On Link  Changer l'autorité compétente 'commune état'
    Element Should Not Contain  css=#sousform-instruction #portlet-actions  Édition
    Element Should Not Contain  css=#sousform-instruction #portlet-actions  Finaliser le document
    # On vérifie que le changement est effectif
    Depuis le contexte du dossier d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#autorite_competente  Commune pour état

    # On supprime l'instruction pour revenir à Commune
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du dossier d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#autorite_competente  Commune pour état
    Supprimer l'instruction  ${di}  Changer l'autorité compétente 'commune état'
    Depuis le contexte du dossier d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=#autorite_competente  Commune pour état
    Element Text Should Be  css=#autorite_competente  Commune

Vérification ajout de Lot
    [Documentation]  Ajout simple de lots avec verification d'erreur

     Depuis la page d'accueil  instr  instr
    ${di} =  Set Variable  AZ 013055 12 00001P0
    Depuis le contexte du dossier d'instruction  ${di}
    On clique sur l'onglet  lot  Lot(s)
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-lot-corner-ajouter
    Click On Submit Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=div.ui-state-error p span.text  SAISIE NON ENREGISTRÉE
    Input Text  css=#libelle  Lot n°1
    Click On Submit Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-lot-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#libelle
    Input Text  css=#libelle  Lot n°2
    Click On Submit Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform


Dossier d'instruction à qualifier
    [Documentation]  Vérifie la qualification des dossiers avec le profil de
    ...  qualificateur.

    # On ajoute un dossier d'instruction avec un type qui demande qualification
    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=particulier
    ...  particulier_nom=DURAND
    ...  particulier_prenom=MICKAEL
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    # On vérifie que le dossier soit bien affiché pour le qualificateur
    Depuis la page d'accueil  qualif  qualif
    Element Should Contain  css=#widget_15  ADS
    Click On Link  Voir tous mes dossiers à qualifier
    Use Simple Search  Tous  ${di}
    Click On Link  ${di}
    Element should Contain  css=#a_qualifier  Oui
    # Une fois qualifié, le dossier ne doit plus apparaître dans le listing des
    # qualificateurs
    Click On Form Portlet Action  dossier_instruction  modifier
    Set Checkbox  a_qualifier  false
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Go To Tab  dossier_qualifier_qualificateur
    Element Should Not Contain  css=#tab-dossier_qualifier_qualificateur .tab-tab  ${di}


Ajout de contraintes
    [Documentation]   Le but de ce test est de vérifier qu'un utilisateur avec
    ...  le profil qualificateur puisse ajouter des contraintes sur un dossier
    ...  d'instruction.

    # On ajoute un dossier d'instruction avec un type qui demande qualification
    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=particulier
    ...  particulier_nom=HOUDE
    ...  particulier_prenom=Pierre
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    # On ajoute une contrainte avec le profil du qualificateur
    Depuis la page d'accueil  qualif  qualif
    Go To Tab  dossier_qualifier_qualificateur
    Use Simple Search  Tous  ${di}
    Click On Link  ${di}
    On clique sur l'onglet  dossier_contrainte  Contrainte(s)
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#action-soustab-dossier_contrainte-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#fieldset-sousform-dossier_contrainte-contraintes-openads legend
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#fieldset-sousform-dossier_contrainte-environnement legend
    Click Element  css=#contrainte_5
    Click Element  css=input[value='Appliquer les changements']
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=.message  La contrainte Pollution puits a été ajoutée au dossier.
    Click On Back Button In Subform
    Element Should Contain  css=#sousform-dossier_contrainte  Le puits d'une profondeur de [...] est pollué.


TNR Instructeur sans division

    [Documentation]  Un instructeur sans division ne doit pas pouvoir instruire
    ...  de dossier.

    # Ajout d'un instructeur
    Depuis la page d'accueil  admin  admin
    Ajouter l'utilisateur  Test  test@test.fr  instrnodiv  instrnodiv  INSTRUCTEUR  MARSEILLE

    #
    #
    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=instrnodiv
    ...  personne_morale_raison_sociale=instrnodiv
    ...  personne_morale_civilite=Monsieur
    ...  personne_morale_nom=instrnodiv
    ...  om_collectivite=MARSEILLE
    ...  personne_morale_prenom=instrnodiv

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE

    ${di_nodiv} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instrnodiv  instrnodiv

    Depuis l'onglet instruction du dossier d'instruction  ${di_nodiv}
    Page Should Not Contain  css=#action-soustab-instruction-corner-ajouter

    Depuis l'onglet contrainte(s) du dossier d'instruction  ${di_nodiv}
    Page Should Not Contain  css=#action-soustab-dossier_contrainte-corner-ajouter

    Depuis l'onglet consultation(s) du dossier d'instruction  ${di_nodiv}
    Page Should Not Contain  css=#action-soustab-consultation-corner-ajouter

    Depuis le contexte du dossier d'instruction  ${di_nodiv}
    On clique sur l'onglet  dossier_commission  Commission(s)
    Page Should Not Contain  css=#action-soustab-dossier_commission-corner-ajouter

    Depuis le contexte du dossier d'instruction  ${di_nodiv}
    On clique sur l'onglet  lot  Lot(s)
    Page Should Not Contain  css=#action-soustab-lot-corner-ajouter

    Depuis l'onglet des messages du dossier d'instruction  ${di_nodiv}
    Page Should Not Contain  css=#action-soustab-blocnote-message-ajouter

    Depuis le contexte du dossier d'instruction  ${di_nodiv}
    On clique sur l'onglet  blocnote  Bloc-note
    Page Should Not Contain  css=#action-soustab-blocnote-corner-ajouter

    Depuis l'onglet des pièces du dossier d'instruction  ${di_nodiv}
    Page Should Not Contain  css=#action-soustab-blocnote-message-ajouter


Champ contentieux de la consultation du DI
    [Documentation]  Ce test case vérifie que le champ contentieux du DI affiche
    ...  bien les pictogrammes RE et IN si les références cadastrales du dossier
    ...  sont en commun avec respectivement au moins un dossier RE et IN non
    ...  clôturé.

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Mylène
    ...  particulier_prenom=Françoise
    ...  om_collectivite=MARSEILLE

    @{ref_cad} =  Create List  001  AA  0007

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ...  om_collectivite=MARSEILLE

    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Mélisande
    ...  particulier_prenom=Amélie
    ...  om_collectivite=MARSEILLE

    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Wanda
    ...  particulier_prenom=Manon
    ...  om_collectivite=MARSEILLE

    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}

    &{args_demande_inf} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ...  terrain_references_cadastrales=${ref_cad}

    ${args_peti} =  Create Dictionary

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  assist  assist
    # On vérifie l'existence du champ et l'absence de dossier contentieux
    Depuis le contexte du dossier d'instruction  ${di}
    Element Should Contain  css=#fieldset-form-dossier_instruction-enjeu  contentieux
    Element Should Not Contain  css=#fieldset-form-dossier_instruction-enjeu  IN
    Element Should Not Contain  css=#fieldset-form-dossier_instruction-enjeu  RE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Recours contentieux
    ...  demande_type=Dépôt Initial REC
    ...  autorisation_contestee=${di}
    ...  om_collectivite=MARSEILLE
    ${di_re} =  Ajouter la demande par WS  ${args_demande}

    # On vérifie l'existence du champ et de RE
    Depuis le contexte du dossier d'instruction  ${di}
    Element Should Contain  css=#fieldset-form-dossier_instruction-enjeu  contentieux
    Element Should Not Contain  css=#fieldset-form-dossier_instruction-enjeu  IN
    Element Should Contain  css=#fieldset-form-dossier_instruction-enjeu  RE

    ${di_inf} =  Ajouter la demande par WS  ${args_demande_inf}  ${args_peti}  ${args_autres_demandeurs}

    # On vérifie l'existence du champ et de RE, IN
    Depuis le contexte du dossier d'instruction  ${di}
    Element Should Contain  css=#fieldset-form-dossier_instruction-enjeu  contentieux
    Element Should Contain  css=#fieldset-form-dossier_instruction-enjeu  RE IN

    # On s'assure que les pictogrammes ne sont plus là lorsque les dossiers
    # sont cloturés
    Ajouter une instruction au DI  ${di_re}  accepter un dossier sans réserve  null  recours
    Ajouter une instruction au DI  ${di_inf}  accepter un dossier sans réserve  null  infraction

    Depuis le contexte du dossier d'instruction  ${di}
    Element Should Contain  css=#fieldset-form-dossier_instruction-enjeu  contentieux
    Element Should Not Contain  css=#fieldset-form-dossier_instruction-enjeu  IN
    Element Should Not Contain  css=#fieldset-form-dossier_instruction-enjeu  RE


TNR champs de fusion et variables de remplacement des éditions
    [Documentation]  On vérifie les champs spéciaux des éditions
    ...  les points verifiés sont:
    ...  - un champ de fusion qui affiche une variable de remplacement qui a un champ de fusion
    ...  - une variable de remplacement qui affiche un champ de fusion qui a une variable de remplacement
    ...  - une variable de remplacement qui affiche une variable de remplacement
    ...  - un champ de fusion qui affiche un champ de fusion

    Depuis la page d'accueil  admin  admin
    Ajouter le paramètre depuis le menu  test1  [complement2_instruction]  MARSEILLE
    Ajouter le paramètre depuis le menu  test2  [complement3_instruction]  MARSEILLE
    Ajouter le paramètre depuis le menu  test3  test_final_variable  MARSEILLE
    Ajouter le paramètre depuis le menu  test4  &test3  MARSEILLE

    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=Larocque
    ...  personne_morale_raison_sociale=Cerise
    ...  personne_morale_nom=Larocque
    ...  personne_morale_prenom=Cerise
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instr  instr
    Depuis l'onglet instruction du dossier d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction-corner-ajouter
    Saisir instruction  TNR d'imbrication de champs de fusion et variables de remplacement
    Input HTML  complement_om_html  &test1
    Input HTML  complement2_om_html  test_final_fusion
    Input HTML  complement3_om_html  &test3
    Input HTML  complement4_om_html  [complement2_instruction]
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    Click Link  TNR d'imbrication de champs de fusion et variables de remplacement
    Click On SubForm Portlet Action  instruction  edition

    Open PDF  sousform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  test_final_variable

    ${body_text} =  Get Text  css=#viewer
    ${lines} =  Get Lines Matching Pattern  ${body_text}  test_final_variable
    ${count} =  Get Line Count  ${lines}
    Should Be Equal As Strings  ${count}  2

    ${lines} =  Get Lines Matching Pattern  ${body_text}  test_final_fusion
    ${count} =  Get Line Count  ${lines}
    Should Be Equal As Strings  ${count}  2

    Close PDF


TNR Les log d'instruction ne doivent pas apparaitre
    [Documentation]  On vérifie l'absence de log_instructions dans la page

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Torri
    ...  particulier_prenom=Renato
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI  ${di}  accepter un dossier sans réserve
    Depuis Le Contexte Du Dossier D'instruction  ${di}
    Page Should Not Contain Element  log_instructions


TNR Bug bind de l'overlay ne s'effectuer plus
    [Documentation]  On vérifie que même après le chargement JS d'un  form
    ...  les overlay s'ouvre toujours.

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Charline
    ...  particulier_prenom=Pinette
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instrpoly  instrpoly

    # Chargement JS
    Depuis le contexte du dossier d'instruction  ${di}
    Click On Form Portlet Action  dossier_instruction  recepisse
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Le récépissé de la demande a été régénéré.

    # On fait appel à l'overlay
    Click On Form Portlet Action  dossier_instruction  donnees_techniques
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#fieldset-sousform-donnees_techniques-construire legend
