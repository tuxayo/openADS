*** Settings ***
Documentation  Test les dépôts de demandes

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Keywords ***
Vérifier l'intégration de l'ajout d'une nouvelle demande / nouveau dossier avec l'utilisateur

    [Documentation]  'Guichet Unique > Nouvelle Demande > Nouveau Dossier'
    ...  - Vérification des éléments d'intégration
    ...  - Saisie de la demande et du pétitionnaire principal
    ...  - Vérification du messgae de validation :
    ...     * info sur le DA,
    ...     * info sur le DI,
    ...     * lien vers le récepissé
    ...  - Ouverture du récepissé
    ...  - Retour vers le tableau de bord

    [Arguments]  ${user}  ${password}

    # On se connecte à l'application
    Depuis la page d'accueil  ${user}  ${password}

    # On clique sur l'entrée de menu dédiée et on vérifie l'intégration
    # - ouverture du menu
    # - titre de la page
    # - titre de l'onglet
    Go To Submenu In Menu  guichet_unique  nouveau-dossier
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Nouveau Dossier
    First Tab Title Should Be  Demande

    # Informations à saisir
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_civilite=Monsieur
    ...  particulier_nom=DURAND
    ...  particulier_prenom=MICKAEL
    ...  particulier_date_naissance=03/01/1956
    ...  particulier_commune_naissance=LILLE
    ...  particulier_departement_naissance=NORD
    ...  numero=12
    ...  voie=RUE DE LA LOI
    ...  complement=APPT 12
    ...  localite=MARSEILLE
    ...  code_postal=13012
    ...  telephone_fixe=0404040404
    ...  fax=0405040404
    # On remplit les champs de la demande
    Saisir la demande  ${args_demande}
    # On ajoute le pétitionnaire
    Ajouter le demandeur  petitionnaire_principal  ${args_petitionnaire}
    # On vérifie que le nom du pétitionnaire saisi est bien affiché dans le
    # formulaire de la demande
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#petitionnaire_principal_delegataire .synthese_demandeur  DURAND MICKAEL
    # On valide
    Click On Submit Button

    # Vérification du message de validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # On vérifie l'intégration
    # - ouverture du menu
    # - titre de la page
    # - titre de l'onglet
    Submenu In Menu Should Be Selected  guichet_unique  nouveau-dossier
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Nouveau Dossier
    First Tab Title Should Be  Demande

    # Vérification qu'il n'y a aucune erreur
    Page Should Not Contain Errors
    # On vérifie le message
    Valid Message Should Contain  Création du dossier d'autorisation n°
    Valid Message Should Contain  Création du dossier d'instruction n°

    # On télécharge le récépissé de la demande
    Click On Link  link_demande_recepisse
    # On vérifie le contenu du PDF
    Open PDF  form
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  RECEPISSE DE DEPOT
    Page Should Contain  DURAND MICKAEL
    Close PDF

    # On clique sur le bouton retour
    Click On Back Button
    Page Title Should Be  Tableau De Bord


Vérifier l'intégration de la rubrique 'Guichet Unique' avec l'utilisateur

    [Documentation]  Ce test vise uniquement à vérifier que les écrans
    ...  correspondant à chaque entrée de menu de la rubrique 'Guichet Unique'
    ...  ne génère pas une erreur de base de données

    [Arguments]  ${user}  ${password}

    # On se connecte à l'application
    Depuis la page d'accueil  ${user}  ${password}

    # On vérifie le menu "Nouveau Dossier"
    Go To Submenu In Menu  guichet_unique  nouveau-dossier
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Nouveau Dossier
    First Tab Title Should Be  Demande
    Page Should Not Contain Errors
    # On vérifie le menu "Dossier En Cours"
    Go To Submenu In Menu  guichet_unique  dossier-existant
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Dossier En Cours
    First Tab Title Should Be  Demande
    Page Should Not Contain Errors
    # On vérifie le menu "Autre Dossier"
    Go To Submenu In Menu  guichet_unique  autre-dossier
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Autre Dossier
    First Tab Title Should Be  Demande
    Page Should Not Contain Errors
    # On vérifie le menu "Récépissé"
    Go To Submenu In Menu  guichet_unique  pdf
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Récépissé
    First Tab Title Should Be  Demande
    Page Should Not Contain Errors
    # On vérifie le menu "Pétitionnaire Fréquent"
    Go to Submenu In Menu  guichet_unique  petitionnaire_frequent
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Pétitionnaire Fréquent
    First Tab Title Should Be  Pétitionnaire Fréquent
    Page Should Not Contain Errors
    # On vérifie le menu "Registre"
    Go To Submenu In Menu  guichet_unique  affichage_reglementaire_registre
    Page Title Should Be  Guichet Unique > Affichage Réglementaire > Registre
    First Tab Title Should Be  Imprimer Le Registre D'affichage Réglementaire
    Page Should Not Contain Errors
    # On vérifie le menu "Attestation"
    Go To Submenu In Menu  guichet_unique  affichage_reglementaire_attestation
    Page Title Should Be  Guichet Unique > Affichage Réglementaire > Attestation
    First Tab Title Should Be  Imprimer L'attestation D'affichage Réglementaire
    Page Should Not Contain Errors


*** Test Cases ***
Création du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer le jeu de données

    Depuis la page d'accueil  admin  admin
    @{etats_autorises} =    Create List
    ...    delai majore
    ...    delai de notification envoye
    ...    dossier sans notification de delai

    &{args_demande_type} =  Create Dictionary
    ...    code=TESTDOC
    ...    libelle=TESTDOC
    ...    groupe=Autorisation ADS
    ...    dossier_autorisation_type_detaille=PCA (Permis de construire comprenant ou non des démolitions)
    ...    demande_nature=Nouveau dossier
    ...    etats_autorises=@{etats_autorises}
    ...    dossier_instruction_type=PCA - Modificatif
    ...    evenement=Notification de delai
    ...    document_obligatoire=Doc1

    Ajouter un nouveau type de demande depuis le menu    ${args_demande_type}


    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Guérette
    ...  particulier_prenom=Hedvige
    ...  om_collectivite=MARSEILLE
    @{ref_cad} =  Create List  789  AB  23
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ...  om_collectivite=MARSEILLE
    ${libelle_di_cadastre} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    Ajouter une instruction au DI et la finaliser  ${libelle_di_cadastre}  accepter un dossier sans réserve
    Set Suite Variable  ${libelle_di_cadastre}

Intégration 'Guichet Unique'

    [Documentation]  Intégration 'Guichet Unique'.

    # Profil GUICHET UNIQUE (mono)
    Vérifier l'intégration de la rubrique 'Guichet Unique' avec l'utilisateur  guichet  guichet
    # Profil ADMINISTRATEUR FONCTIONNEL (mono)
    Vérifier l'intégration de la rubrique 'Guichet Unique' avec l'utilisateur  adminfonct  adminfonct

Intégration 'Guichet Unique > Nouvelle Demande > Nouveau Dossier'

    [Documentation]  Intégration 'Guichet Unique > Nouvelle Demande > Nouveau Dossier'.

    # Profil GUICHET UNIQUE (mono)
    Vérifier l'intégration de l'ajout d'une nouvelle demande / nouveau dossier avec l'utilisateur  guichet  guichet
    # Profil ADMINISTRATEUR FONCTIONNEL (mono)
    Vérifier l'intégration de l'ajout d'une nouvelle demande / nouveau dossier avec l'utilisateur  adminfonct  adminfonct


Intégration 'Guichet Unique > Nouvelle Demande > Autre Dossier'

    [Documentation]  Intégration 'Guichet Unique > Nouvelle Demande > Autre Dossier'.
    ...  - Dépôt d'un modificatif (M01) sur un dossier initial accepté (P0)

    #
    # Constitution du jeu de données spécifique à ce TestCase
    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Beauchamps
    ...  particulier_prenom=Jeanette
    ...  om_collectivite=MARSEILLE
    @{ref_cad} =  Create List  806  AB  25  A  30
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ...  om_collectivite=MARSEILLE
    ${libelle_di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    ${libelle_di_sans_espace} =  Sans espace  ${libelle_di}
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI et la finaliser  ${libelle_di}  accepter un dossier sans réserve

    #
    # Ajout de la nouvelle demande
    #
    # On se connecte en tant que guichet unique
    Depuis la page d'accueil  guichet  guichet
    # On clique sur l'entrée de menu dédiée et on vérifie l'intégration
    # - ouverture du menu
    # - titre de la page
    # - titre de l'onglet
    Go To Submenu In Menu  guichet_unique  autre-dossier
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Autre Dossier
    First Tab Title Should Be  Demande

    # On fait une recherche sur le libellé du DI
    Input Text  recherche  ${libelle_di_sans_espace}
    # On valide
    Click On Search Button
    # On clique sur le bouton ajouter du dossier correspondant
    Click Element  css=#action-tab-demande_autre_dossier-left-consulter-${libelle_di_sans_espace}

    # Intégration
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Autre Dossier > ${libelle_di}
    Submenu In Menu Should Be Selected  guichet_unique  autre-dossier

    # Saisie des informations de la demande
    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande de modification
    # On remplit le formulaire
    Saisir la demande  ${args_demande}

    # On valide
    Click On Submit Button
    # Vérification du message de validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Création du dossier d'instruction n°
    # Vérification qu'il n'y a aucune erreur
    Page Should Not Contain Errors

    # On télécharge le récépissé de la demande
    Click On Link  link_demande_recepisse
    # On vérifie le contenu du PDF
    Open PDF  form
    # On vérifie le contenu
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  RECEPISSE DE DEPOT
    Page Should Contain  Beauchamps Jeanette
    Close PDF

    # On clique sur le bouton retour
    Click On Back Button
    Page Title Should Be  Tableau De Bord


Intégration 'Guichet Unique > Nouvelle Demande > Récépissé'

    [Documentation]  Intégration 'Guichet Unique > Nouvelle Demande > Récépissé'.
    ...  Vérification des éléments de l'interface et de l'enchainement des écrans
    ...  liés à l'entrée de menu en question permettant de rechercher parmi les
    ...  demandes existantes pour pouvoir éditer de nouveau le récépissé lié.

    #
    # Constitution du jeu de données spécifique à ce TestCase
    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=MARTINEZ
    ...  particulier_prenom=Jacques
    ...  om_collectivite=MARSEILLE
    @{ref_cad} =  Create List  810  A  20  A  25
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ...  om_collectivite=MARSEILLE
    ${libelle_di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    ${libelle_di_sans_espace} =  Sans espace  ${libelle_di}

    Depuis la page d'accueil  guichet  guichet

    # On clique sur l'entrée de menu dédiée et on vérifie l'intégration
    # - ouverture du menu
    # - titre de la page
    # - titre de l'onglet
    Go To Submenu In Menu  guichet_unique  pdf
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Récépissé
    First Tab Title Should Be  Demande

    # On fait une recherche sur le libellé du DI
    Input Text  recherche  ${libelle_di_sans_espace}
    Click On Search Button
    # On clique sur le libellé du dossier
    Click Link  ${libelle_di}

    # On vérifie l'intégration
    # - ouverture du menu
    # - titre de la page
    # - titre de l'onglet
    Submenu In Menu Should Be Selected  guichet_unique  pdf
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Récépissé > ${libelle_di}
    First Tab Title Should Be  Demande

    # Les informations sur la demande doivent être correcetes
    Element Text Should Be  css=#dossier_autorisation_type_detaille  ${args_demande.dossier_autorisation_type_detaille}
    Element Text Should Be  css=#demande_type  ${args_demande.demande_type}
    Element Should Contain  css=#petitionnaire_principal_delegataire .synthese_demandeur  ${args_petitionnaire.particulier_nom} ${args_petitionnaire.particulier_prenom}

    # On clique sur l'action dédiée 'Éditer le récepissé PDF'
    Click On Form Portlet Action  demande  pdfetat
    # On vérifie le contenu du PDF
    Open PDF  form
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  RECEPISSE DE DEPOT
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${libelle_di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${args_petitionnaire.particulier_nom} ${args_petitionnaire.particulier_prenom}
    Close PDF

    # On clique sur "Retour"
    Click On Back Button
    # On vérifie l'intégration
    # - ouverture du menu
    # - titre de la page
    # - titre de l'onglet
    Submenu In Menu Should Be Selected  guichet_unique  pdf
    Page Title Should Be  Guichet Unique > Nouvelle Demande > Récépissé
    First Tab Title Should Be  Demande


Affichage de la date de dépôt par defaut
    [Documentation]  Permet de vérifier le bon fonctionnement de l'affichage de
    ...    la date de dépôt ou non

    Depuis la page d'accueil  admin  admin
    # On ouvre le menu nouveau dossier
    Depuis le contexte de nouvelle demande via le menu
    # On sélectionne le type de dossier d'autorisation détaillé
    Select From List By Label  dossier_autorisation_type_detaille  Permis de construire comprenant ou non des démolitions
    Select From List By Label  demande_type  Dépôt Initial
    Textfield Should Contain  date_demande  ${date_ddmmyyyy}

    Ajouter le paramètre depuis le menu  option_date_depot_demande_defaut  false  agglo

    # On ouvre le menu nouveau dossier
    Depuis le contexte de nouvelle demande via le menu
    # On sélectionne le type de dossier d'autorisation détaillé
    Select From List By Label  dossier_autorisation_type_detaille  Permis de construire comprenant ou non des démolitions
    Select From List By Label  demande_type  Dépôt Initial
    Textfield Should Contain  date_demande  ${EMPTY}

    Modifier le paramètre   option_date_depot_demande_defaut  true  agglo

Ajout demande avec documents obligatoires
    [Documentation]  L'objet de ce 'Test Case' est de vérifier l'ajout d'une demande
    ...    avec des documents obligatoire

    # En tant que guichetier
    Depuis la page d'accueil  guichet  guichet
    # On ouvre le menu nouveau dossier
    Depuis le contexte de nouvelle demande via le tableau de bord
    # On sélectionne le type de dossier d'autorisation détaillé
    Select From List By Label  dossier_autorisation_type_detaille  Permis de construire comprenant ou non des démolitions
    # On sélectionne le type de demande
    Select From List By Label  demande_type  TESTDOC
    # Vérification du chargement du dialog
    Wait Until Keyword Succeeds    ${TIMEOUT}    ${RETRY_INTERVAL}    Element Should Contain    css=#ui-dialog-title-liste_doc    Liste des documents obligatoires
    # Validation du dialog
    Click Button    Valider
    # Vérification de l'erreur
    ${alert} =  Get Alert Message
    Should Be Equal As Strings  ${alert}  Tous les documents doivent être présents. Dans le cas contraire, rejeter la demande.
    # Fermeture du dialog
    Click Element  css=.ui-dialog-titlebar-close
    # Vérification de l'erreur
    ${alert} =  Get Alert Message
    Should Be Equal As Strings  ${alert}  Tous les documents doivent être présents. Dans le cas contraire, rejeter la demande.
    # Rejet de la demande avec annulation
    Click Button    Rejeter la demande
    Choose Cancel On Next Confirmation
    ${message}=     Confirm Action
    Should Be Equal As Strings  ${message}  Êtes vous sur de vouloir rejeter la demande ?
    # Rejet de la demande
    Click Button    Rejeter la demande
    ${message}=     Confirm Action
    Should Be Equal As Strings  ${message}  Êtes vous sur de vouloir rejeter la demande ?
    # Vérification du rechargement de la page
    Wait Until Page Contains Element    css=#dossier_autorisation_type_detaille
    Element Should Not Be Visible    css=#demande_type
    # On sélectionne le type de dossier d'autorisation détaillé
    Select From List By Label  dossier_autorisation_type_detaille  Permis de construire comprenant ou non des démolitions
    # On sélectionne le type de demande
    Select From List By Label  demande_type  TESTDOC
    # Vérification du chargement du dialog
    Wait Until Keyword Succeeds    ${TIMEOUT}    ${RETRY_INTERVAL}    Element Should Contain    css=#ui-dialog-title-liste_doc    Liste des documents obligatoires
    # check Doc 1
    Select Checkbox    0
    # Validation du dialog
    Click Button    Valider
    # vérification de la fermeture du dialog
    Element Should Not Be Visible    css=#ui-dialog-title-liste_doc
    # Ajout d'un pétitionnaire
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Cole
    ...  particulier_prenom=Sarah
    ...  frequent=true
    Ajouter le demandeur  petitionnaire_principal    ${args_petitionnaire}
    # On valide
    Click On Submit Button
    # Vérification qu'il n'y a aucune erreur
    Page Should Not Contain Errors


TNR Récupération de l'édition et logo
    [Documentation]  L'objet de ce 'Test Case' est de vérifier que la bonne édition
    ...  et le bon logo sont récupérés dans le récépissé de la demande

    # En tant qu'admin
    Depuis la page d'accueil    admingen    admingen

    # On crée une nouvelle demande via le tableau de bord
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Fistaul
    ...  particulier_prenom=Sarah
    ...  om_collectivite=ALLAUCH

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=ALLAUCH
    # On crée une nouvelle demande via le tableau de bord
    ${di_libelle} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}

    # On ouvre le récépissé de la demande
    Click Element  css=#link_demande_recepisse
    # On ouvre le PDF
    Open PDF  form
    # On vérifie la localisation du terrain
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Commune : Allauch
    # On ferme le PDF
    Close PDF


TNR Récupération des paramètres de collectivité dans le récépissé de dépôt
    [Documentation]  L'objet de ce 'Test Case' est de vérifier que les paramètres
    ...    de la collectivité sont bien fusionné avec le récépissé de dépôt

    # En tant que guichet
    Depuis la page d'accueil    guichetsuivi    guichetsuivi
    # On crée une nouvelle demande via le tableau de bord
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Crosh
    ...  particulier_prenom=Sarah

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial

    # On crée une nouvelle demande via le tableau de bord
    ${di_libelle} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}

    # On ouvre le récépissé de la demande
    Click Element  css=#link_demande_recepisse
    # On ouvre le PDF
    Open PDF  form
    # On va sur la seconde page
    Next Page PDF
    # On vérifie la localisation du terrain
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain   Marseille , le${SPACE}${SPACE}${date_ddmmyyyy}
    # On ferme le PDF
    Close PDF


Ajout d'une demande avec création de répertoire de numérisation

    [Documentation]  Permet de vérifier la création du répertoire de numérisation du
    ...  dossier d'instruction, ainsi que sa date de modification avant et après qu'une
    ...  demande sur existant soit ajoutée au dossier.

    Depuis la page d'accueil  admin  admin
    #
    Activer l'option de numérisation

    Depuis la page d'accueil  instrpoly  instrpoly
    # On crée une nouvelle demande via le tableau de bord
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Fongemie
    ...  particulier_prenom=Christiane
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Demande d'autorisation de construire, d'aménager ou de modifier un ERP
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    # Vérification de la création du dossier et récupération du nom du répertoire contenant les pièces du dossier
    ${repertoire_numerisation_dossier} =  Vérifier création répertoire du dossier  ${di_libelle}
    # On récupère la date de dernière modification du répertoire
    ${create_time} =   Get Modified Time   ${EXECDIR}${/}..${/}var${/}digitalization${/}Todo${/}${repertoire_numerisation_dossier}
    # Ajout du sleep si non ca va trop vite et c'est la même heure entre create et modify
    Sleep  1
    &{args_demande} =  Create Dictionary
    ...  demande_type=Dépôt de pièces complémentaire
    ...  om_collectivite=MARSEILLE
    ...  dossier_instruction=${di_libelle}
    Ajouter la demande par WS  ${args_demande}
    # On vérifie la présence du lien
    Depuis le contexte du dossier d'instruction  ${di_libelle}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Text Should Be  dossier_libelle  ${di_libelle}
    # Les dates de création et de modification du répertoire ne doivent pas être égales
    ${modify_time} =   Get Modified Time   ${EXECDIR}${/}..${/}var${/}digitalization${/}Todo${/}${repertoire_numerisation_dossier}
    Should Not Be Equal  ${create_time}  ${modify_time}
    #
    Désactiver l'option de numérisation


Affichage réglementaire

    [Documentation]  Test de la fonctionnalité 'Affichage réglementaire'

    # On se connecte à l'application
    Depuis la page d'accueil  guichet  guichet
    # On clique sur le menu "Attestation"
    Go To Submenu In Menu  guichet_unique  affichage_reglementaire_attestation
    # On vérifie le nom de l'onglet
    First Tab Title Should Be  Imprimer L'attestation D'affichage Réglementaire
    # On écrit "PC0130551200002P0" dans le champ dossier
    Input Text  css=#dossier  PC0130551200002P0
    # On clique sur "Valider"
    Click On Submit Button
    # On vérifie que le texte est présent
    Error Message Should Contain  Ce dossier n'a jamais été affiché
    # # On clique sur le menu "Registre"
    Go To Submenu In Menu  guichet_unique  affichage_reglementaire_registre
    # On vérifie le nom de l'onglet
    First Tab Title Should Be  Imprimer Le Registre D'affichage Réglementaire
    # On clique sur "Valider"
    Click Element  css=#formulaire div.formControls input[type="submit"]
    # On clique sur "Oui" dans la fenêtre js qui apparaît
    Dismiss Alert
    # On ouvre le PDF
    Wait Until Keyword Succeeds  1 min  0.1 sec  Valid Message Should Be  Traitement terminé. Le registre a été téléchargé.
    # On ouvre le PDF
    Open PDF  form
    # On vérifie le titre du PDF
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Registre des dossiers en cours
    # On ferme le PDF
    Close PDF
    # On vérifie que le texte est présent
    Valid Message Should Contain  Traitement terminé. Le registre a été téléchargé.
    # On clique sur le menu "Attestation"
    Go To Submenu In Menu  guichet_unique  affichage_reglementaire_attestation
    # On vérifie le nom de l'onglet
    First Tab Title Should Be  Imprimer L'attestation D'affichage Réglementaire
    # On clique sur "Valider"
    Click On Submit Button
    # On vérifie que le texte est présent
    Error Message Should Contain  Veuiller saisir un N° de dossier.
    # On écrit "123" dans le champ dossier
    Input Text  css=#dossier  123
    # On clique sur "Valider"
    Click On Submit Button
    # On vérifie que le texte est présent
    Error Message Should Contain  Ce dossier n'existe pas.
    # On écrit "PC0130551200002P0" dans le champ dossier
    Input Text  css=#dossier  PC0130551200002P0
    # On clique sur "Valider"
    Click On Submit Button
    # On vérifie que le texte est présent
    Valid Message Should Contain  Cliquez sur le lien ci-dessous pour télécharger votre attestation d'affichage
    # On clique sur "Attestation d'affichage"
    Click Link  Attestation d'affichage
    # On ouvre le PDF
    Open PDF  file
    # On vérifie le titre du PDF
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ATTESTATION D'AFFICHAGE REGLEMENTAIRE
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  PC 013055 12 00002P0
    # On ferme le PDF
    Close PDF
    # On écrit "PC 013055 12 00002P0" dans le champ dossier avec des espaces
    Input Text  css=#dossier  PC 013055 12 00002P0
    # On clique sur "Valider"
    Click On Submit Button
    # On vérifie que le texte est présent
    Valid Message Should Contain  Cliquez sur le lien ci-dessous pour télécharger votre attestation d'affichage
    # On clique sur "Attestation d'affichage"
    Click Link  Attestation d'affichage
    # On ouvre le PDF
    Open PDF  file
    # On vérifie le titre du PDF
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ATTESTATION D'AFFICHAGE REGLEMENTAIRE
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  PC 013055 12 00002P0
    # On ferme le PDF
    Close PDF

    # On se connecte en tant qu'instructeur pour définaliser l'instruction
    # et vérifier le bon message lorsque le guichet tente de sortir l'attestation PDF
    Depuis la page d'accueil  instr  instr
    Depuis l'onglet instruction du dossier d'instruction  PC 013055 12 00002P0
    Click Link  affichage_obligatoire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Portlet Action Should Be In SubForm  instruction  definaliser
    Click On SubForm Portlet Action  instruction  definaliser
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be In Subform  La définalisation du document s'est effectuée avec succès.
    Depuis la page d'accueil  guichet  guichet
    Go To Submenu In Menu  guichet_unique  affichage_reglementaire_attestation
    Input Text  css=#dossier  PC 013055 12 00002P0
    Click On Submit Button
    Error Message Should Contain  L'attestation de ce dossier existe mais n'est pas finalisée.


Ajout d'une demande sans récépissé de dépôt

    [Documentation]  Au dépôt d'une demande, un lien permettant de télécharger
    ...  le récépissé est affiché dans le message de validation même si
    ...  l'instruction initiale n'a pas de lettre type (pas de récépissé).
    ...  L'action permettant de régénérer ce même document ne doit pas être
    ...  disponible.

    # On supprime la lettre type de l'événement de dépôt des PCI mais avant on
    # récupère la valeur de ce champ pour le repositionner à la fin de ce test
    Depuis la page d'accueil  admin  admin
    # On récupère la valeur de la lettre type depuis le formulaire de
    # modification
    Depuis le contexte de l'événement  Notification du delai legal maison individuelle
    Click On Form Portlet Action  evenement  modifier
    ${label_lettretype} =  Get Text  css=.form-content select#lettretype option:checked
    # On modifie la valeur de la lettre type depuis le même formulaire de
    # modification pour ne pas perdre de temps
    &{args_evenement} =  Create Dictionary
    ...  libelle=Notification du delai legal maison individuelle
    ...  lettretype=choisir Lettre type
    Saisir l'événement  ${args_evenement}
    Click On Submit Button
    Page Should Not Contain Errors
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # On ajoute une nouvelle demande
    Depuis la page d'accueil  guichetsuivi  guichetsuivi
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Bonenfant
    ...  particulier_prenom=Germain

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ${di_libelle} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}

    # On vérifie que dans le message de validation il n'est pas possible de
    # télécharger le récépissé (qui n'existe pas)
    Element Should Not Contain  css=div.message.ui-state-valid p span.text  Télécharger le récépissé de la demande

    # On clique sur le lien du message de validation pour accéder au DI
    Click Link  css=#link_demande_dossier_instruction

    # On vérifie que l'action de régénérer le récépissé n'est pas disponible
    Element Should Not Contain  css=#portlet-actions  Télécharger le récépissé de la demande

    # On repositionne la lettre type de l'événement de dépôt des PCI
    Depuis la page d'accueil  admin  admin
    #
    &{args_evenement} =  Create Dictionary
    ...  libelle=Notification du delai legal maison individuelle
    ...  lettretype=${label_lettretype}
    Modifier l'événement  ${args_evenement}


Affectation automatique

    [Documentation]  Ce test case a pour but de vérifier le fonctionnement de
    ...  l'affectation automatique des dossiers en fonction de leur type de dossier
    ...  d'autorisation détaillé et en fonction du code quartier des références
    ...  cadastrales.

    ## Cas d'utilisation n°1 : dossier affecté à l'instructeur agglo car aucun instructeur
    # mono ne correspond au type de dossier d'autorisation détaillé
    Depuis la page d'accueil  guichet  guichet
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Morneau
    ...  particulier_prenom=Gérard
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    ...  demande_type=Dépôt Initial
    ${di_libelle} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}

    Click Element  css=a#link_demande_recepisse
    Open PDF  form
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Dossier suivi par  Poly Com Marseille
    Close PDF
    Click Element  link_demande_dossier_instruction
    Wait Until Element Is Visible  instructeur
    Element Text Should Be  instructeur  Poly Com Marseille (J)

    ## Cas d'utilisation n°2 : dossier PCI avec code quartier 806 -> affectation à Louis Laurent
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Morneau
    ...  particulier_prenom=Gérard
    @{ref_cad} =  Create List  806  AB  25  A  30
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ${di_libelle} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}

    Click Element  css=a#link_demande_recepisse
    Open PDF  form
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Dossier suivi par  Louis Laurent
    Close PDF
    Click Element  link_demande_dossier_instruction
    Wait Until Element Is Visible  instructeur
    Element Text Should Be  instructeur  Louis Laurent (H)

    ## Cas d'utilisation n°3 : dossier PCI avec code quartier 806 -> affectation à Pierre Martin
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Morneau
    ...  particulier_prenom=Gérard
    @{ref_cad} =  Create List  801  AB  25  A  30
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ${di_libelle2} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}

    Click Element  css=a#link_demande_recepisse
    Open PDF  form
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Dossier suivi par  Pierre Martin
    Close PDF
    Click Element  link_demande_dossier_instruction
    Wait Until Element Is Visible  instructeur
    Element Text Should Be  instructeur  Pierre Martin (H)


Vérification des références cadastrales
    [Documentation]  Teste les messages d'erreur lors de la saisie des références
    ...  cadastrales dans le contexte d'une nouvelle demande et la présence
    ...  de ce champ pour une demande sur existant.


    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=particulier
    ...  particulier_nom=DURAND
    ...  particulier_prenom=MICKAEL

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial

    @{ref_cad} =  Create List  806  DC  ''

    Depuis la page d'accueil  guichet  guichet
    Depuis le contexte de nouvelle demande via l'URL
    Saisir la demande  ${args_demande}
    Ajouter le demandeur  petitionnaire_principal  ${args_petitionnaire}
    Saisir les références cadastrales  ${ref_cad}

    ${css_path_ref_cad_fields} =  Set Variable  .reference_cadastrale_custom_fields .reference_cadastrale_custom_field:nth-child

    # Pour les refcad 806 DC ''
    # Appel au trigger JS qui contrôle la valeur du champ de refcad
    Execute JavaScript  window.jQuery("${css_path_ref_cad_fields}(3)").trigger("change");
    ${alert} =  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Get Alert Message
    Should Contain  ${alert}  Vous ne devez saisir que des nombres entiers

    # Pour les refcad 806 1 22
    Input Text  css=${css_path_ref_cad_fields}(3)  1
    Input Text  css=${css_path_ref_cad_fields}(2)  22
    Execute JavaScript  window.jQuery("${css_path_ref_cad_fields}(2)").trigger("change");
    ${alert} =  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Get Alert Message
    Should Contain  ${alert}  Vous ne devez saisir que des lettres dans ce champs.

    # Validation de création de la demande
    Click Element  css=#formulaire div.formControls input[type="submit"]
    ${alert} =  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Get Alert Message
    Should Be Equal As Strings  ${alert}  Les références cadastrales saisies sont incorrectes. Veuillez les corriger.

    # Vérification de la présence de references cadastrales sur un dossier existant
    Depuis le contexte de demande sur existant via l'URL  ${libelle_di_cadastre}
    Select From List By Label  demande_type  Demande de modification
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=.reference_cadastrale_custom_fields


Vérification de la modification de la date de dossier
    [Documentation]  Il s'agit de tester le changement de date d'un dossier
    ...  il faut d'abord tester les erreur possible: le changemet d'année ou
    ...  le changement de date aprés l'ajout d'une instruction


    ${odl_date} =  Set Variable  25/12/1994
    ${new_date} =  Set Variable  27/11/2013
    ${newer_date} =  Set Variable  25/12/2013

    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=particulier
    ...  particulier_nom=VEILLEUX
    ...  particulier_prenom=MARCELLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=${new_date}


    Depuis la page d'accueil  guichet  guichet
    ${di} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}


    Depuis la page d'accueil  instr  instr
    Depuis le contexte du dossier d'instruction  ${di}
    Click On Form Portlet Action  dossier_instruction  modifier
    Input Text  date_depot  ${odl_date}
    Click On Submit Button
    Error Message Should Contain  L'année de la date de dépôt n'est pas modifiable

    Depuis le contexte du dossier d'instruction  ${di}
    Click On Form Portlet Action  dossier_instruction  modifier
    Input Text  date_depot  ${newer_date}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

    Depuis le contexte du dossier d'instruction  ${di}
    Element Should Contain  date_depot  ${newer_date}
    Depuis l'onglet instruction du dossier d'instruction  ${di}
    Element Should Contain  css=tbody .col-2  ${newer_date}

    Ajouter une instruction au DI et la finaliser  ${di}  Sursis a statuer
    Depuis le contexte du dossier d'instruction  ${di}
    Click On Form Portlet Action  dossier_instruction  modifier
    ${input_text} =  Get Text  date_depot
    Should Be Equal  ${EMPTY}  ${input_text}


Ajout d'une nouvelle demande utilisant le type de formulaire 'DPC'
    [Documentation]  Vérifie que l'ajout d'une nouvelle demande d'un type de
    ...  dossier utilisant l'affichage 'DPC' est correcte. Le dossier doit être
    ...  identique à un 'ADS' excepté pour les demandeurs qui sont le
    ...  pétitionnaire principales, les pétitionnaires secondaires, le
    ...  mandataire, le bailleur principale et les bailleurs secondaires.

    Depuis la page d'accueil  guichet  guichet
    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=particulier
    ...  particulier_nom=CinqMars
    ...  particulier_prenom=Manon
    ${bailleur_nom} =  Set Variable  Clavette
    ${bailleur_prenom} =  Set Variable  Roland
    &{args_bailleur} =  Create Dictionary
    ...  qualite=particulier
    ...  particulier_nom=${bailleur_nom}
    ...  particulier_prenom=${bailleur_prenom}
    &{args_autres_demandeurs} =  Create Dictionary
    ...  bailleur_principal=${args_bailleur}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Fonds de commerce
    ${di_fc} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}  ${args_autres_demandeurs}

    # On vérifie que sur le formulaire du dossier, les demandeurs soient
    # correctement affichés
    Depuis la page d'accueil  instr  instr
    # Utilisation du keyword utilisant la recherche pour vérifier la présence
    # du dossier d'instruction dans le listing
    Depuis le contexte du dossier d'instruction par recherche  ${di_fc}
    Open Fieldset  dossier_instruction  demandeur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=div#liste_demandeur  Bailleur principal
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=div#liste_demandeur  ${bailleur_nom}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=div#liste_demandeur  ${bailleur_prenom}
