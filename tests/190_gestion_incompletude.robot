*** Settings ***
Documentation  Test de la gestion d'incomplétude

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Workflow de l'incomplétude

    [Documentation]  L'objet de ce 'Test Case' est de vérifier le workflow entier d'une
    ...  incomplétude.
    ...  Les points vérifiés sont:
    ...    - Le contenu de l'édition pdf du récépissé de la demande
    ...    - Le fonctionnement des délais d'instruction lors de l'ajout de
    ...      plusieurs majorations
    ...    - La mise à jour de l'état d'un dossier lors d'une incomplétude
    ...    - Le fonctionnement de la date limite de retour RAR


    # dates de references
    ${date} =  Set Variable  01/01/2001
    ${date_limite} =  Set Variable  01/09/2001

    # On crée un nouveau dossier
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=DURAND
    ...  particulier_prenom=MICKAEL
    ...  particulier_date_naissance=03/01/1956
    ...  particulier_commune_naissance=LILLE
    ...  particulier_departement_naissance=NORD
    ...  numero=12
    ...  voie=RUE DE LA LOI
    ...  complement=APPT 12
    ...  localite=MARSEILLE
    ...  code_postal=13012
    ...  telephone_fixe=0404040404

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=${date}


    Depuis la page d'accueil  guichet  guichet
    ${di} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}

    # On vérifie le contenu du PDF
    Click On Link  link_demande_recepisse
    Open PDF  form
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  RECEPISSE DE DEPOT
    Page Should Contain  DURAND MICKAEL
    Close PDF

    # Vérification du fonctionnement des delais d'instruction
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI et la finaliser  ${di}  ABF recours contre avis  ${date}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  La finalisation du document s'est effectuée avec succès.
    Reload Page
    Element Text Should Be  css=#delai  5

    # Notification de pieces manquantes
    Ajouter une instruction au DI et la finaliser  ${di}  Notification de pieces manquante  ${date}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  La finalisation du document s'est effectuée avec succès.
    Click On Back Button In Subform
    Reload Page
    Element Should Contain  etat  dossier incomplet

    Depuis la page d'accueil  admingen  admingen
    Depuis l'instruction du dossier d'instruction  ${di}  ABF recours contre avis
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_rar  ${date}
    Click On Submit Button In Subform

    Depuis l'instruction du dossier d'instruction  ${di}  Notification de pieces manquante
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_retour_rar  ${date}
    Click On Submit Button In Subform

    # Dépôt de pièces complémentaires
    Depuis la page d'accueil  instr  instr
    Depuis l'onglet instruction du dossier d'instruction  ${di}
    Element Should Contain  css=#sousform-instruction .tab-tab  incomplétude après accusé de réception
    Element Should Contain  css=#sousform-instruction .tab-tab  majoration délai abf
    Click Element  action-soustab-instruction-corner-ajouter
    Saisir instruction  dépôt de pièces complémentaires  ${date}
    Click On Submit Button In Subform

    Click On Back Button In Subform
    Reload Page
    Page Should Not Contain  dossier incomplet
    Element Should Contain  delai  8
    Element Should Contain  date_complet  ${date}
    Element Should Contain  date_limite  ${date_limite}


Incomplétude avec date de retour RAR dépassée

    [Documentation]  On crée une incomplétude et on essaye de saisir une date de retour
    ...  RAR dépassée, en vérifiant qu'on obtient une erreur et que l'état du dossier
    ...  reste incomplet.

    # dates de references
    ${date_35d} =  Add Time To Date  ${date_ddmmyyyy}  35 days  %d/%m/%Y  False  %d/%m/%Y


    # On crée un nouveau dossier
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=DURAND
    ...  particulier_prenom=MICKAEL
    ...  particulier_date_naissance=03/01/1956
    ...  particulier_commune_naissance=LILLE
    ...  particulier_departement_naissance=NORD
    ...  numero=12
    ...  voie=RUE DE LA LOI
    ...  complement=APPT 12
    ...  localite=MARSEILLE
    ...  code_postal=13012
    ...  telephone_fixe=0404040404
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instr  instr
    # Notification de pieces manquantes
    Ajouter une instruction au DI et la finaliser  ${di}  Notification de pieces manquante
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  La finalisation du document s'est effectuée avec succès.
    Click On Back Button In Subform
    Click On Link  Notification de pieces manquante
    ${id_instruction_notif} =  Get Text  css=div#form-content span#instruction
    Reload Page
    Element Should Contain  etat  dossier incomplet

    # Verification du retour de l'AR incompletude donc de la presence d'erreur
    Depuis la page d'accueil  suivi  suivi
    ${code_barres} =  STR_PAD_LEFT  ${id_instruction_notif}  10  0
    ${code_barres} =  Catenate  11${code_barres}
    Go To Submenu In Menu  suivi  suivi_mise_a_jour_des_dates
    Select From List By Label  css=#type_mise_a_jour  date de retour de l'AR
    Input Text  date  ${date_35d}
    Input Text  code_barres  ${code_barres}
    Click Element  css=#formulaire div.formControls input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  evenement  Notification de pieces manquante
    Click Element  css=#formulaire div.formControls input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#formulaire .message .text  Problème de dates : contactez l\'instructeur du dossier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#formulaire .message .text  (date d\'événement <= date limite de notification au pétitionnaire)

    # Vérification de l'état du dossier
    Depuis la page d'accueil  instr  instr
    Depuis le contexte du dossier d'instruction  ${di}
    Element Should Not Be Visible  date_limite_incompletude
    Element Should Not Be Visible  delai_incompletude
    Element Should Contain  etat  dossier incomplet

