*** Settings ***
Documentation  Teste l'import spécifique de fichiers CSV

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Import avec succès de dossiers ADS 2007
    #
    Depuis la page d'accueil    admin    admin
    # On importe les dossiers
    Depuis l'import spécifique    ads2007
    Add File    fic1    import_specific_success_ads2007.csv
    Click On Submit Button In Import CSV
    # On vérifie le résultat
    Résultat de l'import doit contenir  4 ligne(s) dans le fichier dont :
    Résultat de l'import doit contenir  - 1 ligne(s) d'entête
    Résultat de l'import doit contenir  - 3 ligne(s) importée(s)

    Le dossier d'instruction doit exister  PD 044185 07 R0013P0
    Le dossier d'instruction doit exister  PA 044185 08 R0051
    Le dossier d'instruction doit exister  PA 044185 08 R0051M1

    # On désactive le suffixe P0 pour les permit de démolir initiaux puis on
    # réimporte les dossiers afin de vérifier que le PD a bien été mis à jour
    # et qu'un autre sans P0 n'a pas été créé
    Go To Tab  dossier_instruction_type
    Use Simple Search   type de dossier d'autorisation détaillé  PD (Permis de démolir)
    Click On Link  Initial
    Click On Form Portlet Action  dossier_instruction_type  modifier
    Unselect Checkbox  suffixe
    Click On Submit Button
    # On importe les dossiers
    Depuis l'import spécifique    ads2007
    Add File    fic1    import_specific_modif_ads2007.csv
    Click On Submit Button In Import CSV
    # On vérifie le résultat
    Résultat de l'import doit contenir  2 ligne(s) dans le fichier dont :
    Résultat de l'import doit contenir  - 1 ligne(s) d'entête
    Résultat de l'import doit contenir  - 1 ligne(s) importée(s)

    # On vérifie qu'un nouveau dossier n'a pas été créé
    Le dossier d'instruction ne doit pas exister  PD 044185 07 R0013

    # On vérifie que le bon dossier a été mis à jour
    Depuis le contexte du dossier d'instruction    PD 044185 07 R0013P0
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#date_depot  29/06/2008

    # On remet le sufixe au dossiers initiaux PD
    Go To Tab  dossier_instruction_type
    Use Simple Search   type de dossier d'autorisation détaillé  PD (Permis de démolir)
    Click On Link  Initial
    Click On Form Portlet Action  dossier_instruction_type  modifier
    Select Checkbox  suffixe
    Click On Submit Button

Rejet lors de l'import de dossiers ADS 2007
    #
    Depuis la page d'accueil    admin    admin
    # On importe l'établissement
    Depuis l'import spécifique    ads2007
    Add File    fic1    import_specific_fail_ads2007.csv
    Click On Submit Button In Import CSV
    # On vérifie le résultat
    Résultat de l'import doit contenir  3 ligne(s) dans le fichier dont :
    Résultat de l'import doit contenir  - 1 ligne(s) d'entête
    Résultat de l'import doit contenir  - 1 ligne(s) importée(s)
    Résultat de l'import doit contenir  - 1 ligne(s) rejetée(s)