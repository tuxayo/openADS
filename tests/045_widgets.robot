*** Settings ***
Documentation  Les widgets.

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Création du jeu de données

    [Documentation]  Constitue le jeu de données.

    Depuis la page d'accueil  admin  admin
    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Reault
    ...  particulier_prenom=Julienne
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Recours contentieux
    ...  demande_type=Dépôt Initial REC
    ...  autorisation_contestee=PC 013055 12 00001P0
    ...  om_collectivite=MARSEILLE
    ${args_peti} =  Create Dictionary

    ${di_rec} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}

    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Ayot
    ...  particulier_prenom=Alain
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Recours contentieux
    ...  demande_type=Dépôt Initial REC
    ...  autorisation_contestee=PC 013055 12 00001P0
    ...  om_collectivite=MARSEILLE
    ${di_rec_2} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}

    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Boncoeur
    ...  particulier_prenom=Amélie
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  petitionnaire=${args_petitionnaire}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Recours gracieux
    ...  demande_type=Dépôt Initial REG
    ...  autorisation_contestee=PC 013055 12 00001P0
    ...  om_collectivite=MARSEILLE

    ${di_reg} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}

    #
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Ferland
    ...  particulier_prenom=Honoré
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Routhier
    ...  particulier_prenom=Vick
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE

    ${di_inf} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}

    #
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Chnadonnet
    ...  particulier_prenom=Gaston
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Audet
    ...  particulier_prenom=Saber
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE

    ${di_inf_2} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}

    Set Suite Variable  ${di_rec}
    Set Suite Variable  ${di_rec_2}
    Set Suite Variable  ${di_reg}
    Set Suite Variable  ${di_inf}
    Set Suite Variable  ${di_inf_2}

Widget "Dossiers Limites"

    [Documentation]    L'objet de ce 'Test Case' est de vérifier le
    ...    fonctionnement du widget 'Dossiers Limites'
    ...    (widget_dossiers_limites)

    #
    # Cas d'utilisation n°1
    # Un paramètre permet de filtrer les dossiers qui apparaissent soit par :
    # - instructeur
    # - division
    # - aucun
    #

    #  ATTENTION : ce test case dépend des jeux de données init_data
    ${di_instr_1_division_1_commune_1} =  Set Variable  DP 013055 12 00001
    ${di_instr_2_division_1_commune_1} =  Set Variable  PC 013055 12 00001P0
    ${di_instr_3_division_2_commune_1} =  Set Variable  PD 013055 12 00001P0

    # Filtre sur l'instructeur
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  dossiers_limites
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=instructeur
    Click On Submit Button
    #
    Depuis la page d'accueil  instr  instr
    Element Should Not Contain  css=.widget_dossiers_limites  Vous n'avez pas de dossiers limites pour le moment.
    Element Should Contain  css=.widget_dossiers_limites  ${di_instr_1_division_1_commune_1}
    Element Should Not Contain  css=.widget_dossiers_limites  ${di_instr_2_division_1_commune_1}
    Element Should Not Contain  css=.widget_dossiers_limites  ${di_instr_3_division_2_commune_1}
    Click Element  css=.widget_dossiers_limites .widget-footer a
    Page Title Should Be  Instruction > Dossiers Limites
    Element Should Contain  css=#tab-dossiers_limites  ${di_instr_1_division_1_commune_1}
    Element Should Not Contain  css=#tab-dossiers_limites  ${di_instr_2_division_1_commune_1}
    Element Should Not Contain  css=#tab-dossiers_limites  ${di_instr_3_division_2_commune_1}

    # Filtre sur la division
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  dossiers_limites
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=division
    Click On Submit Button
    #
    Depuis la page d'accueil  instr  instr
    Element Should Not Contain  css=.widget_dossiers_limites  Vous n'avez pas de dossiers limites pour le moment.
    Element Should Contain  css=.widget_dossiers_limites  ${di_instr_1_division_1_commune_1}
    Element Should Contain  css=.widget_dossiers_limites  ${di_instr_2_division_1_commune_1}
    Element Should Not Contain  css=.widget_dossiers_limites  ${di_instr_3_division_2_commune_1}
    Click Element  css=.widget_dossiers_limites .widget-footer a
    Page Title Should Be  Instruction > Dossiers Limites
    Element Should Contain  css=#tab-dossiers_limites  ${di_instr_1_division_1_commune_1}
    Element Should Contain  css=#tab-dossiers_limites  ${di_instr_2_division_1_commune_1}
    Element Should Not Contain  css=#tab-dossiers_limites  ${di_instr_3_division_2_commune_1}

    #
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  dossiers_limites
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=aucun
    Click On Submit Button
    #
    Depuis la page d'accueil  instr  instr
    Element Should Not Contain  css=.widget_dossiers_limites  Vous n'avez pas de dossiers limites pour le moment.
    Element Should Contain  css=.widget_dossiers_limites  ${di_instr_1_division_1_commune_1}
    Element Should Contain  css=.widget_dossiers_limites  ${di_instr_2_division_1_commune_1}
    Element Should Contain  css=.widget_dossiers_limites  ${di_instr_3_division_2_commune_1}
    Click Element  css=.widget_dossiers_limites .widget-footer a
    Page Title Should Be  Instruction > Dossiers Limites
    Element Should Contain  css=#tab-dossiers_limites  ${di_instr_1_division_1_commune_1}
    Element Should Contain  css=#tab-dossiers_limites  ${di_instr_2_division_1_commune_1}
    Element Should Contain  css=#tab-dossiers_limites  ${di_instr_3_division_2_commune_1}

    #
    # Cas d'utilisation n°2
    # Un paramètre permet de filtrer sur les types de dossiers qui apparaissent
    #

    #  ATTENTION : ce test case dépend des jeux de données init_data
    ${di_type_dp} =  Set Variable  DP 013055 12 00001
    ${di_type_pc} =  Set Variable  PC 013055 12 00001P0

    #
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  dossiers_limites
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    codes_datd=PCI;PCA;PC\nfiltre=aucun
    Click On Submit Button
    #
    Depuis la page d'accueil  instr  instr
    Element Should Not Contain  css=.widget_dossiers_limites  Vous n'avez pas de dossiers limites pour le moment.
    Element Should Contain  css=.widget_dossiers_limites  ${di_type_pc}
    Element Should Not Contain  css=.widget_dossiers_limites  ${di_type_dp}
    Click Element  css=.widget_dossiers_limites .widget-footer a
    Page Title Should Be  Instruction > Dossiers Limites
    Element Should Contain  css=#tab-dossiers_limites  ${di_type_pc}
    Element Should Not Contain  css=#tab-dossiers_limites  ${di_type_dp}

    #
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  dossiers_limites
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    codes_datd=DP;DPS\nfiltre=aucun
    Click On Submit Button
    #
    Depuis la page d'accueil  instr  instr
    Element Should Not Contain  css=.widget_dossiers_limites  Vous n'avez pas de dossiers limites pour le moment.
    Element Should Contain  css=.widget_dossiers_limites  ${di_type_dp}
    Element Should Not Contain  css=.widget_dossiers_limites  ${di_type_pc}
    Click Element  css=.widget_dossiers_limites .widget-footer a
    Page Title Should Be  Instruction > Dossiers Limites
    Element Should Contain  css=#tab-dossiers_limites  ${di_type_dp}
    Element Should Not Contain  css=#tab-dossiers_limites  ${di_type_pc}

    #
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  dossiers_limites
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    codes_datd=ZZ\nfiltre=aucun
    Click On Submit Button
    #
    Depuis la page d'accueil  instr  instr
    Element Should Contain  css=.widget_dossiers_limites  Vous n'avez pas de dossiers limites pour le moment.
    Element Should Not Contain  css=.widget_dossiers_limites  ${di_type_dp}
    Element Should Not Contain  css=.widget_dossiers_limites  ${di_type_pc}
    Element Should Not Contain  css=.widget_dossiers_limites  Voir +

    #
    # Cas d'utilisation n°3
    # Vérifier le paramètre nombre de jours
    #

    #
    # Cas d'utilisation n°4
    # Vérfier les cas de date limite et de l'état du dossier
    # TEST provenant de testWidget.php (test_dossier_limite)
    # XXX les dossiers utilisés ici ne sont pas dans un état cohérent pour apparaître
    ${di_limite} =  Set Variable  PD 013055 12 00001P0
    ${di_limite_incomplet} =  Set Variable  AT 013055 13 00001P0

    # Filtre sur la division
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  dossiers_limites
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=aucun
    Click On Submit Button
    #
    Depuis la page d'accueil  divi  divi
    Element Should Not Contain  css=.widget_dossiers_limites  Vous n'avez pas de dossiers limites pour le moment.
    Element Should Contain  css=.widget_dossiers_limites  ${di_limite}
    Element Should Contain  css=.widget_dossiers_limites  ${di_limite_incomplet}
    Click Element  css=.widget_dossiers_limites .widget-footer a
    Page Title Should Be  Instruction > Dossiers Limites
    Element Should Contain  css=#tab-dossiers_limites  ${di_limite}
    Element Should Contain  css=#tab-dossiers_limites  ${di_limite_incomplet}


Widget "Infos Profil"
    [Documentation]  Ce widget affiche des informations sur l'utilisateur connecté.

    # En tant qu'utilisateur profil QUALIFICATEUR
    Depuis la page d'accueil  qualif  qualif
    # On vérifie que le profil affiché est le bon
    Element Should Contain  css=.profil-infos-profil span.value  QUALIFICATEUR
    # On vérifie que le nom de l'utilisateur est le bon
    Element Should Contain  css=.profil-infos-nom span.value  Qualificateur
    Page Should Not Contain Element  css=.profil-infos-instructeur_qualite span.value
    Element Should Contain  css=.widget_infos_profil .tab-tab  urbanisme
    Element Should Contain  css=.widget_infos_profil .tab-tab  Oui

    # En tant qu'utilisateur profil INSTRUCTEUR
    # On vérifie pour trois utilisateurs différents pour être sûr
    # que l'information de division est correcte
    # instr -> Division H
    Depuis la page d'accueil  instr  instr
    # On vérifie que le profil affiché est le bon
    Element Should Contain  css=.profil-infos-profil span.value  INSTRUCTEUR
    # On vérifie que le nom de l'utilisateur est le bon
    Element Should Contain  css=.profil-infos-nom span.value  Louis Laurent
    # On vérifie que le code de la division est le bon
    # instr1 -> Division H
    Element Should Contain  css=.profil-infos-division span.value  H
    Element Should Contain  css=.profil-infos-instructeur_qualite span.value  instructeur

    Depuis la page d'accueil  instr1  instr
    # On vérifie que le profil affiché est le bon
    Element Should Contain  css=.profil-infos-profil span.value  INSTRUCTEUR
    # On vérifie que le nom de l'utilisateur est le bon
    Element Should Contain  css=.profil-infos-nom span.value  Martine Nadeau
    # On vérifie que le code de la division est le bon
    Element Should Contain  css=.profil-infos-division span.value  H
    # instr2 -> Division J
    Depuis la page d'accueil  instr2  instr
    # On vérifie que le profil affiché est le bon
    Element Should Contain  css=.profil-infos-profil span.value  INSTRUCTEUR
    # On vérifie que le nom de l'utilisateur est le bon
    Element Should Contain  css=.profil-infos-nom span.value  Roland Richard
    # On vérifie que le code de la division est le bon
    Element Should Contain  css=.profil-infos-division span.value  J

    # Pour un utilisateur lié au groupe contentieux
    Depuis la page d'accueil  juriste  juriste
    # On vérifie que le nom de l'utilisateur est le bon
    Element Should Contain  css=.profil-infos-profil span.value  JURISTE
    Element Should Contain  css=.profil-infos-nom span.value  Juriste
    Element Should Contain  css=.profil-infos-instructeur_qualite span.value  juriste
    Element Should Contain  css=.widget_infos_profil .tab-tab  urbanisme
    Element Should Contain  css=.widget_infos_profil .tab-tab  Contentieux
    Element Should Contain  css=.widget_infos_profil .tab-tab  Oui
    Element Should Contain  css=.widget_infos_profil .tab-tab  Non


Widget "Redirection"
    [Documentation]  Ce widget redirige l'utilisateur vers les listing des
    ...  demandes d'avis en cours.

    # On se connecte avec l'utilisateur consu
    Depuis la page d'accueil  consu  consu
    # On vérifie que l'utilisateur est bien redirigé vers le listing souhaité
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Title Should Be  Demandes D'avis > En Cours


Widget "Recherche Dossier"

    [Documentation]    L'objet de ce 'Test Case' est de vérifier le
    ...    fonctionnement du widget 'Recherche Dossier'
    ...    (widget_recherche_dossier)

    # En tant qu'instructeur
    Depuis la page d'accueil  instr  instr

    #
    # Cas d'utilisation n°1
    #
    # Saisie d'un numéro de dossier complet (avec et sans espaces)
    #

    #
    Go to dashboard
    #
    Input Text    css=#dashboard div.widget_recherche_dossier input#dossier    PC 013055 12 00001P0
    #
    Click Element    css=#dashboard div.widget_recherche_dossier div.formControls input
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Instruction > Dossiers D'instruction > PC 013055 12 00001P0 DUPONT JACQUES
    #
    Page Should Not Contain Errors
    #
    Go to dashboard
    #
    Input Text    css=#dashboard div.widget_recherche_dossier input#dossier    PC0130551200001P0
    #
    Click Element    css=#dashboard div.widget_recherche_dossier div.formControls input
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Instruction > Dossiers D'instruction > PC 013055 12 00001P0 DUPONT JACQUES
    #
    Page Should Not Contain Errors


    #
    # Cas d'utilisation n°2
    #
    # Saisie d'une portion d'un numéro de dossier
    #

    # Cas 2a : un seul dossier
    Go to dashboard
    #
    Input Text    css=#dashboard div.widget_recherche_dossier input#dossier    PC*5120*1P0
    #
    Click Element    css=#dashboard div.widget_recherche_dossier div.formControls input
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Instruction > Dossiers D'instruction > PC 013055 12 00001P0 DUPONT JACQUES
    #
    Page Should Not Contain Errors
    #

    # Cas 2b : plusieurs dossiers
    Go to dashboard
    #
    Input Text    css=#dashboard div.widget_recherche_dossier input#dossier    013055
    #
    Click Element    css=#dashboard div.widget_recherche_dossier div.formControls input
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Instruction > Dossiers D'instruction
    #
    Page Should Not Contain Errors
    #
    Textfield Value Should Be    css=#advanced-form #dossier    *013055


    #
    # Cas d'utilisation n°3
    #
    # Saisie d'un numéro de dossier inexistant
    #

    #
    Go to dashboard
    #
    Input Text    css=#dashboard div.widget_recherche_dossier input#dossier    ZZZZZZZZZZZZ
    #
    Click Element    css=#dashboard div.widget_recherche_dossier div.formControls input
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Tableau De Bord
    #
    Page Should Not Contain Errors
    #
    Element Text Should Be    css=#dashboard div.widget_recherche_dossier div.message.ui-state-error p span.text    Aucun dossier trouvé.


    #
    # Cas d'utilisation n°4
    #
    # Aucune valeur saisie
    #

    #
    Go to dashboard
    #
    Click Element    css=#dashboard div.widget_recherche_dossier div.formControls input
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Tableau De Bord
    #
    Page Should Not Contain Errors
    #
    Element Text Should Be    css=#dashboard div.widget_recherche_dossier div.message.ui-state-error p span.text    Veuillez saisir un No de dossier.


    #
    # TNR Bug "Erreur de base de données" lors de saisie de caractères spéciaux
    #

    #
    Go to dashboard
    #
    Input Text    css=#dashboard div.widget_recherche_dossier input#dossier    ;"?#'
    #
    Click Element    css=#dashboard div.widget_recherche_dossier div.formControls input
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Tableau De Bord
    #
    Page Should Not Contain Errors


    #
    # Les DI contentieux ne doivent pas accessibles
    #
    Go to dashboard
    Input Text    css=#dashboard div.widget_recherche_dossier input#dossier    ${di_rec}
    Click Element    css=#dashboard div.widget_recherche_dossier div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Tableau De Bord
    Page Should Not Contain Errors
    Element Text Should Be    css=#dashboard div.widget_recherche_dossier div.message.ui-state-error p span.text    Aucun dossier trouvé.


Widget "Recherche Dossier par type"

    [Documentation]  L'objet de ce 'Test Case' est de vérifier le
    ...  fonctionnement du widget 'Recherche Dossier par type'
    ...  (widget_recherche_dossier_par_type)
    ...
    ...  Vérification des points suivants :
    ...  - un utilisateur mono ne doit pas pouvoir rechercher un DI d'une autre collectivité
    ...  - la recherche fonctionne avec des numéros de dossier avec et sans espaces
    ...  - selon le type de dossier, on est redirigés au bon endroit

    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Petrie
    ...  particulier_prenom=Christelle
    ...  om_collectivite=ALLAUCH
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=ALLAUCH
    ${di_allauch} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  assist  assist

    # Aucune valeur saisie
    Go to dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Selected List Label Should Be  css=select#type_dossier_recherche  ADS
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Tableau De Bord
    Page Should Not Contain Errors
    Element Text Should Be    css=#dashboard div.widget_recherche_dossier_par_type div.message.ui-state-error p span.text    Veuillez saisir un No de dossier.

    # Saisie d'un numéro de dossier inexistant
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    ZZZZZZZZZZZZ
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Tableau De Bord
    Page Should Not Contain Errors
    Element Text Should Be    css=#dashboard div.widget_recherche_dossier_par_type div.message.ui-state-error p span.text    Aucun dossier trouvé.

    # Contrôle de la saisie de caractères spéciaux
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    ;"?#'';'
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Tableau De Bord
    Page Should Not Contain Errors

    # L'utilisateur étant mono, le DI d'une autre collectivité ne doit pas être trouvé
    Go to dashboard
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    ${di_allauch}
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}      Element Text Should Be    css=#dashboard div.widget_recherche_dossier_par_type div.message.ui-state-error p span.text    Aucun dossier trouvé.
    Page Should Not Contain Errors

    # Recherche de dossiers ADS existants avec et sans espaces dans le numéro
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    PC 013055 12 00001P0
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Instruction > Dossiers D'instruction > PC 013055 12 00001P0 DUPONT JACQUES
    Page Should Not Contain Errors

    Go to dashboard
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    PC0130551200001P0
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Instruction > Dossiers D'instruction > PC 013055 12 00001P0 DUPONT JACQUES
    Page Should Not Contain Errors

    # Saisie d'une portion d'un numéro de dossier, avec un seul dossier en résultat
    Go to dashboard
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    PC*5120*1P0
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Instruction > Dossiers D'instruction > PC 013055 12 00001P0 DUPONT JACQUES
    Page Should Not Contain Errors

    # Saisie d'une portion d'un numéro de dossier, avec plusieurs dossiers en résultat
    Go to dashboard
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    013055
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Instruction > Dossiers D'instruction
    Page Should Not Contain Errors
    Textfield Value Should Be    css=#advanced-form #dossier    *013055

    # Passage de l'utilisateur  juriste sur la collectivité agglo pour tester le filtre sur
    # la collectivité
    Depuis la page d'accueil  admin  admin
    Modifier l'utilisateur  juriste  support@atreal.fr  juriste  juriste  JURISTE  agglo

    # Un utilisateur multi doit pouvoir rechercher des dossiers de toutes les collectivités
    Depuis la page d'accueil  juriste  juriste
    Go to dashboard
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    ${di_allauch}
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Instruction > Dossiers D'instruction > ${di_allauch} PETRIE CHRISTELLE
    Page Should Not Contain Errors

    # Modification du type de dossier sélectionné par défaut
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  recherche_dossier_par_type
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    type_defaut=RE*
    Click On Submit Button
    Modifier l'utilisateur  juriste  support@atreal.fr  juriste  juriste  JURISTE  MARSEILLE

    Depuis la page d'accueil  juriste  juriste
    # Recherche d'un dossier IN
    Go to dashboard
    Selected List Label Should Be  css=select#type_dossier_recherche  RE*
    Select From List By Label  css=select#type_dossier_recherche  IN
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    ${di_inf}
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Contentieux > Infraction > ${di_inf}
    Page Should Not Contain Errors

    # Recherche de dossiers IN
    Go to dashboard
    Select From List By Label  css=select#type_dossier_recherche  IN
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    IN
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Contentieux > Infractions
    Page Should Not Contain Errors

    # Recherche d'un dossier RE
    Go to dashboard
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    ${di_rec}
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Contentieux > Recours > ${di_rec}
    Page Should Not Contain Errors

    # Recherche de dossiers RE
    Go to dashboard
    Input Text    css=#dashboard div.widget_recherche_dossier_par_type input#dossier    RE
    Click Element    css=#dashboard div.widget_recherche_dossier_par_type div.formControls input
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page title should be    Contentieux > Recours
    Page Should Not Contain Errors


Widget "Dossiers événement incomplet ou majoration sans RAR"

    [Documentation]  L'objet de ce 'Test Case' est de vérifier le
    ...    fonctionnement du widget 'Dossiers événement incomplet ou majoration sans RAR'
    ...    (dossiers_evenement_incomplet_majoration)

    ##
    ## Constitution du jeu de données
    ##
    ## On crée deux nouvelles collectivités pour être sûr du nombre
    ## de retours de messages à vérifier dans les widgets et tableaux
    ##
    #
    ${collectivite_a} =  Set Variable  DAKAR
    ${collectivite_b} =  Set Variable  SINESALOUM
    #
    ${utilisateur_nom_01} =  Set Variable  Nicole Leduc
    ${utilisateur_login_01} =  Set Variable  nleduc
    ${utilisateur_nom_02} =  Set Variable  Julie Giguère
    ${utilisateur_login_02} =  Set Variable  jguiguere
    ${utilisateur_nom_03} =  Set Variable  Arno Perreault
    ${utilisateur_login_03} =  Set Variable  aperreault
    ${utilisateur_nom_04} =  Set Variable  Albertine Echeverri
    ${utilisateur_login_04} =  Set Variable  aecheverri
    #
    Depuis la page d'accueil  admin  admin
    #
    Ajouter la collectivité depuis le menu  ${collectivite_a}  mono
    Ajouter la collectivité depuis le menu  ${collectivite_b}  mono
    #
    Ajouter l'utilisateur  ${utilisateur_nom_01}  nospam@openmairie.org  ${utilisateur_login_01}  ${utilisateur_login_01}  INSTRUCTEUR  ${collectivite_a}
    Ajouter l'utilisateur  ${utilisateur_nom_02}  nospam@openmairie.org  ${utilisateur_login_02}  ${utilisateur_login_02}  INSTRUCTEUR  ${collectivite_a}
    Ajouter l'utilisateur  ${utilisateur_nom_03}  nospam@openmairie.org  ${utilisateur_login_03}  ${utilisateur_login_03}  INSTRUCTEUR  ${collectivite_a}
    Ajouter l'utilisateur  ${utilisateur_nom_04}  nospam@openmairie.org  ${utilisateur_login_04}  ${utilisateur_login_04}  INSTRUCTEUR  ${collectivite_b}
    #
    Ajouter la direction depuis le menu  D  Direction D  null  Chef D  null  null  ${collectivite_a}
    Ajouter la direction depuis le menu  S  Direction S  null  Chef S  null  null  ${collectivite_b}
    #
    Ajouter la division depuis le menu  D  subdivision D  null  Chef D  null  null  Direction D
    Ajouter la division depuis le menu  DD  subdivision DD  null  Chef D  null  null  Direction D
    Ajouter la division depuis le menu  S  subdivision S  null  Chef S  null  null  Direction S
    #
    #
    Ajouter l'instructeur depuis le menu  ${utilisateur_nom_01}  subdivision D  instructeur  ${utilisateur_nom_01}
    Ajouter l'instructeur depuis le menu  ${utilisateur_nom_02}  subdivision DD  instructeur  ${utilisateur_nom_02}
    Ajouter l'instructeur depuis le menu  ${utilisateur_nom_03}  subdivision D  instructeur  ${utilisateur_nom_03}
    Ajouter l'instructeur depuis le menu  ${utilisateur_nom_04}  subdivision S  instructeur  ${utilisateur_nom_04}
    #
    &{args_affectation} =  Create Dictionary
    ...  instructeur=${utilisateur_nom_01} (D)
    ...  om_collectivite=${collectivite_a}
    Ajouter l'affectation depuis le menu  ${args_affectation}
    &{args_affectation} =  Create Dictionary
    ...  instructeur=${utilisateur_nom_02} (DD)
    ...  om_collectivite=${collectivite_a}
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    Ajouter l'affectation depuis le menu  ${args_affectation}
    &{args_affectation} =  Create Dictionary
    ...  instructeur=${utilisateur_nom_03} (D)
    ...  om_collectivite=${collectivite_a}
    ...  dossier_autorisation_type_detaille=Permis de démolir
    Ajouter l'affectation depuis le menu  ${args_affectation}
    &{args_affectation} =  Create Dictionary
    ...  instructeur=${utilisateur_nom_04} (S)
    ...  om_collectivite=${collectivite_b}
    Ajouter l'affectation depuis le menu  ${args_affectation}

    # DI n°1 : Permis de démolir dans Collectivité A (niveau mono)
    # => Affecté à l'instructeur '${utilisateur_nom_03}' (${utilisateur_login_03})
    # => Division 'D'
    #
    &{args_petitionnaire_01} =  Create Dictionary
    ...  particulier_nom=Chandonnet
    ...  particulier_prenom=Leone
    ...  om_collectivite=${collectivite_a}
    #
    &{args_demande_01} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de démolir
    ...  demande_type=Dépôt Initial
    ...  date_demande=${date_ddmmyyyy}
    ...  om_collectivite=${collectivite_a}
    #
    ${di_01} =  Ajouter la demande par WS  ${args_demande_01}  ${args_petitionnaire_01}

    # DI n°2 : Permis de construire pour une maison individuelle et / ou ses annexes dans Collectivité A (niveau mono)
    # => Affecté à l'instructeur '${utilisateur_nom_01}' (${utilisateur_login_01})
    # => Division 'D'
    #
    &{args_petitionnaire_02} =  Create Dictionary
    ...  particulier_nom=Joly
    ...  particulier_prenom=Frédérique
    ...  om_collectivite=${collectivite_a}
    #
    &{args_demande_02} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=${date_ddmmyyyy}
    ...  om_collectivite=${collectivite_a}
    #
    ${di_02} =  Ajouter la demande par WS  ${args_demande_02}  ${args_petitionnaire_02}

    # DI n°3 : Permis de construire comprenant ou non des démolitions dans Collectivité A (niveau mono)
    # => Affecté à l'instructeur '${utilisateur_nom_02}' (${utilisateur_login_02})
    # => Division 'DD'
    #
    &{args_petitionnaire_03} =  Create Dictionary
    ...  particulier_nom=Boucher
    ...  particulier_prenom=Bernadette
    ...  om_collectivite=${collectivite_a}
    #
    &{args_demande_03} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    ...  demande_type=Dépôt Initial
    ...  date_demande=${date_ddmmyyyy}
    ...  om_collectivite=${collectivite_a}
    #
    ${di_03} =  Ajouter la demande par WS  ${args_demande_03}  ${args_petitionnaire_03}

    # DI n°4 : Permis de construire pour une maison individuelle et / ou ses annexes dans Collectivité B (niveau mono)
    # => Affecté à l'instructeur '${utilisateur_nom_04}' (${utilisateur_login_04})
    # => Division 'S'
    #
    &{args_petitionnaire_04} =  Create Dictionary
    ...  particulier_nom=BOULAGE
    ...  particulier_prenom=Damien
    ...  om_collectivite=${collectivite_b}
    #
    &{args_demande_04} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=${date_ddmmyyyy}
    ...  om_collectivite=${collectivite_b}
    #
    ${di_04} =  Ajouter la demande par WS  ${args_demande_04}  ${args_petitionnaire_04}

    # On applique l'événement "majoration + DPC hors SS" à chaque dossier et on met une
    # date d'envoi RAR à l'événement pour que les dossiers soient affichés dans le widget
    Ajouter une instruction au DI et la finaliser  ${di_01}  majoration + DPC hors SS  ${date_ddmmyyyy}
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_envoi_rar  ${date_ddmmyyyy}
    Click On Submit Button In Subform

    Ajouter une instruction au DI et la finaliser  ${di_02}  majoration + DPC hors SS  ${date_ddmmyyyy}
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_envoi_rar  ${date_ddmmyyyy}
    Click On Submit Button In Subform

    Ajouter une instruction au DI et la finaliser  ${di_03}  majoration + DPC hors SS  ${date_ddmmyyyy}
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_envoi_rar  ${date_ddmmyyyy}
    Click On Submit Button In Subform

    Ajouter une instruction au DI et la finaliser  ${di_04}  majoration + DPC hors SS  ${date_ddmmyyyy}
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_envoi_rar  ${date_ddmmyyyy}
    Click On Submit Button In Subform

    #
    # Cas d'utilisation n°1
    # Un paramètre permet de filtrer les dossiers qui apparaissent soit par :
    # - instructeur
    # - division
    # - aucun (collectivite)
    #
    # Vérification du :
    # - fonctionnement des filtres
    # - de la redirection vers le dossier
    # - des dossiers affichés dans le widget ET la liste "voir plus"

    #
    ${widget_id} =  Set Variable  widget_10

    ## Vérification du filtre par défaut (instructeur)
    # On se connecte en tant que "${utilisateur_login_01}" (Profil 'INSTRUCTEUR')
    Depuis la page d'accueil  ${utilisateur_login_01}  ${utilisateur_login_01}
    Element Should Contain  css=#${widget_id}  ${di_02}
    Element Should Not Contain  css=#${widget_id}  ${di_01}
    Element Should Not Contain  css=#${widget_id}  ${di_03}
    Element Should Not Contain  css=#${widget_id}  ${di_04}
    Click Link  ${di_02}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Title Should Be  Instruction > Dossiers D'instruction > ${di_02} JOLY FRÉDÉRIQUE

    Depuis la page d'accueil  ${utilisateur_login_01}  ${utilisateur_login_01}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#${widget_id} .widget-footer a
    Page Title Should Be  Instruction > Dossiers Événement Incomplet Ou Majoration
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_02}
    Element Should Not Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_01}
    Element Should Not Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_03}
    Element Should Not Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_04}
    Click Link  ${di_02}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Title Should Be  Instruction > Dossiers D'instruction > ${di_02} JOLY FRÉDÉRIQUE

    ## Vérification du filtre instructeur
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  dossiers_evenement_incomplet_majoration
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=instructeur
    Click On Submit Button

    # L'instructeur doit seulement voir son dossier
    Depuis la page d'accueil  ${utilisateur_login_04}  ${utilisateur_login_04}
    Element Should Contain  css=#${widget_id}  ${di_04}
    Element Should Not Contain  css=#${widget_id}  ${di_01}
    Element Should Not Contain  css=#${widget_id}  ${di_02}
    Element Should Not Contain  css=#${widget_id}  ${di_03}

    Click Element  css=#${widget_id} .widget-footer a
    Page Title Should Be  Instruction > Dossiers Événement Incomplet Ou Majoration
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_04}
    Element Should Not Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_01}
    Element Should Not Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_02}
    Element Should Not Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_03}

    # Filtre sur la division
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  dossiers_evenement_incomplet_majoration
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=division
    Click On Submit Button

    # On doit avoir 2 dossiers de la division
    Depuis la page d'accueil  ${utilisateur_login_01}  ${utilisateur_login_01}
    Element Should Contain  css=#${widget_id}  ${di_01}
    Element Should Contain  css=#${widget_id}  ${di_02}
    Element Should Not Contain  css=#${widget_id}  ${di_04}
    Element Should Not Contain  css=#${widget_id}  ${di_03}

    Click Element  css=#${widget_id} .widget-footer a
    Page Title Should Be  Instruction > Dossiers Événement Incomplet Ou Majoration
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_02}
    Element Should Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_01}
    Element Should Not Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_04}
    Element Should Not Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_03}

    # Filtre "aucun" donc sur la collectivité
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  dossiers_evenement_incomplet_majoration
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=aucun
    Click On Submit Button

    # On doit avoir les 3 dossiers de la collectivité
    Depuis la page d'accueil  ${utilisateur_login_03}  ${utilisateur_login_03}
    Element Should Contain  css=#${widget_id}  ${di_01}
    Element Should Contain  css=#${widget_id}  ${di_02}
    Element Should Contain  css=#${widget_id}  ${di_03}
    Element Should Not Contain  css=#${widget_id}  ${di_04}
    Click Element  css=#${widget_id} .widget-footer a
    Page Title Should Be  Instruction > Dossiers Événement Incomplet Ou Majoration
    Element Should Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_01}
    Element Should Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_02}
    Element Should Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_03}
    Element Should Not Contain  css=#tab-dossiers_evenement_incomplet_majoration  ${di_04}

    # Suppression des affectations automatique
    Depuis la page d'accueil  admin  admin
    Supprimer l'affectation depuis le menu  ${utilisateur_nom_01} (D)
    Supprimer l'affectation depuis le menu  ${utilisateur_nom_02} (DD)
    Supprimer l'affectation depuis le menu  ${utilisateur_nom_03} (D)
    Supprimer l'affectation depuis le menu  ${utilisateur_nom_04} (S)


Widget "Alerte parquet"

    [Documentation]  Permet de vérifier le fonctionnement du widget 'Alerte
    ...  parquet'. Ce widget doit afficher les 5 infractions les plus anciennes
    ...  pour lesquelles la date de réception est dépassée depuis plus de 9 mois
    ...  et pour lesquelles la date de transmission au parquet est nulle.

    # On ajoute une infraction dont la date de réception est dépassée de 10 mois
    ${date_di_inf_db} =  Subtract Time From Date  ${DATE_FORMAT_YYYY-MM-DD}  300 days  result_format=%Y-%m-%d
    ${date_di_inf_form} =  Convert Date  ${date_di_inf_db}  result_format=%d/%m/%Y
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Charrette
    ...  particulier_prenom=Ophelia
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Moreau
    ...  particulier_prenom=Marcel
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  date_demande=${date_di_inf_form}
    ...  om_collectivite=MARSEILLE

    ${di_inf} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}

    # On vérifie que l'infraction est affichée dans le widget
    Depuis la page d'accueil  tech  tech
    Element Should Contain  css=.widget_dossier_contentieux_alerte_parquet  ${di_inf}

    # On saisit la date de transmission au parquet
    Depuis la page d'accueil  juriste  juriste
    Ajouter une instruction au DI  ${di_inf}  Transmission au Parquet  null  infraction

    # On vérifie que l'infraction n'est plus affichée dans le widget
    Depuis la page d'accueil  tech  tech
    Element Should Not Contain  css=.widget_dossier_contentieux_alerte_parquet  ${di_inf}


Widget "Alerte visite"

    [Documentation]  Permet de vérifier le fonctionnement du widget 'Alerte
    ...  visite'. Ce widget doit afficher les 5 infractions les plus anciennes
    ...  pour lesquelles la date de réception est dépassée depuis plus de 3 mois
    ...  et pour lesquelles la date de première visite est nulle.

    # On ajoute une infraction dont la date de réception est dépassée de 4 mois
    ${date_di_inf_db} =  Subtract Time From Date  ${DATE_FORMAT_YYYY-MM-DD}  120 days  result_format=%Y-%m-%d
    ${date_di_inf_form} =  Convert Date  ${date_di_inf_db}  result_format=%d/%m/%Y
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Raymond
    ...  particulier_prenom=Bertrand
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Bonsaint
    ...  particulier_prenom=Philippe
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  date_demande=${date_di_inf_form}
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ${di_inf} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}

    # On vérifie que l'infraction est affichée dans le widget
    Depuis la page d'accueil  tech  tech
    Element Should Contain  css=.widget_dossier_contentieux_alerte_visite  ${di_inf}

    # On saisit la date de première visite
    Depuis la page d'accueil  juriste  juriste
    Ajouter une instruction au DI  ${di_inf}  Première visite  null  infraction

    # On vérifie que l'infraction n'est plus affichée dans le widget
    Depuis la page d'accueil  tech  tech
    Element Should Not Contain  css=.widget_dossier_contentieux_alerte_visite  ${di_inf}


Widget "Les infractions non affectées"

    [Documentation]  Permet de vérifier le fonctionnement du widget 'Les
    ...  infractions non affectées'. Ce widget doit afficher les 5 infractions
    ...  les plus anciennes pour lesquelles il n'y a pas de technicien affecté.

    # On ajoute une infraction
    Depuis la page d'accueil  assist  assist
    #
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Flamand
    ...  particulier_prenom=Benjamin
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Pouliotte
    ...  particulier_prenom=Clementine
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ${di_inf} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}
    # On vérifie que l'infraction est affichée dans le widget
    Go To Dashboard
    Element Should Not Contain  css=.widget_dossier_contentieux_inaffectes  ${di_inf}

    # On supprime l'affectation automatique du technicien sur les infractions
    Depuis la page d'accueil  admin  admin
    Supprimer l'affectation depuis le menu  null  Infraction

    # On ajoute une infraction
    Depuis la page d'accueil  assist  assist
    #
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Lagueux
    ...  particulier_prenom=Anne
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Hachée
    ...  particulier_prenom=Diane
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ${args_peti} =  Create Dictionary
    ${di_inf} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}

    # On vérifie que l'infraction est affichée dans le widget
    Go To Dashboard
    Element Should Contain  css=.widget_dossier_contentieux_inaffectes  ${di_inf}

    # On ajoute l'affectation automatique du technicien
    Depuis la page d'accueil  admin  admin
    &{args_affectation} =  Create Dictionary
    ...  instructeur=Juriste (H)
    ...  instructeur_2=Technicien (H)
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Infraction
    Ajouter l'affectation depuis le menu  ${args_affectation}


Widget "Mes clôtures"

    [Documentation]  Permet de vérifier le fonctionnement du widget 'Mes
    ...  clôtures'. Ce widget doit afficher les 5 recours les plus proches
    ...  pour lesquels une date de clôture d'instruction existe, est comprise
    ...  entre le jour courant et un mois dans le futur et pour lesquels
    ...  l'utilisateur connecté est positionné en juriste.

    # On ajoute une autorisation à contester
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Charlebois
    ...  particulier_prenom=Agate
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_conteste} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    # On ajoute un recours
    ${date_di_re_db} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  10 days  result_format=%Y-%m-%d
    ${date_di_re_form} =  Convert Date  ${date_di_re_db}  result_format=%d/%m/%Y
    &{args_requerant} =  Create Dictionary
    ...  particulier_nom=Henrichon
    ...  particulier_prenom=Aurore
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  requerant_principal=${args_requerant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Recours gracieux
    ...  demande_type=Dépôt Initial REG
    ...  autorisation_contestee=${di_conteste}
    ...  om_collectivite=MARSEILLE
    ${di_re} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}

    # On vérifie que le recours n'est pas affiché dans le widget
    Depuis la page d'accueil  juriste  juriste
    Element Should Not Contain  css=.widget_dossier_contentieux_clotures  ${di_re}

    # On saisit la date de clôture du recours
    Ajouter une instruction au DI  ${di_re}  Clôture de l'instruction  ${date_di_re_form}  recours

    # On vérifie que le recours est affiché dans le widget
    Go To Dashboard
    Element Should Contain  css=.widget_dossier_contentieux_clotures  ${di_re}


Widget "Les audiences"

    [Documentation]  Permet de vérifier le fonctionnement du widget 'Les
    ...  audiences'. Ce widget doit afficher les 5 infractions dont la date
    ...  d'audience est la plus proche, pour lesquelles la date d'audience est
    ...  comprise entre le jour courant et un mois dans le futur.

    # On ajoute une infraction
    ${date_di_inf_db} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  10 days  result_format=%Y-%m-%d
    ${date_di_inf_form} =  Convert Date  ${date_di_inf_db}  result_format=%d/%m/%Y
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Courtois
    ...  particulier_prenom=Christine
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Blais
    ...  particulier_prenom=Eugenia
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  om_collectivite=MARSEILLE
    ...  demande_type=Dépôt Initial IN
    ${di_inf} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}

    # On vérifie que l'infraction n'est pas affichée dans le widget
    Depuis la page d'accueil  juriste  juriste
    Element Should Not Contain  css=.widget_dossier_contentieux_audience  ${di_inf}

    # On saisit la date d'audience dans les données techniques
    &{donnees_techniques_values} =  Create Dictionary
    ...  ctx_date_audience=${date_di_inf_form}
    Saisir les données techniques du dossier infraction  ${di_inf}  ${donnees_techniques_values}

    # On vérifie que l'infraction est affichée dans le widget
    Click Element  css=.ui-icon-closethick
    Go To Dashboard
    Element Should Contain  css=.widget_dossier_contentieux_audience  ${di_inf}


Widget "Mes AIT" et "Les AIT"

    [Documentation]  Permet de vérifier le fonctionnement du widget 'Mes AIT' et
    ...  du widget "Les AIT". Ces widgets doivent afficher les 5 infractions les
    ...  plus récentes pour lesquelles il y a un AIT signé, dans le cas du
    ...  widget 'Mes AIT', cela ne concerne que le juriste connecté.

    ##
    ## Widget 'Mes AIT'
    ##

    # On ajoute une infraction
    Depuis la page d'accueil  assist  assist
    #
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Champagne
    ...  particulier_prenom=Felicienne
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Blais
    ...  particulier_prenom=Eugenia
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  om_collectivite=MARSEILLE
    ...  demande_type=Dépôt Initial IN
    ${di_inf} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}

    # On vérifie que l'infraction n'est pas affichée dans le widget
    Go To Dashboard
    Element Should Not Contain  css=.widget_dossier_contentieux_ait  ${di_inf}
    Depuis la page d'accueil  juriste  juriste
    Element Should Not Contain  css=.widget_dossier_contentieux_ait  ${di_inf}

    # On saisit la date d'audience d'ait et on renseigne sa date de retour
    # signature
    Ajouter une instruction au DI et la finaliser  ${di_inf}  Arrêté interruptif des travaux  null  infraction
    &{args_instruction} =  Create Dictionary
    ...  date_retour_signature=${DATE_FORMAT_DD/MM/YYYY}
    Modifier le suivi des dates  ${di_inf}  Arrêté interruptif des travaux  ${args_instruction}  infraction

    # On vérifie que l'infraction est affichée dans le widget
    Go To Dashboard
    Element Should Contain  css=.widget_dossier_contentieux_ait  ${di_inf}
    Depuis la page d'accueil  assist  assist
    Element Should Contain  css=.widget_dossier_contentieux_ait  ${di_inf}

    ##
    ## Widget 'Les AIT'
    ##

    # On supprime l'affectation automatique du technicien sur les infractions
    Depuis la page d'accueil  admin  admin
    Supprimer l'affectation depuis le menu  null  Infraction

    # On ajoute une infraction non affectée
    Depuis la page d'accueil  assist  assist
    #
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Talon
    ...  particulier_prenom=Petrie
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Baril
    ...  particulier_prenom=Martin
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ${args_peti} =  Create Dictionary
    ${di_inf_2} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}

    # On vérifie que l'infraction n'est pas affichée dans le widget
    Go To Dashboard
    Element Should Not Contain  css=.widget_dossier_contentieux_ait  ${di_inf_2}
    Depuis la page d'accueil  juriste  juriste
    Element Should Not Contain  css=.widget_dossier_contentieux_ait  ${di_inf_2}

    # On saisit la date d'ait et on renseigne sa date de retour signature
    Depuis la page d'accueil  admin  admin
    Ajouter une instruction au DI et la finaliser  ${di_inf_2}  Arrêté interruptif des travaux  null  infraction
    &{args_instruction} =  Create Dictionary
    ...  date_retour_signature=${DATE_FORMAT_DD/MM/YYYY}
    Modifier le suivi des dates  ${di_inf_2}  Arrêté interruptif des travaux  ${args_instruction}  infraction

    # On vérifie que l'infraction est affichée dans le widget
    Depuis la page d'accueil  assist  assist
    Element Should Contain  css=.widget_dossier_contentieux_ait  ${di_inf_2}
    Depuis la page d'accueil  juriste  juriste
    Element Should Not Contain  css=.widget_dossier_contentieux_ait  ${di_inf_2}

    # On ajoute l'affectation automatique du technicien
    Depuis la page d'accueil  admin  admin
    &{args_affectation} =  Create Dictionary
    ...  instructeur=Juriste (H)
    ...  instructeur_2=Technicien (H)
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Infraction
    Ajouter l'affectation depuis le menu  ${args_affectation}


Widget "Mes contradictoires" et "Les contradictoires"

    [Documentation]  Permet de vérifier le fonctionnement du widget 'Mes
    ...  contradictoires' et du widget "Les contradictoires". Ces widgets
    ...  doivent afficher les 5 infractions les plus anciennes pour lesquelles
    ...  la date de contradictoire est supérieure ou égale à la date du jour + 3
    ...  semaines OU la date de retour du contradictoire est vide, pour
    ...  lesquelles il n'y a pas d'événements de type 'Annlation de
    ...  contradictoire' et pour lesquelles il n'y a pas d'AIT créé. Dans le cas
    ...  du widget 'Mes contradictoires', cela ne concerne que le juriste
    ...  connecté.

    ##
    ## Cas n°1 : infraction dont la date de contradictoire est supérieure ou
    ## égale à la date du jour + 3 semaines, la date retour de contradictoire
    ## est saisie, sans événements de type 'Annlation de contradictoire' et sans
    ## AIT
    ##

    # On ajoute une infraction
    Depuis la page d'accueil  assist  assist
    #
    ${date_di_inf_db} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  28 days  result_format=%Y-%m-%d
    ${date_di_inf_form} =  Convert Date  ${date_di_inf_db}  result_format=%d/%m/%Y
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Archambault
    ...  particulier_prenom=Corette
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Cantin
    ...  particulier_prenom=Joanna
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ${di_inf_1} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_autres_demandeurs}
    # On vérifie que l'infraction n'est pas affichée dans les widgets
    Go To Dashboard
    Element Should Not Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_1}
    Depuis la page d'accueil  juriste  juriste
    Element Should Not Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_1}

    # On saisit une date de contradictoire et une date de retour de
    # contradictoire
    Ajouter une instruction au DI  ${di_inf_1}  Contradictoire  ${date_di_inf_form}  infraction
    Ajouter une instruction au DI  ${di_inf_1}  Retour du contradictoire  null  infraction

    # On vérifie que l'infraction est affichée dans les widgets
    Go To Dashboard
    Element Should Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_1}
    Depuis la page d'accueil  assist  assist
    Element Should Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_1}

    ##
    ## Cas n°2 : infraction dont la date de contradictoire n'est pas supérieure
    ## ou égale à la date du jour + 3 semaines, la date de retour du
    ## contradictoire n'est pas saisie, sans événements de type 'Annlation de
    ## contradictoire' et sans AIT
    ##

    #
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Desnoyer
    ...  particulier_prenom=Etoile
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Meunier
    ...  particulier_prenom=Eglantine
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ${args_peti} =  Create Dictionary
    ${di_inf_2} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}

    # On vérifie que l'infraction n'est pas affichée dans les widgets
    Go To Dashboard
    Element Should Not Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_2}
    Depuis la page d'accueil  juriste  juriste
    Element Should Not Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_2}

    # On saisit une date de contradictoire
    Ajouter une instruction au DI  ${di_inf_2}  Contradictoire  null  infraction

    # On vérifie que l'infraction est affichée dans les widgets
    Go To Dashboard
    Element Should Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_2}
    Depuis la page d'accueil  assist  assist
    Element Should Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_2}

    ##
    ## Cas n°3 : infraction du cas n°1 avec un événement de type 'Annlation de
    ## contradictoire'
    ##

    # On saisit une date de contradictoire
    Depuis la page d'accueil  juriste  juriste
    Ajouter une instruction au DI  ${di_inf_1}  Annulation de contradictoire  null  infraction

    # On vérifie que l'infraction n'est plus affichée dans les widgets
    Go To Dashboard
    Element Should Not Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_1}
    Depuis la page d'accueil  assist  assist
    Element Should Not Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_1}

    ##
    ## Cas n°4 : infraction du cas n°2 avec un AIT créé
    ##

    # On saisit une date de contradictoire
    Depuis la page d'accueil  juriste  juriste
    Ajouter une instruction au DI  ${di_inf_2}  Arrêté interruptif des travaux  null  infraction

    # On vérifie que l'infraction n'est plus affichée dans les widgets
    Go To Dashboard
    Element Should Not Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_2}
    Depuis la page d'accueil  assist  assist
    Element Should Not Contain  css=.widget_dossier_contentieux_contradictoire  ${di_inf_2}


Widget "Mes Recours" et "Mes Infractions"
    [Documentation]  Ces widget affiche les derniers Dossiers recours ou infraction.

    # Constitution du jeu de données
    Depuis la page d'accueil  admin  admin

    # On recupere les dossiers à creer en JSON
    ${json} =  Get File  binary_files/dossier_widget_contentieux_test.json
    # On parse le json pour avoir une liste de dossiers
    ${dossier_list}=  Evaluate  json.loads('''${json}''')  json

    # Création du juriste et du tech
    &{args_affectation_inf} =  Create Dictionary
    ...  instructeur=Félicien Roland (H)
    ...  instructeur_2=Sylvain Camille (H)
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Infraction

    &{args_affectation_rec} =  Create Dictionary
    ...  instructeur=Félicien Roland (H)
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Recours contentieux

    Ajouter l'utilisateur depuis le menu  Félicien Roland  support@atreal.fr  juriste2  juriste2  JURISTE  MARSEILLE
    Ajouter l'instructeur depuis le menu  Félicien Roland  subdivision H  juriste  Félicien Roland
    Ajouter l'utilisateur depuis le menu  Sylvain Camille  support@atreal.fr  tech2  tech2  TECHNICIEN  MARSEILLE
    Ajouter l'instructeur depuis le menu  Sylvain Camille  subdivision H  technicien  Sylvain Camille
    Supprimer l'affectation depuis le menu  null  Infraction
    Supprimer l'affectation depuis le menu  null  Recours contentieux
    Ajouter l'affectation depuis le menu  ${args_affectation_inf}
    Ajouter l'affectation depuis le menu  ${args_affectation_rec}

    # Initialisation du compteur de di
    ${di_id} =  Set Variable  0

    :FOR  ${dossier}  IN  @{dossier_list["infraction"]}

    # Incrementation de l'identifient de di
    \  ${di_id} =  Evaluate  ${di_id}+1

    \  &{args_demandeur} =  Create Dictionary
    \  ...  particulier_prenom=${dossier["args_petitionnaire"]["particulier_prenom"]}
    \  ...  particulier_nom=${dossier["args_petitionnaire"]["particulier_nom"]}
    \  ...  om_collectivite=MARSEILLE

    \  &{args_petitionnaire} =  Create Dictionary
    \  ...  contrevenant_principal=&{args_demandeur}

    \  &{args_demande} =  Create Dictionary
    \  ...  dossier_autorisation_type_detaille=${dossier["args_demande"]["dossier_autorisation_type_detaille"]}
    \  ...  demande_type=${dossier["args_demande"]["demande_type"]}
    \  ...  date_demande=${dossier["args_demande"]["date_demande"]}
    \  ...  om_collectivite=MARSEILLE

    \  ${di} =  Ajouter la demande par WS  ${args_demande}  ${NULL}  ${args_petitionnaire}
    # Concatenation du nom de di_ et du numero de di
    \  Set Test Variable  ${di_inf_${di_id}}  ${di}
    \  Set Suite Variable  ${di_inf_${di_id}}

    # Initialisation du compteur de di
    ${di_id} =  Set Variable  0

    :FOR  ${dossier}  IN  @{dossier_list["recours"]}

    # Incrementation de l'identifient de di
    \  ${di_id} =  Evaluate  ${di_id}+1

    \  &{args_petitionnaire} =  Create Dictionary
    \  ...  particulier_prenom=${dossier["args_petitionnaire"]["particulier_prenom"]}
    \  ...  particulier_nom=${dossier["args_petitionnaire"]["particulier_nom"]}
    \  ...  om_collectivite=MARSEILLE

    \  &{args_demande} =  Create Dictionary
    \  ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    \  ...  demande_type=Dépôt Initial
    \  ...  om_collectivite=MARSEILLE

    \  ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    \  &{args_demande} =  Create Dictionary
    \  ...  dossier_autorisation_type_detaille=${dossier["args_demande"]["dossier_autorisation_type_detaille"]}
    \  ...  demande_type=${dossier["args_demande"]["demande_type"]}
    \  ...  date_demande=${dossier["args_demande"]["date_demande"]}
    \  ...  autorisation_contestee=${di}
    \  ...  om_collectivite=MARSEILLE

    \  ${di_rec} =  Ajouter la demande par WS  ${args_demande}  ${NULL}
    # Concatenation du nom de di_ et du numero de di
    \  Set Test Variable  ${di_rec_${di_id}}  ${di_rec}
    \  Set Suite Variable  ${di_rec_${di_id}}

    \  Set Test Variable  ${di_${di_id}}  ${di}
    \  Set Suite Variable  ${di_${di_id}}


    # Vérification des accés du juriste (accés au infraction et pas au recours)
    Depuis la page d'accueil  juriste2  juriste2

    # On vérifie l'absence du dossier trop vieux
    Element Should Not Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_1}
    # On vérifi la presence des autres
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_2}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_3}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_4}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_5}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_6}

    # On verifie l'ingrité des raccourci
    Click Link  ${di_inf_6}
    Page Title Should Be  Contentieux > Infraction > ${di_inf_6}
    Go Back

    # On verifie le voir +
    Wait Until Element Is Visible  css=.widget_dossier_contentieux_infraction .widget-footer a
    Click Element  css=.widget_dossier_contentieux_infraction .widget-footer a
    Page Title Should Be  Contentieux > Infractions
    Element Should Contain  css=.tab-tab  ${di_inf_1}
    Element Should Contain  css=.tab-tab  ${di_inf_2}
    Element Should Contain  css=.tab-tab  ${di_inf_3}
    Element Should Contain  css=.tab-tab  ${di_inf_4}
    Element Should Contain  css=.tab-tab  ${di_inf_5}
    Element Should Contain  css=.tab-tab  ${di_inf_6}

    Go Back

    # On vérifie l'absence du dossier trop vieux
    Element Should Not Contain  css=#dashboard div.widget_dossier_contentieux_recours  ${di_rec_1}
    # On vérifi la presence des autres
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_recours  ${di_rec_2}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_recours  ${di_rec_3}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_recours  ${di_rec_4}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_recours  ${di_rec_5}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_recours  ${di_rec_6}

    # On verifie l'ingrité des raccourci
    Click Link  ${di_rec_6}
    Page Title Should Be  Contentieux > Recours > ${di_rec_6}
    Go Back

    # On verifie le voir +
    Wait Until Element Is Visible  css=.widget_dossier_contentieux_recours .widget-footer a
    Click Element  css=.widget_dossier_contentieux_recours .widget-footer a
    Page Title Should Be  Contentieux > Recours
    Element Should Contain  css=.tab-tab  ${di_rec_1}
    Element Should Contain  css=.tab-tab  ${di_rec_2}
    Element Should Contain  css=.tab-tab  ${di_rec_3}
    Element Should Contain  css=.tab-tab  ${di_rec_4}
    Element Should Contain  css=.tab-tab  ${di_rec_5}
    Element Should Contain  css=.tab-tab  ${di_rec_6}


    Depuis la page d'accueil  tech2  tech2

    # On vérifie l'absence du dossier trop vieux
    Element Should Not Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_1}
    # On vérifi la presence des autres
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_2}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_3}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_4}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_5}
    Element Should Contain  css=#dashboard div.widget_dossier_contentieux_infraction  ${di_inf_6}

    # On verifie l'ingrité des raccourci
    Click Link  ${di_inf_6}
    Page Title Should Be  Contentieux > Infraction > ${di_inf_6}
    Go Back

    # On verifie le voir +
    Wait Until Element Is Visible  css=.widget_dossier_contentieux_infraction .widget-footer a
    Click Element  css=.widget_dossier_contentieux_infraction .widget-footer a
    Page Title Should Be  Contentieux > Infractions
    Element Should Contain  css=.tab-tab  ${di_inf_1}
    Element Should Contain  css=.tab-tab  ${di_inf_2}
    Element Should Contain  css=.tab-tab  ${di_inf_3}
    Element Should Contain  css=.tab-tab  ${di_inf_4}
    Element Should Contain  css=.tab-tab  ${di_inf_5}
    Element Should Contain  css=.tab-tab  ${di_inf_6}

    # On remet les affectations automatique par défaut
    Depuis la page d'accueil  admin  admin
    Supprimer l'affectation depuis le menu  null  Infraction
    Supprimer l'affectation depuis le menu  null  Recours contentieux
    &{args_affectation_inf} =  Create Dictionary
    ...  instructeur=Juriste (H)
    ...  instructeur_2=Technicien (H)
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Infraction
    &{args_affectation_rec} =  Create Dictionary
    ...  instructeur=Juriste (H)
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Recours contentieux
    Ajouter l'affectation depuis le menu  ${args_affectation_inf}
    Ajouter l'affectation depuis le menu  ${args_affectation_rec}


Widget "Nouveau dossier" et "Nouveau dossier contentieux"
    [Documentation]  Ces widgets affichent un lien pour rediriger l'utilisateur
    ...  Soit vers un nouveau dossier ADS soit vers un nouveau dossier contentieux.

    Depuis la page d'accueil  guichet  guichet
    Go To Dashboard
    Click On Link  Cliquer ici pour saisir une nouvelle demande concernant le dépôt d'un nouveau dossier
    Submenu In Menu Should Be Selected  guichet_unique  nouveau-dossier

    Depuis la page d'accueil  assist  assist
    Go To Dashboard
    Click On Link  Cliquer ici pour saisir une nouvelle demande concernant le dépôt d'un nouveau dossier
    Submenu In Menu Should Be Selected  contentieux  nouveau-dossier


Widget "Retours de Commission"
    [Documentation]  Ce widget indique le nombre de retours de commission non
    ...  lus.

    # On crée une collectivité pour ne pas perturber ni être perturbé par
    # les autres tests.
    ${collectivite_a} =  Set Variable  MÉRIGNAC
    ${collectivite_b} =  Set Variable  SAINT-JOSEPH
    ${direction_a} =  Set Variable  Direction ME
    ${direction_b} =  Set Variable  Direction SJ
    ${direction_code_a} =  Set Variable  ME
    ${direction_code_b} =  Set Variable  SJ
    ${div_a1} =  Set Variable  subdivision ME1
    ${div_code_a1} =  Set Variable  ME1
    ${div_a2} =  Set Variable  subdivision ME2
    ${div_code_a2} =  Set Variable  ME2
    ${div_b} =  Set Variable  subdivision SJ
    ${div_code_b} =  Set Variable  SJ
    #
    ${utilisateur_nom_01} =  Set Variable  Arnolda Calis
    ${utilisateur_login_01} =  Set Variable  acalis
    ${utilisateur_nom_02} =  Set Variable  Bekhan Panova
    ${utilisateur_login_02} =  Set Variable  bpanova
    ${utilisateur_nom_03} =  Set Variable  Jolina Toral
    ${utilisateur_login_03} =  Set Variable  jtoral
    ${utilisateur_nom_04} =  Set Variable  Felix Simonsen
    ${utilisateur_login_04} =  Set Variable  fsimonsen
    #
    Depuis la page d'accueil  admin  admin
    #
    Ajouter la collectivité depuis le menu  ${collectivite_a}  mono
    Ajouter la collectivité depuis le menu  ${collectivite_b}  mono
    #
    Ajouter l'utilisateur  ${utilisateur_nom_01}  nospam@openmairie.org  ${utilisateur_login_01}  ${utilisateur_login_01}  INSTRUCTEUR  ${collectivite_a}
    Ajouter l'utilisateur  ${utilisateur_nom_02}  nospam@openmairie.org  ${utilisateur_login_02}  ${utilisateur_login_02}  INSTRUCTEUR  ${collectivite_a}
    Ajouter l'utilisateur  ${utilisateur_nom_03}  nospam@openmairie.org  ${utilisateur_login_03}  ${utilisateur_login_03}  INSTRUCTEUR  ${collectivite_a}
    Ajouter l'utilisateur  ${utilisateur_nom_04}  nospam@openmairie.org  ${utilisateur_login_04}  ${utilisateur_login_04}  INSTRUCTEUR  ${collectivite_b}
    #
    Ajouter la direction depuis le menu  ${direction_code_a}  ${direction_a}
    ...  null  Chef A  null  null  ${collectivite_a}
    Ajouter la direction depuis le menu  ${direction_code_b}  ${direction_b}
    ...  null  Chef B  null  null  ${collectivite_b}
    #
    Ajouter la division depuis le menu  ${div_code_a1}  ${div_a1}  null
    ...  Chef A  null  null  ${direction_a}
    Ajouter la division depuis le menu  ${div_code_a2}  ${div_a2}  null
    ...  Chef A  null  null  ${direction_a}
    Ajouter la division depuis le menu  ${div_code_b}  ${div_b}  null
    ...  Chef B  null  null  ${direction_b}
    #
    #
    Ajouter l'instructeur depuis le menu  ${utilisateur_nom_01}  ${div_a1}
    ...  instructeur  ${utilisateur_nom_01}
    Ajouter l'instructeur depuis le menu  ${utilisateur_nom_02}  ${div_a2}
    ...  instructeur  ${utilisateur_nom_02}
    Ajouter l'instructeur depuis le menu  ${utilisateur_nom_03}  ${div_a1}
    ...  instructeur  ${utilisateur_nom_03}
    Ajouter l'instructeur depuis le menu  ${utilisateur_nom_04}  ${div_b}
    ...  instructeur  ${utilisateur_nom_04}
    #
    &{args_affectation} =  Create Dictionary
    ...  instructeur=${utilisateur_nom_01} (${div_code_a1})
    ...  om_collectivite=${collectivite_a}
    Ajouter l'affectation depuis le menu  ${args_affectation}
    &{args_affectation} =  Create Dictionary
    ...  instructeur=${utilisateur_nom_02} (${div_code_a2})
    ...  om_collectivite=${collectivite_a}
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    Ajouter l'affectation depuis le menu  ${args_affectation}
    &{args_affectation} =  Create Dictionary
    ...  instructeur=${utilisateur_nom_03} (${div_code_a1})
    ...  om_collectivite=${collectivite_a}
    ...  dossier_autorisation_type_detaille=Permis de démolir
    Ajouter l'affectation depuis le menu  ${args_affectation}
    &{args_affectation} =  Create Dictionary
    ...  instructeur=${utilisateur_nom_04} (${div_code_b})
    ...  om_collectivite=${collectivite_b}
    Ajouter l'affectation depuis le menu  ${args_affectation}

    # DI n°1 : Permis de démolir dans Collectivité A (niveau mono)
    # => Affecté à l'instructeur '${utilisateur_nom_03}' (${utilisateur_login_03})
    # => Division 'J'
    #
    &{args_petitionnaire_01} =  Create Dictionary
    ...  particulier_nom=Hajnal
    ...  particulier_prenom=Katalin
    ...  om_collectivite=${collectivite_a}
    #
    &{args_demande_01} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de démolir
    ...  demande_type=Dépôt Initial
    ...  date_demande=${date_ddmmyyyy}
    ...  om_collectivite=${collectivite_a}
    #
    ${di_01} =  Ajouter la demande par WS  ${args_demande_01}  ${args_petitionnaire_01}

    # DI n°2 : Permis de construire pour une maison individuelle et / ou ses annexes dans Collectivité A (niveau mono)
    # => Affecté à l'instructeur '${utilisateur_nom_01}' (${utilisateur_login_01})
    # => Division 'H'
    #
    &{args_petitionnaire_02} =  Create Dictionary
    ...  particulier_nom=Stoter
    ...  particulier_prenom=Ashni
    ...  om_collectivite=${collectivite_a}
    #
    &{args_demande_02} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=${date_ddmmyyyy}
    ...  om_collectivite=${collectivite_a}
    #
    ${di_02} =  Ajouter la demande par WS  ${args_demande_02}  ${args_petitionnaire_02}

    # DI n°3 : Permis de construire comprenant ou non des démolitions dans Collectivité A (niveau mono)
    # => Affecté à l'instructeur '${utilisateur_nom_02}' (${utilisateur_login_02})
    # => Division 'L'
    #
    &{args_petitionnaire_03} =  Create Dictionary
    ...  particulier_nom=West
    ...  particulier_prenom=Simon
    ...  om_collectivite=${collectivite_a}
    #
    &{args_demande_03} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    ...  demande_type=Dépôt Initial
    ...  date_demande=${date_ddmmyyyy}
    ...  om_collectivite=${collectivite_a}
    #
    ${di_03} =  Ajouter la demande par WS  ${args_demande_03}  ${args_petitionnaire_03}

    # DI n°4 : Permis de construire pour une maison individuelle et / ou ses annexes dans Collectivité B (niveau mono)
    # => Affecté à l'instructeur '${utilisateur_nom_04}' (${utilisateur_login_04})
    # => Division 'H'
    #
    &{args_petitionnaire_04} =  Create Dictionary
    ...  particulier_nom=Martin
    ...  particulier_prenom=Eloise
    ...  om_collectivite=${collectivite_b}
    #
    &{args_demande_04} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=${date_ddmmyyyy}
    ...  om_collectivite=${collectivite_b}
    #
    ${di_04} =  Ajouter la demande par WS  ${args_demande_04}  ${args_petitionnaire_04}


    ${code_type_commission_c} =  Set Variable  TC
    &{args_type_de_commission} =  Create Dictionary
    ...  code=${code_type_commission_c}
    ...  libelle=Type C
    ...  om_collectivite=${collectivite_a}
    Ajouter type de commission  ${args_type_de_commission}

    ${code_type_commission_d} =  Set Variable  TD
    &{args_type_de_commission} =  Create Dictionary
    ...  code=${code_type_commission_d}
    ...  libelle=Type D
    ...  om_collectivite=${collectivite_b}
    Ajouter type de commission  ${args_type_de_commission}

    # Demandes passage de commission
    Ajouter la commission depuis le contexte du dossier d'instruction
    ...  ${di_01}  Type C  ${date_ddmmyyyy}
    Ajouter la commission depuis le contexte du dossier d'instruction
    ...  ${di_02}  Type C  ${date_ddmmyyyy}
    Ajouter la commission depuis le contexte du dossier d'instruction
    ...  ${di_03}  Type C  ${date_ddmmyyyy}
    Ajouter la commission depuis le contexte du dossier d'instruction
    ...  ${di_04}  Type D  ${date_ddmmyyyy}

    # On vérifie que le widget n'indique aucun retour non lu
    Depuis la page d'accueil  ${utilisateur_login_01}  ${utilisateur_login_01}
    Element Should Contain  css=.widget_commission_retours .widget-content
    ...  Aucun retour de commission non lu.

    # Créer une commission pour les 2 collectivités
    Depuis la page d'accueil  admin  admin
    &{args_commission} =  Create Dictionary
    ...  om_collectivite=${collectivite_a}
    ...  commission_type=Type C
    Ajouter un suivi de commission  ${args_commission}
    &{args_commission} =  Create Dictionary
    ...  om_collectivite=${collectivite_b}
    ...  commission_type=Type D
    Ajouter un suivi de commission  ${args_commission}

    # Planifier et rendre des avis sur les 4 DI
    Planifier un dossier pour une commission
    ...  ${di_01}  ${code_type_commission_c}${DATE_FORMAT_YYYYMMDD}
    Rendre un avis sur dossier passé en commission
    ...  favorable  ${di_01}  ${code_type_commission_c}${DATE_FORMAT_YYYYMMDD}

    Planifier un dossier pour une commission
    ...  ${di_02}  ${code_type_commission_c}${DATE_FORMAT_YYYYMMDD}
    Rendre un avis sur dossier passé en commission
    ...  favorable  ${di_02}  ${code_type_commission_c}${DATE_FORMAT_YYYYMMDD}

    Planifier un dossier pour une commission
    ...  ${di_03}  ${code_type_commission_c}${DATE_FORMAT_YYYYMMDD}
    Rendre un avis sur dossier passé en commission
    ...  favorable  ${di_03}  ${code_type_commission_c}${DATE_FORMAT_YYYYMMDD}

    Planifier un dossier pour une commission
    ...  ${di_04}  ${code_type_commission_d}${DATE_FORMAT_YYYYMMDD}
    Rendre un avis sur dossier passé en commission
    ...  favorable  ${di_04}  ${code_type_commission_d}${DATE_FORMAT_YYYYMMDD}

    ## Vérification du filtre par défaut (instructeur)
    Depuis la page d'accueil  ${utilisateur_login_01}  ${utilisateur_login_01}
    Element Should Contain  css=.widget_commission_retours .box-icon  1
    # On clique sur le lien du widget (Voir +)
    Click Link  css=.widget_commission_retours .widget-footer a
    Page Title Should Be  Instruction > Commissions > Mes Retours
    # L'instructeur doit seulement voir son dossier
    Element Should Contain  css=#tab-commission_mes_retours  ${di_02}
    Element Should Not Contain  css=#tab-commission_mes_retours  ${di_01}
    Element Should Not Contain  css=#tab-commission_mes_retours  ${di_03}
    Element Should Not Contain  css=#tab-commission_mes_retours  ${di_04}

    Page Should Not Contain Errors
    Click Link  ${di_02}
    Page Title Should Contain  Instruction > Dossiers D'instruction > ${di_02}
    Page Should Not Contain Errors

    ## Vérification du filtre instructeur
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  commission_retours
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=instructeur
    Click On Submit Button

    Depuis la page d'accueil  ${utilisateur_login_04}  ${utilisateur_login_04}
    Element Should Contain  css=.widget_commission_retours .box-icon  1
    # On clique sur le lien du widget (Voir +)
    Click Link  css=.widget_commission_retours .widget-footer a
    Page Title Should Be  Instruction > Commissions > Mes Retours
    # L'instructeur doit seulement voir son dossier
    Element Should Contain  css=#tab-commission_mes_retours  ${di_04}
    Element Should Not Contain  css=#tab-commission_mes_retours  ${di_01}
    Element Should Not Contain  css=#tab-commission_mes_retours  ${di_02}
    Element Should Not Contain  css=#tab-commission_mes_retours  ${di_03}

    ## Filtre sur la division
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  commission_retours
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=division
    Click On Submit Button

    Depuis la page d'accueil  ${utilisateur_login_01}  ${utilisateur_login_01}
    # On doit avoir 2 dossiers de la division
    Element Should Contain  css=.widget_commission_retours .box-icon  2
    # On clique sur le lien du widget (Voir +)
    Click Link  css=.widget_commission_retours .widget-footer a
    Page Title Should Be  Instruction > Commissions > Retours De Ma Division
    Element Should Contain  css=#tab-commission_retours_ma_division  ${di_01}
    Element Should Contain  css=#tab-commission_retours_ma_division  ${di_02}
    Element Should Not Contain  css=#tab-commission_retours_ma_division  ${di_04}
    Element Should Not Contain  css=#tab-commission_retours_ma_division  ${di_03}

    ## Filtre sur la division quand aucun instructeur sur le DI
    # TNR pour s'assurer que la division est récupérée directement sur le DI et
    # pas sur l'instructeur du DI. On supprime donc l'instructeur d'un DI.
    Depuis la page d'accueil  admin  admin
    &{modifications} =  Create Dictionary  instructeur=choisir l'instructeur${EMPTY}
    Modifier le dossier d'instruction  ${di_01}  ${modifications}

    Depuis la page d'accueil  ${utilisateur_login_01}  ${utilisateur_login_01}
    # On doit avoir 2 dossiers de la division
    Element Should Contain  css=.widget_commission_retours .box-icon  2
    # On clique sur le lien du widget (Voir +)
    Click Link  css=.widget_commission_retours .widget-footer a
    Page Title Should Be  Instruction > Commissions > Retours De Ma Division
    Element Should Contain  css=#tab-commission_retours_ma_division  ${di_01}
    Element Should Contain  css=#tab-commission_retours_ma_division  ${di_02}
    Element Should Not Contain  css=#tab-commission_retours_ma_division  ${di_04}
    Element Should Not Contain  css=#tab-commission_retours_ma_division  ${di_03}

    ## Filtre "aucun" donc sur la collectivité
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  commission_retours
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=aucun
    Click On Submit Button

    Depuis la page d'accueil  ${utilisateur_login_01}  ${utilisateur_login_01}
    # On doit avoir 3 dossiers de la collectivité
    Element Should Contain  css=.widget_commission_retours .box-icon  3
    # On clique sur le lien du widget (Voir +)
    Click Link  css=.widget_commission_retours .widget-footer a
    Page Title Should Be  Instruction > Commissions > Tous Les Retours
    Element Should Contain  css=#tab-commission_tous_retours  ${di_01}
    Element Should Contain  css=#tab-commission_tous_retours  ${di_02}
    Element Should Contain  css=#tab-commission_tous_retours  ${di_03}
    Element Should Not Contain  css=#tab-commission_tous_retours  ${di_04}

    ## Marquage comme non lu et vérifation widget et listing
    Click Link  ${di_01}  # pas le même instructeur mais même division
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Portlet Action Should Be In SubForm  dossier_commission  marquer_comme_lu
    # On marque comme lu
    Click On SubForm Portlet Action  dossier_commission  marquer_comme_lu
    Valid Message Should Contain In Subform    Mise à jour effectuée avec succès
    Element Should Contain  css=#lu  Oui

    # vérification que c'est bien pris en compte dans widget et listing
    Depuis la page d'accueil  ${utilisateur_login_01}  ${utilisateur_login_01}
    # On doit avoir 2 dossiers de la collectivité
    Element Should Contain  css=.widget_commission_retours .box-icon  2
    # On clique sur le lien du widget (Voir +)
    Click Link  css=.widget_commission_retours .widget-footer a
    Page Title Should Be  Instruction > Commissions > Tous Les Retours
    Element Should Contain  css=#tab-commission_tous_retours  ${di_02}
    Element Should Contain  css=#tab-commission_tous_retours  ${di_03}
    Element Should Not Contain  css=#tab-commission_tous_retours  ${di_01}
    Element Should Not Contain  css=#tab-commission_tous_retours  ${di_04}

    ## Nettoyage: suppression de l'argument du widget pour ne pas perturber les
    ## autres tests.
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  commission_retours
    Click On Form Portlet Action  om_widget  modifier
    Input Text  arguments  ${EMPTY}
    Click On Submit Button
