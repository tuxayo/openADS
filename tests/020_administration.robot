*** Settings ***
Documentation    Menu Administration
# On inclut les mots-clefs
Resource    resources/resources.robot
# On ouvre et on ferme le navigateur respectivement au début et à la fin
# du Test Suite.
Suite Setup    For Suite Setup
Suite Teardown    For Suite Teardown


*** Test Cases ***
Synchronisation des utilisateurs avec un annuaire LDAP

    [Documentation]  On teste la synchronisation des utilisateurs avec le ldap
    ...  Les utilisateurs qui devront être ajoutés et mis à jour :
    ...  einstein, newton, galieleo, tesla
    ...  Et les utilisateurs qui devront être supprimés :
    ...  ldap_instructeur et ldap_service

    [Tags]  exclude

    #
    Depuis la page d'accueil  admin  admin
    # On accède à l'écran de synchronisation via le menu
    Go To Submenu In Menu  administration  annuaire
    # On vérifie le titre de l'écran
    Page Title Should Be  Administration > Utilisateur > Synchronisation Annuaire
    # On vérifie que le menu est ouvert sur l'élément correct
    Submenu In Menu Should Be Selected  administration  annuaire
    # ATTENTION POSTULAT : Il y a deux utilisateurs LDAP dans la base
    # et le ldap auquel nous sommes connectés contient 4 utilisateurs qui ne
    # sont pas les deux déjà en base
    Page Should Contain  Il y a 4 utilisateur(s) présent(s) dans l'annuaire et non présent(s) dans la base => 4 ajout(s)
    Page Should Contain  Il y a 2 utilisateur(s) présent(s) dans la base et non présent(s) dans l'annuaire => 2 suppression(s)
    Page Should Contain  Il y a 0 utilisateur(s) présent(s) à la fois dans la base et l'annuaire => 0 mise(s) à jour
    # On clique sur "Synchroniser"
    Click On Submit Button
    # On vérifie que tout s'est bien passé
    Valid Message Should Be  La synchronisation des utilisateurs est terminée.

    # l'utilisateur ldap_instructeur ne doit plus être présent
    Depuis le tableau des utilisateurs
    Use Simple Search  login  ldap_instructeur
    Element Should Contain  css=#tab-om_utilisateur table.tab-tab tbody  Aucun enregistrement.

    # l'utilisateur ldap_instructeur ne doit plus être présent
    Depuis le tableau des utilisateurs
    Use Simple Search  login  ldap_service
    Element Should Contain  css=#tab-om_utilisateur table.tab-tab tbody  Aucun enregistrement.

    # On vérifie que les 3 utilisateurs sont bien présents avec l'information LDAP
    Depuis le contexte de l'utilisateur  einstein
    Depuis le contexte de l'utilisateur  newton
    Depuis le contexte de l'utilisateur  galieleo
    Depuis le contexte de l'utilisateur  tesla

    # On supprime un des 3 utilisateurs
    Supprimer l'utilisateur  galieleo

    # On retourne au tableau de bord
    Go To Dashboard
    # On accède à l'écran de synchronisation via le menu
    Go To Submenu In Menu  administration  annuaire
    # On vérifie le titre de l'écran
    Page Title Should Be  Administration > Utilisateur > Synchronisation Annuaire
    # ATTENTION POSTULAT : Il n'y a aucun utilisateur LDAP dans la base
    # et le ldap auquel nous sommes connectés contient 3 utilisateurs
    Page Should Contain  Il y a 1 utilisateur(s) présent(s) dans l'annuaire et non présent(s) dans la base => 1 ajout(s)
    Page Should Contain  Il y a 0 utilisateur(s) présent(s) dans la base et non présent(s) dans l'annuaire => 0 suppression(s)
    Page Should Contain  Il y a 3 utilisateur(s) présent(s) à la fois dans la base et l'annuaire => 3 mise(s) à jour
    # On clique sur "Synchroniser"
    Click On Submit Button
    # On vérifie que tout s'est bien passé
    Valid Message Should Be  La synchronisation des utilisateurs est terminée.

    # Test WS
    ${json} =  Set Variable  { "module": "user", "data": "NA"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Synchronisation terminée.


Affichage des champs de fusion

    [Documentation]    Permet de tester la liste des champs de fusion.

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Test
    ...  particulier_prenom=Fusion
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ...  terrain_adresse_voie_numero=27
    ...  terrain_adresse_voie=rue grande
    ...  terrain_adresse_lieu_dit=Moulin de redon
    ...  terrain_adresse_code_postal=13390
    ...  terrain_adresse_localite=Auriol

    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instr  instr

    Ajouter une consultation depuis un dossier  ${di_libelle}  59.72 - DDTM 13 - Service Urbanisme

    Depuis l'onglet consultation du dossier  ${di_libelle}
    Click On Link  59.72 - DDTM 13 - Service Urbanisme
    Click On Link  Éditer la consultation PDF

    Open PDF  file

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Moulin de redon

    Close PDF


TNR Listing des utilisateurs sans le mot de passe

    [Documentation]    Dans le listing des utilisateurs la colonne mot de passe
    ...    ne doit pas apparaître.

    # On s'identifie en tant qu'admin
    Depuis la page d'accueil  admin  admin
    # On accède au listing des utilisateurs
    Go to tab    om_utilisateur
    # On recherche l'utilisateur admin
    Use Simple Search    login    admin
    # On vérifie que la page ne contient pas le hash md5 de l'utilisateur admin
    Page Should Not Contain    21232f297a57a5a743894a0e4a801fc3


TNR Ajout d'une bible

    [Documentation]    On doit pouvoir créer une bible liée ou non à un événement.

    # On s'identifie en tant qu'admin
    Depuis la page d'accueil  admin  admin
    # Sans liaison
    Ajouter une bible depuis le paramétrage dossiers  null  Bible seule  Pour plus tard  null  null  null  MARSEILLE
    # Avec liaison
    Ajouter une bible depuis le paramétrage dossiers  Abandonner les travaux  Bible liée  Pour maintenant  null  null  null  MARSEILLE


TNR Vérifier l'orthographe des libellés des types de dossier d'instruction

    [Documentation]  Vérifie que les libellés des types de DI ont la bonne orthographe.

    #
    Depuis la page d'accueil  admin  admin
    # On va sur menu
    Go To Submenu In Menu  parametrage-dossier  dossier_instruction_type
    # On recherche le type de DA DP
    Use Simple Search  type de dossier d'autorisation détaillé  DP (Déclaration préalable)
    # On vérifie que le libellé "Initiale" existe
    Page Should Contain  Initiale
    # On recherche le type de DA DPS
    Use Simple Search  type de dossier d'autorisation détaillé  DPS (DECLARATION PREALABLE SIMPLE)
    # On vérifie que le libellé "Initiale" existe
    Page Should Contain  Initiale
    # On recherche le type de DA AZ
    Use Simple Search  type de dossier d'autorisation détaillé  AZ (Demande d'autorisation spéciale de travaux dans le périmètre d'une AVAP)
    # On vérifie que le libellé "Initiale" existe
    Page Should Contain  Initiale
    # On recherche le type de DA AT
    Use Simple Search  type de dossier d'autorisation détaillé  AT (Demande d'autorisation de construire, d'aménager ou de modifier un ERP)
    # On vérifie que le libellé "Initiale" existe
    Page Should Contain  Initiale


Paramétrage d'un logo

    [Documentation]  Le but de ce test est de vérifier si la création de logo
    ...  le format de fichier envoyer et si la copie du logo duplique le fichier

    # On se connecte en tant que "admin"
    Depuis la page d'accueil  admin  admin

    ##
    ## Ajout d'un logo, avec vérification des contraintes sur le fichier
    ##
    # On accède au listing des logos
    Depuis le listing des logos
    # On clique sur le lien d'ajout de logo
    Click On Add Button
    # On saisie des données
    Input Text  css=#id  logo.jpg
    Input Text  css=#libelle  Un logo
    Select Checkbox  css=#actif
    Select From List By Label  css=#om_collectivite  MARSEILLE
    # On tente d'ajouter un fichier trop gros
    Add File and Expect Error Message Contain  fichier  image_1.jpg  excède la directive
    # On tente d'ajouter un fichier avec une mauvais extension
    Add File and Expect Error Message Be  fichier  fichier_1.odt  Le fichier n'est pas conforme à la liste des extension(s) autorisée(s) (.gif;.jpg;.jpeg;.png;.txt;.pdf;.csv;). [fichier_1.odt]
    # On ajoute un fichier correct
    Add File  fichier  image_2.jpg
    # On ajoute le logo en BDD
    Click On Submit Button
    # On vérifie que le logo s'est bien ajouté
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Back Button

    # On va créer une copie du logo
    Click Link  Un logo
    Click On Form Portlet Action  om_logo  copier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Button  Confirmer
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  L’élément a été correctement dupliqué.
    Click On Back Button

    # On va supprimer le duplicata et verifier si le fichier existe toujour
    Click Link  copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_logo  supprimer
    Click On Submit Button
    Click On Back Button
    Click Link  Un logo
    Element Should Not Contain  content  Le fichier n'existe pas ou n'est pas accessible.


Paramétrage d'un instructeur appartenant à une division
    [Documentation]  Vérification du fonctionnement de l'ajout d'instructeur
    ...  depuis l'onglet dans utilisateur et depuis le menu instructeur pour
    ...  vérifier le filtre sur les collectivités,
    ...  il va vérifier aussi:
    ...   - La liste vide en cas d'ajour
    ...   - La list des instructeur pour allauch
    ...   - Pour une modification les parametres par defaut
    ...   - Si on change la division que la liste d'utilisateur suit
    ...   - Que la liste des division soit bien limité pour les utilisateurs mono
    ...   - Si la direction change qu'on puisse toujour choisir la division d'origine
    ...  Puis dans les autres interfaces integrant les instructeurs
    ...  (division et om_utilisateur):
    ...  - La liste de l'ajout
    ...  - La liste de modification


    # Vérification du fonctionnement de base.
    Depuis la page d'accueil  admin  admin

    ${collectivite} =  Set Variable  AUVERGNE
    ${division} =  Set Variable  subdivision A
    ${utilisateur_instr_nom} =  Set Variable  Esperanza Lachance
    ${utilisateur_instr_login} =  Set Variable  elachance
    ${utilisateur_admingen_nom} =  Set Variable  France Martel
    ${utilisateur_admingen_login} =  Set Variable  fmartel
    Ajouter la collectivité depuis le menu  ${collectivite}  mono
    Ajouter la direction depuis le menu  A  Direction A  null  Chef A  null  null  ${collectivite}
    Ajouter la division depuis le menu  A  ${division}  null  Chef A  null  null  Direction A
    Ajouter l'utilisateur  ${utilisateur_instr_nom}  nospam@openmairie.org  ${utilisateur_instr_login}  ${utilisateur_instr_login}  INSTRUCTEUR  ${collectivite}
    Ajouter l'utilisateur  ${utilisateur_admingen_nom}  nospam@openmairie.org  ${utilisateur_admingen_login}  ${utilisateur_admingen_login}  ADMINISTRATEUR GENERAL  ${collectivite}

    Depuis le tableau des instructeurs
    Click On Add Button
    Select From List By Label  division  ${division}
    Sleep  1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label  om_utilisateur  ${utilisateur_instr_nom}
    @{select_utilisateur} =  Get List Items  om_utilisateur
    Should Not Contain Match  ${select_utilisateur}  Instructeur
    Should Contain Match  ${select_utilisateur}  ${utilisateur_instr_nom}

    # On vérifie si à l'ajout le select est vide
    Depuis le tableau des instructeurs
    Click On Add Button
    @{liste_instructeur} =  Create List
    ...  choisir Utilisateur
    Select List Should Be  om_utilisateur  ${liste_instructeur}

    # On verifie la liste d'instructeur pour le une division d'Allauch
    Select From List By Label  division  subdivision L
    @{liste_instructeur_allauch} =  Create List
    ...  choisir Utilisateur
    ...  Instr. poly. Allauch
    Select List Should Be  om_utilisateur  ${liste_instructeur_allauch}

    # On vérifie que les paramètres par defauts sont remplis
    Depuis le tableau des instructeurs
    Use Simple Search  nom  Poly Com Allauch
    Click Link  Poly Com Allauch
    Click On Form Portlet Action  instructeur  modifier
    Selected List Label Should Be  division  subdivision L
    Selected List Label Should Be  om_utilisateur  Instr. poly. Allauch

    # On verifie le changement de liste dans lors du chengement de division
    Select From List By Label  division  ${division}
    @{select_utilisateur} =  Get List Items  om_utilisateur
    Should Contain Match  ${select_utilisateur}  ${utilisateur_instr_nom}

    # Modification de la collectivité de la direction ADS
    Depuis le tableau des directions
    Click Link  Direction ADS
    Click On Form Portlet Action  direction  modifier
    Select From List By Label  om_collectivite  agglo
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

    Depuis la page d'accueil  admingenmars  admingenmars
    Go To Tab  om_utilisateur
    Use Simple Search  login  instr
    Click Link  instr
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  On clique sur l'onglet  instructeur  Instructeur
    Click Link  Instructeur
    Click On SubForm Portlet Action  instructeur  modifier
    @{liste_division} =  Create List
    ...  choisir division
    ...  subdivision H
    Select List Should Be  division  ${liste_division}
    Selected List Label Should Be  division  subdivision H

    Depuis la page d'accueil  admin  admin
    Depuis le tableau des directions
    Click Link  Direction ADS
    Click On Form Portlet Action  direction  modifier
    Select From List By Label  om_collectivite  MARSEILLE
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

    # On verifie que la liste des division soit bien limité pour les utilisateurs mono
    Depuis la page d'accueil  ${utilisateur_admingen_login}  ${utilisateur_admingen_login}
    Depuis le tableau des instructeurs
    Click On Add Button
    @{liste_division_A} =  Create List
    ...  choisir division
    ...  subdivision A
    Select List Should Be  division  ${liste_division_A}

    # On vérifie la liste des division (contenu / selection) dans utilisateur
    Depuis la page d'accueil  admin  admin
    Go To Tab  om_utilisateur
    Use Simple Search  login  instr
    Click Link  instr
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  On clique sur l'onglet  instructeur  Instructeur
    Click On Add Button
    @{liste_division} =  Create List
    ...  choisir division
    ...  subdivision H
    ...  subdivision J
    Select List Should Be  division  ${liste_division}

    Go To Tab  om_utilisateur
    Use Simple Search  login  instr
    Click Link  instr
    On clique sur l'onglet  instructeur  Instructeur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link  Instructeur
    Click On SubForm Portlet Action  instructeur  modifier
    Selected List Label Should Be  division  subdivision H

    # On vérifie la liste des division (contenu / selection) dans division
    Go To Tab  division
    Use Simple Search  code  L
    Click Link  L
    On clique sur l'onglet  instructeur  Instructeur
    Click On Add Button
    @{liste_utilisateur} =  Create List
    ...  choisir Utilisateur
    ...  Instr. poly. Allauch
    Select List Should Be  om_utilisateur  ${liste_utilisateur}


    Go To Tab  division
    Use Simple Search  code  L
    Click Link  L
    On clique sur l'onglet  instructeur  Instructeur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link  Poly Com Allauch
    Click On SubForm Portlet Action  instructeur  modifier
    Selected List Label Should Be  om_utilisateur  Instr. poly. Allauch