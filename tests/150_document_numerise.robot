#
# Description
#
# @package openads
# @version SVN : $Id $
#

*** Settings ***
Documentation  Test les pièces.

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    # Liste des arguments pour la demande
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    # Liste des arguments pour le pétitionnaire
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_civilite=Madame
    ...  particulier_nom=Rivière
    ...  particulier_prenom=Coralie
    ...  om_collectivite=MARSEILLE
    #
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    # Liste des arguments pour la demande
    &{args_demande_at} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Demande d'autorisation de construire, d'aménager ou de modifier un ERP
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    # Liste des arguments pour le pétitionnaire
    &{args_petitionnaire_at} =  Create Dictionary
    ...  particulier_civilite=Madame
    ...  particulier_nom=Duplanty
    ...  particulier_prenom=Dominic
    ...  om_collectivite=MARSEILLE
    #
    ${di_at} =  Ajouter la demande par WS  ${args_demande_at}  ${args_petitionnaire_at}

    #
    Depuis la page d'accueil  instr  instr
    #
    Ajouter une consultation depuis un dossier  ${di}  59.13 - Régie des Tranports de Marseille - DTP/CIP

    # Les dossiers sont accessibles dans la suite du test
    Set Suite Variable  ${di}
    Set Suite Variable  ${di_at}


Catégorie de pièce
    [Documentation]  Ajoute, modifie et supprime une catégorie de pièce.
    ...  Vérifie l'ajout de type depuis le sous-formulaire.

    # On ajoute une catégorie
    Depuis la page d'accueil  admin  admin
    ${dntc_libelle} =  Set Variable  Document numérisé
    &{dntc_values} =  Create Dictionary
    ...  libelle=${dntc_libelle}
    Ajouter la catégorie de pièces  ${dntc_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On récupère l'identifiant de la catégorie
    Depuis le contexte de la catégorie de pièces  ${dntc_libelle}
    ${dntc_id} =  Get Text    css=#document_numerise_type_categorie

    # On modifie une catégorie
    ${dntc2_libelle} =  Set Variable  Pièce numérisée
    &{dntc_values} =  Create Dictionary
    ...  libelle=${dntc2_libelle}
    Modifier la catégorie de pièces  ${dntc_libelle}  ${dntc_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # On supprime une catégorie
    ${dntc3_libelle} =  Set Variable  Catégorie à supprimer
    &{dntc_values} =  Create Dictionary
    ...  libelle=${dntc3_libelle}
    Ajouter la catégorie de pièces  ${dntc_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Supprimer la catégorie de pièces  ${dntc3_libelle}
    Valid Message Should Contain  La suppression a été correctement effectuée.

    # On ajoute un type depuis la catégorie
    ${instructeur_qualite} =  Create List
    ...  instructeur
    &{dnt_values} =  Create Dictionary
    ...  code=IMG
    ...  libelle=Image
    ...  instructeur_qualite=${instructeur_qualite}
    Depuis le contexte de la catégorie de pièces  ${dntc2_libelle}
    On clique sur l'onglet  document_numerise_type  Type De Pièces
    # On vérifie que le tableau est vide
    Element Should Contain  css=#sousform-document_numerise_type  Aucun enregistrement
    # On vérifie que la catégorie soit déjà sélectionné et que les cases à
    # cocher 'aff_da' et 'aff_service_consulte' soient déjà cochées
    Click On Add Button JS
    Form Value Should Be  css=input#document_numerise_type_categorie  ${dntc_id}
    Form Value Should Be  aff_service_consulte  Oui
    Form Value Should Be  aff_da  Oui
    Saisir le type de pièces en sous-formulaire  ${dnt_values}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.


Type de pièce
    [Documentation]  Ajoute, modifie et supprime un type de pièce.

    # On ajoute une catégorie car c'est un champ obligatoire pour les types
    Depuis la page d'accueil  admin  admin
    ${dntc_libelle} =  Set Variable  Plan pour dossier
    &{dntc_values} =  Create Dictionary
    ...  libelle=${dntc_libelle}
    Ajouter la catégorie de pièces  ${dntc_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # On ajoute un type, on vérifie que les cases à cocher 'aff_da' et
    # 'aff_service_consulte' soient déjà cochées et que le champ
    # 'synchro_metadonnee' non visible soit à false
    ${dnt_code} =  Set Variable  DCPL01
    &{dnt_values} =  Create Dictionary
    ...  code=${dnt_code}
    ...  libelle=Plan pour dossier de coordination
    ...  document_numerise_type_categorie=${dntc_libelle}
    Go To Tab  document_numerise_type
    Click On Add Button
    Form Value Should Be  aff_service_consulte  Oui
    Form Value Should Be  aff_da  Oui
    Saisir le type de pièces  ${dnt_values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Depuis le contexte du type de pièces  ${dnt_code}
    ${synchro_metadonnee} =  Get Mandatory Value  css=#synchro_metadonnee
    Should Be equal  ${synchro_metadonnee}  f

    # On modifie le champ aff_da du type et on vérifie que le champ
    #'synchro_metadonnee' devient true
    &{dnt_values} =  Create Dictionary
    ...  libelle=Plan pour dossier de coordination 01
    ...  aff_da=false
    Modifier le type de pièces  ${dnt_code}  ${dnt_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Depuis le contexte du type de pièces  ${dnt_code}
    ${synchro_metadonnee} =  Get Mandatory Value  css=#synchro_metadonnee
    Should Be equal  ${synchro_metadonnee}  t

    # On supprime un type
    ${dnt2_code} =  Set Variable  DC
    &{dnt_values} =  Create Dictionary
    ...  code=${dnt2_code}
    ...  libelle=Document numérisé
    ...  document_numerise_type_categorie=${dntc_libelle}
    Ajouter le type de pièces  ${dnt_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Supprimer le type de pièces  ${dnt2_code}
    Valid Message Should Contain  La suppression a été correctement effectuée.


Ajout de pièces

    [Documentation]  Vérifie que l'ajout de 2 pièces ayant la même date et le même type
    ...  crée bien les fichiers avec un suffixe.
    ...  On vérifie également que le type de pièce n'est pas visible pour un instructeur
    ...  si l'option n'est pas activé pour ce type de pièce.

    # On ajoute une catégorie car c'est un champ obligatoire pour les types
    Depuis la page d'accueil  admin  admin
    ${dntc_libelle} =  Set Variable  Le roi
    &{dntc_values} =  Create Dictionary
    ...  libelle=${dntc_libelle}
    Ajouter la catégorie de pièces  ${dntc_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # On ajoute un type, on vérifie que les cases à cocher 'aff_da',
    #'aff_service_consulte' soient déjà cochées et que le
    # champ 'synchro_metadonnee' non visible soit à false
    ${dnt_code} =  Set Variable  LR01
    ${type_libelle_dict} =  Create List
    ...  Document très important
    &{dnt_values} =  Create Dictionary
    ...  code=${dnt_code}
    ...  libelle=${type_libelle_dict}
    ...  document_numerise_type_categorie=${dntc_libelle}
    Ajouter le type de pièces  ${dnt_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    #
    Depuis la page d'accueil  instr  instr

    # On vérifie le message en cas d'un listing vide
    Depuis l'onglet des pièces du dossier d'instruction  ${di}
    Element Should Contain  css=#sousform-document_numerise  Aucun enregistrement

    # Données de la pièce
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=15/09/2015
    ...  document_numerise_type=vues et coupes du projet dans le profil du terrain naturel

    # Décomposition de l'ajout de pièce pour vérifier que le type de pièce créé précédemment
    # n'est pas visible par l'instructeur
    Depuis l'onglet des pièces du dossier d'instruction  ${di}
    #
    Wait Until Element Is Visible  id=action-soustab-blocnote-message-ajouter
    Click Element  id=action-soustab-blocnote-message-ajouter
    Wait Until Element Is Visible  document_numerise_type
    Select List Should Not Contain List  document_numerise_type  ${type_libelle_dict}
    Saisir la pièce  ${document_numerise_values}
    # On valide le formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.

    # On vérifie qu'il n'y ait pas de confirmation de transmission ERP
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=#sformulaire div.message p span.text  Le message a été transmis au référentiel ERP.

    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=10/09/2016
    ...  document_numerise_type=arrêté retour préfecture
    Ajouter une pièce depuis le dossier d'instruction  ${di}  ${document_numerise_values}

    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=10/09/2016
    ...  document_numerise_type=arrêté retour préfecture
    Ajouter une pièce depuis le dossier d'instruction  ${di}  ${document_numerise_values}

    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=10/09/2016
    ...  document_numerise_type=arrêté retour préfecture
    Ajouter une pièce depuis le dossier d'instruction  ${di}  ${document_numerise_values}
    Click On Back Button In Subform

    Element Should Contain  css=#sousform-document_numerise  20150915DGPA05.pdf
    Element Should Contain  css=#sousform-document_numerise  20160910ART.pdf
    Element Should Contain  css=#sousform-document_numerise  20160910ART-1.pdf
    Element Should Contain  css=#sousform-document_numerise  20160910ART-2.pdf

    # On vérifie le contrôle d'extension lors de l'upload d'un fichier
    Depuis l'onglet des pièces du dossier d'instruction  ${di}
    # On clique sur l'action d'ajout
    Wait Until Element Is Visible  id=action-soustab-blocnote-message-ajouter
    Click Element  id=action-soustab-blocnote-message-ajouter
    Wait Until Element Is Visible  uid_upload
    # On vérifie qu'une image ne peut pas être uploadée
    Add File and Expect Error Message Contain  uid  testImportManuel.jpg  Le fichier n'est pas conforme à la liste des extension(s) autorisée(s)


Modification d'une pièce

    [Documentation]  Modifie une pièce et vérifie que son nom est régénéré.

    # Données de la pièce
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel2.pdf
    ...  date_creation=20/09/2015
    #
    Depuis la page d'accueil  instrpoly  instrpoly
    #
    Modifier une pièce depuis le dossier d'instruction  ${di}  vues et coupes du projet dans le profil du terrain naturel  ${document_numerise_values}

    # Récupération de l'UID utilisé dans le test de suppression de la pièce
    Depuis le contexte de la pièce par le dossier d'instruction  ${di}  vues et coupes du projet dans le profil du terrain naturel
    Click On Subform Portlet Action  document_numerise  modifier
    ${document_numerise_uid} =  Get Value  uid
    Set Suite Variable  ${document_numerise_uid}
    #
    Depuis le contexte du dossier d'instruction  ${di}
    On clique sur l'onglet  document_numerise  Pièce(s)
    # On clique pour visualiser le document, le nom doit avoir été modifié par
    # rapport à la date
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=tr.col3 td.firstcol a.lienTable span.reqmo-16
    Open PDF  file
    # On vérifie le contenu du PDF
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  TEST IMPORT MANUEL 2
    # On ferme le PDF
    Close PDF


Vérification de l'affichage sur le dossier d'autorisation

    [Documentation]  Vérifie l'affichage sur les dossiers d'autorisation.

    # On ajoute un type de pièce non affiché sur les DA
    Depuis la page d'accueil  admin  admin
    ${dntc_libelle} =  Set Variable  Non visible au public
    &{dntc_values} =  Create Dictionary
    ...  libelle=${dntc_libelle}
    Ajouter la catégorie de pièces  ${dntc_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${dnt_code} =  Set Variable  NVPLAN
    ${dnt_libelle} =  Set Variable  Plan non public
    ${instructeur_qualite} =  Create List
    ...  instructeur
    &{dnt_values} =  Create Dictionary
    ...  code=${dnt_code}
    ...  libelle=${dnt_libelle}
    ...  document_numerise_type_categorie=${dntc_libelle}
    ...  aff_da=false
    ...  instructeur_qualite=${instructeur_qualite}
    Ajouter le type de pièces  ${dnt_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # On ajoute une pièce numérisée sur le DI vérifié
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=21/09/2015
    ...  document_numerise_type=${dnt_libelle}
    Ajouter une pièce depuis le dossier d'instruction  ${di}  ${document_numerise_values}

    # On récupère le numéro du dossier d'autorisation depuis le numéro du DI
    ${dossier_autorisation} =  Get Substring  ${di}  0  -2
    #
    Depuis la page d'accueil  guichet  guichet
    #
    Depuis l'onglet des pièces du dossier d'autorisation  ${dossier_autorisation}
    # On vérifie que le numéro du dossier d'instruction est affiché
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${di}
    # On vérifie que le nom du fichier est affiché
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  20150920DGPA05.pdf
    # On vérifie que la pièce n'est pas affiché
    Page Should Not Contain  20150921NVPLAN.pdf
    # On clique pour visualiser le document
    Click Element  css=tr.col4 td.col-1 a.lienTable span.reqmo-16
    Open PDF  file
    # On vérifie la localisation du terrain
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  TEST IMPORT MANUEL 2
    # On ferme le PDF
    Close PDF


Vérification de l'affichage sur la demande d'avis

    [Documentation]  Vérifie l'affichage sur les demandes d'avis.

    # On ajoute un type de pièce non affiché sur les demandes d'avis
    Depuis la page d'accueil  admin  admin
    ${dntc_libelle} =  Set Variable  Top secret
    &{dntc_values} =  Create Dictionary
    ...  libelle=${dntc_libelle}
    Ajouter la catégorie de pièces  ${dntc_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${dnt_code} =  Set Variable  TSPLAN
    ${dnt_libelle} =  Set Variable  Plan top secret
    ${instructeur_qualite} =  Create List
    ...  instructeur
    &{dnt_values} =  Create Dictionary
    ...  code=${dnt_code}
    ...  libelle=${dnt_libelle}
    ...  document_numerise_type_categorie=${dntc_libelle}
    ...  aff_service_consulte=false
    ...  instructeur_qualite=${instructeur_qualite}
    Ajouter le type de pièces  ${dnt_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # On ajoute une pièce numérisée sur le DI vérifié
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=21/09/2015
    ...  document_numerise_type=${dnt_libelle}
    Ajouter une pièce depuis le dossier d'instruction  ${di}  ${document_numerise_values}

    #
    Depuis la page d'accueil  consu  consu
    #
    Depuis l'onglet des pièces de la demande d'avis en cours du dossier d'instruction  ${di}
    # On vérifie que le nom du fichier est affiché
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  20150920DGPA05.pdf
    # On vérifie que la pièce n'est pas affiché
    Page Should Not Contain  20150921TSPLAN.pdf
    # On vérifie que la pièce n'est pas affiché
    Page Should Not Contain  20150921TSPLAN.pdf
    # On clique pour visualiser le document
    Click Element  css=tr.col3 td.firstcol a.lienTable span.reqmo-16
    Open PDF  file
    # On vérifie la localisation du terrain
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  TEST IMPORT MANUEL 2
    # On ferme le PDF
    Close PDF


Suppression d'une pièce

    [Documentation]  Vérifie dans le filestorage si le fichier du document numérisé est
    ...  correctement supprimé lors de la suppression d'une pièce

    ${path_1} =  Get Substring  ${document_numerise_uid}  0  2
    ${path_2} =  Get Substring  ${document_numerise_uid}  0  4
    # Vérification dans le filestorage
    File Should Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${document_numerise_uid}
    File Should Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${document_numerise_uid}.info
    #
    Depuis la page d'accueil  instrpoly  instrpoly
    #
    Supprimer une pièce depuis le dossier d'instruction  ${di}  vues et coupes du projet dans le profil du terrain naturel
    # Vérification dans le filestorage
    File Should Not Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${document_numerise_uid}
    File Should Not Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${document_numerise_uid}.info


TNR Ajout de pièces au DI en tant qu'instructeur

    [Documentation]  L'utilisation d'un getval faisait qu'un dossier au hasard était
    ...  récupéré lors de l'ajout de pièces, quand ce dossier était clôturé l'ajout de
    ...  pièces produisait une erreur de droits insuffisants pour les instructeurs.

    # On crée une nouvelle demande pour le TNR
        &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Riel
    ...  particulier_prenom=Sébastien
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    # Le bug provenait du fait que l'on instanciait à tort le premier document
    # numérisé créé en base de données. Celui-ci est lié au AZ 013055 12 00001P0.
    # Ainsi on testait toujours ce DI pour savoir s'il était clôturé et donc si
    # on avait le droit ou non d'ajouter une pièce.
    Depuis la page d'accueil  instrpoly  instrpoly
    Ajouter une instruction au DI  AZ 013055 12 00001P0  ARRÊTÉ DE REFUS
    Click On Back Button In Subform
     # En cloturant le AZ 013055 12 00001P0 on reproduit le use case.
    Click On Link  ARRÊTÉ DE REFUS
    Click On SubForm Portlet Action  instruction  finaliser
    Click On SubForm Portlet Action  instruction  definaliser
    Click On SubForm Portlet Action  instruction  modifier
    Input Datepicker  date_retour_rar  ${date_ddmmyyyy}
    Click On Submit Button In Subform

    # On teste l'ajout de pièces sur le DI de test en tant qu'instructeur
    # Cela doit fonctionner bien que le AZ 013055 12 00001P0 soit clôturé
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=15/09/2015
    ...  document_numerise_type=vues et coupes du projet dans le profil du terrain naturel
    Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    # On supprime les événements d'instruction créés spécifiquement pour le TNR
    Depuis la page d'accueil  admin  admin
    Supprimer l'instruction  AZ 013055 12 00001P0  Arrêté de Refus signé
    Supprimer l'instruction  AZ 013055 12 00001P0  ARRÊTÉ DE REFUS


TNR Vérification des métadonnées des fichiers

    [Documentation]  Vérifie les métadonnées des fichiers créé par
    ...  l'application.

    # On crée une nouvelle demande pour le TNR
        &{args_petitionnaire} =  Create Dictionary
    ...  particulier_civilite=Monsieur
    ...  particulier_nom=Dufresne
    ...  particulier_prenom=Thierry
    ...  om_collectivite=MARSEILLE
    #
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    #
    ${di_metadata} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    ${libelle_di_metadata} =  Sans espace  ${di_metadata}

    # On ajoute une pièce sur le dossier d'instruction initial
    Depuis la page d'accueil  admin  admin
    # Données de la pièce
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  document_numerise_type=arrêté
    #
    Ajouter une pièce depuis le dossier d'instruction  ${di_metadata}  ${document_numerise_values}
    # On récupére l'UID de la pièce pour définir les chemins
    Depuis le contexte de la pièce par le dossier d'instruction  ${di_metadata}  arrêté
    Click On Subform Portlet Action  document_numerise  modifier
    ${uid} =  Get Value  uid
    ${path_1} =  Get Substring  ${uid}  0  2
    ${path_2} =  Get Substring  ${uid}  0  4
    # On vérifie les métadonnées depuis le fichier ".info" dans le filesystem
    ${file_info} =  Get File  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}.info
    Should Contain  ${file_info}  dossier=${libelle_di_metadata}
    Should Contain  ${file_info}  dossier_version=0
    Should Contain  ${file_info}  typeInstruction=P

    # On accepte le dossier d'instruction initial
    Ajouter une instruction au DI  ${di_metadata}  accepter un dossier sans réserve

    # On ajoute un modificatif sur le dossier d'instruction
    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande de modification
    ...  dossier_instruction=${di_metadata}
    #
    ${di_metadata_1} =  Ajouter la demande par WS  ${args_demande}
    # On récupère le numéro du dossier sans espace
    ${libelle_di_metadata_1} =  Sans espace  ${di_metadata_1}
    # On ajoute une pièce sur le dossier d'instruction de modification 1
    Depuis la page d'accueil  admin  admin
    # Données de la pièce
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  document_numerise_type=arrêté
    #
    Ajouter une pièce depuis le dossier d'instruction  ${di_metadata_1}  ${document_numerise_values}
    # On récupére l'UID de la pièce pour définir les chemins
    Depuis le contexte de la pièce par le dossier d'instruction  ${di_metadata_1}  arrêté
    Click On Subform Portlet Action  document_numerise  modifier
    ${uid} =  Get Value  uid
    ${path_1} =  Get Substring  ${uid}  0  2
    ${path_2} =  Get Substring  ${uid}  0  4
    # On vérifie les métadonnées depuis le fichier ".info" dans le filesystem
    ${file_info} =  Get File  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}.info
    Should Contain  ${file_info}  dossier=${libelle_di_metadata_1}
    Should Contain  ${file_info}  dossier_version=01
    Should Contain  ${file_info}  typeInstruction=M

    # On accepte le dossier d'instruction de modification 1
    Ajouter une instruction au DI  ${di_metadata_1}  accepter un dossier sans réserve

    #
    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande de modification
    ...  dossier_instruction=${di_metadata_1}
    #
    ${di_metadata_2} =  Ajouter la demande par WS  ${args_demande}
    # On récupère le numéro du dossier sans espace
    ${libelle_di_metadata_2} =  Sans espace  ${di_metadata_2}
    # On ajoute une pièce sur le dossier d'instruction de modification 2
    # Données de la pièce
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  document_numerise_type=arrêté
    #
    Ajouter une pièce depuis le dossier d'instruction  ${di_metadata_2}  ${document_numerise_values}
    # On récupére l'UID de la pièce pour définir les chemins
    Depuis le contexte de la pièce par le dossier d'instruction  ${di_metadata_2}  arrêté
    Click On Subform Portlet Action  document_numerise  modifier
    ${uid} =  Get Value  uid
    ${path_1} =  Get Substring  ${uid}  0  2
    ${path_2} =  Get Substring  ${uid}  0  4
    # On vérifie les métadonnées depuis le fichier ".info" dans le filesystem
    ${file_info} =  Get File  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}.info
    Should Contain  ${file_info}  dossier=${libelle_di_metadata_2}
    Should Contain  ${file_info}  dossier_version=02
    Should Contain  ${file_info}  typeInstruction=M

    # On ajoute une pièce sur le dossier d'instruction de modification 1
    Depuis la page d'accueil  admin  admin
    # Données de la pièce
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  document_numerise_type=arrêté de conformité
    #
    Ajouter une pièce depuis le dossier d'instruction  ${di_metadata_1}  ${document_numerise_values}
    # On récupére l'UID de la pièce pour définir les chemins
    Depuis le contexte de la pièce par le dossier d'instruction  ${di_metadata_1}  arrêté de conformité
    Click On Subform Portlet Action  document_numerise  modifier
    ${uid} =  Get Value  uid
    ${path_1} =  Get Substring  ${uid}  0  2
    ${path_2} =  Get Substring  ${uid}  0  4
    # On vérifie les métadonnées depuis le fichier ".info" dans le filesystem
    ${file_info} =  Get File  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}.info
    Should Contain  ${file_info}  dossier=${libelle_di_metadata_1}
    Should Contain  ${file_info}  dossier_version=01
    Should Contain  ${file_info}  typeInstruction=M


Téléchargement de l'intégralité des pièces

    [Documentation]  Contrôle que l'action "télécharger toutes les pièces" dans l'onglet
    ...  Pièce(s) d'un dossier d'instruction, dans l'onglet Pièce(s) du DA et les demandes
    ...  d'avis produit bien une archive téléchargeable contenant toutes les pièces.
    ...  On ajoute plusieurs fois le même type de pièce le même jour pour vérifier que les
    ...  fichiers sont bien suffixés.

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Compagnon
    ...  particulier_prenom=Émilie
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  admin  admin

    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=31/03/2016
    ...  document_numerise_type=avis obligatoires
    Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=31/03/2016
    ...  document_numerise_type=avis obligatoires
    Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=31/03/2016
    ...  document_numerise_type=avis obligatoires
    Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel2.pdf
    ...  date_creation=30/03/2016
    ...  document_numerise_type=avis obligatoires
    Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    # Pièce de type "arrêté retour prefecture" qui sera disponible depuis le DA
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=31/03/2016
    ...  document_numerise_type=arrêté retour préfecture
    Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    Depuis la page d'accueil  instr  instr
    # Ajout d'une consultation au dossier pour que consu puisse accéder aux pièces
    Ajouter une consultation depuis un dossier  ${di_libelle}  59.01 - Direction de l'Eau et de l'Assainissement

    Depuis l'onglet des pièces du dossier d'instruction  ${di_libelle}
    ${path_archive}  ${archive_name} =  Télécharger toutes les pièces

    # Composition du nom correct de l'archive zip
    ${di_libelle_spaceless} =  Sans espace  ${di_libelle}
    ${date_ddmmyyyy} =  Date du jour EN
    ${date_jour_sans_tirets} =  STR_REPLACE  -  ${EMPTY}  ${date_ddmmyyyy}
    ${correct_archive_name_di} =  Set Variable  ${di_libelle_spaceless}_${date_jour_sans_tirets}.zip

    # Vérification du nom de l'archive, qui doit commencer par le n° de DI
    Should Be Equal  ${correct_archive_name_di}  ${archive_name}
    # L'archive doit contenir les 3 pièces du DI
    Archive Should Contain File  ${path_archive}  20160331AVIS.pdf
    Archive Should Contain File  ${path_archive}  20160331AVIS-1.pdf
    Archive Should Contain File  ${path_archive}  20160331AVIS-2.pdf
    Archive Should Contain File  ${path_archive}  20160330AVIS.pdf
    Archive Should Contain File  ${path_archive}  20160331ART.pdf

    # Récupération de l'archive dans le contexte du DA du DI utilisé précédemment
    ${da_libelle} =  Get Substring  ${di_libelle}  0  -2
    ${da_libelle_spaceless} =  Sans espace  ${da_libelle}
    ${correct_archive_name_da} =  Set Variable  ${da_libelle_spaceless}_${date_jour_sans_tirets}.zip

    Depuis l'onglet des pièces du dossier d'autorisation  ${da_libelle}
    ${path_archive}  ${archive_name} =  Télécharger toutes les pièces

    # Le nom de l'archive doit commencer par le numéro de DA (sans le P0 du DI)
    Should Be Equal  ${correct_archive_name_da}  ${archive_name}
    Archive Should Contain File  ${path_archive}  20160331AVIS.pdf
    Archive Should Contain File  ${path_archive}  20160331AVIS-1.pdf
    Archive Should Contain File  ${path_archive}  20160331AVIS-2.pdf
    Archive Should Contain File  ${path_archive}  20160330AVIS.pdf
    Archive Should Contain File  ${path_archive}  20160331ART.pdf

    # Récupération de l'archive depuis la demande d'avis en cours
    Depuis la page d'accueil  consu  consu
    Depuis la demande d'avis en cours du dossier  ${di_libelle}
    On clique sur l'onglet  document_numerise  Pièce(s)

    ${path_archive}  ${archive_name} =  Télécharger toutes les pièces

    Should Be Equal  ${correct_archive_name_di}  ${archive_name}
    Archive Should Contain File  ${path_archive}  20160331AVIS.pdf
    Archive Should Contain File  ${path_archive}  20160331AVIS-1.pdf
    Archive Should Contain File  ${path_archive}  20160331AVIS-2.pdf
    Archive Should Contain File  ${path_archive}  20160330AVIS.pdf
    Archive Should Contain File  ${path_archive}  20160331ART.pdf

    # On rend l'avis sur la consultation pour qu'elle devienne "passée"
    On clique sur l'onglet  main  Demandes D'avis En Cours
        &{args_avis_consultation} =  Create Dictionary
    ...  avis_consultation=Favorable
    ...  motivation=Pas de réserves
    ...  fichier_upload=testImportManuel.pdf

    Rendre l'avis sur la consultation du dossier  ${di_libelle}  ${args_avis_consultation}
    Depuis la demande d'avis passée du dossier  ${di_libelle}
    On clique sur l'onglet  document_numerise  Pièce(s)

    ${path_archive}  ${archive_name} =  Télécharger toutes les pièces

    Should Be Equal  ${correct_archive_name_di}  ${archive_name}
    Archive Should Contain File  ${path_archive}  20160331AVIS.pdf
    Archive Should Contain File  ${path_archive}  20160331AVIS-1.pdf
    Archive Should Contain File  ${path_archive}  20160331AVIS-2.pdf
    Archive Should Contain File  ${path_archive}  20160330AVIS.pdf
    Archive Should Contain File  ${path_archive}  20160331ART.pdf


Vérification du message de notification à l'ajout d'une pièce numérisée

    [Documentation]  Vérification des différents cas concernant la notification
    ...  par message à l'ajout de pièce numérisée.


    # On ajoute un instructeur de la même division que instrpolycomm2
    ${utilisateur_nom} =  Set Variable  Patricia O''Maley
    Depuis la page d'accueil  admin  admin
    Ajouter l'utilisateur  ${utilisateur_nom}  nospam@openmairie.org  pomaley  pomaley  INSTRUCTEUR POLYVALENT  MARSEILLE
    Ajouter l'instructeur depuis le menu  ${utilisateur_nom}  subdivision J  instructeur  ${utilisateur_nom}

    # On ajoute un DI
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Rousseau
    ...  particulier_prenom=Matilda
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Déclaration préalable
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    #
    # Cas n°1
    #

    # On se connecte avec l'instructeur affecté au dossier pour ajouter une
    # pièce
    Depuis la page d'accueil  instrpolycomm2  instrpolycomm2
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=${date_ddmmyyyy}
    ...  document_numerise_type=autres pièces composant le dossier (A0)
    ${dossier_message_1} =  Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    # On vérifie que le message est déjà marqué comme lu
    Depuis l'onglet des messages du dossier d'instruction  ${di_libelle}
    Total Results In Subform Should Be Equal  1  dossier_message
    Depuis le contexte du message dans le dossier d'instruction  ${di_libelle}  ${dossier_message_1}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Static Value Should Be  lu  Oui

    #
    # Cas n°2
    #

    # On ajoute une nouvelle pièce sur le même dossier avec le même utilisateur
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=${date_ddmmyyyy}
    ...  document_numerise_type=autres pièces composant le dossier (A3/A4)
    ${dossier_message_2} =  Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    # On vérifie qu'il n'y a pas de message ajouté
    Should Be Empty  ${dossier_message_2}
    Depuis l'onglet des messages du dossier d'instruction  ${di_libelle}
    Total Results In Subform Should Be Equal  1  dossier_message

    #
    # Cas n°3
    #

    # On se connecte avec un instructeur qui n'est pas affecté au dossier mais
    # de la même division
    Depuis la page d'accueil  pomaley  pomaley
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=${date_ddmmyyyy}
    ...  document_numerise_type=autres pièces composant le dossier délivré (A0)
    ${dossier_message_3} =  Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    # On vérifie que le message est marqué comme non lu
    Depuis l'onglet des messages du dossier d'instruction  ${di_libelle}
    Total Results In Subform Should Be Equal  2  dossier_message
    Depuis le contexte du message dans le dossier d'instruction  ${di_libelle}  ${dossier_message_3}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Static Value Should Be  lu  Non

    #
    # Cas n°4
    #

    # On ajoute une nouvelle pièce avec l'instructeur qui n'est pas affecté au
    # dossier mais de la même division
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=${date_ddmmyyyy}
    ...  document_numerise_type=autres pièces composant le dossier délivré (A3/A4)
    ${dossier_message_4} =  Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    # On vérifie qu'il n'y a pas de message ajouté
    Should Be Empty  ${dossier_message_4}
    Depuis l'onglet des messages du dossier d'instruction  ${di_libelle}
    Total Results In Subform Should Be Equal  2  dossier_message

    #
    # Cas n°5
    #

    # On marque comme lu le message du précédent dépôt de pièce
    Marquer comme lu le message dans le dossier d'instruction  ${di_libelle}  ${dossier_message_3}

    # On ajoute une nouvelle pièce avec l'instructeur qui n'est pas affecté au
    # dossier mais de la même division
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=${date_ddmmyyyy}
    ...  document_numerise_type=avis obligatoires
    ${dossier_message_5} =  Ajouter une pièce depuis le dossier d'instruction  ${di_libelle}  ${document_numerise_values}

    # On vérifie que le message est marqué comme non lu
    Depuis l'onglet des messages du dossier d'instruction  ${di_libelle}
    Total Results In Subform Should Be Equal  3  dossier_message
    Depuis le contexte du message dans le dossier d'instruction  ${di_libelle}  ${dossier_message_5}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Static Value Should Be  lu  Non


Traitement des métadonnées

    [Documentation]  Vérification du traitement des métadonnées.

    #
    # Cas n°1 : Premier traitement, tous les types de pièces doivent avoir le
    #           flag de modification à 'true'. Les fichiers déjà existants
    #           doivent avoir les deux nouvelles métadonnées 'aff_da' et
    #           'aff_sc'.
    #
    Depuis la page d'accueil  admin  admin

    @{md_no} =  Create List
    ...  consultationPublique
    ...  consultationTiers

    # Les 2 fichiers sont présents dans le jeu de données et copiés par om_tests
    ${doc01_fichier_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  483cf5c504c9f81a7c7f470c5a209140
    ${doc02_fichier_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  79d433ed40812262504c289980960f18
    ${doc03_fichier_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  891807ffed15ac8fd09bc1032760017b

    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${doc01_fichier_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${doc02_fichier_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${doc03_fichier_path}

    Mise à jour des métadonnées
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Le traitement s'est correctement déroulé.

    # On vérifie les métadonnées du fichier
    ${md} =  Create Dictionary
    ...  consultationPublique=true
    ...  consultationTiers=true
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc01_fichier_path}
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc02_fichier_path}
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc03_fichier_path}

    #
    # Cas n°2 : Il n'y a aucune modification.
    #
    Mise à jour des métadonnées
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Il n'y a aucun type de pièces modifié.
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc01_fichier_path}
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc02_fichier_path}
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc03_fichier_path}

    #
    # Cas n°3 : Modification du paramètre 'aff_da' d'un type de pièces, on
    #           vérifie que la métadonnée sur le fichier de ce type a
    #           correctement été modifiée.
    #
    # On modifie un type de pièces
    ${dnt_code} =  Set Variable  ART
    &{dnt_values} =  Create Dictionary
    ...  aff_da=false
    Modifier le type de pièces  ${dnt_code}  ${dnt_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    #
    Mise à jour des métadonnées
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Le traitement s'est correctement déroulé.
    # On vérifie les métadonnées du fichier
    ${md} =  Create Dictionary
    ...  consultationPublique=false
    ...  consultationTiers=true
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc02_fichier_path}
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc03_fichier_path}

    #
    # Cas n°4 : Modification du paramètre 'aff_da' d'un type de pièces non utilisé
    #

    ${dnt_code} =  Set Variable  RDA
    &{dnt_values} =  Create Dictionary
    ...  aff_da=false
    Modifier le type de pièces  ${dnt_code}  ${dnt_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    #
    Mise à jour des métadonnées
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Il n'y a aucun fichier dont les métadonnées doivent être mises à jour.
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc01_fichier_path}
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc02_fichier_path}
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${doc03_fichier_path}

    #
    # Cas n°5 : Test du fonctionnement normal du web service de maj des métadonnées, puis
    #           suppression d'un document du filestorage alors qu'il est toujours en base.
    #           La mise à jour des métadonnées doit afficher qu'un document est en erreur.
    #
    Remove Directory  ../var/filestorage/79/79d4  true
    # On modifie un type de pièces
    ${dnt_code} =  Set Variable  ART
    &{dnt_values} =  Create Dictionary
    ...  aff_da=true
    Modifier le type de pièces  ${dnt_code}  ${dnt_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Mise à jour des métadonnées
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Le traitement s'est correctement déroulé, sauf pour les pièces numérisées ci-dessous :
    Valid Message Should Contain  Dossier d'instruction n°AZ0130551200001P0 : le document 20160919ART.pdf n'a pas pu être mis à jour.

    # Test du web service de mise à jour de toutes les pièces
    ${json} =  Set Variable  {"module":"maj_metadonnees_documents_numerises"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Tous les documents ont été traités.

    # On modifie le type de pièces pour que le traitement s'effectue de nouveau
    &{dnt_values} =  Create Dictionary
    ...  aff_da=false
    Modifier le type de pièces  ${dnt_code}  ${dnt_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # Suppression du document numérisé sur le filesystem
    Remove Directory  var/digitalization/b/b120

    # Test du web service de mise à jour de toutes les pièces
    ${json} =  Set Variable  { "module": "maj_metadonnees_documents_numerises"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Liste des fichiers en erreur : Dossier d'instruction n°AZ0130551200001P0 - le document 20160919ART.pdf n'a pas pu être mis à jour


Configuration des méthodes de traitement sur les métadonnées des fichiers liés aux dossiers d'instruction
    [Documentation]  Vérification de l'exécution des méthodes de traitements sur
    ...  les métadonnées des fichiers liés aux dossiers d'instruction,
    ...  configurées depuis le connecteur du filestorage.

    # On ajoute le DI depuis lequel on va vérifier la mise à jour des
    # métadonnées
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Duffet
    ...  particulier_prenom=Felicien
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    # On ajoute un fichier de chaque lien possible avec le DI :
    # - l'instruction est ajoutée automatiquement grâce au récépissé de la demande ;
    # - une pièce numérisée ;
    # - une consultation et le fichier joint au rendu d'avis ;
    # - le rapport d'instruction
    Depuis la page d'accueil  instrpoly  instrpoly
    &{args_dn} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  document_numerise_type=avis obligatoires
    Ajouter une pièce depuis le dossier d'instruction  ${di}  ${args_dn}
    Ajouter une consultation depuis un dossier  ${di}  59.01 - Direction de l'Eau et de l'Assainissement
    Depuis la page d'accueil  consu  consu
    &{args_ac} =  Create Dictionary
    ...  avis_consultation=Favorable
    ...  motivation=Pas de réserves
    ...  fichier_upload=testImportManuel.pdf
    Rendre l'avis sur la consultation du dossier  ${di}  ${args_ac}
    Depuis la page d'accueil  instrpoly  instrpoly
    &{args_ri} =  Create Dictionary
    ...  description_projet_om_html=Description du projet
    Ajouter et finaliser le rapport d'instruction  ${di}  ${args_ri}

    # On récupère les chemins de chaque fichier info dont le l'uid est accessible depuis
    # le DOM
    ${dn_info_path} =  Récupérer le chemin du fichier .info de la pièce stocké  ${di}  avis obligatoires
    ${consultation_fj_info_path} =  Récupérer le chemin du fichier .info du fichier joint de la consultation  ${di}  59.01 - Direction de l'Eau et de l'Assainissement
    @{list_path}  Create List  ${dn_info_path}  ${consultation_fj_info_path}

    # On vérifie les métadonnées de chaque fichier afin de contrôler que la
    # métadonnée *concerneERP* à comme valeur 'false'
    :FOR  ${path}  IN  @{list_path}
    \  ${info} =  Get File  ${path}
    \  Should Contain  ${info}  concerneERP=false

    # On définit les deux modification de DI
    &{args_di_true} =  Create Dictionary
    ...  erp=true
    &{args_di_false} =  Create Dictionary
    ...  erp=false

    ##
    ## Sans la configuration du traitement des métadonnées dans le connecteur du
    ## filestorage
    ##

    # On coche le champ ERP du dossier d'instruction pour vérifier que la
    # métadonnée des fichiers n'est pas mise à jour (valeur 'false')
    Modifier le dossier d'instruction  ${di}  ${args_di_true}
    :FOR  ${path}  IN  @{list_path}
    \  ${info} =  Get File  ${path}
    \  Should Contain  ${info}  concerneERP=false
    Modifier le dossier d'instruction  ${di}  ${args_di_false}

    ##
    ## Avec la configuration du traitement des métadonnées dans le connecteur du
    ## filestorage, mais sans la méthode de traitement renseignée
    ##

    # On change la configuration du filestorage
    Move File  ..${/}dyn${/}filestorage.inc.php  ..${/}dyn${/}filestorage.inc.php.bak
    Copy File  ..${/}tests${/}binary_files${/}filestorage_1.inc.php  ..${/}dyn${/}
    Move File  ..${/}dyn${/}filestorage_1.inc.php  ..${/}dyn${/}filestorage.inc.php

    # On coche le champ ERP du dossier d'instruction pour vérifier que la
    # métadonnée des fichiers n'est pas mise à jour (valeur 'false')
    Modifier le dossier d'instruction  ${di}  ${args_di_true}
    :FOR  ${path}  IN  @{list_path}
    \  ${info} =  Get File  ${path}
    \  Should Contain  ${info}  concerneERP=false
    Modifier le dossier d'instruction  ${di}  ${args_di_false}

    ##
    ## Avec la configuration du traitement des métadonnées dans le connecteur du
    ## filestorage et la méthode de traitement renseignée
    ##

    # On change la configuration du filestorage
    Copy File  ..${/}tests${/}binary_files${/}filestorage_2.inc.php  ..${/}dyn${/}
    Move File  ..${/}dyn${/}filestorage_2.inc.php  ..${/}dyn${/}filestorage.inc.php

    # On coche le champ ERP du dossier d'instruction pour vérifier que la
    # métadonnée des fichiers est mise à jour (valeur 'true')
    Modifier le dossier d'instruction  ${di}  ${args_di_true}
    :FOR  ${path}  IN  @{list_path}
    \  ${info} =  Get File  ${path}
    \  Should Contain  ${info}  concerneERP=true

    # On modifie à nouveau le DI sans changer la valeur du champ ERP et on
    # contrôle que la métadonnée des fichiers n'est pas modifiée (valeur 'true')
    ${args_di} =  Create Dictionary
    Modifier le dossier d'instruction  ${di}  ${args_di}
    :FOR  ${path}  IN  @{list_path}
    \  ${info} =  Get File  ${path}
    \  Should Contain  ${info}  concerneERP=true

    # On modifie une dernière fois le DI en changeant la valeur du champ ERP et
    # on inspecte la métadonnée des fichiers (valeur 'false')
    Modifier le dossier d'instruction  ${di}  ${args_di_false}
    :FOR  ${path}  IN  @{list_path}
    \  ${info} =  Get File  ${path}
    \  Should Contain  ${info}  concerneERP=false

    # On remet la configuration du filestorage par défaut
    Move File  ..${/}dyn${/}filestorage.inc.php.bak  ..${/}dyn${/}filestorage.inc.php

