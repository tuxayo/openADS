*** Settings ***
Documentation  Test les demandeurs

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Champs de fusion de l'identité d'un demandeur
    [Documentation]  Ce test case vérifie qu'en plus du champ de fusion
    ...  concaténant plusieurs valeurs, ces dernières sont disponibles seules.
    ...  Seule l'édition des arrêtés est testée.

    ######################################################################
    #                              Prérequis                             #
    # * paramétrage de la lettre-type                                    #
    ######################################################################

    # On rajoute les nouveaux champs de fusion dans la lettre-type
    ${corps} =  catenate  SEPARATOR=
    ...  begin
    ...  __civilite_petitionnaire_principal_[civilite_petitionnaire_principal]
    ...  __nom_particulier_petitionnaire_principal_[nom_particulier_petitionnaire_principal]
    ...  __prenom_particulier_petitionnaire_principal_[prenom_particulier_petitionnaire_principal]
    ...  __raison_sociale_petitionnaire_principal_[raison_sociale_petitionnaire_principal]
    ...  __denomination_petitionnaire_principal_[denomination_petitionnaire_principal]
    ...  __end
    ...  begin
    ...  __civilite_petitionnaire_1_[civilite_petitionnaire_1]
    ...  __nom_particulier_petitionnaire_1_[nom_particulier_petitionnaire_1]
    ...  __prenom_particulier_petitionnaire_1_[prenom_particulier_petitionnaire_1]
    ...  __raison_sociale_petitionnaire_1_[raison_sociale_petitionnaire_1]
    ...  __denomination_petitionnaire_1_[denomination_petitionnaire_1]
    ...  __end
    ...  begin
    ...  __civilite_delegataire_[civilite_delegataire]
    ...  __nom_particulier_delegataire_[nom_particulier_delegataire]
    ...  __prenom_particulier_delegataire_[prenom_particulier_delegataire]
    ...  __raison_sociale_delegataire_[raison_sociale_delegataire]
    ...  __denomination_delegataire_[denomination_delegataire]
    ...  __end
    Depuis la page d'accueil  admingen  admingen
    Modifier la lettre-type XXX
    ...  arrete
    ...  null
    ...  null
    ...  ${corps}

    ######################################################################
    #                              Cas 1/2                               #
    # * particulier en pétitionnaire principal                           #
    # * particulier en pétitionnaire 1                                   #
    ######################################################################

    # On crée une nouvelle demande pour le test case
    &{dossier_case_1} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    &{petitionnaire_principal_case_1} =  Create Dictionary
    ...  particulier_nom=Planck
    ...  particulier_prenom=Hubert
    ...  particulier_civilite=Monsieur
    ...  om_collectivite=MARSEILLE
    &{petitionnaire_1_case_1} =  Create Dictionary
    ...  particulier_nom=Planck
    ...  particulier_prenom=Rosy
    ...  particulier_civilite=Madame
    ${di_case_1} =  Ajouter la demande par WS
    ...  ${dossier_case_1}
    ...  ${petitionnaire_principal_case_1}

    # On ajoute un pétitionnaire
    Depuis la page d'accueil  instr  instr
    Depuis le formulaire de modification du dossier d'instruction  ${di_case_1}
    Open Fieldset    dossier_instruction    demandeur
    Ajouter le demandeur  petitionnaire  ${petitionnaire_1_case_1}
    Click On Submit Button

    # On crée un arrêté sur ce nouveau dossier
    Ajouter une instruction au DI et la finaliser  ${di_case_1}  ARRÊTÉ DE REFUS

    # On édite l'arrêté
    Click On SubForm Portlet Action  instruction  edition
    Open PDF  file
    # On vérifie les champs de fusion
    ${expected_case_1_principal} =  catenate  SEPARATOR=
    ...  begin
    ...  __civilite_petitionnaire_principal_Monsieur
    ...  __nom_particulier_petitionnaire_principal_Planck
    ...  __prenom_particulier_petitionnaire_principal_Hubert
    ...  __raison_sociale_petitionnaire_principal_
    ...  __denomination_petitionnaire_principal_
    ...  __end
    ${expected_case_1_petitionnaire_1} =  catenate  SEPARATOR=
    ...  begin
    ...  __civilite_petitionnaire_1_Madame
    ...  __nom_particulier_petitionnaire_1_Planck
    ...  __prenom_particulier_petitionnaire_1_Rosy
    ...  __raison_sociale_petitionnaire_1_
    ...  __denomination_petitionnaire_1_
    ...  __end
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${expected_case_1_principal}
    Page Should Contain  ${expected_case_1_petitionnaire_1}
    Close PDF

    ######################################################################
    #                              Cas 2/2                               #
    # * personne morale en pétitionnaire principal                       #
    # * personne morale représentée par un particulier en délégataire    #
    ######################################################################

    # On crée une nouvelle demande pour le test case
    &{dossier_case_2} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    &{petitionnaire_principal_case_2} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=NG
    ...  personne_morale_raison_sociale=NEWGREEN
    ...  om_collectivite=MARSEILLE
    &{delegataire_case_2} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_nom=McFitch
    ...  personne_morale_prenom=John
    ...  personne_morale_raison_sociale=OLDRED
    ...  personne_morale_denomination=OR
    ${di_case_2} =  Ajouter la demande par WS
    ...  ${dossier_case_2}
    ...  ${petitionnaire_principal_case_2}

    # On ajoute un délégataire
    Depuis la page d'accueil  instr  instr
    Depuis le formulaire de modification du dossier d'instruction  ${di_case_2}
    Open Fieldset    dossier_instruction    demandeur
    Ajouter le demandeur  delegataire  ${delegataire_case_2}
    Click On Submit Button

    # On crée un arrêté sur ce nouveau dossier
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI et la finaliser  ${di_case_2}  ARRÊTÉ DE REFUS

    # On édite l'arrêté
    Click On SubForm Portlet Action  instruction  edition
    Open PDF  file
    # On vérifie les champs de fusion
    ${expected_case_2_principal} =  catenate  SEPARATOR=
    ...  begin
    ...  __civilite_petitionnaire_principal_
    ...  __nom_particulier_petitionnaire_principal_
    ...  __prenom_particulier_petitionnaire_principal_
    ...  __raison_sociale_petitionnaire_principal_NEWGREEN
    ...  __denomination_petitionnaire_principal_NG
    ...  __end
    ${expected_case_2_delegataire} =  catenate  SEPARATOR=
    ...  begin
    ...  __civilite_delegataire_
    ...  __nom_particulier_delegataire_McFitch
    ...  __prenom_particulier_delegataire_John
    ...  __raison_sociale_delegataire_OLDRED
    ...  __denomination_delegataire_OR
    ...  __end
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${expected_case_2_principal}
    Page Should Contain  ${expected_case_2_delegataire}
    Close PDF


TNR Filtre des pétitionnaires fréquents par collectivité dans le listing
    [Documentation]  Ce test case vérifie que lorsqu'on est sur une collectivité mono, la
    ...  liste des pétitionnaires fréquents affiche les pétitionnaires fréquents de la
    ...  commune de l'utilisateur, et ceux créés par une collectivité de niveau 2.
    ...  Un compte Agglo doit voir et accéder à tous les pétitionnaires fréquents.

    # En tant que guichetier collectivité Marseille
    Depuis la page d'accueil  guichetsuivi  guichetsuivi
    # Ajout d'un pétitionnaire fréquent sur Marseille
    &{args_petitionnaire_marseille} =  Create Dictionary
    ...  particulier_nom=Lebrun
    ...  particulier_prenom=Carole

    Go To Submenu  petitionnaire_frequent
    Ajouter le pétitionnaire fréquent depuis le menu pétitionnaire fréquent  ${args_petitionnaire_marseille}

    # On récupère l'identifiant du demandeur
    Depuis le contexte du pétitionnaire fréquent  Lebrun Carole
    ${demandeur_id} =  Get Value    demandeur

    # En tant qu'utilisateur de collectivité de niveau 2
    Depuis la page d'accueil  admin  admin
    Go to Submenu In Menu  guichet_unique  petitionnaire_frequent
    # On doit pas avoir le pétitionnaire fréquent de Marseille
    Page Should Contain  Lebrun Carole
    # On doit pouvoir accéder au pétitionnaire
    Click On Link  Lebrun Carole
    Page Should Not Contain Errors
    Page Should Contain  Lebrun
    Page Should Contain  Carole
    Page Should Not Contain  Personne Morale
    Click On Back Button

    # Ajout d'un pétitionnaire fréquent sur collectivité Agglo
    &{args_petitionnaire_agglo} =  Create Dictionary
    ...  particulier_nom=Bélanger
    ...  particulier_prenom=Jeannine
    ...  om_collectivite=agglo

    Ajouter le pétitionnaire fréquent depuis le menu pétitionnaire fréquent  ${args_petitionnaire_agglo}

    # En tant qu'instructeur d'Allauch
    Depuis la page d'accueil  instrpolycomm3  instrpolycomm3
    Go to Submenu In Menu  guichet_unique  petitionnaire_frequent
    # On ne doit pas avoir les pétitionnaires fréquents de Marseille
    Page Should Not Contain  Lebrun Carole
    # On doit avoir le pétitionnaire fréquent de l'Agglo
    Page Should Contain  Bélanger Jeannine

    # On vérifie que l'utilisateur d'Allauch ne peut pas accéder au pétitionnaire fréquent
    # de Marseille
    Depuis le tableau des pétitionnaires fréquents
    Page Should Not Contain  Lebrun Carole
    # On vérifie que l'utilisateur d'Allauch ne peut pas accéder au pétitionnaire fréquent
    # de Marseille depuis l'URL
    ${URL} =  Set Variable  ${PROJECT_URL}/scr/form.php?obj=petitionnaire_frequent&action=3&idx=${demandeur_id}
    Go To  ${URL}
    # L'URL doit afficher une erreur
    Error Message Should Contain  Droits insuffisants.

    # On vérifie que l'utilisateur d'Allauch peut accéder au pétitionnaire fréquent multi
    Depuis le tableau des pétitionnaires fréquents
    Click On Link  Bélanger Jeannine
    Element Text Should Be  particulier_nom  Bélanger


Création et recherche de pétitionnaires fréquents sur plusieurs collectivités
    [Documentation]  L'objet de ce 'Test Case' est de vérifier que la recherche fonctionne
    ...  en contexte utilisateur avec un profil mono, et que seulement les pétitionnaires
    ...  fréquents de la collectivité de l'utilisateur et de l'agglo sont présents.

    # Ajout d'un pétitionnaire fréquent sur collectivité Agglo
    Depuis la page d'accueil  admin  admin
    &{args_petitionnaire_agglo} =  Create Dictionary
    ...  particulier_nom=L' Gougeon
    ...  particulier_prenom=Élodie
    ...  om_collectivite=agglo

    Go to Submenu In Menu  guichet_unique  petitionnaire_frequent
    Ajouter le pétitionnaire fréquent depuis le menu pétitionnaire fréquent  ${args_petitionnaire_agglo}

    # Ajout d'un pétitionnaire fréquent sur Marseille
    &{args_petitionnaire_marseille} =  Create Dictionary
    ...  particulier_nom=Lavoie
    ...  particulier_prenom=Sophie
    ...  om_collectivite=MARSEILLE

    Go to Submenu In Menu  guichet_unique  petitionnaire_frequent
    Ajouter le pétitionnaire fréquent depuis le menu pétitionnaire fréquent  ${args_petitionnaire_marseille}

    # Ajout de 2 pétitionnaires fréquents avec le même nom sur Allauch
    Depuis la page d'accueil  instrpolycomm3  instrpolycomm3
    &{args_petitionnaire_allauch} =  Create Dictionary
    ...  particulier_nom=Desjardins
    ...  particulier_prenom=Halette
    Go to Submenu In Menu  guichet_unique  petitionnaire_frequent
    Ajouter le pétitionnaire fréquent depuis le menu pétitionnaire fréquent  ${args_petitionnaire_allauch}

    &{args_petitionnaire_allauch} =  Create Dictionary
    ...  particulier_nom=Desjardins
    ...  particulier_prenom=Thomas
    Go to Submenu In Menu  guichet_unique  petitionnaire_frequent
    Ajouter le pétitionnaire fréquent depuis le menu pétitionnaire fréquent  ${args_petitionnaire_allauch}

    ## En tant qu'utilisateur d'Allauch
    ## Recherche du pétitionnaire fréquent sur l'agglo

    Depuis le contexte de nouvelle demande via le tableau de bord
    Select From List By Label  dossier_autorisation_type_detaille  Permis de construire comprenant ou non des démolitions
    ${present}=  Run Keyword And Return Status    Element Should Be Visible   id=demande_type
    Run Keyword If    ${present} == True    Select From List By Label    id=demande_type    Dépôt Initial
    # On clique sur le bouton d'ajout du pétitionnaire principal
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  add_petitionnaire_principal
    # On saisit le couple nom/prénom
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input Text  particulier_nom  L' Gougeon
    Input Text  particulier_prenom  Élodie
    Click Element  css=.search-frequent-16
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  select-petitionnaire  L' Gougeon Élodie
    Click Element  css=div.dialog-search-frequent-petitionnaire.dialog-search-frequent-petitionnaire div a span

    ## Recherche du pétitionnaire fréquent sur Marseille, qui ne doit pas être trouvé

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input Text  particulier_nom  Lavoie
    Input Text  particulier_prenom  Sophie
    Click Element  css=.search-frequent-16
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Aucune correspondance trouvée.
    Click Element  css=div.dialog-search-frequent-petitionnaire.dialog-search-frequent-petitionnaire div a span

    ## Recherche des pétitionnaires fréquents sur Allauch

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input Text  particulier_nom  Desjardins
    Click Element  css=.search-frequent-16
    # Les 2 pétitionnaires doivent être trouvés
    ${list} =  Create List  Desjardins Halette  Desjardins Thomas
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select List Should Be  css=#select-petitionnaire  ${list}
    Select From List By Label  css=#select-petitionnaire  Desjardins Halette
    Click Element    css=div.dialog-search-frequent-petitionnaire div.ui-dialog-buttonpane button.ui-button
    # On vérifie que la page ne contient pas d'erreur
    Page Should Not Contain Errors
    Click On Back Button In Subform
    # On vérifie le fieldset pétionnaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Desjardins Halette


Passage d'un pétitionnaire en non-fréquent
    [Documentation]  L'objet de ce 'Test Case' est de vérifier que l'ajout d'un
    ...  pétitionnaire fréquent par la création de demande fonctionne, et que le passage
    ...  de ce pétitionnaire en non-fréquent ne le supprime pas du dossier lié.

    # Ajout d'un pétitionnaire fréquent en passant par la demande

    &{args_petitionnaire_marseille} =  Create Dictionary
    ...  particulier_nom=Therrien
    ...  particulier_prenom=Oliver
    ...  frequent=true
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_marseille}

    Depuis la page d'accueil  admin  admin
    # Vérification que le demandeur est bien lié au dossier
    Depuis le contexte du dossier d'instruction  ${di_libelle}
    Page Title Should Be    Instruction > Dossiers D'instruction > ${di_libelle} THERRIEN OLIVER

    Depuis le tableau des pétitionnaires fréquents
    Use Simple Search  nom  Therrien
    Click Link  Therrien Oliver
    Click On Form Portlet Action  petitionnaire_frequent  non_frequent
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Mise à jour effectuée avec succès
    Click On Back Button
    # Le pétitionnaire ne doit plus apparaître dans la liste des fréquents
    Use Simple Search  nom  Therrien Oliver
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  tab-petitionnaire_frequent  Aucun enregistrement.

    # Vérification que le demandeur est bien lié au dossier après le passage en non-fréquent
    Depuis le contexte du dossier d'instruction  ${di_libelle}
    Page Title Should Be    Instruction > Dossiers D'instruction > ${di_libelle} THERRIEN OLIVER

Lien vers le di dans le message de validation de la demande

    [Documentation]  Vérifie si le lien dans le message de validation est
    ...  fonctionnel.

    #
    Depuis la page d'accueil  guichet  guichet
    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=DUPONT
    ...  particulier_prenom=Geralt

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ${libelle_di} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}
    # On clique sur le lien vers le DI du message de validation
    Click Link  css=#link_demande_dossier_instruction
    # On vérifie le fil d'Ariane
    Page Title Should Be    Instruction > Dossiers D'instruction > ${libelle_di} DUPONT GERALT


Vérification de la récuperation des pétitionnaires
    [Documentation]  Vérifie si les types de demandes avec le champ contrainte à
    ...  avec_recup utilisent bien les pétitionnaires et qu'ils soient bien
    ...  remplaçables et que les sans_recup sont bien vides.

    Depuis la page d'accueil  admin  admin

    &{args_petitionnaire_principal} =  Create Dictionary
    ...  particulier_nom=Amorette
    ...  particulier_prenom=David
    ...  frequent=true
    ...  om_collectivite=MARSEILLE
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Racine
    ...  particulier_prenom=Gill
    ...  frequent=true
    ...  om_collectivite=MARSEILLE
    &{args_petitionnaire_remplacement} =  Create Dictionary
    ...  particulier_nom=Couturier
    ...  particulier_prenom=Ignace
    ...  frequent=true
    ...  om_collectivite=MARSEILLE
    &{args_correspondant} =  Create Dictionary
    ...  particulier_nom=Belisarda
    ...  particulier_prenom=Aubin
    ...  frequent=true
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  delegataire=${args_correspondant}
    ...  petitionnaire=${args_petitionnaire}

    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire_principal}  ${args_autres_demandeurs}

    Ajouter une instruction au DI et la finaliser  ${di_libelle}  accepter un dossier sans réserve
    Go To Submenu In Menu  guichet_unique  autre-dossier
    Rechercher et créer une demande sur dossier existant  ${di_libelle}
    Select From List By Label  demande_type  Déclaration attestant l'achèvement et la conformité des travaux

    # Vérification de la contrainte avec_recup
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  liste_demandeur  ${args_petitionnaire_principal.particulier_prenom}
    Element Should Contain  liste_demandeur  ${args_correspondant.particulier_prenom}
    Element Should Contain  liste_demandeur  ${args_petitionnaire.particulier_nom}

    Click Element  css=.petitionnaire .demandeur_del
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  liste_demandeur  ${args_petitionnaire.particulier_prenom}

    Ajouter le demandeur  petitionnaire  ${args_petitionnaire_remplacement}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  liste_demandeur  ${args_petitionnaire_remplacement.particulier_prenom}

    # Vérification de la contrainte sans_recup
    Select From List By Label  demande_type  Demande de transfert
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  add_petitionnaire_principal
