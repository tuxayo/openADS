*** Settings ***
Documentation  Test des fonctionnalités introduites par le multicollectivité.
...    Chaque 'Test Case' est indépendant.

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Variables ***

${json_update_dossier_autorisation}    {"module":"update_dossier_autorisation"}

*** Test Cases ***

Constitution du jeu de données
    [Documentation]  Constitution du jeu de données
    ...    En tant que guichetier
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=DURAND
    ...  particulier_prenom=GÉRARD
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}


Affichage des dossiers d'autorisation pour les services consultés
    [Documentation]  Test l'affichage des DA qui ont une demande d'avis pour
    ...    le profil "Service consulté"

    Depuis la page d'accueil  consu  consu
    Go To Tab  dossier_autorisation_avis
    Element Should Contain  tab-dossier_autorisation_avis  PC 013055 12 00002
    Click Link  PC 013055 12 00002
    Page Should Not Contain Errors
    Comment  À faire : vérifier que le lisitng des DA affiche seulement les DA
    ...  pour lesquels l'utilisateur a eu une demande d'avis.


Etat perime
    [Documentation]  L'objet de ce 'Test Case' est de vérifier que le WS passe
    ...    à l'état périmé les dossiers d'autorisation remplissant les conditions :
    ...    - état = Accordé
    ...    - avec date de décision
    ...    - date de validité dans le passé
    ...    - aucun DOC ou DAACT avec avis favorable

    Depuis la page d'accueil    instr    instr
    Go To Submenu In Menu    autorisation    dossier_autorisation
    Page Should Not Contain Errors
    ${libelle_sans_espace} =  Sans espace  PA 013055 12 00001
    Input Text  css=div#adv-search-adv-fields input#dossier  ${libelle_sans_espace}
    Click On Search Button
    Click On Link    PA 013055 12 00001
    Page Title Should Be    Autorisation > Dossiers D'autorisation
    Element Text Should Be    css=#da_etat    Accordé
    Vérifier le code retour du web service et vérifier que son message est    Post    maintenance    ${json_update_dossier_autorisation}    200    2 dossier(s) d'autorisation(s) mis à jour.
    Vérifier le code retour du web service et vérifier que son message est    Post    maintenance    ${json_update_dossier_autorisation}    200    Aucune mise à jour

    Reload Page
    Element Text Should Be    css=#da_etat    Périmé


TNR Bug "Erreur de base de données" dans la recherche avancée des DA sur le critère date de décision

    [Documentation]    Test de non régression sur le bug "Erreur de base de
    ...    données" sur la validation de la recherche avancée dans le listing
    ...    "Autorisation" -> "Dossiers d'Autorisation" sur le critère "Date de
    ...    décision".

    #
    Depuis la page d'accueil    instr    instr
    #
    Depuis le listing des dossiers d'autorisation
    # On remplit les critères date de décision de la recherche avancée
    Input Text  css=div#adv-search-adv-fields input#date_decision_min  01/05/2015
    Input Text  css=div#adv-search-adv-fields input#date_decision_max  31/05/2015
    # On valide le formulaire de recherche
    Click On Search Button
    # On ne fait aucune vérification ici car le keyword précédent "Click On
    # Search Button" permet de vérifier qu'il n'y a pas d'erreur de base de
    # données

TNR Bug Recalcul du DA si plusieurs DI sans décision

    [Documentation]    Test de non régression sur le bug impactant le recalcul
    ...     du DA si plusieurs DI sans décision

    #
    Depuis la page d'accueil    admin    admin

    @{etats_autorises} =    Create List
    ...    delai majore
    ...    delai de notification envoye
    ...    dossier sans notification de delai

    &{args_demande_type} =  Create Dictionary
    ...    code=TNR Bug Recalcul du DA
    ...    libelle=TNR Bug Recalcul du DA
    ...    groupe=Autorisation ADS
    ...    dossier_autorisation_type_detaille=PCI (Permis de construire pour une maison individuelle et / ou ses annexes)
    ...    demande_nature=Dossier existant
    ...    etats_autorises=@{etats_autorises}
    ...    contraintes=Avec récupération demandeur
    ...    dossier_instruction_type=PCI - Modificatif
    ...    evenement=Notification de delai

    Ajouter un nouveau type de demande depuis le menu    ${args_demande_type}

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=DURAND
    ...  particulier_prenom=Marcel
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    &{args_demande} =  Create Dictionary
    ...  demande_type=TNR Bug Recalcul du DA
    ...  dossier_instruction=${di_libelle}
    ${di_libelle_2} =  Ajouter la demande par WS  ${args_demande}


Visibilité des DA

    [Documentation]  Ce test case vérifie que cocher l'option "masquer DA" d'un type de
    ...  dossier d'autorisation rend bien invisible et inacessible les DA de ce type dans
    ...  toute l'application.

    Depuis la page d'accueil    admin    admin

    &{args_type_da} =  Create Dictionary
    ...  cacher_da=true
    Modifier le type de dossier d'autorisation  PC  ${args_type_da}


    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=MAROIS
    ...  particulier_prenom=Seymour
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_libelle} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    ${da_libelle} =  Get Substring  ${di_libelle}  0  -2
    ${da_libelle_sans_espace} =  Sans Espace  ${da_libelle}

    # Accès par le menu Autorisation > Dossier d'autorisation pour service consultés (dossier_autorisation_avis)
    Depuis la page d'accueil  consu  consu
    Go To Tab  dossier_autorisation_avis
    Input Text  css=div#adv-search-adv-fields input#dossier  PC
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Aucun enregistrement


    # Accès par le menu Autorisation > Dossier d'autorisation
    Depuis la page d'accueil  instr  instr
    Depuis le listing des dossiers d'autorisation
    Input Text  css=div#adv-search-adv-fields input#dossier  ${da_libelle_sans_espace}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Aucun enregistrement

    # Accès directement par l'URL
    Go To  ${PROJECT_URL}scr/form.php?obj=dossier_autorisation&action=3&idx=${da_libelle_sans_espace}
    Page Should Contain  Droits insuffisants

    # Accès par l'onglet des dossiers liés du DI
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${di_libelle}
    Element Should Not Be Visible  sousform-dossier_autorisation

    # On vérifie que les fonctionnalités d'accès par le portail citoyen sont désactivées
    Depuis le contexte du dossier d'instruction  ${di_libelle}
    # On vérifie que le champ contenant la clé d'accès au portail citoyen est vide
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open Fieldset    dossier_instruction    demandeur
    Element Should Not Be Visible  cle_acces_citoyen

    Portlet Action Should Not Be In Form  dossier_instruction  generate_citizen_access_key
    Portlet Action Should Not Be In Form  dossier_instruction  regenerate_citizen_access_key

    # On remet le type de DA "PC" dans l'état initial
    Depuis la page d'accueil    admin    admin

    &{args_type_da} =  Create Dictionary
    ...  cacher_da=false
    Modifier le type de dossier d'autorisation  PC  ${args_type_da}
