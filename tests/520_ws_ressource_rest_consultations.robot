*** Settings ***
Documentation  WS Ressource REST 'consultations'.

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
REST

    [Documentation]  Ce TestCase vérifie la partie REST du WS
    ...  - la seule méthode disponible est le POST, les autres doivent retourner un code 400,
    ...  - les clés suivantes sont obligatoires dans le tableau JSON : type, date, emetteur, dossier_instruction, contenu. Si une des clés n'est pas présente ou si il y a une clé supplémentaire dans les données d'entrées, le WS doit retourner un code 400.

    ## Seule la méthode PUT doit être disponible sur cette ressource
    ${json} =  Set Variable  { "type": ""}
    Vérifier le code retour du web service et vérifier que son message est  Get  consultations/123  ${json}  400  La méthode GET n'est pas disponible sur cette ressource.
    Vérifier le code retour du web service et vérifier que son message est  Post  consultations  ${json}  400  La méthode POST n'est pas disponible sur cette ressource.
    Vérifier le code retour du web service et vérifier que son message est  Delete  consultations/123  ${json}  400  La méthode DELETE n'est pas disponible sur cette ressource.

    ## L'identifiant est obligatoire
    # On ne fourni pas de numéro de dossier d'instruction
    ${json} =  Set Variable  { "avis": "complet", "date_retour": "${DATE_FORMAT_dd/mm/yyyy}"}
    Vérifier le code retour du web service et vérifier que son message est  Put  consultations  ${json}  400  Aucun identifiant fourni pour la ressource.

    ## Deux clés sont obligatoires
    # sans la clé 'date_retour'
    ${json} =  Set Variable  { "avis": ""}
    Vérifier le code retour du web service et vérifier que son message est  Put  consultations/123  ${json}  400  La structure des données reçues n'est pas correcte.
    # sans la clé 'avis'
    ${json} =  Set Variable  { "date_retour": ""}
    Vérifier le code retour du web service et vérifier que son message est  Put  consultations/123  ${json}  400  La structure des données reçues n'est pas correcte.


Métier

    [Documentation]  Ce TestCase vérifie la partie Métier du WS
    ...  - ...

    ##
    #
    ${json} =  Set Variable  { "avis": "Favorable", "date_retour": "15/01/2013"}
    #
    Vérifier le code retour du web service et vérifier que son message est  Put  consultations/3  ${json}  200  L'avis de la consultation 3 a été pris en compte
    #
    Vérifier le code retour du web service et vérifier que son message est  Put  consultations/3  ${json}  400  Un retour d'avis a déjà été rendu pour la consultation '3'.

    ##
    #
    ${json} =  Set Variable  { "avis": "Favorable", "date_retour": "15/03/2013", "fichier_base64": "JVBERi0xLjQKJcOkw7zDtsOfCjIgM", "nom_fichier": "plop.pdf"}
    #
    Vérifier le code retour du web service et vérifier que son message est  Put  consultations/4  ${json}  200  L'avis de la consultation 4 a été pris en compte
    #
    Vérifier le code retour du web service et vérifier que son message est  Put  consultations/4  ${json}  400  Un retour d'avis a déjà été rendu pour la consultation '4'.
