*** Settings ***
Documentation  Gestion des consultations.

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown

*** Variables ***
${json_consultation}  {"module":"consultation"}


*** Test Cases ***
TNR Routine de mise à jour des consultations tacites

    [Documentation]  Permet de vérifier l'état des consultations après le
    ...  traitement tacite.
    ...  Attention ! ce test est en premier car il utilise les données du init_data
    ...  il est nécessaire d'améilorer ce point

    # On exécute le WS de mise à jour des consultations
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json_consultation}  200  2 consultations mise(s) à jour.

    # On vérifie que la valeur de la consultation qui a été passée en tacite
    # est bien marquée comme 'non lu'
    # Définition de l'id du widget des retours de consultation du profil INSTRUCTEUR
    ${widget_id} =  Set Variable  widget_3
    # En tant que profil 'INSTRUCTEUR'
    Depuis la page d'accueil  instr  instr
    # On vérifie que les consultations apparaissent bien sur le tableau de bord de l'instructeur
    Element Should Contain  css=#${widget_id} .widget-content-wrapper span.box-icon  1
    # On clique sur le lien "Voir +" du widget
    Click Element  css=#${widget_id} .widget-footer a
    # On accède au listing des restours de consultation
    Page Title Should Be  Instruction > Consultations > Mes Retours
    # On clique sur le dossier en question
    Click On Link  PC 013055 12 00002P0
    # On vérifie que les champs ont bien été mis à jour par le webservice
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Static Value Should Be  css=#lu  Non
    Form Static Value Should Be  css=#avis_consultation  Tacite
    Form Static Value Should Be  css=#date_retour  01/02/2013


Constitution du jeu de données

    [Documentation]  Constitue le jeu de données.


    #
    # Supposition : ces dossiers sont affectés à l'instructeur "Louis Laurent"
    # (instr) division "H" même division que "Martine Nadeau" (instr1)
    #

    #
    &{args_petitionnaire_1} =  Create Dictionary
    ...  particulier_nom=DUPONT
    ...  particulier_prenom=Jacques
    ...  om_collectivite=MARSEILLE
    #
    &{args_demande_1} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=12/04/2015
    ...  om_collectivite=MARSEILLE
    #
    ${di_1} =  Ajouter la demande par WS  ${args_demande_1}  ${args_petitionnaire_1}
    Set Suite Variable  ${di_1}

    #
    Depuis la page d'accueil  instr  instr
    Ajouter une consultation depuis un dossier  ${di_1}  59.01 - Direction de l'Eau et de l'Assainissement

    #
    &{args_avis_consultation_1} =  Create Dictionary
    ...  avis_consultation=Favorable

    Depuis la page d'accueil  consu  consu
    #
    Rendre l'avis sur la consultation du dossier  ${di_1}  ${args_avis_consultation_1}
    #
    &{args_petitionnaire_3} =  Create Dictionary
    ...  particulier_nom=DUPONT
    ...  particulier_prenom=Jacques
    ...  om_collectivite=MARSEILLE
    #
    &{args_demande_3} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=01/04/2016
    ...  om_collectivite=MARSEILLE
    #
    ${di_3} =  Ajouter la demande par WS  ${args_demande_3}  ${args_petitionnaire_3}
    Set Suite Variable  ${di_3}

    &{args_petitionnaire_2} =  Create Dictionary
    ...  particulier_nom=DUPONT
    ...  particulier_prenom=Jacques
    ...  om_collectivite=MARSEILLE
    #
    &{args_demande_2} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=12/04/2015
    ...  om_collectivite=MARSEILLE
    #
    ${di_2} =  Ajouter la demande par WS  ${args_demande_2}  ${args_petitionnaire_2}
    Set Suite Variable  ${di_2}


    Depuis la page d'accueil  instr  instr
    Ajouter une consultation depuis un dossier  ${di_2}  59.01 - Direction de l'Eau et de l'Assainissement

    &{args_avis_consultation} =  Create Dictionary
    ...  avis_consultation=Favorable
    Depuis la page d'accueil  consu  consu
    Rendre l'avis sur la consultation du dossier  ${di_2}  ${args_avis_consultation}

Ajout d'une consultation simple

    [Documentation]

    ##
    ## Constitution du jeu de données
    ##
    # Données du demandeur
    &{args_petitionnaire} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=DAINEAU Ets
    ...  personne_morale_civilite=Monsieur
    ...  personne_morale_nom=MICHEL
    ...  personne_morale_prenom=Alain
    ...  om_collectivite=MARSEILLE
    # Données de la demande
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=03/05/2016
    ...  om_collectivite=MARSEILLE
    # Données techniques du dossiers pour le tableau des surfaces
    &{donnees_techniques_values} =  Create Dictionary
    ...  su_avt_shon1=10
    ...  su_avt_shon2=10
    ...  su_avt_shon3=10
    ...  su_avt_shon4=10
    ...  su_avt_shon5=10
    ...  su_avt_shon6=10
    ...  su_avt_shon7=10
    ...  su_avt_shon8=10
    ...  su_avt_shon9=10
    ...  su_cstr_shon1=10
    ...  su_cstr_shon2=10
    ...  su_cstr_shon3=10
    ...  su_cstr_shon4=10
    ...  su_cstr_shon5=10
    ...  su_cstr_shon6=10
    ...  su_cstr_shon7=10
    ...  su_cstr_shon8=10
    ...  su_cstr_shon9=10
    ...  su_chge_shon1=10
    ...  su_chge_shon2=10
    ...  su_chge_shon3=10
    ...  su_chge_shon4=10
    ...  su_chge_shon5=10
    ...  su_chge_shon6=10
    ...  su_chge_shon7=10
    ...  su_chge_shon8=10
    ...  su_chge_shon9=10
    ...  su_demo_shon1=10
    ...  su_demo_shon2=10
    ...  su_demo_shon3=10
    ...  su_demo_shon4=10
    ...  su_demo_shon5=10
    ...  su_demo_shon6=10
    ...  su_demo_shon7=10
    ...  su_demo_shon8=10
    ...  su_demo_shon9=10
    ...  su_sup_shon1=10
    ...  su_sup_shon2=10
    ...  su_sup_shon3=10
    ...  su_sup_shon4=10
    ...  su_sup_shon5=10
    ...  su_sup_shon6=10
    ...  su_sup_shon7=10
    ...  su_sup_shon8=10
    ...  su_sup_shon9=10
    # Ajout de la nouvelle demande pour création du DI
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    # Saisie des données techniques sur le DI
    Depuis la page d'accueil  instr  instr
    Modifier les données techniques pour le calcul des surfaces  ${di}  ${donnees_techniques_values}

    ##
    ## Cas d'usage n°1
    ##
    ## Le principe ici est de tester l'ajout d'une consultation simple par un
    ## profil qui n'a pas les permissions de sélectionner la date d'envoi, les
    ## points clés testés sont :
    ##  - l'ajout de consultation simple est disponible
    ##  - le champ date d'envoi n'est pas présent
    ##  - le champ service est obligatoire
    ##  - un mail est envoyé au service
    ##  - le champ date d'envoi est positionné à la date du jour à l'enregistrement
    ##  - l'édition PDF est accessible et contient :
    ##    * le demandeur
    ##    * le total du calcul des surfaces
    ##
    # On se connecte en tant que "instr" (Profil 'INSTRUCTEUR')
    Depuis la page d'accueil  instr  instr
    # Définition du service dans ce cas d'usage - notification email configurée sur ce service
    ${service_1} =  Set Variable  59.01 - Direction de l'Eau et de l'Assainissement
    # On accède à l'onglet "Consultation(s)" du DI
    Depuis l'onglet consultation du dossier  ${di}
    # On vérifie que nous avons bien le bon nombre de consultations affichées
    Element Should Contain  css=#sousform-consultation .pagination-text  1 - 0 enregistrement(s) sur 0
    # On clique sur le lien "Ajouter" dans le listing
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-consultation-corner-ajouter
    # On attend que le formulaire soit chargé correctement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#service
    # En tant que Profil 'INSTRUCTEUR', le champ date d'envoi doit être caché
    Element Should Not Be Visible  css=#sformulaire #date_envoi
    # On valide le formulaire sans sélectionner de service
    Click On Submit Button In Subform
    # On vérifie que la soumission du formulaire est rejetée
    Error Message Should Contain In Subform  SAISIE NON ENREGISTRÉE
    # Le service est obligatoire
    Error Message Should Contain In Subform  Le champ Service est obligatoire
    # En tant que Profil 'INSTRUCTEUR', le champ date d'envoi doit être caché
    Element Should Not Be Visible  css=#sformulaire #date_envoi
    # On sélectionne le service
    Select From List By Label  css=#sformulaire #service  ${service_1}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie que la soumission du formulaire est validée
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées
    # On vérifie que le courriel de notification a été envoyé
    Valid Message Should Contain In Subform  Envoi d'un mail de notification au service
    # On retourne au listing
    Click On Back Button In Subform
    # On vérifie que nous avons bien le bon nombre de consultations affichées
    Element Should Contain  css=#sousform-consultation .pagination-text  1 - 1 enregistrement(s) sur 1
    # On accède à la fiche de visualisation de la consultation créée
    Depuis le contexte de la consultation  ${di}  ${service_1}
    Portlet Action Should Not Be In SubForm  consultation  finalise
    # On définalise pour voir l'action de suppression
    Click On SubForm Portlet Action  consultation  unfinalise
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain In Subform  La définalisation du document s'est effectuée avec succès.
    Portlet Action Should Not Be In SubForm  rapport_instruction  unfinalise
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Portlet Action Should Be In SubForm  consultation  supprimer
    Click On SubForm Portlet Action  consultation  finalise
    # Vérification que la date d'envoi de la consultation est bien la date du jour
    Element Text Should Be  css=#sformulaire #date_envoi  ${date_ddmmyyyy}
    # On clique sur l'action édition
    Click On SubForm Portlet Action  consultation  consulter_pdf
    # On ouvre le PDF
    Open PDF  file
    # On vérifie le champ de fusion
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  DAINEAU Ets représenté(e) par Monsieur MICHEL Alain
    # On vérifie le résultat total du tableau des surface
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Surface totale : 90
    # On ferme le PDF
    Close PDF

    ##
    ## Cas d'usage n°2
    ##
    ## Le principe ici est de tester l'ajout d'une consultation simple par un
    ## profil qui a les permissions de sélectionner la date d'envoi, les
    ## points clés testés sont :
    ##  - l'ajout de consultation simple est disponible
    ##  - le champ date d'envoi est présent
    ##  - YYY le champ date d'envoi du formulaire est positionné à la date du jour
    ##  - un mail n'est pas envoyé au service
    ##  - le champ date d'envoi n'est pas positionné à la date du jour à l'enregistrement
    ##  - le délai est calculé correctement
    ##
    # On se connecte en tant que "admingen" (Profil 'ADMINISTRATEUR GENERAL')
    Depuis la page d'accueil  admingen  admingen
    # Définition du service dans ce cas d'usage
    # - pas de notification email configurée sur ce service
    # - délai 1 mois
    ${service_2} =  Set Variable  59.02 - Atelier du Patrimoine
    # On accède à l'onglet "Consultation(s)" du DI
    Depuis l'onglet consultation du dossier  ${di}
    # On vérifie que nous avons bien le bon nombre de consultations affichées
    Element Should Contain  css=#sousform-consultation .pagination-text  1 - 1 enregistrement(s) sur 1
    # On clique sur le lien "Ajouter" dans le listing
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-consultation-corner-ajouter
    # On attend que le formulaire soit chargé correctement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#service
    # En tant que Profil 'ADMINISTRATEUR GENERAL', le champ date d'envoi doit être affiché
    Element Should Be Visible  css=#sformulaire #date_envoi
    # On vérifie que la date du jour est pré-remplie dans le champs "date d'envoi"
    Form Value Should Be  css=#sformulaire #date_envoi  ${date_ddmmyyyy}
    # On sélectionne le service
    Select From List By Label  css=#sformulaire #service  ${service_2}
    # On vide le champ de date d'envoi
    Input Text  date_envoi  ${EMPTY}
    # On valide le formulaire sans sélectionner de date d'envoi
    Click On Submit Button In Subform
    # On vérifie que la soumission du formulaire est rejetée
    Error Message Should Contain In Subform  SAISIE NON ENREGISTRÉE
    # Le service est obligatoire
    Error Message Should Contain In Subform  Le champ Date d'envoi est obligatoire
    # On positionne une date d'envoi
    Input Text  date_envoi  01/04/2016
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie que la soumission du formulaire est validée
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées
    # On vérifie qu'il n'y a pas eu de notification email
    Page Should Not Contain  Envoi d'un mail de notification au service
    # On vérifie que le calcul du délai est correct
    Valid Message Should Contain In Subform  Délai Retour 1 Mois -> Retour 01/05/2016
    # On retourne au listing
    Click On Back Button In Subform
    # On vérifie que nous avons bien le bon nombre de consultations affichées
    Element Should Contain  css=#sousform-consultation .pagination-text  1 - 2 enregistrement(s) sur 2
    # On accède à la fiche de visualisation de la consultation créée
    Depuis le contexte de la consultation  ${di}  ${service_2}
    # Vérification que la date d'envoi de la consultation est bien la date saisie
    Element Text Should Be  css=#sformulaire #date_envoi  01/04/2016
    # Vérification que la date limite de la consultation est bien la date saisie + 1 mois
    Element Text Should Be  css=#sformulaire #date_limite  01/05/2016


Ajout d'une consultation multiple

    [Documentation]

    ##
    ## Constitution du jeu de données
    ##
    # Données du demandeur
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Bourgeau
    ...  particulier_prenom=Aurore
    ...  om_collectivite=MARSEILLE
    # Données de la demande
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=08/04/2016
    ...  om_collectivite=MARSEILLE

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    ##
    ## Cas d'usage n°1
    ##
    ## Le principe ici est de tester l'ajout d'une consultation multiple par un
    ## profil qui n'a pas les permissions de sélectionner la date d'envoi, les
    ## points clés testés sont :
    ##  - l'ajout de consultation multiple est disponible
    ##  - le champ date d'envoi n'est pas présent
    ##  - il est obligatoire de sélectionner au moins un service
    ##  - l'interface de sélection/désélection de services fonctionne
    ##  - le champ date d'envoi est positionné à la date du jour à l'enregistrement
    ##  - l'option version papier génère un PDF multiple
    ##  - l'édition PDF est accessible et contient :
    ##    * une page par consultation
    ##    * le demandeur sur chaque page
    ##
    # On se connecte en tant que "instr" (Profil 'INSTRUCTEUR')
    Depuis la page d'accueil  instr  instr
    # On accède à l'onglet "Consultation(s)" du DI
    Depuis l'onglet consultation du dossier  ${di}
    # On vérifie que nous avons bien le bon nombre de consultations affichées
    Element Should Contain  css=#sousform-consultation .pagination-text  1 - 0 enregistrement(s) sur 0
    # On clique sur le lien "Ajouter multiples" dans le tableau
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-consultation-corner-ajouter_multiple
    # On attend que le formulaire soit chargé correctement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  button_val
    # En tant que Profil 'INSTRUCTEUR', le champ date d'envoi doit être caché
    Element Should Not Be Visible  css=#sformulaire #date_envoi
    # On clique sur le bouton "Ajouter" du formulaire sans sélectionner de service
    Click Element  button_val
    # On vérifie qu'une alerte javascript nous indique qu'il y a une erreur de saisie
    ${alert} =  Get Alert Message
    Should Be Equal As Strings  ${alert}  Veuillez choisir au moins un service et une date d envoi
    # En tant que Profil 'INSTRUCTEUR', le champ date d'envoi doit être caché
    Element Should Not Be Visible  date_envoi
    # On sélectionne quatre services
    Click Element  t10_572_0_
    ${status} =  Run Keyword And Return Status  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#t10_572_0_.liste_gauche_service_selected
    Run Keyword If  ${status} == False  Click Element  t10_572_0_
    Click Element  t10_575_0_
    Click Element  t2_13_0_
    Click Element  t10_542_0_
    # On les ajoute à la sélection
    Click Element  add-ser-them
    # On sélectionne un des services sélectionnés
    Click Element  css=div[name="t10_542_0_"]
    # On l'enlève de la sélection
    Click Element  del-ser-them
    # On coche la case pour la consultation papier pour deux des services
    Select Checkbox  css=div.cell2 > div > input.t10_575_0_
    Select Checkbox  css=div.cell2 > div > input.t10_572_0_
    # On clique sur le bouton "Ajouter" du formulaire
    Click Element  button_val
    # On vérifie qu'il n'y a pas d'erreur sur l'élément affiché
    Page Should Not Contain Errors
    # Le PDF s'ouvre tout seul, on sélectionne la bonne fenêtre
    Open PDF  form
    # On vérifie que le PDF a bien deux pages
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  PDF Pages Number Should Be  2
    # Et que le nom du pétitionnaire est bien remplacé sur chaque page
    PDF Page Number Should Contain  1  Bourgeau Aurore
    PDF Page Number Should Contain  2  Bourgeau Aurore
    # On ferme le PDF
    Close PDF
    # On vérifie que le message de validation est présent avec le bon nombre de consultations
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain In Subform  3 service(s) sélectionné(s) dont 2 consultation(s) papier.
    # On vérifie que nous avons bien le bon nombre de consultations affichées
    Element Should Contain  css=#sousform-consultation .pagination-text  1 - 3 enregistrement(s) sur 3
    # On accède à la fiche de visualisation de la consultation créée
    Depuis le contexte de la consultation  ${di}  59.88 - DAE - COMMERCE ARTISANAT
    # Vérification que la date d'envoi de la consultation est bien la date du jour
    Element Text Should Be  date_envoi  ${date_ddmmyyyy}

    ##
    ## Cas d'usage n°2
    ##
    ## Le principe ici est de tester l'ajout d'une consultation multiple par un
    ## profil qui a les permissions de sélectionner la date d'envoi, les
    ## points clés testés sont :
    ##  - l'ajout de consultation multiple est disponible
    ##  - le champ date d'envoi est présent
    ##  - le champ date d'envoi du formulaire est positionné à la date du jour
    ##  - le champ date d'envoi n'est pas positionné à la date du jour à l'enregistrement
    ##
    # On se connecte en tant que "admingen" (Profil 'ADMINISTRATEUR GENERAL')
    Depuis la page d'accueil  admingen  admingen
    # On accède à l'onglet "Consultation(s)" du DI
    Depuis l'onglet consultation du dossier  ${di}
    # On vérifie que nous avons bien le bon nombre de consultations affichées
    Element Should Contain  css=#sousform-consultation .pagination-text  1 - 3 enregistrement(s) sur 3
    # On clique sur le lien "Ajouter multiples" dans le tableau
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-consultation-corner-ajouter_multiple
    # On attend que le formulaire soit chargé correctement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  button_val
    # En tant que Profil 'ADMINISTRATEUR GENERAL', le champ date d'envoi doit être affiché
    Element Should Be Visible  css=#sformulaire #date_envoi
    # On vérifie que la date du jour est pré-remplie dans le champs "date d'envoi"
    Form Value Should Be  css=#sformulaire #date_envoi  ${date_ddmmyyyy}
    # Service 59.12 - Direction de la Propreté Urbaine
    Click Element  t10_12_0_
    # Service 59.30 - Orange France
    Click Element  t10_15_0_
    # On les ajoute à la sélection
    Click Element  add-ser-them
    # Input de la date en JavaScript pour éviter l'appel au onChange sur ce
    # champ, qui appelle une fonction JS fonctionnant une fois sur deux dans
    # les tests
    Input Value With JS  date_envoi  01/04/2016
    # On clique sur le bouton "Ajouter" du formulaire
    Click Element  button_val
    # On vérifie qu'il n'y a pas d'erreur sur l'élément affiché
    Page Should Not Contain Errors
    # On vérifie le message de validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain In Subform  2 service(s) sélectionné(s) dont 0 consultation(s) papier.
    # On vérifie que nous avons bien le bon nombre de consultations affichées
    Element Should Contain  css=#sousform-consultation .pagination-text  1 - 5 enregistrement(s) sur 5
    # On accède à la fiche de visualisation de la consultation créée
    Depuis le contexte de la consultation  ${di}  59.12 - Direction de la Propreté Urbaine
    # Vérification que la date d'envoi de la consultation est bien la date saisie
    Element Text Should Be  date_envoi  01/04/2016


Gestion des retours de consultation depuis la rubrique 'Instruction'

    [Documentation]

    ##
    ## Constitution du jeu de données
    ##
    ## On cré trois deux nouvelles collectivités pour être sûr du nombre
    ## de retours de consultations à vérifier dans les widgets et tableaux
    ##
    #
    Depuis la page d'accueil  admin  admin
    # collectivité 01 'WORKINGTON' de niveau 1
    Ajouter la collectivité depuis le menu  WORKINGTON  mono
    #
    Ajouter l'utilisateur  Carter SANCHEZ  nospam@openmairie.org  csanchez  csanchez  INSTRUCTEUR  WORKINGTON
    Ajouter la direction depuis le menu  WKT  Direction WKT  null  Chef WKT  null  null  WORKINGTON
    Ajouter la division depuis le menu  WTH  subdivision WTH  null  Chef WKT  null  null  Direction WKT
    Ajouter la division depuis le menu  WTJ  subdivision WTJ  null  Chef WKT  null  null  Direction WKT
    Ajouter l'instructeur depuis le menu  Carter SANCHEZ  subdivision WTH  instructeur  Carter SANCHEZ
    &{args_affectation} =  Create Dictionary
    ...  instructeur=Carter SANCHEZ (WTH)
    ...  om_collectivite=WORKINGTON
    Ajouter l'affectation depuis le menu  ${args_affectation}
    #
    Ajouter l'utilisateur  Selma SAUNDERS  nospam@openmairie.org  ssaunders  ssaunders  INSTRUCTEUR  WORKINGTON
    Ajouter l'instructeur depuis le menu  Selma SAUNDERS  subdivision WTH  instructeur  Selma SAUNDERS
    &{args_affectation} =  Create Dictionary
    ...  instructeur=Selma SAUNDERS (WTH)
    ...  om_collectivite=WORKINGTON
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    Ajouter l'affectation depuis le menu  ${args_affectation}
    #
    Ajouter l'utilisateur  Harriet SANTIAGO  nospam@openmairie.org  hsantiago  hsantiago  INSTRUCTEUR  WORKINGTON
    Ajouter l'instructeur depuis le menu  Harriet SANTIAGO  subdivision WTJ  instructeur  Harriet SANTIAGO
    &{args_affectation} =  Create Dictionary
    ...  instructeur=Harriet SANTIAGO (WTJ)
    ...  om_collectivite=WORKINGTON
    ...  dossier_autorisation_type_detaille=Permis de démolir
    Ajouter l'affectation depuis le menu  ${args_affectation}
    #
    Ajouter l'utilisateur  Alden SYKES  nospam@openmairie.org  asykes  asykes  SERVICE CONSULTÉ  WORKINGTON
    &{service} =  Create Dictionary
    ...  abrege=95A
    ...  libelle=Direction de la circulation de Workington
    ...  edition=Consultation - Demande d'avis
    ...  om_collectivite=WORKINGTON
    Ajouter le service depuis le listing  ${service}
    &{lien_service_om_utilisateur} =  Create Dictionary
    ...  om_utilisateur=Alden SYKES
    ...  service=Direction de la circulation de Workington
    Ajouter lien service/utilisateur  ${lien_service_om_utilisateur}
    # collectivité 02 'LIDINGO' de niveau 1
    Ajouter la collectivité depuis le menu  LIDINGO  mono
    #
    Ajouter l'utilisateur  Mary JOYCE  nospam@openmairie.org  mjoyce  mjoyce  INSTRUCTEUR  LIDINGO
    Ajouter la direction depuis le menu  LDG  Direction LDG  null  Chef LDG  null  null  LIDINGO
    Ajouter la division depuis le menu  LDG  subdivision LDG  null  Chef LDG  null  null  Direction LDG
    Ajouter l'instructeur depuis le menu  Mary JOYCE  subdivision LDG  instructeur  Mary JOYCE
    &{args_affectation} =  Create Dictionary
    ...  instructeur=Mary JOYCE (LDG)
    ...  om_collectivite=LIDINGO
    Ajouter l'affectation depuis le menu  ${args_affectation}

    #
    Ajouter l'utilisateur  Alexandra TERRELL  nospam@openmairie.org  aterrell  aterrell  SERVICE CONSULTÉ  LIDINGO
    &{service} =  Create Dictionary
    ...  abrege=96A
    ...  libelle=Direction de la circulation de Lidingo
    ...  edition=Consultation - Demande d'avis
    ...  om_collectivite=LIDINGO
    Ajouter le service depuis le listing  ${service}
    &{lien_service_om_utilisateur} =  Create Dictionary
    ...  om_utilisateur=Alexandra TERRELL
    ...  service=Direction de la circulation de Lidingo
    Ajouter lien service/utilisateur  ${lien_service_om_utilisateur}
    #
    &{args_avis_consultation} =  Create Dictionary
    ...  avis_consultation=Favorable
    # DI n°1 :
    # - Collectivité 'WORKINGTON' (niveau mono)
    # - Instructeur 'Harriet SANTIAGO' (hsantiago)
    # - Division 'J'
    #
    &{args_petitionnaire_01} =  Create Dictionary
    ...  particulier_nom=DUPONT
    ...  particulier_prenom=Jacques
    ...  om_collectivite=WORKINGTON
    #
    &{args_demande_01} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de démolir
    ...  demande_type=Dépôt Initial
    ...  date_demande=12/04/2015
    ...  om_collectivite=WORKINGTON
    #
    ${di_01} =  Ajouter la demande par WS  ${args_demande_01}  ${args_petitionnaire_01}
    # DI n°2 :
    # - Collectivité 'WORKINGTON' (niveau mono)
    # - Instructeur 'Carter SANCHEZ' (csanchez)
    # - Division 'H'
    #
    &{args_petitionnaire_02} =  Create Dictionary
    ...  particulier_nom=VACHIER
    ...  particulier_prenom=Arthur
    ...  om_collectivite=WORKINGTON
    #
    &{args_demande_02} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=12/04/2015
    ...  om_collectivite=WORKINGTON
    #
    ${di_02} =  Ajouter la demande par WS  ${args_demande_02}  ${args_petitionnaire_02}
    # DI n°3 :
    # - Collectivité 'WORKINGTON' (niveau mono)
    # - Instructeur 'Selma SAUNDERS' (ssaunders)
    # - Division 'H'
    #
    &{args_petitionnaire_03} =  Create Dictionary
    ...  particulier_nom=BRAY
    ...  particulier_prenom=Guy
    ...  om_collectivite=WORKINGTON
    #
    &{args_demande_03} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire comprenant ou non des démolitions
    ...  demande_type=Dépôt Initial
    ...  date_demande=12/04/2015
    ...  om_collectivite=WORKINGTON
    #
    ${di_03} =  Ajouter la demande par WS  ${args_demande_03}  ${args_petitionnaire_03}
    # DI n°4 :
    # - Collectivité 'LIDINGO' (niveau mono)
    # - Instructeur 'Mary JOYCE' (mjoyce)
    # - Division 'H'
    #
    &{args_petitionnaire_04} =  Create Dictionary
    ...  particulier_nom=BOULAGE
    ...  particulier_prenom=Damien
    ...  om_collectivite=LIDINGO
    #
    &{args_demande_04} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=12/04/2015
    ...  om_collectivite=LIDINGO
    #
    ${di_04} =  Ajouter la demande par WS  ${args_demande_04}  ${args_petitionnaire_04}
    #
    Ajouter une consultation depuis un dossier  ${di_01}  95A - Direction de la circulation de Workington
    Ajouter une consultation depuis un dossier  ${di_02}  95A - Direction de la circulation de Workington
    Ajouter une consultation depuis un dossier  ${di_03}  95A - Direction de la circulation de Workington
    Ajouter une consultation depuis un dossier  ${di_04}  96A - Direction de la circulation de Lidingo
    #
    Depuis la page d'accueil  asykes  asykes

    Rendre l'avis sur la consultation du dossier  ${di_01}  ${args_avis_consultation}
    Rendre l'avis sur la consultation du dossier  ${di_02}  ${args_avis_consultation}
    Rendre l'avis sur la consultation du dossier  ${di_03}  ${args_avis_consultation}
    #
    Depuis la page d'accueil  aterrell  aterrell
    Rendre l'avis sur la consultation du dossier  ${di_04}  ${args_avis_consultation}
    #

    ##
    ## Cas d'usage n°1
    ##
    ##
    ##
    #
    ${widget_id} =  Set Variable  widget_3

    # On se connecte en tant que utilisateur de niveau 2
    Depuis la page d'accueil  admin  admin
    # On vérifie qu'on a la collonne collectivité dans le listing tous les retours
    Go To Submenu In Menu  instruction  consultation_tous_retours
    Page Title Should Be  Instruction > Consultations > Tous Les Retours
    First Tab Title Should Be  Consultation
    Page Should Contain  Les consultations marquées comme 'non lu' qui concernent des dossiers d'instruction situés dans toutes les collectivités.
    Element Should Contain  css=#tab-consultation_tous_retours table thead  instructeur
    Element Should Contain  css=#tab-consultation_tous_retours table thead  division
    Element Should Contain  css=#tab-consultation_tous_retours table thead  collectivité
    # On va sur le listing 'Tous les retours'
    # Il doit contenir des retours des deux collectivités
    Element Should Contain  css=#tab-consultation_tous_retours table  WORKINGTON
    Element Should Contain  css=#tab-consultation_tous_retours table  LIDINGO

    # On se connecte en tant que "csanchez" (Profil 'INSTRUCTEUR')
    Depuis la page d'accueil  csanchez  csanchez
    # On vérifie que les consultations apparaissent bien sur le tableau de bord de l'instructeur
    Element Should Contain  css=#${widget_id} .widget-content-wrapper span.box-icon  1
    # On clique sur le lien "Voir +" du widget
    Click Element  css=#${widget_id} .widget-footer a
    # Le lien Voir + nous amène sur le listing 'Mes retours'
    # Il ne doit contenir qu'un seul retour
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Submenu In Menu Should Be Selected  instruction  consultation_mes_retours
    Page Title Should Be  Instruction > Consultations > Mes Retours
    First Tab Title Should Be  Consultation
    Page Should Contain  Les consultations marquées comme 'non lu' qui concernent des dossiers d'instruction dont je suis l'instructeur.
    Element Should Contain  css=#tab-consultation_mes_retours .pagination-text  1 - 1 enregistrement(s) sur 1
    # On va sur le listing 'Retours de ma division'
    # Il doit contenir deux retours
    Go To Submenu In Menu  instruction  consultation_retours_ma_division
    Page Title Should Be  Instruction > Consultations > Retours De Ma Division
    First Tab Title Should Be  Consultation
    Page Should Contain  Les consultations marquées comme 'non lu' qui concernent des dossiers d'instruction situés dans ma division.
    Element Should Contain  css=#tab-consultation_retours_ma_division .pagination-text  1 - 2 enregistrement(s) sur 2
    Element Should Contain  css=#tab-consultation_retours_ma_division table  Selma SAUNDERS
    # On va sur le listing 'Tous les retours'
    # Il doit contenir trois retours
    Go To Submenu In Menu  instruction  consultation_tous_retours
    Page Title Should Be  Instruction > Consultations > Tous Les Retours
    First Tab Title Should Be  Consultation
    Page Should Contain  Les consultations marquées comme 'non lu' qui concernent des dossiers d'instruction situés dans ma collectivité.
    Element Should Contain  css=#tab-consultation_tous_retours .pagination-text  1 - 3 enregistrement(s) sur 3

    # Filtre sur l'instructeur
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  consultation_retours
    Click On Form Portlet Action  om_widget  modifier
    Input Text  arguments  filtre=division
    Click On Submit Button

    # On se connecte en tant que "csanchez" (Profil 'INSTRUCTEUR')
    Depuis la page d'accueil  csanchez  csanchez
    # On vérifie que les consultations apparaissent bien sur le tableau de bord de l'instructeur
    Element Should Contain  css=#${widget_id} .widget-content-wrapper span.box-icon  2
    # On clique sur le lien "Voir +" du widget
    Click Element  css=#${widget_id} .widget-footer a
    # Le lien Voir + nous amène sur le listing 'Retours de ma division'
    # Il doit contenir deux retours
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Submenu In Menu Should Be Selected  instruction  consultation_retours_ma_division
    Page Title Should Be  Instruction > Consultations > Retours De Ma Division
    First Tab Title Should Be  Consultation
    Page Should Contain  Les consultations marquées comme 'non lu' qui concernent des dossiers d'instruction situés dans ma division.
    Element Should Contain  css=#tab-consultation_retours_ma_division .pagination-text  1 - 2 enregistrement(s) sur 2

    # Filtre sur l'instructeur
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  consultation_retours
    Click On Form Portlet Action  om_widget  modifier
    Input Text  arguments  filtre=aucun
    Click On Submit Button

    # On se connecte en tant que "csanchez" (Profil 'INSTRUCTEUR')
    Depuis la page d'accueil  csanchez  csanchez
    # On vérifie que les consultations apparaissent bien sur le tableau de bord de l'instructeur
    Element Should Contain  css=#${widget_id} .widget-content-wrapper span.box-icon  3
    # On clique sur le lien "Voir +" du widget
    Click Element  css=#${widget_id} .widget-footer a
    # Le lien Voir + nous amène sur le listing 'Tous les retours'
    # Il doit contenir trois retours
    Submenu In Menu Should Be Selected  instruction  consultation_tous_retours
    Page Title Should Be  Instruction > Consultations > Tous Les Retours
    First Tab Title Should Be  Consultation
    Page Should Contain  Les consultations marquées comme 'non lu' qui concernent des dossiers d'instruction situés dans ma collectivité.
    Element Should Contain  css=#tab-consultation_tous_retours .pagination-text  1 - 3 enregistrement(s) sur 3

    # Filtre sur l'instructeur
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du widget  consultation_retours
    Click On Form Portlet Action  om_widget  modifier
    Input Text  arguments  filtre=instructeur
    Click On Submit Button

    # On se connecte en tant que Profil 'INSTRUCTEUR'
    Depuis la page d'accueil  mjoyce  mjoyce
    # On vérifie que les consultations apparaissent bien sur le tableau de bord de l'instructeur
    Element Should Contain  css=#${widget_id} .widget-content-wrapper span.box-icon  1
    # On clique sur le lien "Voir +" du widget
    Click Element  css=#${widget_id} .widget-footer a
    # On clique sur le lien "59.01 Direction de l'Eau et de l'Assainissement" dans le tableau
    Click Link  ${di_04}
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#sousform-consultation #service
    #
    Page Title Should Contain  ${di_04}
    Page Title Should Contain  BOULAGE DAMIEN

    #
    Portlet Action Should Be In SubForm  consultation  marquer_comme_lu
    #
    Click On SubForm Portlet Action  consultation  marquer_comme_lu
    #
    Valid Message Should Contain In Subform  Mise à jour effectuée avec succès

    Portlet Action Should Not Be In SubForm  consultation  supprimer
    #
    Depuis la page d'accueil  mjoyce  mjoyce
    #
    # On vérifie que lorsqu'il n'y a aucune consultation, un message dans le widget 'Retours de consultation'
    # l'indique et que le lien Voir + n'est pas présent
    #
    Element Should Contain  css=#${widget_id} .widget-content-wrapper  Aucun retour de consultation non lu.
    Element Should Not Contain  css=#${widget_id}  Voir +

    #
    # On clique sur les trois listings liés pour vérifier qu'il n'y a aucun résultat
    #
    Go To Submenu In Menu  instruction  consultation_mes_retours
    Page Title Should Be  Instruction > Consultations > Mes Retours
    First Tab Title Should Be  Consultation
    Page Should Contain  Les consultations marquées comme 'non lu' qui concernent des dossiers d'instruction dont je suis l'instructeur.
    Element Should Contain  css=#tab-consultation_mes_retours .pagination-text  1 - 0 enregistrement(s) sur 0
    Element Should Not Contain  css=#tab-consultation_mes_retours table thead  instructeur
    Element Should Not Contain  css=#tab-consultation_mes_retours table thead  division
    Element Should Not Contain  css=#tab-consultation_mes_retours table thead  collectivité
    #
    Go To Submenu In Menu  instruction  consultation_retours_ma_division
    Page Title Should Be  Instruction > Consultations > Retours De Ma Division
    First Tab Title Should Be  Consultation
    Page Should Contain  Les consultations marquées comme 'non lu' qui concernent des dossiers d'instruction situés dans ma division.
    Element Should Contain  css=#tab-consultation_retours_ma_division .pagination-text  1 - 0 enregistrement(s) sur 0
    Element Should Contain  css=#tab-consultation_retours_ma_division table thead  instructeur
    Element Should Not Contain  css=#tab-consultation_retours_ma_division table thead  division
    Element Should Not Contain  css=#tab-consultation_retours_ma_division table thead  collectivité
    #
    Go To Submenu In Menu  instruction  consultation_tous_retours
    Page Title Should Be  Instruction > Consultations > Tous Les Retours
    First Tab Title Should Be  Consultation
    Page Should Contain  Les consultations marquées comme 'non lu' qui concernent des dossiers d'instruction situés dans ma collectivité.
    Element Should Contain  css=#tab-consultation_tous_retours .pagination-text  1 - 0 enregistrement(s) sur 0
    Element Should Contain  css=#tab-consultation_tous_retours table thead  instructeur
    Element Should Contain  css=#tab-consultation_tous_retours table thead  division
    Element Should Not Contain  css=#tab-consultation_tous_retours table thead  collectivité


Paramétrage d'un service et de l'édition PDF de la consultation
    [Documentation]  L'objet de ce TestCase est de vérifier que l'édition
    ...  paramétrée sur un service est correctement répercutée lors de la
    ...  consultation d'un service après finalisation et avant définalisation
    ...  et après définalisation.

    ##
    ## Étape 1
    ##
    # On ajoute deux états dont l'id commence par 'consultation_' : un qui
    # contient test_10_1 et l'autre test_10_2.
    # On ajoute un service en sélectionnant l'édition test_10_1.
    ##
    Depuis la page d'accueil  admin  admin
    Ajouter le état depuis le menu  consultation_testconsultation10_1  testconsultation10_1  test_10_1  test_10_1  Récapitulatif consultation  true  agglo
    Ajouter le état depuis le menu  consultation_testconsultation10_2  testconsultation10_2  test_10_2  test_10_2  Récapitulatif consultation  true  agglo
    &{service} =  Create Dictionary
    ...  abrege=ts10
    ...  libelle=test_service_10
    ...  edition=testconsultation10_1
    ...  om_collectivite=agglo
    Ajouter le service depuis le listing  ${service}

    ##
    ## Étape 2
    ##
    # On ajoute une consultation du service ajouté précédemment sur un dossier
    # et on vérifie que l'édition contient bien la chaine test_10_1 et ne
    # contient pas la chaine test_10_2.
    ##
    Depuis la page d'accueil  instr  instr
    Ajouter une consultation depuis un dossier  ${di_1}  ts10 - test_service_10
    Depuis le contexte de la consultation  ${di_1}  ts10 - test_service_10
    Click On SubForm Portlet Action  consultation  consulter_pdf
    Open PDF  file
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  test_10_1
    Page Should Not Contain  test_10_2
    Close PDF

    ##
    ## Étape 3
    ##
    # On modifie le paramétrage du service pour lui sélectionner l'édition
    # test_10_2.
    ##
    Depuis la page d'accueil  admin  admin
    &{service} =  Create Dictionary
    ...  edition=testconsultation10_2
    Modifier le service  ts10  test_service_10  ${service}

    ##
    ## Étape 4.1
    ##
    # On retourne sur la consultation précédente, on vérifie que l'édition
    # contient toujours bien la chaine test_10_1 et ne contient pas la chaine
    # test_10_2.
    ##
    Depuis la page d'accueil  instr  instr
    Depuis le contexte de la consultation  ${di_1}  ts10 - test_service_10
    Click On SubForm Portlet Action  consultation  consulter_pdf
    Open PDF  file
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  test_10_1
    Page Should Not Contain  test_10_2
    Close PDF
    ##
    ## Étape 4.2
    ##
    # Ensuite on définalise l'édition et on vérifie que l'édition contient bien
    # la chaine test_10_2 et ne contient pas la chaine test_10_1.
    ##
    Click On SubForm Portlet Action  consultation  unfinalise
    Valid Message Should Be In Subform  La définalisation du document s'est effectuée avec succès.
    Click On SubForm Portlet Action  consultation  consulter_pdf
    Open PDF  sousform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  test_10_2
    Page Should Not Contain  test_10_1
    Close PDF


TNR Bug Recherche sur le critère "Instructeur" de la recherche avancée de "Instruction > Consultations > Tous les retours" ne fonctionne pas

    [Documentation]  La recherche portait sur l'identifiant de l'instructeur
    ...  au lieu de porter sur son nom

    #
    Depuis la page d'accueil  instr1  instr
    #
    Go To Tab  dossier_autorisation
    Go To Submenu In Menu    instruction    consultation_tous_retours
    #
    Click Element  css=#toggle-advanced-display
    #
    Wait Until Element Is Visible  css=div#adv-search-adv-fields input#instructeur
    # On remplit
    Input Text  css=div#adv-search-adv-fields input#instructeur  Louis Laurent
    # On valide le formulaire de recherche
    Click On Search Button
    #
    Page Should Not Contain  Aucun enregistrement


TNR Bug Droits insuffisants sur l'onglet pièces sur la demande d'avis

    [Documentation]  L'accés à l'onglet pièce n'était pas possible

    # On se connecte en tant que "consu"
    Depuis la page d'accueil  consu  consu
    #
    Depuis la demande d'avis passée du dossier  ${di_1}
    On clique sur l'onglet  document_numerise  Pièce(s)
    Page Should Not Contain    Droits insuffisants. Vous n'avez pas suffisamment de droits pour acceder à cette page.


TNR Bug demande de consultation par le profil guichetsuivi

    [Documentation]  Vérification du dépôt de consultation par le profil
    ...  guichetsuivi

    &{args_petitionnaire_1} =  Create Dictionary
    ...  particulier_nom=DUPONT
    ...  particulier_prenom=Maurice
    ...  om_collectivite=MARSEILLE
    #
    &{args_demande_1} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=12/09/2015
    ...  om_collectivite=MARSEILLE
    #
    ${di_1} =  Ajouter la demande par WS  ${args_demande_1}  ${args_petitionnaire_1}

    Depuis la page d'accueil  guichetsuivi  guichetsuivi
    Ajouter une consultation depuis un dossier  ${di_1}  DAEWE - DAE - BUREAU ENTREPOT INDUSTRIE AGRICOLE


TNR Bug suppression de la pièce jointe à la modification d'une consultation

    [Documentation]  Vérifie que la pièce jointe d'une consultation ne disparaît
    ...  pas quand on modifie la consultation en tant qu'instructeur polyvalent.

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Roussel
    ...  particulier_prenom=Agnès
    ...  om_collectivite=MARSEILLE
    #
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=03/02/2016
    ...  om_collectivite=MARSEILLE
    #
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instrpoly  instrpoly
    Ajouter une consultation depuis un dossier  ${di}  59.01 - Direction de l'Eau et de l'Assainissement
    # Définalise la consultation pour pouvoir la modifier
    Depuis le contexte de la consultation  ${di}  59.01 - Direction de l'Eau et de l'Assainissement
    Click On SubForm Portlet Action  consultation  unfinalise

    # Ajoute une pièce à la consultation
    &{piece_values} =  Create Dictionary
    ...  fichier_upload=testImportManuel.pdf
    ...  date_demande=03/02/2016
    ...  avis_consultation=Tacite
    ${nom_piece} =  Ajouter une pièce à la consultation  ${piece_values}

    # Vérifie que l'instructeur polyvalent peut modifier la pièce
    &{piece_values_2} =  Create Dictionary
    ...  fichier_upload=testImportManuel2.pdf
    ${nom_piece_2} =  Ajouter une pièce à la consultation  ${piece_values_2}

    # Nouvelles valeurs de la consultation
    &{saisie_values} =  Create Dictionary
    ...  avis_consultation=Favorable

    Modifier la consultation  ${saisie_values}
    Click On Back Button In Subform
    # Vérifie que le fichier est toujours ajouté à la consultation
    Page Should Contain  ${nom_piece_2}


Ajout consultation et rendu d'avis par le profil instructeur service

    [Documentation]  Vérifie l'affichage de l'avis rendu, de la motivation et du
    ...  fichier
    ...  Vérifier que ce testcase n'est pas un doublon du "090.Réponse à une consultation par le service consulté" (CU n°4)
    #
    Depuis la page d'accueil  instrserv  instrserv
    Ajouter une consultation depuis un dossier  ${di_3}  59.01 - Direction de l'Eau et de l'Assainissement

    &{args_avis_consultation} =  Create Dictionary
    ...  avis_consultation=Favorable
    ...  motivation=Pas de réserves
    ...  fichier_upload=testImportManuel.pdf

    Rendre l'avis sur la consultation du dossier  ${di_3}  ${args_avis_consultation}
    # On clique sur l'action édition
    Depuis la demande d'avis passée du dossier  ${di_3}

    Element Should Contain  avis_consultation  Favorable
    Element Should Contain  motivation  Pas de réserves
    Element Should Contain  fichier  consultation_avis


TNR Vérifie que le fichier est supprimé à la suppression de la consultation

    [Documentation]  Vérifie dans le filestorage si le fichier de l'édition de
    ...  la consultation est correctement supprimé lors de la suppression de la
    ...  consultation.

   Depuis la page d'accueil  guichet  guichet
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Goguen
    ...  particulier_prenom=Diane
    #
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  date_demande=29/04/2016
    #
    ${di} =  Ajouter la nouvelle demande depuis le tableau de bord  ${args_demande}  ${args_petitionnaire}
    #
    Depuis la page d'accueil  instr  instr
    Ajouter une consultation depuis un dossier  ${di}  59.70 - AUTRE
    # Récupération de l'UID
    Depuis le contexte de la consultation  ${di}  59.70 - AUTRE
    ${uid} =  Get Value  om_fichier_consultation
    ${path_1} =  Get Substring  ${uid}  0  2
    ${path_2} =  Get Substring  ${uid}  0  4
    # Vérification dans le filestorage
    File Should Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}
    File Should Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}.info
    #
    Depuis le contexte de la consultation  ${di}  59.70 - AUTRE
    # On clique sur l'action de définalisation
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Click On SubForm Portlet Action  consultation  unfinalise
    # On vérifie le message de validation
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Valid Message Should Be  La définalisation du document s'est effectuée avec succès.
    #
    Supprimer la consultation depuis le contexte du dossier d'instruction  ${di}  59.70 - AUTRE
    # Vérification dans le filestorage
    File Should Not Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}
    File Should Not Exist  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}.info

Vérification de la visibilité des consultation dans l'édition
    [Documentation]  Test des actions direct de tableau et de formulaire
    ...  masquant/affichant les consultations dans les éditions.


    # Création du jeu de données

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Massé
    ...  particulier_prenom=Astrid
    ...  om_collectivite=MARSEILLE

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ...  date_demande=27/11/2015

    #Creation de la variable du text à chercher dans le dossier
    ${service_libelle} =  Set Variable  Service Prévention et Gestion des Risques ERP
    #Creation de la variable de nom de service pour la creation de consultation
    ${service} =  Set Variable  59.10 - Service Prévention et Gestion des Risques ERP

    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}

    Depuis la page d'accueil  instr  instr
    Ajouter une consultation depuis un dossier  ${di}  ${service}

    # Cas 1 : On affiche/masque les consultations via leur formulaire
    Click On Back Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  ${service}
    Click On SubForm Portlet Action  consultation  masquer_dans_edition
    Valid Message Should Be In Subform  La consultation est masquée dans les éditions.
    # Vérification du changement de l'état de la consultation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Static Value Should Be  css=#visible  Non

    Depuis le contexte du dossier d'instruction  ${di}
    # On click pour créer le PDF Récapitulatif
    Click On Form Portlet Action  dossier_instruction  edition
    Open PDF  form
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Consultation
    # On vérifie l'abscence de la consultation
    Page Should Not Contain  ${service_libelle}
    Close PDF

    # Affichage de la saisie du rapport
    Depuis le contexte du rapport d'instruction  ${di}
    Click On Submit Button In Subform
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform

    Depuis le contexte du rapport d'instruction  ${di}
    # On clique sur l'action édition
    Click On SubForm Portlet Action  rapport_instruction  edition
    Open PDF  sousform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ADRESSE DU DEMANDEUR TITULAIRE
    # On vérifie que la valeur de test n'est pas présente
    Page Should Not Contain  ${service_libelle}
    Close PDF


    Depuis l'onglet consultation du dossier  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  ${service}
    # On clique sur l'action de masquer le document
    Click On SubForm Portlet Action  consultation  afficher_dans_edition
    Valid Message Should Be In Subform  La consultation est affichée dans les éditions.
    # Vérification du changement de l'état de la consultation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Static Value Should Be  css=#visible  Oui
    Click On Back Button In Subform

    # Cas 2 : On affiche/masque les consultations via leur listing

    # On vérifie que l'action du tableau fonctionne
    Element Should Not Be Visible  css=a[id*='afficher_dans_edition']
    Click Element  css=a[id*='masquer_dans_edition']
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  La consultation est masquée dans les éditions.

    Sleep  2
    Element Should Not Be Visible  css=a[id*='masquer_dans_edition']
    Click Element  css=a[id*='afficher_dans_edition']
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  La consultation est affichée dans les éditions.

    Depuis le contexte du dossier d'instruction  ${di}
    # On click pour créer le PDF Récapitulatif
    Click On Form Portlet Action  dossier_instruction  edition
    Open PDF  form
    # On vérifie la presence de la consultation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${service_libelle}
    Close PDF

    Depuis le contexte du rapport d'instruction  ${di}
    # On clique sur l'action édition
    Click On SubForm Portlet Action  rapport_instruction  edition
    Open PDF  sousform
    # On vérifie que la valeur de test est présente
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${service_libelle}
    Close PDF

    # Cas 3 : On vérifie l’interaction des deux types d'action

    Depuis l'onglet consultation du dossier  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  ${service}
    Click On SubForm Portlet Action  consultation  masquer_dans_edition
    Valid Message Should Be In Subform  La consultation est masquée dans les éditions.
    # Vérification du changement de l'état de la consultation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Static Value Should Be  css=#visible  Non
    Click On Back Button In Subform

    # On vérifie que l'action du tableau fonctionne en croisent avec depuis la consultaion de la consultation
    Element Should Not Be Visible  css=a[id*='masquer_dans_edition']
    Click Element  css=a[id*='afficher_dans_edition']
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  La consultation est affichée dans les éditions.
    Element Should Not Be Visible  css=a[id*='afficher_dans_edition']
    Click Element  css=a[id*='masquer_dans_edition']
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  La consultation est masquée dans les éditions.

    Depuis l'onglet consultation du dossier  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  ${service}
    # On clique sur l'action de masquer le document
    Click On SubForm Portlet Action  consultation  afficher_dans_edition
    Valid Message Should Be In Subform  La consultation est affichée dans les éditions.
    # Vérification du changement de l'état de la consultation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Static Value Should Be  css=#visible  Oui
    Click On Back Button In Subform

    # On vérifie que l'action du tableau fonctionne en croisent avec depuis la consultaion
    Element Should Be Visible  css=a[id*='masquer_dans_edition']

    # Dans le cas où le dossier d'instruction est clôturé et que l'utilisateur
    # est un instructeur de même division n'ayant pas de permission bypass, on
    # vérifie que les actions ne sont plus disponible sur le listing et sur le
    # formulaire
    Ajouter une instruction au DI  ${di}  accepter un dossier sans réserve
    Depuis l'onglet consultation du dossier  ${di}
    Wait Until Page Contains  ${service}
    Element Should Not Be Visible  css=a[id*='afficher_dans_edition']
    Element Should Not Be Visible  css=a[id*='masquer_dans_edition']
    Click On Link  ${service}
    Portlet Action Should Not Be In SubForm  consultation  afficher_dans_edition
    Portlet Action Should Not Be In SubForm  consultation  masquer_dans_edition

TNR Vérification du fonctionnement de la rediredtion
    [Documentation]  Le but de ce test case est de vérifier si la redirection
    ...  entre la liste des consultations de mes retours et une consultation
    ...  fonctionne. On va donc créer un dossier une consultation, rendre un avi
    ...  et vérifier l'ajout de consultations multiples.


    Depuis la page d'accueil  instr  instr
    Click Element  css=#widget_3 a
    Click Link  ${di_2}
    Click On Back Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-consultation-corner-ajouter_multiple
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  t10_575_0_
    ${status} =  Run Keyword And Return Status  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#t10_575_0_.liste_gauche_service_selected

    # Sélection du dans la liste de Direction de l'Eau et de l'Assainissement
    Run Keyword If  ${status} == False  Click Element  t10_575_0_

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  css=#t10_575_0_.liste_gauche_service_selected
    Click On Back Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link  59.01 - Direction de l'Eau et de l'Assainissement
    ${status} =  Run Keyword And Return Status  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Be Visible  css=div > table
    Run Keyword If  ${status} == False  Click Link  59.01 - Direction de l'Eau et de l'Assainissement
    Click On SubForm Portlet Action  consultation  marquer_comme_lu