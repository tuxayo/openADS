*** Settings ***
Documentation     TestSuite "Documentation" : cette suite permet d'extraire
...    automatiquement les captures à destination de la documentation.
# On inclut les mots-clefs
Resource    resources/resources.robot
# On ouvre et on ferme le navigateur respectivement au début et à la fin
# du Test Suite.
Suite Setup    For Suite Setup
Suite Teardown    For Suite Teardown
# A chaque début de Test Case on positionne la taille de la fenêtre
# pour obtenir des captures homogènes
Test Setup    Set Window Size  ${1280}  ${1024}




*** Keywords ***
Highlight heading
    [Arguments]  ${locator}

    Update element style  ${locator}  margin-top  0.75em
    Highlight  ${locator}

Capturer le menu et le dashboard des profils
    [Arguments]  ${logins}

    #
    :FOR  ${login}  IN  @{logins}
    #
    \  Depuis la page d'accueil  ${login}  ${login}
    \  Go To Dashboard
    #
    \  Capture and crop page screenshot  screenshots/profils/dashboard_${login}.png
    \  ...  content
    #
    \  Capture and crop page screenshot  screenshots/profils/menu_${login}.png
    \  ...  menu-list


Capturer le menu des profils
    [Arguments]  ${logins}

    #
    :FOR  ${login}  IN  @{logins}
    #
    \  Depuis la page d'accueil  ${login}  ${login}
    \  Capture and crop page screenshot  screenshots/profils/menu_${login}.png
    \  ...  menu-list


*** Test Cases ***
Prérequis

    [Documentation]  L'objet de ce 'Test Case' est de respecter les prérequis
    ...    nécessaires aux captures d'écran.

    [Tags]  doc

    # Création des répertoires destinés à recevoir les captures d'écran
    # selon le respect de l'architecture de la documentation
    Create Directory    results/screenshots
    Create Directory    results/screenshots/ergonomie
    Create Directory    results/screenshots/profils


Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de de
    ...    données cohérent pour les scénarios fonctionnels qui suivent.

    [Tags]  doc

    Depuis la page d'accueil  admin  admin
    &{service} =  Create Dictionary
    ...  abrege=95A
    ...  libelle=Direction de la circulation
    ...  edition=Consultation - Demande d'avis
    ...  om_collectivite=MARSEILLE
    Ajouter le service depuis le listing  ${service}
    &{lien_service_om_utilisateur} =  Create Dictionary
    ...  om_utilisateur=Service consulté interne
    ...  service=Direction de la circulation
    Ajouter lien service/utilisateur  ${lien_service_om_utilisateur}

    # Ajout du paramétrage des taxes pour la colllectivité MARSEILLE
    &{args_taxes} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  val_forf_surf_cstr=705
    ...  val_forf_empl_tente_carav_rml=3000
    ...  val_forf_empl_hll=10000
    ...  val_forf_surf_piscine=200
    ...  val_forf_nb_eolienne=3000
    ...  val_forf_surf_pann_photo=10
    ...  val_forf_nb_parking_ext=2000
    ...  tx_depart=2.00
    ...  tx_comm_secteur_1=1.00
    ...  tx_rap=0.40
    Ajouter le paramétrage des taxes  ${args_taxes}

    # On affiche les divisions pour les affectations automatiques
    Modifier le paramètre   option_afficher_division  true  agglo

    #
    &{args_petitionnaire_1} =  Create Dictionary
    ...  particulier_nom=DUPONT
    ...  particulier_prenom=Jacques
    ...  om_collectivite=MARSEILLE
    #
    &{args_demande_1} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  date_demande=12/04/2015
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    #
    Depuis la page d'accueil  guichet  guichet
    ${di_1} =  Ajouter la demande par WS  ${args_demande_1}  ${args_petitionnaire_1}
    Set Suite Variable  ${di_1}

    #
    &{args_petitionnaire_2} =  Create Dictionary
    ...  particulier_nom=Boulanger
    ...  particulier_prenom=Denis
    #
    @{ref_cad} =  Create List  001  AA  007
    &{args_demande_2} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de démolir
    ...  date_demande=20/05/2016
    ...  terrain_references_cadastrales=${ref_cad}
    #
    &{args_petitionnaire_3} =  Create Dictionary
    ...  qualite=personne morale
    ...  personne_morale_denomination=The Network Chef Inc.
    ...  personne_morale_raison_sociale=Société
    ...  personne_morale_civilite=Monsieur
    ...  personne_morale_nom=Barteaux
    ...  personne_morale_prenom=René
    ...  om_collectivite=MARSEILLE
    #
    &{args_demande_3} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE

    Depuis la page d'accueil  guichet  guichet
    ${di_2} =  Ajouter la nouvelle demande depuis le tableau de bord  ${args_demande_2}  ${args_petitionnaire_2}
    Set Suite Variable  ${di_2}

    #
    ${di_3} =  Ajouter la demande par WS  ${args_demande_3}  ${args_petitionnaire_3}
    Set Suite Variable  ${di_3}

    #
    Depuis la page d'accueil  instrpoly  instrpoly
    Ajouter une consultation depuis un dossier  ${di_1}  59.01 - Direction de l'Eau et de l'Assainissement
    Ajouter une consultation depuis un dossier  ${di_1}  95A - Direction de la circulation

    # Pour que le dossier soit affiché dans le widget dossiers_evenement_incomplet_majoration
    Ajouter une instruction au DI et la finaliser  ${di_1}  majoration + DPC hors SS  ${date_ddmmyyyy}
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_envoi_rar  ${date_ddmmyyyy}
    Click On Submit Button In Subform

    ${code_barres} =  Récupérer le code barres de l'instruction  ${di_3}  Notification du delai legal maison individuelle
    Ajouter une instruction au DI et la finaliser  ${di_3}  accepter un dossier sans réserve
    ${code_barres2} =  Récupérer le code barres de l'instruction  ${di_3}  accepter un dossier sans réserve
    Set Suite Variable  ${code_barres}
    Set Suite Variable  ${code_barres2}

    # Connexion en admin pour pouvoir modifier les dates de suivi
    Depuis la page d'accueil  admin  admin

    # Pour que le dossier soit affiché dans le widget dossiers_evenement_incomplet_majoration
    Depuis la page d'accueil  admin  admin
    Ajouter une instruction au DI et la finaliser  ${di_2}  majoration + DPC hors SS  ${date_ddmmyyyy}
    Click On SubForm Portlet Action  instruction  modifier_suivi
    Input Datepicker  date_envoi_rar  ${date_ddmmyyyy}
    Click On Submit Button In Subform

    ## Changement du filtre en aucun (collectivite) pour avoir les 2 dossiers
    # Depuis la page d'accueil  admin  admin
    Go To Tab  om_widget
    Click On Link    dossiers_evenement_incomplet_majoration
    Click On Form Portlet Action    om_widget    modifier
    Input Text    arguments    filtre=aucun
    Click On Submit Button

    &{args_avis_consultation_1} =  Create Dictionary
    ...  avis_consultation=Favorable

    #
    Depuis la page d'accueil  consu  consu

    Rendre l'avis sur la consultation du dossier  ${di_1}  ${args_avis_consultation_1}

    Depuis la page d'accueil  admin  admin

    # On active l'option de notification par message
    Modifier le paramètre  option_notification_piece_numerisee  true

    # On ajoute un document numérisé par DI
    &{document_numerise_values} =  Create Dictionary
    ...  uid_upload=testImportManuel.pdf
    ...  date_creation=${date_ddmmyyyy}
    ...  document_numerise_type=autres pièces composant le dossier (A0)
    Ajouter une pièce depuis le dossier d'instruction  ${di_1}  ${document_numerise_values}

    # Ajoute des infractions dont la date de réception est dépassée de 10 mois
    # Ces infractions seront affichées dans les widgets 'Alerte parquet' et
    #'Alerte visite'
    Depuis la page d'accueil  assist  assist
    ${date_di_inf_1_db} =  Subtract Time From Date  ${DATE_FORMAT_YYYY-MM-DD}  300 days  result_format=%Y-%m-%d
    ${date_di_inf_1_form} =  Convert Date  ${date_di_inf_1_db}  result_format=%d/%m/%Y
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Charrette
    ...  particulier_prenom=Ophelia
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Moreau
    ...  particulier_prenom=Marcel
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ...  date_demande=${date_di_inf_1_form}
    ${args_peti} =  Create Dictionary

    ${di_inf_1} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    #
    ${date_di_inf_2_db} =  Subtract Time From Date  ${DATE_FORMAT_YYYY-MM-DD}  300 days  result_format=%Y-%m-%d
    ${date_di_inf_2_form} =  Convert Date  ${date_di_inf_2_db}  result_format=%d/%m/%Y
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Raymond
    ...  particulier_prenom=Bertrand
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Bonsaint
    ...  particulier_prenom=Philippe
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ...  date_demande=${date_di_inf_2_form}
    ${args_peti} =  Create Dictionary
    ${di_inf_2} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}

    # Ajoute des infractions non affectées à des technicien
    # Ces infractions seront affichées dans le widget 'Les infractions non
    # affectées'
    # On supprime l'affectation automatique du technicien sur les infractions
    Depuis la page d'accueil  admin  admin
    Supprimer l'affectation depuis le menu  null  Infraction
    #
    Depuis la page d'accueil  assist  assist
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Lagueux
    ...  particulier_prenom=Anne
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Hachée
    ...  particulier_prenom=Diane
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  om_collectivite=MARSEILLE
    ...  demande_type=Dépôt Initial IN
    ${args_peti} =  Create Dictionary
    ${di_inf_3} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    #
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Grandbois
    ...  particulier_prenom=Stéphane
    ...  om_collectivite=MARSEILLE
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Patel
    ...  particulier_prenom=Nicolas
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ${args_peti} =  Create Dictionary
    ${di_inf_4} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    # On ajoute l'affectation automatique du technicien
    Depuis la page d'accueil  admin  admin
    &{args_affectation} =  Create Dictionary
    ...  instructeur=Juriste (H)
    ...  instructeur_2=Technicien (H)
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Infraction
    Ajouter l'affectation depuis le menu  ${args_affectation}

    # Ajoute des recours dont la date de réception est comprise dans le mois
    # courant
    # Ces recours seront affichés dans le widget 'Mes clôtures'
    # On ajoute une autorisation à contester
    Depuis la page d'accueil  guichet  guichet
    #
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Charlebois
    ...  particulier_prenom=Agate
    ...  om_collectivite=MARSEILLE
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  om_collectivite=MARSEILLE
    ${di_conteste} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    #
    Depuis la page d'accueil  assist  assist
    ${date_di_re_1_db} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  10 days  result_format=%Y-%m-%d
    ${date_di_re_1_form} =  Convert Date  ${date_di_re_1_db}  result_format=%d/%m/%Y
    &{args_requerant} =  Create Dictionary
    ...  particulier_nom=Henrichon
    ...  particulier_prenom=Aurore
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  requerant_principal=${args_requerant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Recours gracieux
    ...  demande_type=Dépôt Initial REG
    ...  om_collectivite=MARSEILLE
    ...  autorisation_contestee=${di_conteste}
    ${args_peti} =  Create Dictionary
    ${di_re_1} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    #
    ${date_di_re_2_db} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  10 days  result_format=%Y-%m-%d
    ${date_di_re_2_form} =  Convert Date  ${date_di_re_2_db}  result_format=%d/%m/%Y
    &{args_requerant} =  Create Dictionary
    ...  particulier_nom=Gagné
    ...  particulier_prenom=Daniel
    ...  om_collectivite=MARSEILLE
    &{args_autres_demandeurs} =  Create Dictionary
    ...  requerant_principal=${args_requerant}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Recours gracieux
    ...  demande_type=Dépôt Initial REG
    ...  autorisation_contestee=${di_conteste}
    ...  om_collectivite=MARSEILLE
    ${args_peti} =  Create Dictionary
    ${di_re_2} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    # On saisit les dates de clôture des recours
    Depuis la page d'accueil  juriste  juriste
    Ajouter une instruction au DI  ${di_re_1}  Clôture de l'instruction  ${date_di_re_1_form}  recours
    Ajouter une instruction au DI  ${di_re_2}  Clôture de l'instruction  ${date_di_re_2_form}  recours

    # Ajoute des infractions dont la date d'audience est comprise dans le mois
    # courant
    # Ces infractions seront affichées dans le widget 'Les audiences'
    Depuis la page d'accueil  assist  assist
    ${date_di_inf_5_db} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  10 days  result_format=%Y-%m-%d
    ${date_di_inf_5_form} =  Convert Date  ${date_di_inf_5_db}  result_format=%d/%m/%Y
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Courtois
    ...  om_collectivite=MARSEILLE
    ...  particulier_prenom=Christine
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Blais
    ...  om_collectivite=MARSEILLE
    ...  particulier_prenom=Eugenia
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Infraction
    ...  date_demande=${date_di_inf_5_form}
    ${args_peti} =  Create Dictionary
    ${di_inf_5} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    #
    ${date_di_inf_6_db} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  10 days  result_format=%Y-%m-%d
    ${date_di_inf_6_form} =  Convert Date  ${date_di_inf_6_db}  result_format=%d/%m/%Y
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Legault
    ...  om_collectivite=MARSEILLE
    ...  particulier_prenom=Liane
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Brisebois
    ...  om_collectivite=MARSEILLE
    ...  particulier_prenom=Manon
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Infraction
    ...  date_demande=${date_di_inf_6_form}
    ${args_peti} =  Create Dictionary
    ${di_inf_6} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    # On saisit la date d'audience dans les données techniques
    Depuis la page d'accueil  juriste  juriste
    &{donnees_techniques_values} =  Create Dictionary
    ...  ctx_date_audience=${date_di_inf_5_form}
    Saisir les données techniques du dossier infraction  ${di_inf_5}  ${donnees_techniques_values}
    &{donnees_techniques_values} =  Create Dictionary
    ...  ctx_date_audience=${date_di_inf_6_form}
    Saisir les données techniques du dossier infraction  ${di_inf_6}  ${donnees_techniques_values}

    # Ajoute des infractions qui ont un AIT signé
    # Ces infraction seront affichées dans les widgets 'Mes AIT' et 'Les AIT'
    Depuis la page d'accueil  assist  assist
    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Courtois
    ...  om_collectivite=MARSEILLE
    ...  particulier_prenom=Christine
    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Blais
    ...  om_collectivite=MARSEILLE
    ...  particulier_prenom=Eugenia
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  demande_type=Dépôt Initial IN
    ...  dossier_autorisation_type_detaille=Infraction
    ...  om_collectivite=MARSEILLE
    ${args_peti} =  Create Dictionary
    ${di_inf_7} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    #
    &{args_contrevenant} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  particulier_nom=Talon
    ...  particulier_prenom=Petrie
    &{args_plaignant} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  particulier_nom=Baril
    ...  particulier_prenom=Martin
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ${args_peti} =  Create Dictionary
    ${di_inf_8} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    # On saisit les date d'ait et de retour signature
    Depuis la page d'accueil  juriste  juriste
    Ajouter une instruction au DI et la finaliser  ${di_inf_7}  Arrêté interruptif des travaux  null  infraction
    &{args_instruction} =  Create Dictionary
    ...  date_retour_signature=${DATE_FORMAT_DD/MM/YYYY}
    Modifier le suivi des dates  ${di_inf_7}  Arrêté interruptif des travaux  ${args_instruction}  infraction
    Ajouter une instruction au DI et la finaliser  ${di_inf_8}  Arrêté interruptif des travaux  null  infraction
    &{args_instruction} =  Create Dictionary
    ...  date_retour_signature=${DATE_FORMAT_DD/MM/YYYY}
    Modifier le suivi des dates  ${di_inf_8}  Arrêté interruptif des travaux  ${args_instruction}  infraction

    # Ajoute des infractions dont la date de contradictoire est supérieure ou
    # égale à la date du jour + 3 semaines, sans date de retour de
    # contradictoire, sans événements de type 'Annlation de contradictoire' et
    # sans AIT
    # Ces infraction seront affichées dans les widgets 'Mes contradictoires' et
    # 'Les contradictoires'
    Depuis la page d'accueil  assist  assist
    ${date_di_inf_9_db} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  28 days  result_format=%Y-%m-%d
    ${date_di_inf_9_form} =  Convert Date  ${date_di_inf_9_db}  result_format=%d/%m/%Y
    &{args_contrevenant} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  particulier_nom=Archambault
    ...  particulier_prenom=Corette
    &{args_plaignant} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  particulier_nom=Cantin
    ...  particulier_prenom=Joanna
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  date_demande=${date_di_inf_9_form}
    ${args_peti} =  Create Dictionary
    ${di_inf_9} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    #
    ${date_di_inf_10_db} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  28 days  result_format=%Y-%m-%d
    ${date_di_inf_10_form} =  Convert Date  ${date_di_inf_10_db}  result_format=%d/%m/%Y
    &{args_contrevenant} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  particulier_nom=Archambault
    ...  particulier_prenom=Corette
    &{args_plaignant} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  particulier_nom=Cantin
    ...  particulier_prenom=Joanna
    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}
    &{args_demande} =  Create Dictionary
    ...  om_collectivite=MARSEILLE
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  date_demande=${date_di_inf_10_form}
    ${args_peti} =  Create Dictionary
    ${di_inf_10} =  Ajouter la demande par WS  ${args_demande}  ${args_peti}  ${args_autres_demandeurs}
    # On saisit une date de contradictoire
    Depuis la page d'accueil  juriste  juriste
    Ajouter une instruction au DI  ${di_inf_9}  Contradictoire  ${date_di_inf_9_form}  infraction
    Ajouter une instruction au DI  ${di_inf_10}  Contradictoire  ${date_di_inf_10_form}  infraction

    # Renseigne les données nécessaires au calcul des taxes
    Depuis la page d'accueil  instr  instr
    &{args_dt_taxes} =  Create Dictionary
    ...  tax_surf_tot_cstr=160
    ...  tax_su_princ_surf1=160
    ...  tax_sup_bass_pisc_cr=50
    ...  tax_am_statio_ext_cr=2
    ...  tax_surf_loc_arch=0.5
    ...  tax_surf_pisc_arch=2
    ...  mtn_exo_ta_part_commu=0
    ...  mtn_exo_ta_part_depart=0
    ...  mtn_exo_ta_part_reg=0
    ...  mtn_exo_rap=0
    Modifier les données techniques pour le calcul des impositions  ${di_1}  ${args_dt_taxes}

    # On ajoute un service qui sera lié à l'utilisateur ayant le profil de
    # service consulté interne
    Depuis la page d'accueil  admin  admin
    &{service} =  Create Dictionary
    ...  abrege=95A
    ...  libelle=Direction de la circulation
    ...  edition=Consultation - Demande d'avis
    ...  om_collectivite=MARSEILLE
    Ajouter le service depuis le listing  ${service}
    &{lien_service_om_utilisateur} =  Create Dictionary
    ...  om_utilisateur=Service consulté étendu
    ...  service=Direction de la circulation
    Ajouter lien service/utilisateur  ${lien_service_om_utilisateur}

    # On ajoute un service qui sera lié à l'utilisateur ayant le profil de
    # service consulté étendu
    &{service} =  Create Dictionary
    ...  abrege=96B
    ...  libelle=Direction de la circulation piétonne
    ...  edition=Consultation - Demande d'avis
    ...  om_collectivite=MARSEILLE
    Ajouter le service depuis le listing  ${service}
    &{lien_service_om_utilisateur} =  Create Dictionary
    ...  om_utilisateur=Service consulté étendu
    ...  service=Direction de la circulation piétonne
    Ajouter lien service/utilisateur  ${lien_service_om_utilisateur}


CE des menus, widgets et tableaux de bord

    [Documentation]  L'objet de ce 'Test Case' est de réaliser les captures
    ...  d'écran des menus, widgets et tableaux de bord à destination de la
    ...  documentation.

    [Tags]  doc

    #
    # WIDGETS
    #

    Depuis la page d'accueil    adminfonct    adminfonct
    Go To Dashboard
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_nouvelle_demande_dossier_encours.png
    ...    css=div.widget_nouvelle_demande_dossier_encours

    #
    Depuis la page d'accueil    assist    assist
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_recherche_dossier_par_type.png
    ...    css=div.widget_recherche_dossier_par_type
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_contentieux_inaffectes.png
    ...    css=div.widget_dossier_contentieux_inaffectes
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_contentieux_ait.png
    ...    css=div.widget_dossier_contentieux_ait
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_contentieux_contradictoire.png
    ...    css=div.widget_dossier_contentieux_contradictoire

    #
    Depuis la page d'accueil    guichet    guichet
    Go To Dashboard
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_nouvelle_demande_nouveau_dossier.png
    ...    css=div.widget_nouvelle_demande_nouveau_dossier
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_nouvelle_demande_autre_dossier.png
    ...    css=div.widget_nouvelle_demande_autre_dossier
    #
    Depuis la page d'accueil    instr    instr
    Go To Dashboard
    #
    Capture and crop page screenshot  screenshots/ergonomie/tableau-de-bord-exemple.png
    ...    css=#content
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_infos_profil.png
    ...    css=div.widget_infos_profil
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossiers_limites.png
    ...    css=div.widget_dossiers_limites
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_recherche_dossier.png
    ...    css=div.widget_recherche_dossier
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_consultation_retours.png
    ...    css=div.widget_consultation_retours
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_messages_retours.png
    ...    css=div.widget_messages_retours
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossiers_evenement_incomplet_majoration.png
    ...    css=div.widget_dossiers_evenement_incomplet_majoration

    #
    Depuis la page d'accueil    tech    tech
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_contentieux_alerte_parquet.png
    ...    css=div.widget_dossier_contentieux_alerte_parquet
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_contentieux_alerte_visite.png
    ...    css=div.widget_dossier_contentieux_alerte_visite

    #
    Depuis la page d'accueil    juriste    juriste
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_contentieux_clotures.png
    ...    css=div.widget_dossier_contentieux_clotures
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_contentieux_audience.png
    ...    css=div.widget_dossier_contentieux_audience
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_contentieux_ait.png
    ...    css=div.widget_dossier_contentieux_ait
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_contentieux_recours.png
    ...    css=div.widget_dossier_contentieux_recours
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_contentieux_infraction.png
    ...    css=div.widget_dossier_contentieux_infraction

    #
    # MENUS ET DASHBOARDS
    #

    @{logins_menu_dashboard}  Create List
    ...  admin
    ...  adminfonct
    ...  admingen
    ...  suivi
    ...  chef
    ...  divi
    ...  guichet
    ...  guichetsuivi
    ...  instr
    ...  instrserv
    ...  instrpoly
    ...  instrpolycomm
    ...  qualif
    ...  visuda
    ...  visudadi
    ...  dirinf
    ...  dirrec
    ...  dirconsu
    ...  respinf
    ...  tech
    ...  juriste
    ...  chefctx
    ...  assist

    Capturer le menu et le dashboard des profils  ${logins_menu_dashboard}

    @{logins_menu}  Create List
    ...  consuint
    ...  consuetendu
    ...  consu
    ...  consudi


    Capturer le menu des profils  ${logins_menu}


CE des dossiers d'instruction

    [Documentation]  L'objet de ce 'Test Case' est de réaliser les captures d'écran
    ...    à destination de la documentation.

    [Tags]  doc

    #
    # MESSAGES
    #

    Depuis la page d'accueil    instr    instr
    Depuis l'onglet des messages du dossier d'instruction  ${di_1}
    #
    Capture and crop page screenshot  screenshots/instruction_dossier_message_tab.png
    ...    formulaire
    #
    Click On Link  Ajout de pièce(s)
    #
    Capture and crop page screenshot  screenshots/instruction_dossier_message_form.png
    ...    sousform-dossier_message

    Depuis le contexte du rapport d'instruction  ${di_1}

    # Multiline string with newlines
    ${analyse_reglementaire}=  catenate  SEPARATOR=\n
    ...  Accès (article 3) : Conforme/Non Conforme
    ...  ${EMPTY}
    ...  Réseaux (article 4) : Conforme/Non Conforme
    ...  ${EMPTY}
    ...  Implantation (articles 6 7 8) : Conforme/Non Conforme
    ...  (implantation à m de la limite séparative la plus proche pour une différence d'altitude de m, et à plus de m de l'alignement de la voie)
    ...  ${EMPTY}
    ...  Emprise au sol (article 9) : Conforme/Non Conforme/Non réglementé
    ...  ${EMPTY}
    ...  Hauteur (article 10) : Conforme/Non Conforme
    ...  (m pour une hauteur maxi de m)
    ...  ${EMPTY}
    ...  Aspect architectural (article11) : Conforme/Non Conforme
    ...  ${EMPTY}
    ...  Stationnement (article 12) : Conforme/Non Conforme
    ...  (surface totale de plancher totale : m²)
    ...  dans le bâtiment : en surface :
    ...  ${EMPTY}
    ...  Espaces Verts (article 13) : Conforme/Non Conforme
    ...  ${EMPTY}
    ...  C.O.S (article 14) et surface des terrains (article5) : Non réglementé
    ...  ${EMPTY}
    ...  Taxes et redevances :
    ...  Taxe aménagement : oui/non
    ...  Redevance archéologie : oui/non

    Input HTML  analyse_reglementaire_om_html  ${analyse_reglementaire}

    Capture and crop page screenshot  screenshots/instruction_portlet_rapport_instruction.png
    ...    sousform-rapport_instruction

    # Screenshot pour la qualification ERP
    Depuis le formulaire de modification du dossier d'instruction  ${di_1}
    Highlight heading  css=#erp
    Capture and crop page screenshot  screenshots/instruction_qualification_erp.png
    ...  css=#fieldset-form-dossier_instruction-qualification

    Depuis la page d'accueil  admin  admin

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Mylène
    ...  particulier_prenom=Françoise
    ...  om_collectivite=MARSEILLE

    @{ref_cad} =  Create List  001  AA  007

    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ...  om_collectivite=MARSEILLE

    &{args_contrevenant} =  Create Dictionary
    ...  particulier_nom=Mélisande
    ...  particulier_prenom=Amélie
    ...  om_collectivite=MARSEILLE

    &{args_plaignant} =  Create Dictionary
    ...  particulier_nom=Wanda
    ...  particulier_prenom=Manon
    ...  om_collectivite=MARSEILLE

    &{args_autres_demandeurs} =  Create Dictionary
    ...  contrevenant_principal=${args_contrevenant}
    ...  plaignant_principal=${args_plaignant}

    &{args_demande_inf} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Infraction
    ...  demande_type=Dépôt Initial IN
    ...  om_collectivite=MARSEILLE
    ...  terrain_references_cadastrales=${ref_cad}

    ${args_peti} =  Create Dictionary

    # Ajout du DI initial
    ${di} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    Depuis le contexte de nouvelle demande via l'URL
    Select From List By Label    dossier_autorisation_type_detaille    Recours contentieux
    Select From List By Label    om_collectivite    MARSEILLE
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input Text    autorisation_contestee    ${di}
    Click Button    css=#autorisation_contestee_search_button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain    css=#petitionnaire_principal_delegataire    Mylène Françoise
    Sleep  1
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Page Should Not Contain Errors
    Ajouter la demande par WS  ${args_demande_inf}  ${args_peti}  ${args_autres_demandeurs}
    Depuis le contexte du dossier d'instruction  ${di}

    Highlight heading  css=#fieldset-form-dossier_instruction-enjeu>.fieldsetContent>.field-type-static
    Capture and crop page screenshot  screenshots/instruction_dossier_instruction_form_enjeu_fieldset.png
    ...  css=#fieldset-form-dossier_instruction-enjeu.cadre

    Click On Form Portlet Action    dossier_instruction    modifier

    Highlight heading  css=.field-type-date
    Capture and crop page screenshot  screenshots/instruction_action_modifier_date.png
    ...  css=div#tabs-1


CE des demandes d'avis

    [Documentation]  Captures d'écran concernant les demandes d'avis.

    [Tags]  doc

    #
    # L'onglet "Pièce(s)"
    #

    Depuis la page d'accueil  consu  consu
    Depuis l'onglet des pièces de la demande d'avis passée du dossier d'instruction  ${di_1}
    Capture and crop page screenshot  screenshots/service_consulte_demande_avis_piece.png
    ...    content

    #
    # L'onglet "Consultation(s)"
    #

    Depuis la page d'accueil  consuetendu  consuetendu
    Depuis l'onglet des consultations de la demande d'avis en cours du dossier d'instruction  ${di_1}
    Capture and crop page screenshot  screenshots/service_consulte_demande_avis_consultation.png
    ...    content


CE du paramétrage des pièces

    [Documentation]  Captures d'écran concernant la gestion des pièces.

    [Tags]  doc

    #
    # Type de pièce
    #

    Depuis la page d'accueil  admin  admin
    Go To Tab  document_numerise_type
    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage_document_numerise_type_form.png
    ...    content


CE du menu de mise à jour des métadonnées

    [Documentation]  Captures d'écran concernant la gestion des pièces.

    [Tags]  doc

    #
    # Type de pièce
    #

    Depuis la page d'accueil  admin  admin
    Go To Tab  document_numerise_type
    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage_document_numerise_type_form.png
    ...    content

    #
    # Traitement des pièces
    #

    Go To  ${PROJECT_URL}scr/form.php?obj=document_numerise_traitement_metadonnees&action=100&idx=0
    Capture and crop page screenshot  screenshots/parametrage_document_numerise_metadata_treatment.png
    ...    content

    # Afin d'avoir un fichier en erreur, on le supprime sur le filestorage
    Remove Directory  ../var/filestorage/79/79d4  true
    # On modifie un type de pièces
    ${dnt_code} =  Set Variable  ART
    &{dnt_values} =  Create Dictionary
    ...  aff_da=true
    Modifier le type de pièces  ${dnt_code}  ${dnt_values}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    Go To  ${PROJECT_URL}scr/form.php?obj=document_numerise_traitement_metadonnees&action=100&idx=0
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Cette page permet de mettre à jour certaines métadonnées des pièces numérisées.
    Click On Submit Button
    Sleep  1
    Page Should Not Contain Errors
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Le traitement s'est correctement déroulé, sauf pour les pièces numérisées ci-dessous :
    Valid Message Should Contain  Dossier d'instruction n°AZ0130551200001P0 : le document 20160919ART.pdf n'a pas pu être mis à jour.

    Capture and crop page screenshot  screenshots/parametrage_document_numerise_metadata_treatment_res.png
    ...    content


CE des consultations
    [Tags]  doc
    [Documentation]  Captures d'écran concernant les consultation.

    # Login pour la visualisation de consultation
    Depuis la page d'accueil  instr  instr
    # On ce met sur l'onglet de consultation
    Depuis l'onglet consultation du dossier  ${di_1}
    # On fait la CE du tableau
    Capture and crop page screenshot  screenshots/instruction_dossier_consultation_tab.png
    ...    sousform-consultation
    # On rentre dans la consultation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  59.01 - Direction de l'Eau et de l'Assainissement

    Highlight heading  css=div#sousform-container>div.formEntete>div#portlet-actions #action-sousform-consultation-masquer_dans_edition span
    # On enléve le soulignement du marquer comme lu #action-sousform-consultation-marquer_comme_lu
    Mouse Out  css=div#sousform-container>div.formEntete>div#portlet-actions #action-sousform-consultation-marquer_comme_lu
    # On fait la CE du portlet
    Capture and crop page screenshot  screenshots/portlet_masquer_consultation.png
    ...    css=div#sousform-container>div.formEntete>div#portlet-actions

    Click On Back Button In Subform
    # On supprime en JS l'action de trop pour donner une impression de zoom sur le bouton uniquement
    Execute Javascript  return (function(){ jQuery("a[id*='action-soustab-consultation-left-consulter']").remove(); return true; })();
    Capture and crop page screenshot  screenshots/instruction_tab_masquer_consultation.png
    ...    css=td.icons

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  59.01 - Direction de l'Eau et de l'Assainissement
    # On clique sur l'action de masquer le document
    Click On SubForm Portlet Action  consultation  masquer_dans_edition
    # Vérification du message de succès pour attendre
    Valid Message Should Be In Subform  La consultation est masquée dans les éditions.
    Highlight heading  css=div#sousform-container>div.formEntete>div#portlet-actions #action-sousform-consultation-afficher_dans_edition span
    # On enléve le soulignement du marquer comme lu
    Mouse Out  css=div#sousform-container>div.formEntete>div#portlet-actions #action-sousform-consultation-marquer_comme_lu

    Capture and crop page screenshot  screenshots/portlet_visible_consultation.png
    ...    css=div#sousform-container>div.formEntete>div#portlet-actions

    Click On Back Button In Subform
    # On supprime en JS l'action de trop pour donner une impression de zoom sur le bouton uniquement
    Execute Javascript  return (function(){ jQuery("a[id*='action-soustab-consultation-left-consulter']").remove(); return true; })();
    Capture and crop page screenshot  screenshots/instruction_tab_visible_consultation.png
    ...    css=td.icons


CE de l'onglet des dossiers liés

    [Documentation]  Captures d'écran concernant l'onglet "Dossiers liés" d'un
    ...  dossier d'instruction.

    [Tags]  doc

    Depuis l'onglet dossiers liés du dossier d'instruction  ${di_1}
    Click On Add Button

    Capture and crop page screenshot  screenshots/instruction_dossiers_lies_form_ajout.png
    ...    content


CE de la création de lettre RAR

    [Documentation]  Captures d'écran concernant les lettre RAR

    [Tags]  doc


    Depuis la page d'accueil  suivi  suivi
    Click Link  envoi lettre RAR
    Page Title Should Be  Suivi > Suivi Des Pièces > Envoi Lettre RAR

    Capture and crop page screenshot  screenshots/suivi_envoi_lettre_rar_formulaire.png
    ...    formulaire

    # Vérification sans valeur saisie
    Click On Submit Button
    Error Message Should Be  Tous les champs doivent être remplis.

    Capture and crop page screenshot  screenshots/suivi_envoi_lettre_rar_message_aucune_saisie.png
    ...    css=.message

    # Vérification avec un numéro non valide
    Input Text  liste_code_barres_instruction  a
    Click On Submit Button
    Error Message Should Be  Le code barres d'instruction a n'est pas valide.

    Capture and crop page screenshot  screenshots/suivi_envoi_lettre_rar_message_evenement_instruction_incorrect.png
    ...    css=.message

    # Vérification avec un numéro non présent en base
    Input Text  liste_code_barres_instruction  123
    Click On Submit Button
    Error Message Should Be  Le numéro 123 ne correspond à aucun code barres d'instruction.

    Capture and crop page screenshot  screenshots/suivi_envoi_lettre_rar_message_evenement_instruction_inexistant.png
    ...    css=.message

    Click Link  envoi lettre RAR
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Input Text  date  ${date_ddmmyyyy}
    Input Text  liste_code_barres_instruction  ${code_barres}
    Click On Submit Button
    Valid Message Should Contain  Cliquez sur le lien ci-dessous pour télécharger votre document :
    Click Element  css=fieldset#fieldset-form-rar-lien_di>legend

    Capture and crop page screenshot  screenshots/suivi_envoi_lettre_rar_message_evenement_instruction_ok.png
    ...    css=.message

    Click Link  envoi lettre RAR
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Input Text  date  27/11/2020
    Input Text  liste_code_barres_instruction  ${code_barres}
    Click On Submit Button
    Error Message Should Contain  Une lettre correspondante

    Capture and crop page screenshot  screenshots/suivi_envoi_lettre_rar_message_evenement_instruction_deja.png
    ...    css=.message


CE du parametrage des commissions
    [Tags]  doc
    [Documentation]  L'objet de ce 'Test Case' est de faire une CE du
    ...  type de commission.

    Depuis la page d'accueil  admin  admin
    Go To Tab  commission_type
    Click On Add Button
    Capture and crop page screenshot  screenshots/type_commission_parametrage.png
    ...    css=#formulaire


CE du widget retour de commission
    [Tags]  doc
    [Documentation]  L'objet de ce 'Test Case' est de faire une CE du
    ...  widget retour de commission.

    # On crée une collectivité pour ne pas perturber ni être perturbé par
    # les autres tests.
    ${collectivite} =  Set Variable  CHÂTEAUVERT
    ${utilisateur_instructeur_nom} =  Set Variable  Arman Christiaanse
    ${utilisateur_instructeur_login} =  Set Variable  achristiaanse

    Depuis la page d'accueil  admin  admin
    Ajouter la collectivité depuis le menu  ${collectivite}  mono
    Ajouter la direction depuis le menu  ${collectivite}  Direction A  null
    ...  Chef A  null  null  ${collectivite}
    Ajouter la division depuis le menu  div A  subdivision A  null
    ...  Chef A  null  null  Direction A

    Ajouter l'utilisateur  ${utilisateur_instructeur_nom}  test@example.org
    ...  ${utilisateur_instructeur_login}  ${utilisateur_instructeur_login}
    ...  INSTRUCTEUR  ${collectivite}
    Ajouter l'instructeur depuis le menu  ${utilisateur_instructeur_nom}
    ...  subdivision A  instructeur  ${utilisateur_instructeur_nom}

    &{args_affectation} =  Create Dictionary
    ...  instructeur=${utilisateur_instructeur_nom}
    ...  om_collectivite=${collectivite}
    Ajouter l'affectation depuis le menu  ${args_affectation}

    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Vaillancourt
    ...  particulier_prenom=Harbin
    ...  om_collectivite=${collectivite}
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de démolir
    ...  demande_type=Dépôt Initial
    ...  date_demande=${date_ddmmyyyy}
    ...  om_collectivite=${collectivite}
    ${di_01} =  Ajouter la demande par WS  ${args_demande}  ${args_petitionnaire}
    ${code_type_commission} =  Set Variable  TC

    &{args_type_de_commission} =  Create Dictionary
    ...  code=${code_type_commission}
    ...  libelle=Type C
    ...  listes_de_diffusion=support@atreal.fr
    ...  participants=Atreal
    ...  corps_du_courriel=Type C
    ...  om_collectivite=${collectivite}
    Ajouter type de commission  ${args_type_de_commission}

    ## Début workflow commission
    Depuis la page d'accueil  ${utilisateur_instructeur_login}  ${utilisateur_instructeur_login}
    Ajouter la commission depuis le contexte du dossier d'instruction
    ...  ${di_01}  Type C  ${date_ddmmyyyy}

    Depuis la page d'accueil  admin  admin
    &{args_commission} =  Create Dictionary
    ...  om_collectivite=${collectivite}
    ...  commission_type=Type C
    Ajouter un suivi de commission  ${args_commission}

    Planifier un dossier pour une commission
    ...  ${di_01}  ${code_type_commission}${DATE_FORMAT_YYYYMMDD}

    Rendre un avis sur dossier passé en commission
    ...  favorable  ${di_01}  ${code_type_commission}${DATE_FORMAT_YYYYMMDD}

    Depuis la page d'accueil  ${utilisateur_instructeur_login}  ${utilisateur_instructeur_login}
    Element Should Contain  css=.widget_commission_retours .box-icon  1
    Capture and crop page screenshot
    ...  screenshots/ergonomie/widget_commission_mes_retours.png
    ...  css=.widget_commission_retours


CE du parametrage des bibles
    [Tags]  doc
    [Documentation]  L'objet de ce 'Test Case' est de faire une CE des
    ...  bibles

    Depuis la page d'accueil  admin  admin
    Go To Tab  bible
    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage_bible.png
    ...    css=#formulaire


CE de la simulation des taxes
    [Tags]  doc
    [Documentation]  Permet de réaliser les captures d'écrans concernant la
    ...  simulation des taxes.

    # On active l'option de simulation des taxes
    Depuis la page d'accueil  admin  admin
    Ajouter le paramètre depuis le menu  option_simulation_taxes  true  agglo

    # CE du paramétrage des taxes
    Depuis la page d'accueil  admin  admin
    Depuis le contexte du paramétrage des taxes  MARSEILLE
    Click On Form Portlet Action  taxe_amenagement  modifier
    Capture and crop page screenshot  screenshots/taxe_amenagement_form.png
    ...    css=#formulaire

    # CE du fieldset de simulation des taxes
    Depuis la page d'accueil  instr  instr
    &{args_dt_taxes} =  Create Dictionary
    ...  tax_surf_tot_cstr=160
    ...  tax_su_princ_surf1=160
    ...  tax_sup_bass_pisc_cr=50
    ...  tax_am_statio_ext_cr=2
    ...  tax_surf_loc_arch=0.5
    ...  tax_surf_pisc_arch=2
    ...  mtn_exo_ta_part_commu=100
    ...  mtn_exo_ta_part_depart=100
    ...  mtn_exo_ta_part_reg=0
    ...  mtn_exo_rap=20
    Modifier les données techniques pour le calcul des impositions  ${di_1}  ${args_dt_taxes}
    &{args_di} =  Create Dictionary
    ...  tax_secteur=Secteur 1
    Modifier le dossier d'instruction  ${di_1}  ${args_di}
    Depuis le contexte du dossier d'instruction  ${di_1}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open Fieldset  dossier_instruction  simulation-des-taxes
    Capture and crop page screenshot  screenshots/instruction_simulation_taxes.png
    ...  css=#fieldset-form-dossier_instruction-simulation-des-taxes

    # CE des données techniques nécessaires au calcul de la TA
    Click On Form Portlet Action  dossier_instruction  donnees_techniques
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  donnees_techniques  modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open Fieldset In Subform  donnees_techniques  declaration-des-elements-necessaires-au-calcul-des-impositions
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open Fieldset In Subform  donnees_techniques  exonerations
    Sleep  1
    Highlight heading  css=#tax_surf_tot_cstr
    Highlight heading  css=#tax_empl_ten_carav_mobil_nb_cr
    Highlight heading  css=#tax_empl_hll_nb_cr
    Highlight heading  css=#tax_sup_bass_pisc_cr
    Highlight heading  css=#tax_eol_haut_nb_cr
    Highlight heading  css=#tax_pann_volt_sup_cr
    Highlight heading  css=#tax_am_statio_ext_cr
    Highlight heading  css=#tax_su_princ_surf4
    Highlight heading  css=#tax_su_princ_surf3
    Highlight heading  css=#tax_su_heber_surf3
    Highlight heading  css=#tax_su_princ_surf1
    Highlight heading  css=#tax_su_princ_surf2
    Highlight heading  css=#tax_su_non_habit_surf2
    Highlight heading  css=#tax_su_non_habit_surf3
    Highlight heading  css=#tax_su_non_habit_surf4
    Highlight heading  css=#tax_su_parc_statio_expl_comm_surf
    Highlight heading  css=#mtn_exo_ta_part_commu
    Highlight heading  css=#mtn_exo_ta_part_depart
    Highlight heading  css=#mtn_exo_ta_part_reg
    Capture and crop page screenshot  screenshots/instruction_simulation_taxes_dt_ta.png
    ...  css=#fieldset-sousform-donnees_techniques-declaration-des-elements-necessaires-au-calcul-des-impositions
    Click On Back Button In Subform

    # CE des données techniques nécessaires au calcul de la RAP
    Click On Form Portlet Action  dossier_instruction  donnees_techniques
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  donnees_techniques  modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open Fieldset In Subform  donnees_techniques  declaration-des-elements-necessaires-au-calcul-des-impositions
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open Fieldset In Subform  donnees_techniques  exonerations
    Sleep  1
    Highlight heading  css=#tax_surf_loc_arch
    Highlight heading  css=#tax_surf_tot_cstr
    Highlight heading  css=#tax_empl_ten_carav_mobil_nb_arch
    Highlight heading  css=#tax_empl_ten_carav_mobil_nb_cr
    Highlight heading  css=#tax_empl_hll_nb_arch
    Highlight heading  css=#tax_empl_hll_nb_cr
    Highlight heading  css=#tax_surf_pisc_arch
    Highlight heading  css=#tax_sup_bass_pisc_cr
    Highlight heading  css=#tax_am_statio_ext_arch
    Highlight heading  css=#tax_am_statio_ext_cr
    Highlight heading  css=#tax_su_princ_surf4
    Highlight heading  css=#tax_su_princ_surf3
    Highlight heading  css=#tax_su_heber_surf3
    Highlight heading  css=#tax_su_princ_surf1
    Highlight heading  css=#tax_su_princ_surf2
    Highlight heading  css=#tax_su_non_habit_surf2
    Highlight heading  css=#tax_su_non_habit_surf3
    Highlight heading  css=#tax_su_non_habit_surf4
    Highlight heading  css=#tax_su_parc_statio_expl_comm_surf
    Highlight heading  css=#mtn_exo_rap
    Capture and crop page screenshot  screenshots/instruction_simulation_taxes_dt_rap.png
    ...  css=#fieldset-sousform-donnees_techniques-declaration-des-elements-necessaires-au-calcul-des-impositions
    Click On Back Button In Subform


CE de la creation des nouveaux dossiers contentieux
    [Tags]  doc
    [Documentation]  Captures d'écran de la creation des nouveaux dossiers
    ...  contentieux.

    Depuis la page d'accueil  assist  assist
    Depuis le contexte de nouvelle demande contentieux via l'URL
    &{args_demande_mauvais_di} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Recours contentieux
    ...  autorisation_contestee=DP0130551710001P0
    &{args_demande_valides} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Recours contentieux
    ...  autorisation_contestee=${di_2}

    Run Keyword And Expect Error  *
    ...  Saisir la demande  ${args_demande_mauvais_di}
    Capture and crop page screenshot
    ...  screenshots/contentieux_demande_dossier_recours.png
    ...  css=.ui-state-error

    Saisir la demande  ${args_demande_valides}
    Sleep  2
    Capture and crop page screenshot
    ...  screenshots/contentieux_demande_dossier_recours_erreur_dossier_conteste.png
    ...  css=#content #formulaire


CE du paramétrage des groupes
    [Tags]  doc
    [Documentation]  L'objet de ce 'Test Case' est de faire les CE du
    ...  paramétrage des groupes, par profil et par utilisateur

    Depuis la page d'accueil  admin  admin

    Ajouter l'utilisateur  Baril Amélie  support@atreal.fr  abaril  abaril  VISUALISATION DA et DI  MARSEILLE

    Depuis l'onglet groupe du profil  VISUALISATION DA et DI
    Capture and crop page screenshot  screenshots/administration_om_profil_groupe.png
    ...    content

    Depuis l'onglet groupe de l'utilisateur  abaril

    Ajouter le groupe depuis l'onglet groupe de l'utilisateur  Autorisation ADS  true  true
    Ajouter le groupe depuis l'onglet groupe de l'utilisateur  Changement d'usage  false  true
    Ajouter le groupe depuis l'onglet groupe de l'utilisateur  Renseignement d'urbanisme  false  true
    Ajouter le groupe depuis l'onglet groupe de l'utilisateur  ERP  false  true

    Capture and crop page screenshot  screenshots/administration_om_utilisateur_groupe.png
    ...    content


CE des dossiers liés
    [Tags]  doc
    [Documentation]  L'objet de ce 'Test Case' est de faire les CE des
    ...  listings de l'onglet Dossiers Liés du DI

    Depuis la page d'accueil  instrpolycomm3  instrpolycomm3
    &{args_petitionnaire_autre_commune} =  Create Dictionary
    ...  particulier_nom=Beauchamps
    ...  particulier_prenom=Maurissette
    @{ref_cad_autre_commune} =  Create List  806  AB  25
    &{args_demande_autre_commune} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad_autre_commune}
    ${libelle_di_autre_commune} =  Ajouter la nouvelle demande  ${args_demande_autre_commune}  ${args_petitionnaire_autre_commune}
    Depuis la page d'accueil  guichet  guichet
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Beauchamps
    ...  particulier_prenom=Jeanette
    @{ref_cad} =  Create List  806  AB  25  A  30
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ${libelle_di} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}
    ${libelle_di_spaceless} =  Sans espace  ${libelle_di}
    &{args_petitionnaire} =  Create Dictionary
    ...  particulier_nom=Forest
    ...  particulier_prenom=David
    @{ref_cad} =  Create List  806  AB  01  A  50
    &{args_demande} =  Create Dictionary
    ...  dossier_autorisation_type_detaille=Permis de construire pour une maison individuelle et / ou ses annexes
    ...  demande_type=Dépôt Initial
    ...  terrain_references_cadastrales=${ref_cad}
    ${libelle_di2} =  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}
    ${libelle_di2_spaceless} =  Sans espace  ${libelle_di2}
    ${libelle_da} =  Get Substring  ${libelle_di}  0  -2
    ${libelle_da_spaceless} =  Sans espace  ${libelle_da}
    ${libelle_da2} =  Get Substring  ${libelle_di2}  0  -2
    ${libelle_da_autre_commune} =  Get Substring  ${libelle_di_autre_commune}  0  -2
    ${libelle_di_autre_commune_spaceless} =  Sans espace  ${libelle_di_autre_commune}
    Depuis la page d'accueil  instr  instr
    Ajouter une instruction au DI et la finaliser  ${libelle_di}  accepter un dossier sans réserve
    &{args_demande} =  Create Dictionary
    ...  demande_type=Demande de modification
    Depuis la page d'accueil  guichet  guichet
    ${libelle_di_modification} =  Ajouter la demande sur existant depuis le tableau de bord  ${libelle_di}  ${args_demande}
    ${libelle_di_modification_spaceless} =  Sans espace  ${libelle_di_modification}
    Depuis la page d'accueil  admin  admin
    Depuis le contexte de nouvelle demande via l'URL
    Select From List By Label    dossier_autorisation_type_detaille    Recours contentieux
    Select From List By Label    om_collectivite    MARSEILLE
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input Text    autorisation_contestee    ${libelle_di}
    Click Button    css=#autorisation_contestee_search_button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain    css=#petitionnaire_principal_delegataire    Beauchamps Jeanette
    Sleep  1
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Page Should Not Contain Errors
    ${libelle_di_re} =  Get Text  id=new_di
    ${libelle_di_re_spaceless} =  Sans espace  ${libelle_di_re}
    Depuis le contexte de nouvelle demande via l'URL
    Select From List By Label    dossier_autorisation_type_detaille    Recours contentieux
    Select From List By Label    om_collectivite    MARSEILLE
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input Text    autorisation_contestee    ${libelle_di2}
    Click Button    css=#autorisation_contestee_search_button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain    css=#petitionnaire_principal_delegataire    Forest David
    Sleep  1
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Page Should Not Contain Errors
    ${libelle_di_re2} =  Get Text  id=new_di
    ${libelle_di_re_2spaceless} =  Sans espace  ${libelle_di_re2}
    Depuis l'onglet des messages du dossier d'instruction  ${libelle_di}
    Click On Link  Autorisation contestée
    Element Text Should Be  contenu  Cette autorisation a été contestée par le recours ${libelle_di_re_spaceless}.
    Depuis la page d'accueil  instrpoly  instrpoly
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di_modification}
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di_autre_commune}
    Click On Submit Button In SubForm
    Valid Message Should Contain In Subform  Le dossier ${libelle_di_autre_commune_spaceless} a été lié.
    Click On Link  link_dossier_instruction_lie
    Page Title Should Be    Instruction > Dossiers D'instruction > ${libelle_di_autre_commune} BEAUCHAMPS MAURISSETTE
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di_modification}
    Element Should Contain  sousform-dossier_lies  ${libelle_di_autre_commune}
    Depuis la page d'accueil  instr  instr
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di_modification}
    Element Should Not Contain  sousform-dossier_lies  ${libelle_di_autre_commune}
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di2}
    Click On Submit Button In SubForm
    Valid Message Should Contain In Subform  Le dossier ${libelle_di2_spaceless} a été lié.
    Click On Link  link_dossier_instruction_lie
    Page Title Should Be    Instruction > Dossiers D'instruction > ${libelle_di2} FOREST DAVID
    On clique sur l'onglet  lien_dossier_dossier  Dossiers Liés
    Element Should Contain  sousform-dossier_lies  Aucun enregistrement.
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di_modification_spaceless}
    Click On Submit Button In SubForm
    Valid Message Should Contain In Subform  Le dossier ${libelle_di_modification_spaceless} a été lié.
    Click On Back Button In SubForm
    Element Should Contain  sousform-dossier_lies  ${libelle_di_modification}
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di2}
    Click Element  action-soustab-dossier_lies-corner-ajouter
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  dossier cible
    Input Text  dossier_cible  ${libelle_di_re2}
    Click On Submit Button In SubForm
    Valid Message Should Contain In Subform  Le dossier ${libelle_di_re2_spaceless} a été lié.
    Depuis la page d'accueil  instr  instr
    Depuis l'onglet Dossiers Liés du dossier d'instruction  ${libelle_di2}
    Capture and crop page screenshot
    ...  screenshots/instruction_dossiers_lies.png
    ...  css=#sousform-lien_dossier_dossier



CE du paramétrage des logos
    [Documentation]  Captures d'écran concernant la gestion des logos.
    [Tags]  doc

    Depuis la page d'accueil  admin  admin
    Go To Tab  om_logo
    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage_edition_logo.png
    ...    content
    Go To Tab  om_logo
    Click Link  logopdf.png multi
    Capture and crop page screenshot  screenshots/parametrage_edition_logo_portlet.png
    ...  portlet-actions
