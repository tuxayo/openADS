<?php
/**
 * Ce fichier regroupe les tests unitaire général à l'application OpenADS
 *
 * @package openfoncier
 * @version SVN : $Id: testGeneral.php 5673 2015-12-21 19:35:24Z nmeucci $
 */

/**
 * Cette classe permet de tester unitairement les fonctions de l'application.
 */
class General extends PHPUnit_Framework_TestCase {

    /**
     * Méthode lancée en début de traitement
     */
    public function setUp() {

        // Instancie la timezone
        date_default_timezone_set('Europe/Paris');
        echo ' = '.get_class().'.'.str_replace('test_', '', $this->getName())."\r\n";
    }

    public function onNotSuccessfulTest(Exception $e){
        echo 'Line '.$e->getLine().' : '.$e->getMessage()."\r\n";
        parent::onNotSuccessfulTest($e);
    }

    /**
     * Méthode lancée en fin de traitement
     */
    public function tearDown() {
        
        //
    }

    /**
     * Test la fonction mois_date() de la classe Utils.
     */
    public function test_01_utils_mois_date() {
        // Instance de la classe Utils
        require_once "../obj/utils.class.php";
        @session_start();
        $_SESSION['collectivite'] = 1;
        $_SESSION['login'] = "admin";
        $_SERVER['REQUEST_URI'] = "";
        $f = new utils("nohtml");
        $f->disableLog();

        // Avec des mois

        // Pour les additions
        // Initialisation des tableaux
        $tab_date_dep = array();
        $tab_delay = array();
        $tab_date_fin = array();
        
        // Tableau des date de départ
        $tab_date_dep = array(
            0 => "2013-12-31",
            1 => "2014-01-31",
            2 => "2014-01-01",
            3 => "2014-01-31",
            4 => "2015-12-31",
            5 => "",
            6 => "",
            7 => "",
            8 => "2015-12-31",
            9 => "2015-12-31",
            10 => "openADS",
            11 => "openADS",
            12 => "openADS",
        );
        // Tableau des delais
        $tab_delay = array(
            0 => "2",
            1 => "5",
            2 => "12",
            3 => "27",
            4 => "2",
            5 => "2",
            6 => "openads",
            7 => "",
            8 => "openADS",
            9 => "",
            10 => "2",
            11 => "openads",
            12 => "",
        );
        // Tableau des date résultat
        $tab_date_fin = array(
            0 => "2014-02-28",
            1 => "2014-06-30",
            2 => "2015-01-01",
            3 => "2016-04-30",
            4 => "2016-02-29",
            5 => null,
            6 => null,
            7 => null,
            8 => "2015-12-31",
            9 => "2015-12-31",
            10 => null,
            11 => null,
            12 => null,
        );

        // Pour chaque date
        foreach ($tab_date_dep as $key => $date_dep) {
            // Calcul la date résultat
            $date_fin = $f->mois_date($date_dep, $tab_delay[$key], "+");
            // Vérifie que la date résultat est correct
            $this->assertEquals($date_fin, $tab_date_fin[$key]);
        }

        // Pour les soustractions
        // Initialisation des tableaux
        $tab_date_dep = array();
        $tab_delay = array();
        $tab_date_fin = array();

        // Tableau des date de départ
        $tab_date_dep = array(
            0 => "2014-01-31",
        );
        // Tableau des delais
        $tab_delay = array(
            0 => "4",
        );
        // Tableau des date résultat
        $tab_date_fin = array(
            0 => "2013-09-30",
        );

        // Pour chaque date
        foreach ($tab_date_dep as $key => $date_dep) {
            // Calcul la date résultat
            $date_fin = $f->mois_date($date_dep, $tab_delay[$key], "-");
            // Vérifie que la date résultat est correct
            $this->assertEquals($date_fin, $tab_date_fin[$key]);
        }

        // Avec des jours

        // Pour les additions
        // Initialisation des tableaux
        $tab_date_dep = array();
        $tab_delay = array();
        $tab_date_fin = array();
        
        // Tableau des date de départ
        $tab_date_dep = array(
            0 => "2013-12-31",
            1 => "2014-01-31",
            2 => "2014-01-01",
            3 => "2014-01-31",
            4 => "2015-12-31",
            5 => "",
            6 => "",
            7 => "",
            8 => "2015-12-31",
            9 => "2015-12-31",
            10 => "openADS",
            11 => "openADS",
            12 => "openADS",
        );
        // Tableau des delais
        $tab_delay = array(
            0 => "2",
            1 => "5",
            2 => "12",
            3 => "27",
            4 => "2",
            5 => "2",
            6 => "openads",
            7 => "",
            8 => "openADS",
            9 => "",
            10 => "2",
            11 => "openads",
            12 => "",
        );
        // Tableau des dates résultats
        $tab_date_fin = array(
            0 => "2014-01-02",
            1 => "2014-02-05",
            2 => "2014-01-13",
            3 => "2014-02-27",
            4 => "2016-01-02",
            5 => null,
            6 => null,
            7 => null,
            8 => "2015-12-31",
            9 => "2015-12-31",
            10 => null,
            11 => null,
            12 => null,
        );

        // Pour chaque date
        foreach ($tab_date_dep as $key => $date_dep) {
            // Calcul la date résultat
            $date_fin = $f->mois_date($date_dep, $tab_delay[$key], "+", "jour");
            // Vérifie que la date résultat est correct
            $this->assertEquals($date_fin, $tab_date_fin[$key]);
        }

        // Pour les soustractions
        // Initialisation des tableaux
        $tab_date_dep = array();
        $tab_delay = array();
        $tab_date_fin = array();

        // Tableau des date de départ
        $tab_date_dep = array(
            0 => "2014-01-31",
        );
        // Tableau des delais
        $tab_delay = array(
            0 => "4",
        );
        // Tableau des date résultat
        $tab_date_fin = array(
            0 => "2014-01-27",
        );

        // Pour chaque date
        foreach ($tab_date_dep as $key => $date_dep) {
            // Calcul la date résultat
            $date_fin = $f->mois_date($date_dep, $tab_delay[$key], "-", "jour");
            // Vérifie que la date résultat est correct
            $this->assertEquals($date_fin, $tab_date_fin[$key]);
        }

        // Destruction de la classe Utils
        $f->__destruct();
    }


    /**
     * Vérification de la méthode permettant de selectionner le logo de la bonne
     * collectivité.
     */
    function test_02_TNR_selection_du_logo_dans_les_editions() {
        // Instance de la classe Utils
        require_once "../obj/utils.class.php";
        @session_start();
        $_SESSION['collectivite'] = 1;
        $_SESSION['login'] = "admin";
        $_SERVER['REQUEST_URI'] = "";
        $f = new utils("nohtml");
        $f->disableLog();

        require_once "../core/om_edition.class.php";
        $edition = new edition();
        $edition->f = $f;
        // Vérification du logo de collectivité mono dans le cas où :
        // - un logo multi actif est défini
        // - un logo actif pour la collectivité est défini
        $logo = $edition->get_logo_from_collectivite("logopdf.png", 2);
        // XXX le double // dans le path est nécessaire
        $logo_valid = array(
            "file" => "../var/filestorage//88/8815/88154c6f68d3a0e1928c84fc0187993d",
            "w" => 43.349333333333334,
            "h" => 43.349333333333334,
            "type" => "png",
        );
        // Le logo commune doit être retourné par la méthode
        $this->assertEquals($logo, $logo_valid);

        // Désactivation du logo de la commune
        $val_logo["actif"] = 'f';
        $f->db->autoExecute(DB_PREFIXE."om_logo", $val_logo, DB_AUTOQUERY_UPDATE, "om_logo=2");

        // Vérification du logo de collectivité mono dans le cas où :
        // - un logo multi actif est défini
        // - un logo la collectivité est inactif
        $logo = $edition->get_logo_from_collectivite("logopdf.png", 1);
        // XXX le double // dans le path est nécessaire
        $logo_valid = array(
            "file" => "../var/filestorage//d2/d20a/d20a5c36d3b83464bab63035a7f61901",
            "w" => 43.349333333333334,
            "h" => 43.349333333333334,
            "type" => "png",
        );
        // Le logo de la collectivité multi doit être retourné
        $this->assertEquals($logo, $logo_valid);

        // Désactivation du logo de la commune
        $val_logo["actif"] = 'f';
        $f->db->autoExecute(DB_PREFIXE."om_logo", $val_logo, DB_AUTOQUERY_UPDATE, "om_logo=6");

        // Vérification du logo de collectivité mono dans le cas où aucun logo
        // n'est activé
        $logo = $edition->get_logo_from_collectivite("logopdf.png", 1);
        $logo_valid = null;
        // Un valeur null doit être retournée
        $this->assertEquals($logo, $logo_valid);
        // Destruction de la classe Utils
        $f->__destruct();
    }

    /**
     * Vérification de la méthode permettant de formater le corps du courriel
     * notifié aux communes.
     */
    function test_03_instruction_formater_modele() {
        // Instance de la classe Utils
        require_once "../obj/utils.class.php";
        @session_start();
        $_SESSION['collectivite'] = 1;
        $_SESSION['login'] = "admin";
        $_SERVER['REQUEST_URI'] = "";
        $f = new utils("nohtml");
        $f->disableLog();
        $GLOBALS["f"] = $f;
        // template du modèle
        $template = 'DI : <DOSSIER_INSTRUCTION>
LIEN : <URL_INSTRUCTION>
INSTRUCTION : <ID_INSTRUCTION>';
        // résultat attendu
        $di = 'PC01';
        $inst = 7;
        $url = 'http://localhost/openads/spg/direct_link.php?obj=dossier_instruction&action=3';
        $url .= '&direct_field=dossier&direct_form=instruction&direct_action=3&direct_idx='.$inst;
        $link = '<a href="'.$url.'">'.$url.'</a>';
        $expected = 'DI : '.$di.'<br/>';
        $expected .= 'LIEN : '.$link.'<br/>';
        $expected .= 'INSTRUCTION : '.$inst;
        // Instance de la classe instruction
        require_once "../obj/instruction.class.php";
        $instr = new instruction(']', $f->db, DEBUG);
        // Traitement
        $result = $instr->formater_modele($template, $di, $inst);
        // Vérification du traitement
        $this->assertEquals($expected, $result);
        // Destruction de la classe Utils
        $f->__destruct();
    }


    /**
     * TNR du bug de champs de fusion [*_correspondant] dans la om_requete n°7 "dossier".
     * Le test vérifie qu'une seule ligne est retournée par la requête et qu'elle contient
     *  bien le nom du pétitionnaire principal
     */
    function test_05_TNR_om_requete_dossier() {
        // Instance de la classe Utils
        require_once "../obj/utils.class.php";
        @session_start();
        $_SESSION['collectivite'] = 1;
        $_SESSION['login'] = "admin";
        $_SERVER['REQUEST_URI'] = "";
        $f = new utils("nohtml");
        $f->disableLog();
        $GLOBALS["f"] = $f;

        require_once "../obj/om_requete.class.php";
        $om_requete = new om_requete(7);

        // récupération de la requête SQL
        $sql = $om_requete->getVal('requete');
        // remplacement des &idx par la valeur du dossier
        $sql = str_replace('&idx', "AT0130551300001P0", $sql);
        // définition du schéma
        $sql = str_replace('&DB_PREFIXE', DB_PREFIXE, $sql);
        // exécution de la requête
        $res = $f->db->query($sql);
        $f->isDatabaseError($res);
        $count = $res->numRows();
        // La om_requete doit retourner une seule ligne
        $this->assertEquals(1, $count);
        // récupération du résultat de la om_requete
        $values = &$res->fetchRow(DB_FETCHMODE_ASSOC);
        // La valeur de la colonne nom_correspondant doit être le nom du pétitionnaire
        // principal
        $this->assertEquals("Dupont Jean", $values['nom_correspondant']);
        // Destruction de la classe Utils
        $f->__destruct();
    }

    /**
     * TNR de la methode factorisé de création de dossier_message 
     */
    function test_06_TNR_dossier_message() {
        // Instanciation de tout les requis
        require_once "../obj/utils.class.php";
        @session_start();
        $_SESSION['collectivite'] = 2;
        $_SESSION['login'] = "admin";
        $_SERVER['REQUEST_URI'] = "";
        $f = new utils("nohtml");
        $f->disableLog();
        $GLOBALS["f"] = $f;
        require_once "../obj/dossier_message.class.php";
        $dossier_message_factory = new dossier_message(0, $f->db, 0);
        $dossier_message_val = array();

        $dossier_message_val['contenu'] = 'Test Success';
        $dossier_message_val['dossier'] = 'AT0130551200001P0';
        $dossier_message_val['type'] = _('Ajout de pièce(s)');
        $dossier_message_val['emetteur'] = 'instr';
        $dossier_message_val['login'] = 'instr';
        $dossier_message_val['date_emission'] = date('Y-m-d H:i:s');
        $add = $dossier_message_factory->add_notification_message($dossier_message_val);
        $this->assertEquals(true, $add);
        $this->assertEquals(true, isset($dossier_message_factory->valF[$dossier_message_factory->clePrimaire]));
        $dossier_message = new dossier_message($dossier_message_factory->valF[$dossier_message_factory->clePrimaire], $f->db, 0);
        $this->assertEquals($dossier_message_val['dossier'], $dossier_message->getVal('dossier'));
        $this->assertEquals('t', $dossier_message->getVal('lu'));
        
        $dossier_message_val['contenu'] = 'Test Duplicata';
        $add = $dossier_message_factory->add_notification_message($dossier_message_val);
        $this->assertEquals(true, $add);
                
        $dossier_message_val['contenu'] = 'Test nouveau type même dossier';
        $dossier_message_val['type'] = _('Autorisation contestée');
        $add = $dossier_message_factory->add_notification_message($dossier_message_val);
        $this->assertEquals(true, $add);
        
        $dossier_message_val['contenu'] = 'Test message non lu';
        $dossier_message_val['dossier'] = 'AZ0130551200001P0';
        $dossier_message_val['emetteur'] = $f->get_connected_user_login_name();
        $dossier_message_val['login'] = $_SESSION['login'];
        $add = $dossier_message_factory->add_notification_message($dossier_message_val);
        $this->assertEquals(true, $add);
        $dossier_message = new dossier_message($dossier_message_factory->valF[$dossier_message_factory->clePrimaire], $f->db, 0);
        $this->assertEquals('f', $dossier_message->getVal('lu'));

        // Destruction de la classe Utils
        $f->__destruct();
    }


}

?>
