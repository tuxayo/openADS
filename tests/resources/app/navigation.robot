*** Settings ***
Documentation     Actions navigation

*** Keywords ***
Modifier la lettre-type XXX
    [Documentation]  Redéfinition temporaire du Keyword openMairie, à cause d'un nom de
    ...  colonne différent dans le listing des lettres-types  (id sur openADS, identifiant
    ...  sur openMairie). Permet de redéfinir une lettre-type en lui passant les données.
    [Arguments]  ${id}  ${libelle}=null  ${titre}=null  ${corps}=null  ${sql}=null  ${actif}=null  ${collectivite}=null  ${logo}=null

    # On ouvre le tableau des lettres-types
    Depuis le tableau des lettres-types
    # On recherche la lettre-type
    Run Keyword If    '${id}' != 'null'    Use Simple Search    id    ${id}    ELSE IF    '${libelle}' != 'null'    Use Simple Search    libellé    ${libelle}    ELSE    Fail
    # On clique sur la lettre-type
    Run Keyword If    '${id}' != 'null'    Click On Link    ${id}    ELSE IF    '${libelle}' != 'null'    Click On Link    ${libelle}    ELSE    Fail
    # On clique sur le bouton modifier
    Click On Form Portlet Action  om_lettretype  modifier
    # On remplit le formulaire
    Saisir la lettre-type  ${id}  ${libelle}  ${titre}  ${corps}  ${sql}  ${actif}  ${collectivite}  ${logo}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
