*** Settings ***
Documentation  Actions spécifiques aux instructions.

*** Keywords ***
Ouvrir la bible du complément d'instruction n°
    [Arguments]  ${numero_complement}

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=a[onclick^="bible(${numero_complement});"]

Ajout automatique de complément(s) d'instruction
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=a[onclick^="bible_auto();"]

Ajouter une instruction au DI
    [Arguments]  ${di}  ${evenement}  ${date_evenement}=null  ${menu}=null  ${signataire_arrete}=null

    Depuis l'onglet instruction du dossier d'instruction  ${di}  ${menu}
    Run Keyword If  '${menu}' == 'infraction'  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction_contexte_ctx_inf-corner-ajouter
    ...  ELSE IF  '${menu}' == 'recours'  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction_contexte_ctx_re-corner-ajouter
    ...  ELSE  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction-corner-ajouter
    Saisir instruction  ${evenement}  ${date_evenement}  ${signataire_arrete}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.

Ajouter une instruction au DI et la finaliser
    [Arguments]  ${di}  ${evenement}  ${date_evenement}=null  ${menu}=null  ${signataire_arrete}=null

    Depuis l'onglet instruction du dossier d'instruction  ${di}  ${menu}
    Run Keyword If  '${menu}' == 'infraction'  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction_contexte_ctx_inf-corner-ajouter
    ...  ELSE IF  '${menu}' == 'recours'  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction_contexte_ctx_re-corner-ajouter
    ...  ELSE  Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-instruction-corner-ajouter
    Saisir instruction  ${evenement}  ${date_evenement}  ${signataire_arrete}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    Click On Link  ${evenement}
    ${instruction} =  Get Text  css=.form-content #instruction
    ${code_barres} =  STR_PAD_LEFT  ${instruction}  10  0
    ${code_barres} =  Catenate  11${code_barres}
    Run Keyword If  '${menu}' == 'infraction'  Click On SubForm Portlet Action  instruction_contexte_ctx_inf  finaliser
    ...  ELSE IF  '${menu}' == 'recours'  Click On SubForm Portlet Action  instruction_contexte_ctx_re  finaliser
    ...  ELSE  Click On SubForm Portlet Action  instruction  finaliser

    [Return]  ${code_barres}

Saisir instruction
    [Arguments]  ${evenement}=null  ${date_evenement}=null  ${signataire_arrete}=null

    Wait Until Element Is Visible  evenement
    # On sélectionne l'événement, avec utilisation de doubles quotes pour que
    # les événements contenant une apostrophe ne fassent pas bug
    Run Keyword If  "${evenement}" != "null"  Select From List By Label  evenement  ${evenement}
    # On saisit la date
    Run Keyword If  "${date_evenement}" != "null"  Input Text  date_evenement  ${date_evenement}
    # On saisie le signataire
    Run Keyword If  "${signataire_arrete}" != "null"  Select From List By Label  signataire_arrete  ${signataire_arrete}

Depuis l'instruction du dossier d'instruction

    [Documentation]  Permet d'accéder à la fiche de l'instruction du dossier
    ...  d'instruction.

    [Arguments]  ${dossier_instruction}  ${instruction}  ${menu}=null

    Depuis l'onglet instruction du dossier d'instruction  ${dossier_instruction}  ${menu}
    # On clique sur l'instruction
    Click On Link  ${instruction}

Supprimer l'instruction
    [Arguments]  ${di}  ${libelle}

    Depuis l'instruction du dossier d'instruction  ${di}  ${libelle}
    # On clique sur l'action modifier
    Click On SubForm Portlet Action  instruction  supprimer
    # On valide
    Click On Submit Button In Subform
    # Vérification qu'il n'y a aucune erreur
    Page Should Not Contain Errors
    # On vérifie le message de validation
    Valid Message Should Contain  La suppression a été correctement effectuée.


Récupérer le code barres de l'instruction
    [Arguments]  ${di}  ${libelle}

    Depuis l'instruction du dossier d'instruction  ${di}  ${libelle}
    ${instruction} =  Get Text  css=.form-content #instruction
    ${code_barres} =  STR_PAD_LEFT  ${instruction}  10  0
    ${code_barres} =  Catenate  11${code_barres}

    [Return]  ${code_barres}


Modifier le suivi des dates
    [Documentation]  Permet d'utiliser l'action 'Suivi des dates' du portlet.
    [Arguments]  ${di}  ${instruction}  ${instruction_values}  ${menu}=null

    Depuis l'instruction du dossier d'instruction  ${di}  ${instruction}  ${menu}
    # On clique sur l'action 'Suivi des dates'
    ${sousform} =  Set Variable If  '${menu}' == 'infraction'  instruction_contexte_ctx_inf
    ...  ELSE IF  '${menu}' == 'recours'  instruction_contexte_ctx_re
    ...  ELSE  instruction
    Click On SubForm Portlet Action  ${sousform}  modifier_suivi
    Si "date_finalisation_courrier" existe dans "${instruction_values}" on execute "Input Text" dans "${sousform}"
    Si "date_envoi_signature" existe dans "${instruction_values}" on execute "Input Text" dans "${sousform}"
    Si "date_retour_signature" existe dans "${instruction_values}" on execute "Input Text" dans "${sousform}"
    Si "date_envoi_rar" existe dans "${instruction_values}" on execute "Input Text" dans "${sousform}"
    Si "date_retour_rar" existe dans "${instruction_values}" on execute "Input Text" dans "${sousform}"
    Si "date_envoi_controle_legalite" existe dans "${instruction_values}" on execute "Input Text" dans "${sousform}"
    Si "date_retour_controle_legalite" existe dans "${instruction_values}" on execute "Input Text" dans "${sousform}"
    # On valide le formulaire
    Click On Submit Button In Subform
    # On contôle le message de validation
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
