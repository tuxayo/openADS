*** Settings ***
Documentation  Actions spécifiques aux signataires d'arrêté.

*** Keywords ***
Depuis le tableau des signataires
    [Documentation]  Permet d'accéder au tableau des signataires.

    # On ouvre le tableau
    Go To Tab  signataire_arrete

Depuis le contexte du signataire
    [Documentation]  Permet d'accéder au formulaire en consultation
    ...    d'un signataire.
    [Arguments]  ${nom}=null  ${qualite}=null

    # On ouvre le tableau des signataires
    Depuis le tableau des signataires
    # On recherche l'signataire
    Run Keyword If    '${nom}' != 'null'    Use Simple Search    nom    ${nom}    ELSE IF    '${qualite}' != 'null'    Use Simple Search    qualite    ${qualite}    ELSE    Fail
    # On clique sur l'signataire
    Run Keyword If    '${nom}' != 'null'    Click On Link    ${nom}    ELSE IF    '${qualite}' != 'null'    Click On Link    ${qualite}    ELSE    Fail

Ajouter le signataire depuis le menu
    [Documentation]  Permet d'ajouter un signataire.
    [Arguments]  ${civilite}=null  ${nom}=null  ${prenom}=null  ${qualite}=null  ${signature}=null  ${defaut}=null  ${collectivite}=null  ${debut}=null  ${fin}=null

    # On ouvre le tableau des signataires
    Depuis le tableau des signataires
    # On clique sur l'icone d'ajout
    Click On Add Button
    # On remplit le formulaire
    Saisir le signataire  ${civilite}  ${nom}  ${prenom}  ${qualite}  ${signature}  ${defaut}  ${collectivite}  ${debut}  ${fin}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Saisir le signataire
    [Documentation]  Permet de remplir le formulaire d'un signataire.
    [Arguments]  ${civilite}  ${nom}  ${prenom}  ${qualite}  ${signature}  ${defaut}  ${collectivite}  ${debut}  ${fin}

    # On sélectionne la civilité
    Run Keyword If  '${civilite}' != 'null'  Select From List By Label  civilite  ${civilite}
    # On saisit le nom
    Run Keyword If  '${nom}' != 'null'  Input Text  nom  ${nom}
    # On saisit le prénom
    Run Keyword If  '${prenom}' != 'null'  Input Text  prenom  ${prenom}
    # On saisit la qualité
    Run Keyword If  '${qualite}' != 'null'  Input Text  qualite  ${qualite}
    # On saisit la signature
    Run Keyword If  '${signature}' != 'null'  Input Text  signature  ${signature}
    # On coche "défaut" si spécifié
    Run Keyword If  '${defaut}' == 'true'  Select Checkbox  defaut
    # On décoche "défaut" si spécifié
    Run Keyword If  '${defaut}' == 'false'  Unselect Checkbox  defaut
    # On sélectionne la collectivité
    Run Keyword If  '${collectivite}' != 'null'  Select From List By Label  om_collectivite  ${collectivite}
    # On saisit la date de début de validité
    Run Keyword If  '${debut}' != 'null'  Input Datepicker  om_validite_debut  ${debut}
    # On saisit la date de fin de validite
    Run Keyword If  '${fin}' != 'null'  Input Datepicker  om_validite_fin  ${fin}