*** Settings ***
Documentation     Actions spécifiques aux pièces.

*** Keywords ***
Depuis le contexte de la pièce par le dossier d'instruction

    [Documentation]  Permet d'accéder à l'écran de visualisation de la pièce.

    [Arguments]  ${dossier_instruction}  ${document_numerise}

    #
    Depuis l'onglet des pièces du dossier d'instruction  ${dossier_instruction}
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link  ${document_numerise}


Ajouter une pièce depuis le dossier d'instruction

    [Documentation]  Permet d'ajouter une pièce sur un dossier d'instruction.

    [Arguments]  ${dossier_instruction}  ${document_numerise_values}  ${message}=null

    #
    Depuis l'onglet des pièces du dossier d'instruction  ${dossier_instruction}
    #
    Wait Until Element Is Visible  id=action-soustab-blocnote-message-ajouter
    Click Element  id=action-soustab-blocnote-message-ajouter
    #
    Saisir la pièce  ${document_numerise_values}
    # On valide le formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Submit Button In Subform
    # On vérifie le message de validation
    Run Keyword If  '${message}' != 'null'  Message Should Contain In Subform  ${message}
    Run Keyword If  '${message}' == 'null'  Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    # Retourne l'identifiant du message de notification
    ${dossier_message_id} =  Get Value  dossier_message_id
    [Return]  ${dossier_message_id}


Modifier une pièce depuis le dossier d'instruction

    [Documentation]  Permet de modifier une pièce sur un dossier d'instruction.

    [Arguments]  ${dossier_instruction}  ${document_numerise}  ${document_numerise_values}  ${message}=null

    #
    Depuis le contexte de la pièce par le dossier d'instruction  ${dossier_instruction}  ${document_numerise}
    # On clique sur l'action modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  document_numerise  modifier
    #
    Saisir la pièce  ${document_numerise_values}
    # On valide le formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Submit Button In Subform
    # On vérifie le message de validation
    Run Keyword If  '${message}' != 'null'  Message Should Contain In Subform  ${message}
    Run Keyword If  '${message}' == 'null'  Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.


Supprimer une pièce depuis le dossier d'instruction

    [Documentation]  Permet de supprimer une pièce sur un dossier d'instruction.

    [Arguments]  ${dossier_instruction}  ${document_numerise}  ${message}=null

    #
    Depuis le contexte de la pièce par le dossier d'instruction  ${dossier_instruction}  ${document_numerise}
    # On clique sur laction supprimer
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  document_numerise  supprimer
    # On valide le formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Submit Button In Subform
    # On vérifie le message de validation
    Run Keyword If  '${message}' != 'null'  Message Should Contain In Subform  ${message}
    Run Keyword If  '${message}' == 'null'  Valid Message Should Contain In Subform  La suppression a été correctement effectuée.


Saisir la pièce

    [Documentation]  Saisit les valeurs du formulaire.

    [Arguments]  ${document_numerise_values}

    #
    Si "uid_upload" existe dans "${document_numerise_values}" on execute "Add File" sur "uid"
    Si "date_creation" existe dans "${document_numerise_values}" on execute "Input Datepicker From Css Selector" dans "document_numerise"
    Si "document_numerise_type" existe dans "${document_numerise_values}" on execute "Select From List By Label" dans "document_numerise"


Télécharger toutes les pièces

    [Documentation]  Utilise l'action "Récupérer toutes les pièces" pour récupérer
    ...  l'archive, qui est ensuite enregistrée dans le répertoire courant

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  zip_download_link
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Êtes vous sûr de vouloir télécharger l'intégralité des pièces du dossier
    Click Element  css=button.ui-button:nth-child(1)
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Votre archive est prête,
    Page Should Contain  Cliquez ici pour la télécharger
    ${link} =  Get Element Attribute  archive_download_link@href
    ${output_dir}  ${output_name} =  Télécharger un fichier  ${SESSION_COOKIE}  ${link}  ${EXECDIR}${/}binary_files${/}
    ${full_path_to_file} =  Catenate  SEPARATOR=  ${output_dir}  ${output_name}
    Click Element  css=.ui-icon-closethick

    [Return]  ${full_path_to_file}  ${output_name}


Activer l'option de numérisation
    [Documentation]  Permet d'activer l'option de numérisation.
    ${path} =  Set Variable  ..${/}dyn${/}config.inc.php
    ${config} =  Get File  ${path}
    ${output} =  Replace String  ${config}  ?>  $config["digitalization_folder_path"] = "../var/digitalization/";$config["option_digitalization_folder"] = true;?>
    Remove File  ${path}
    Wait Until Removed  ${path}
    Append To File  ${path}  ${output}


Désactiver l'option de numérisation
    [Documentation]  Permet de désactiver l'option de numérisation
    ${path} =  Set Variable  ..${/}dyn${/}config.inc.php
    ${config} =  Get File  ${path}
    ${output} =  Replace String  ${config}  $config["digitalization_folder_path"] = "../var/digitalization/";$config["option_digitalization_folder"] = true;?>  ?>
    Remove File  ${path}
    Wait Until Removed  ${path}
    Append To File  ${path}  ${output}


Vérifier création répertoire du dossier
    [Documentation]    Nécessite la librairie OperatingSystem
    [Arguments]  ${dossier_instruction}

    # On supprime les espaces
    ${temp} =  Sans espace  ${dossier_instruction}
    # On compte la longueur du libellé
    ${input_lenght} =  Get Length  ${temp}
    # A laquelle on ote 2 (taille du suffixe)
    ${over} =    Evaluate    ${input_lenght}-2
    # On récupère le suffixe
    ${part2} =  Get Substring  ${temp}  ${over}
    # On récupère le préfixe
    ${part1} =  Replace String  ${temp}  ${part2}  ${EMPTY}
    # On concatène les deux, séparés par un point
    ${repertoire} =  Catenate  ${part1}.${part2}
    # On vérifie l'existance du répertoire
    Directory Should Exist  ${EXECDIR}${/}..${/}var${/}digitalization${/}Todo${/}${repertoire}

    [Return]  ${repertoire}


Récupérer le chemin du fichier .info de la pièce stocké
    [Documentation]  Permet de récupérer le chemin du .info d'une pièce numérisée dans le
    ...  cas de l'utilisation du conencteur filesystem.
    [Arguments]  ${di}  ${dn}

    Depuis le contexte de la pièce par le dossier d'instruction  ${di}  ${dn}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Subform Portlet Action  document_numerise  modifier
    ${uid} =  Get Value  uid
    ${path_1} =  Get Substring  ${uid}  0  2
    ${path_2} =  Get Substring  ${uid}  0  4
    [Return]  ..${/}var${/}filestorage${/}${path_1}${/}${path_2}${/}${uid}.info


Ajouter une pièce depuis le dossier contentieux

    [Documentation]  Permet d'ajouter une pièce sur un dossier contentieux.

    [Arguments]  ${contentieux}  ${dossier}  ${document_numerise_values}  ${message}=null

    #

    Run Keyword If  '${contentieux}' == 'infraction'  Depuis l'onglet des pièces du dossier infraction  ${dossier}
    Run Keyword If  '${contentieux}' == 'recours'  Depuis l'onglet des pièces du dossier recours  ${dossier}
    #
    Wait Until Element Is Visible  id=action-soustab-blocnote-message-ajouter
    Click Element  id=action-soustab-blocnote-message-ajouter
    #
    Saisir la pièce dans le contexte ctx  ${document_numerise_values}
    # On valide le formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Submit Button In Subform
    # On vérifie le message de validation
    Run Keyword If  '${message}' != 'null'  Message Should Contain In Subform  ${message}
    Run Keyword If  '${message}' == 'null'  Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    # Retourne l'identifiant du message de notification
    ${dossier_message_id} =  Get Value  dossier_message_id
    [Return]  ${dossier_message_id}


Saisir la pièce dans le contexte ctx

    [Documentation]  Saisit les valeurs du formulaire dans le contexte des contentieux.

    [Arguments]  ${document_numerise_values}

    #
    Si "uid_upload" existe dans "${document_numerise_values}" on execute "Add File" sur "uid"
    Si "date_creation" existe dans "${document_numerise_values}" on execute "Input Datepicker From Css Selector" dans "document_numerise_contexte_ctx"
    Si "document_numerise_type" existe dans "${document_numerise_values}" on execute "Select From List By Label" dans "document_numerise_contexte_ctx"
