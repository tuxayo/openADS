*** Settings ***
Documentation     Actions spécifiques aux demandes

*** Keywords ***
Ajouter un nouveau type de demande depuis le menu
    [Arguments]  ${form_values}

    Depuis le tableau des types de demandes
    # On clique sur l'icone ajouter
    Click On Add Button
    # On remplit le formulaire
    Saisir le type de demande  ${form_values}
    # On valide
    Click On Submit Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # Vérification qu'il n'y a aucune erreur
    Page Should Not Contain Errors

Saisir le type de demande
    [Arguments]  ${form_values}

    Si "code" existe dans "${form_values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${form_values}" on execute "Input Text" dans le formulaire
    # On sélectionne le groupe
    Si "groupe" existe dans "${form_values}" on execute "Select From List By Label" dans le formulaire
    # On sélectionne le type de DA
    Si "dossier_autorisation_type_detaille" existe dans "${form_values}" on execute "Select From List By Label" dans le formulaire
    # On sélectionne la nature de la demande
    Si "demande_nature" existe dans "${form_values}" on execute "Select From List By Label" dans le formulaire
    # etats_autorises
    Si "etats_autorises" existe dans "${form_values}" on execute "Select Multiple By Label" dans le formulaire
    # On sélectionne les contraintes
    Si "contraintes" existe dans "${form_values}" on execute "Select From List By Label" dans le formulaire
    # On sélectionne le type de DI
    Si "dossier_instruction_type" existe dans "${form_values}" on execute "Select From List By Label" dans le formulaire
    # On sélectionne l'événement de la première instruction
    Si "evenement" existe dans "${form_values}" on execute "Select From List By Label" dans le formulaire
    # On sélectionne la collectivité si renseignée
    Si "document_obligatoire" existe dans "${form_values}" on execute "Input Text" dans le formulaire


Depuis le tableau des types de demandes
    Go To Dashboard
    Go To Submenu In Menu  parametrage-dossier  demande_type

Depuis le contexte du type de demande avec libellé unique
    [Arguments]  ${libelle}
    Depuis le tableau des types de demandes
    Use Simple Search  libellé  ${libelle}
    Click on Link  ${libelle}
