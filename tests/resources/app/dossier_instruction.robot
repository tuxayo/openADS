*** Settings ***
Documentation     Actions spécifiques aux dossiers d'instruction.

*** Keywords ***
Depuis le contexte du dossier d'instruction par recherche
    [Documentation]    Permet d'accéder à l'écran de visualisation d'un dossier d'instruction.
    [Arguments]  ${dossier_instruction}

    # On accède directement au tableau de tous les dossiers d'instruction
    Go To Tab  dossier_instruction
    # On supprime les éventuels espaces du libellé
    ${libelle_sans_espace} =  Sans espace  ${dossier_instruction}
    # On fait une recherche sur le libellé du DI
    Input Text  css=div#adv-search-adv-fields input#dossier  ${libelle_sans_espace}
    # On valide le formulaire de recherche
    Click On Search Button
    # On accède à la visualisation du DI
    Click On Link  ${dossier_instruction}
    #
    Page Title Should Contain  ${dossier_instruction}

Depuis le contexte du dossier d'instruction
    [Documentation]    Permet d'accéder à l'écran de visualisation d'un dossier d'instruction.
    [Arguments]  ${dossier_instruction}  ${menu}=null

    # On supprime les éventuels espaces du libellé
    ${libelle_sans_espace} =  Sans espace  ${dossier_instruction}
    # On accède directement au dossier d'instruction
    Run Keyword If  '${menu}' == 'infraction'  Go To  ${PROJECT_URL}scr/form.php?obj=dossier_contentieux_toutes_infractions&action=3&idx=${libelle_sans_espace}
    ...  ELSE IF  '${menu}' == 'recours'  Go To  ${PROJECT_URL}scr/form.php?obj=dossier_contentieux_tous_recours&action=3&idx=${libelle_sans_espace}
    ...  ELSE  Go To  ${PROJECT_URL}scr/form.php?obj=dossier_instruction&action=3&idx=${libelle_sans_espace}
    # On vérifie qu'on est bien sur le DI
    Page Title Should Contain  ${dossier_instruction}


Depuis le contexte du dossier d'instruction de mes encours
    [Documentation]    Permet d'accéder à l'écran de visualisation d'un dossier d'instruction.
    [Arguments]  ${dossier_instruction}

    # On accède directement au tableau de tous les dossiers d'instruction
    Go To Tab  dossier_instruction_mes_encours
    # On supprime les éventuels espaces du libellé
    ${libelle_sans_espace} =  Sans espace  ${dossier_instruction}
    # On fait une recherche sur le libellé du DI
    Use Simple Search  Tous  ${libelle_sans_espace}
    # On accède à la visualisation du DI
    Click On Link  ${dossier_instruction}
    #
    Page Title Should Contain  ${dossier_instruction}


Depuis le formulaire de modification du dossier d'instruction
    [Documentation]
    [Arguments]  ${dossier_instruction}

    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    Click On Form Portlet Action  dossier_instruction  modifier


Depuis l'onglet consultation(s) du dossier d'instruction
    [Documentation]    Permet d'accéder à l'onglet consultation(s) dans le contexte d'un
    ...    dossier d'instruction.
    [Arguments]  ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    #
    On clique sur l'onglet  consultation  Consultation(s)

Depuis l'onglet contrainte(s) du dossier d'instruction
    [Documentation]    Permet d'accéder à l'onglet contrainte(s) dans le contexte d'un
    ...    dossier d'instruction.
    [Arguments]  ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    #
    On clique sur l'onglet  dossier_contrainte  Contrainte(s)

Depuis l'onglet instruction du dossier d'instruction
    [Documentation]    Permet d'accéder à l'onglet instruction dans le contexte d'un
    ...    dossier d'instruction.
    [Arguments]  ${dossier_instruction}  ${menu}=null

    #
    Depuis le contexte du dossier d'instruction  ${dossier_instruction}  ${menu}
    #
    Run Keyword If  '${menu}' == 'infraction'  On clique sur l'onglet  instruction_contexte_ctx_inf  Instruction
    ...  ELSE IF  '${menu}' == 'recours'  On clique sur l'onglet  instruction_contexte_ctx_re  Instruction
    ...  ELSE  On clique sur l'onglet  instruction  Instruction


Depuis l'onglet des pièces du dossier d'instruction

    [Documentation]  Permet d'accéder à l'onglet des pièces dans le contexte
    ...  d'un dossier d'instruction.

    [Arguments]  ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    #
    On clique sur l'onglet  document_numerise  Pièce(s)


Depuis l'onglet des messages du dossier d'instruction

    [Documentation]  Permet d'accéder à l'onglet des messages dans le contexte
    ...  d'un dossier d'instruction.
    [Arguments]  ${dossier_instruction}

    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    On clique sur l'onglet  dossier_message  Message(s)


Ajouter une consultation depuis l'onglet du dossier d'instruction
    [Arguments]  ${dossier_instruction}

    Depuis l'onglet consultation(s) du dossier d'instruction  ${dossier_instruction}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-consultation-corner-ajouter

Ajouter un lot de consultations depuis l'onglet du dossier d'instruction
    [Arguments]  ${dossier_instruction}

    Depuis l'onglet consultation(s) du dossier d'instruction  ${dossier_instruction}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-consultation-corner-ajouter_multiple

Ajouter une contrainte depuis l'onglet du dossier d'instruction
    [Arguments]  ${dossier_instruction}

    Depuis l'onglet contrainte(s) du dossier d'instruction  ${dossier_instruction}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-soustab-dossier_contrainte-corner-ajouter


Saisir le formulaire du dossier d'instruction

    [Documentation]  Permet de saisir le formulaire du dossier d'instruction.
    [Arguments]  ${values}  ${context}=null

    # Ouvre les fieldsets nécessaires
    Run Keyword If  '${context}' == 'infraction'  Open Fieldset  dossier_contentieux_toutes_infractions  localisation
    ...  ELSE IF  '${context}' == 'recours'  Open Fieldset  dossier_contentieux_tous_recours  localisation
    ...  ELSE  Open Fieldset  dossier_instruction  localisation
    Wait Until Element Is Visible  css=#terrain_adresse_voie_numero
    ${exist} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${values}  tax_secteur
    Run Keyword If  '${exist}' == 'True'  Open Fieldset  dossier_instruction  simulation-des-taxes
    Run Keyword If  '${exist}' == 'True'  Wait Until Element Is Visible  css=#tax_secteur

    Si "erp" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "instructeur" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "instructeur_2" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "division" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "tax_secteur" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "terrain_adresse_voie_numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terrain_adresse_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terrain_adresse_lieu_dit" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terrain_adresse_localite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terrain_adresse_code_postal" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terrain_adresse_bp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terrain_adresse_cedex" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terrain_superficie" existe dans "${values}" on execute "Input Text" dans le formulaire


Modifier le dossier d'instruction

    [Documentation]  Permet de modifier le dossier d'instruction.

    [Arguments]  ${dossier_instruction}  ${values}  ${context}=null

    Depuis le contexte du dossier d'instruction  ${dossier_instruction}  ${context}
    # On clique sur l'action modifier du portlet
    Run Keyword If  '${context}' == 'infraction'  Click On Form Portlet Action  dossier_contentieux_toutes_infractions  modifier
    ...  ELSE IF  '${context}' == 'recours'  Click On Form Portlet Action  dossier_contentieux_tous_recours  modifier
    ...  ELSE  Click On Form Portlet Action  dossier_instruction  modifier
    # On saisit le formulaire
    Saisir le formulaire du dossier d'instruction  ${values}  ${context}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Saisir les données techniques pour le calcul des impositions

    [Documentation]  Permet de saisir le formulaire données techniques pour le
    ...  calcul des impositions.

    [Arguments]  ${values}

    # On déplie le fieldset de la taxe d'aménagement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open Fieldset In Subform  donnees_techniques  declaration-des-elements-necessaires-au-calcul-des-impositions
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open Fieldset In Subform  donnees_techniques  exonerations
    # On saisit les données
    Wait Until Element Is Visible  css=#tax_surf_tot_cstr
    Si "tax_surf_tot_cstr" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_surf_loc_stat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_surf_tot" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_surf" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_surf_suppr_mod" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_log_nb1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_log_nb2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_log_nb3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_log_nb4" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_log_nb_tot1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_log_nb_tot2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_log_nb_tot3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_log_nb_tot4" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf4" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf_sup1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf_sup2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf_sup3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf_sup4" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_log_nb1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_log_nb2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_log_nb3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_log_nb_tot1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_log_nb_tot2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_log_nb_tot3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_surf1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_surf2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_surf3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_surf_sup1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_surf_sup2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_surf_sup3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_secon_log_nb" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_tot_log_nb" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_secon_log_nb_tot" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_tot_log_nb_tot" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_secon_surf" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_tot_surf" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_secon_surf_sup" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_tot_surf_sup" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_ext_pret" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "tax_ext_desc" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_surf_tax_exist_cons" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_log_exist_nb" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_am_statio_ext" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_sup_bass_pisc" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_empl_ten_carav_mobil_nb" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_empl_hll_nb" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_eol_haut_nb" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_pann_volt_sup" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_am_statio_ext_sup" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_sup_bass_pisc_sup" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_empl_ten_carav_mobil_nb_sup" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_empl_hll_nb_sup" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_eol_haut_nb_sup" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_pann_volt_sup_sup" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_trx_presc_ppr" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "tax_monu_hist" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "tax_comm_nb" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf4" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf5" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf6" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf7" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_sup1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_sup2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_sup3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_sup4" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_sup5" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_sup6" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_sup7" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_dest_loc_tr" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_surf_loc_stat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf_stat1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf_stat2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf_stat3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_princ_surf_stat4" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_secon_surf_stat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_surf_stat1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_surf_stat2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_heber_surf_stat3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_tot_surf_stat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_stat1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_stat2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_stat3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_stat4" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_stat5" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_stat6" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_non_habit_surf_stat7" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_su_parc_statio_expl_comm_surf" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_log_ap_trvx_nb" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_am_statio_ext_cr" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_sup_bass_pisc_cr" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_empl_ten_carav_mobil_nb_cr" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_empl_hll_nb_cr" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_eol_haut_nb_cr" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_pann_volt_sup_cr" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_surf_loc_arch" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_surf_pisc_arch" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_am_statio_ext_arch" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_empl_ten_carav_mobil_nb_arch" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_empl_hll_nb_arch" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_eol_haut_nb_arch" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tax_desc" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mtn_exo_ta_part_commu" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mtn_exo_ta_part_depart" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mtn_exo_ta_part_reg" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mtn_exo_rap" existe dans "${values}" on execute "Input Text" dans le formulaire


Modifier les données techniques pour le calcul des impositions

    [Documentation]  Permet de modifier les données techniques pour le calcul
    ...  des impositions du dossier d'instruction.

    [Arguments]  ${dossier_instruction}  ${values}

    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    # On clique sur l'action données techniques du portlet
    Click On Form Portlet Action  dossier_instruction  donnees_techniques
    # On clique sur l'action modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  donnees_techniques  modifier
    # On saisit le formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Saisir les données techniques pour le calcul des impositions  ${values}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Vos modifications ont bien été enregistrées.


Depuis le contexte du rapport d'instruction

    [Documentation]  Depuis la fiche du rapport d'instruction.

    [Arguments]  ${dossier_instruction}

    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    # On clique sur l'action du rapport d'instruction
    Click On Form Portlet Action  dossier_instruction  rapport_instruction


Modifier le rapport d'instruction
    [Documentation]  Permet de modifier le rapport d'instruction.
    [Arguments]  ${di}  ${values}

    Depuis le contexte du rapport d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  rapport_instruction  modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Saisir le rapport d'instruction  ${values}
    Click On Submit Button In Subform
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Saisir le rapport d'instruction
    [Documentation]  Remplit le formulaire du rapport d'instruction.
    [Arguments]  ${values}

    Si "analyse_reglementaire_om_html" existe dans "${values}" on execute "Input HTML" dans "rapport_instruction"
    Si "description_projet_om_html" existe dans "${values}" on execute "Input HTML" dans "rapport_instruction"
    Si "proposition_decision" existe dans "${values}" on execute "Input Text" dans "rapport_instruction"


Ajouter le rapport d'instruction
    [Documentation]  Permet d'ajouter le rapport d'instruction du DI.
    [Arguments]  ${di}  ${values}

    Depuis le contexte du rapport d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Saisir le rapport d'instruction  ${values}
    Click On Submit Button In Subform
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform


Finaliser le rapport d'instruction
    [Documentation]  Permet de finaliser le rapport d'instruction du DI.
    [Arguments]  ${di}

    Depuis le contexte du rapport d'instruction  ${di}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On SubForm Portlet Action  rapport_instruction  finalise
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be  La finalisation du document s'est effectuée avec succès.
    Click On Back Button In Subform


Ajouter et finaliser le rapport d'instruction
    [Documentation]  Mot-clef raccourcis permettant d'ajouter et de finaliser le
    ...  rapport d'instruction du DI.
    [Arguments]  ${di}  ${values}

    Ajouter le rapport d'instruction  ${di}  ${values}
    Finaliser le rapport d'instruction  ${di}


Modifier les données techniques pour le calcul des surfaces

    [Documentation]  Permet de modifier les données techniques pour le calcul
    ...  des impositions du dossier d'instruction.

    [Arguments]  ${dossier_instruction}  ${donnees_techniques_values}

    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    # On clique sur l'action données techniques du portlet
    Click On Form Portlet Action    dossier_instruction    donnees_techniques
    # On clique sur l'action modifier
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click On SubForm Portlet Action  donnees_techniques  modifier
    # On saisit le formulaire
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Saisir les données techniques pour le calcul des surfaces  ${donnees_techniques_values}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Saisir les données techniques pour le calcul des surfaces

    [Documentation]

    [Arguments]  ${donnees_techniques_values}

    # On déplie le fieldset "Construire"
    Open Fieldset In Subform  donnees_techniques  construire
    # On déplie le fieldset "Destinations et surfaces des constructions"
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Open Fieldset In Subform  donnees_techniques  destinations-et-surfaces-des-constructions
    #
    Si "su_avt_shon1" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_avt_shon2" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_avt_shon3" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_avt_shon4" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_avt_shon5" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_avt_shon6" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_avt_shon7" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_avt_shon8" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_avt_shon9" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_cstr_shon1" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_cstr_shon2" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_cstr_shon3" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_cstr_shon4" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_cstr_shon5" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_cstr_shon6" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_cstr_shon7" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_cstr_shon8" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_cstr_shon9" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_chge_shon1" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_chge_shon2" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_chge_shon3" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_chge_shon4" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_chge_shon5" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_chge_shon6" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_chge_shon7" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_chge_shon8" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_chge_shon9" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_demo_shon1" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_demo_shon2" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_demo_shon3" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_demo_shon4" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_demo_shon5" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_demo_shon6" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_demo_shon7" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_demo_shon8" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_demo_shon9" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_sup_shon1" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_sup_shon2" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_sup_shon3" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_sup_shon4" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_sup_shon5" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_sup_shon6" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_sup_shon7" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_sup_shon8" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su_sup_shon9" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"

    # Tableau des surface (version 2)
    Si "su2_avt_shon1" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon2" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon3" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon4" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon5" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon6" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon7" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon8" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon9" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon10" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon11" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon12" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon13" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon14" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon15" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon16" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon17" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon18" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon19" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_avt_shon20" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon1" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon2" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon3" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon4" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon5" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon6" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon7" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon8" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon9" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon10" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon11" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon12" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon13" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon14" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon15" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon16" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon17" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon18" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon19" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_cstr_shon20" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon1" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon2" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon3" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon4" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon5" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon6" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon7" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon8" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon9" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon10" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon11" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon12" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon13" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon14" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon15" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon16" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon17" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon18" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon19" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_chge_shon20" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon1" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon2" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon3" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon4" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon5" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon6" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon7" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon8" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon9" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon10" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon11" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon12" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon13" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon14" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon15" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon16" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon17" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon18" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon19" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_demo_shon20" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon1" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon2" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon3" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon4" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon5" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon6" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon7" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon8" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon9" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon10" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon11" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon12" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon13" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon14" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon15" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon16" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon17" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon18" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon19" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Si "su2_sup_shon20" existe dans "${donnees_techniques_values}" on execute "Input Text" dans "donnees_techniques"
    Sleep  1

Saisir les données techniques du DI

    [Documentation]  Permet de saisir les données techniques dans n'importe quel fieldset
    ...  Prend en paramètre le n° de dossier d'inscription, et la liste de données
    ...  techniques à insérer sous la forme de dictionary ex : ope_proj_desc=testset

    [Arguments]  ${dossier_instruction}  ${donnees_techniques_values}


    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    # On clique sur l'action données techniques du portlet
    Click On Form Portlet Action    dossier_instruction    donnees_techniques
    # On clique sur l'action modifier
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click On SubForm Portlet Action  donnees_techniques  modifier
    Saisir les données techniques  ${donnees_techniques_values}

Saisir les données techniques

    [Arguments]  ${donnees_techniques_values}  ${class_suffix}=${EMPTY}

    # Contient l'id de tous les fieldsets qui seront dépliés automatiquement
    @{liste_fieldsets} =  Create List
    ...  terrain
    ...  description-de-la-demande---du-projet
    ...  construire,-amenager-ou-modifier-un-erp
    ...  amenager
    ...  projet-d_amenagement
    ...  description-amenagement
    ...  complement-d_amenagement
    ...  construire
    ...  projet-construction
    ...  complement-construction
    ...  destinations-et-surfaces-des-constructions
    ...  divers-construction
    ...  demolir
    ...  ouverture-de-chantier
    ...  achevement-des-travaux
    ...  declaration-dintention-daliener-un-bien
    ...  designation-du-bien
    ...  usage-et-occupation
    ...  droits-reels-ou-personnels
    ...  modalites-de-la-cession
    ...  vente-amiable
    ...  adjudication
    ...  les-soussignes-declarent
    ...  observations
    ...  cnil-opposition-a-lutilisation-des-informations-du-formulaire-a-des-fins-commerciales
    ...  declaration-des-elements-necessaires-au-calcul-des-impositions
    ...  declaration-dintention-daliener-un-bien
    ...  designation-du-bien
    ...  usage-et-occupation
    ...  droits-reels-ou-personnels
    ...  modalites-de-la-cession
    ...  les-soussignes-declarent
    ...  observations
    ...  contentieux

    # Déplie tous les fieldsets présents dans la liste {liste_fieldsets}
    :FOR  ${values}  IN  @{liste_fieldsets}
    \  Open Fieldset In Subform  donnees_techniques${class_suffix}  ${values}

    # On convertit le dictionnaire en liste
    ${items}=    Get Dictionary Items    ${donnees_techniques_values}
    # Pour chaque couple champ-valeur dans la liste
    :FOR    ${field}    ${value}    IN    @{items}
    # On saisit la valeur dans le champ
    \  Run Keyword If  "${value}" == "t"  Select Checkbox  css=#${field}  ELSE IF  "${value}" == "f"  Unselect Checkbox  css=#${field}  ELSE  Input Text  css=#${field}  ${value}

    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be  Vos modifications ont bien été enregistrées.

Depuis l'onglet Dossiers Liés du dossier d'instruction

    [Documentation]  Permet d'accéder à l'onglet des dossiers liés dans le contexte
    ...  d'un dossier d'instruction.

    [Arguments]  ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    On clique sur l'onglet  lien_dossier_dossier  Dossiers Liés


Le dossier d'instruction doit exister

    [Documentation]  Vérifie que le dossier a bien été créé.
    [Arguments]  ${dossier_instruction}

    # On se rend sur le dossier d'instruction directement par URL
    Depuis le contexte du dossier d'instruction  ${dossier_instruction}

Le dossier d'instruction ne doit pas exister

    [Documentation]  Vérifie que le dossier n'a pas été crée.
    [Arguments]  ${dossier_instruction}

    # On supprime les éventuels espaces du libellé
    ${libelle_sans_espace} =  Sans espace  ${dossier_instruction}
    # On accède directement à la page des DI
    Go To  ${PROJECT_URL}scr/form.php?obj=dossier_instruction&action=3&idx=${libelle_sans_espace}
    # On vérifie qu'on voit bien la page d'erreur de dossier non trouvé.
    # Qui est identique à celle d'accès réfusé.
    Error Message Should Be  Droits insuffisants. Vous n'avez pas suffisamment de droits pour acceder à cette page.
    Page Should Not Contain  ${dossier_instruction}
