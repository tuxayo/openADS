#!/usr/bin/python
# -*- coding: utf-8 -*-
from resources.core.om_tests import om_tests_core


class om_tests(om_tests_core):
    """
    """

    _database_name_default = 'openads'

    _instance_name_default = 'openads'

    _params_copy_files = [
        #
        {'in': 'tests/binary_files/tests_services/referentiel_erp_test.php',
         'out': 'tests_services/'},
        {'in': 'tests/binary_files/tests_services/rest_entry.php',
         'out': 'tests_services/'},
        {'in': 'tests/binary_files/tests_services/REST/demande_api.php',
         'out': 'tests_services/REST/'},
        {'in': 'tests/binary_files/tests_services/metier/demandemanager.php',
         'out': 'tests_services/metier/'},
        #
        {'in': 'tests/binary_files/filestorage/*', 'out': 'var/filestorage/'},
        {'in': 'tests/binary_files/dyn/*', 'out': 'dyn/'},
        {'in': 'tests/binary_files/htaccess_deny_from_all', 'out': 'var/.htaccess'},
    ]

    _params_create_folders = [
        'tests_services/',
        'tests_services/REST/',
        'tests_services/metier/',
        'var/',
        'var/filestorage/',
        'var/digitalization/',
        'var/digitalization/Todo/',
        'var/digitalization/Done/',
        'var/tmp/',
        'var/log/',
    ]

    _params_chmod_777 = [
        # apache doit pouvoir écrire dans les répertoires de storage et de log
        'var/',
        # apache doit pouvoir écrire dans les répertoires destinés à recevoir 
        # des fichiers générés
        'gen/obj/', 'gen/sql/pgsql/', 
        'core/obj/', 'core/sql/pgsql/',
        'obj', 'sql/pgsql/',
        'tests/resources/core/gen',
        'tests/resources/app/gen',
        # Les fichiers de configuration doivent être modifiables
        'dyn/',
    ]

