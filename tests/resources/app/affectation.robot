*** Settings ***
Documentation  Actions spécifiques aux affectations automatiques.

*** Keywords ***
Depuis le tableau des affectations
    [Documentation]  Permet d'accéder au tableau des affectations automatiques.

    # On ouvre le tableau
    Go To Tab  affectation_automatique

Depuis le contexte de l'affectation
    [Documentation]  Permet d'accéder au formulaire en consultation
    ...    d'une affectation.
    [Arguments]  ${instructeur}=null  ${type_dad}=null

    # On ouvre le tableau des affectations automatiques
    Depuis le tableau des affectations
    # On recherche l'affectation
    Run Keyword If    '${instructeur}' != 'null'    Use Simple Search    instructeur    ${instructeur}    ELSE IF    '${type_dad}' != 'null'    Use Simple Search    type de dossier d'autorisation détaillé    ${type_dad}    ELSE    Fail
    # On clique sur l'affectation
    Run Keyword If    '${instructeur}' != 'null'    Click On Link    ${instructeur}    ELSE IF    '${type_dad}' != 'null'    Click On Link    ${type_dad}    ELSE    Fail

Ajouter l'affectation depuis le menu
    [Documentation]  Permet d'ajouter une affectation.
    [Arguments]  ${values}

    # On ouvre le tableau des affectations automatiques
    Depuis le tableau des affectations
    # On clique sur l'icone d'ajout
    Click On Add Button
    # On remplit le formulaire
    Saisir l'affectation  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Saisir l'affectation
    [Documentation]  Permet de remplir le formulaire d'une affectation.
    [Arguments]  ${values}

    Si "arrondissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "quartier" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "section" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "instructeur" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "instructeur_2" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_autorisation_type_detaille" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

Supprimer l'affectation depuis le menu

    [Documentation]

    [Arguments]  ${instructeur}=null  ${type_dad}=null

    #
    Depuis le contexte de l'affectation  ${instructeur}  ${type_dad}
    # On clique sur l'action supprimer
    Click On Form Portlet Action  affectation_automatique  supprimer
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  La suppression a été correctement effectuée.