*** Settings ***
Documentation  Ressources (librairies, keywords et variables)

# Mots-clefs framework
Resource  core${/}om_resources.robot

# Mots-clefs métier
Resource  app${/}affectation.robot
Resource  app${/}bible.robot
Resource  app${/}bordereau.robot
Resource  app${/}commission.robot
Resource  app${/}commission_type.robot
Resource  app${/}contrainte.robot
Resource  app${/}contrainte_parametree.robot
Resource  app${/}consultation.robot
Resource  app${/}demande.robot
Resource  app${/}demande_type.robot
Resource  app${/}demandeur.robot
Resource  app${/}direction.robot
Resource  app${/}division.robot
Resource  app${/}document_numerise.robot
Resource  app${/}document_numerise_type.robot
Resource  app${/}document_numerise_type_categorie.robot
Resource  app${/}dossier_autorisation.robot
Resource  app${/}dossier_autorisation_type.robot
Resource  app${/}dossier_commission.robot
Resource  app${/}dossier_infraction.robot
Resource  app${/}dossier_instruction.robot
Resource  app${/}dossier_instruction_type.robot
Resource  app${/}dossier_message.robot
Resource  app${/}dossier_recours.robot
Resource  app${/}evenement.robot
Resource  app${/}export_import.robot
Resource  app${/}import_specific.robot
Resource  app${/}instructeur.robot
Resource  app${/}instruction.robot
Resource  app${/}lien_om_utilisateur_groupe.robot
Resource  app${/}lot.robot
Resource  app${/}navigation.robot
Resource  app${/}service.robot
Resource  app${/}signataire.robot
Resource  app${/}statistique.robot
Resource  app${/}suivi.robot
Resource  app${/}taxe_amenagement.robot
Resource  app${/}om_profil.robot
Resource  app${/}om_utilisateur.robot
Resource  app${/}om_widget.robot
Resource  app${/}formulaire.robot
Resource  app${/}demandeur.robot
Resource  app${/}phase.robot
Resource  app${/}dossier_message.robot
Resource  app${/}commission.robot
Resource  app${/}document_numerise_type_categorie.robot
Resource  app${/}document_numerise_type.robot
Resource  app${/}cerfa.robot
Resource  app${/}dossier_autorisation_type_detaille.robot
Resource  app${/}action.robot
Resource  app${/}menu.robot

*** Variables ***
${SERVER}          localhost
${PROJECT_NAME}    openads
${BROWSER}         firefox
${DELAY}           0
${ADMIN_USER}      admin
${ADMIN_PASSWORD}  admin
${PROJECT_URL}     http://${SERVER}/${PROJECT_NAME}/
${PATH_BIN_FILES}  ${EXECDIR}${/}binary_files${/}
${TITLE}           :: openMairie :: openADS
${SESSION_COOKIE}  openads

*** Keywords ***
For Suite Setup
    # Les keywords définit dans le resources.robot sont prioritaires
    Set Library Search Order  resources
    Ouvrir le navigateur
    Tests Setup

For Suite Teardown
    Fermer le navigateur

