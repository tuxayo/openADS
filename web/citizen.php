<?php
/**
 * Ce fichier permet d'afficher le portail d'accès citoyen aux utilisateurs
 * anonymes.
 *
 * @package openfoncier
 * @version SVN : $Id$
 */

// La classe openads_web qui est une surcharge de la classe utils est nécessaire
require_once "web.class.php";
// Instanciation d'openads_web
$f = new openads_web("anonym");

// Instanciation de la classe dossier_autorisation
require_once "../obj/dossier_autorisation.class.php";
$inst = new dossier_autorisation(0, $f->db, null);

// On affiche la fiche anonyme du dossier d'autorisation
$inst->view_consulter_anonym($f->get_content_only_param());

?>
