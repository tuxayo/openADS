<?php
/**
 * Ce script a pour objet de recuperer la liste des bailleurs correspondant aux
 * critères de recherche
 *
 * @package openfoncier
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");
$f->isAccredited(array("demande","demande_modifier","demande_ajouter"), "OR");
//Récupération des valeurs envoyées
$f->set_submitted_value();
$f->disableLog();

// Donnees
$par_nom = ($f->get_submitted_post_value("particulier_nom") != null) ? $f->get_submitted_post_value("particulier_nom") : "";
$par_nom = str_replace('*', '', $par_nom);
$par_nom = $f->db->escapeSimple($par_nom);

$par_prenom = ($f->get_submitted_post_value("particulier_prenom'") != null) ? $f->get_submitted_post_value("particulier_prenom'") : "";
$par_prenom = str_replace('*', '', $par_prenom);
$par_prenom = $f->db->escapeSimple($par_prenom);

$mor_denomination = ($f->get_submitted_post_value("personne_morale_denomination") != null) ? $f->get_submitted_post_value("personne_morale_denomination") : "";
$mor_denomination = str_replace('*', '', $mor_denomination);
$mor_denomination = $f->db->escapeSimple($mor_denomination);

$mor_nom = ($f->get_submitted_post_value("mor_nom") != null) ? $f->get_submitted_post_value("mor_nom") : "";
$mor_nom = str_replace('*', '', $mor_nom);
$mor_nom = $f->db->escapeSimple($mor_nom);

$om_collectivite = ($f->get_submitted_post_value("om_collectivite") != null) ? $f->get_submitted_post_value("om_collectivite") : $_SESSION['collectivite'];
$listData = "";

$requete = "frequent is TRUE AND 
            type_demandeur = 'bailleur' AND";
if($par_nom != "") {
    $requete .= " particulier_nom ILIKE '%$par_nom%'";
    $requete .= " AND";
}
if($par_prenom != "") {
    $requete .= " particulier_prenom ILIKE '%$par_prenom%'";
    $requete .= " AND";
}

if($mor_denomination != "") {
    $requete .= " personne_morale_denomination ILIKE '%$mor_denomination%'";
    $requete .= " AND";
}

if($mor_nom != "") {
    $requete .= " personne_morale_nom ILIKE '%$mor_nom%'";
    $requete .= " AND";
}

// Ajoute une condition sur la collectivité de l'utilisateur, ou celle de niveau 2
$requete .= ' (om_collectivite = '.$om_collectivite;
$requete .= ' OR om_collectivite = (
    SELECT om_collectivite from '.DB_PREFIXE.'om_collectivite
    WHERE niveau=\'2\'
))';

$sql = 'SELECT
    demandeur as value,
    trim(concat(particulier_nom,\' \', particulier_prenom, \' \',
    personne_morale_raison_sociale, \' \', personne_morale_denomination,
    \' \', personne_morale_nom, \' \', personne_morale_siret, \' \',
    personne_morale_nom, \' \', personne_morale_prenom, \' \',
    code_postal, \' \', localite)) as content
FROM '.DB_PREFIXE.'demandeur
WHERE '.$requete;

$res = $f->db->query($sql);
$f->isDatabaseError($res);
$listData=array();
while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
    $listData[] = $row;
}

echo json_encode($listData);

?>