<?php
/**
 * WIDGET DASHBOARD - widget_dossier_contentieux_alerte_visite.
 *
 * Ce script permet d'interfacer le widget 'Alerte Visite'.
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) {
    $f = new utils(null, "widget_dossier_mes_contradictoires", _("Widget - Mes contradictoires"));
}

/**
 *
 */
//
require_once "../obj/om_widget.class.php";
$om_widget = new om_widget(0);
//
if (!isset($content)) {
    $content = null;
}
//
$om_widget->view_widget_dossier_contentieux_alerte_visite($content);

?>
