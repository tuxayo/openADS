/**
 * Script JS spécifique à l'applicatif, ce script est destiné à être
 * appelé en dernier dans la pile des fichiers JS.
 *
 * @package openfoncier
 * @version SVN : $Id: script.js 6066 2016-03-01 11:11:26Z nhaye $
 */

/**
 * Méthode exécutée au chargement de la page.
 * Permet, au rafraichissement de la page, d'avoir le focus des champs à saisir
 * et le bind des actions direct du portlet.
 *
 * @return {[type]} [description]
 */
function initBindFocus(){

    /**
     * Ajout du focus sur le champ login au chargement de la page.
     */
     $('#login_form #login').focus();

    /**
     * Spécifique à l'action affichage_reglementaire_attestation de la classe 
     * demande pour donner le focus sur le champ de recherche au chargement de
     * la page.
     */
     $('#affichage_reglementaire_attestation_form #dossier').focus();

    /**
     * WIDGET DASHBOARD - widget_recherche_dossier.
     *
     * Spécifique app/widget_recherche_dossier.php pour donner le
     * focus sur le champ de recherche au chargement de la page.
     */
     $('#widget_recherche_dossier_form #dossier').focus();

    /**
     * WIDGET DASHBOARD - widget_recherche_dossier_par_type.
     *
     * Spécifique app/widget_recherche_dossier_par_type.php pour donner le
     * focus sur le champ de recherche au chargement de la page.
     */
     $('#widget_recherche_dossier_par_type_form #dossier').focus();

   /**
    * Spécifique app/suivi_retours_de_consultation.php pour donner le focus sur le champ de
    * recherche au chargement de la page.
    */
   $('#suivi_retours_de_consultation_form #code_barres').focus();

    /**
    * Spécifique app/suivi_mise_a_jour_des_dates.php pour donner le focus sur le champ
    * instruction au chargement de la page si la date n'est pas vide.
    */
    if($('#suivi_mise_a_jour_des_dates_form #date').val()!="") {
        $('#suivi_mise_a_jour_des_dates_form #code_barres').focus();
    }
    
    /**
    * Spécifique app/suivi_envoi_lettre_rar.php pour donner le focus sur le champ
    * liste des codes barres d'instructions scannés au chargement de la page.
    */
    $('#suivi_envoi_lettre_rar_form #liste_code_barres_instruction').focus();

    /**
    * Spécifique app/suivi_mise_a_jour_des_dates.php pour donner le focus sur le champ
    * instruction au chargement de la page si la date n'est pas vide.
    */
    if($('#bordereau_envoi_maire #date').val()!="") {
        $('#bordereau_envoi_maire #code_barres').focus();
    }
    
    /**
     * Sur les widgets du tableau de bord, si on détecte un bloc d'aide
     * alors on le déplace dans le titre du widget (i).
     */
    $("#dashboard .widget-content div.widget-help").each(function(){
        widget = $(this).parent().parent().parent();
        header = widget.find(".widget-header");
        header.prepend($(this));
    });

    /**
     * Affichage d'un dialog pour les règles de calcul associés à l'action
     */
    $('.regle_action').dialog({
        autoOpen: false,
        show: "fade",
        hide: "fade",
        dialogClass: "alert",
        minHeight: 30,
        minWidth: 400
    });
    
    $('.wf_evenement_action').mouseover(
        function(){
            var id = $(this).attr("id");
            $( "#regle_action"+ id ).dialog({ position: { my: "left top", at: "left bottom", of: $(this) } });
            $( "#regle_action"+ id ).dialog("open");
            $(".ui-dialog-titlebar").hide();
        }
    );
    
    $('.wf_evenement_action').mouseleave(
        function(){
            var id = $(this).attr("id");
            $( "#regle_action"+ id ).dialog( "close" );
        }
    );

    /**
     * Plugin jquery qui bind les actions du formulaire dossier_instruction pour
     * ouvrir des overlay.
     *
     * @param string action Identifiant de l'action
     * @param string obj    Formulaire ouvert en overlay
     *
     * @return void
     */
    (function($){
        //Within this wrapper, $ is the jQuery namespace
        $.fn.bind_action_for_overlay = function(obj, width, height, callback, callbackParams) {
            if( typeof(width) == 'undefined' ){
                width = 'auto';
            }
            if( typeof(height) == 'undefined' ){
                height = 'auto';
            }
            if( typeof(callback) == 'undefined' ){
                callback = '';
            }
            if( typeof(callbackParams) == 'undefined' ){
                callbackParams = '';
            }

            // bind de la function passée en arg sur l'event click des actions portlet
            $(this).off("click").on("click", function(event) {
                //
                elem_href = $(this).attr('href');
                if (elem_href != '#') {
                    $(this).attr('data-href', elem_href);
                    $(this).attr('href', '#');
                }
                //
                popupIt(obj, $(this).attr('data-href'), width, height, callback, callbackParams);
            });
            return $(this);
        }
    })(jQuery);

    // Bind actions données techniques depuis dossier instruction
    $('a[id^=action-form-dossier_instruction][id$=-donnees_techniques]').each(function(){
        $(this).bind_action_for_overlay("donnees_techniques");
    });
    // Bind actions données techniques depuis dossier contentieux
    $('a[id^=action-form-dossier_contentieux][id$=-donnees_techniques]').each(function(){
        $(this).bind_action_for_overlay("donnees_techniques_contexte_ctx");
    });
    // Bind actions rapport d'instruction depuis dossier instruction
    $('a[id^=action-form-dossier_instruction][id$=-rapport_instruction]').each(function(){
        $(this).bind_action_for_overlay("rapport_instruction");
    });
    // Bind actions geolocalisation depuis dossier instruction
    $('a[id^=action-form-dossier_instruction][id$=-geolocalisation]').each(function(){
        $(this).bind_action_for_overlay("geolocalisation_sig");
    });
    // Bind actions geolocalisation depuis dossier contentieux
    $('a[id^=action-form-dossier_contentieux][id$=-geolocalisation]').each(function(){
        $(this).bind_action_for_overlay("geolocalisation_sig");
    });
    // Bind actions geolocalisation depuis demande_avis
    $('#action-sousform-demande_avis_encours-rendre_avis').each(function(){
        $(this).bind_action_for_overlay("demande_avis_encours", "auto", "auto", returnToTab, 'demande_avis_encours');
    });

 }

// Document is ready
$(initBindFocus);

/**
 * Surcharge de la fonction ajaxIt spécifique au formulaire ouvert en
 * overlay ayant des actions directs.
 *
 * @param string objsf Objet ouvert en sous-formulaire
 * @param string link  Lien vers contenu à afficher
 *
 * @return void
 */
function overlayIt(objsf, link) {
    // recuperation du terme recherche
    var recherche = document.getElementById("recherchedyn");
    if (recherche != null) {
        link += "&recherche="+recherche.value;
    }else {
        link += "&recherche=";
    }
    // execution de la requete en POST
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            $("#sousform-href").attr("id","sousform-href-disabled");
            $("#sousform-"+objsf).empty();
            $("#sousform-"+objsf).append(html);
            // Affiche la div necessaire aux actions directs
            var href = '<div id="sousform-href" ></div>'
            $("#sousform-"+objsf).append(href);
            if ($("#sousform-href").length) {
                $("#sousform-href").attr("data-href", link);
            }
            om_initialize_content(true);
        }
    });
}

/**
 * WIDGET liés au formulaire et sousformulaire
 * 
 * Ces fonctions javascript sont appelées depuis les méthodes setOnChange,
 * setOnClick, ...
 */
//
// Cette fonction permet de retourner les informations sur le fichier téléchargé
// du formulaire d'upload vers le formulaire d'origine
function bible_return(form, champ) {
    // Initialisation de la variable contenant les valeurs sélectionnées
    var listeElement = '';
    // Récupération du contenu de chacun des éléments cochés
    $("span.content").each(function( index ) {
        if (document.getElementById('checkbox'+index).checked == true) {
            listeElement += $(this).attr('title').replace(/\r\n|\n|\r/g, '<br />')+'<br />';
        }
    });
    obj = tinyMCE.get(champ);
    // Remplissage du textarea et déclenchement du trigger autosize
    if(listeElement != '') {
        obj.setContent(obj.getContent()+"<br/>"+listeElement+"<br/>");
    }
    // Fermeture de la boite de dialog
    $('#upload-container').dialog('close').remove();
}
//
function bible(numero) {
    //
    var ev = document.f2.evenement.value;
    //
    if (ev == "") {
        window.alert("Vous devez d'abord sélectionner un événement.");
        return false;
    }
    //
    var idx = document.f2.dossier.value;
    //
    var link = "../scr/form.php?obj=instruction&action=130&complement="+numero+"&ev="+ev+"&idx="+idx;
    load_form_in_modal(link);
    //
    return false;
}
// bible_auto - type httpclick
function bible_auto(){
    // Récupération de l'identifiant de l'événement
    var ev=document.f2.evenement.value;
    // Si pas d'événement on affiche un message d'erreur
    if (ev == "") {
        window.alert("Vous devez d'abord sélectionner un événement.");
        return false;
    }
    // Récupération de l'identifiant du dossier
    var idx=document.f2.dossier.value;
    // Récupération des retours de consultation et de la bible
    $.ajax({
        type: "GET",
        url: "../scr/form.php?obj=instruction&action=140&idx="+idx+"&ev="+ev,
        cache: false,
        dataType: "json",
        success: function(data){
        
            // Remplissage du textarea complement_om_html
            if(data.complement_om_html != '') {
                var obj = tinyMCE.get('complement_om_html');
                obj.setContent(obj.getContent()+"<br/>"+data.complement_om_html+"<br/>");
            }
            // Remplissage du textarea complement2_om_html
            if(data.complement2_om_html != '') {
                var obj2 = tinyMCE.get('complement2_om_html');
                obj2.setContent(obj2.getContent()+"<br/>"+data.complement2_om_html+"<br/>");
            }
            // Remplissage du textarea complement2_om_html
            if(data.complement3_om_html != '') {
                var obj3 = tinyMCE.get('complement3_om_html');
                obj3.setContent(obj3.getContent()+"<br/>"+data.complement3_om_html+"<br/>");
            }
            // Remplissage du textarea complement4_om_html
            if(data.complement4_om_html != '') {
                var obj4 = tinyMCE.get('complement4_om_html');
                obj4.setContent(obj4.getContent()+"<br/>"+data.complement4_om_html+"<br/>");
            }
        }
    });

    return false;
}
// VerifNumdec - type text
function VerifNumdec(champ) {
    champ.value = champ.value.replace(",", "."); // remplacement de la virgule
    //if (champ.value.lastIndexOf(".") == -1){ // champ decimal
        if (isNaN(champ.value)) {
            alert(msg_alert_error_verifnum);
            champ.value = "";
            return;
        }
    //}
    
}

/**
 * Cette fonction permet de compléter le champ par des zéro par la gauche
 * @param  string  champ  Champ concerné
 * @param  integer length Taille du champ retourné
 */
function str_pad_js(champ, length) {

    // Initialisation de la variable str
    var str = '' + champ.value;

    // Si le champ n'est pas vide
    if (str != '') {
        // Tant que la taille n'est pas atteint,
        // on ajoute des 0
        while (str.length < length) {
            str = '0' + str;
        }
        // Modifie le champ
        champ.value = str;
    }
}

// Ce widget permet de charger les données d'un select en ajax
function changeDataSelect(tableName, linkedField, joker){
    var id_dossierAutorisation = $("#dossier_autorisation").val();
    var id = $("#"+linkedField).val();
    link = "../app/listData.php?idx=" + id + "&tableName=" + tableName +
            "&linkedField=" + linkedField ;
    if(id_dossierAutorisation != "") {
        link += "&nature=EXIST";
    }
    var val_tableName = $('#'+tableName).val();
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(html){
            
            $('#'+tableName).empty();
            var selected = "";
            if(val_tableName == "") {
                selected=' selected="selected"';
            }
            if ( joker == true )
                $('#'+tableName).append(
                    '<option value=""'+selected+'>*</option>'
                );
            else {
                $('#'+tableName).append(
                    '<option value=""'+selected+'>Choisir ' + tableName + '</option>'
                );
            }
            if ( html !== '' ){
                
                html = html.split(';');
                for ( i = 0 ; i < html.length - 1 ; i++ ){
                    
                    html_temp = html[i].split('_');
                    selected = "";
                    if(val_tableName == html_temp[0]) {
                        selected=' selected="selected"';
                    }
                    $('#'+tableName).append(
                        '<option value="'+html_temp[0]+'"'+selected+' >'+html_temp[1]+'</option>'
                    );
                    
                }
            }
        },
        async: false
    });
}

/**
 * Fonction de récupération des paramètres GET de la page
 * @return Array Tableau associatif contenant les paramètres GET
 */
function extractUrlParams(){    
    var t = location.search.substring(1).split('&');
    var f = [];
    for (var i=0; i<t.length; i++){
        var x = t[ i ].split('=');
        f[x[0]]=x[1];
    }
    return f;
}

// vuploadMulti - XXX
function vuploadMulti(champ) {
    //
    if (fenetreouverte == true) {
        pfenetre.close();
    }
    //
    pfenetre = window.open("../spg/upload.php?origine="+champ+"&form="+$('input[name='+champ+']').closest('form').attr('name'),"upload","width=400,height=300,top=120,left=120");
    //pfenetre = window.open("../spg/upload2.php?origine="+champ,"upload2","width=300,height=100,top=120,left=120");
    //
    fenetreouverte = true;
}

// Cette fonction permet de gérer la validation de l'action 
// affichage_reglementaire_registre de la classe demande
function affichage_reglementaire_registre(button) {
    // Popup de confirmation du traitement par l'utilisateur
    if (trt_confirm() == false) {
        return false;
    }
    // Suppression du bouton pour que l'utilisateur ne puisse pas cliquer une
    // deuxième fois
    $(button).remove();
    // Affichage du spinner
    $("#msg").html(msg_loading);
    // Requête AJAX vers le fichier l'action affichage_reglementaire_registre
    // pour mettre à jour les dossiers
    // XXX layout
    $.ajax({
        type: "GET",
        url: "../scr/form.php?obj=demande_affichage_reglementaire_registre&action=110&idx=0&update",
        cache: false,
        success: function(html){
            // Ajout d'un bloc de message vide
            $('#msg').html(
                '<div class="message ui-widget ui-corner-all ui-state-highlight">'+
                    '<p>'+
                        '<span class="ui-icon ui-icon-info"></span>'+
                        '<span class="text">'+
                        '</span>'+
                    '</p>'+
                '</div>'
            );
            // Si le retour de l'appel Ajax n'est pas vide, alors il y a eu une
            // lors du traitement
            if ( html.length > 2 ) {
                $("#msg .message").addClass("ui-state-error");
                $("#msg .text").html(html);
            } else {
                // Sinon message de succès et appel de l'édition
                $("#msg .message").addClass("ui-state-valid");
                $("#msg .text").html("Traitement terminé. Le registre a été téléchargé.");
                window.open("../scr/form.php?obj=demande_affichage_reglementaire_registre&action=111&idx=0");
            }
        },
        async: false
    });
    //
    return false;
}

/**
 * WIDGET DASHBOARD - widget_recherche_dossier.
 *
 * Fonction de redirection pour le widget de recherche de dossier
 */
function widget_recherche_dossier(data, nbRes, obj) {
    // S'il n'y a qu'un seul résultat, afficher un résumé
    if (nbRes == 1) {
        window.location = "../scr/form.php?obj=" + obj + "&action=3&" +
            "idx=" + data + "&premier=0&advs_id=&recherche=&tricol=&" +
            "selectioncol=&valide=&retour=tab";
    }
    // S'il y a une liste de dossier, redirection vers le tableau
    else {
        window.location = "../app/web_entry.php?obj=" + obj + "&field=dossier&value=" + data;
    }
}

/**
 * Retour spécifique de l'écran de consultation multiple - surcharge de ajaxIt
 * @todo XXX voir les différences avec ajaxIt et si il n'est pas possible
 * d'effectuer  la modification dans le core
 */
function messageIt(objsf, link, empty) {
    // recuperation du terme recherche
    var recherche = document.getElementById("recherchedyn");
    if (recherche != null) {
        link += "&recherche="+recherche.value;
    }else {
        link += "&recherche=";
    }
    // execution de la requete en GET
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            (empty == true )?$("#sousform-"+objsf).empty():'';
            $("#sousform-"+objsf).append(html);
            om_initialize_content();
        },
        async: false
    });
}

/**
 * TTélécharegement de fichier pdf en ajax
 * spécifique de l'écran de consultation multiple 
 */
/* Plugin jQuery qui lance un espèce d'appel AJAX vers un script PHP de téléchargement de fichier*/
jQuery.download = function(url, data, method){
    //url and data options required
    if( url && data ){ 
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : jQuery.param(data);
        //split params into form inputs
        var inputs = '';
        jQuery.each(data.split('&'), function(){ 
            var pair = this.split('=');
            inputs+='<input type="hidden" name="'+ pair[0] +'" value="'+ pair[1] +'" />'; 
        });
        //send request
        jQuery('<form action="'+ url +'" method="'+ (method||'post') +'">'+inputs+'</form>')
        .prependTo('body').submit().remove();
    };
};

/**
 * Cette fonction permet de charger dans un dialog jqueryui un formulaire tel
 * qu'il aurait été chargé avec ajaxIt
 * 
 * @param objsf string : objet de sousformulaire
 * @param link string : lien vers un sousformulaire (../scr/sousform.php...)
 * @param width integer: width en px
 * @param height integer: height en px
 * @param callback function (optionel) : nom de la méthode à appeler
 *                                       à la fermeture du dialog
 * @param callbackParams mixed (optionel) : paramètre à traiter dans la function
 *                                          callback 
 *
 **/
function popupIt(objsf, link, width, height, callback, callbackParams) {
    // Insertion du conteneur du dialog
    var dialog = $('<div id=\"sousform-'+objsf+'\"></div>').insertAfter('#tabs-1 .formControls');
    $('<input type=\"text\" name=\"recherchedyn\" id=\"recherchedyn\" value=\"\" class=\"champFormulaire\" style=\"display:none\" />').insertAfter('#sousform-'+objsf);
    // execution de la requete passee en parametre
    // (idem ajaxIt + callback)
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            //Suppression d'un precedent dialog
            dialog.empty();
            //Ajout du contenu recupere
            dialog.append(html);
            //Initialisation du theme OM
            om_initialize_content();
            //Creation du dialog
            $(dialog).dialog({
            //OnClose suppression du contenu
            close: function(ev, ui) {
                // Si le formulaire est submit et valide on execute la méthode
                // passée en paramètre
                if (typeof(callback) === "function") {
                    callback(callbackParams);
                }
                $(this).remove();
            },
            resizable: true,
            modal: true,
            width: width,
            height: height,
            position: 'left top',
          });
        },
        async : false
    });
    //Fermeture du dialog lors d'un clic sur le bouton retour
    $('#sousform-'+objsf).off("click").on("click",'a.retour',function() {
        $(dialog).dialog('close').remove();
        return false;
    });
}

/***
 * Fonction getter des paramètres de l'url courante
 */
// Parse URL Queries Method
(function($){
    $.getQuery = function( query ) {
        query = query.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var expr = "[\\?&]"+query+"=([^&#]*)";
        var regex = new RegExp( expr );
        var results = regex.exec( window.location.href );
        if( results !== null ) {
            return results[1];
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        } else {
            return false;
        }
    };
})(jQuery);


/*
 * Javascript concernant la demande d'avis
 * 
 * 
 */

/**
 * Fonction de callback appellée lors de la fermeture du dialog (popupit)
 * du retour d'avis d'un service
 **/
function returnToTab(objsf) {
    var valid=$('#sousform-'+objsf+' div.ui-state-valid');
    if(valid.length > 0) {
        document.location.href="../scr/tab.php?obj="+$.getQuery('obj')+"&premier="+$.getQuery('premier')
        +"&advs_id="+$.getQuery('advs_id')+"&recherche="+$.getQuery('recherche')+"&tricol="+$.getQuery('tricol')
        +"&selectioncol="+$.getQuery('selectioncol');
    }
};


/*
 * Javascript concernant la demande
 * 
 * 
 */

 /**
  * Fonction permettant de mettre à jour les infos du demandeur
  **/
function getDemandeurId(type) {
    var id_demandeur=$('#id_retour').val();
    if($.isNumeric(id_demandeur)) {
        afficherDemandeur(id_demandeur,type);
        om_initialize_content();
    }
}

/**
 * Fonction permettant d'afficher la synthèse d'un demandeur
 */
function afficherDemandeur(id,type) {
    $.ajax({
        type: "GET",
        url: '../app/afficher_synthese_demandeur.view.php?iddemandeur='+id+'&type='+type,
        cache: false,
        success: function(html){
            $(html).insertBefore('#add_'+type).fadeIn(500);

        },
        async:false
    });
    affichageBoutonsDemandeurs();
}

/**
 * Fonction permettant de modifier un demandeur
 */
function editDemandeur(obj,id,type,id_css) {
    var url = '../scr/sousform.php?obj='+obj+'&retourformulaire=demande';
    //Vérification des contraintes de récupération des demandeurs
    if(getDemandeInfo('contraintes') == 'avec_recup') {
        url += '&action=0&idx_demandeur='+id;
    } else {
        url += '&action=1&idx='+id;
    }
    popupIt(obj, url, 860, 'auto',
            replaceDemandeur, {'type':type,'id': id, 'id_css':id_css});
    affichageBoutonsDemandeurs();
}

/**
 * Function permettant de remplacer un contenu déjà existant
 **/
function replaceDemandeur(obj) {
    var new_demandeur=$('#id_retour').val();
    if($.isNumeric(new_demandeur)) {
        $.ajax({
            type: "GET",
            url: '../app/afficher_synthese_demandeur.view.php?iddemandeur='+new_demandeur+'&type='+obj.type,
            cache: false,
            success: function(html){
                $(obj.id_css).replaceWith(html);
            }
        });
    }
}
/**
 * Function permettant de remplacer un contenu déjà existant
 **/
function removeDemandeur(id) {
    var div_class=$('#'+id).attr("class");
    $('#'+id).remove();
    if(div_class == "delegataire") {
        $('#add_delegataire').fadeIn(500);
    }
    affichageBoutonsDemandeurs();
}

/**
 * Fonction permettant d'afficher et cacher les boutons d'ajout de demandeurs
 */
function affichageBoutonsDemandeurs(){
    // Si le fieldset Demandeurs n'est pas présent, on sort de la fonction
    if($('#liste_demandeur').size() === 0) {
        return false;
    }
    // Affichage des blocs en fonction du type de demande
    type_aff_form = getDemandeInfo('type_aff_form');
    switch (type_aff_form) {
        case 'ADS':
            // Suppression des types de demandeurs d'autres type de DA
            $('.plaignant_principal').each(function() {
                $(this).remove();
            });
            $('.plaignant').each(function() {
                $(this).remove();
            });
            $('.contrevenant_principal').each(function() {
                $(this).remove();
            });
            $('.contrevenant').each(function() {
                $(this).remove();
            });
            $('.requerant').each(function() {
                $(this).remove();
            });
            $('.avocat').each(function() {
                $(this).remove();
            });
            $('.bailleur_principal').each(function() {
                $(this).remove();
            });
            $('.bailleur').each(function() {
                $(this).remove();
            });
            // Affichage ou non des blocs
            $('#plaignant_contrevenant').hide();
            $('#requerant_avocat').hide();
            $('#petitionnaire_principal_delegataire_bailleur').hide();
            $('#petitionnaire_principal_delegataire').fadeIn(500);
            break;
            
        case 'CTX RE':
            // Suppression des types de demandeurs d'autres type de DA
            $('.plaignant_principal').each(function() {
                $(this).remove();
            });
            $('.plaignant').each(function() {
                $(this).remove();
            });
            $('.contrevenant_principal').each(function() {
                $(this).remove();
            });
            $('.contrevenant').each(function() {
                $(this).remove();
            });
            $('.bailleur_principal').each(function() {
                $(this).remove();
            });
            $('.bailleur').each(function() {
                $(this).remove();
            });
            // Affichage ou non des blocs
            $('#plaignant_contrevenant').hide();
            $('#requerant_avocat').hide();
            $('#petitionnaire_principal_delegataire_bailleur').hide();
            $('#petitionnaire_principal_delegataire').fadeIn(500);
            // Affichage du bloc requerant/avocat
            if ($('input[name^=petitionnaire_principal]').size() > 0) {
                $('#requerant_avocat').fadeIn(500);
            } else {
                $('#requerant_avocat').hide();
            }
            break;
            
        case 'CTX IN':
            $('.petitionnaire_principal').each(function() {
                $(this).remove();
            });
            $('.delegataire').each(function() {
                $(this).remove();
            });
            $('.petitionnaire').each(function() {
                $(this).remove();
            });
            $('.requerant').each(function() {
                $(this).remove();
            });
            $('.avocat').each(function() {
                $(this).remove();
            });
            $('.bailleur_principal').each(function() {
                $(this).remove();
            });
            $('.bailleur').each(function() {
                $(this).remove();
            });
            $('#requerant_avocat').hide();
            $('#petitionnaire_principal_delegataire').hide(); 
            $('#listePetitionnaires').hide();
            $('#petitionnaire_principal_delegataire_bailleur').hide();
            $('#plaignant_contrevenant').fadeIn(500);
            $('#listeContrevenants').fadeIn(500);
            break;

        case 'DPC':
            // Suppression des types de demandeurs d'autres type de DA
            $('.plaignant_principal').each(function() {
                $(this).remove();
            });
            $('.plaignant').each(function() {
                $(this).remove();
            });
            $('.contrevenant_principal').each(function() {
                $(this).remove();
            });
            $('.contrevenant').each(function() {
                $(this).remove();
            });
            $('.requerant').each(function() {
                $(this).remove();
            });
            $('.avocat').each(function() {
                $(this).remove();
            });
            // Affichage ou non des blocs
            $('#plaignant_contrevenant').hide();
            $('#requerant_avocat').hide();
            $('#petitionnaire_principal_delegataire').fadeIn(500);
            $('#petitionnaire_principal_delegataire_bailleur').fadeIn(500);
            $('#listeBailleurs').fadeIn(500);
            break;
        default:
            
    }

    // Si formulaire après validation on cache les boutons d'ajout de demandeurs
    url = document.location + "" ;
    if ($('#liste_demandeur').size() > 0
        && $("form[name=f1] .form-is-valid").size() > 0 ) {

        $('#add_petitionnaire_principal').hide();
        $('#add_delegataire').hide();
        if($('input[name=delegataire][type=hidden]').size() == 0) {
            $('#delegataire').hide();
        }
        $('#add_petitionnaire').hide();
        $('#add_plaignant').hide();
        $('#add_contrevenant_principal').hide();
        $('#add_contrevenant').hide();
        if($('input[name=contrevenant][type=hidden]').size() == 0) {
            $('#contrevenant').hide();
        }
        $('#add_requerant').hide();
        $('#add_avocat').hide();
        $('#add_bailleur_principal').hide();
        $('#add_bailleur').hide();
        if($('input[name=bailleur][type=hidden]').size() == 0) {
            $('#bailleur').hide();
        }

    } else if ($('form[name=f1] #liste_demandeur').size() > 0) {
        // Affichage du bouton d'ajout du petitionnaire principal
        if($('input[name^=petitionnaire_principal]').size() > 0) {
            $('#add_petitionnaire_principal').hide();
        } else {
            $('#add_petitionnaire_principal').fadeIn(500);
        }
        // Affichage du bloc delegataire
        if($('input[name^=petitionnaire_principal]').size() == 0) {
            $('#delegataire').hide();
        } else {
            $('#delegataire').fadeIn(500);
        }
        // Affichage du bouton délégataire
        if($('input[name^=petitionnaire_principal]').size() == 0 ||
            $('input[name^=delegataire]').size() > 0) {
            $('#add_delegataire').hide();
        } else {
            $('#add_delegataire').fadeIn(500);
        }
        // Affichage du bloc petitionnaire
        if($('input[name^=petitionnaire_principal]').size() == 0) {
            $('#listePetitionnaires').hide();
        } else {
            $('#listePetitionnaires').fadeIn(500);
        }
        // Affichage du bouton petitionnaire
        if($('input[name^=petitionnaire_principal]').size() == 0) {
            $('#add_petitionnaire').hide();
        } else {
            $('#add_petitionnaire').fadeIn(500);
        }
        // Affichage du bloc petitionnaire
        if($('input[name^=petitionnaire_principal]').size() == 0) {
            $('#listePetitionnaires').hide();
        } else {
            $('#listePetitionnaires').fadeIn(500);
        }
        if($('input[name^=requerant_principal]').size() == 0) {
            $('#listeAutresRequerants').hide();
        } else {
            $('#listeAutresRequerants').fadeIn(500);
        }
        if($('input[name^=avocat_principal]').size() == 0) {
            $('#listeAutresAvocats').hide();
        } else {
            $('#listeAutresAvocats').fadeIn(500);
        }

        if ($('input[name^=petitionnaire_principal]').size() == 0 ||
            $('input[name^=requerant_principal]').size() > 0) {
            $('#add_requerant_principal').hide();
            $('#add_requerant').fadeIn(500);
        } else {
            $('#add_requerant_principal').fadeIn(500);
            $('#add_requerant').hide();
        }
        
        if ($('input[name^=petitionnaire_principal]').size() == 0 ||
            $('input[name^=avocat_principal]').size() > 0) {
            $('#add_avocat_principal').hide();
            $('#add_avocat').fadeIn(500);
        } else {
            $('#add_avocat_principal').fadeIn(500);
            $('#add_avocat').hide();
        }

        // Formulaire CTX IN
        // Affichage du bouton d'ajout du contrevenant principal
        if($('input[name^=contrevenant_principal]').size() > 0) {
            $('#add_contrevenant_principal').hide();
        } else {
            $('#add_contrevenant_principal').fadeIn(500);
        }
        // Affichage du bouton contrevenant
        if($('input[name^=contrevenant_principal]').size() == 0) {
            $('#listeAutresContrevenants').hide();
            $('#add_contrevenant').hide();
        } else {
            $('#listeAutresContrevenants').fadeIn(500);
            $('#add_contrevenant').fadeIn(500);
        }
        // Affichage du bloc Plaignants
        if($('input[name^=contrevenant_principal]').size() == 0) {
            $('#listePlaignants').hide();
        } else {
            $('#listePlaignants').fadeIn(500);
        }
        if($('input[name^=plaignant_principal]').size() == 0) {
            $('#listeAutresPlaignants').hide();
        } else {
            $('#listeAutresPlaignants').fadeIn(500);
        }
        if ($('input[name^=contrevenant_principal]').size() == 0 ||
            $('input[name^=plaignant_principal]').size() > 0) {
            $('#add_plaignant_principal').hide();
            $('#add_plaignant').fadeIn(500);
        } else {
            $('#add_plaignant_principal').fadeIn(500);
            $('#add_plaignant').hide();
        }

        // Formulaire DPC
        if($('input[name^=bailleur_principal]').size() > 0) {
            $('#add_bailleur_principal').hide();
        } else {
            $('#add_bailleur_principal').fadeIn(500);
        }
        if($('input[name^=bailleur_principal]').size() == 0) {
            $('#listeAutresBailleurs').hide();
            $('#add_bailleur').hide();
        } else {
            $('#listeAutresBailleurs').fadeIn(500);
            $('#add_bailleur').fadeIn(500);
        }
    }
}

/**
 * Appel au chargement de la page
 **/
$(function() {
    if ( $('#type_demandeur') == 'petitionnaire' || 
        $('#type_demandeur') == 'avocat' || 
        $('#type_demandeur') == 'bailleur'){
        addSearchIcon();
        addDivDialog('.bloc_demandeur');
    }
    affichageBoutonsDemandeurs();
    // Bind de la fonction permettant l'ajout du pétitionnaire principal
    $("#formulaire").on("click","#add_petitionnaire_principal",  function() {
        popupIt('petitionnaire',
                '../scr/sousform.php?obj=petitionnaire&action=0'+
                '&retourformulaire=demande&principal=true', 860, 'auto',
                getDemandeurId, 'petitionnaire_principal');
    });
    // Bind de la fonction permettant l'ajout du plaignant
    $("#formulaire").on("click","#add_plaignant_principal",  function() {
        popupIt('plaignant',
                '../scr/sousform.php?obj=plaignant&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'plaignant_principal');
    });
    // Bind de la fonction permettant l'ajout du plaignant
    $("#formulaire").on("click","#add_plaignant",  function() {
        popupIt('plaignant',
                '../scr/sousform.php?obj=plaignant&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'plaignant');
    });
    // Bind de la fonction permettant l'ajout du contrevenant principal
    $("#formulaire").on("click","#add_contrevenant_principal",  function() {
        popupIt('contrevenant',
                '../scr/sousform.php?obj=contrevenant&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'contrevenant_principal');
    });
    // Bind de la fonction permettant l'ajout du contrevenant
    $("#formulaire").on("click","#add_contrevenant",  function() {
        popupIt('contrevenant',
                '../scr/sousform.php?obj=contrevenant&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'contrevenant');
    });
    // Bind de la fonction permettant l'ajout du requerant
    $("#formulaire").on("click","#add_requerant_principal",  function() {
        popupIt('requerant',
                '../scr/sousform.php?obj=requerant&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'requerant_principal');
    });
    // Bind de la fonction permettant l'ajout du requerant
    $("#formulaire").on("click","#add_requerant",  function() {
        popupIt('requerant',
                '../scr/sousform.php?obj=requerant&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'requerant');
    });
    // Bind de la fonction permettant l'ajout de l'avocat
    $("#formulaire").on("click","#add_avocat_principal",  function() {
        popupIt('avocat',
                '../scr/sousform.php?obj=avocat&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'avocat_principal');
    });
    // Bind de la fonction permettant l'ajout de l'avocat
    $("#formulaire").on("click","#add_avocat",  function() {
        popupIt('avocat',
                '../scr/sousform.php?obj=avocat&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'avocat');
    });
    // Bind de la fonction permettant l'ajout du délégataire
    $("#formulaire").on("click","#add_delegataire", function(event) {
        popupIt('delegataire',
                '../scr/sousform.php?obj=delegataire&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'delegataire');
    });
    // Bind de la fonction permettant l'ajout des pétitionnaires
    $("#formulaire").on("click","#add_petitionnaire", function(event) {
        popupIt('petitionnaire',
                '../scr/sousform.php?obj=petitionnaire&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'petitionnaire');
    });
    // Bind de la fonction permettant l'ajout du bailleur principal
    $("#formulaire").on("click","#add_bailleur_principal",  function() {
        popupIt('bailleur',
                '../scr/sousform.php?obj=bailleur&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'bailleur_principal');
    });
    // Bind de la fonction permettant l'ajout des bailleurs
    $("#formulaire").on("click","#add_bailleur",  function() {
        popupIt('bailleur',
                '../scr/sousform.php?obj=bailleur&action=0'+
                '&retourformulaire=demande', 860, 'auto',
                getDemandeurId, 'bailleur');
    });

    url = document.location + "" ;
    if ((
        url.indexOf("form.php?obj=demande&") != -1 ||
        url.indexOf("form.php?obj=dossier&") != -1 ||
        url.indexOf("form.php?obj=dossier_instruction&") != -1 ||
        url.indexOf("form.php?obj=dossier_instruction_mes_encours&") != -1 ||
        url.indexOf("form.php?obj=dossier_instruction_tous_encours&") != -1 ||
        url.indexOf("form.php?obj=dossier_instruction_mes_clotures&") != -1 ||
        url.indexOf("form.php?obj=dossier_instruction_tous_clotures&") != -1 ||
        url.indexOf("form.php?obj=dossier_contentieux_mes_infractions&") != -1 ||
        url.indexOf("form.php?obj=dossier_contentieux_toutes_infractions&") != -1 ||
        url.indexOf("form.php?obj=dossier_contentieux_mes_recours&") != -1 ||
        url.indexOf("form.php?obj=dossier_contentieux_tous_recours&") != -1
        )
        && url.indexOf("&action=3") == -1) {

        formatFieldReferenceCadastrale();
    }

    // S'il y a une erreur durant la validation lors de l'ajout
    // d'une nouvelle demande
    if ((url.indexOf("form.php?obj=demande_nouveau_dossier&") != -1
        || url.indexOf("form.php?obj=demande_nouveau_dossier_contentieux&") != -1)
        && (url.indexOf("&action=3") == -1
        && url.indexOf("&action=2") == -1
        && url.indexOf("&validation") != -1
        && $(".form-is-valid").size() == 0)) {

        addButtonCadastraleAdesse();
        formatFieldReferenceCadastrale();
        manage_display_demande($('#dossier_autorisation_type_detaille').val());
    }
    
    if ((url.indexOf("form.php?obj=demande_nouveau_dossier&") != -1
        || url.indexOf("form.php?obj=demande_nouveau_dossier_contentieux&") != -1)
        && (url.indexOf("&action=3") == -1
        && url.indexOf("&action=2") == -1
        && url.indexOf("&validation") == -1
        && $(".form-is-valid").size() == 0)) {
        
        addButtonCadastraleAdesse();
        changeDemandeType();
    }
    
    // Ajout de demande
    if ((url.indexOf("form.php?obj=demande_nouveau_dossier&") != -1
        || url.indexOf("form.php?obj=demande_nouveau_dossier_contentieux&") != -1
        || url.indexOf("form.php?obj=demande_dossier_encours&") != -1
        || url.indexOf("form.php?obj=demande_autre_dossier&") != -1)
        && url.indexOf("&action=0") != -1 ){

        /*Cache les champs avant que dossier_autorisation_type_detaille soit choisi*/
        hideFields();
    }
    
    // Ajout de demande sur dossier existant
    if ((url.indexOf("form.php?obj=demande_dossier_encours&") != -1
        || url.indexOf("form.php?obj=demande_autre_dossier&") != -1)
        && url.indexOf("&action=0") != -1 ){

        //Cache les champs avant que dossier_autorisation_type_detaille soit choisi
        hideFields();
        //On affiche le select du type de la demande
        $('#demande_type').parent().parent().show();
    }
    
    // Modification de demande
    if (
        (
            url.indexOf("form.php?obj=demande&") != -1
            || url.indexOf("form.php?obj=demande_nouveau_dossier&") != -1
            || url.indexOf("form.php?obj=demande_nouveau_dossier_contentieux&") != -1
            || url.indexOf("form.php?obj=demande_dossier_encours&") != -1
            || url.indexOf("form.php?obj=demande_autre_dossier&") != -1
        )
        && url.indexOf("&action") == -1 
    ){

        formatFieldReferenceCadastrale();
    }

    
    if (url.indexOf("form.php?obj=commission&") != -1
        && (url.indexOf("&action=0") != -1
        || url.indexOf("&action=1") != -1)
        && url.indexOf("&validation") == -1
        && $(".form-is-valid").size() == 0) {
        changeCommissionType();    
    }
    
});

/*
 * Action sur les champs pour les références cadastrales
 */
function formatFieldReferenceCadastrale(){

    addNewFieldReferencesCadastrales();
    $('#terrain_references_cadastrales').parent().parent().hide();
    
    url = document.location + "";

    reference_cadastrale = $('#terrain_references_cadastrales').val();
    /*Formatage de la reference cadastrale*/
    if ( reference_cadastrale != '' ){
        
        /* Récupère la référence cadastrale non formatée */
        references_cadastrales = reference_cadastrale.split(';');
        donnees = new Array();
        
        i = 0 ;
        /* Boucle sur les références, elles étaient séparées par un ; */
        for ( l = 0 ; l < references_cadastrales.length - 1 ; l ++ ){
            
            /*Récupère le code impots du qartier [CHIFFRES]*/
            k = 0;
            donnees[i] = '';
            for ( j = k ; j < references_cadastrales[l].length ; j++ ){
                
                if ( references_cadastrales[l].charAt(j) >= 0 && 
                    references_cadastrales[l].charAt(j) <= 9 ){
                        
                    donnees[i] += references_cadastrales[l].charAt(j);
                    k++;
                    
                } else {
                    
                    i++;
                    break;
                }
            }
            
            /* Récupère la section [LETTRES] */     
            donnees[i] = '';
            for ( j = k ; j < references_cadastrales[l].length ; j++ )
                if ( isAlpha(references_cadastrales[l].charAt(j)) ){
                    
                    donnees[i] += references_cadastrales[l].charAt(j);
                    k++;
                    
                } else {
                    
                    i++;
                    break;
                }
            
            /* Récupère la parcelle [CHIFFRES] */
            donnees[i] = '';
            for ( j = k ; j < references_cadastrales[l].length ; j++ )
                if ( references_cadastrales[l].charAt(j) >= 0 && 
                    references_cadastrales[l].charAt(j) <= 9 ){
                        
                    donnees[i] += references_cadastrales[l].charAt(j);
                    k++;
                    
                } else {
                    
                    break;
                }
            
            /* Récupère les séparateurs [ A / ] et les sections */
            m = 0 ; // Nombre de suffixe
            if ( k < references_cadastrales[l].length ){
                
                for ( j = k ; j < references_cadastrales[l].length ; j++ )
                    if ( isAlpha(references_cadastrales[l].charAt(j)) ){
                        
                        m++;
                        donnees[++i] = references_cadastrales[l].charAt(j);
                        donnees[++i] = '';
                    }
                    else {
                        
                        donnees[i] += references_cadastrales[l].charAt(j);
                    }
            }
            
            /*Créé autant de champs de que de référence */
            donnees[++i] = ';';
            i++;

            if ( l > 0 ) {
                
                $('.reference_cadastrale_custom_fields').append( 
                    "<br/>" + fieldReferenceCadastraleBase()
                );
            }
            
            actionFormReferenceCadastrale();
            
            if ( m > 0 ){
                        
                for ( j = 0 ; j < m ; j++ ){
                    
                    $('#moreFieldReferenceCadastrale' + 
                        ( $('.moreFieldReferenceCadastrale').length - 1 ) ).
                    before(
                        newInputReferenceCadastrale()
                    );
                }
            }
        }
         /* Action sur les boutons [+ ajouter d'autres champs] et [+ ajouter 
          * d'autres lignes] */
        actionLineFormReferenceCadastrale();
        
        /* Ajoute les données dans les champs nouvellement créés */
        $('.reference_cadastrale_custom_field').each(
            function(index) {
                
                $(this).val(donnees[index])
            }
        );
    }
    else{
        actionFormReferenceCadastrale();
        actionLineFormReferenceCadastrale();
    }
}

/**
 * Ajoute les icônes pour la recherche de pétitionnaire fréquent.
 */
function addSearchIcon(){

    $("#form-content:not(.form-is-valid) .search_fields").each(
        function() {

            // N'ajoute pas l'icône de recherche sur la collectivité
            if ($(this).find("#om_collectivite").length <= 0) {

                //Ajout de l'icône après le champs dénomination et nom de la personne morale*/
                $(this).append(
                    '<span '+
                        'class="om-icon om-icon-16 om-icon-fix search-frequent-16" '+ 
                        'title="Chercher un frequent"> '+
                    '</span>'
                );
            }
        }
    );
    
    /*Ajout des actions sur les boutons*/
    addActionSearchIcon();
    addActionRemove();

}

/**
 * Fonction permettant de revenir sur le formulaire d'ajout de demandeur
 **/
function addActionRemove(){
    $('.erase-petitionnaire').click(
        function(){
            ajaxIt('petitionnaire','../scr/sousform.php?obj=petitionnaire&action=0&retourformulaire=demande');
    });
    $('.erase-avocat').click(
        function(){
            ajaxIt('avocat','../scr/sousform.php?obj=avocat&action=0&retourformulaire=demande');
    });
    $('.erase-bailleur').click(
        function(){
            ajaxIt('bailleur','../scr/sousform.php?obj=bailleur&action=0&retourformulaire=demande');
    });
}

/**
 * Vérifie que les champs necessaires sont remplis et retourne les données necessaires
 * sous forme de tableau JSOn
 */
function getDataSearch(){

    // Récupèration de la valeur de la collectivité du dossier...
    var om_collectivite_value = $('#om_collectivite').val();
    // ... écrasée éventuellement par celle du demandeur
    if ($('#sousform-petitionnaire #om_collectivite').length) {
        om_collectivite_value = $('#sousform-petitionnaire #om_collectivite').val();
    }
    // Il faut un minimum de trois lettres pour lancer la recherche
    var minChar = false;
    // Récupération des données
    var dataJson = "{";
    $('.search_fields .champFormulaire').each(
        function(){
            if ( $(this).val().length >= 3 ){
                minChar = true;
            }
            
            var idInput = $(this).attr("id");
            var valInput = $(this).val();
            dataJson += "\"" + idInput + "\":\"" + valInput + "\",";
        }
    );

    // Si aucune collectivité est sélectionné
    if (om_collectivite_value == "") {
        //
        alert('Veuillez sélectionner une collectivité.');
        return "";
    }

    if (!minChar){
        alert('Saisissez au moins trois lettres pour la recherche');
        return "";
    }
    // Transformation de la chaîne de caractères en tableau
    dataJson = $.parseJSON(dataJson.substring(0,dataJson.length-1)+"}");
    // Mono : on définit la collectivité
    // Multi : on écrase la collectivité
    dataJson.om_collectivite = om_collectivite_value;
    // Retour des champs avec leur valeur
    return dataJson;
}

/*
 * Ajoute les actions sur les icônes de recherche
 */
function addActionSearchIcon(){
    
    //Selon l'objet dans lequel on se trouve
    var objName = '';
    var objReturn = '';
    if ( $('#sousform-petitionnaire').length == 1 ){
        objName = 'petitionnaire';
        objReturn = 'demande';
    }
    else if($('#architecte').length == 1){
        objName = 'architecte';
        objReturn = 'donnees_techniques';
    }
    else if($('#sousform-avocat').length == 1){
        objName = 'avocat';
        objReturn = 'demande';
    }
    else if($('#sousform-bailleur').length == 1){
        objName = 'bailleur';
        objReturn = 'demande';
    }
    $('.search-frequent-16').click(
        function(){
            //Récupération des données
            dataJson = getDataSearch();
            //Si ce n'est pas un tableau JSON on n'exécute pas le reste du code
            if ( typeof dataJson !== 'object' ){
                return;
            }
            
            //Requête qui va récupérer les données du addSearchIcon(es) 
            //pétitionnaire(s) correspondant(s) à la recherche
            $.ajax({
                type: "POST",
                dataType: "json",
                data: dataJson,
                url: "../app/find" + objName.charAt(0).toUpperCase() + objName.substring(1) + ".php" ,
                cache: false,
                success: function(obj){
                    var freq = obj;
                    var res='';
                    /*Si la recherche a donné des résultats*/
                    if ( freq.length > 0 ){
                        /*Limitation des résultats à 50 */
                        if ( freq.length > 50 ){
                            
                            nbRes = 50;
                            res += 'Votre recherche a donn&eacute; ' + freq.length 
                                + ' r&eacute;sultats. Seul les cinquantes premiers ' +
                                'seront affich&eacute;s.<br/>';
                        } else {
                            nbRes = freq.length;
                        }
                        
                        res += '<select id="select-'+objName+'">' ;
                        
                        /* Met les résultats de la recherche dans un select */
                       for ( i = 0 ; i < nbRes ; i++ ){
                            res += '<option value="' + freq[i].value + '">' + 
                                        freq[i].content +
                                   '</option>';
                        }
                        
                        res += '</select>';
                    } else {
                        res += 'Aucune correspondance trouvée.';
                    }
                    
                    addDivDialog('#sousform-' + objName);
                    /* Affichage de l'overlay */
                    $('#dialog').html(res);
                       
                    $( "#dialog" ).dialog({
                        dialogClass: "dialog-search-frequent-"+objName,
                        modal: true,
                        buttons : {
                            Valider: function(){
                                if ( res != 'Aucune correspondance trouvée.'){
                                    var id = $('#select-'+objName+' option:selected').val();
                                    if($.isNumeric(id)) {
                                        ajaxIt(objName,
                                        '../scr/sousform.php?obj=' + objName + 
                                        '&action=110&retourformulaire=' + objReturn + 
                                        '&idx='+id
                                        );
                                    }
                                }
                                // Fermeture de l'overlay
                                $(this).dialog( "close" );
                                $(this).remove();
                            }
                        },
                        close: function(){
                            $(this).remove();
                        }
                    });
                },
            });
        }
    );
}

/*
 * Ajoute un div pour l'overlay dialog de jQuery
 */
function addDivDialog(id){
    
    $(id).prepend('<div id="dialog"></div>');

}

/*
    Action au changement du select de la qualite du demandeur
 * */
function changeDemandeurType(id){

    /*Réinitialise les champs et cache les champs inutiles selon la qualité du demandeur*/
    /*Si la qualite du demandeur est particulier */
    if ( $('#' + id ).val() == 'particulier' ) {
        
        $('.personne_morale_fields input').each(
            function(){
                $(this).val('');
            }
        );
        $('.personne_morale_fields select option[value=""]').each(
            function(){
                $(this).attr('selected', 'selected');
            }
        );
        
        $('.personne_morale_fields').hide();
        $('.particulier_fields').show();
    }
    /*Si c'est une personne morale*/
    else if ( $('#' + id ).val() == 'personne_morale' ) {
        
       $('.particulier_fields input').each(
            function(){
                $(this).val('');
            }
        );
        $('.particulier_fields select option[value=""]').each(
            function(){
                $(this).attr('selected', 'selected');
            }
        );
        
        $('.particulier_fields').hide();
        $('.personne_morale_fields').show();
    }
}

/*
    Fonction de test des champs
 * */
function isAlpha(str) {
    return /^[a-zA-Z\/]+$/.test(str);
}

function isMail(str){
    return /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(str);
}

function isPhoneNumber(str){
    return /[0-9-()+]{3,20}/.test(str);
}

function testSeparator(obj){

    if ( obj.value != 'A' && 
         obj.value != '/' ) {
             
        alert('Saisissez uniquement un A ou un / comme séparateur');
        obj.value = '';
    }
}
/* Fin fonction test */

/*
 *  Ajoute les ations spécifiques pour le formulaire personnalisé d'ajout de 
 *  référence cadastrale
*/
function actionFormReferenceCadastrale(){

    if( $("form[name=f1] .form-is-valid").size() == 0) {

        $('#moreFieldReferenceCadastrale' + 
            ($('.moreFieldReferenceCadastrale').length - 1 )).
        on("click", function() {
            
            $(this).before(newInputReferenceCadastrale());
        });
    }
    
}

/*
 * Récupère les données saisies dans les champs de références cadastrales
 */
function getDataFieldReferenceCadastrale(){
    
    var reference_cadastrale = '';
    var reference_cadastrale_temp = '';
    var error = false;
    
    /*Pour chacun des champs du formulaire de saisie de référence cadastrale*/
    $('.reference_cadastrale_custom_field').each(
        function(){
            
            /*Si on est à la fin d'une ligne de champs*/
            if ( $(this).val() == ';' ){
                
                reference_cadastrale_bis = reference_cadastrale_temp ;
                
                /* Vérifie que les données sont correctement formatées avant de 
                 * les ajouter au résultat*/
                while( reference_cadastrale_bis != ''){
                    if ( /^([0-9]{1,4}[a-zA-Z]{1,3}[0-9]{1,5}([A\/]{1}[0-9]{1,4})*)*$/.test(reference_cadastrale_bis) ){
                        
                        reference_cadastrale += reference_cadastrale_bis + ";";
                        break;
                    }
                    else{
                        alert("Les références cadastrales saisies sont incorrectes. Veuillez les corriger.");
                        error = true;
                        return false;
                    }    
                }
                
                reference_cadastrale_temp = '';
            }
            
            else {
                
                /*Sinon, on récupère la valeur du champ*/
                reference_cadastrale_temp += $(this).val();
            }
        }
    );
    
    if(error) return false;
    /*Met la valeur du résultat dans le champs généré par le framework*/
    $('#terrain_references_cadastrales').val(reference_cadastrale);
    return true;
}

/*
    Action pour l'ajout de nouvelle ligne dans le formulaire d'ajout 
    de référence cadastrale
 * */
function actionLineFormReferenceCadastrale(){
    if( $("form[name=f1] .form-is-valid").size() == 0) {

        $('#morelineReferenceCadastrale').click( 
            function(){
                
                /*Ajout des trois champs de base*/
                $('.reference_cadastrale_custom_fields').
                append( "<br/>" + fieldReferenceCadastraleBase());

                /*Ajout du bind pour l'ajout de nouveaux champs*/
                $('#moreFieldReferenceCadastrale'+ 
                    ($('.moreFieldReferenceCadastrale').length - 1 )).
                on("click", function() {
                    $(this).before(newInputReferenceCadastrale());
                });
            }
        );
    }
}


/**
 * Gestion du type de commission et de l'affichage des champs du formulaire
 * lors de la sélection de la collectivité.
 *
 * @return void
 */
function changeCommissionType(){

    var idCollectivite = $("#om_collectivite").val();

    // Si l'identifiant fourni est valide
    if ($.isNumeric(idCollectivite)){
        
        // Met à jour la liste déroulante des type de commission
        // avec les données correspondantes
        filterSelect(idCollectivite, 'commission_type', 'om_collectivite', 'commission');
    }
}


/**
 * Gestion du type de la demande et de l'affichage des champs du formulaire
 * lors de la sélection du type de DA détaillé.
 *
 * @return void
 */
function changeDemandeType(){

    var idDossierAutorisationTypeDetaille = 
        $("#dossier_autorisation_type_detaille").val();
        
    // Récupère les données saisies dans les champs pour la référence cadastrale
    getDataFieldReferenceCadastrale();

    // Si l'identifiant fourni est valide
    if ( $.isNumeric(idDossierAutorisationTypeDetaille) ){
        
        // Met à jour la liste déroulante du type de demande
        // avec les données correspondantes
        filterSelect(idDossierAutorisationTypeDetaille, 'demande_type', 'dossier_autorisation_type_detaille', 'demande');
        // Affiche la liste déroulante du type de demande
        $('#demande_type').parent().parent().show();

        // S'il n'y a pas de type de demande
        // on affiche directement les champs d'après
        if ( $('#demande_type option').size() < 2 ) {
            showFormDemande();
        }
        // S'il n'y a qu'un seul type de demande
        // on le sélectionne et on le masque
        // puis on affiche les champs d'après
        if ( $('#demande_type option').size() === 2 ) {
            var idx_type_demande = $('#demande_type option').eq(1).val();
            $('#demande_type').val(idx_type_demande);
            $('#demande_type').parent().parent().hide();
            manage_display_demande(idDossierAutorisationTypeDetaille);
        }

        // Si aucune option n'est sélectionnée on masque les champs d'après
        if($('#demande_type').val() == '' ) {
            $('.demande_hidden_bloc').each(
                function(){
                    $(this).hide();
                }
            );
            $('input[type=submit]').hide();
        }
    }
    // Sinon on cache tous les champs du formulaire
    else {
        hideFields();
    }
}

/*
    Ajoute le code HTML des champs pour les références cadastrales
 * */
function addNewFieldReferencesCadastrales(){

    var html = '<div class="field field-type-text references_cadastrales_new_field" >'+
            '<div class="form-libelle">' +
                '<label '+
                    'class="libelle-terrain_references_cadastrales" '+
                    'for="terrain_references_cadastrales">'+
                    ' Références cadastrales '+
                '</label>' +
            '</div>' +
            '<div class="form-content reference_cadastrale_custom_fields">' +
                 fieldReferenceCadastraleBase() +
            '</div>' +
        '</div>';
    
    url = document.location + "";
    demandeInfo = getDemandeInfo('nature');
    if((demandeInfo == 'NOUV' || demandeInfo == 'NONE' ) && $(".form-is-valid").size() == 0 && url.indexOf('action=3') == -1 ) {
        html += '<div class="field field-type-text" id="morelineReferenceCadastrale">' +
            '<div class="form-libelle"></div>' +
            '<div class="form-content">' +
                '<span class="om-form-button add-16" title="Ajouter">ajouter d\'autres lignes</span>' +
            '</div>' +
       '</div>';
    }

    $('.references_cadastrales_new_field').remove();
    $('.moreFieldReferenceCadastrale').remove();
    $('#morelineReferenceCadastrale').remove();
    $('#terrain_references_cadastrales').parent().parent().before(
        html
    );
}

function addButtonCadastraleAdesse(){

    // Permet d'ajouter le bouton de récupération d'adresse si sig activé
    if ($('#option_sig').val() == 'sig_externe'){
    
        $('#terrain_references_cadastrales').parent().parent().after('<div class="field field-type-text" >' +
            '<div class="form-libelle"></div>' +
            '<div class="form-content buttonCadAdr">' +
                '<input id="cad-adr-them" '+
                    'class="ui-button ui-widget ui-state-default ui-corner-all" '+
                    'type="button" '+
                    'onclick="getAdressFromCadastrale();" '+
                    'value="Récupérer l\'adresse de la parcelle"/>' +
            '</div>' +
       '</div>' );
    }
}

/*
    Séparateur caché ; pour marqué la fin d'une ligne de référence cadastrale
 * */
function hiddenSeparatorField(){

    return '<input ' +
                'class="reference_cadastrale_custom_field" ' +
                'type="hidden" ' +
                'maxlength="2" ' +
                'size="2" ' +
                'value=";" />';
}

/**
 * Retourne l'info passée en paramètre parmi trois possibilités:
 * - la nature de la demande
 * - le type de demande est à avec_recup (de petitionnaire)
 * - le type d'affichage
 **/
function getDemandeInfo(info) {
    var id_demande_type = $('#demande_type').val();
    var res = "";
    if ( typeof id_demande_type !== "undefined" && id_demande_type !== ''){
        $.ajax({
            type: "GET",
            url: "../app/getDemandeInfo.php?iddemandetype=" + id_demande_type
                    + "&info=" + info,
            cache: false,
            async: false,
            success: function(val){
                res = val;
            }
        });
    }
    else {
        
        res = "NONE"
    }
    return res;
}
/*
    Ajout d'une nouvelle ligne de champ de référence cadastrale
    Retourne le code HTML
 * */
function fieldReferenceCadastraleBase(){
    
    url = document.location + "";
    
    var type = getDemandeInfo('nature');
    var reference_cadastrale = '<input ' +
                'class="champFormulaire reference_cadastrale_custom_field" ' +
                'type="text" ' +
                'onchange="VerifNum(this);str_pad_js(this, 3);" ' + 
                'maxlength="3" ' +
                'size="3" '+ 
                'placeholder="Quart." ';
                
    // désactivation des champs de référence cadastrale
    if( (type != 'NOUV' &&  type != 'NONE') || $(".form-is-valid").size() > 0 || url.indexOf('action=3') != -1 ) {
        reference_cadastrale += 'disabled="disabled" ';
    }
    
    reference_cadastrale += 'value="" />';

    reference_cadastrale += '<input ' +
                'class="champFormulaire reference_cadastrale_custom_field" ' +
                'type="text" ' +
                'maxlength="3" ' +
                'size="3" '+ 
                'placeholder="Sect." ';
    
    // désactivation des champs de référence cadastrale
    if((type != 'NOUV'&&  type != 'NONE') || $(".form-is-valid").size() > 0 || url.indexOf('action=3') != -1 ) {
        reference_cadastrale += 'disabled="disabled" ';
    }
    
    reference_cadastrale += 'value="" '+
                'onchange="if ( !isAlpha(this.value) && this.value != \'\' ){ alert(\'Vous ne devez saisir que des lettres dans ce champs.\'); this.value = \'\'; }; this.value=this.value.toUpperCase();"/>';
    reference_cadastrale += '<input ' +
                'class="champFormulaire reference_cadastrale_custom_field" ' +
                'type="text" ' +
                'onchange="VerifNum(this);str_pad_js(this, 4);" ' + 
                'maxlength="4" ' +
                'size="4" '+ 
                'placeholder="Parc." ';
    
    // désactivation des champs de référence cadastrale
    if((type != 'NOUV'&&  type != 'NONE') || $(".form-is-valid").size() > 0 || url.indexOf('action=3') != -1 ) {
        reference_cadastrale += 'disabled="disabled" ';
    }
    
    reference_cadastrale += 'value="" />';

    reference_cadastrale += '<span id="moreFieldReferenceCadastrale' +
        $('.moreFieldReferenceCadastrale').length +
        '" class="moreFieldReferenceCadastrale">' + hiddenSeparatorField();

    // Si form validé pas de bouton
    url = document.location + "";
    if( (type == 'NOUV' ||  type == 'NONE') && $("form[name=f1] .form-is-valid").size() == 0 && url.indexOf('action=3') == -1 ) {

        reference_cadastrale += 
            '<span class="om-form-button add-16" title="Ajouter">ajouter d\'autres champs</span>';
    }

    reference_cadastrale += '</span>';
    
    return reference_cadastrale;
}

/**
 * Cache les champs inutules [formulaire de demande]
 **/
function hideFields(){
    $('.demande_hidden_bloc').each(
        function(){
            $(this).hide();
        }
    );
    
    $('.field-type-text').hide();
    $('input[type=submit]').hide();
    $('#demande_type').parent().parent().hide();
}

/**
 * Gestion de la checklist des documents obligatoires, fonction appelée onchange
 * du champ demande_type.
 *
 * @param node demande_type champ demande_type
 */
function manage_document_checklist(demande_type) {
    // Appel de la vue correspondante à l'action 100 de l'objet demande_type
    $.getJSON( "../scr/form.php?obj=demande_type&action=100&idx="+$(demande_type).val(), function( data ) {
        if(data != false) {
            // Si l'action retourne une autre valeur que false on affiche la checklist
            show_document_checklist(data);
        }
    });
}

/**
 * Affichage et gestion du comportement de la checklist des documents obligatoires.
 *
 * @param {obj} data Objet correspondant au json retourné par l'action 100 de demande_type
 */
function show_document_checklist(data) {

    // Préparation du contenu du dialog
    var html_list = "<div id='liste_doc'><form><ul>";
    // Une checkbox par document obligatoire
    $.each(data.documents, function( key, value) {
        html_list += "<li><input type='checkbox' name='"+key+"'/> "+value+"</li>"
    });
    html_list += "</ul></form></div>";

    // Création de la fonction de vérification des checkbox cochées appelée lors
    // de la fermeture du dialog
    var check_doc = function(){
        // Si le nombre de checkbox cochées ne correspond pas au nombre de doc
        // fournis par l'action 100 de demande_type on affiche un message d'erreur
        // et bloque la fermeture du dialog
        if(data.documents.length != $("#liste_doc form input:checked").length) {
            alert(data.message_ko);
            return false;
        }
    }
    // Définition des boutons du dialog
    var dialog_buttons = {};
    dialog_buttons[data.libelle_cancel] = function(){
        // Si Rejet de la demande, affichage d'un message de confirmation et rechargement
        if(window.confirm(data.message_rejet)) {
            location.reload();
        }
    }
    dialog_buttons[data.libelle_ok] = function(){
        // Si validation du dialog : fermeture
        $(this).dialog('close');
    }
    // Insertion dans le dom du dialog non instancié
    $("html").append(html_list);
    // Instanciation du dialog
    $( "#liste_doc" ).dialog({
        title: data.title,
        resizable: false,
        modal: true,
        buttons: dialog_buttons,
        // Action avant fermeture : verification des checkbox cochées
        beforeClose: check_doc
    });
}
/*
    Affiche les champs dont on a besoin dans le formulaire de demande
 * */
function showFormDemande(){
    
    
    if($('#demande_type').val() != "") {

        $('.demande_hidden_bloc').each(
            function(){
                $(this).show();
            }
        );
        formatFieldReferenceCadastrale();
        $('.field-type-text').show();
        $('input[type=submit]').show();
        $('.terrain_references_cadastrales_custom').hide();
        
        $('#terrain_references_cadastrales').parent().parent().hide();

        //Vérification des contraintes de récupération des demandeurs si 
        // pas de récupération, on efface la liste
        var demandeInfo = getDemandeInfo('contraintes');
        if(demandeInfo == 'sans_recup') {
            if($('input[name^=petitionnaire_principal]').size() != -1) {
                removeDemandeur("petitionnaire_principal_" + $('input[name^=petitionnaire_principal]').val());
            }
            if($('input[name^=delegataire]').size() != -1) {
                removeDemandeur("delegataire_" + $('input[name^=delegataire]').val());
            }

            $('#listePetitionnaires input.demandeur_id').each(function(){
                if($(this).size() != -1) {

                    removeDemandeur("petitionnaire_" + $(this).val());
                }
            });
        } else if (demandeInfo == 'avec_recup' ) {
			//Récupération des demandeurs si la contrainte le permet
            $.ajax({
                type: "GET",
                url: "../app/getDemandeurList.php?dossier_autorisation=" + $('#dossier_autorisation').val(),
                cache: false,
                async: false,
                success: function(html){
                    $('#liste_demandeur').replaceWith(html);
                }
            });
        }
        affichageBoutonsDemandeurs();
    } else {
        
        /*Récupère les references cadastrales*/
        getDataFieldReferenceCadastrale();
        
        $('.demande_hidden_bloc').each(
            function(){
                $(this).hide();
            }
        );
        $('input[type=submit]').hide();

    }
}

function lookingForAutorisationContestee() {
    // Récupération de l'identifiant du dossier contesté sans espace.
    var idx = $('#autorisation_contestee').val().replace(/\s+/g,'');
    idx = $.trim(idx);
    $('#autorisation_contestee').val(idx);
    // Initialisation du bloc de message d'erreur.
    var error_message = 
    '<div ' +
        'class="message ui-widget ui-corner-all ui-state-highlight ui-state-error">' +
        '<p>' +
            '<span class="ui-icon ui-icon-info"></span>' +
            '<span class="text">' +
                '{0}' +
            '</span>' +
        '</p>' +
    '</div>';
    // On vide les demandeurs possiblement déjà présents dans le formulaire
    // puisqu'ils vont être récupérés depuis le dossier contesté.
    $('.petitionnaire_principal').each(function() {
        $(this).remove();
    });
    $('.delegataire').each(function() {
        $(this).remove();
    });
    $('.petitionnaire').each(function() {
        $(this).remove();
    });
    $('.avocat').each(function() {
        $(this).remove();
    });
    $('.requerent').each(function() {
        $(this).remove();
    });
    $('.plaignant').each(function() {
        $(this).remove();
    });
    $('.contrevenant').each(function() {
        $(this).remove();
    });
    $('.bailleur').each(function() {
        $(this).remove();
    });
                    
    if (idx != '') {
        $('#form-message').html(msg_loading);
        //
        $.ajax({
            type: "POST",
            url: "../scr/form.php?obj=dossier_instruction&action=220&idx="+idx+"&validation=1&contentonly=true",
            cache: false,
            data: "submit=plop&",
        }).done(function(json) {
            
            var infos_dossier = jQuery.parseJSON(json);
            if (infos_dossier.hasOwnProperty('error')) {
                $('#autorisation_contestee').val('');
                // Ajout du contenu récupéré (uniquement le bloc message)
                $('#form-message').html(error_message.format(infos_dossier.error));
                return false;
            }
            $('#autorisation_contestee_search_button').prop('disabled', true);
            $('#autorisation_contestee_search_button').button('option', 'disabled', true);
            $('#autorisation_contestee').prop('readonly', true);
            $('#form-message').html('');
            // Affichage des valeurs de formulaires
            $.each(infos_dossier, function( index, value ) {
                if ($('#'+index).length == 1) {
                    $('#'+index).val(value);
                }
            });
            // Affichage des demandeurs
            afficherDemandeur(infos_dossier.demandeurs.petitionnaire_principal, 'petitionnaire_principal');
            if (infos_dossier.demandeurs.hasOwnProperty("delegataire") == true) {
                afficherDemandeur(infos_dossier.demandeurs.delegataire, 'delegataire');
            }
            if (infos_dossier.demandeurs.hasOwnProperty("petitionnaire") == true) {
                $.each(infos_dossier.demandeurs.petitionnaire, function( type, id ) {
                    afficherDemandeur(id, 'petitionnaire');
                });
            }
            showFormDemande();
        });


    }
}

function eraseAutorisationContestee(){
    $('#autorisation_contestee_search_button').prop('disabled', false);
    $('#autorisation_contestee_search_button').button('option', 'disabled', false);
    $('#autorisation_contestee').prop('readonly', false);
    $('#autorisation_contestee').val('');
    hideFields();
}

/*
    Action au clique sur le bouton " + ajouter d'autres champs"
 * */
function newInputReferenceCadastrale(){
    
    // Champs désactivé si le formulaire a été validé et est valide
    var type = getDemandeInfo('nature');
    var reference_cadastrale_disabled = '';
    if((type != 'NOUV'&&  type != 'NONE') || $(".form-is-valid").size() > 0 || url.indexOf('action=3') != -1 ) {
        reference_cadastrale_disabled = 'disabled="disabled" ';
    }
    
    return '<input ' +
            'class="champFormulaire reference_cadastrale_custom_field" ' +
            'type="text" ' +
            'maxlength="1" ' +
            'size="3" ' +
            'value="" ' + 
            'placeholder="Sep." ' + 
            reference_cadastrale_disabled +
            'onchange="testSeparator(this);"/>' +
        '<input ' +
            'class="champFormulaire reference_cadastrale_custom_field" ' +
            'type="text" ' +
            'onchange="VerifNum(this);str_pad_js(this, 4)" ' + 
            'maxlength="4" ' +
            'size="4" ' +
            'placeholder="Parc." ' + 
            reference_cadastrale_disabled +
            'value="" />';
}

/**
 * Permet d'éventuellement lancer des scripts spécifiques à l'application.
 */
function app_initialize_content(tinymce_load) {
    // Supression des champs de recherche sousform pour les listing specifiques
    if ($('#sousform-document_numerise').exists()) {
        $('#recherche_onglet').html("");
    }
    if ($('#sousform-dossier_autorisation').exists()) {
        $('#recherche_onglet').html("");
    }
    if ($('#sousform-dossier_contrainte').exists()) {
        $('#recherche_onglet').html("");
    }
    // Si l'onglet consultatione existe
    if ($('#sousform-consultation').exists()) {
        // Si le champ de recherche de l'onglet à dans son attribut onkeyup
        // la chaîne de caractère 'retourformulaire=demande_avis'
        if ($('#recherche_onglet input#recherchedyn').exists()
            && $('#recherche_onglet input#recherchedyn').attr("onkeyup").match(/retourformulaire=demande_avis/g) !== null) {
            //
            $('#recherche_onglet').html("");
        }
    }
    changeDemandeurType('qualite');
    addSearchIcon();
    addDivDialog('.bloc_demandeur');
    // Interface de gestion de la commission
    commission_manage_interface();
    // Bind actions afficher les données technique depuis les lots
    $('a[id^=action-sousform-lot][id$=-donnees_techniques]').each(function(){
        $(this).bind_action_for_overlay("donnees_techniques");
    });

    // Bind des actions après le rafraichissement.
    initBindFocus();

    // Bind actions afficher les données technique depuis les lots
    $('a[id^=action-sousform-lot][id$=-donnees_techniques]').each(function(){
        $(this).bind_action_for_overlay("donnees_techniques");
    });

    // Fermeture overlay sur clic bouton retour
    $("#sousform-donnees_techniques a.retour").off('click').on('click', function(event) {
        $('#sousform-donnees_techniques').dialog('close').remove();
        return false;
    });
    $("#sousform-architecte a.retour").off('click').on('click', function(event) {
        $('#sousform-architecte').dialog('close').remove();
        return false;
    });
    $("#sousform-demande_avis_encours a.retour").off('click').on('click', function(event) {
        $('#sousform-demande_avis_encours').dialog('close').remove();
        return false;
    });
    $("#sousform-rapport_instruction a.retour").off('click').on('click', function(event) {
        $('#sousform-rapport_instruction').dialog('close').remove();
        return false;
    });

    var myObj = '';
    if ($('#sousform-lien_dossier_dossier').exists()) {
        myObj = 'lien_dossier_dossier';
    }
    if ($('#sousform-lien_dossier_dossier_contexte_ctx_re').exists()) {
        myObj = 'lien_dossier_dossier_contexte_ctx_re';
    }
    if ($('#sousform-lien_dossier_dossier_contexte_ctx_inf').exists()) {
        myObj = 'lien_dossier_dossier_contexte_ctx_inf';
    }
    // Onglet "Dossiers Liés" du DI
    if (myObj !== '' && $('#action-soustab-dossier_lies-corner-ajouter').exists()) {

        // Étant donné que la vue spécifique comporte trois tableaux
        // et que l'action "Ajouter" d'un tableau remplace uniquement
        // celui-ci par le formulaire, on redéfinit son comportement
        // en chargeant ce formulaire dans l'onglet entier.
        $('#action-soustab-dossier_lies-corner-ajouter').prop('onclick', null).off('click').on('click', function() {
            // On récupère le lien de l'action passé dans la foncion JS ajaxIt() appelée dans le onclick
            var on_click = $(this).attr('onclick');
            var reg = new RegExp(/^ajaxIt\('.*','(.*)'\);$/);
            var res = reg.exec(on_click);
            if (res !== null) {
                var link_action = res[1];
                ajaxIt(myObj, link_action);
            }
            return false;
        });
        // De même on affiche le message de validation des actions-direct
        // en entête de l'onglet au lieu du tableau concerné.
        $('#sousform-dossier_lies a.action-direct').prop('onclick', null).off('click').on('click', function() {
            // On récupère le lien de l'action passé dans la foncion JS ajaxIt() appelée dans le onclick
            var on_click = $(this).attr('onclick');
            var reg = new RegExp(/^ajaxIt\('.*','(.*)'\);$/);
            var res = reg.exec(on_click);
            if (res !== null) {
                var link_action = res[1] + '&validation=1&contentonly=true';
                var link_tab = $('#tab_dossier_lies_href').attr('data-href');
                var msg_container = '#sousform-' + myObj + ' > .soustab-message';
                var tab_container = '#sousform-dossier_lies .soustab-container';
                $(msg_container).html(msg_loading);
                //
                $.ajax({
                    type: "POST",
                    url: link_action,
                    cache: false,
                    success: function(html){
                        // Ajout du contenu récupéré (uniquement le bloc message)
                        $(msg_container).html($(html).find('div.message').get(0));
                        // Rafraichissement du tableau
                        
                        $.get(link_tab, function(html_content) {
                            $(tab_container).html(html_content);
                            om_initialize_content();
                        });
                    }
                });
            }
            return false;
        });
    }
}


function activate_data_href() {
    $('#sousform-href-disabled').attr('id', 'sousform-href');
}

jQuery.fn.exists = function(){return this.length>0;}

// Retourne la valeur d'une variable GET de l'URL
function getQuerystring(key, default_)
{
  if (default_==null) default_="";
  key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
  var qs = regex.exec(window.location.href);
  if(qs == null)
    return default_;
  else
    return qs[1];
}

// Récupère l'id de l'enregistrement lié au lot passé en parametre puis appel popupit
function overlayCerfa(idLot) {
    $.get("../app/displayLotCerfa.php?lot="+idLot, function(data) {
        if(data != '') {
                popupIt('donnees_techniques',
                        '../scr/sousform.php?obj=donnees_techniques&idx='+data+
                            '&retourformulaire=lot&maj=1&idxformulaire='+idLot,
                            950, 700);
                
            } else {
                // Si pas d'id retourner on créer un nouvel enregistrement de donnéees techniques
                popupIt('donnees_techniques',
                        '../scr/sousform.php?obj=donnees_techniques&retourformulaire=lot&maj=0&idxformulaire='+idLot,
                            950, 700);
            }

        });
}

/**
 * Fonction permettant de faire la somme des champs passé en 2nd parametre et
 * le stocker dans le champ passé en 1er
 */
function sommeChampsCerfa(cible, source) {
    val_cible = 0;
    
    $.each(source, function(index, value) {
        // Conversion des champs source en integer
        val_source = parseInt($('#'+value).val());
        // Cumul des valeurs des champs source
        val_cible = val_cible + (isNaN(val_source) ? 0 : val_source);
    });
    // Affectation de la nouvelle valeur
    $('#'+cible).val(val_cible);
}


/**
 * Calcul automatique des tableaux des surfaces.
 *
 * @param integer tab Numéro du tableau des surface (1 pour les destinations, 2
 * pour les sous-destinations).
 *
 * @return void
 */
function calculSurfaceTotal(tab) {

    // On vérifie que le paramètre fait référence à un des tableaux
    if (tab !== 1 && tab !== 2) {
        return;
    }

    // Préfixe des champs du tableau
    var prefix;
    // Nom des colonnes du tableau
    var nom_col;
    // Nombre de ligne du tableau
    var nb_ligne;

    // Tableau des destinations
    if (tab === 1) {
        //
        prefix = "su"
        //
        nom_col = [ "su_avt_shon",
                    "su_cstr_shon",
                    "su_chge_shon",
                    "su_demo_shon",
                    "su_sup_shon",
                    "su_tot_shon" ];
        //
        nb_ligne = 9;
    }

    // Tableau des sous-destinations
    if (tab === 2) {
        //
        prefix = "su2"
        //
        nom_col = [ "su2_avt_shon",
                    "su2_cstr_shon",
                    "su2_chge_shon",
                    "su2_demo_shon",
                    "su2_sup_shon",
                    "su2_tot_shon" ];
        //
        nb_ligne = 20;
    }

    // Calcul des totaux de colonnes
    $.each(nom_col, function(index, value) {
        var tot_col = 0;
        for ( var i = 1; i <= nb_ligne; i++ ) {
            // Conversion des champs source en numérique flotant
            val_source = parseFloat($('#'+value+i).val());
            val_source = parseFloat(val_source.toFixed(2));
            if (isNaN(val_source) == false) {
                $('#'+value+i).val(val_source);
            }
            // Cumul des valeurs des champs source
            tot_col = tot_col + (isNaN(val_source) ? 0 : val_source);
        }

        // Affectation du résultat de ligne
        $('#'+value+'_tot').val(tot_col);
    });

    // Calcul des totaux des lignes
    for ( var j = 1; j <= nb_ligne; j++ ) {
        // Conversion des champs source en numérique flotant
        su_avt_shon = parseFloat($('#'+prefix+'_avt_shon'+j).val());
        if (isNaN(su_avt_shon) == false) {
            su_avt_shon = parseFloat(su_avt_shon.toFixed(2));
            $('#'+prefix+'_avt_shon'+j).val(su_avt_shon);
        }
        su_cstr_shon = parseFloat($('#'+prefix+'_cstr_shon'+j).val());
        if (isNaN(su_cstr_shon) == false) {
            su_cstr_shon = parseFloat(su_cstr_shon.toFixed(2));
            $('#'+prefix+'_cstr_shon'+j).val(su_cstr_shon);
        }
        su_chge_shon = parseFloat($('#'+prefix+'_chge_shon'+j).val());
        if (isNaN(su_chge_shon) == false) {
            su_chge_shon = parseFloat(su_chge_shon.toFixed(2));
            $('#'+prefix+'_chge_shon'+j).val(su_chge_shon);
        }
        su_demo_shon = parseFloat($('#'+prefix+'_demo_shon'+j).val());
        if (isNaN(su_demo_shon) == false) {
            su_demo_shon = parseFloat(su_demo_shon.toFixed(2));
            $('#'+prefix+'_demo_shon'+j).val(su_demo_shon);
        }
        su_sup_shon = parseFloat($('#'+prefix+'_sup_shon'+j).val());
        if (isNaN(su_sup_shon) == false) {
            su_sup_shon = parseFloat(su_sup_shon.toFixed(2));
            $('#'+prefix+'_sup_shon'+j).val(su_sup_shon);
        }
        
        // Cumul des valeurs des champs source
        su_avt_shon = isNaN(su_avt_shon) ? 0 : su_avt_shon;
        su_cstr_shon = isNaN(su_cstr_shon) ? 0 : su_cstr_shon;
        su_chge_shon = isNaN(su_chge_shon) ? 0 : su_chge_shon;
        su_demo_shon = isNaN(su_demo_shon) ? 0 : su_demo_shon;
        su_sup_shon = isNaN(su_sup_shon) ? 0 : su_sup_shon;

        // Affectation du résultat de ligne
        su_tot_shon = (su_avt_shon+su_cstr_shon+su_chge_shon)-(su_demo_shon+su_sup_shon);
        su_tot_shon = parseFloat(su_tot_shon.toFixed(2));
        $('#'+prefix+'_tot_shon'+j).val(su_tot_shon);

    }

    // Calcul du total des totaux
    // Conversion des champs source en numérique flotant
    su_avt_shon = parseFloat($('#'+prefix+'_avt_shon_tot').val());
    su_avt_shon = parseFloat(su_avt_shon.toFixed(2));
    if (isNaN(su_avt_shon) == false) {
        su_avt_shon = parseFloat(su_avt_shon.toFixed(2));
        $('#'+prefix+'_avt_shon_tot').val(su_avt_shon);
    }
    su_cstr_shon = parseFloat($('#'+prefix+'_cstr_shon_tot').val());
    su_cstr_shon = parseFloat(su_cstr_shon.toFixed(2));
    if (isNaN(su_cstr_shon) == false) {
        su_cstr_shon = parseFloat(su_cstr_shon.toFixed(2));
        $('#'+prefix+'_cstr_shon_tot').val(su_cstr_shon);
    }
    su_chge_shon = parseFloat($('#'+prefix+'_chge_shon_tot').val());
    su_chge_shon = parseFloat(su_chge_shon.toFixed(2));
    if (isNaN(su_chge_shon) == false) {
        su_chge_shon = parseFloat(su_chge_shon.toFixed(2));
        $('#'+prefix+'_chge_shon_tot').val(su_chge_shon);
    }
    su_demo_shon = parseFloat($('#'+prefix+'_demo_shon_tot').val());
    su_demo_shon = parseFloat(su_demo_shon.toFixed(2));
    if (isNaN(su_demo_shon) == false) {
        su_demo_shon = parseFloat(su_demo_shon.toFixed(2));
        $('#'+prefix+'_demo_shon_tot').val(su_demo_shon);
    }
    su_sup_shon = parseFloat($('#'+prefix+'_sup_shon_tot').val());
    su_sup_shon = parseFloat(su_sup_shon.toFixed(2));
    if (isNaN(su_sup_shon) == false) {
        su_sup_shon = parseFloat(su_sup_shon.toFixed(2));
        $('#'+prefix+'_sup_shon_tot').val(su_sup_shon);
    }

    // Cumul des valeurs des champs source
    su_avt_shon = isNaN(su_avt_shon) ? 0 : su_avt_shon;
    su_cstr_shon = isNaN(su_cstr_shon) ? 0 : su_cstr_shon;
    su_chge_shon = isNaN(su_chge_shon) ? 0 : su_chge_shon;
    su_demo_shon = isNaN(su_demo_shon) ? 0 : su_demo_shon;
    su_sup_shon = isNaN(su_sup_shon) ? 0 : su_sup_shon;

    // Affectation du résultat de ligne
    su_tot_shon_tot = (su_avt_shon+su_cstr_shon+su_chge_shon)-(su_demo_shon+su_sup_shon);
    su_tot_shon_tot = parseFloat(su_tot_shon_tot.toFixed(2));
    $('#'+prefix+'_tot_shon_tot').val(su_tot_shon_tot);

}


/*
 * Marque comme lu une consultation ou commission
 */
function portletUpdateData(id, objet, objetc, file, field, message){
    
    /*Vérifie que l'identifiant passé en paramètre est bien un chiffre
     * et que le type d'objet est défini
     * */
    if ( $.isNumeric(id) && objet != '' ){
        donnees = "?ido="+id
            +'&obj='+objet
            +'&objk='+objetc
            +'&idxDossier='+getQuerystring('idx')
            +'&message='+message;
        $.ajax({
            type: "GET",
            url: "../app/"+file+".php" + donnees ,
            cache: false,
            success: function(html){
                
                $('#sousform-' + objet + ' .message').remove();
                /*Change la valeur affiché et affiche un message valide*/
                if ( $.parseJSON(html) == "Mise a jour effectue avec succes" || 
                $.parseJSON(html).indexOf("Transfert effectue avec succes") != -1 ){
                    
                    // On modife le champ field
                    if ( field != '' && message != '' ){
                        $('#'+field).html(message);
                        html = $.parseJSON(html);
                    }
                    else {
                        html = $.parseJSON(html).split(';');
                        
                        $('#'+field).html(html[0]);
                        
                        html = html[1];
                    }
                    
                    // On supprime l'action
                    $('span.'+field+'-16').parent().parent().remove();
                    // On affiche le message
                    $('#sousform-' + objet + ' .subtitle').after(
                        '<div ' +
                            'class="message ui-widget ui-corner-all ui-state-highlight ui-state-valid">' +
                            '<p>' +
                                '<span class="ui-icon ui-icon-info"></span>' +
                                '<span class="text">' +
                                    html +
                                '</span>' +
                            '</p>' +
                        '</div>'                    
                    );
                }
                /*Affichage d'une erreur*/
                else{
                    $('#sousform-' + objet + ' .subtitle').after(
                        '<div ' +
                            'class="message ui-widget ui-corner-all ui-state-highlight ui-state-error">' +
                            '<p>' +
                                '<span class="ui-icon ui-icon-info"></span>' +
                                '<span class="text">' +
                                    $.parseJSON(html) +
                                '</span>' +
                            '</p>' +
                        '</div>'                    
                    );
                }
            }
        });
    }
}

/*
 * Marque comme non frequent un petitionnaire
 */
function portletUpdatePetitionnaire(id, objet, objetc, file, field, message){
    
    /*
     *Vérifie que l'identifiant passé en paramètre est bien un chiffre
     * et que le type d'objet est défini
     */
    if ( $.isNumeric(id) && objet != '' ){
        
        donnees = "?ido=" + id + '&obj=' + objet + '&objk=' + objetc + '&idxDossier=' + getQuerystring('idx');
        $.ajax({
            type: "GET",
            url: "../app/"+file+".php" + donnees ,
            cache: false,
            success: function(html){
                
                $('#formulaire .message').remove();
                /*Change la valeur affiché et affiche un message valide*/
                if ( $.parseJSON(html) == "Mise à jour effectué avec succès" || 
                $.parseJSON(html).indexOf("Transfert effectue avec succes") != -1 ){
                    
                    // On modife le champ field
                    if ( field != '' && message != '' ){
                        $('#'+field).html(message);
                        html = $.parseJSON(html);
                    }
                    else {
                        html = $.parseJSON(html).split(';');
                        
                        $('#'+field).html(html[0]);
                        
                        html = html[1];
                    }
                    
                    // On supprime l'action
                    $('span.'+field+'-16').parent().parent().remove();
                    // On affiche le message
                    $('#formulaire #tabs-1').prepend(
                        '<div ' +
                            'class="message ui-widget ui-corner-all ui-state-highlight ui-state-valid">' +
                            '<p>' +
                                '<span class="ui-icon ui-icon-info"></span>' +
                                '<span class="text">' +
                                    html +
                                '</span>' +
                            '</p>' +
                        '</div>'
                    );
                }
                /*Affichage d'une erreur*/
                else{
                    $('#formulaire #tabs-1').prepend(
                        '<div ' +
                            'class="message ui-widget ui-corner-all ui-state-highlight ui-state-error">' +
                            '<p>' +
                                '<span class="ui-icon ui-icon-info"></span>' +
                                '<span class="text">' +
                                    $.parseJSON(html) +
                                '</span>' +
                            '</p>' +
                        '</div>'
                    );
                }
            }
        });
    }
}


// Affiche le sous formulaire onglet
function redirectPortletAction(id, onglet, nom_tabs){

    var nom_tabs =  ( typeof nom_tabs === "undefined" ) ? ".ui-tabs" : nom_tabs;
    var $tabs = $(nom_tabs).tabs(); 

    lien_onglet = $('#' + onglet).attr('href');
    lien_onglet = lien_onglet.substring( lien_onglet.length - 1, lien_onglet.length);

    $tabs.tabs('select', lien_onglet); 
}

/**
 * COMMISSION
 */
// Gestion de l'interface de gestion de la commission
function commission_manage_interface() {
    // Gestion des onglets
    var $tabs = $("#commission-manage-tabs").tabs({
        load: function(event, ui) {
            //
            om_initialize_content(true);
            return true;
        },
        select: function(event, ui) {
            // Suppression du contenu de l'onglet precedemment selectionne
            // #ui-tabs-X correspond uniquement aux ids des onglets charges
            // dynamiquement
            selectedTabIndex = $tabs.tabs('option', 'selected');
            $("#ui-tabs-"+(selectedTabIndex+1)).empty();
            return true;
        },
        ajaxOptions: {
            error: function(xhr, status, index, anchor) {
                $(anchor.hash).html(msg_alert_error_tabs);
            }
        }
    });
}
// Gestion spécifique de la soumission du formulaire qui contient des checkbox
// et qui est soumis via une requête Ajax. Il n'est pas possible de savoir
// quelles checkbox sont cochées, elles sont toutes renvoyées.
// On compose donc un champ particulier représentant les cases cochées
// avant de poster le formulaire.
function commission_submit_plan_or_unplan_demands(objsf, link, formulaire) {
    //
    $("#view_form_plan_or_unplan_demands .message").remove();
    $("#view_form_plan_or_unplan_demands").prepend(
        '<div class="msg_loading">'+msg_loading +'</div>'
    );
    //
    var checkeds = '';
    $("#view_form_plan_or_unplan_demands form input[type='checkbox']").each(
        function(index) {
            if ($(this).is(":checked")) {
                checkeds += $(this).val()+";";
            }
        }
    );
    //
    var input = document.createElement("input");
    input.type = "hidden";
    input.name = "checkeds";
    input.value = checkeds;
    formulaire.appendChild(input);
    //
    affichersform(objsf, link, formulaire);
    //
    $("#view_form_plan_or_unplan_demands .msg_loading").remove();
}
// Aide à la saisie, récupère les données du type de commission et pré-remplit
// les champs de la commission
function commission_update_data_from_commission_type(commission_type_id){
    // Requête ajax de récupération des données
    $.ajax({
        type: "GET",
        url: "../scr/form.php?obj=commission_type&action=11&idx="+commission_type_id,
        dataType: "json",
        async: false,
        success: function(data){
            // Ajout des données dans les champs
            $('#libelle').val(data.libelle);
            $('#lieu_adresse_ligne1').val(data.lieu_adresse_ligne1);
            $('#lieu_adresse_ligne2').val(data.lieu_adresse_ligne2);
            $('#lieu_salle').val(data.lieu_salle);
            $('#listes_de_diffusion').val(data.listes_de_diffusion);
            $('#participants').val(data.participants);
        }
    });
}

/**
 * Popup de confirmation pour les traitements
 */
//
function trt_confirm() {
    //
    if (confirm("Etes-vous sur de vouloir confirmer cette action ?")) {
        //
        return true;
    } else {
        //
        return false;
    }
}

/**
 * Cette fonction permet d'afficher les options d'un select par rapport
 * à un autre champ
 * 
 * @param  int id               Données (identifiant) du champ visé
 * @param  string tableName     Nom de la table
 * @param  string linkedField   Champ visé
 * @param  string formCible     Formulaire visé
 */
function filterSelect(id, tableName, linkedField, formCible) {

    //lien vers script php
    link = "../app/filterSelect.php?idx=" + id + "&tableName=" + tableName +
            "&linkedField=" + linkedField + "&formCible=" + formCible;

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(res){
            //
            $('#'+tableName).empty();
            //
            for ( j=0 ; j < res[0].length ; j++ ){
                //
                $('#'+tableName).append(
                    '<option value="'+res[0][j]+'" >'+res[1][j]+'</option>'
                );
            }
        },
        async: false
    });
}

/**
 * Redirige vers une page après un temps d'attente
 * @param  string   page            lien de la page vers la redirection
 * @param  int      milliseconde    temps d'attente avant la redirection
 */
function redirection(page, milliseconde) {

    //
    setTimeout(window.location = page, milliseconde);
}

/**
 * Retourne ce qu'il y a à afficher dans le formulaire de retour (bouton ajouter ou supprimer et éditer)
 */
function getObjId(obj){

    //Récupération de l'id de l'objet
    var id=$('#id_retour').val();

    //Retour des données
    if ( $('#id_retour').length > 0 ){
        setDataFrequent(id, obj);
        om_initialize_content();
    }
}

function setDataFrequent(id,obj) {
    $.ajax({
        type: "GET",
        url: '../app/afficher_synthese_obj.view.php?idx='+id+'&obj=' + obj,
        cache: false,
        success: function(html){
            $('.add_'+obj).each(
                function(){
                    $(this).remove();
                }
            );
            $(html).insertBefore('#'+obj).fadeIn(500);
            $('#'+obj).val(id);
        },
        async:false
    });
}

/**
 * Redirige vers le script PHP pour mettre à jour les informations
 * et met à jour l'interface pour l'utilisateur
 * @param  string   obj             Objet du dossier
 * @param  string   id              Identifiant du dossier
 * @param  string   fieldname       Nom du champ
 * @param  Function callback        Fonction mettant à jour l'interface des données
 * @param  string   confirm_message Texte du message de confirmation
 */
function geolocalisation_treatment(obj, id, fieldname, callback, confirm_message) {

    var idx_dossier = id;
    // Overlay de confirmation du traitement
    if (confirm_message != null && confirm_message != '') {
        var dialog_confirm = confirm(confirm_message);
        if( dialog_confirm == false ){
          return false;
        }
    }

    // Affichage du spinner
    $('#'+fieldname).each(
        function(){
            $(this).children().removeClass();
            $(this).children().addClass('message ui-widget ui-corner-all ui-state-highlight ui-state-empty');
            $(this).children().children().children(".text").html(msg_loading);
        }
    );
    
    // lien vers script PHP faisant l'appel au webservice
    link = '../scr/form.php?obj=' + obj + '&idx='+id+'&action=';
    // sélection de l'action en fonction du bouton cliqué
    switch(fieldname) {
        case 'verif_parcelle':
            link += '121';
            break;
        case 'calcul_emprise':
            link += '122';
            break;
        case 'dessin_emprise':
            link += '123';
            break;
        case 'calcul_centroide':
            link += '124';
            break;
        case 'recup_contrainte':
            link += '125';
            break;
    } 

    // Traitement
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(res){
            //
            $('#'+fieldname).each(
                function(){
                    // Affichage du message en face de l'action
                    $(this).children().children().children(".text").text(res['log']['message']);
                    $(this).children().removeClass();
                    // Changement de la couleur du message selon la réponse de
                    // l'action
                    if (res['log']['etat'] == true) {
                        $(this).children().addClass('message ui-widget ui-corner-all ui-state-highlight ui-state-valid');
                    } else {
                        $(this).children().addClass('message ui-widget ui-corner-all ui-state-highlight ui-state-error');
                    }
                }
            );

            // Appel de la méthode de callback si l'action s'est déroulée correctement
            if (typeof(callback) === "function" && res['log']['etat'] == true) {
                callback(res, idx_dossier);
            }
        },
        async:false
    });
}

/**
 * Efface le message en haut du formulaire
 * @param array res Résultat après le traitement du script PHP
 */
function set_geolocalisation_message(res, id) {
    // Si il y a un message d'erreur
    if ($('#geolocalisation-message')[0]) {
        // Supprime le message
        $('#geolocalisation-message').remove();
        // Modifie les messages des autres fonctionnalités
        $('#calcul_emprise').each(
            function() {
                $(this).children().removeClass();
                $(this).children().addClass('message ui-widget ui-corner-all ui-state-highlight ui-state-error');
                $(this).children().children().children(".text").text(res['log']['message_diff_parcelle']);
            }
        );
        $('#calcul_centroide').each(
            function() {
                $(this).children().removeClass();
                $(this).children().addClass('message ui-widget ui-corner-all ui-state-highlight ui-state-error');
                $(this).children().children().children(".text").text(res['log']['message_diff_parcelle']);
            }
        );
        $('#recup_contrainte').each(
            function() {
                $(this).children().removeClass();
                $(this).children().addClass('message ui-widget ui-corner-all ui-state-highlight ui-state-error');
                $(this).children().children().children(".text").text(res['log']['message_diff_parcelle']);
            }
        );
    }
    
}

/**
 * Met le champ centroïde à jour
 * @param  array   res	Résultat après le traitement du script PHP
 */
function set_geolocalisation_centroide(res, id) {
    var content = "<a id='action-form-localiser'"+
            " target='_SIG' href='../scr/form.php?obj=dossier_instruction&action=140&idx="+id+"'>"+
            "<span class='om-icon om-icon-16 om-icon-fix sig-16' title='Localiser'>Localiser</span> "+
            " POINT("+res['x']+" "+res['y']+")"+
            " </a>";
    
    $('span#centroide').html(content);
}

/**
 * Met le champ contrainte à jour
 * @param  array   res	Résultat après le traitement du script PHP
 */
function set_geolocalisation_contrainte(res, id) {
    // Contenus du champ contrainte
    var content = $('span#msg_contrainte_sig').html();
    // Si aucune contraintes est récupérées du SIG
    if (res['dossier_contrainte']['nb_contrainte_sig'] == 0) {
        content = res['dossier_contrainte']['msg_contrainte_sig_empty'];
    }
    // S'il y a des contraintes récupérées
    if (res['dossier_contrainte']['nb_contrainte_sig'] != 0) {
        content = res['dossier_contrainte']['nb_contrainte_sig'] + " " + res['dossier_contrainte']['msg_contrainte_sig']
    }
    // Affiche le message
    $('span#msg_contrainte_sig').html(content);
}

/**
 * Redirige vers le sig
 * @param array res Résultat après le traitement du script PHP
 */
function redirection_web_sig(res, id) {
    window.open(res['return']);
}

/**
 * Traitement du bouton permettant de calculer toutes les données géographiques
 * @param  string   obj                 Objet du dossier
 * @param  string   id                  Identifiant du dossier
 * @param  string   confirm_message     Texte du message de confirmation
 */
function all_geolocalisation_treatments(obj, id, confirm_message) {

    // Overlay de confirmation du traitement
    if (confirm_message != null && confirm_message != '') {
        var dialog_confirm = confirm(confirm_message);
        if( dialog_confirm == false ){
          return false;
        }
    }

    // Initialisation des variables utilisées en paramètres
    var fieldname = '';
    var callback = '';
    var confirm_message = '';
    var flag = '';

    // Modification des variables utilisées en paramètres pour "Vérifier les
    // parcelles"
    fieldname = 'verif_parcelle';
    callback = set_geolocalisation_message;
    // Traitement "Vérifier les parcelles"
    geolocalisation_treatment(obj, id, fieldname, callback, confirm_message);

    // Positionne le flag sur le message de "Vérifier les parcelles"
    flag = $('#'+fieldname+"-message").attr("class");
    // Si c'est un message d'erreur on arrete le traitement
    if (flag == 'message ui-widget ui-corner-all ui-state-highlight ui-state-error') {
        return false;
    }

    // Modification des variables utilisées en paramètres pour "Calculer 
    // l'emprise"
    fieldname = 'calcul_emprise';
    callback = '';
    // Traitement "Calculer l'emprise"
    geolocalisation_treatment(obj, id, fieldname, callback, confirm_message);

    // Positionne le flag sur le message de "Calculer l'emprise"
    flag = $('#'+fieldname+"-message").attr("class");
    // Si c'est un message d'erreur on arrete le traitement
    if (flag == 'message ui-widget ui-corner-all ui-state-highlight ui-state-error') {
        return false;
    }

    // Modification des variables utilisées en paramètres pour "Calculer le 
    // centroïde"
    fieldname = 'calcul_centroide';
    callback = set_geolocalisation_centroide;
    // Traitement "Calculer le centroïde"
    geolocalisation_treatment(obj, id, fieldname, callback, confirm_message);

    // Positionne le flag sur le message de "Calculer le centroïde"
    flag = $('#'+fieldname+"-message").attr("class");
    // Si c'est un message d'erreur on arrete le traitement
    if (flag == 'message ui-widget ui-corner-all ui-state-highlight ui-state-error') {
        return false;
    }

    // Modification des variables utilisées en paramètres pour "Récupérer les
    // contraintes"
    fieldname = 'recup_contrainte';
    callback = set_geolocalisation_contrainte;
    //Traitement "Récupérer les contraintes"
    geolocalisation_treatment(obj, id, fieldname, callback, confirm_message);
    
}

/**
 * Remplit le formulaire avec l'adresse trouvée ou affiche un message d'erreur
 */
function getAdressFromCadastrale(){
    
    //Récupération des références cadastrales
    var referenceCadastrale = '{"refcad":[{';
    var i = 0 ;
    var j = 1 ;
    var delimit = 0;
    var parcelleDelimit = 0;
    var noReferenceCadastrale = false;
    var om_collectivite = "";
    if($("#om_collectivite").attr("type") != "hidden") {
        om_collectivite = $("#om_collectivite").val();
        if(om_collectivite == '') {
            alert('Une collectivité doit être sélectionnée');
            return;
        }
    }
    $(".reference_cadastrale_custom_field").each(
        function(){
            
            //On vérifie que le champ n'est pas vide
            if ($(this).val()!=""&&$(this).val() != ";"){
                noReferenceCadastrale = true;
            }
            
            //On va à la ligne suivante
            if ( $(this).val() == ";" ){
                referenceCadastrale +=(delimit!=0)?'}]':''; 
                referenceCadastrale += "}" ;
                i++;
                j = 1;
                delimit = 0;
                parcelleDelimit = 0;
            }
            //On parcourt la ligne
            else {
                switch(true){
                    //Quartier
                    case (j==1):
                        referenceCadastrale += (i!=0)?',':'';
                        referenceCadastrale += '"' + i + '"' + ':{"quartier":"'+$(this).val()+'"';
                        break;
                    //Section
                    case (j==2):
                        referenceCadastrale += ', "section":"'+$(this).val()+'"';
                        break;
                    //Parcelle
                    case (j==3):
                        referenceCadastrale += ', "parcelle":"'+$(this).val()+'"';
                        break;
                    //Le délimiteur
                    case (j%2==0&&j!=2):
                        if ( delimit==0 ){
                            referenceCadastrale += ', "delimit":[{"'+(delimit++)+'":"'+$(this).val()+'"';
                        }
                        else {
                            referenceCadastrale += ', "'+(delimit++)+'":"'+$(this).val()+'"';
                        }
                        break;
                    //La parcelle après le délimiteur
                    case (j%2==1&&j!=1&&j!=3):
                        referenceCadastrale += ',"'+(delimit++)+'":"'+$(this).val()+'"';
                        break;
                }
                j++;
            }
        }
    );
    if ( noReferenceCadastrale == true ){
        referenceCadastrale += "}]";
        if(om_collectivite != "") {
            referenceCadastrale += ', "om_collectivite":'+om_collectivite;
        }
        referenceCadastrale += "}";
    }
    else {
        referenceCadastrale = "";
    }
    // Lien
    link = "../scr/form.php?obj=demande&action=130&idx=0";
    // Affichage du spinner
    ;
    $('#cad-adr-them').parent().append('<img id="adresse_cadastral_spinner" src="../img/loading.gif" alt="Le traitement est en cours. Merci de patienter.">');
    //Lance la tentative de récupération de l'adresse
    $.ajax({
        type: "POST",
        url: link,
        data: $.parseJSON(referenceCadastrale),
        cache: false,
        dataType: "json",
        success: function(data){
            //Si le retour est un objet JSON, on a un résultat
            if ( $.isPlainObject(data)){
                
                //On met l'adresse dans les champs
                $("#terrain_adresse_voie_numero").val(data.return_addr.numero_voie);
                $("#terrain_adresse_voie").val(data.return_addr.nom_voie);
                $("#terrain_adresse_code_postal").val(data.return_addr.code_postal);
                $("#terrain_adresse_localite").val(data.return_addr.localite);
            }
            //Sinon, on affiche un message d'erreur
            else {
                alert(data);
            }
            $('#adresse_cadastral_spinner').remove();
        },
        async: false
    });
}

/**
 * Modifie les champs requis pour le formulaire demande_type
 * @param  integer  demande_nature                         Identifiant
 * @param  string   lib_dossier_autorisation_type_detaille Libellé du champ
 * dossier_autorisation_type_detaille
 * @param  string   lib_dossier_instruction_type           Libellé du champ
 * dossier_instruction_type
 */
function required_fields_demande_type(demande_nature, lib_dossier_autorisation_type_detaille, lib_dossier_instruction_type) {

    // Lien
    link = "../scr/form.php?obj=demande_nature&action=11&idx=" + demande_nature;

    // Traitement
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(demande_nature_info) {

            // On enlève le "*" au libellé du champ 
            // dossier_autorisation_type_detaille pour montrer qu'il n'est 
            // pas obligatoire
            $("#lib-dossier_autorisation_type_detaille").text('');
            $("#lib-dossier_autorisation_type_detaille")
                .html(lib_dossier_autorisation_type_detaille);

            // On enlève le "*" au libellé du champ 
            // dossier_instruction_type pour montrer qu'il n'est 
            // pas obligatoire
            $("#lib-dossier_instruction_type").text('');
            $("#lib-dossier_instruction_type")
                .html(lib_dossier_instruction_type);
            
            // Si c'est une nouvelle demande
            if (demande_nature_info != ''
                && demande_nature_info != null) {

                if (demande_nature_info['code'] == 'NOUV') {

                    // On ajoute le "*" au libellé du champ 
                    // dossier_autorisation_type_detaille pour montrer qu'il est 
                    // obligatoire
                    $("#lib-dossier_autorisation_type_detaille").text('');
                    $("#lib-dossier_autorisation_type_detaille")
                        .html(lib_dossier_autorisation_type_detaille + ' <span class="not-null-tag">*</span>');

                    // On ajoute le "*" au libellé du champ 
                    // dossier_instruction_type pour montrer qu'il est 
                    // obligatoire
                    $("#lib-dossier_instruction_type").text('');
                    $("#lib-dossier_instruction_type")
                        .html(lib_dossier_instruction_type + ' <span class="not-null-tag">*</span>');

                } 

            }
            
        },
        async:false
    });
}

/**
 * Appel au chargement de la page
 **/
$(function() {

    // url de la page
    url = document.location + "";

    // Si dans le formulaire des type de demande
    // et que c'est en mode ajout ou modifier
    if (url.indexOf("form.php?obj=demande_type&") != -1
        && (url.indexOf("&action=0") != -1
        || url.indexOf("&action=1") != -1
        || url.indexOf("&validation=") != -1)) {

        // Récupère les paramètres nécessaires
        var demande_nature = $("#demande_nature").val();
        var lib_dossier_autorisation_type_detaille = $("#lib-dossier_autorisation_type_detaille").text();
        var lib_dossier_instruction_type = $("#lib-dossier_instruction_type").text();

        // Appelle la fonction pour indiquer si les champs sont requis ou non
        required_fields_demande_type(demande_nature, lib_dossier_autorisation_type_detaille, lib_dossier_instruction_type);
    }

});

/**
 * Cache les champs du formulaire événement
 * @param  array fields Tableau des champs
 */
function hideFieldsEvenement(fields) {

    // Pour chaque champ
    for (var cpt = 0; cpt < fields.length; cpt++) {

        $('#' + fields[cpt]).each(
            function(){

                // Récupère la balise parent et la valeur
                var parentField = $(this).parent();
                var valueField = $(this).val();
                // Supprime le champ
                $(this).remove();
                // Affiche le champ en tant que static
                parentField.append('<span id="'+fields[cpt]+'" class="field_value">'+valueField+'</span>');
            }
        );

    }
    
    // Cache les champs "evenement_retour_ar" et "evenement_retour_signature"
    $('#evenement_retour_ar').parent().parent().hide();
    $('#evenement_retour_signature').parent().parent().hide();
}

/**
 * Affiche les champs du formulaire événement
 * @param  array fields Tableau des champs
 */
function showFieldsEvenement(fields) {

    // Pour chaque champ
    for (var cpt = 0; cpt < fields.length; cpt++) {

        $('#' + fields[cpt]).each(
            function(){

                // Récupère la balise parent et la valeur
                var parentField = $(this).parent();
                var valueField = $(this).text();
                // Supprime le champ
                $(this).remove();

                // Si le champ est restriction
                if (this.id == 'restriction') {

                    // Réaffiche le champ en type text
                    parentField.append('<input id="'+this.id+'" class="champFormulaire" type="text" maxlength="60" size="30" value="'+ valueField +'" name="'+this.id+'">');
                } else {

                    // Réaffiche les selects
                    parentField.append('<select id="'+this.id+'" class="champFormulaire" size="1" name="'+this.id+'"></select>');
                    filterSelect(valueField, fields[cpt], 'delai', 'evenement');
                }
            }
        );

    }

    // Affiche les champs "evenement_retour_ar" et "evenement_retour_signature"
    $('#evenement_retour_ar').parent().parent().show();
    $('#evenement_retour_signature').parent().parent().show();
}

/**
 * Action onchange sur la case à cocher "retour" du formulaire "evenement"
 * @param  object field Champ de type booléen
 */
function retourOnchangeEvenement(field) {

    // liste des champs à modifier dans le formulaire
    var fields = new Array(
        'restriction', 
        'delai', 
        'accord_tacite', 
        'delai_notification', 
        'avis_decision'
    );

    // Si le champ booléen est à 'Oui'
    if (field.value == 'Oui') {
        // Cache et rend static les champs de la liste
        hideFieldsEvenement(fields);
    } else {
        // Affiche et rend modifiable les champs de la liste
        showFieldsEvenement(fields);
    }
}

function dossierContrainteValidationForm(objsf, link, formulaire) {
    // composition de la chaine data en fonction des elements du formulaire
    var data = "";
    if (formulaire) {
        for (i=0; i<formulaire.elements.length; i++) {
            data += formulaire.elements[i].name+"="+encodeURIComponent(formulaire.elements[i].value)+"&";
        }
    }
    
    // execution de la requete en POST
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: data,
        dataType: "json",
        success: function(html){
            // Efface le message
            $('.message').remove();
            // Affiche le message
            $('.subtitle').after(
                '<div ' +
                    'class="message ui-widget ui-corner-all ui-state-highlight ui-state-valid">' +
                    '<p>' +
                        '<span class="ui-icon ui-icon-info"></span>' +
                        '<span class="text">' +
                            html +
                        '</span>' +
                    '</p>' +
                '</div>'                    
            );
            // Décoche toutes les checkbox
            $(":checkbox").attr('checked', false);
            $(":checkbox").attr('value', '');
        }
    });
}

/**
 * Permet de recharger la page.
 */
function refresh_page_return() {
    // Recharge la page
    location.reload();
}

/**
 * Action permettant la génération d'une archive zip contenant les documents
 * numérisés de la page.
 *
 * @param {array}  var_text Chaînes d'affichage.
 * @param {string} dossier  Identifiant du DA ou DI (selon contexte pièce).
 */
function zip_doc_numerise(var_text, dossier, obj) {
    // Message d'attente
    waiting_message_loading ='<img src="../img/loading.gif" alt="'+var_text.waiting_message+'" />'+var_text.waiting_message;
    // Création du modal dialog de confirmation
    id_sousform = "#sousform-" + obj;
    addDivDialog(id_sousform);
    $( "#dialog" ).html(var_text.confirm_message);
    $( "#dialog" ).dialog({
        title: var_text.title,
        resizable: false,
        height:140,
        modal: true,
        buttons: [
            {
                text: var_text.confirm_button_ok,
                click: function() {
                    // Si confirmation :
                    // on enlève les boutons
                    $( "#dialog" ).dialog( "option", "buttons", {});
                    // on change le contenu du modal
                    $( "#dialog" ).html(waiting_message_loading);
                    // on récupère les identifiant des documents numérisés
                    ids = new Array();
                    $('tr td .lienDocumentNumerise').each(function() {
                        ids.push(this.id.replace("document_numerise_", ""));
                    });
                    // On appel le script de génération de l'archive
                    $.ajax({
                        url: "../scr/form.php?obj=" + obj + "&action=100&idx=0&dossier="+dossier+"&ids="+ids.join(','),
                        dataType: "json"
                    }).done(
                        function(data) {
                            // Une fois la génération terminée on affiche l'erreur
                            // ou le lien de téléchargement
                            if(data.status == false) {
                                $( "#dialog" ).html(var_text.error_message);
                            } else {
                                link = '<a id="archive_download_link" href="../spg/file.php?uid='+data.file+
                                    '&dl=download&mode=temporary">'+
                                    '<span class="om-icon om-icon-16 om-icon-fix archive-16" title="'+var_text.download_link_message+'">'+
                                    var_text.download_link_message+'</span>'+var_text.download_link_message+'</a>';
                                $( "#dialog" ).html(var_text.download_message+'<br/>'+link);
                            }
                        }
                    ).fail(
                        function() {
                            $( "#dialog" ).html(var_text.error_message);
                        }
                    );
                }
            }, {
                text: var_text.confirm_button_ko,
                click: function() {
                    $(this).dialog('close');
                }
            }
        ],
        //OnClose suppression du contenu
        close: function(ev, ui) {
            $(this).remove();
        }
    });
}

function manage_display_demande(idx_datd) {
    var request = $.ajax({
        type: "GET",
        url: "../scr/form.php?obj=dossier_autorisation_type_detaille&action=4&idx="+idx_datd,
        cache: false,
        dataType: "json",
    });
    request.done(function(affichage_form) {
        $('.demande_autorisation_contestee_hidden_bloc').hide();
        hideFields();
        
        switch(affichage_form) {
            
            case 'CTX RE':
                // On affiche le champ de recherche de dossier à contester
                $('.demande_autorisation_contestee_hidden_bloc').show();
                // Désactivation de la validation du formulaire de la demande
                // par l'appui sur la touche entrée
                $(document).on("keypress", '#autorisation_contestee', function (e) {
                    var code = e.keyCode || e.which;
                    if (code == 13) {
                        e.preventDefault();
                        return false;
                    }
                });
                // Dans le cas d'un formulaire soumis en erreur le champ
                // peut être déjà saisie, on recherche donc les informations
                if ($('#autorisation_contestee').val() != '') {
                    lookingForAutorisationContestee();
                }
                break;
            default:
                showFormDemande();
                break;
        }
    });
}

/**
 * Méthode de mise en forme semblable à sprintf :
 * "lorem {0} dolor {1} amet".format("ipsum", "sit");
 *
 * @return,  string Chaîne fourni avec remplacement des index
 */
String.prototype.format = function () {
        var args = [].slice.call(arguments);
        return this.replace(/(\{\d+\})/g, function (a){
            return args[+(a.substr(1,a.length-2))||0];
        });
};
