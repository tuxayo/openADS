<?php
/**
 * Ce script a pour objet de recuperer la liste des quartiers d'un arrondissement
 *
 * @package openfoncier
 * @version SVN : $Id: listData.php 4418 2015-02-24 17:30:28Z tbenita $
 */

require_once "../obj/utils.class.php";

$f = new utils("nohtml");
$f->isAccredited(array("demande","demande_modifier","demande_ajouter"), "OR");
$f->disableLog();

// Identifiant de l'arrondissement
$idx = ($f->get_submitted_get_value("idx") != null) ? $f->get_submitted_get_value("idx") : "";
$tableName = ($f->get_submitted_get_value("tableName") != null) ? $f->get_submitted_get_value("tableName") : "";
$linkedField = ($f->get_submitted_get_value("linkedField") != null) ? $f->get_submitted_get_value("linkedField") : "";
$nature = ($f->get_submitted_get_value("nature") != null) ? $f->get_submitted_get_value("nature") : "";

$sql = 
    "SELECT 
        $tableName, libelle
    FROM 
        ".DB_PREFIXE."$tableName";

if ( isset($idx) && $idx !== '' && $idx !== '*' && is_numeric($idx)){
    
    /*Requête qui récupère les quartiers en fonction de leur arrondissement*/
    $sql .= 
        " WHERE 
            $linkedField = $idx ";
    if($nature != "") {
        $sql .= "AND demande_nature = 2 ";
    } else {
        $sql .= "AND demande_nature = 1 ";
    }
    $sql .= "ORDER BY 
            libelle
        ";
}

$res = $f->db->query($sql);
$f->isDatabaseError($res);

$listData = "";
while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
    
    $listData .= $row[$tableName]."_".$row['libelle'].";";
}

echo json_encode($listData);

?>