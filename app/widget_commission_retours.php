<?php
/**
 * WIDGET DASHBOARD - widget_commission_mes_retours
 *
 * @package openads
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
if (!isset($f)) {
    $f = new utils(null, "widget_commission_retours",
                   _("Widget - Retours de commission"));
}

require_once "../obj/om_widget.class.php";
$om_widget = new om_widget(0);
//
if (!isset($content)) {
    $content = null;
}
//
$om_widget->view_widget_commission_retours($content);

?>