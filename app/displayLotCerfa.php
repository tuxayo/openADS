<?php
/**
 * Ce script permet de récupérer l'id des donnéees techniques en fonction de celui du lot
 *
 * @package openfoncier
 * @version SVN : $Id: displayLotCerfa.php 4418 2015-02-24 17:30:28Z tbenita $
 */
//
require_once "../obj/utils.class.php";
$f = new utils("nohtml", "donnees_techniques");
$f->disableLog();
//
$idx = ($f->get_submitted_get_value("lot") !== null ? $f->get_submitted_get_value("lot") : "");

$sql = "SELECT donnees_techniques
            FROM ".DB_PREFIXE."donnees_techniques
            WHERE donnees_techniques.lot = ".$idx;
$res = $f->db->getOne($sql);
$f->isDatabaseError($res);
echo $res;
?>