<?php
/**
 * Ce script a pour objet de récupérer la liste des pétitionnaires
 * correspondants aux critères de recherche
 *
 * @package openfoncier
 * @version SVN : $Id: afficher_synthese_obj.view.php 4752 2015-05-13 08:00:03Z nhaye $
 */
require_once "../obj/utils.class.php";
require_once "../obj/om_formulaire.class.php";
//
$f = new utils("nohtml");
$f->disableLog();
//Données
$idx = ($f->get_submitted_get_value('idx') != null ? $f->get_submitted_get_value('idx') : "]" );
$obj = ($f->get_submitted_get_value('obj') != null ? $f->get_submitted_get_value('obj') : "" );
//
$f->isAccredited(array($obj,$obj."consulter",), "OR");
//
require_once "../obj/".$obj.".class.php";
// Affichage des données
$obj = new $obj($idx,$f->db,false);
if($idx == ']') {
    $obj->setParameter("maj", 0);
}
$obj->afficherSynthese();


$obj->__destruct();

?>
