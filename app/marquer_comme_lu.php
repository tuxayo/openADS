<?php
/**
 * Ce script a pour objet de mettre à "lu" la consultation idc
 *
 * @package openfoncier
 * @version SVN : $Id: marquer_comme_lu.php 4701 2015-04-30 16:36:34Z vpihour $
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");
$f->disableLog();

/*Donnees*/
$ido = ($f->get_submitted_get_value('ido') ? $f->get_submitted_get_value('ido') : "" );
$obj = ($f->get_submitted_get_value('obj') ? $f->get_submitted_get_value('obj') : "" );
$objk = ($f->get_submitted_get_value('objk') ? $f->get_submitted_get_value('objk') : "" );

/*Vérifie que l'utilisateur a les droits de mettre à jour une consultation 
 * ou a un bypass*/
if ($f->isAccredited(array($obj, $obj . "_modifier_lu"), "OR")||$f->can_bypass($obj, "modifier_lu")) {
    
    /*Vérifie que l'identifiant passé est correcte*/
    if ( is_numeric($ido) && $ido != "" && $obj != "" && $objk != "" ){
        
        /*Vérifie que l'identifiant existe*/
        $sql = "SELECT 
                    $objk
                FROM
                    ".DB_PREFIXE."$obj
                WHERE $objk = $ido";
                 
        $res = $f->db->query($sql);
        $f->isDatabaseError($res);
        
        /*Si l'identifiant existe bien, met à jour*/
        if ( $res->numrows() > 0 ){
            
            /*Si la consultation n'est pas marquée comme lue*/
            $sql = "SELECT 
                        $objk
                    FROM
                        ".DB_PREFIXE."$obj
                    WHERE $objk = $ido AND lu = FALSE";
                    
            $res = $f->db->query($sql);
            $f->isDatabaseError($res);
            
            if ( $res->numrows() > 0 ){
                
                $valF = array (
                    "lu" => TRUE
                    );
        
                $res = $f->db->autoExecute(
                            DB_PREFIXE."$obj", 
                            $valF, 
                            DB_AUTOQUERY_UPDATE,
                            "$objk = ".$ido
                        );
                $f->isDatabaseError($res);
                
                /*Envoie du message de retour*/
                echo json_encode("Mise a jour effectue avec succes");
            }
            /*Sinon*/
            else {
                    
                /*Envoie du message de retour*/
                echo json_encode("Element deja marque comme lu");
            }
            
        }
        /*Sinon*/
        else {
            
            /*Envoie du message de retour*/
            echo json_encode("Cet identifiant n'existe pas");
        }
    }
    /*Sinon*/
    else {
        
        /*Envoie du message de retour*/
        echo json_encode("Identifiant de $obj incorrect");
    }
}
/*Sinon*/
else {
    
    /*Envoie du message de retour*/
    echo json_encode("Droits insuffisants. Vous n'avez pas suffisamment de droits pour acceder a cette page.");
}

?>