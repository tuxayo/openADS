<?php
/**
 * Ce script a pour objet d'effectuer le traitement de transfert de lot.
 *
 * @package openfoncier
 * @version SVN : $Id: transferer_lot_nouveaux_demandeurs.php 4418 2015-02-24 17:30:28Z tbenita $
 */

//
require_once "../obj/utils.class.php";
$f = new utils("nohtml", "lot");
$f->disableLog();

/*Donnees*/
$ido = ($f->get_submitted_get_value('ido')!==null ? $f->get_submitted_get_value('ido') : "" );
$obj = ($f->get_submitted_get_value('obj')!==null ? $f->get_submitted_get_value('obj') : "" );
$objk = ($f->get_submitted_get_value('objk')!==null ? $f->get_submitted_get_value('objk') : "" );
$idxDossier = ($f->get_submitted_get_value('idxDossier')!==null ? $f->get_submitted_get_value('idxDossier') : "" );

//Vérifie que l'utilisateur a les droits de transférer les lots
if ($f->isAccredited(array($obj, $obj . "_transferer"), "OR")) {
    
    //Vérifie que l'identifiant passé est correcte
    if ( is_numeric($ido) && $ido != "" && $obj != "" && $objk != "" ){
        
        //Vérifie que l'identifiant existe
        $sql = "SELECT 
                    $objk
                FROM
                    ".DB_PREFIXE."$obj
                WHERE $objk = $ido";
                 
        $res = $f->db->query($sql);
        $f->isDatabaseError($res);
        
        //Si l'identifiant existe bien, transfère
        if ( $res->numrows() > 0 ){
            
            //Si les liaisons n'existent pas déjà
            $sql = "SELECT 
                        $obj.$objk as $objk, lien_lot_demandeur.demandeur as demandeur
                    FROM
                        ".DB_PREFIXE."lien_dossier_demandeur
                    INNER JOIN ".DB_PREFIXE."lien_lot_demandeur
                        ON
                            lien_lot_demandeur.demandeur = lien_dossier_demandeur.demandeur
                    INNER JOIN ".DB_PREFIXE."$obj
                        ON 
                            $obj.$objk = $ido
                    WHERE lien_dossier_demandeur.dossier = '$idxDossier'";

            $res = $f->db->query($sql);
            $f->isDatabaseError($res);
            
            // Récupère la liste des demandeurs associés aux lot et dossier d'instruction
            $listDemandeurLie = array();
            if ( $res->numrows() > 0 ) {
                
                $i = 0;        
                while($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
                     
                     $listDemandeurLie[$i++] = $row['demandeur'];
                }
            }
            
            // Récupère les demandeurs du dossier d'instruction
            $sql = "SELECT 
                        lien_dossier_demandeur.demandeur as demandeur, 
                        lien_dossier_demandeur.petitionnaire_principal as pp
                    FROM
                        ".DB_PREFIXE."lien_dossier_demandeur
                    WHERE lien_dossier_demandeur.dossier = '$idxDossier'";

            $res = $f->db->query($sql);
            $f->isDatabaseError($res);
            
            
            // Transfert des demandeurs entre le dossier et le lot
            if ( count($listDemandeurLie) != $res->numrows() ){
                
                //Supprime les anciens liens
                $sql = "DELETE FROM ".DB_PREFIXE."lien_lot_demandeur 
                        WHERE lien_lot_demandeur.lot = $ido";

                $res2 = $f->db->query($sql);
                $f->isDatabaseError($res2);
                
                $ret = "";
                
                // Créé autant de liaisons que de demandeurs liés au dossier d'instruction
                while($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
                    
                    if ( !in_array($row['demandeur'], $listDemandeurLie)){
                           
                        $valLLD = array();
                        $valLLD['lien_lot_demandeur'] = NULL;
                        $valLLD['lot'] = $ido;
                        $valLLD['demandeur'] = $row['demandeur'];
                        $valLLD['petitionnaire_principal'] = $row['pp'];
                        
                        require_once '../obj/lien_lot_demandeur.class.php';    
                        $lld = new lien_lot_demandeur("]", $f->db, DEBUG);
                        $lld->valF = "";
                                                
                        $lld->ajouter($valLLD, $f->db, DEBUG) ;
                        
                        $sql = "SELECT
                                    civilite.code as code, 
                                    CASE WHEN demandeur.qualite='particulier' 
                                        THEN TRIM(CONCAT(demandeur.particulier_nom, ' ', demandeur.particulier_prenom)) 
                                        ELSE TRIM(CONCAT(demandeur.personne_morale_raison_sociale, ' ', demandeur.personne_morale_denomination)) 
                                    END as nom
                                FROM
                                    ".DB_PREFIXE."demandeur
                                LEFT JOIN
                                    ".DB_PREFIXE."civilite
                                    ON
                                        demandeur.particulier_civilite = civilite.civilite OR
                                        demandeur.personne_morale_civilite = civilite.civilite
                                WHERE demandeur.demandeur = ".$row['demandeur'];
        
                        $res2 = $f->db->query($sql);
                        $f->isDatabaseError($res2);
                        $row=& $res2->fetchRow(DB_FETCHMODE_ASSOC);
                        $ret .= $row['code']. " " . $row['nom'] . "<br/>" ;
                    }
                }        
                
                //Envoie du message de retour
                echo json_encode($ret.";Transfert effectue avec succes");
            }
            //Sinon
            else {
                //Envoie du message de retour
                echo json_encode("Les demandeurs ont deja ete transferes");
            }
            
        }
        //Sinon
        else {
            
            //Envoie du message de retour
            echo json_encode("Cet identifiant n'existe pas");
        }
    }
    //Sinon
    else {
        
        //Envoie du message de retour
        echo json_encode("Identifiant de $obj incorrect");
    }
}
//Sinon
else {
    
    //Envoie du message de retour
    echo json_encode("Droits insuffisants. Vous n'avez pas suffisamment de droits pour acceder a cette page.");
}

?>