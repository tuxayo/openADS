--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a8
--
-- XXX Ce fichier doit être renommé en v3.14.0-a8.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.14.0-a8.sql 3008 2014-04-29 09:13:41Z vpihour $
--------------------------------------------------------------------------------

-- Les champs du rapport d'instruction en html
ALTER TABLE rapport_instruction RENAME analyse_reglementaire TO analyse_reglementaire_om_html;
ALTER TABLE rapport_instruction RENAME description_projet TO description_projet_om_html;

UPDATE om_requete SET requete=replace(requete, 'description_projet as','description_projet_om_html as');
UPDATE om_requete SET requete=replace(requete, 'analyse_reglementaire as','analyse_reglementaire_om_html as');

-- Contrainte d'unicité sur l'identifiant du DI dans données techniques
ALTER TABLE donnees_techniques ADD CONSTRAINT donnees_techniques_dossier_instruction_unique UNIQUE (dossier_instruction);
ALTER TABLE donnees_techniques ADD CONSTRAINT donnees_techniques_lot_unique UNIQUE (lot);
ALTER TABLE donnees_techniques ADD CONSTRAINT donnees_techniques_dossier_autorisation_unique UNIQUE (dossier_autorisation);
-- Ajout d'un champ cerfa au données techniques
ALTER TABLE donnees_techniques ADD COLUMN cerfa integer NOT NULL DEFAULT 0;
ALTER TABLE donnees_techniques ALTER COLUMN cerfa DROP DEFAULT;

-- Ajout des droits de visualisation et de téléchargement
-- du pdf associé à l'avis d'un service (objet consultation)
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_fichier_voir', 
(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE NOT EXISTS (
    SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_voir' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
);
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_fichier_telecharger', 
(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE NOT EXISTS (
    SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
);

--
-- Modification du sous-état instruction
-- Modification de la requête SQL : suppression de la colonne lettretype + ajout
-- des colonnes date_envoi_signature, date_retour_signature, date_envoi_rar,
-- date_retour_rar + alias de date_evenement corrigé.
--
UPDATE om_sousetat SET
entete_orientation = '0|0|0|0|0|0',
entetecolone_bordure = 'TLB|LTBR|LTBR|LTBR|LTBR|LTBR',
entetecolone_align = 'C|C|C|C|C|C',
cellule_largeur = '70|25|25|25|25|25',
cellule_bordure_un = 'LTBR|LTBR|LTBR|LTBR|LTBR|LTBR',
cellule_bordure = 'LTBR|LTBR|LTBR|LTBR|LTBR|LTBR',
cellule_align = 'L|C|C|C|C|C',
cellule_bordure_total = 'TBL|TBLR|TBLR|TBLR|TBLR|TBLR',
cellule_align_total = 'L|C|C|C|C|C',
cellule_bordure_moyenne = 'BTL|BTLR|BTLR|BTLR|BTLR|BTLR',
cellule_align_moyenne = 'L|C|C|C|C|C',
cellule_bordure_nbr = 'TBL|TBLR|TBLR|TBLR|TBLR|TBLR',
cellule_align_nbr = 'L|C|C|C|C|C',
cellule_numerique = '999|999|999|999|999|999',
cellule_total = '0|0|0|0|0|0',
cellule_moyenne = '0|0|0|0|0|0',
cellule_compteur = '0|0|0|0|0|0',
om_sql = '
SELECT evenement.libelle as evenement,
    to_char(date_evenement,''DD/MM/YYYY'') as date_evenement,
    to_char(date_envoi_signature,''DD/MM/YYYY'') as date_envoi_signature,
    to_char(date_retour_signature,''DD/MM/YYYY'') as date_retour_signature,
    to_char(date_envoi_rar,''DD/MM/YYYY'') as date_envoi_rar,
    to_char(date_retour_rar,''DD/MM/YYYY'') as date_retour_rar
    from &DB_PREFIXEinstruction
    inner join &DB_PREFIXEevenement on evenement.evenement = instruction.evenement
    where dossier = ''&idx''
    and evenement.retour IS FALSE
    order by instruction.date_evenement asc, instruction asc
' WHERE id = 'instruction';

--
-- Modification des sous-état consultation, bordereau_controle_legalite,
-- bordereau_courriers_signature_maire, bordereau_avis_maire_prefet
-- et bordereau_decisions
-- Suppression de la dernière ligne faisant état du nombre de ligne du tableau
--
UPDATE om_sousetat SET cellule_compteur = '0|0|0'
WHERE id = 'consultation';
UPDATE om_sousetat SET cellule_compteur = '0|0|0|0'
WHERE id = 'bordereau_controle_legalite';
UPDATE om_sousetat SET cellule_compteur = '0|0|0|0'
WHERE id = 'bordereau_courriers_signature_maire';
UPDATE om_sousetat SET cellule_compteur = '0|0|0|0'
WHERE id = 'bordereau_avis_maire_prefet';
UPDATE om_sousetat SET cellule_compteur = '0|0|0|0'
WHERE id = 'bordereau_decisions';

---
--- Ajout du code insee en paramètre
---
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'), 'insee', '13055', 1
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'insee'
    )
LIMIT 1;

UPDATE donnees_techniques SET cerfa = 1 WHERE lot IS NULL;
UPDATE donnees_techniques SET cerfa = 2 WHERE lot IS NOT NULL;

-- Ajout des droits de visualisation et de téléchargement
-- du pdf associé à l'avis d'un service (objet consultation)
-- CELLULE SUIVI
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_fichier_voir', 
(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE NOT EXISTS (
    SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_voir' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
);
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_fichier_telecharger', 
(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE NOT EXISTS (
    SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
);
-- SERVICE CONSULTÉ
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_fichier_voir', 
(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
WHERE NOT EXISTS (
    SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_voir' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
);
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_fichier_telecharger', 
(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
WHERE NOT EXISTS (
    SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
);
-- SERVICE CONSULTÉ INTERNE
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_fichier_voir', 
(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE NOT EXISTS (
    SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_voir' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
);
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_fichier_telecharger', 
(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE NOT EXISTS (
    SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
);

--
-- Ajout du droit pour accéder au plan sig 
--
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'dossier_instruction_localiser-sig-externe', 
(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
WHERE NOT EXISTS (
    SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_localiser-sig-externe' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
);
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'dossier_instruction_localiser-sig-externe', 
(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE NOT EXISTS (
    SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_localiser-sig-externe' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
);

-- Mise à jour du paramétrage pour qu'il corresponde à ce qui est attendu fonctionnellement
UPDATE evenement
SET type = 'arrete'
WHERE evenement_retour_signature IN (
    SELECT evenement
    FROM evenement
    WHERE retour IS TRUE AND type = 'arrete');

UPDATE evenement
SET type = null
WHERE retour IS TRUE AND type = 'arrete';

UPDATE evenement
SET type = 'majoration_delai'
WHERE evenement_retour_ar IN (
    SELECT evenement
    FROM evenement
    WHERE retour IS TRUE AND type = 'majoration_delai');

UPDATE evenement
SET type = null
WHERE retour IS TRUE AND type = 'majoration_delai';
