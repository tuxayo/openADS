--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.3.0-fix
--
-- @package openfoncier
-- @version SVN : $Id: v3.3.0-fix.sql 1804 2013-04-30 14:45:17Z fmichon $
--------------------------------------------------------------------------------

-- Suppression des champs co_finan* des tables cerfa et donnee_techniques
ALTER TABLE cerfa DROP COLUMN co_finan1_id;
ALTER TABLE cerfa DROP COLUMN co_finan1_nb;
ALTER TABLE cerfa DROP COLUMN co_finan2_id;
ALTER TABLE cerfa DROP COLUMN co_finan2_nb;
ALTER TABLE cerfa DROP COLUMN co_finan3_id;
ALTER TABLE cerfa DROP COLUMN co_finan3_nb;
ALTER TABLE cerfa DROP COLUMN co_finan4_id;
ALTER TABLE cerfa DROP COLUMN co_finan4_nb;
ALTER TABLE cerfa DROP COLUMN co_finan5_id;
ALTER TABLE cerfa DROP COLUMN co_finan5_nb;

ALTER TABLE donnees_techniques DROP COLUMN co_finan1_id;
ALTER TABLE donnees_techniques DROP COLUMN co_finan1_nb;
ALTER TABLE donnees_techniques DROP COLUMN co_finan2_id;
ALTER TABLE donnees_techniques DROP COLUMN co_finan2_nb;
ALTER TABLE donnees_techniques DROP COLUMN co_finan3_id;
ALTER TABLE donnees_techniques DROP COLUMN co_finan3_nb;
ALTER TABLE donnees_techniques DROP COLUMN co_finan4_id;
ALTER TABLE donnees_techniques DROP COLUMN co_finan4_nb;
ALTER TABLE donnees_techniques DROP COLUMN co_finan5_id;
ALTER TABLE donnees_techniques DROP COLUMN co_finan5_nb;

-- Mise à jour des contraintes de la table demande_type
UPDATE demande_type SET contraintes = '' WHERE code = 'DI';
UPDATE demande_type SET contraintes = 'sans_recup' WHERE code = 'DT';
UPDATE demande_type SET contraintes = 'avec_recup' WHERE code = 'DM';
UPDATE demande_type SET contraintes = 'sans_recup' WHERE code = 'DAACT';
UPDATE demande_type SET contraintes = 'sans_recup' WHERE code = 'DOC';

-- Modification de la table evenement
ALTER TABLE ONLY evenement ADD COLUMN evenement_retour_signature integer;
ALTER TABLE ONLY evenement
    ADD CONSTRAINT evenement_evenement_retour_signature_fkey FOREIGN KEY (evenement_retour_signature) REFERENCES evenement(evenement);

-- gestion du champ date_dernier_depot
ALTER TABLE ONLY dossier ADD COLUMN date_dernier_depot date;
UPDATE dossier SET  date_dernier_depot=date_depot;
ALTER TABLE ONLY dossier ALTER date_dernier_depot SET NOT NULL;
ALTER TABLE ONLY action ADD COLUMN regle_date_dernier_depot character varying(60);
ALTER TABLE ONLY instruction ADD COLUMN archive_date_dernier_depot date;

-- liste des états de DA sur lesquelles le type de demande sera autorisés

-- Suppression de l'état "En cour de création"
UPDATE dossier_autorisation SET etat_dossier_autorisation=7;
DELETE FROM etat_dossier_autorisation WHERE etat_dossier_autorisation=1;

CREATE TABLE lien_demande_type_etat_dossier_autorisation (
    lien_demande_type_etat_dossier_autorisation integer NOT NULL,
    demande_type integer NOT NULL,
    etat_dossier_autorisation  integer NOT NULL
);
ALTER TABLE ONLY lien_demande_type_etat_dossier_autorisation
    ADD CONSTRAINT lien_demande_type_etat_dossier_autorisation_pkey
    PRIMARY KEY (lien_demande_type_etat_dossier_autorisation);

ALTER TABLE ONLY lien_demande_type_etat_dossier_autorisation
    ADD CONSTRAINT lien_demande_type_etat_dossier_autorisation_demande_type_fk 
    FOREIGN KEY (demande_type) REFERENCES demande_type(demande_type);

ALTER TABLE ONLY lien_demande_type_etat_dossier_autorisation
    ADD CONSTRAINT lien_demande_type_etat_dossier_autorisation_etat_dossier_autorisation_fk 
    FOREIGN KEY (etat_dossier_autorisation) REFERENCES etat_dossier_autorisation(etat_dossier_autorisation);

-- Création de la séquence
CREATE SEQUENCE lien_demande_type_etat_dossier_autorisation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE demande_type DROP etats_dossier_autorisation_autorises;