--
-- START - Mise à jour des sous-états bordereaux de suivi - Décembre 2015
--

UPDATE om_sousetat
SET om_sql = '
SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' \ '',
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEgroupe
    ON
        dossier_autorisation_type.groupe = groupe.groupe
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    evenement.retour IS FALSE
    AND LOWER(autorite_competente.code) = ''com''
    AND instruction.date_envoi_controle_legalite >= ''&date_bordereau_debut''
    AND instruction.date_envoi_controle_legalite <= ''&date_bordereau_fin''
    AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    AND (LOWER(groupe.code) = ''ads'' OR LOWER(groupe.code) = ''cs'')
    AND dossier.om_collectivite IN (&collectivite)

ORDER BY
    dossier.dossier'
WHERE id = 'bordereau_controle_legalite';

UPDATE om_sousetat
SET om_sql = '
SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' \ '',
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEgroupe
    ON
        dossier_autorisation_type.groupe = groupe.groupe
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    evenement.retour IS FALSE
    AND (LOWER(autorite_competente.code) = ''etatmaire''
      OR LOWER(autorite_competente.code) = ''etat'')
    AND instruction.date_envoi_rar >= ''&date_bordereau_debut''
    AND instruction.date_envoi_rar <= ''&date_bordereau_fin''
    AND LOWER(groupe.code) = ''ads''
    AND lien_dossier_demandeur.petitionnaire_principal IS TRUE 
    AND instruction.evenement = ''&id_evenement_bordereau_avis_maire_prefet''
    AND dossier.om_collectivite IN (&collectivite)
ORDER BY
    dossier.dossier'
WHERE id = 'bordereau_avis_maire_prefet';

UPDATE om_sousetat
SET om_sql = '
SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' \ '',
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEgroupe
    ON
        dossier_autorisation_type.groupe = groupe.groupe
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
WHERE
    evenement.retour IS FALSE
    AND LOWER(evenement.type) = ''arrete''
    AND instruction.date_envoi_signature >= ''&date_bordereau_debut''
    AND instruction.date_envoi_signature <= ''&date_bordereau_fin''
    AND LOWER(groupe.code) = ''ads''
    AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    AND dossier.om_collectivite IN (&collectivite)
ORDER BY
    dossier.dossier'
WHERE id = 'bordereau_decisions';

UPDATE om_sousetat
SET om_sql = 'SELECT
dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' \ '',
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEgroupe
    ON
        dossier_autorisation_type.groupe = groupe.groupe
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur.demandeur 
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    evenement.retour IS FALSE
    AND instruction.date_envoi_signature >= ''&date_bordereau_debut''
    AND instruction.date_envoi_signature <= ''&date_bordereau_fin''
    AND LOWER(groupe.code) = ''ads''
    AND (evenement.type <> ''arrete'' OR evenement.type IS NULL)
    AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    AND dossier.om_collectivite IN (&collectivite)
ORDER BY
    dossier.dossier'
WHERE id = 'bordereau_courriers_signature_maire';

--
-- END - Mise à jour des sous-états bordereaux de suivi - Novembre 2015
--

-- 
-- Start - Définition de l'autorité compétente Mairie par défaut lors de la création d'un DI
-- 

UPDATE evenement 
SET autorite_competente = 1 
WHERE libelle = 'Notification du delai legal maison individuelle'; 

UPDATE action 
SET regle_autorite_competente = 'autorite_competente' 
WHERE action = 'initialisation'; 

--
-- END - Définition de l'autorité compétente Mairie par défaut lors de la création d'un DI
--

--
-- START / #288 — Renommer la table sig_elyx
--

-- Renommer la table
ALTER TABLE sig_elyx RENAME TO dossier_geolocalisation;

-- Supprime la contrainte de clé primaire
ALTER TABLE dossier_geolocalisation DROP CONSTRAINT sig_elyx_pkey;

-- Renomme la colonne sig_elyx
ALTER TABLE dossier_geolocalisation RENAME COLUMN sig_elyx TO dossier_geolocalisation;

-- La colonne renommer devient la clé primaire
ALTER TABLE ONLY dossier_geolocalisation
    ADD CONSTRAINT dossier_geolocalisation_pkey PRIMARY KEY (dossier_geolocalisation);

-- Modifie le commentaire sur la table
COMMENT ON TABLE dossier_geolocalisation IS 'État de chaque traitement de géolocalisation d''un dossier';

-- Renommer la séquence
ALTER SEQUENCE sig_elyx_seq RENAME TO dossier_geolocalisation_seq;

-- Lier la séquence sur la bonne table
ALTER SEQUENCE dossier_geolocalisation_seq OWNED BY dossier_geolocalisation.dossier_geolocalisation;

--
-- END / #288 — Renommer la table sig_elyx
--

--
-- START / Ajout de droits pour la récupération de l'adresse à partir de la ref cadastrale
-- 

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE') 
WHERE 
    NOT EXISTS (  
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_recuperer_adresse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE') 
    ); 

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT') 
WHERE 
    NOT EXISTS (  
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_recuperer_adresse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT') 
    );

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL') 
WHERE 
    NOT EXISTS (  
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_recuperer_adresse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL') 
    );

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI') 
WHERE 
    NOT EXISTS (  
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_recuperer_adresse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI') 
    );

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE') 
WHERE 
    NOT EXISTS (  
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_recuperer_adresse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE') 
    );

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_nouveau_dossier_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE') 
WHERE 
    NOT EXISTS (  
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_nouveau_dossier_recuperer_adresse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE') 
    ); 

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_nouveau_dossier_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT') 
WHERE 
    NOT EXISTS (  
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_nouveau_dossier_recuperer_adresse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT') 
    );

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_nouveau_dossier_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL') 
WHERE 
    NOT EXISTS (  
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_nouveau_dossier_recuperer_adresse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL') 
    );

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_nouveau_dossier_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI') 
WHERE 
    NOT EXISTS (  
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_nouveau_dossier_recuperer_adresse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI') 
    );

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_nouveau_dossier_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE') 
WHERE 
    NOT EXISTS (  
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_nouveau_dossier_recuperer_adresse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE') 
    );

--
-- END / Ajout de droits pour la récupération de l'adresse à partir de la ref cadastrale
-- 

--
-- START / #162 — Modification du dossier d'instruction par l'instructeur qui
-- ajoute la demande
--

-- Suppression du droit qui va être renommé
--
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_instruction_finaliser_guichet_unique' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI');
--
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_instruction_finaliser_guichet_unique' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE');

-- Ajout du droit de régénérer le recepissé
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
--
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );

--
-- END / #162 — Modification du dossier d'instruction par l'instructeur qui
-- ajoute la demande
--