--
-- START / [#8654] Ajout de champ de fusion pour les DIA
--

-- om_requete
UPDATE om_requete SET requete = '
SELECT

    --Données générales de l''événement d''instruction
    
    instruction.complement_om_html as complement_instruction,
    instruction.complement_om_html as complement1_instruction,
    instruction.complement2_om_html as complement2_instruction,
    instruction.complement3_om_html as complement3_instruction,
    instruction.complement4_om_html as complement4_instruction,
    instruction.code_barres as code_barres_instruction,
    to_char(instruction.date_evenement,''DD/MM/YYYY'') as date_evenement_instruction, 
    om_lettretype.libelle as libelle_om_lettretype,
    instruction.archive_delai as archive_delai_instruction,

    --Données générales du dossier d''instruction

    dossier.dossier_libelle as libelle_dossier,
    dossier.dossier as code_barres_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    to_char(dossier_autorisation.date_decision, ''DD/MM/YYYY'') as date_decision_da,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    dossier.terrain_superficie as terrain_superficie_dossier,
    quartier.libelle as libelle_quartier,

    avis_decision.libelle as libelle_avis_decision,

    dossier_autorisation.cle_acces_citoyen,

    --Données générales du paramétrage de l''événement

    evenement.libelle as libelle_evenement,
    evenement.etat as etat_evenement,
    evenement.delai as delai_evenement,
    evenement.accord_tacite as accord_tacite_evenement,
    evenement.delai_notification as delai_notification_evenement,
    evenement.avis_decision as avis_decision_evenement,
    evenement.autorite_competente as autorite_competente_evenement,

    --Coordonnées de l''instructeur

    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    --Adresse du terrain du dossier d''instruction

    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    CASE 
        WHEN dossier.terrain_adresse_bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', dossier.terrain_adresse_bp)
    END as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    CASE 
        WHEN dossier.terrain_adresse_cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', dossier.terrain_adresse_cedex)
    END as terrain_adresse_cedex_dossier,

    arrondissement.libelle as libelle_arrondissement,

    --Taxe d''aménagement du dossier d''instruction

    CASE
        WHEN tax_secteur = 1 THEN taxe_amenagement.tx_comm_secteur_1
        WHEN tax_secteur = 2 THEN taxe_amenagement.tx_comm_secteur_2
        WHEN tax_secteur = 3 THEN taxe_amenagement.tx_comm_secteur_3
        WHEN tax_secteur = 4 THEN taxe_amenagement.tx_comm_secteur_4
        WHEN tax_secteur = 5 THEN taxe_amenagement.tx_comm_secteur_5
        WHEN tax_secteur = 6 THEN taxe_amenagement.tx_comm_secteur_6
        WHEN tax_secteur = 7 THEN taxe_amenagement.tx_comm_secteur_7
        WHEN tax_secteur = 8 THEN taxe_amenagement.tx_comm_secteur_8
        WHEN tax_secteur = 9 THEN taxe_amenagement.tx_comm_secteur_9
        WHEN tax_secteur = 10 THEN taxe_amenagement.tx_comm_secteur_10
        WHEN tax_secteur = 11 THEN taxe_amenagement.tx_comm_secteur_11
        WHEN tax_secteur = 12 THEN taxe_amenagement.tx_comm_secteur_12
        WHEN tax_secteur = 13 THEN taxe_amenagement.tx_comm_secteur_13
        WHEN tax_secteur = 14 THEN taxe_amenagement.tx_comm_secteur_14
        WHEN tax_secteur = 15 THEN taxe_amenagement.tx_comm_secteur_15
        WHEN tax_secteur = 16 THEN taxe_amenagement.tx_comm_secteur_16
        WHEN tax_secteur = 17 THEN taxe_amenagement.tx_comm_secteur_17
        WHEN tax_secteur = 18 THEN taxe_amenagement.tx_comm_secteur_18
        WHEN tax_secteur = 19 THEN taxe_amenagement.tx_comm_secteur_19
        WHEN tax_secteur = 20 THEN taxe_amenagement.tx_comm_secteur_20
    END as tax_taux_secteur,
    dossier.tax_secteur as tax_numero_secteur,
    dossier.tax_mtn_part_commu as tax_montant_part_communale,
    dossier.tax_mtn_part_depart as tax_montant_part_departementale,
    dossier.tax_mtn_part_reg as tax_montant_part_regionale,
    dossier.tax_mtn_total as tax_montant_total,

    --Coordonnées du pétitionnaire principal

    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
    END as nom_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_principal_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_nom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_denomination
    END as denomination_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire principal initial

    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_initial_civilite.libelle, petitionnaire_principal_initial.particulier_nom, petitionnaire_principal_initial.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_principal_initial.personne_morale_nom IS NOT NULL OR petitionnaire_principal_initial.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_initial.personne_morale_raison_sociale, petitionnaire_principal_initial.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_initial_civilite.libelle, petitionnaire_principal_initial.personne_morale_nom, petitionnaire_principal_initial.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_principal_initial.personne_morale_raison_sociale, '' '', petitionnaire_principal_initial.personne_morale_denomination))
            END
    END as nom_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier'' OR petitionnaire_principal_initial.personne_morale_nom IS NOT NULL OR petitionnaire_principal_initial.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_principal_initial_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN petitionnaire_principal_initial.particulier_nom
        ELSE
            CASE WHEN petitionnaire_principal_initial.personne_morale_nom IS NOT NULL
                THEN petitionnaire_principal_initial.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN petitionnaire_principal_initial.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_principal_initial.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal_initial.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal_initial.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal_initial.personne_morale_denomination
    END as denomination_petitionnaire_principal_initial,
    petitionnaire_principal_initial.numero as numero_petitionnaire_principal_initial,
    petitionnaire_principal_initial.voie as voie_petitionnaire_principal_initial,
    petitionnaire_principal_initial.complement as complement_petitionnaire_principal_initial,
    petitionnaire_principal_initial.lieu_dit as lieu_dit_petitionnaire_principal_initial,
    CASE 
        WHEN petitionnaire_principal_initial.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal_initial.bp)
    END as bp_petitionnaire_principal_initial,
    petitionnaire_principal_initial.code_postal as code_postal_petitionnaire_principal_initial,
    petitionnaire_principal_initial.localite as localite_petitionnaire_principal_initial,
    CASE 
        WHEN petitionnaire_principal_initial.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal_initial.cedex)
    END as cedex_petitionnaire_principal_initial,
    petitionnaire_principal_initial.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1

    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_1_civilite.libelle, petitionnaire_1.particulier_nom, petitionnaire_1.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_1.personne_morale_raison_sociale, petitionnaire_1.personne_morale_denomination, ''représenté(e) par'', petitionnaire_1_civilite.libelle, petitionnaire_1.personne_morale_nom, petitionnaire_1.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
            END
    END as nom_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier'' OR petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_1_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_nom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL
                THEN petitionnaire_1.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_1.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_denomination
    END as denomination_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2

    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_2_civilite.libelle, petitionnaire_2.particulier_nom, petitionnaire_2.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_2.personne_morale_raison_sociale, petitionnaire_2.personne_morale_denomination, ''représenté(e) par'', petitionnaire_2_civilite.libelle, petitionnaire_2.personne_morale_nom, petitionnaire_2.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
            END
    END as nom_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier'' OR petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_2_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_nom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL
                THEN petitionnaire_2.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_2.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_denomination
    END as denomination_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3

    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_3_civilite.libelle, petitionnaire_3.particulier_nom, petitionnaire_3.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_3.personne_morale_raison_sociale, petitionnaire_3.personne_morale_denomination, ''représenté(e) par'', petitionnaire_3_civilite.libelle, petitionnaire_3.personne_morale_nom, petitionnaire_3.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
            END
    END as nom_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier'' OR petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_3_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_nom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL
                THEN petitionnaire_3.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_3.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_denomination
    END as denomination_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4

    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_4_civilite.libelle, petitionnaire_4.particulier_nom, petitionnaire_4.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_4.personne_morale_raison_sociale, petitionnaire_4.personne_morale_denomination, ''représenté(e) par'', petitionnaire_4_civilite.libelle, petitionnaire_4.personne_morale_nom, petitionnaire_4.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
            END
    END as nom_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier'' OR petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_4_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_nom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL
                THEN petitionnaire_4.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_4.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_denomination
    END as denomination_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5

    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_5_civilite.libelle, petitionnaire_5.particulier_nom, petitionnaire_5.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_5.personne_morale_raison_sociale, petitionnaire_5.personne_morale_denomination, ''représenté(e) par'', petitionnaire_5_civilite.libelle, petitionnaire_5.personne_morale_nom, petitionnaire_5.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
            END
    END as nom_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier'' OR petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_5_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_nom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL
                THEN petitionnaire_5.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_5.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_denomination
    END as denomination_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,

    --Coordonnées du délégataire

    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', delegataire_civilite.libelle, delegataire.particulier_nom, delegataire.particulier_prenom))
        ELSE
            CASE WHEN delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', delegataire.personne_morale_raison_sociale, delegataire.personne_morale_denomination, ''représenté(e) par'', delegataire_civilite.libelle, delegataire.personne_morale_nom, delegataire.personne_morale_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_delegataire,
    CASE WHEN delegataire.qualite=''particulier'' OR delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
        THEN delegataire_civilite.libelle
        ELSE ''''
    END as civilite_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN delegataire.particulier_nom
        ELSE
            CASE WHEN delegataire.personne_morale_nom IS NOT NULL
                THEN delegataire.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN delegataire.particulier_prenom
        ELSE
            CASE WHEN delegataire.personne_morale_prenom IS NOT NULL
                THEN delegataire.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN ''''
        ELSE delegataire.personne_morale_raison_sociale
    END as raison_sociale_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN ''''
        ELSE delegataire.personne_morale_denomination
    END as denomination_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complement_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    CASE 
        WHEN delegataire.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', delegataire.bp)
    END as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    CASE 
        WHEN delegataire.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', delegataire.cedex)
    END as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT_WS('' '', delegataire_civilite.libelle, delegataire.particulier_nom, delegataire.particulier_prenom))
                ELSE
                    CASE WHEN delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                        THEN TRIM(CONCAT_WS('' '', delegataire.personne_morale_raison_sociale, delegataire.personne_morale_denomination, ''représenté(e) par'', delegataire_civilite.libelle, delegataire.personne_morale_nom, delegataire.personne_morale_prenom))
                        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
                    END
            END
    END as nom_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal_civilite.libelle
                ELSE ''''
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier'' OR delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                THEN delegataire_civilite.libelle
                ELSE ''''
            END
    END as civilite_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN petitionnaire_principal.particulier_nom
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                        THEN petitionnaire_principal.personne_morale_nom
                        ELSE ''''
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN delegataire.particulier_nom
                ELSE
                    CASE WHEN delegataire.personne_morale_nom IS NOT NULL
                        THEN delegataire.personne_morale_nom
                        ELSE ''''
                    END
            END
    END as nom_particulier_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN petitionnaire_principal.particulier_prenom
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                        THEN petitionnaire_principal.personne_morale_prenom
                        ELSE ''''
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN delegataire.particulier_prenom
                ELSE
                    CASE WHEN delegataire.personne_morale_prenom IS NOT NULL
                        THEN delegataire.personne_morale_prenom
                        ELSE ''''
                    END
            END
    END as prenom_particulier_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN ''''
                ELSE petitionnaire_principal.personne_morale_raison_sociale
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN ''''
                ELSE delegataire.personne_morale_raison_sociale
            END
    END as raison_sociale_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN ''''
                ELSE petitionnaire_principal.personne_morale_denomination
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN ''''
                ELSE delegataire.personne_morale_denomination
            END
    END as denomination_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', petitionnaire_principal.bp)
            END
        ELSE 
            CASE 
                WHEN delegataire.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', delegataire.bp)
            END
    END as bp_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
            END 
        ELSE 
            CASE 
                WHEN delegataire.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', delegataire.cedex)
            END 
    END as cedex_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    --Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_complet, ''DD/MM/YYYY'') as date_completude,
    to_char(dossier.date_dernier_depot, ''DD/MM/YYYY'') as date_dernier_depot,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    CASE WHEN dossier.incomplet_notifie IS TRUE AND dossier.incompletude IS TRUE 
        THEN to_char(dossier.date_limite_incompletude ,''DD/MM/YYYY'') 
        ELSE to_char(dossier.date_limite ,''DD/MM/YYYY'')
    END as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    to_char(dossier.date_notification_delai,''DD/MM/YYYY'') as date_notification_delai_dossier,
    
    --Noms des signataires
    TRIM(CONCAT(signataire_civilite.libelle, '' '',signataire_arrete.prenom, '' '', signataire_arrete.nom)) as arrete_signataire,
    TRIM(CONCAT(signataire_arrete.qualite, '' '', signataire_arrete.signature)) as signature_signataire,
    division.chef as chef_division,
    direction.chef as chef_direction,
    direction.libelle as libelle_direction,
    direction.description as description_direction,
    
    --Données générales des données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.ope_proj_desc as ope_proj_desc_donnees_techniques,
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_cstr_shon_tot
        ELSE donnees_techniques.su_cstr_shon_tot
    END as su_cstr_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_demo_shon_tot
        ELSE donnees_techniques.su_demo_shon_tot
    END as su_demo_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN
            REGEXP_REPLACE(CONCAT(
                CASE WHEN donnees_techniques.su2_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation agricole - '', donnees_techniques.su2_cstr_shon1, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation forestière - '', donnees_techniques.su2_cstr_shon2, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Logement - '', donnees_techniques.su2_cstr_shon3, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement - '', donnees_techniques.su2_cstr_shon4, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Artisanat et commerce de détail - '', donnees_techniques.su2_cstr_shon5, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Restauration - '', donnees_techniques.su2_cstr_shon6, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Commerce de gros - '', donnees_techniques.su2_cstr_shon7, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Activités de services où s''''effectue l''''accueil d''''une clientèle - '', donnees_techniques.su2_cstr_shon8, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement hôtelier et touristique - '', donnees_techniques.su2_cstr_shon9, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon10 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Cinéma - '', donnees_techniques.su2_cstr_shon10, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon11 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux et bureaux accueillant du public des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon11, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon12 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux techniques et industriels des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon12, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon13 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Établissements d''''enseignement, de santé et d''''action sociale - '', donnees_techniques.su2_cstr_shon13, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon14 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Salles d''''art et de spectacles - '', donnees_techniques.su2_cstr_shon14, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon15 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Équipements sportifs - '', donnees_techniques.su2_cstr_shon15, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon16 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Autres équipements recevant du public - '', donnees_techniques.su2_cstr_shon16, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon17 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Industrie - '', donnees_techniques.su2_cstr_shon17, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon18 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Entrepôt - '', donnees_techniques.su2_cstr_shon18, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon19 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Bureau - '', donnees_techniques.su2_cstr_shon19, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon20 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Centre de congrès et d''''exposition - '', donnees_techniques.su2_cstr_shon20, '' m²'')
                END
            ), '' / $'', '''')
        ELSE
            REGEXP_REPLACE(CONCAT(
                CASE
                    WHEN donnees_techniques.su_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
                END, 
                CASE
                    WHEN donnees_techniques.su_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
                END
            ), '' / $'', '''')
    END as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_tot_shon_tot
        ELSE donnees_techniques.su_tot_shon_tot
    END as su_tot_shon_tot_donnees_techniques,

    -- Données techniques pour les AT
    CONCAT_WS('', '',
        CASE WHEN donnees_techniques.erp_cstr_neuve IS TRUE THEN ''construction neuve'' END,
        CASE WHEN donnees_techniques.erp_trvx_acc IS TRUE THEN ''travaux de mise en conformité totale aux règles d’accessibilité'' END,
        CASE WHEN donnees_techniques.erp_extension IS TRUE THEN ''extension'' END,
        CASE WHEN donnees_techniques.erp_rehab IS TRUE THEN ''réhabilitation'' END,
        CASE WHEN donnees_techniques.erp_trvx_am IS TRUE THEN ''travaux d’aménagement (remplacement de revêtements, rénovation électrique, création d’une rampe, par exemple)'' END,
        CASE WHEN donnees_techniques.erp_vol_nouv_exist IS TRUE THEN ''création de volumes nouveaux dans des volumes existants (modification du cloisonnement, par exemple)'' END
    ) as at_type_travaux,
    donnees_techniques.erp_public_eff_tot as at_effectif_public_total,
    CONCAT_WS('' - '', erp_categorie.libelle, erp_categorie.description) as at_categorie_etablissement,
    CONCAT_WS('' - '', erp_type.libelle, erp_type.description) as at_type_etablissement,

    -- Données techniques pour une DIA
    COALESCE (donnees_techniques.dia_mod_cess_prix_vente,'''') AS dia_mod_cess_prix_vente,
    COALESCE (donnees_techniques.dia_acquereur_nom_prenom,'''') AS dia_acquereur_nom_prenom,
    CONCAT(
        COALESCE(donnees_techniques.dia_acquereur_adr_num_voie || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_ext || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_type_voie || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_nom_voie || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_lieu_dit_bp || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_cp || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_localite || '' '','''')
        ) as dia_acquereur_adresse,

    --Bordereau d''envoi au maire
    CASE
        WHEN evenement.type = ''arrete''
        THEN CONCAT(''transmission d''''une proposition de décision sur '', evenement.libelle)
        ELSE CONCAT(''transmission d''''un courrier d''''instruction sur '', evenement.libelle)
    END as objet_bordereau_envoi_maire
    
FROM
    &DB_PREFIXEinstruction
LEFT JOIN
    &DB_PREFIXEsignataire_arrete
    ON instruction.signataire_arrete = signataire_arrete.signataire_arrete
LEFT JOIN
    &DB_PREFIXEcivilite as signataire_civilite
    ON signataire_arrete.civilite = signataire_civilite.civilite
LEFT JOIN
    &DB_PREFIXEom_lettretype
    ON instruction.lettretype = om_lettretype.id and om_lettretype.actif IS TRUE
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        instruction.dossier=dossier.dossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        instruction.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite as petitionnaire_principal_civilite
    ON
        petitionnaire_principal.particulier_civilite = petitionnaire_principal_civilite.civilite OR petitionnaire_principal.personne_morale_civilite = petitionnaire_principal_civilite.civilite
LEFT JOIN
    &DB_PREFIXElien_dossier_autorisation_demandeur
    ON
        dossier.dossier_autorisation = lien_dossier_autorisation_demandeur.dossier_autorisation AND lien_dossier_autorisation_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal_initial
    ON
        lien_dossier_autorisation_demandeur.demandeur = petitionnaire_principal_initial.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite as petitionnaire_principal_initial_civilite
    ON
        petitionnaire_principal_initial.particulier_civilite = petitionnaire_principal_initial_civilite.civilite OR petitionnaire_principal_initial.personne_morale_civilite = petitionnaire_principal_initial_civilite.civilite
LEFT JOIN
    (
    &DB_PREFIXElien_dossier_demandeur AS lien_dossier_delegataire
        JOIN &DB_PREFIXEdemandeur as delegataire
        ON
            lien_dossier_delegataire.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
    )
    ON
        instruction.dossier = lien_dossier_delegataire.dossier AND lien_dossier_delegataire.petitionnaire_principal IS FALSE
LEFT JOIN
    &DB_PREFIXEcivilite as delegataire_civilite
    ON
        delegataire.particulier_civilite = delegataire_civilite.civilite OR delegataire.personne_morale_civilite = delegataire_civilite.civilite
LEFT JOIN (
    SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
    FROM &DB_PREFIXElien_dossier_demandeur
    LEFT JOIN &DB_PREFIXEdossier
        ON lien_dossier_demandeur.dossier=dossier.dossier 
        AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
    LEFT JOIN &DB_PREFIXEinstruction
        ON instruction.dossier = dossier.dossier
    WHERE instruction.instruction = &idx
    GROUP BY lien_dossier_demandeur.dossier
) as sub_petitionnaire_autre
ON instruction.dossier = sub_petitionnaire_autre.dossier
LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1] AND petitionnaire_1.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_1_civilite
    ON
        petitionnaire_1.particulier_civilite = petitionnaire_1_civilite.civilite OR petitionnaire_1.personne_morale_civilite = petitionnaire_1_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2] AND petitionnaire_2.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_2_civilite
    ON
        petitionnaire_2.particulier_civilite = petitionnaire_2_civilite.civilite OR petitionnaire_2.personne_morale_civilite = petitionnaire_2_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3] AND petitionnaire_3.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_3_civilite
    ON
        petitionnaire_3.particulier_civilite = petitionnaire_3_civilite.civilite OR petitionnaire_3.personne_morale_civilite = petitionnaire_3_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4] AND petitionnaire_4.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_4_civilite
    ON
        petitionnaire_4.particulier_civilite = petitionnaire_4_civilite.civilite OR petitionnaire_4.personne_morale_civilite = petitionnaire_4_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5] AND petitionnaire_5.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_5_civilite
    ON
        petitionnaire_5.particulier_civilite = petitionnaire_5_civilite.civilite OR petitionnaire_5.personne_morale_civilite = petitionnaire_5_civilite.civilite
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN 
    &DB_PREFIXEdirection
    ON division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN 
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN 
    &DB_PREFIXEquartier
    ON
        dossier.quartier = quartier.quartier
LEFT JOIN
    &DB_PREFIXEtaxe_amenagement
    ON
        dossier.om_collectivite = taxe_amenagement.om_collectivite
LEFT JOIN
    &DB_PREFIXEerp_categorie
    ON
        donnees_techniques.erp_class_cat = erp_categorie.erp_categorie
LEFT JOIN
    &DB_PREFIXEerp_type
    ON
        donnees_techniques.erp_class_type = erp_type.erp_type
WHERE instruction.instruction = &idx
',
merge_fields = '
--Données générales de l''événement d''instruction
[complement1_instruction]
[complement2_instruction]
[complement3_instruction]
[complement4_instruction]
[code_barres_instruction]
[date_evenement_instruction]
[libelle_om_lettretype]
[archive_delai_instruction]

--Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]    [delai_dossier]    [terrain_references_cadastrales_dossier]
[terrain_superficie_dossier]
[libelle_quartier]
[libelle_da]

[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]

[libelle_avis_decision]

--Données générales du paramétrage de l''événement
[libelle_evenement]
[etat_evenement]
[delai_evenement]
[accord_tacite_evenement]
[delai_notification_evenement]
[avis_decision_evenement]
[autorite_competente_evenement]
[cle_acces_citoyen]

--Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

--Adresse du terrain du dossier d''instruction 
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]

[libelle_arrondissement]

--Taxe d''aménagement du dossier d''instruction
[tax_taux_secteur]
[tax_numero_secteur]
[tax_montant_part_communale]
[tax_montant_part_departementale]
[tax_montant_part_regionale]
[tax_montant_total]

--Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[civilite_petitionnaire_principal]
[nom_particulier_petitionnaire_principal]
[prenom_particulier_petitionnaire_principal]
[raison_sociale_petitionnaire_principal]
[denomination_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées du pétitionnaire principal initial
[nom_petitionnaire_principal_initial]
[civilite_petitionnaire_principal_initial]
[nom_particulier_petitionnaire_principal_initial]
[prenom_particulier_petitionnaire_principal_initial]
[raison_sociale_petitionnaire_principal_initial]
[denomination_petitionnaire_principal_initial]
[numero_petitionnaire_principal_initial]    [voie_petitionnaire_principal_initial]    [complement_petitionnaire_principal_initial]
[lieu_dit_petitionnaire_principal_initial]    [bp_petitionnaire_principal_initial]
[code_postal_petitionnaire_principal_initial]    [localite_petitionnaire_principal_initial]    [cedex_petitionnaire_principal_initial]
[pays_petitionnaire_principal_initial]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''à 5)
[civilite_petitionnaire_1](jusqu''à 5)
[nom_particulier_petitionnaire_1](jusqu''à 5)
[prenom_particulier_petitionnaire_1](jusqu''à 5)
[raison_sociale_petitionnaire_1](jusqu''à 5)
[denomination_petitionnaire_1](jusqu''à 5)
[numero_petitionnaire_1](jusqu''à 5)    [voie_petitionnaire_1](jusqu''à 5)    
[complement_petitionnaire_1](jusqu''à 5)
[lieu_dit_petitionnaire_1](jusqu''à 5)    [bp_petitionnaire_1](jusqu''à 5)
[code_postal_petitionnaire_1](jusqu''à 5)    [localite_petitionnaire_1](jusqu''à 5)    
[cedex_petitionnaire_1](jusqu''à 5)
[pays_petitionnaire_1](jusqu''à 5)

--Coordonnées du délégataire
[nom_delegataire]
[civilite_delegataire]
[nom_particulier_delegataire]Fsu_
[prenom_particulier_delegataire]
[raison_sociale_delegataire]
[denomination_delegataire]
[numero_delegataire]    [voie_delegataire]    [complement_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire]

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[civilite_correspondant]
[nom_particulier_correspondant]
[prenom_particulier_correspondant]
[raison_sociale_correspondant]
[denomination_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant]

--Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_completude]
[date_dernier_depot]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]
[date_notification_delai_dossier]
[date_decision_da]

--Noms des signataires
[arrete_signataire]
[signature_signataire]
[chef_division]
[chef_direction]
[libelle_direction]
[description_direction]

--Données générales des données techniques
[co_projet_desc_donnees_techniques]    [am_projet_desc_donnees_techniques]
[dm_projet_desc_donnees_techniques]    [ope_proj_desc_donnees_techniques]
[projet_desc_donnees_techniques]
[co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
-- Les données techniques suivantes concernent le tableau des surfaces
-- Elles récupèrent les valeurs du tableau composé des sous-destinations si au
-- moins une valeur de celui-ci est saisie
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]
[su_tot_shon_tot_donnees_techniques]

--Données techniques des AT
[at_type_travaux]
[at_effectif_public_total]
[at_categorie_etablissement]
[at_type_etablissement]

--Données techniques des DIA
[dia_mod_cess_prix_vente]
[dia_acquereur_nom_prenom]
[dia_acquereur_adresse]

--Bordereau envoi au maire
[objet_bordereau_envoi_maire]
'
WHERE code = 'instruction';

--
-- END / [#8654] Ajout de champ de fusion pour les DIA
--

--
-- START / [#8657] Ajout de champs de fusion des CERFA DOC et DAACT
--

UPDATE om_requete SET
requete = '
SELECT

    --Données générales de l''événement d''instruction
    
    instruction.complement_om_html as complement_instruction,
    instruction.complement_om_html as complement1_instruction,
    instruction.complement2_om_html as complement2_instruction,
    instruction.complement3_om_html as complement3_instruction,
    instruction.complement4_om_html as complement4_instruction,
    instruction.code_barres as code_barres_instruction,
    to_char(instruction.date_evenement,''DD/MM/YYYY'') as date_evenement_instruction, 
    om_lettretype.libelle as libelle_om_lettretype,
    instruction.archive_delai as archive_delai_instruction,

    --Données générales du dossier d''instruction

    dossier.dossier_libelle as libelle_dossier,
    dossier.dossier as code_barres_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    to_char(dossier_autorisation.date_decision, ''DD/MM/YYYY'') as date_decision_da,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    dossier.terrain_superficie as terrain_superficie_dossier,
    quartier.libelle as libelle_quartier,

    avis_decision.libelle as libelle_avis_decision,

    dossier_autorisation.cle_acces_citoyen,

    --Données générales du paramétrage de l''événement

    evenement.libelle as libelle_evenement,
    evenement.etat as etat_evenement,
    evenement.delai as delai_evenement,
    evenement.accord_tacite as accord_tacite_evenement,
    evenement.delai_notification as delai_notification_evenement,
    evenement.avis_decision as avis_decision_evenement,
    evenement.autorite_competente as autorite_competente_evenement,

    --Coordonnées de l''instructeur

    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    --Adresse du terrain du dossier d''instruction

    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    CASE 
        WHEN dossier.terrain_adresse_bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', dossier.terrain_adresse_bp)
    END as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    CASE 
        WHEN dossier.terrain_adresse_cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', dossier.terrain_adresse_cedex)
    END as terrain_adresse_cedex_dossier,

    arrondissement.libelle as libelle_arrondissement,

    --Taxe d''aménagement du dossier d''instruction

    CASE
        WHEN tax_secteur = 1 THEN taxe_amenagement.tx_comm_secteur_1
        WHEN tax_secteur = 2 THEN taxe_amenagement.tx_comm_secteur_2
        WHEN tax_secteur = 3 THEN taxe_amenagement.tx_comm_secteur_3
        WHEN tax_secteur = 4 THEN taxe_amenagement.tx_comm_secteur_4
        WHEN tax_secteur = 5 THEN taxe_amenagement.tx_comm_secteur_5
        WHEN tax_secteur = 6 THEN taxe_amenagement.tx_comm_secteur_6
        WHEN tax_secteur = 7 THEN taxe_amenagement.tx_comm_secteur_7
        WHEN tax_secteur = 8 THEN taxe_amenagement.tx_comm_secteur_8
        WHEN tax_secteur = 9 THEN taxe_amenagement.tx_comm_secteur_9
        WHEN tax_secteur = 10 THEN taxe_amenagement.tx_comm_secteur_10
        WHEN tax_secteur = 11 THEN taxe_amenagement.tx_comm_secteur_11
        WHEN tax_secteur = 12 THEN taxe_amenagement.tx_comm_secteur_12
        WHEN tax_secteur = 13 THEN taxe_amenagement.tx_comm_secteur_13
        WHEN tax_secteur = 14 THEN taxe_amenagement.tx_comm_secteur_14
        WHEN tax_secteur = 15 THEN taxe_amenagement.tx_comm_secteur_15
        WHEN tax_secteur = 16 THEN taxe_amenagement.tx_comm_secteur_16
        WHEN tax_secteur = 17 THEN taxe_amenagement.tx_comm_secteur_17
        WHEN tax_secteur = 18 THEN taxe_amenagement.tx_comm_secteur_18
        WHEN tax_secteur = 19 THEN taxe_amenagement.tx_comm_secteur_19
        WHEN tax_secteur = 20 THEN taxe_amenagement.tx_comm_secteur_20
    END as tax_taux_secteur,
    dossier.tax_secteur as tax_numero_secteur,
    dossier.tax_mtn_part_commu as tax_montant_part_communale,
    dossier.tax_mtn_part_depart as tax_montant_part_departementale,
    dossier.tax_mtn_part_reg as tax_montant_part_regionale,
    dossier.tax_mtn_total as tax_montant_total,

    --Coordonnées du pétitionnaire principal

    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
    END as nom_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_principal_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_nom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_denomination
    END as denomination_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_siret
    END as siret_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_categorie_juridique
    END as categorie_juridique_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,
    petitionnaire_principal.division_territoriale as division_territoriale_petitionnaire_principal,

    --Coordonnées du pétitionnaire principal initial

    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_initial_civilite.libelle, petitionnaire_principal_initial.particulier_nom, petitionnaire_principal_initial.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_principal_initial.personne_morale_nom IS NOT NULL OR petitionnaire_principal_initial.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_initial.personne_morale_raison_sociale, petitionnaire_principal_initial.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_initial_civilite.libelle, petitionnaire_principal_initial.personne_morale_nom, petitionnaire_principal_initial.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_principal_initial.personne_morale_raison_sociale, '' '', petitionnaire_principal_initial.personne_morale_denomination))
            END
    END as nom_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier'' OR petitionnaire_principal_initial.personne_morale_nom IS NOT NULL OR petitionnaire_principal_initial.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_principal_initial_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN petitionnaire_principal_initial.particulier_nom
        ELSE
            CASE WHEN petitionnaire_principal_initial.personne_morale_nom IS NOT NULL
                THEN petitionnaire_principal_initial.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN petitionnaire_principal_initial.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_principal_initial.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal_initial.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal_initial.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal_initial.personne_morale_denomination
    END as denomination_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal_initial.personne_morale_siret
    END as siret_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal_initial.personne_morale_categorie_juridique
    END as categorie_juridique_petitionnaire_principal_initial,

    petitionnaire_principal_initial.numero as numero_petitionnaire_principal_initial,
    petitionnaire_principal_initial.voie as voie_petitionnaire_principal_initial,
    petitionnaire_principal_initial.complement as complement_petitionnaire_principal_initial,
    petitionnaire_principal_initial.lieu_dit as lieu_dit_petitionnaire_principal_initial,
    CASE 
        WHEN petitionnaire_principal_initial.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal_initial.bp)
    END as bp_petitionnaire_principal_initial,
    petitionnaire_principal_initial.code_postal as code_postal_petitionnaire_principal_initial,
    petitionnaire_principal_initial.localite as localite_petitionnaire_principal_initial,
    CASE 
        WHEN petitionnaire_principal_initial.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal_initial.cedex)
    END as cedex_petitionnaire_principal_initial,
    petitionnaire_principal_initial.pays as pays_petitionnaire_principal,
    petitionnaire_principal_initial.division_territoriale as division_territoriale_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1

    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_1_civilite.libelle, petitionnaire_1.particulier_nom, petitionnaire_1.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_1.personne_morale_raison_sociale, petitionnaire_1.personne_morale_denomination, ''représenté(e) par'', petitionnaire_1_civilite.libelle, petitionnaire_1.personne_morale_nom, petitionnaire_1.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
            END
    END as nom_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier'' OR petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_1_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_nom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL
                THEN petitionnaire_1.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_1.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_denomination
    END as denomination_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_siret
    END as siret_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_categorie_juridique
    END as categorie_juridique_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,
    petitionnaire_1.division_territoriale as division_territoriale_petitionnaire_1,

    --Coordonnées du pétitionnaire 2

    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_2_civilite.libelle, petitionnaire_2.particulier_nom, petitionnaire_2.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_2.personne_morale_raison_sociale, petitionnaire_2.personne_morale_denomination, ''représenté(e) par'', petitionnaire_2_civilite.libelle, petitionnaire_2.personne_morale_nom, petitionnaire_2.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
            END
    END as nom_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier'' OR petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_2_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_nom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL
                THEN petitionnaire_2.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_2.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_denomination
    END as denomination_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_categorie_juridique
    END as categorie_juridique_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_siret
    END as siret_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,
    petitionnaire_2.division_territoriale as division_territoriale_petitionnaire_2,

    --Coordonnées du pétitionnaire 3

    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_3_civilite.libelle, petitionnaire_3.particulier_nom, petitionnaire_3.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_3.personne_morale_raison_sociale, petitionnaire_3.personne_morale_denomination, ''représenté(e) par'', petitionnaire_3_civilite.libelle, petitionnaire_3.personne_morale_nom, petitionnaire_3.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
            END
    END as nom_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier'' OR petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_3_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_nom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL
                THEN petitionnaire_3.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_3.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_denomination
    END as denomination_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_siret
    END as siret_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_categorie_juridique
    END as categorie_juridique_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,
    petitionnaire_3.division_territoriale as division_territoriale_petitionnaire_3,

    --Coordonnées du pétitionnaire 4

    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_4_civilite.libelle, petitionnaire_4.particulier_nom, petitionnaire_4.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_4.personne_morale_raison_sociale, petitionnaire_4.personne_morale_denomination, ''représenté(e) par'', petitionnaire_4_civilite.libelle, petitionnaire_4.personne_morale_nom, petitionnaire_4.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
            END
    END as nom_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier'' OR petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_4_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_nom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL
                THEN petitionnaire_4.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_4.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_denomination
    END as denomination_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_categorie_juridique
    END as categorie_juridique_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_siret
    END as siret_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,
    petitionnaire_4.division_territoriale as division_territoriale_petitionnaire_4,

    --Coordonnées du pétitionnaire 5

    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_5_civilite.libelle, petitionnaire_5.particulier_nom, petitionnaire_5.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_5.personne_morale_raison_sociale, petitionnaire_5.personne_morale_denomination, ''représenté(e) par'', petitionnaire_5_civilite.libelle, petitionnaire_5.personne_morale_nom, petitionnaire_5.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
            END
    END as nom_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier'' OR petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_5_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_nom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL
                THEN petitionnaire_5.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_5.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_denomination
    END as denomination_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_categorie_juridique
    END as categorie_juridique_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_siret
    END as siret_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,
    petitionnaire_5.division_territoriale as division_territoriale_petitionnaire_5,

    --Coordonnées du délégataire

    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', delegataire_civilite.libelle, delegataire.particulier_nom, delegataire.particulier_prenom))
        ELSE
            CASE WHEN delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', delegataire.personne_morale_raison_sociale, delegataire.personne_morale_denomination, ''représenté(e) par'', delegataire_civilite.libelle, delegataire.personne_morale_nom, delegataire.personne_morale_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_delegataire,
    CASE WHEN delegataire.qualite=''particulier'' OR delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
        THEN delegataire_civilite.libelle
        ELSE ''''
    END as civilite_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN delegataire.particulier_nom
        ELSE
            CASE WHEN delegataire.personne_morale_nom IS NOT NULL
                THEN delegataire.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN delegataire.particulier_prenom
        ELSE
            CASE WHEN delegataire.personne_morale_prenom IS NOT NULL
                THEN delegataire.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN ''''
        ELSE delegataire.personne_morale_raison_sociale
    END as raison_sociale_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN ''''
        ELSE delegataire.personne_morale_denomination
    END as denomination_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN ''''
        ELSE delegataire.personne_morale_siret
    END as siret_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN ''''
        ELSE delegataire.personne_morale_categorie_juridique
    END as categorie_juridique_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complement_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    CASE 
        WHEN delegataire.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', delegataire.bp)
    END as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    CASE 
        WHEN delegataire.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', delegataire.cedex)
    END as cedex_delegataire,
    delegataire.pays as pays_delegataire,
    delegataire.division_territoriale as division_territoriale_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT_WS('' '', delegataire_civilite.libelle, delegataire.particulier_nom, delegataire.particulier_prenom))
                ELSE
                    CASE WHEN delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                        THEN TRIM(CONCAT_WS('' '', delegataire.personne_morale_raison_sociale, delegataire.personne_morale_denomination, ''représenté(e) par'', delegataire_civilite.libelle, delegataire.personne_morale_nom, delegataire.personne_morale_prenom))
                        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
                    END
            END
    END as nom_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal_civilite.libelle
                ELSE ''''
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier'' OR delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                THEN delegataire_civilite.libelle
                ELSE ''''
            END
    END as civilite_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN petitionnaire_principal.particulier_nom
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                        THEN petitionnaire_principal.personne_morale_nom
                        ELSE ''''
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN delegataire.particulier_nom
                ELSE
                    CASE WHEN delegataire.personne_morale_nom IS NOT NULL
                        THEN delegataire.personne_morale_nom
                        ELSE ''''
                    END
            END
    END as nom_particulier_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN petitionnaire_principal.particulier_prenom
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                        THEN petitionnaire_principal.personne_morale_prenom
                        ELSE ''''
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN delegataire.particulier_prenom
                ELSE
                    CASE WHEN delegataire.personne_morale_prenom IS NOT NULL
                        THEN delegataire.personne_morale_prenom
                        ELSE ''''
                    END
            END
    END as prenom_particulier_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN ''''
                ELSE petitionnaire_principal.personne_morale_raison_sociale
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN ''''
                ELSE delegataire.personne_morale_raison_sociale
            END
    END as raison_sociale_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN ''''
                ELSE petitionnaire_principal.personne_morale_denomination
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN ''''
                ELSE delegataire.personne_morale_denomination
            END
    END as denomination_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN ''''
                ELSE petitionnaire_principal.personne_morale_siret
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN ''''
                ELSE delegataire.personne_morale_siret
            END
    END as siret_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN ''''
                ELSE petitionnaire_principal.personne_morale_categorie_juridique
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN ''''
                ELSE delegataire.personne_morale_categorie_juridique
            END
    END as categorie_juridique_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', petitionnaire_principal.bp)
            END
        ELSE 
            CASE 
                WHEN delegataire.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', delegataire.bp)
            END
    END as bp_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
            END 
        ELSE 
            CASE 
                WHEN delegataire.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', delegataire.cedex)
            END 
    END as cedex_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,
    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.division_territoriale
        ELSE delegataire.division_territoriale
    END as division_territoriale_correspondant,

    --Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_complet, ''DD/MM/YYYY'') as date_completude,
    to_char(dossier.date_dernier_depot, ''DD/MM/YYYY'') as date_dernier_depot,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    CASE WHEN dossier.incomplet_notifie IS TRUE AND dossier.incompletude IS TRUE 
        THEN to_char(dossier.date_limite_incompletude ,''DD/MM/YYYY'') 
        ELSE to_char(dossier.date_limite ,''DD/MM/YYYY'')
    END as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    to_char(dossier.date_notification_delai,''DD/MM/YYYY'') as date_notification_delai_dossier,
    
    --Noms des signataires
    TRIM(CONCAT(signataire_civilite.libelle, '' '',signataire_arrete.prenom, '' '', signataire_arrete.nom)) as arrete_signataire,
    TRIM(CONCAT(signataire_arrete.qualite, '' '', signataire_arrete.signature)) as signature_signataire,
    division.chef as chef_division,
    direction.chef as chef_direction,
    direction.libelle as libelle_direction,
    direction.description as description_direction,
    
    --Données générales des données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.ope_proj_desc as ope_proj_desc_donnees_techniques,
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_cstr_shon_tot
        ELSE donnees_techniques.su_cstr_shon_tot
    END as su_cstr_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_demo_shon_tot
        ELSE donnees_techniques.su_demo_shon_tot
    END as su_demo_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN
            REGEXP_REPLACE(CONCAT(
                CASE WHEN donnees_techniques.su2_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation agricole - '', donnees_techniques.su2_cstr_shon1, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation forestière - '', donnees_techniques.su2_cstr_shon2, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Logement - '', donnees_techniques.su2_cstr_shon3, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement - '', donnees_techniques.su2_cstr_shon4, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Artisanat et commerce de détail - '', donnees_techniques.su2_cstr_shon5, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Restauration - '', donnees_techniques.su2_cstr_shon6, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Commerce de gros - '', donnees_techniques.su2_cstr_shon7, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Activités de services où s''''effectue l''''accueil d''''une clientèle - '', donnees_techniques.su2_cstr_shon8, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement hôtelier et touristique - '', donnees_techniques.su2_cstr_shon9, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon10 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Cinéma - '', donnees_techniques.su2_cstr_shon10, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon11 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux et bureaux accueillant du public des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon11, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon12 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux techniques et industriels des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon12, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon13 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Établissements d''''enseignement, de santé et d''''action sociale - '', donnees_techniques.su2_cstr_shon13, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon14 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Salles d''''art et de spectacles - '', donnees_techniques.su2_cstr_shon14, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon15 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Équipements sportifs - '', donnees_techniques.su2_cstr_shon15, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon16 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Autres équipements recevant du public - '', donnees_techniques.su2_cstr_shon16, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon17 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Industrie - '', donnees_techniques.su2_cstr_shon17, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon18 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Entrepôt - '', donnees_techniques.su2_cstr_shon18, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon19 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Bureau - '', donnees_techniques.su2_cstr_shon19, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon20 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Centre de congrès et d''''exposition - '', donnees_techniques.su2_cstr_shon20, '' m²'')
                END
            ), '' / $'', '''')
        ELSE
            REGEXP_REPLACE(CONCAT(
                CASE
                    WHEN donnees_techniques.su_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
                END, 
                CASE
                    WHEN donnees_techniques.su_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
                END
            ), '' / $'', '''')
    END as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_tot_shon_tot
        ELSE donnees_techniques.su_tot_shon_tot
    END as su_tot_shon_tot_donnees_techniques,

    -- Données techniques pour les AT
    CONCAT_WS('', '',
        CASE WHEN donnees_techniques.erp_cstr_neuve IS TRUE THEN ''construction neuve'' END,
        CASE WHEN donnees_techniques.erp_trvx_acc IS TRUE THEN ''travaux de mise en conformité totale aux règles d’accessibilité'' END,
        CASE WHEN donnees_techniques.erp_extension IS TRUE THEN ''extension'' END,
        CASE WHEN donnees_techniques.erp_rehab IS TRUE THEN ''réhabilitation'' END,
        CASE WHEN donnees_techniques.erp_trvx_am IS TRUE THEN ''travaux d’aménagement (remplacement de revêtements, rénovation électrique, création d’une rampe, par exemple)'' END,
        CASE WHEN donnees_techniques.erp_vol_nouv_exist IS TRUE THEN ''création de volumes nouveaux dans des volumes existants (modification du cloisonnement, par exemple)'' END
    ) as at_type_travaux,
    donnees_techniques.erp_public_eff_tot as at_effectif_public_total,
    CONCAT_WS('' - '', erp_categorie.libelle, erp_categorie.description) as at_categorie_etablissement,
    CONCAT_WS('' - '', erp_type.libelle, erp_type.description) as at_type_etablissement,

    -- Données techniques pour une DIA
    COALESCE (donnees_techniques.dia_mod_cess_prix_vente,'''') AS dia_mod_cess_prix_vente,
    COALESCE (donnees_techniques.dia_acquereur_nom_prenom,'''') AS dia_acquereur_nom_prenom,
    CONCAT(
        COALESCE(donnees_techniques.dia_acquereur_adr_num_voie || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_ext || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_type_voie || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_nom_voie || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_lieu_dit_bp || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_cp || '' '',''''),
        COALESCE(donnees_techniques.dia_acquereur_adr_localite || '' '','''')
        ) as dia_acquereur_adresse,

    --Bordereau d''envoi au maire
    CASE
        WHEN evenement.type = ''arrete''
        THEN CONCAT(''transmission d''''une proposition de décision sur '', evenement.libelle)
        ELSE CONCAT(''transmission d''''un courrier d''''instruction sur '', evenement.libelle)
    END as objet_bordereau_envoi_maire
    
FROM
    &DB_PREFIXEinstruction
LEFT JOIN
    &DB_PREFIXEsignataire_arrete
    ON instruction.signataire_arrete = signataire_arrete.signataire_arrete
LEFT JOIN
    &DB_PREFIXEcivilite as signataire_civilite
    ON signataire_arrete.civilite = signataire_civilite.civilite
LEFT JOIN
    &DB_PREFIXEom_lettretype
    ON instruction.lettretype = om_lettretype.id and om_lettretype.actif IS TRUE
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        instruction.dossier=dossier.dossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        instruction.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite as petitionnaire_principal_civilite
    ON
        petitionnaire_principal.particulier_civilite = petitionnaire_principal_civilite.civilite OR petitionnaire_principal.personne_morale_civilite = petitionnaire_principal_civilite.civilite
LEFT JOIN
    &DB_PREFIXElien_dossier_autorisation_demandeur
    ON
        dossier.dossier_autorisation = lien_dossier_autorisation_demandeur.dossier_autorisation AND lien_dossier_autorisation_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal_initial
    ON
        lien_dossier_autorisation_demandeur.demandeur = petitionnaire_principal_initial.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite as petitionnaire_principal_initial_civilite
    ON
        petitionnaire_principal_initial.particulier_civilite = petitionnaire_principal_initial_civilite.civilite OR petitionnaire_principal_initial.personne_morale_civilite = petitionnaire_principal_initial_civilite.civilite
LEFT JOIN
    (
    &DB_PREFIXElien_dossier_demandeur AS lien_dossier_delegataire
        JOIN &DB_PREFIXEdemandeur as delegataire
        ON
            lien_dossier_delegataire.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
    )
    ON
        instruction.dossier = lien_dossier_delegataire.dossier AND lien_dossier_delegataire.petitionnaire_principal IS FALSE
LEFT JOIN
    &DB_PREFIXEcivilite as delegataire_civilite
    ON
        delegataire.particulier_civilite = delegataire_civilite.civilite OR delegataire.personne_morale_civilite = delegataire_civilite.civilite
LEFT JOIN (
    SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
    FROM &DB_PREFIXElien_dossier_demandeur
    LEFT JOIN &DB_PREFIXEdossier
        ON lien_dossier_demandeur.dossier=dossier.dossier 
        AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
    LEFT JOIN &DB_PREFIXEinstruction
        ON instruction.dossier = dossier.dossier
    WHERE instruction.instruction = &idx
    GROUP BY lien_dossier_demandeur.dossier
) as sub_petitionnaire_autre
ON instruction.dossier = sub_petitionnaire_autre.dossier
LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1] AND petitionnaire_1.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_1_civilite
    ON
        petitionnaire_1.particulier_civilite = petitionnaire_1_civilite.civilite OR petitionnaire_1.personne_morale_civilite = petitionnaire_1_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2] AND petitionnaire_2.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_2_civilite
    ON
        petitionnaire_2.particulier_civilite = petitionnaire_2_civilite.civilite OR petitionnaire_2.personne_morale_civilite = petitionnaire_2_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3] AND petitionnaire_3.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_3_civilite
    ON
        petitionnaire_3.particulier_civilite = petitionnaire_3_civilite.civilite OR petitionnaire_3.personne_morale_civilite = petitionnaire_3_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4] AND petitionnaire_4.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_4_civilite
    ON
        petitionnaire_4.particulier_civilite = petitionnaire_4_civilite.civilite OR petitionnaire_4.personne_morale_civilite = petitionnaire_4_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5] AND petitionnaire_5.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_5_civilite
    ON
        petitionnaire_5.particulier_civilite = petitionnaire_5_civilite.civilite OR petitionnaire_5.personne_morale_civilite = petitionnaire_5_civilite.civilite
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN 
    &DB_PREFIXEdirection
    ON division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN 
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN 
    &DB_PREFIXEquartier
    ON
        dossier.quartier = quartier.quartier
LEFT JOIN
    &DB_PREFIXEtaxe_amenagement
    ON
        dossier.om_collectivite = taxe_amenagement.om_collectivite
LEFT JOIN
    &DB_PREFIXEerp_categorie
    ON
        donnees_techniques.erp_class_cat = erp_categorie.erp_categorie
LEFT JOIN
    &DB_PREFIXEerp_type
    ON
        donnees_techniques.erp_class_type = erp_type.erp_type
WHERE instruction.instruction = &idx
',
merge_fields = '
--Données générales de l''événement d''instruction
[complement1_instruction]
[complement2_instruction]
[complement3_instruction]
[complement4_instruction]
[code_barres_instruction]
[date_evenement_instruction]
[libelle_om_lettretype]
[archive_delai_instruction]

--Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]    [delai_dossier]    [terrain_references_cadastrales_dossier]
[terrain_superficie_dossier]
[libelle_quartier]
[libelle_da]

[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]

[libelle_avis_decision]

--Données générales du paramétrage de l''événement
[libelle_evenement]
[etat_evenement]
[delai_evenement]
[accord_tacite_evenement]
[delai_notification_evenement]
[avis_decision_evenement]
[autorite_competente_evenement]
[cle_acces_citoyen]

--Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

--Adresse du terrain du dossier d''instruction 
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]

[libelle_arrondissement]

--Taxe d''aménagement du dossier d''instruction
[tax_taux_secteur]
[tax_numero_secteur]
[tax_montant_part_communale]
[tax_montant_part_departementale]
[tax_montant_part_regionale]
[tax_montant_total]

--Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[civilite_petitionnaire_principal]
[nom_particulier_petitionnaire_principal]
[prenom_particulier_petitionnaire_principal]
[raison_sociale_petitionnaire_principal]
[denomination_petitionnaire_principal]
[siret_petitionnaire_principal]
[categorie_juridique_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal] [division_territoriale_petitionnaire_principal]

--Coordonnées du pétitionnaire principal initial
[nom_petitionnaire_principal_initial]
[civilite_petitionnaire_principal_initial]
[nom_particulier_petitionnaire_principal_initial]
[prenom_particulier_petitionnaire_principal_initial]
[raison_sociale_petitionnaire_principal_initial]
[denomination_petitionnaire_principal_initial]
[siret_petitionnaire_principal_initial]
[categorie_juridique_petitionnaire_principal_initial]
[numero_petitionnaire_principal_initial]    [voie_petitionnaire_principal_initial]    [complement_petitionnaire_principal_initial]
[lieu_dit_petitionnaire_principal_initial]    [bp_petitionnaire_principal_initial]
[code_postal_petitionnaire_principal_initial]    [localite_petitionnaire_principal_initial]    [cedex_petitionnaire_principal_initial]
[pays_petitionnaire_principal_initial] [division_territoriale__petitionnaire_principal_initial] 

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''à 5)
[civilite_petitionnaire_1](jusqu''à 5)
[nom_particulier_petitionnaire_1](jusqu''à 5)
[prenom_particulier_petitionnaire_1](jusqu''à 5)
[raison_sociale_petitionnaire_1](jusqu''à 5)
[denomination_petitionnaire_1](jusqu''à 5)
[siret_petitionnaire_1](jusqu''à 5)
[categorie_juridique_petitionnaire_1](jusqu''à 5)
[numero_petitionnaire_1](jusqu''à 5)    [voie_petitionnaire_1](jusqu''à 5)    
[complement_petitionnaire_1](jusqu''à 5)
[lieu_dit_petitionnaire_1](jusqu''à 5)    [bp_petitionnaire_1](jusqu''à 5)
[code_postal_petitionnaire_1](jusqu''à 5)    [localite_petitionnaire_1](jusqu''à 5)    
[cedex_petitionnaire_1](jusqu''à 5)
[pays_petitionnaire_1](jusqu''à 5) [division_territoriale_petitionnaire_1](jusqu''à 5) 

--Coordonnées du délégataire
[nom_delegataire]
[civilite_delegataire]
[nom_particulier_delegataire]Fsu_
[prenom_particulier_delegataire]
[raison_sociale_delegataire]
[denomination_delegataire]
[siret_delegataire]
[categorie_juridique_delegataire]
[numero_delegataire]    [voie_delegataire]    [complement_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire] [division_territoriale_delegataire] 

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[civilite_correspondant]
[nom_particulier_correspondant]
[prenom_particulier_correspondant]
[raison_sociale_correspondant]
[denomination_correspondant]
[siret_correspondant]
[categorie_juridique_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant] [division_territoriale_correspondant] 

--Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_completude]
[date_dernier_depot]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]
[date_notification_delai_dossier]
[date_decision_da]

--Noms des signataires
[arrete_signataire]
[signature_signataire]
[chef_division]
[chef_direction]
[libelle_direction]
[description_direction]

--Données générales des données techniques
[co_projet_desc_donnees_techniques]    [am_projet_desc_donnees_techniques]
[dm_projet_desc_donnees_techniques]    [ope_proj_desc_donnees_techniques]
[projet_desc_donnees_techniques]
[co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
-- Les données techniques suivantes concernent le tableau des surfaces
-- Elles récupèrent les valeurs du tableau composé des sous-destinations si au
-- moins une valeur de celui-ci est saisie
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]
[su_tot_shon_tot_donnees_techniques]

--Données techniques des AT
[at_type_travaux]
[at_effectif_public_total]
[at_categorie_etablissement]
[at_type_etablissement]

--Bordereau envoi au maire
[dia_mod_cess_prix_vente]
[dia_acquereur_nom_prenom]
[dia_acquereur_adresse]

--Bordereau envoi au maire
[objet_bordereau_envoi_maire]
'
WHERE code = 'instruction';

--
-- END / [#8657] Ajout de champs de fusion des CERFA DOC et DAACT
--

--
-- START / [#8656] Erreur de permission concernant le profil *CHEF de SERVICE*
--

-- Permissions pour le profil *CHEF DE SERVICE*
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_finaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_definaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );

--
-- END / [#8656] Erreur de permission concernant le profil *CHEF de SERVICE*
--

--
-- START / [#8652] Ajout de données techniques concernant les demandes de PA
--

-- cerfa
--
ALTER TABLE cerfa ADD COLUMN am_terr_res_demon boolean DEFAULT false;
COMMENT ON COLUMN cerfa.am_terr_res_demon IS 'Aménagement d’un terrain pour au moins 2 résidences démontables, créant une surface de plancher totale supérieure à 40M², constituant l’habitat permanent de leurs utilisateurs';
--
ALTER TABLE cerfa ADD COLUMN am_air_terr_res_mob boolean DEFAULT false;
COMMENT ON COLUMN cerfa.am_air_terr_res_mob IS 'Aménagement d’une aire d’accueil ou d’un terrain familial des gens du voyage recevant plus de deux résidences mobiles';

-- donnees_techniques
--
ALTER TABLE donnees_techniques ADD COLUMN am_terr_res_demon boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.am_terr_res_demon IS 'Aménagement d’un terrain pour au moins 2 résidences démontables, créant une surface de plancher totale supérieure à 40M², constituant l’habitat permanent de leurs utilisateurs';
--
ALTER TABLE donnees_techniques ADD COLUMN am_air_terr_res_mob boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.am_air_terr_res_mob IS 'Aménagement d’une aire d’accueil ou d’un terrain familial des gens du voyage recevant plus de deux résidences mobiles';

--
-- END / [#8652] Ajout de données techniques concernant les demandes de PA
--
