--
--  START - [#8384] Rendre possible le suivi des dates depuis le dossier aux instructeurs polyvalents commune
--

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_modification_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_modification_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_modification_dates_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_modification_dates_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_modification_dates_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_modification_dates_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );

--
--  END - [#8384] Rendre possible le suivi des dates depuis le dossier aux instructeurs polyvalents commune
--

--
-- START / #28 — Gestion du workflow des DIA
--

-- CERFA 10072*02

-- Désignation du bien
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_terre boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_terre IS 'Occupation du sol en superficie (m²) : Terres';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_pres boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_pres IS 'Occupation du sol en superficie (m²) : Près';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_verger boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_verger IS 'Occupation du sol en superficie (m²) : Vergers';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_vigne boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_vigne IS 'Occupation du sol en superficie (m²) : Vignes';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_bois boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_bois IS 'Occupation du sol en superficie (m²) : Bois';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_lande boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_lande IS 'Occupation du sol en superficie (m²) : Landes';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_carriere boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_carriere IS 'Occupation du sol en superficie (m²) : Carrières';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_eau_cadastree boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_eau_cadastree IS 'Occupation du sol en superficie (m²) : Eaux cadastrées';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_jardin boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_jardin IS 'Occupation du sol en superficie (m²) : Jardins';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_terr_batir boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_terr_batir IS 'Occupation du sol en superficie (m²) : Terrains à bâtir';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_terr_agr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_terr_agr IS 'Occupation du sol en superficie (m²) : Terrains d''agrément';
--
ALTER TABLE cerfa ADD COLUMN dia_occ_sol_su_sol boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_occ_sol_su_sol IS 'Occupation du sol en superficie (m²) : Sol';
--
ALTER TABLE cerfa ADD COLUMN dia_bati_vend_tot boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_bati_vend_tot IS 'Bâtiments vendus en totalité';
--
ALTER TABLE cerfa ADD COLUMN dia_bati_vend_tot_txt boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_bati_vend_tot_txt IS 'Bâtiments vendus en totalité (texte)';
--
ALTER TABLE cerfa ADD COLUMN dia_su_co_sol boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_su_co_sol IS 'Surface construite au sol (m2)';
--
ALTER TABLE cerfa ADD COLUMN dia_su_util_hab boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_su_util_hab IS 'Surface utile ou habitable (m2)';
--
ALTER TABLE cerfa ADD COLUMN dia_nb_niv boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_nb_niv IS 'Nombre de : Niveaux';
--
ALTER TABLE cerfa ADD COLUMN dia_nb_appart boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_nb_appart IS 'Nombre de : Appartements';
--
ALTER TABLE cerfa ADD COLUMN dia_nb_autre_loc boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_nb_autre_loc IS 'Nombre de : Autres locaux';
--
ALTER TABLE cerfa ADD COLUMN dia_vente_lot_volume boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_vente_lot_volume IS 'Vente en lot de volumes';
--
ALTER TABLE cerfa ADD COLUMN dia_vente_lot_volume_txt boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_vente_lot_volume_txt IS 'Vente en lot de volumes (texte)';
--
ALTER TABLE cerfa ADD COLUMN dia_lot_nat_su boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_lot_nat_su IS 'Nature et surface utile ou habitable';
--
ALTER TABLE cerfa ADD COLUMN dia_lot_bat_achv_plus_10 boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_lot_bat_achv_plus_10 IS 'Le bâtiment est achevé depuis : Plus de 10 ans';
--
ALTER TABLE cerfa ADD COLUMN dia_lot_bat_achv_moins_10 boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_lot_bat_achv_moins_10 IS 'Le bâtiment est achevé depuis : Moins de 10 ans';
--
ALTER TABLE cerfa ADD COLUMN dia_lot_regl_copro_publ_hypo_plus_10 boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_lot_regl_copro_publ_hypo_plus_10 IS 'Le règlement de copropriété a été publié aux hypothèques depuis : Plus de 10 ans';
--
ALTER TABLE cerfa ADD COLUMN dia_lot_regl_copro_publ_hypo_moins_10 boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_lot_regl_copro_publ_hypo_moins_10 IS 'Le règlement de copropriété a été publié aux hypothèques depuis : Moins de 10 ans';
--
ALTER TABLE cerfa ADD COLUMN dia_indivi_quote_part boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_indivi_quote_part IS 'En cas d’indivision, quote-part du bien vendu';
--
ALTER TABLE cerfa ADD COLUMN dia_design_societe boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_design_societe IS 'Désignation de la société';
--
ALTER TABLE cerfa ADD COLUMN dia_design_droit boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_design_droit IS 'Désignation des droits';
--
ALTER TABLE cerfa ADD COLUMN dia_droit_soc_nat boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_droit_soc_nat IS 'Nature';
--
ALTER TABLE cerfa ADD COLUMN dia_droit_soc_nb boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_droit_soc_nb IS 'Nombre';
--
ALTER TABLE cerfa ADD COLUMN dia_droit_soc_num_part boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_droit_soc_num_part IS 'Numéro des parts';

-- Droits réels ou personnels
--
ALTER TABLE cerfa ADD COLUMN dia_droit_reel_perso_grevant_bien_oui boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_droit_reel_perso_grevant_bien_oui IS 'Grevant les biens : OUI';
--
ALTER TABLE cerfa ADD COLUMN dia_droit_reel_perso_grevant_bien_non boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_droit_reel_perso_grevant_bien_non IS 'Grevant les biens : NON';
--
ALTER TABLE cerfa ADD COLUMN dia_droit_reel_perso_nat boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_droit_reel_perso_nat IS 'Préciser la nature';
--
ALTER TABLE cerfa ADD COLUMN dia_droit_reel_perso_viag boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_droit_reel_perso_viag IS 'Indiquer si rente viagère antérieure';

-- Modalités de la cession
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_adr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_adr IS 'Si vente indissociable d’autres biens : Adresse précise du bien';

--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_sign_act_auth boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_sign_act_auth IS 'Modalités de paiement : comptant à la signature de l’acte authentique';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_terme boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_terme IS 'Modalités de paiement : à terme';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_terme_prec boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_terme_prec IS 'Modalités de paiement : à terme (préciser)';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_bene_acquereur boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_bene_acquereur IS 'Bénéficiaire : acquéreur';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_bene_vendeur boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_bene_vendeur IS 'Bénéficiaire : vendeur';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_paie_nat boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_paie_nat IS 'Paiement en nature';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_design_contr_alien boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_design_contr_alien IS 'Désignation de la contrepartie de l’aliénation';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_eval_contr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_eval_contr IS 'Evaluation de la contrepartie';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_rente_viag boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_rente_viag IS 'Rente viagère';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_mnt_an boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_mnt_an IS 'Montant annuel';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_mnt_compt boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_mnt_compt IS 'Montant comptant';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_bene_rente boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_bene_rente IS 'Bénéficiaire(s) de la rente';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_droit_usa_hab boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_droit_usa_hab IS 'Droit d’usage et d’habitation';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_droit_usa_hab_prec boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_droit_usa_hab_prec IS 'Droit d’usage et d’habitation (à préciser)';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_eval_usa_usufruit boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_eval_usa_usufruit IS 'Evaluation de l’usage ou de l’usufruit';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_vente_nue_prop boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_vente_nue_prop IS 'Vente de la nue-propriété';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_vente_nue_prop_prec boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_vente_nue_prop_prec IS 'Vente de la nue-propriété (à préciser)';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_echange boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_echange IS 'Echange';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_design_bien_recus_ech boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_design_bien_recus_ech IS 'Désignation des biens reçus en échange';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_mnt_soulte boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_mnt_soulte IS 'Montant de la soulte le cas échéant';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_prop_contre_echan boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_prop_contre_echan IS 'Propriétaires contre-échangistes';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_apport_societe boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_apport_societe IS 'Apport en société';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_bene boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_bene IS 'Bénéficiaire';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_esti_bien boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_esti_bien IS 'Estimation du bien apporté';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_cess_terr_loc_co boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_cess_terr_loc_co IS 'Cession de tantième de terrains contre remise de locaux à construire';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_esti_terr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_esti_terr IS 'Estimation du terrain';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_esti_loc boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_esti_loc IS 'Estimation des locaux à remettre';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_esti_imm_loca boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_esti_imm_loca IS 'Location-accession – Estimation de l’immeuble objet de la location-accession';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_adju_vol boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_adju_vol IS 'Volontaire';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_adju_obl boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_adju_obl IS 'Rendue obligatoire par une disposition législative ou réglementaire';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_adju_fin_indivi boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_adju_fin_indivi IS 'Mettant fin à une indivision ne résultant pas d’une donation-partage';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_adju_date_lieu boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_adju_date_lieu IS 'Date et lieu de l’adjudication';
--
ALTER TABLE cerfa ADD COLUMN dia_mod_cess_mnt_mise_prix boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_mod_cess_mnt_mise_prix IS 'Montant de la mise à prix';

-- Les soussignés déclarent
--
ALTER TABLE cerfa ADD COLUMN dia_prop_titu_prix_indique boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_prop_titu_prix_indique IS 'Que le(s) propriétaire(s) nommé(s) à la rubrique 1 : Demande(nt) au titulaire du droit de préemption d’acquérir les biens désignés à la rubrique 3 aux prix et conditions indiqués';
--
ALTER TABLE cerfa ADD COLUMN dia_prop_recherche_acqu_prix_indique boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_prop_recherche_acqu_prix_indique IS 'Que le(s) propriétaire(s) nommé(s) à la rubrique 1 : A (ont) recherché un acquéreur disposé à acquérir les biens désignés à la rubrique 3 aux prix et conditions indiqués';
--
ALTER TABLE cerfa ADD COLUMN dia_acquereur_prof boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_acquereur_prof IS 'Profession (facultatif)';
--
ALTER TABLE cerfa ADD COLUMN dia_indic_compl_ope boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_indic_compl_ope IS 'Indications complémentaires concernant l’opération envisagée par l’acquéreur (facultatif)';
--
ALTER TABLE cerfa ADD COLUMN dia_vente_adju boolean DEFAULT false;
COMMENT ON COLUMN cerfa.dia_vente_adju IS 'Qu’il est chargé de procéder à la vente par voie d’adjudication comme indiqué à la rubrique F-2 des biens désignés à la rubrique C appartenant a(ux) propriétaire(s) nommé(s) en A';

-- DONNEES_TECHNIQUES

-- Modification du type de champs déjà existant
--
ALTER TABLE donnees_techniques ALTER COLUMN dia_lot_numero TYPE text;
--
ALTER TABLE donnees_techniques ALTER COLUMN dia_lot_bat TYPE text;
--
ALTER TABLE donnees_techniques ALTER COLUMN dia_lot_etage TYPE text;
--
ALTER TABLE donnees_techniques ALTER COLUMN dia_lot_quote_part TYPE text;

-- Ajout des nouveaux champs
-- Désignation du bien
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_terre text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_terre IS 'Occupation du sol en superficie (m²) : Terres';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_pres text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_pres IS 'Occupation du sol en superficie (m²) : Près';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_verger text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_verger IS 'Occupation du sol en superficie (m²) : Vergers';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_vigne text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_vigne IS 'Occupation du sol en superficie (m²) : Vignes';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_bois text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_bois IS 'Occupation du sol en superficie (m²) : Bois';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_lande text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_lande IS 'Occupation du sol en superficie (m²) : Landes';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_carriere text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_carriere IS 'Occupation du sol en superficie (m²) : Carrières';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_eau_cadastree text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_eau_cadastree IS 'Occupation du sol en superficie (m²) : Eaux cadastrées';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_jardin text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_jardin IS 'Occupation du sol en superficie (m²) : Jardins';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_terr_batir text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_terr_batir IS 'Occupation du sol en superficie (m²) : Terrains à bâtir';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_terr_agr text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_terr_agr IS 'Occupation du sol en superficie (m²) : Terrains d''agrément';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_occ_sol_su_sol text;
COMMENT ON COLUMN donnees_techniques.dia_occ_sol_su_sol IS 'Occupation du sol en superficie (m²) : Sol';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_bati_vend_tot boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_bati_vend_tot IS 'Bâtiments vendus en totalité';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_bati_vend_tot_txt text;
COMMENT ON COLUMN donnees_techniques.dia_bati_vend_tot_txt IS 'Bâtiments vendus en totalité (texte)';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_su_co_sol text;
COMMENT ON COLUMN donnees_techniques.dia_su_co_sol IS 'Surface construite au sol (m2)';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_su_util_hab text;
COMMENT ON COLUMN donnees_techniques.dia_su_util_hab IS 'Surface utile ou habitable (m2)';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_nb_niv text;
COMMENT ON COLUMN donnees_techniques.dia_nb_niv IS 'Nombre de : Niveaux';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_nb_appart text;
COMMENT ON COLUMN donnees_techniques.dia_nb_appart IS 'Nombre de : Appartements';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_nb_autre_loc text;
COMMENT ON COLUMN donnees_techniques.dia_nb_autre_loc IS 'Nombre de : Autres locaux';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_vente_lot_volume boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_vente_lot_volume IS 'Vente en lot de volumes';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_vente_lot_volume_txt text;
COMMENT ON COLUMN donnees_techniques.dia_vente_lot_volume_txt IS 'Vente en lot de volumes (texte)';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_lot_nat_su text;
COMMENT ON COLUMN donnees_techniques.dia_lot_nat_su IS 'Nature et surface utile ou habitable';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_lot_bat_achv_plus_10 boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_lot_bat_achv_plus_10 IS 'Le bâtiment est achevé depuis : Plus de 10 ans';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_lot_bat_achv_moins_10 boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_lot_bat_achv_moins_10 IS 'Le bâtiment est achevé depuis : Moins de 10 ans';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_lot_regl_copro_publ_hypo_plus_10 boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_lot_regl_copro_publ_hypo_plus_10 IS 'Le règlement de copropriété a été publié aux hypothèques depuis : Plus de 10 ans';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_lot_regl_copro_publ_hypo_moins_10 boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_lot_regl_copro_publ_hypo_moins_10 IS 'Le règlement de copropriété a été publié aux hypothèques depuis : Moins de 10 ans';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_indivi_quote_part text;
COMMENT ON COLUMN donnees_techniques.dia_indivi_quote_part IS 'En cas d’indivision, quote-part du bien vendu';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_design_societe text;
COMMENT ON COLUMN donnees_techniques.dia_design_societe IS 'Désignation de la société';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_design_droit text;
COMMENT ON COLUMN donnees_techniques.dia_design_droit IS 'Désignation des droits';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_droit_soc_nat text;
COMMENT ON COLUMN donnees_techniques.dia_droit_soc_nat IS 'Nature';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_droit_soc_nb text;
COMMENT ON COLUMN donnees_techniques.dia_droit_soc_nb IS 'Nombre';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_droit_soc_num_part text;
COMMENT ON COLUMN donnees_techniques.dia_droit_soc_num_part IS 'Numéro des parts';

-- Droits réels ou personnels
--
ALTER TABLE donnees_techniques ADD COLUMN dia_droit_reel_perso_grevant_bien_oui boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_droit_reel_perso_grevant_bien_oui IS 'Grevant les biens : OUI';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_droit_reel_perso_grevant_bien_non boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_droit_reel_perso_grevant_bien_non IS 'Grevant les biens : NON';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_droit_reel_perso_nat text;
COMMENT ON COLUMN donnees_techniques.dia_droit_reel_perso_nat IS 'Préciser la nature';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_droit_reel_perso_viag text;
COMMENT ON COLUMN donnees_techniques.dia_droit_reel_perso_viag IS 'Indiquer si rente viagère antérieure';

-- Modalités de la cession
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_adr text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_adr IS 'Si vente indissociable d’autres biens : Adresse précise du bien';

--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_sign_act_auth boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_sign_act_auth IS 'Modalités de paiement : comptant à la signature de l’acte authentique';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_terme boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_terme IS 'Modalités de paiement : à terme';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_terme_prec text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_terme_prec IS 'Modalités de paiement : à terme (préciser)';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_bene_acquereur boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_bene_acquereur IS 'Bénéficiaire : acquéreur';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_bene_vendeur boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_bene_vendeur IS 'Bénéficiaire : vendeur';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_paie_nat boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_paie_nat IS 'Paiement en nature';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_design_contr_alien text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_design_contr_alien IS 'Désignation de la contrepartie de l’aliénation';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_eval_contr text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_eval_contr IS 'Evaluation de la contrepartie';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_rente_viag boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_rente_viag IS 'Rente viagère';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_mnt_an text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_mnt_an IS 'Montant annuel';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_mnt_compt text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_mnt_compt IS 'Montant comptant';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_bene_rente text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_bene_rente IS 'Bénéficiaire(s) de la rente';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_droit_usa_hab boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_droit_usa_hab IS 'Droit d’usage et d’habitation';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_droit_usa_hab_prec text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_droit_usa_hab_prec IS 'Droit d’usage et d’habitation (à préciser)';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_eval_usa_usufruit text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_eval_usa_usufruit IS 'Evaluation de l’usage ou de l’usufruit';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_vente_nue_prop boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_vente_nue_prop IS 'Vente de la nue-propriété';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_vente_nue_prop_prec text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_vente_nue_prop_prec IS 'Vente de la nue-propriété (à préciser)';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_echange boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_echange IS 'Echange';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_design_bien_recus_ech text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_design_bien_recus_ech IS 'Désignation des biens reçus en échange';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_mnt_soulte text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_mnt_soulte IS 'Montant de la soulte le cas échéant';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_prop_contre_echan text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_prop_contre_echan IS 'Propriétaires contre-échangistes';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_apport_societe text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_apport_societe IS 'Apport en société';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_bene text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_bene IS 'Bénéficiaire';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_esti_bien text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_esti_bien IS 'Estimation du bien apporté';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_cess_terr_loc_co boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_cess_terr_loc_co IS 'Cession de tantième de terrains contre remise de locaux à construire';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_esti_terr text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_esti_terr IS 'Estimation du terrain';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_esti_loc text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_esti_loc IS 'Estimation des locaux à remettre';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_esti_imm_loca boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_esti_imm_loca IS 'Location-accession – Estimation de l’immeuble objet de la location-accession';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_adju_vol boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_adju_vol IS 'Volontaire';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_adju_obl boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_adju_obl IS 'Rendue obligatoire par une disposition législative ou réglementaire';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_adju_fin_indivi boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_adju_fin_indivi IS 'Mettant fin à une indivision ne résultant pas d’une donation-partage';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_adju_date_lieu text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_adju_date_lieu IS 'Date et lieu de l’adjudication';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_mod_cess_mnt_mise_prix text;
COMMENT ON COLUMN donnees_techniques.dia_mod_cess_mnt_mise_prix IS 'Montant de la mise à prix';

-- Les soussignés déclarent
--
ALTER TABLE donnees_techniques ADD COLUMN dia_prop_titu_prix_indique boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_prop_titu_prix_indique IS 'Que le(s) propriétaire(s) nommé(s) à la rubrique 1 : Demande(nt) au titulaire du droit de préemption d’acquérir les biens désignés à la rubrique 3 aux prix et conditions indiqués';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_prop_recherche_acqu_prix_indique boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_prop_recherche_acqu_prix_indique IS 'Que le(s) propriétaire(s) nommé(s) à la rubrique 1 : A (ont) recherché un acquéreur disposé à acquérir les biens désignés à la rubrique 3 aux prix et conditions indiqués';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_acquereur_prof text;
COMMENT ON COLUMN donnees_techniques.dia_acquereur_prof IS 'Profession (facultatif)';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_indic_compl_ope text;
COMMENT ON COLUMN donnees_techniques.dia_indic_compl_ope IS 'Indications complémentaires concernant l’opération envisagée par l’acquéreur (facultatif)';
--
ALTER TABLE donnees_techniques ADD COLUMN dia_vente_adju boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.dia_vente_adju IS 'Qu’il est chargé de procéder à la vente par voie d’adjudication comme indiqué à la rubrique F-2 des biens désignés à la rubrique C appartenant a(ux) propriétaire(s) nommé(s) en A';

--
-- END / #28 — Gestion du workflow des DIA
--
