-- Suite au déplacement du code du script spécifique app/getDemandeNatureInfo.php
-- dans une méthode de la classe 'demande_nature', il est nécessaire d'ajouter
-- la permission permettant d'accéder à cette vue pour les profils qui peuvent
-- ajouter ou modifier une demande_type.
INSERT INTO om_droit (om_droit, libelle, om_profil) 
SELECT nextval('om_droit_seq'), 'demande_nature_consulter', om_profil.om_profil
FROM om_profil 
WHERE 
	om_profil IN (
		SELECT om_profil 
		FROM om_droit 
		WHERE libelle='demande_type' OR libelle='demande_type_ajouter' OR libelle='demande_type_modifier'
	)
	AND om_profil NOT IN (
		SELECT om_profil 
		FROM om_droit 
		WHERE libelle='demande_nature_consulter' OR libelle='demande_nature'
	)
;
