--
-- START - Nettoyage des restes d'openFoncier - Décembre 2015
--

DELETE FROM om_etat
WHERE id IN ('registre_dossiers_decisions_par_type_par_date','registre_dossiers_depots_par_type_par_date','registre_dossiers_par_type_par_date');

DELETE FROM om_sousetat
WHERE id IN ('registre_dossiers_decisions_par_type_par_date','registre_dossiers_depots_par_type_par_date','registre_dossiers_par_type_par_date');

--
-- END - Nettoyage des restes d'openFoncier - Décembre 2015
--
