--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.4.0-a1
--
-- @package openfoncier
-- @version SVN : $Id: v3.4.0-a1.sql 1850 2013-05-06 14:39:22Z fmichon $
--------------------------------------------------------------------------------

--
-- Nouvelle fonctionnalité : ajout de la gestion des code-barres pour
-- les courriers de consultation et d'instruction
--
ALTER TABLE consultation ADD COLUMN code_barres character varying(12);
ALTER TABLE instruction ADD COLUMN code_barres character varying(12);
UPDATE instruction SET code_barres = CONCAT('11',LPAD(CAST(instruction as text), 10, '0'));
UPDATE consultation SET code_barres = CONCAT('12',LPAD(CAST(consultation as text),10, '0'));

--
-- Ajout d'une colonne mouvement_sitadel à la table dossier_instruction_type
-- pour le développement de l'export SITADEL
--
ALTER TABLE dossier_instruction_type ADD COLUMN mouvement_sitadel character varying(20);

--
-- Mise à jour des deux requêtes consultation et d'instruction pour l'ajout du
-- champ de fusion code_barres
--
UPDATE om_requete SET requete = '
SELECT

    service.libelle as service,
    service.adresse,
    service.adresse2,
    service.cp,
    service.ville,
    dossier.dossier,
    dossier_autorisation_type_detaille.libelle as nature,
    civilite.libelle as demandeur_civilite,
    to_char(dossier.date_depot,''DD/MM/YYYY'')  as date_depot,
    to_char(dossier.date_rejet,''DD/MM/YYYY'')  as date_rejet,
    to_char(consultation.date_envoi,''DD/MM/YYYY'')  as date_envoi,
    consultation.code_barres as code_barres


FROM
    &DB_PREFIXEconsultation
    LEFT JOIN &DB_PREFIXEservice
        ON service.service=consultation.service
    LEFT JOIN &DB_PREFIXEdossier
        ON dossier.dossier=consultation.dossier
    LEFT JOIN &DB_PREFIXEdossier_autorisation
        ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
    LEFT JOIN &DB_PREFIXEinstructeur
        ON instructeur.instructeur=dossier.instructeur
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur
        ON lien_dossier_demandeur.dossier=dossier.dossier
    LEFT JOIN &DB_PREFIXEdemandeur
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur
    LEFT JOIN &DB_PREFIXEcivilite
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite
    LEFT JOIN &DB_PREFIXEdossier_instruction_type
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille

WHERE
    consultation.consultation = &idx
    AND lien_dossier_demandeur.petitionnaire_principal IS TRUE' WHERE om_requete.code = 'consultation';
UPDATE om_requete SET requete = '
SELECT

    --
    -- CHAMPS DE L''EVENEMENT D''INSTRUCTION

    instruction.complement,
    instruction.complement2,
    instruction.code_barres as code_barres,
    om_lettretype.libelle as instruction_objet,

    --
    -- INFORMATIONS CONCERNANT LE DOSSIER
    --

    dossier.dossier as dossier,
    dossier.dossier_autorisation,

    dossier_autorisation_type_detaille.code as code_type_da_det,
    dossier_autorisation_type_detaille.libelle as libelle_type_da_det,
    dossier_autorisation_type.code as code_type_da,
    dossier_autorisation_type.libelle as libelle_type_da,
    dossier_instruction_type.code as code_type_di,
    dossier_instruction_type.libelle as libelle_type_di,

    avis_decision.libelle as decision,

    --
    -- INSTRUCTEUR
    --

    instructeur.nom as instructeur_nom,
    instructeur.telephone as instructeur_telephone,
    division.code as instructeur_division,
    om_utilisateur.email as instructeur_email,

    --
    -- ADRESSE TERRAIN
    --

    dossier.terrain_adresse_voie_numero,
    dossier.complement as adresse_voie,
    dossier.terrain_adresse_lieu_dit,
    dossier.terrain_adresse_bp,
    dossier.terrain_adresse_code_postal,
    dossier.terrain_adresse_localite as terrain_adresse_ville,
    dossier.terrain_adresse_cedex,

    arrondissement.libelle as arrondissement,

    --
    -- PETITIONNAIRE PRINCIPAL
    --

    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as petitionnaire_principal_nom,
    petitionnaire_principal.numero as petitionnaire_principal_numero,
    petitionnaire_principal.voie as petitionnaire_principal_voie,
    petitionnaire_principal.complement as petitionnaire_principal_complement,
    petitionnaire_principal.lieu_dit as petitionnaire_principal_lieu_dit,
    petitionnaire_principal.bp as petitionnaire_principal_bp,
    petitionnaire_principal.code_postal as petitionnaire_principal_code_postal,
    petitionnaire_principal.localite as petitionnaire_principal_ville,
    petitionnaire_principal.cedex as petitionnaire_principal_cedex,
    petitionnaire_principal.pays as petitionnaire_principal_pays,

    --
    -- DELEGATAIRE
    --

    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as delegataire_nom,
    delegataire.numero as delegataire_numero,
    delegataire.voie as delegataire_voie,
    delegataire.complement as delegataire_complement,
    delegataire.lieu_dit as delegataire_lieu_dit,
    delegataire.bp as delegataire_bp,
    delegataire.code_postal as delegataire_code_postal,
    delegataire.localite as delegataire_ville,
    delegataire.cedex as delegataire_cedex,
    delegataire.pays as delegataire_pays,

    --
    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    --

    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as correspondant_nom,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as correspondant_numero,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as correspondant_voie,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as correspondant_complement,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as correspondant_lieu_dit,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.bp
        ELSE delegataire.bp
    END as correspondant_bp,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as correspondant_code_postal,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as correspondant_ville,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.cedex
        ELSE delegataire.cedex
    END as correspondant_cedex,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as correspondant_pays,

    --
    -- DATES DI
    --

    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite

FROM
    &DB_PREFIXEinstruction
LEFT JOIN
    &DB_PREFIXEom_lettretype
    ON instruction.lettretype = om_lettretype.id and om_lettretype.actif IS TRUE
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        instruction.dossier=dossier.dossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        instruction.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur
LEFT JOIN
    (
    &DB_PREFIXElien_dossier_demandeur AS lien_dossier_delegataire
        JOIN openads.demandeur as delegataire
        ON
            lien_dossier_delegataire.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
    )
    ON
        instruction.dossier = lien_dossier_delegataire.dossier AND lien_dossier_delegataire.petitionnaire_principal IS FALSE
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
WHERE instruction.instruction = ''&idx''', merge_fields = '' WHERE om_requete.code = 'instruction';


---
--- Ajout de la table document_numerise_type
---

CREATE TABLE document_numerise_type(
    document_numerise_type integer NOT NULL,
    code character varying(6) NOT NULL,
    libelle character varying(255) NOT NULL,
    CONSTRAINT document_numerise_type_pkey PRIMARY KEY (document_numerise_type)
);

CREATE SEQUENCE document_numerise_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE document_numerise_type_seq OWNED BY document_numerise_type.document_numerise_type;

INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'APA', 'arrêté participations');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ARA', 'arrêté d''annulation');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ARD', 'arrêté de différé de travaux');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ARE', 'arrêté rectificatif');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ARM', 'arrêté modificatif');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ARN', 'arrêté de transfert de nom');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ARO', 'arrêté de prorogation');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ARR', 'arrêté de conformité');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ARRT', 'arrêté');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ART', 'arrêté retour préfecture');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ARV', 'arrêté de vente par anticipation');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ASPA27', 'étude de sécurité');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ASPA49', 'récépissé de dépôt en préfecture de la demande d''autorisation IGH');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ASPA50', 'dossier spécifique accessibilité ERP');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ASPA51', 'dossier sécurité ERP');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ASPC16', 'étude de sécurité');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ASPC38', 'récépissé de dépôt en préfecture de la demande d''autorisation IGH');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ASPC39', 'dossier spécifique accessibilité ERP');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'ASPC40', 'dossier sécurité ERP');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'AUTG', 'toutes les pièces composant le dossier (A0)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'AUTP', 'toutes les pièces composant le dossier (A3/A4)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'AUTPAG', 'autres pièces composant le dossier (A0)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'AUTPAP', 'autres pièces composant le dossier (A3/A4)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'AUTPCG', 'autres pièces composant le dossier (A0)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'AUTPCP', 'autres pièces composant le dossier (A3/A4)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'AUTPDG', 'autres pièces composant le dossier (A0)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'AUTPDP', 'autres pièces composant le dossier (A3/A4)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'AVIS', 'avis obligatoires');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'CCO', 'certificat conformité totale lotissement');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'CCP', 'certificat conformité partielle lotissement');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'COM', 'passage en commission');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DAACT', 'déclaration attestant l''achèvement et la conformité des travaux');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGCUB', 'toutes les pièces du certificat d''urbanisme opérationnel');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGIMPA', 'Imprimé de demande de permis d''aménager');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGIMPC', 'Imprimé de demande de permis de construire');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGIMPD', 'Imprimé de demande de permis de démolir');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPA01', 'plan de situation');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPA02', 'notice descriptive terrain et projet d''aménagement prévu');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPA03', 'plan de l''état actuel du terrain à aménager et de ses abords');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPA04', 'plan de composition d''ensemble du projet');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPA05', 'vues et coupes du projet dans le profil du terrain naturel');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPA08', 'programme et plans des travaux d''équipement du lotissement');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPA10', 'projet de règlement du lotissement');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPC01', 'plan de situation');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPC02', 'plan de masse des constructions à édifier ou à modifier');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPC03', 'plan en coupe du terrain et de la construction');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPC04', 'notice décrivant le terrain et présentant le projet');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPC32', 'plan de division du terrain');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPD01', 'plan de situation');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPD02', 'plan de masse des constructions à démolir ou à conserver');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPD03', 'photographie du ou des bâtiments à démolir');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPD04', 'notice expliquant pourquoi la conservation du bâtiment ne peut plus être assurée (démolition totale)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPD05', 'photographies des façades et toitures du bâtiment et des dispositions intérieures (démolition totale)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPD06', 'notice expliquant pourquoi la conservation du bâtiment ne peut plus être assurée (démolition partielle)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPD07', 'photographies des façades et toitures du bâtiment et de ses dispositions intérieures (démolition partielle)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPD08', 'descriptif des moyens mis en oeuvre pour éviter toute atteinte aux parties conservées du bâtiment');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPD09', 'photographies de l''ensemble des parties extérieures et intérieures du bâtiment adossées à l''immeuble classé');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DGPD10', 'descriptif des moyens mis en oeuvre pour éviter toute atteinte à l''immeuble classé');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DOC', 'déclaration d''ouverture de chantier');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DOS01', 'autres pièces composant le dossier délivré (A3/A4)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'DOS02', 'autres pièces composant le dossier délivré (A0)');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'INC', 'incomplétude');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'INCCS', 'incomplétude changement usage');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'IRA', 'irrecevabilité d''annulation');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'IRN', 'irrecevabilité de transfert de nom');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'IRO', 'irrecevabilité de prorogation');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'IRR', 'irrecevabilité');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'LIC', 'lettre d''information complémentaire');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'LMA', 'lettre de mise en attente');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'MDR', 'mise en demeure de retrait');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'NDL', 'notification de délai');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'PDL', 'prolongation de délai');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'PRL', 'prolongation de delais');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RAA', 'rapport d''annulation');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RAM', 'rapport modificatif');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RAN', 'rapport de transfert de nom');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RAO', 'rapport de prorogation');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RAP', 'rapport ( ou avis )');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RAR', 'rapport de conformité');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RDA', 'refus de la déclaration attestant l''achèvement et la conformité des travaux');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIN', 'lettre rappel incomplétude');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIPA06', 'photographies du terrain');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIPA07', 'photographie du terrain dans le paysage lointain');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIPA09', 'document graphique des hypothèses d''implantation des bâtiments');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIPA17', 'plan de masse des constructions à édifier ou à modifier');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIPA18', 'plan des façades et des toitures');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIPA19', 'plan en coupe du terrain et de la construction');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIPC05', 'plan des façades et des toitures');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIPC06', 'insertion graphique et photographies du terrain');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIPC07', 'photographie du terrain dans l''environnement proche');
INSERT INTO document_numerise_type VALUES (nextval('document_numerise_type_seq'), 'RIPC08', 'photographie du terrain dans le paysage lointain');

---
--- Ajout de la table document_numerise
---

CREATE TABLE document_numerise(
    document_numerise integer NOT NULL,
    uid character varying(64) NOT NULL,
    dossier character varying(20) NOT NULL,
    CONSTRAINT document_numerise_pkey PRIMARY KEY (document_numerise)
);

ALTER TABLE document_numerise 
    ADD CONSTRAINT document_numerise_dossier_fkey 
        FOREIGN KEY (dossier) REFERENCES dossier(dossier) MATCH FULL 
        ON DELETE SET NULL ON UPDATE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY document_numerise
    ADD CONSTRAINT document_numerise_uid_key UNIQUE (uid);

CREATE SEQUENCE document_numerise_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE document_numerise_seq OWNED BY document_numerise.document_numerise;

-- Ajout de champs concernant le cerfa d'Ouverture de chantier
ALTER TABLE cerfa ADD COLUMN doc_date boolean;
ALTER TABLE cerfa ADD COLUMN doc_tot_trav boolean;
ALTER TABLE cerfa ADD COLUMN doc_tranche_trav boolean;
ALTER TABLE cerfa ADD COLUMN doc_tranche_trav_desc boolean;
ALTER TABLE cerfa ADD COLUMN doc_surf boolean;
ALTER TABLE cerfa ADD COLUMN doc_nb_log boolean;
ALTER TABLE cerfa ADD COLUMN doc_nb_log_indiv boolean;
ALTER TABLE cerfa ADD COLUMN doc_nb_log_coll boolean;
ALTER TABLE cerfa ADD COLUMN doc_nb_log_lls boolean;
ALTER TABLE cerfa ADD COLUMN doc_nb_log_aa boolean;
ALTER TABLE cerfa ADD COLUMN doc_nb_log_ptz boolean;
ALTER TABLE cerfa ADD COLUMN doc_nb_log_autre boolean;

ALTER TABLE donnees_techniques ADD COLUMN doc_date date;
ALTER TABLE donnees_techniques ADD COLUMN doc_tot_trav boolean;
ALTER TABLE donnees_techniques ADD COLUMN doc_tranche_trav boolean;
ALTER TABLE donnees_techniques ADD COLUMN doc_tranche_trav_desc text;
ALTER TABLE donnees_techniques ADD COLUMN doc_surf numeric;
ALTER TABLE donnees_techniques ADD COLUMN doc_nb_log integer;
ALTER TABLE donnees_techniques ADD COLUMN doc_nb_log_indiv integer;
ALTER TABLE donnees_techniques ADD COLUMN doc_nb_log_coll integer;
ALTER TABLE donnees_techniques ADD COLUMN doc_nb_log_lls integer;
ALTER TABLE donnees_techniques ADD COLUMN doc_nb_log_aa integer;
ALTER TABLE donnees_techniques ADD COLUMN doc_nb_log_ptz integer;
ALTER TABLE donnees_techniques ADD COLUMN doc_nb_log_autre integer;

-- Ajout de champs concernant le cerfa d'Achèvement des travaux
ALTER TABLE cerfa ADD COLUMN daact_date boolean;
ALTER TABLE cerfa ADD COLUMN daact_date_chgmt_dest boolean;
ALTER TABLE cerfa ADD COLUMN daact_tot_trav boolean;
ALTER TABLE cerfa ADD COLUMN daact_tranche_trav boolean;
ALTER TABLE cerfa ADD COLUMN daact_tranche_trav_desc boolean;
ALTER TABLE cerfa ADD COLUMN daact_surf boolean;
ALTER TABLE cerfa ADD COLUMN daact_nb_log boolean;
ALTER TABLE cerfa ADD COLUMN daact_nb_log_indiv boolean;
ALTER TABLE cerfa ADD COLUMN daact_nb_log_coll boolean;
ALTER TABLE cerfa ADD COLUMN daact_nb_log_lls boolean;
ALTER TABLE cerfa ADD COLUMN daact_nb_log_aa boolean;
ALTER TABLE cerfa ADD COLUMN daact_nb_log_ptz boolean;
ALTER TABLE cerfa ADD COLUMN daact_nb_log_autre boolean;

ALTER TABLE donnees_techniques ADD COLUMN daact_date date;
ALTER TABLE donnees_techniques ADD COLUMN daact_date_chgmt_dest date;
ALTER TABLE donnees_techniques ADD COLUMN daact_tot_trav boolean;
ALTER TABLE donnees_techniques ADD COLUMN daact_tranche_trav boolean;
ALTER TABLE donnees_techniques ADD COLUMN daact_tranche_trav_desc text;
ALTER TABLE donnees_techniques ADD COLUMN daact_surf numeric;
ALTER TABLE donnees_techniques ADD COLUMN daact_nb_log integer;
ALTER TABLE donnees_techniques ADD COLUMN daact_nb_log_indiv integer;
ALTER TABLE donnees_techniques ADD COLUMN daact_nb_log_coll integer;
ALTER TABLE donnees_techniques ADD COLUMN daact_nb_log_lls integer;
ALTER TABLE donnees_techniques ADD COLUMN daact_nb_log_aa integer;
ALTER TABLE donnees_techniques ADD COLUMN daact_nb_log_ptz integer;
ALTER TABLE donnees_techniques ADD COLUMN daact_nb_log_autre integer;

