--
-- START / CERFA
--

-- cerfa
COMMENT ON COLUMN cerfa.tax_surf_tot_cstr IS 'Surface taxable totale créée de la ou des construction(s), hormis les surfaces de stationnement closes et couvertes (en m²)';
ALTER TABLE cerfa ADD COLUMN tax_surf_loc_stat boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_surf_loc_stat IS 'Surface taxable des locaux clos et couverts à usage de stationnement';
COMMENT ON COLUMN cerfa.tab_tax_su_princ IS 'Tableau : Locaux à usage d’habitation principale et leurs annexes';
ALTER TABLE cerfa ADD COLUMN tax_log_ap_trvx_nb boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_log_ap_trvx_nb IS 'Quel est le nombre de logements après travaux ?';
ALTER TABLE cerfa ADD COLUMN tax_am_statio_ext_cr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_am_statio_ext_cr IS 'Nombre de places de stationnement non couvertes ou non closes';
ALTER TABLE cerfa ADD COLUMN tax_sup_bass_pisc_cr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_sup_bass_pisc_cr IS 'Superficie du bassin de la piscine';
ALTER TABLE cerfa ADD COLUMN tax_empl_ten_carav_mobil_nb_cr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_empl_ten_carav_mobil_nb_cr IS 'Nombre d’emplacements de tentes, de caravanes et de résidences mobiles de loisirs';
ALTER TABLE cerfa ADD COLUMN tax_empl_hll_nb_cr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_empl_hll_nb_cr IS 'Nombre d’emplacements pour les habitations légères de loisirs';
ALTER TABLE cerfa ADD COLUMN tax_eol_haut_nb_cr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_eol_haut_nb_cr IS 'Nombre d’éoliennes dont la hauteur est supérieure à 12 m';
ALTER TABLE cerfa ADD COLUMN tax_pann_volt_sup_cr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_pann_volt_sup_cr IS 'Superficie des panneaux photo-voltaïques posés au sol';
ALTER TABLE cerfa ADD COLUMN tax_surf_loc_arch boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_surf_loc_arch IS 'Surface concernée au titre des locaux (m² de surface taxable créée)';
ALTER TABLE cerfa ADD COLUMN tax_surf_pisc_arch boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_surf_pisc_arch IS 'Surface concernée au titre de la piscine (m² de bassin créé)';
ALTER TABLE cerfa ADD COLUMN tax_am_statio_ext_arch boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_am_statio_ext_arch IS 'Nombre d’emplacements de stationnement concernés';
ALTER TABLE cerfa ADD COLUMN tab_tax_su_parc_statio_expl_comm integer;
COMMENT ON COLUMN cerfa.tab_tax_su_parc_statio_expl_comm IS 'Tableau : Parcs de stationnement couverts faisant l’objet d’une exploitation commerciale';
ALTER TABLE cerfa ADD COLUMN tax_empl_ten_carav_mobil_nb_arch boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_empl_ten_carav_mobil_nb_arch IS 'Nombre d’emplacements de tentes, de caravanes et de résidences mobiles de loisirs concernés';
ALTER TABLE cerfa ADD COLUMN tax_empl_hll_nb_arch boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_empl_hll_nb_arch IS 'Nombre d’emplacements pour les habitations légères de loisirs concernés';
ALTER TABLE cerfa ADD COLUMN tax_eol_haut_nb_arch boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_eol_haut_nb_arch IS 'Nombre d’éoliennes dont la hauteur est supérieure à 12 m concernées';
COMMENT ON COLUMN cerfa.doc_nb_log_aa IS 'Accession Aidée (hors prêt à taux zéro)';
ALTER TABLE cerfa ADD COLUMN ope_proj_div_co boolean DEFAULT false;
COMMENT ON COLUMN cerfa.ope_proj_div_co IS 'Division en vue de construire';
ALTER TABLE cerfa ADD COLUMN ope_proj_div_contr boolean DEFAULT false;
COMMENT ON COLUMN cerfa.ope_proj_div_contr IS 'Division dans une commune qui a institué le contrôle des divisions dans le cadre de l’article L. 111-5-2 du code de l’urbanisme.';
ALTER TABLE cerfa ADD COLUMN tax_desc boolean DEFAULT false;
COMMENT ON COLUMN cerfa.tax_desc IS 'Informations/Explications complémentaires pouvant vous permettre de bénéficier d’impositions plus favorables.';

-- donnees_techniques
COMMENT ON COLUMN donnees_techniques.tax_surf_tot_cstr IS 'Surface taxable totale créée de la ou des construction(s), hormis les surfaces de stationnement closes et couvertes';
ALTER TABLE donnees_techniques ADD COLUMN tax_surf_loc_stat integer;
COMMENT ON COLUMN donnees_techniques.tax_surf_loc_stat IS 'Surface taxable des locaux clos et couverts à usage de stationnement';
-- tableau 'Locaux à usage d’habitation principale et leurs annexes'
ALTER TABLE donnees_techniques ADD COLUMN tax_su_princ_surf_stat1 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf_stat1 IS 'Tableau "Locaux à usage d’habitation principale et leurs annexes" Ligne "Ne bénéficiant pas de prêt aidé" Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_princ_surf_stat2 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf_stat2 IS 'Tableau "Locaux à usage d’habitation principale et leurs annexes" Ligne "Bénéficiant d''un PLAI ou LLTS" Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_princ_surf_stat3 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf_stat3 IS 'Tableau "Locaux à usage d’habitation principale et leurs annexes" Ligne "Beneficiant d''autres prets aides (PLUS, LES, PSLA, PLS, LLS)" Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_princ_surf_stat4 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf_stat4 IS 'Tableau "Locaux à usage d’habitation principale et leurs annexes" Ligne "Beneficiant d''un pret a taux zero plus (PTZ+)" Colonne "Surfaces créées pour le stationnement clos et couvert"';
-- tableau 'Locaux à usage d'habitation secondaire et leurs annexes'
ALTER TABLE donnees_techniques ADD COLUMN tax_su_secon_surf_stat numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_secon_surf_stat IS 'Tableau "Locaux à usage d''habitation secondaire et leurs annexes" Colonne "Surfaces créées pour le stationnement clos et couvert"';
-- tableau 'Locaux à usage d’hébergement et leurs annexes '
ALTER TABLE donnees_techniques ADD COLUMN tax_su_heber_surf_stat1 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_heber_surf_stat1 IS 'Tableau "Locaux à usage d’hébergement et leurs annexes" Ligne "Ne bénéficiant pas de prêt aidé" Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_heber_surf_stat2 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_heber_surf_stat2 IS 'Tableau "Locaux à usage d’hébergement et leurs annexes" Ligne "Bénéficiant d''un PLAI ou LLTS" Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_heber_surf_stat3 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_heber_surf_stat3 IS 'Tableau "Locaux à usage d’hébergement et leurs annexes" Ligne "Bénéficiant d''autres prêts aidés" Colonne "Surfaces créées pour le stationnement clos et couvert"';
-- tableau 'Nombre total de logements'
ALTER TABLE donnees_techniques ADD COLUMN tax_su_tot_surf_stat numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_tot_surf_stat IS 'Tableau "Nombre total de logements" Colonne "Surfaces créées pour le stationnement clos et couvert"';
-- tableau 'Création ou extension de locaux non destinés à l’habitation'
ALTER TABLE donnees_techniques ADD COLUMN tax_su_non_habit_surf_stat1 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_stat1 IS 'Tableau "Création ou extension de locaux non destinés à l''habitation" Ligne "Total des surfaces crées ou supprimées, y compris les surfaces des annexes" Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_non_habit_surf_stat2 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_stat2 IS 'Tableau "Création ou extension de locaux non destinés à l''habitation" Ligne "Locaux industriels et leurs annexes" Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_non_habit_surf_stat3 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_stat3 IS 'Tableau "Création ou extension de locaux non destinés à l''habitation" Ligne "Locaux artisanaux et leurs annexes" Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_non_habit_surf_stat4 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_stat4 IS 'Tableau "Création ou extension de locaux non destinés à l''habitation" Ligne "Entrepôts et hangars faisant l''objet d''une exploitation commerciale et non ouverts au public" Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_non_habit_surf_stat5 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_stat5 IS 'Tableau "Création ou extension de locaux non destinés à l''habitation" Ligne "Parc de stationnement couverts faisant l''objet d''une exploitation commerciale" Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_non_habit_surf_stat6 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_stat6 IS 'Tableau "Création ou extension de locaux non destinés à l''habitation" Ligne "Dans les exploitations et coopératives agricoles..." Colonne "Surfaces créées pour le stationnement clos et couvert"';
ALTER TABLE donnees_techniques ADD COLUMN tax_su_non_habit_surf_stat7 numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_stat7 IS 'Tableau "Création ou extension de locaux non destinés à l''habitation" Ligne "Dans les centres équestres : Surfaces de plancher affectées aux seules activités équestres" Colonne "Surfaces créées pour le stationnement clos et couvert"';
-- tableau 'Parcs de stationnement couverts faisant l’objet d’une exploitation
-- commerciale'
ALTER TABLE donnees_techniques ADD COLUMN tax_su_parc_statio_expl_comm_surf numeric;
COMMENT ON COLUMN donnees_techniques.tax_su_parc_statio_expl_comm_surf IS 'Tableau : Parcs de stationnement couverts faisant l’objet d’une exploitation commerciale';
--
ALTER TABLE donnees_techniques ADD COLUMN tax_log_ap_trvx_nb integer;
COMMENT ON COLUMN donnees_techniques.tax_log_ap_trvx_nb IS 'Quel est le nombre de logements apres travaux ?';
ALTER TABLE donnees_techniques ADD COLUMN tax_am_statio_ext_cr integer;
COMMENT ON COLUMN donnees_techniques.tax_am_statio_ext_cr IS 'Nombre de places de stationnement non couvertes ou non closes';
ALTER TABLE donnees_techniques ADD COLUMN tax_sup_bass_pisc_cr numeric;
COMMENT ON COLUMN donnees_techniques.tax_sup_bass_pisc_cr IS 'Superficie du bassin de la piscine';
ALTER TABLE donnees_techniques ADD COLUMN tax_empl_ten_carav_mobil_nb_cr integer;
COMMENT ON COLUMN donnees_techniques.tax_empl_ten_carav_mobil_nb_cr IS 'Nombre d’emplacements de tentes, de caravanes et de résidences mobiles de loisirs';
ALTER TABLE donnees_techniques ADD COLUMN tax_empl_hll_nb_cr integer;
COMMENT ON COLUMN donnees_techniques.tax_empl_hll_nb_cr IS 'Nombre d’emplacements pour les habitations légères de loisirs';
ALTER TABLE donnees_techniques ADD COLUMN tax_eol_haut_nb_cr integer;
COMMENT ON COLUMN donnees_techniques.tax_eol_haut_nb_cr IS 'Nombre d’éoliennes dont la hauteur est supérieure à 12 m';
ALTER TABLE donnees_techniques ADD COLUMN tax_pann_volt_sup_cr numeric;
COMMENT ON COLUMN donnees_techniques.tax_pann_volt_sup_cr IS 'Superficie des panneaux photo-voltaïques posés au sol';
ALTER TABLE donnees_techniques ADD COLUMN tax_surf_loc_arch numeric;
COMMENT ON COLUMN donnees_techniques.tax_surf_loc_arch IS 'Surface concernée au titre des locaux (m² de surface taxable créée)';
ALTER TABLE donnees_techniques ADD COLUMN tax_surf_pisc_arch numeric;
COMMENT ON COLUMN donnees_techniques.tax_surf_pisc_arch IS 'Surface concernée au titre de la piscine (m² de bassin créé)';
ALTER TABLE donnees_techniques ADD COLUMN tax_am_statio_ext_arch integer;
COMMENT ON COLUMN donnees_techniques.tax_am_statio_ext_arch IS 'Nombre d’emplacements de stationnement concernés';
ALTER TABLE donnees_techniques ADD COLUMN tax_empl_ten_carav_mobil_nb_arch integer;
COMMENT ON COLUMN donnees_techniques.tax_empl_ten_carav_mobil_nb_arch IS 'Nombre d’emplacements de tentes, de caravanes et de résidences mobiles de loisirs concernés';
ALTER TABLE donnees_techniques ADD COLUMN tax_empl_hll_nb_arch integer;
COMMENT ON COLUMN donnees_techniques.tax_empl_hll_nb_arch IS 'Nombre d’emplacements pour les habitations légères de loisirs concernés';
ALTER TABLE donnees_techniques ADD COLUMN tax_eol_haut_nb_arch integer;
COMMENT ON COLUMN donnees_techniques.tax_eol_haut_nb_arch IS 'Nombre d’éoliennes dont la hauteur est supérieure à 12 m concernées';
COMMENT ON COLUMN donnees_techniques.doc_nb_log_aa IS 'Accession Aidée (hors prêt à taux zéro)';
ALTER TABLE donnees_techniques ADD COLUMN ope_proj_div_co boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.ope_proj_div_co IS 'Division en vue de construire';
ALTER TABLE donnees_techniques ADD COLUMN ope_proj_div_contr boolean DEFAULT false;
COMMENT ON COLUMN donnees_techniques.ope_proj_div_contr IS 'Division dans une commune qui a institué le contrôle des divisions dans le cadre de l’article L. 111-5-2 du code de l’urbanisme.';
ALTER TABLE donnees_techniques ADD COLUMN tax_desc text;
COMMENT ON COLUMN donnees_techniques.tax_desc IS 'Informations/Explications complémentaires pouvant vous permettre de bénéficier d’impositions plus favorables.';

--
-- END / CERFA
--

-- Correction de l'affichage de la description du projet en plusieurs fois
UPDATE om_lettretype SET 
    titre_om_htmletat = replace(titre_om_htmletat, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
UPDATE om_lettretype SET 
    titre_om_htmletat = replace(titre_om_htmletat, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
UPDATE om_lettretype SET 
    titre_om_htmletat = replace(titre_om_htmletat, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
UPDATE om_lettretype SET 
    corps_om_htmletatex = replace(corps_om_htmletatex, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
UPDATE om_lettretype SET 
    corps_om_htmletatex = replace(corps_om_htmletatex, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
UPDATE om_lettretype SET 
    corps_om_htmletatex = replace(corps_om_htmletatex, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');

UPDATE om_etat SET 
    titre_om_htmletat = replace(titre_om_htmletat, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
UPDATE om_etat SET 
    titre_om_htmletat = replace(titre_om_htmletat, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
UPDATE om_etat SET 
    titre_om_htmletat = replace(titre_om_htmletat, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
UPDATE om_etat SET 
    corps_om_htmletatex = replace(corps_om_htmletatex, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
UPDATE om_etat SET 
    corps_om_htmletatex = replace(corps_om_htmletatex, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
UPDATE om_etat SET 
    corps_om_htmletatex = replace(corps_om_htmletatex, '[projet_desc_donnees_techniques][projet_desc_donnees_techniques]', '[projet_desc_donnees_techniques]');
