--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.9.0-a2
--
-- XXX Ce fichier doit être renommé en v3.9.0-a2.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.9.0-a2.sql 2341 2013-09-20 18:00:59Z vpihour $
--------------------------------------------------------------------------------

---
--- Ajout de la table dossier_parcelle
---
CREATE TABLE dossier_parcelle (
    dossier_parcelle integer NOT NULL,
    dossier character varying(30) NOT NULL,
    parcelle character varying(20) DEFAULT NULL,
    libelle character varying(20)
);

---
--- Ajout de la clé primaire de la table dossier_parcelle
---
ALTER TABLE ONLY dossier_parcelle
    ADD CONSTRAINT dossier_parcelle_pkey PRIMARY KEY (dossier_parcelle);

---
--- Ajout des clés étrangères de la table dossier_parcelle
---
ALTER TABLE ONLY dossier_parcelle
    ADD CONSTRAINT dossier_parcelle_dossier_fkey FOREIGN KEY (dossier) REFERENCES dossier(dossier);
ALTER TABLE ONLY dossier_parcelle
    ADD CONSTRAINT dossier_parcelle_parcelle_fkey FOREIGN KEY (parcelle) REFERENCES parcelle(parcelle);

---
--- Ajout de la séquence de la table dossier_parcelle
---
CREATE SEQUENCE dossier_parcelle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

---
--- Ajout de l'option sig dans om_parametre
---
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'), 'option_sig', 'aucun', 1
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'option_sig'
    );


--
-- Mise à niveau openmairie_exemple 4.4.0
--
ALTER TABLE om_collectivite ALTER COLUMN om_collectivite TYPE integer;
ALTER TABLE om_droit ALTER COLUMN om_droit TYPE integer;
ALTER TABLE om_utilisateur ALTER COLUMN om_utilisateur TYPE integer;
ALTER TABLE om_utilisateur ALTER COLUMN om_collectivite TYPE integer;
ALTER TABLE om_etat ALTER COLUMN om_etat TYPE integer;
ALTER TABLE om_etat ALTER COLUMN om_collectivite TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN om_lettretype TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN om_collectivite TYPE integer;
ALTER TABLE om_parametre ALTER COLUMN om_parametre TYPE integer;
ALTER TABLE om_parametre ALTER COLUMN om_collectivite TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN om_sousetat TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN om_collectivite TYPE integer;

ALTER TABLE om_etat ALTER COLUMN logoleft TYPE integer;
ALTER TABLE om_etat ALTER COLUMN logotop TYPE integer;
ALTER TABLE om_etat ALTER COLUMN titreleft TYPE integer;
ALTER TABLE om_etat ALTER COLUMN titretop TYPE integer;
ALTER TABLE om_etat ALTER COLUMN titrelargeur TYPE integer;
ALTER TABLE om_etat ALTER COLUMN titrehauteur TYPE integer;
ALTER TABLE om_etat ALTER COLUMN titretaille TYPE integer;
ALTER TABLE om_etat ALTER COLUMN corpsleft TYPE integer;
ALTER TABLE om_etat ALTER COLUMN corpstop TYPE integer;
ALTER TABLE om_etat ALTER COLUMN corpslargeur TYPE integer;
ALTER TABLE om_etat ALTER COLUMN corpshauteur TYPE integer;
ALTER TABLE om_etat ALTER COLUMN corpstaille TYPE integer;
ALTER TABLE om_etat ALTER COLUMN se_margeleft TYPE integer;
ALTER TABLE om_etat ALTER COLUMN se_margetop TYPE integer;
ALTER TABLE om_etat ALTER COLUMN se_margeright TYPE integer;

ALTER TABLE om_lettretype ALTER COLUMN logoleft TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN logotop TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN titreleft TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN titretop TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN titrelargeur TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN titrehauteur TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN titretaille TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN corpsleft TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN corpstop TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN corpslargeur TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN corpshauteur TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN corpstaille TYPE integer;

ALTER TABLE om_sousetat ALTER COLUMN titrehauteur TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN titretaille TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN intervalle_debut TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN intervalle_fin TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN entete_hauteur TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN tableau_largeur TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN tableau_fontaille TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_hauteur TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_fontaille_total TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_hauteur_total TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_fontaille_moyenne TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_hauteur_moyenne TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_fontaille_nbr TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_hauteur_nbr TYPE integer;

ALTER TABLE om_sig_map ALTER COLUMN actif TYPE boolean USING CASE WHEN actif='Oui' THEN true ELSE false END;
ALTER TABLE om_parametre ALTER COLUMN libelle TYPE character varying(100);


---
---Suppression de la table om_paramètre des données inutiles
---
DELETE FROM om_parametre WHERE libelle = 'champ_remplacement_etat' OR libelle = 'champ_remplacement_lettretype';

---
---Remise en forme des champs de fusion
---
UPDATE om_requete SET 
requete = 'SELECT
    --Données générales du rapport d''instruction
    rapport_instruction.dossier_instruction as dossier_instruction_rapport_instruction, 
    analyse_reglementaire as analyse_reglementaire_rapport_instruction,
    description_projet as description_projet_rapport_instruction,
    proposition_decision as proposition_decision_rapport_instruction, 

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier, 
    etat as etat_dossier,
    pos as pos_dossier, 
    servitude as servitude_dossier, 
    dossier.delai as delai_dossier,
    dossier_autorisation_type_detaille.libelle as libelle_datd, 
    avis_decision.libelle as libelle_avis_decision,

    --Adresse du terrain dossier d''instruction
    terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    terrain_adresse_localite as terrain_adresse_localite_dossier,
    terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    terrain_adresse_bp as terrain_adresse_bp_dossier,
    terrain_adresse_cedex as terrain_adresse_cedex_dossier,
    terrain_superficie as terrain_superficie_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    
    --Coordonnées du demandeur
    civilite.code as code_civilite,
    particulier_nom as particulier_nom_demandeur,
    particulier_prenom as particulier_prenom_demandeur,
    personne_morale_denomination as personne_morale_denomination_demandeur,
    personne_morale_raison_sociale as personne_morale_raison_sociale_demandeur,
    personne_morale_siret as personne_morale_siret_demandeur,
    personne_morale_nom as personne_morale_nom_demandeur,
    personne_morale_prenom as personne_morale_prenom_demandeur,
    numero as numero_demandeur,
    voie as voie_demandeur,
    demandeur.complement as complement_demandeur,
    lieu_dit as lieu_dit_demandeur,
    localite as localite_demandeur,
    code_postal as code_postal_demandeur,
    bp as bp_demandeur,
    cedex as cedex_demandeur,
    
    --Nom de l''instructeur
    instructeur.nom as nom_instructeur, 

    --Noms des signataires
    division.chef as chef_division,
    direction.chef as chef_direction,

    --Données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9) as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques


FROM 

    &DB_PREFIXErapport_instruction 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=rapport_instruction.dossier_instruction 
    LEFT JOIN &DB_PREFIXEavis_decision 
        ON dossier.avis_decision = avis_decision.avis_decision
    LEFT JOIN &DB_PREFIXEdonnees_techniques
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdivision
        ON dossier.division = division.division
    LEFT JOIN &DB_PREFIXEdirection
        ON division.direction = direction.direction
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur 
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
    
WHERE 

    rapport_instruction = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE',
merge_fields = '--Données générales du rapport d''instruction
[dossier_instruction_rapport_instruction]    [analyse_reglementaire_rapport_instruction]
[description_projet_rapport_instruction]    [proposition_decision_rapport_instruction]

--Données générales du dossier d''instruction
[libelle_dossier]    [etat_dossier]
[pos_dossier]    [servitude_dossier]
[delai_dossier]    [libelle_datd]
[libelle_avis_decision]

--Adresse du terrain dossier d''instruction
[terrain_adresse_voie_numero_dossier]     [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_cedex_dossier]    [terrain_superficie_dossier]
[terrain_references_cadastrales_dossier]

--Coordonnées du demandeur
[code_civilite]
[particulier_nom_demandeur]    [particulier_prenom_demandeur]    
[personne_morale_denomination_demandeur]    [personne_morale_raison_sociale_demandeur]    [personne_morale_siret_demandeur]
[personne_morale_nom_demandeur]    [personne_morale_prenom_demandeur]
[numero_demandeur]    [voie_demandeur]
[complement_demandeur]    [lieu_dit_demandeur]
[code_postal_demandeur]    [localite_demandeur]   [bp_demandeur]    [cedex_demandeur]

--Nom de l''instructeur
[nom_instructeur]

--Noms des signataires
[chef_division]
[chef_direction]

--Données techniques
[co_projet_desc_donnees_techniques]    [am_projet_desc_donnees_techniques]
[dm_projet_desc_donnees_techniques]    
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]    
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]    
[tab_surface_donnees_techniques]
[co_tot_log_nb_donnees_techniques]     [co_statio_place_nb_donnees_techniques]'
WHERE code = 'rapport_instruction';

UPDATE om_etat SET 
titre = '&ville
[libelle_datd] : [etat_dossier]',
corps = 'DOSSIER N [dossier_instruction_rapport_instruction]
NOM du DEMANDEUR TITULAIRE
[code_civilite] [particulier_nom_demandeur] [particulier_prenom_demandeur] [personne_morale_nom_demandeur] [personne_morale_nom_demandeur]
[personne_morale_denomination_demandeur] [personne_morale_raison_sociale_demandeur] [personne_morale_siret_demandeur]

ADRESSE DU DEMANDEUR TITULAIRE: [numero_demandeur] [voie_demandeur] [complement_demandeur] [lieu_dit_demandeur] - [code_postal_demandeur] [localite_demandeur] [cedex_demandeur] [bp_demandeur]

NATURE DES TRAVAUX: [libelle_datd]


ADRESSE DES TRAVAUX: [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] - [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier] [terrain_adresse_bp_dossier]

DESTIONATION en m2 : [terrain_superficie_dossier]        SECTEUR POS :         [pos_dossier]

[servitude_dossier]

[description_projet_rapport_instruction]

[analyse_reglementaire_rapport_instruction]

&rapport_instruction_consultation

En CONSÉQUENCE, il est propose : [proposition_decision_rapport_instruction]

Fait le: &aujourdhui         Vu le:

                         Le technicien instructeur                         Le chef de subdivision


                         [chef_division]'
WHERE id = 'rapport_instruction';

UPDATE om_requete SET 
requete = 'SELECT
    --Données générales de la commission
    libelle as libelle_commission, 
    to_char(date_commission,''DD/MM/YYYY'') as date_commission, 
    lieu_salle as lieu_salle_commission,
    heure_commission as heure_commission, 
    lieu_adresse_ligne1 as lieu_adresse_ligne1_commission, 
    lieu_adresse_ligne2 as lieu_adresse_ligne2_commission
FROM
    &DB_PREFIXEcommission
WHERE
    commission = &idx',
merge_fields = '--Données générales de la commission
[libelle_commission]
[date_commission]
[lieu_salle_commission]
[heure_commission]
[lieu_adresse_ligne1_commission]
[lieu_adresse_ligne2_commission]'
WHERE code = 'commission';

UPDATE om_etat SET 
corps = 'Direction de l''Aménagement Durable et de l''Urbanisme
ORDRE DU JOUR DE LA [libelle_commission] DU [date_commission]
[lieu_salle_commission] à [heure_commission]
[lieu_adresse_ligne1_commission] [lieu_adresse_ligne2_commission]'
WHERE id = 'commission_proposition_ordre_jour';

UPDATE om_etat SET 
corps = 'Direction de l''Aménagement Durable et de l''Urbanisme
ORDRE DU JOUR DE LA [libelle_commission] DU [date_commission]
[lieu_salle_commission] à [heure_commission]
[lieu_adresse_ligne1_commission] [lieu_adresse_ligne2_commission]'
WHERE id = 'commission_ordre_jour';

UPDATE om_etat SET 
corps = 'Direction de l''Aménagement Durable et de l''Urbanisme
ORDRE DU JOUR DE LA [libelle_commission] DU [date_commission]
[lieu_salle_commission] à [heure_commission_commission]
[lieu_adresse_ligne1_commission] [lieu_adresse_ligne2_commission]'
WHERE id = 'commission_compte_rendu';

UPDATE om_requete SET 
requete = 'SELECT

    -- Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,

    avis_decision.libelle as libelle_avis_decision,

    -- Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    -- Noms des signataires
    division.chef as division_chef,
    direction.chef as direction_chef,

    -- Adresse du terrain du dossier d''instruction
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    dossier.terrain_adresse_bp as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    dossier.terrain_adresse_cedex as terrain_adresse_cedex_dossier,

    arrondissement.libelle as libelle_arrondissement,

    -- Coordonnées du pétitionnaire principal
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as nom_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    petitionnaire_principal.bp as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    petitionnaire_principal.cedex as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    -- Coordonnées du délégataire
    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as nom_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complement_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    delegataire.bp as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    delegataire.cedex as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.bp
        ELSE delegataire.bp
    END as bp_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.cedex
        ELSE delegataire.cedex
    END as cedex_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    -- Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    
    -- Données générales des données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9) as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques

FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as delegataire
    ON
        lien_dossier_demandeur.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN
    &DB_PREFIXEdirection
    ON
        division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
WHERE dossier.dossier = ''&idx''',
merge_fields = '-- Données générales du dossier d''instruction
[libelle_dossier]    
[libelle_da]    
[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]
[delai_dossier]
[terrain_references_cadastrales_dossier]
[libelle_avis_decision]

-- Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

-- Noms des signataires
[division_chef]
[direction_chef]

-- Adresse du terrain du dossier d''instruction
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]
[libelle_arrondissement]

-- Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

-- Coordonnées du délégataire
[nom_delegataire]
[numero_delegataire]    [voie_delegataire]    [complement_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire]

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant]

-- Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]

-- Données générales des données techniques
[co_projet_desc_donnees_techniques]    [co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_projet_desc_donnees_techniques]    [am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
[dm_projet_desc_donnees_techniques]
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]'
WHERE code = 'dossier';

UPDATE om_etat SET 
titre = '[libelle_datd]
RECAPITULATIF AU &aujourdhui
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]'
WHERE id = 'dossier';

UPDATE om_requete SET 
requete = 'SELECT 
    --Coordonnées du service
    service.libelle as libelle_service, 
    service.adresse as adresse_service, 
    service.adresse2 as adresse2_service, 
    service.cp as cp_service, 
    service.ville as ville_service,

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    dossier_autorisation_type_detaille.libelle as libelle_datd, 

    --Adresse du terrain du dossier d''instruction
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,

    --Coordonnées du demandeur
    civilite.libelle as civilite_demandeur,
    CASE WHEN demandeur.qualite=''particulier''
        THEN TRIM(CONCAT(demandeur.particulier_nom, '' '', demandeur.particulier_prenom))
        ELSE TRIM(CONCAT(demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_denomination))
    END as nom_demandeur,
    CONCAT(demandeur.numero, '' '', demandeur.voie) as adresse_demandeur,
    demandeur.code_postal as code_postal_demandeur,
    demandeur.localite as ville_demandeur,
    demandeur.personne_morale_denomination as societe_demandeur,

    --Dates importantes du dossier d''instruction
    to_char(dossier.date_depot,''DD/MM/YYYY'') as date_depot_dossier, 
    to_char(dossier.date_rejet,''DD/MM/YYYY'') as date_rejet_dossier, 
    to_char(consultation.date_envoi,''DD/MM/YYYY'') as date_envoi_dossier, 

    --Code barres de la consultation
    consultation.code_barres as code_barres_consultation,

    --Données générales des données techniques
    donnees_techniques.co_projet_desc as co_prejet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9) as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_station_place_nb_donnees_techniques
    
FROM 


    &DB_PREFIXEconsultation 
    LEFT JOIN &DB_PREFIXEservice 
        ON service.service=consultation.service 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=consultation.dossier 
    LEFT JOIN &DB_PREFIXEdonnees_techniques 
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdossier_autorisation 
        ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation 
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        
         
WHERE 


    consultation.consultation = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE',
merge_fields = '--Coordonnées du service
[libelle_service] 
[adresse_service] 
[adresse2_service] 
[cp_service]    [ville_service]

--Données générales du dossier d''instruction
[libelle_dossier]    [terrain_references_cadastrales_dossier]
[libelle_datd] 

--Adresse du terrain du dossier d''instruction
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier

--Coordonnées du demandeur
[civilite_demandeur]    [nom_demandeur]
[adresse_demandeur]
[code_postal_demandeur]    [ville_demandeur]
[societe_demandeur]

--Dates importantes du dossier d''instruction
[date_depot_dossier] 
[date_rejet_dossier] 
[date_envoi_dossier] 

--Code barres de la consultation
[code_barres_consultation]

--Données générales des données techniques
[co_prejet_desc_donnees_techniques]    [co_tot_log_nb_donnees_techniques]    [co_station_place_nb_donnees_techniques]
[am_projet_desc_donnees_techniques]    [am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
[dm_projet_desc_donnees_techniques]
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]'
WHERE code = 'consultation';

UPDATE om_etat SET 
corps = '<b>[libelle_datd]</b>
Avis sollicité
____________________________________________________________________________________________________________
  Dossier numéro        <b>&departement &commune  [libelle_dossier]</b>  déposé le <b>[date_depot_dossier]</b>
      
                       par         <b>[civilite_demandeur] [nom_demandeur]</b>
        demeurant à         [adresse_demandeur]   [code_postal_demandeur] - [ville_demandeur]
                société         [societe_demandeur]
        sur le terrain        <b>[terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]  [terrain_adresse_code_postal_dossier] -  [terrain_adresse_localite_dossier]</b>
____________________________________________________________________________________________________________
  PJ en communication : 1 exemplaire du dossier





Le respect de la réglementation en vigueur, notamment en matière de délai de réponse ou de décision de l''administration, me conduit à attirer votre attention sur le fait qu''en l absence de réponse dans un délai de 1 mois à dater de la réception de la demande jointe, votre service est reputé émettre un avis favorable sur ladite demande.

Il conviendra cependant, même dans cette éventualité, de me faire retour de l''exemplaire communiqué, dans les meilleurs délais.
 
Je vous prie d''agréer, Monsieur, l''expression de ma considération distinguée.                



                                                                                                              &ville, le [date_envoi_dossier]
                                                                                                              le Maire &delaville


                                                                                                              &nom



RÉFÉRENCES
PJ EN RETOUR : l''exemplaire du dossier communiqué'
WHERE id = 'consultation_avec_avis_attendu';

UPDATE om_etat SET 
corps = '<b>[libelle_datd]</b>
Pour information
____________________________________________________________________________________________________________
  Dossier numéro        <b>&departement &commune  [libelle_dossier]</b>  déposé le <b>[date_depot_dossier]</b>
      
                       par         <b>[civilite_demandeur] [nom_demandeur]</b>
        demeurant à         [adresse_demandeur]   [code_postal_demandeur] - [ville_demandeur]
                société         [societe_demandeur]
        sur le terrain        <b>[terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]  [terrain_adresse_code_postal_dossier] -  [terrain_adresse_localite_dossier]</b>
____________________________________________________________________________________________________________
  PJ en communication : 1 exemplaire du dossier





Le respect de la réglementation en vigueur, notamment en matière de délai de réponse ou de décision de l''administration, me conduit à attirer votre attention sur le fait qu''en l absence de réponse dans un délai de 1 mois à dater de la réception de la demande jointe, votre service est reputé émettre un avis favorable sur ladite demande.

Il conviendra cependant, même dans cette éventualité, de me faire retour de l''exemplaire communiqué, dans les meilleurs délais.

Je vous prie d''agréer, [civilite_demandeur] [nom_demandeur], l''expression de ma considération distinguée.                




                                                                                                              &ville, le [date_envoi_dossier]
                                                                                                              le Maire &delaville


                                                                                                              &nom



RÉFÉRENCES
PJ EN RETOUR : l''exemplaire du dossier communiqué'
WHERE id = 'consultation_pour_information';

UPDATE om_etat SET 
corps = '<b>[libelle_datd]</b>
Avis solicité pour conformité
____________________________________________________________________________________________________________
  Dossier numéro        <b>&departement &commune  [libelle_dossier]</b>  déposé le <b>[date_depot_dossier]</b>
      
                       par         <b>[civilite_demandeur] [nom_demandeur]</b>
        demeurant à         [adresse_demandeur]   [code_postal_demandeur] - [ville_demandeur]
                société         [societe_demandeur]
        sur le terrain        <b>[terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]  [terrain_adresse_code_postal_dossier] -  [terrain_adresse_localite_dossier]</b>
____________________________________________________________________________________________________________
  PJ en communication : 1 exemplaire du dossier





Le respect de la réglementation en vigueur, notamment en matière de délai de réponse ou de décision de l''administration, me conduit à attirer votre attention sur le fait qu''en l absence de réponse dans un délai de 1 mois à dater de la réception de la demande jointe, votre service est réputé émettre un avis favorable sur ladite demande.

Il conviendra cependant, même dans cette éventualité, de me faire retour de l''exemplaire communiqué, dans les meilleurs délais.
 
Je vous prie d''agréer, Monsieur, l''expression de ma considération distinguée.                



                                                                                                              &ville, le [date_envoi_dossier]
                                                                                                              le Maire &delaville


                                                                                                              &nom



RÉFÉRENCES
PJ EN RETOUR : l''exemplaire du dossier communiqué'
WHERE id = 'consultation_pour_conformite';

UPDATE om_requete SET 
requete = 'SELECT

    --Données générales de l''événement d''instruction
    instruction.complement as complement_instruction,
    instruction.complement2 as complement2_instruction,
    instruction.code_barres as code_barres_instruction,
    om_lettretype.libelle as libelle_om_lettretype,

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,

    avis_decision.libelle as libelle_avis_decision,

    --Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    --Adresse du terrain du dossier d''instruction 
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    dossier.terrain_adresse_bp as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    dossier.terrain_adresse_cedex as terrain_adresse_cedex_dossier,

    arrondissement.libelle as libelle_arrondissement,

    --Coordonnées du pétitionnaire principale
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as nom_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    petitionnaire_principal.bp as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    petitionnaire_principal.cedex as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du délégataire
    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as nom_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complemennt_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    delegataire.bp as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    delegataire.cedex as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.bp
        ELSE delegataire.bp
    END as bp_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.cedex
        ELSE delegataire.cedex
    END as cedex_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    --Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    
    --Noms des signataires
    CONCAT(signataire_arrete.prenom, '' '', signataire_arrete.nom) as arrete_signataire,
    division.chef as chef_division,
    direction.chef as chef_direction,
    
    --Données générales des données techniquesDONNÉES TECHNIQUES
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9) as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques
    
FROM
    &DB_PREFIXEinstruction
LEFT JOIN
    &DB_PREFIXEsignataire_arrete
    ON instruction.signataire_arrete = signataire_arrete.signataire_arrete
LEFT JOIN
    &DB_PREFIXEom_lettretype
    ON instruction.lettretype = om_lettretype.id and om_lettretype.actif IS TRUE
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        instruction.dossier=dossier.dossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        instruction.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur
LEFT JOIN
    (
    &DB_PREFIXElien_dossier_demandeur AS lien_dossier_delegataire
        JOIN &DB_PREFIXEdemandeur as delegataire
        ON
            lien_dossier_delegataire.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
    )
    ON
        instruction.dossier = lien_dossier_delegataire.dossier AND lien_dossier_delegataire.petitionnaire_principal IS FALSE
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN 
    &DB_PREFIXEdirection
    ON division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
WHERE instruction.instruction = &idx',
merge_fields = '--Données générales de l''événement d''instruction
[complement_instruction]
[complement2_instruction]
[code_barres_instruction]
[libelle_om_lettretype]

--Données générales du dossier d''instruction
[libelle_dossier]    [delai_dossier]    [terrain_references_cadastrales_dossier]
[libelle_da]

[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]

[libelle_avis_decision]

--Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

--Adresse du terrain du dossier d''instruction 
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]

[libelle_arrondissement]

--Coordonnées du pétitionnaire principale
[nom_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées du délégataire
[nom_delegataire]
[numero_delegataire]    [voie_delegataire]    [complemennt_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire]

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant]

--Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]

--Noms des signataires
[arrete_signataire]
[chef_division]
[chef_direction]

--Données générales des données techniquesDONNÉES TECHNIQUES
[co_projet_desc_donnees_techniques]    [co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_projet_desc_donnees_techniques]    [am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
[dm_projet_desc_donnees_techniques]
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]'
WHERE code = 'instruction';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

Vous avez déposé ce jour une demande de [libelle_datd]. Le délai d''instruction de votre dossier est de [delai_dossier] mois. en l''absence de courrier de la part de l''administration dans ce délai, vous bénéficierez d''un [libelle_datd] tacite(1).

* Toutefois, dans le mois qui suit le dépôt de votre dossier, l''administration peut vous écrire :
   - soit pour vous avertir qu''un autre délai est applicable, lorsque le code de l''urbanisme l''a prévu pour permettre les consultations nécessaires (si votre projet nécessite la consultation d''autres services ...)
   - soit pour vous indiquer qu''il manque une ou plusieurs pièces à votre dossier,
   - soit pour vous informer que votre projet  correspond à un des cas où un accord tacite n''est pas possible.

* Si vous recevez une telle lettre avant la fin du premier mois, celle-ci remplacera le premier récépissé.

* Si vous n''avez rien reçu à la fin du premier mois suivant le dépôt, le délai de [delai_dossier] mois ne pourra être modifié. Si aucun courrier de l''administration ne vous est parvenu à l''issu de ce délai de [delai_dossier] mois, vous pourrez commencer les travaux (2) après avoir :
   - adresser au maire, en 3 exemplaires, une déclaration d''ouverture de chantier (vous trouverez un modèle de déclaration CERFA n° 13407 01 à la mairie ou sur le site internet urbanisme du gouvernement)
   - afficher sur le terrain ce recepissé sur lequel la mairie a mis son cachet pour attester la date de dépôt ;
   - installer sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie publique décrivant le projet (vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux).

* ATTENTION : le permis n''est définitif qu''en l''absence de recours ou de retrait :
   - dans un délai de 2 mois à compter de l''affichage sur le terrain, sa légalité peut être contestée par un tiers. Dans ce cas l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours.
    - dans un délai de 3 mois après la date de [libelle_datd], l''autorité compétente peut le retirer, si elle l''estime illégal. Elle est tenue de vous en informer préalablement et de vous permettre de répondre à ses observations.
\p
Le projet ayant fait l''objet de la demande de [libelle_datd] dont les références sont indiquées dans le cadre ci-dessus déposé à la mairie le [date_depot_dossier] fera l''objet d''un permis tacite (1) à défaut de réponse de l''administration [delai_dossier] mois après cette date. Les travaux (2) pourront alors être exécutés après affichage sur le terrain du présent récépissé et d''un panneau décrivant le projet conforme au modèle réglementaire.





                                                                                                              &ville , le  &datecourrier

                                                                                            Cachet de la mairie et signature du receveur








(1) Permis tacite : afin de vous éviter d''être en infraction, il vous est recommandé, dans le cas d''un permis tacite, de vous assurer auprès de l''administration de la légalité dudit permis avant tout commencement de travaux. Sur simple demande de votre part,le maire pourra alors vous délivrer une attestation certifiant qu''aucune décision de refus n''a été prise à votre insu. En effet, si le premis tacite était irrégulier, il serait retiré et les tribunaux pourraient vous astreindre à remettre les lieux en leur état initial.
(2) Certains travaux ne peuvent pas être commencés dès la délivrance de [libelle_datd] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas







                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
Le permis tacite peut faire l''objet d''un recours gracieux ou d''un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de deux mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme et en application du décret n°2008-1353 du 19 décembre 2008, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délais de trois ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.'
WHERE id = 'recepisse_1';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

Vous avez déposé ce jour une demande de [libelle_datd]. Le délai d''instruction de votre dossier est de [delai_dossier] mois. En l''absence de courrier de la part de l''administration dans ce délai, vous bénéficierez d''un [libelle_datd] tacite(1).

* Toutefois, dans le mois qui suit le dépôt de votre dossier, l''administration peut vous écrire :
   - soit pour vous avertir qu''un autre délai est applicable, lorsque le code de l''urbanisme l''a prévu pour permettre les consultations nécessaires (si votre projet nécessite la consultation d''autres services ...)
   - soit pour vous indiquer qu''il manque une ou plusieurs pièces à votre dossier,
   - soit pour vous informer que votre projet  correspond à un des cas où un accord tacite n''est pas possible.

* Si vous recevez une telle lettre avant la fin du premier mois, celle-ci remplacera le premier récépissé.

* Si vous n''avez rien reçu à la fin du premier mois suivant le dépôt, le délai de [delai_dossier] mois ne pourra être modifié. Sans aucun courrier de l''administration à l''issue de ce délai de [delai_dossier] mois, vous pourrez commencer les travaux (2) quinze jours après la date à laquelle le [libelle_datd] tacite est acquis. Vous devrez péalablement avoir :
   - adressé au maire, en 3 exemplaires, une déclaration d''ouverture de chantier (vous trouverez un modèle de déclaration CERFA n° 13407 01 à la mairie ou sur le site internet urbanisme du gouvernement)
   - affiché sur le terrain ce recepissé sur lequel la mairie a mis son cachet pour attester la date de dépôt ;
   - installé sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie publique décrivant le projet (vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux).

* ATTENTION : le permis n''est définitif qu''en l''absence de recours ou de retrait :
   - dans un délai de 2 mois à compter de l''affichage sur le terrain, sa légalité peut être contestée par un tiers. Dans ce cas l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours.
    - dans un délai de 3 mois après la date de [libelle_datd], l''autorité compétente peut le retirer, si elle l''estime illégal. Elle est tenue de vous en informer préalablement et de vous permettre de répondre à ses observations.
\p
Le projet ayant fait l''objet de la demande de [libelle_datd] dont les références sont indiquées dans le cadre ci-dessus déposé à la mairie le [date_depot_dossier] fera l''objet d''un permis tacite (1) à défaut de réponse de l''administration [delai_dossier] mois après cette date. Les travaux (2) pourront alors être exécutés après affichage sur le terrain du présent récépissé et d''un panneau décrivant le projet conforme au modèle réglementaire.





                                                                                                              &ville , le  &datecourrier

                                                                                            Cachet de la mairie et signature du receveur








(1) Permis tacite : afin de vous éviter d''être en infraction, il vous est recommandé, dans le cas d''un permis tacite, de vous assurer auprès de l''administration de la légalité dudit permis avant tout commencement de travaux. Sur simple demande de votre part, le maire pourra alors vous délivrer une attestation certifiant qu''aucune décision de refus n''a été prise à votre insu.
(2) Certains travaux ne peuvent pas être commencés dès la délivrance de [libelle_datd] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas.







                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
Le permis tacite peut faire l''objet d''un recours gracieux ou d''un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.'
WHERE id = 'recepisse_PD';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]'
WHERE id = 'standard';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

J''ai l''honneur de vous informer que  votre projet est subordonné à l''accord ou à une autorisation prévu par une autre législation ou fait l''objet d''une évocation par un ministre chargé des sites, de la protection de la nature, des monuments historiques et des espaces protégés.

En conséquence, le délai d''instruction initialement fixé par les articles R 423-23 à R 423-33 du code de l''urbanisme est ANNULE.

Le délai d''instruction de votre [libelle_datd] est donc prolongé par la loi (Art. R423-34 à R.423-37 du code de l''urbanisme) et la nouvelle date limite d''instruction est fixée au  [date_limite_dossier].


La décision doit donc vous être notifiée avant expiration de ce délai.

En application des articles R 423-44 et R 424-2 ou R 424-3 du code de l''urbanisme, le défaut de notification d''une décision à la date du [date_limite_dossier], vaudra REFUS de [libelle_datd].

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée.


                                                                                                 &ville , le  &datecourrier
                                                                                                 le Maire &delaville
                                                                                                 &titreelu



                                                                                                              &nom



NB : Copie de la présente lettre est transmise au représentant de l''Etat dans les conditions définies aux articles L2131-1 et L2131 -2 du  code des collectivités territoriales.

p

                                       INFORMATION - A LIRE ATTENTIVEMENT

DELAIS ET VOIES DE RECOURS :
Le pétitionnaire qui désire contester la décision peut saisir le tribunal administratif compétent d''un recours contentieux dans le délai de DEUX MOIS à partir de la transmission de la décision attaquée. Il peut également saisir d''un recours gracieux l''auteur de la décision hiérarchique le Ministre chargé de l''urbanisme ou le préfet pour les  permis délivrés au nom de l''Etat.
Cette démarche prolonge le délai de recours qui doit alors être introduit dans les deux mois suivant la réponse (l''absence de réponse au terme de deux mois vaut rejet implicite)'
WHERE id = 'prolongation';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Le Maire,


Vu la demande susvisée
Vu le Code de l''Urbanisme, notamment ses articles L 421-1 et L 421-6, R 421-1 et R 421-14 à R 421-16.
Vu le permis de construire N° &departement &commune  [libelle_dossier] délivré le  [date_decision_dossier]



                                                         A R R E T E

ARTICLE 1:  Le permis de construire n° &departement &commune  [libelle_dossier]     délivré le   [date_decision_dossier]  à [nom_petitionnaire_principal]  est retiré.

&complement



                                                                                                 &ville , le  &datecourrier
                                                                                                 le Maire &delaville
                                                                                                 &titreelu



                                                                                                              &nom'
WHERE id = 'retrait';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

J''ai l''honneur de vous informer que votre projet est soumis, en raison de son emplacement, de son utilisation ou de sa nature à un régime d''autorisation ou à des prescriptions prévues par une autre législation ou réglementation que le code de l''urbanisme, et qu''il nécessite l''avis d''un service ou d''une commission ne relevant pas de mon autorité.

En conséquence, le délai d''instruction initialement fixé par l''article R 423-23 du code de l''urbanisme est  MODIFIE.

Le délai d''instruction de votre [libelle_datd] est donc majoré par la loi (Art. R423-24 à R.423-33 du code de l''urbanisme) et la nouvelle date limite d''instruction est fixée au  [date_limite_dossier].
La décision doit donc vous être notifiée  avant expiration de ce délai.

En l''absence d''une notification d''une décision par l''autorité compétente la présente lettre vaudra    [libelle_datd] tacite.

Dans le délai de deux mois à compter de la décision tacite, l''autorité compétente pourra prendre un arrêté fixant les participations applicables au projet.

&complement

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée.



                                                                             &ville , le
                                                                              Le Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A la Politique Foncière


                                                                              &nom


NB : Copie de la présente lettre est transmise au représentant de l''Etat dans les conditions définies aux articles L2131-1 et L2131 -2 du  code des collectivités territoriales.
\p





(1) Les travaux ne peuvent  être entrepris, conformément au projet décrit dans la demande, qu''à compter de la décision tacite.
UN PERMIS ENTACHE D''ILLIGALITE PEUT ETRE RETIRE PAR L''ADMINISTRATION COMPETENTE PENDANT LE DELAI LEGAL DE RECOURS CONTENTIEUX
 Aussi, afin de vous évitez d''être en infraction, il vous est recommandé, dans le cas d''une décision tacite, de vous assurer auprès des services municipaux qu''une décision de refus n''a été prise avant de commencer les travaux. Dans cette éventualité, il vous sera délivré sur simple demande, une attestation (2) certifiant qu''aucune décision de refus n''a été prise à votre encontre.

 En effet, si le permis tacite, était irrégulier, il serait retiré et les tribuneaux pourraient vous astreindre de remettre les lieux en leur état initial

Dans le délai de deux mois à compter de la date de décision tacite, il vous sera notifié éventuellement un arrêté fixant les taxes et participations applicables au projet.

Certains travaux ne peuvent pas être commencés dès la délivrance de [libelle_datd] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas.

(2) Le maire ou le préfet.

\p
                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
La déclaration préalable ou le permis peut faire l''objet d'' un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.'
WHERE id = 'majoration';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,
  J''ai l''honneur de vous informer que la demande de [libelle_datd] que vous m''avez adressée a été enregistrée sous les références portées dans le cadre ci-dessus.

  Toutefois, je ne peux entreprendre l''instruction de cette demande car elle est IRRECEVABLE pour le (s) motif (s) suivant(s) :
&complement
&complement2
En conséquence vous trouverez votre dossier en retour sous ce pli.
Je vous prie d''agréer, Madame,Monsieur, l''expression de ma considération distinguée.

                                                                              ville , le
                                                                              Le Maire A djoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A la Politique Foncière




                                                                              &nom

NB : Copie de la présente lettre est transmise au représentant de l''Etat dans les conditions définies aux articles L2131-1 et L2131 -2 du  code des collectivités territoriales.'
WHERE id = 'dossier_irrecevable';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,


Vous avez déposé une demande de [libelle_datd], le [date_depot_dossier] pour un projet de [libelle_dat], situé [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]  [terrain_adresse_code_postal_dossier] -  [terrain_adresse_localite_dossier].

Le récépissé de dépôt de votre dossier indiquait qu''en cas de silence de l''Administration à l''expiration du délai d''instruction de droit commun de trois mois,  vous bénéficieriez d''un permis tacite.

Toutefois ce même récépissé vous informait également que, dans le mois suivant le dépôt  de votre dossier, l''Administration pouvait vous écrire :

-soit pour vous avertir qu''un autre délai d''instruction est applicable, en vertu du code de l''urbanisme
-soit pour vous indiquer qu''il manque une ou plusieurs pièces à votre dossier
-soit pour vous informer que votre projet correspond à un des cas où un permis tacite n''est pas possible.

Je vous informe que le délai d''instruction de votre projet doit effectivement être modifié :

MODIFICATION DU DELAI D''INSTRUCTION DE LA DEMANDE DE PERMIS DE CONSTRUIRE :

Votre projet porte sur la construction d''un immeuble de grande hauteur tel que défini aux articles R122-2 et R122-3 du code de la Construction et de l''Habitation.

Conformément aux articles L123-1 et R123-1 du code de l''environnement, la construction d''un immeuble de grande hauteur est soumise à enquête publique.

Conformément  à l''article R423-20 du code de l''urbanisme, par exception à l''article R423-19 du même code, le délai d''instruction d''un dossier complet part de la réception par l''autorité compétente du rapport du commissaire enquêteur ou de la commission d''enquête.

Conformément à l''article R423-32 du code de l''urbanisme, le délai d''instruction de votre demande de permis de construire est porté à deux mois à compter de la réception par la Ville du rapport du commissaire enquêteur ou de la commission d''enquête

Ce délai annule et remplace le délai de droit commun de 3 mois, qui figure sur le récépissé de dépôt de votre demande de permis de construire.

Conformément  aux articles R423-34 à R423-37 du code de l''urbanisme, votre délai d''instruction pourra faire l''objet d''une prorogation exceptionnelle. Cette dernière vous sera notifiée avant l''expiration du délai d''instruction.

Je vous informe également que votre dossier n''est pas complet :


DEMANDE DE PIECES MANQUANTES DANS LE DOSSIER DE DEMANDE DE PERMIS DE CONSTRUIRE :

Conformément à l''article R423-38 du code de l''urbanisme, je vous informe que, après examen des pièces jointes à votre demande de permis de construire les pièces suivantes sont manquantes ou insuffisantes. Il convient donc que vous me fassiez parvenir les pièces suivantes :

&complement


Conformément à l''article R423-39 du code de l''urbanisme, je vous informe que :

- les pièces manquantes doivent être adressées à la mairie dans le délai de 3 mois à compter de la réception du présent courrier (Hôtel de ville service permis de construire BP 90196 13637 Arles cedex).
- A défaut de production de l''ensemble des pièces manquantes dans ce délai, votre demande de permis de construire fera l''objet d''une décision tacite de rejet



CAS OU UN PERMIS TACITE N''EST PAS POSSIBLE

Enfin, conformément  à l''article R424-2 du code de l''urbanisme, le défaut de notification d''une décision expresse dans le délai d''instruction vaut décision implicite de rejet


                                                                              &ville , le  &datecourrier
                                                                              David GRZYB
                                                                              Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A la Politique Foncière




                            INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
La déclaration préalable ou le permis peut faire l''objet d'' un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.'
WHERE id = 'majoration_IGH';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

J''ai l''honneur de vous informer que votre projet est soumis, en raison de son emplacement, de son utilisation ou de sa nature à un régime d''autorisation ou à des prescriptions prévues par une autre législation ou réglementation que le code de l''urbanisme, et qu''il nécessite l''avis d''un service ou d''une commission ne relevant pas de mon autorité.

En conséquence, le délai d''instruction initialement fixé par l''article R 423-23 du code de l''urbanisme est MODIFIE.

Le délai d''instruction de votre [libelle_datd] est donc majoré par la loi (Art. R423-32 du code de l''urbanisme).
La décision doit donc vous être notifiée  avant l''expiration de  deux mois à compter de la réception du rapport du commissaire enquêteur.

En application de l''article R 424-2 du code de d''urbanisme, le défaut de notification d''une décision
au                        , vaudra refus de [libelle_datd].

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée.



                                                                              ville , le
                                                                              Le Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitatl
                                                                              A la Politique Foncière





                                                                              &nom



NB : Copie de la présente lettre est transmise au représentant de l''Etat dans les conditions définies aux articles L2131-1 et L2131 -2 du  code des collectivités territoriales.
\p



                                       INFORMATION A LIRE ATTENTIVEMENT


DELAIS ET VOIES DE RECOURS
Le pétitionnaire qui désire contester la décision peut saisir le tribunal administratif compétent d''un recours contentieux dans le délai de DEUX MOIS à partir de la transmission de la décision attaquée. Il peut également saisir d''un recours gracieux l''auteur de la décision hiérarchique, le ministère chargé de l''urbanisme ou le préfet pour les permis délivrés au nom de l''Etat.

Cette démarche prolonge le délai de recours qui doit alors être introduit dans les deux mois suivant réponse (l''absence de réponse au terme de deux mois vaut rejet implicite).'
WHERE id = 'majoration_enquete_publique';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,


Vous avez déposé une demande de [libelle_datd], le [date_depot_dossier] pour un projet de [libelle_dat], situé [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]  [terrain_adresse_code_postal_dossier] -  [terrain_adresse_localite_dossier].

Le récépissé de dépôt de votre dossier indiquait qu''en cas de silence de l''Administration à l''expiration du délai d''instruction de droit commun de 3 mois,  vous bénéficieriez d''un permis tacite.

Toutefois ce même récépissé vous informait également que, dans le mois suivant le dépôt  de votre dossier, l''Administration pouvait vous écrire :

-soit pour vous avertir qu''un autre délai d''instruction est applicable, en vertu du code de l''urbanisme
-soit pour vous indiquer qu''il manque une ou plusieurs pièces à votre dossier
-soit pour vous informer que votre projet correspond à un des cas où un permis tacite n''est pas possible.

Je vous informe que le délai d''instruction de votre projet doit effectivement être modifié :

MODIFICATION DU DELAI D''INSTRUCTION DE LA DEMANDE DE PERMIS DE CONSTRUIRE :

Votre demande de permis de construire porte sur un projet situé dans le périmètre de protection des immeubles classés ou inscrits au titre des Monuments Historiques.

Conformément à l''article R423-28 b du code de l''urbanisme, le délai d''instruction de votre permis de construire est donc porté à 6 mois à compter de la réception en mairie d''un dossier complet suivant l''article R423-19 du code de l''urbanisme.

Ce délai annule et remplace le délai de droit commun de 3 mois, qui figure sur le récépissé de dépôt de votre demande de permis de construire.

Conformément aux articles R423-34 à R423-37 du code de l''urbanisme, votre délai d''instruction pourra faire l''objet d''une prorogation exceptionnelle. Cette dernière vous sera notifiée avant l''expiration du délai d''instruction.
Je vous informe également que votre dossier n''est pas complet.


DEMANDE DE PIECES MANQUANTES DANS LE DOSSIER DE DEMANDE DE PERMIS DE CONSTRUIRE :

Conformément à l''article R423-38 du code de l''urbanisme, je vous informe que, après examen des pièces jointes à votre demande de permis de construire les pièces suivantes sont manquantes ou insuffisantes. Il convient donc que vous me fassiez parvenir les pièces suivantes :

&complement


Conformément à l''article R423-39 du code de l''urbanisme, je vous informe que :

*Les pièces manquantes doivent être adressées à la mairie dans le délai de 3 mois à compter de la réception du présent courrier (Hôtel de ville service permis de construire BP 90196 13637 Arles cedex).
*A défaut de production de l''ensemble des pièces manquantes dans ce délai, votre demande de permis de construire fera l''objet d''une décision tacite de rejet
*Le délai d''instruction commencera à courir à compter de la réception des pièces manquantes par la mairie


CAS OU UN PERMIS TACITE N''EST PAS POSSIBLE

L''article R424-3 du code de l''urbanisme prévoit que, par exception au b de l''article R424-1, dans les cas où la décision est soumise à l''accord de l''Architecte des Bâtiments de France (ABF), le défaut de notification d''une décision expresse dans le délai d''instruction vaut décision implicite de rejet  lorsque, dans le délai de 4 mois suivant la réception de la demande d''avis, l''Architecte des Bâtiments de France notifie un avis défavorable ou un avis favorable assorti de prescriptions sur le projet. Par conséquent si vous recevez un courrier de l''ABF vous informant qu''il a émis un avis défavorable ou un avis favorable assorti de prescriptions sur votre projet, un permis de construire tacite n''est pas possible. Si alors aucune décision ne vous est envoyée dans le délai de 6 mois à compter du dépôt de toutes les pièces manquantes en mairie, vous pourrez considérer que votre demande est refusée, en application de l''article R424-3 du code de l''urbanisme.

Si vous ne recevez pas un tel courrier de l''ABF et qu''aucune décision ne vous est envoyée dans le délai de 6 mois à compter du dépôt de toutes les pièces manquantes en mairie, votre projet fera l''objet d''un permis de construire tacite.


En cas d''obtention d''un permis tacite (1), vous pourrez alors commencer les travaux (2) après avoir :
          - adressé au maire en trois exemplaires, une déclaration d''ouverture de chantier(vous trouverez un modèle de déclaration CERFA n°13407*01 à la mairie ou sur le site internet : http://developpement-durable.gouv.fr/
          - affiché sur le terrain le présent courrier ;
          - installé sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie publique décrivant le projet. Vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux)
ATTENTION  : LE PERMIS N''EST DEFINITIF QU''EN L''ABSENCE DE RECOURS OU DE RETRAIT :
          - Dans le délai de deux mois à compter de son affichage sur le terrain, sa légalité peut être contestée par un tiers. Dans ce cas, l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours.
          - dans le délai de trois mois après la date du permis, l''autorité compétente peut le retirer , si elle l''estime illégal. Elle est tenue de vous en informer préalablement et de vous permettre de répondre à ses observations.

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée




                                                                              &ville , le  &datecourrier
                                                                              David GRZYB
                                                                              Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A la Politique Foncière



(1) Le maire en délivre certificat sur simple demande.
(2) Certains travaux ne peuvent pas être commencés dès la délivrance du permis et doivent être différés : c''est le cas du permis de démolir, ou des travaux en site inscrit, ainsi que des travaux faisant l''objet de prescriptions au titre de l''archéologie préventive.


                            INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
La déclaration préalable ou le permis peut faire l''objet d'' un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.'
WHERE id = 'majoration_DPC_hors_SS';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]'
WHERE id = 'recepisse_CU';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

Vous avez déposé ce jour une [libelle_datd] à des travaux ou aménagements non soumis à permis. Le délai d''instruction de votre dossier est de [delai_dossier] mois. En l''absence de courrier de la part de l''administration dans ce délai, vous bénéficierez d''une décision de non-opposition à ces travaux ou aménagements (1).

* Toutefois, dans le mois qui suit le dépôt de votre dossier, l''administration peut vous écrire:
    - soit pour vous avertir qu''un autre délai est applicable, lorsque le code de l''urbanisme l''a prévu pour permettre les consultations nécessaires (si votre projet nécessite la consultation d''autres services ...)
    - soit pour vous indiquer qu''il manque une ou plusieurs pièces à votre dossier,

* Si vous recevez une telle lettre avant la fin du mois qui suit le dépôt de votre déclaration, celle ci remplacera le présent récépissé.

* Si vous n''avez rien reçu à la fin du premier mois suivant le dépôt, le délai de [delai_dossier] mois ne pourra être modifié. Sans courrier de l''administration à l''issu de ce délai de [delai_dossier] mois, vous pourrez commencer les travaux (2) après avoir :
  - affiché sur le terrain ce recepissé sur lequel la mairie a mis son cachet pour attester la date de dépôt ;
  - installé sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie puiblique décrivant le projet (Vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux)

* Attention : la décision de non-opposition n''est définitive qu''en l''absence de recours. En effet, dans un délai de 2 mois à compter de l''affichage sur le terrain, sa légalité peut être contestée par un tiers devant le tribunal administratif compétent. Dans ce cas l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours (L424-5).

A défaut de réponse de l''administration dans un délai de [delai_dossier], à compter du [date_depot_dossier] (date de dépôt en mairie de la [libelle_datd] dont les références sont indiquées dans le cadre ci-dessus) votre  [libelle_datd] fera l''objet d''une décision de non-opposition (1). Les travaux ou aménagements (2)pourront alors être exécutés après affichage sur le terrain du présent récépissé et d''un panneau décrivant le projet conforme au modèle réglementaire.



                                                                                                              &ville , le  &datecourrier

                                                                                            Cachet de la Mairie et signature du receveur



(1) Décision de non-opposition : afin de vous éviter d''être en infraction, il vous est recommandé de vous assurer auprès de l''administration de la légalité dudit permis avant tout commencement de travaux. Sur simple demande de votre part, le maire pourra alors vous délivrer une attestation certifiant qu''aucune décision de refus n''a été prise à votre insu. En effet, si le projet était irrégulier, les tribunaux pourraient vous astreindrede remettre les lieux en leur état inital.
(2) certains travaux ne peuvent pas débuter dès la décision de non-opposition. C''est le cas des travaux de coupe et abattage des arbres, des transformations de logement en un autre usage dans les communes de plus de 200.000 habitants et dans les départements de Paris, des hauts de seine, de la Seine Saint Denis et du Val de Marne, ou des installations classées pour la protection de l''environnement. Vous pouvez vérifier auprès des services municipaux que votre projet n''entre pas dans ces cas.





                                     INFORMATIONS A LIRE ATTENTIVEMENT :





DELAIS ET VOIES DE RECOURS :
La décision de non-opposition peut faire l''objet d''un recours gracieux ou d''un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.


&complement'
WHERE id = 'recepisse_DP';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

Vous avez déposé (1) ce jour des pièces complémentaires concernant le dossier de [libelle_datd] dont les références sont indiqués dans les cadre ci-dessus.

Le délai d''instruction de votre dossier est de [delai_dossier]mois. En l''absence Si de courrier de la part de l''administration dans ce délai, vous bénéficierez d''un permis tacite (2).

* Toutefois, dans le mois qui suit le dépôt de pièces complémentaires, l''administration peut vous écrire :
    - soit pour vous avertir qu''un autre délai est applicable, lorsque le code de l''ubanisme l''a prévu pour permettre les consultations nécessaires (si votre projet nécessite la consultation s''autres services ...)
   - soit pour vous indiquer qu''il manque encore une ou plusieurs pièces à votre dossier (1)
   - soit pour vous informer que votre projet correspond à un des cas où un permis tacite n''est pas possible.

Si vou srecevez une telle lettreavant la fin du premier mois, celle-ci remplacera et annulera le présent récépissé.

Si vous n''avez rien reçu à la fin du premier mois suivant le dépôt des pièces complémentaires, le délai de [delai_dossier] mois ne pourra plus être modifié. Si aucun courrier de l''administration ne vous est parvenu à l''issu du délai de [delai_dossier] mois, vous pourrez commencer les travaux (3) après avoir respecté les consignes suivantes :
  - adresser au maire, en 3 exemplaires, une déclaration d''ouverture de chantier (vous trouverez un modèle de déclaration CERFA n° 13407 01 à la mairie ou sur le site internet urbanisme du gouvernement)
   - afficher sur le terrain ce récépissé sur lequel la mairie a mis son cachet pour attester la date de dépôt ;
   - installer sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie publique décrivant le projet (vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux).

* ATTENTION : le permis n''est définitif qu''en l''absence de recours ou de retrait :
   - dans un délai de 2 mois à compter de l''affichage sur le terrain, sa légalité peut être contestée par un tiers. Dans ce cas l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours.
    - dans un délai de 3 mois après la date de [libelle_datd], l''autorité compétente peut le retirer, si elle l''estime illégal. Elle est tenue de vous en informer préalablement et de vous permettre de répondre à ses observations.

Le projet ayant fait l''objet de la demande de [libelle_datd] dont les références sont indiquées dans le cadre ci-dessus déposé à la mairie le [date_depot_dossier] fera l''objet d''un permis tacite (2) à défaut de réponse de l''administration [delai_dossier] mois après cette date. Les travaux (3) pourront alors être exécutés après affichage sur le terrain du présent récépissé et d''un panneau décrivant le projet conforme au modèle réglementaire.



                                                                                                              &ville , le  &datecourrier


                                                                                            Cachet de la mairie et signature du receveur



(1) le présent récepissé ne préjuge en aucune façon de la recevabilité des documents déposés.
Le défaut de production de l''ensemble des pièces manquantes dans le délai de TROIS MOIS à compter de la date de la première lettre de l''administration vous indiquant que votre dossier est incomplet, vaudra décision tacite de REJET de la demande.

(2) Permis tacite : afin de vous évitez dêtre en infraction, il vous est recommandé, dans le cas d''un permis tacite, de vous assurer auprès des services municipaux de la légalité dudit permis avant de commencer les travaux. Dans cette éventualité, il vous sera délivré sous quinzaine, une attestation certifiant qu''aucune décision de refus n''a été prise à votre encontre.
En effet, si le permis tacite était irrégulier, il serait retiré et les tribunaux pourraient vous astreindre de remettre en son état initial le projet.
Dans le délai de deux mois à compter de la date de décision tacite, il vous sera éventuellement notifié un arrêté fixant les taxes et participations applicables au projet.

(3) Certains travaux ne peuvent pas être commencés dès la délivrance de [libelle_datd] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas.

\p
                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
Le permis peut faire l''objet d''un recours gracieux ou d''un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de deux mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.

&complement1'
WHERE id = 'recepisse_DPC';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

Vous avez déposé (1) ce jour des pièces complémentaires concernant le dossier de [libelle_datd] dont les références sont indiqués dans les cadre ci-dessus.

Le délai d''instruction de votre dossier est de 1 mois. En l''absence de courrier de la part de l''administration dans ce délai, vous bénéficierez d''une décision de non-opposition aux travaux ou aménagements décrits dans la déclaration préalable (2).

* Toutefois, dans le mois qui suit le dépôt de pièces complémentaires, l''administration peut vous écrire :
    - soit pour vous avertir qu''un autre délai est applicable, lorsque le code de l''ubanisme l''a prévu pour permettre les consultations nécessaires (si votre projet nécessite la consultation s''autres services ...)
   - soit pour vous indiquer qu''il manque encore une ou plusieurs pièces à votre dossier (1)
   - soit pour vous informer que votre projet correspond à un des cas où un permis tacite n''est pas possible.
Si vous recevez une telle lettre  avant la fin du premier mois, celle-ci remplacera et annulera le présent récépissé.

* Si vous n''avez rien reçu à la fin du premier mois suivant le dépôt des pièces complémentaires, le délai de 1 mois ne pourra plus être modifié. Si aucun courrier de l''administration ne vous est parvenu à l''issu du délai de 1 mois, vous pourrez commencer les travaux (3) après avoir respecté les consignes suivantes :
- adresser au maire, en 3 exemplaires, une déclaration d''ouverture de chantier (vous trouverez un modèle de déclaration CERFA n° 13407 01 à la mairie ou sur le site internet urbanisme du gouvernement)
   - afficher sur le terrain ce récépissé sur lequel la mairie a mis son cachet pour attester la date de dépôt ;
   - installer sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie publique décrivant le projet (vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux).

ATTENTION : le permis n''est définitif qu''en l''absence de recours ou de retrait :
   - dans un délai de 2 mois à compter de l''affichage sur le terrain, sa légalité peut être contestée par un tiers. Dans ce cas l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours.
    - dans un délai de 3 mois après la date de [libelle_datd], l''autorité compétente peut la retirer, si elle l''estime illégal. Elle est tenue de vous en informer préalablement et de vous permettre de répondre à ses observations.

Le projet ayant fait l''objet de la demande de [libelle_datd] dont les références sont indiquées dans le cadre ci-dessus déposé à la mairie le [date_depot_dossier] fera l''objet d''un permis tacite (3) à défaut de réponse de l''administration [delai_dossier] mois après cette date. Les travaux pourront alors être exécutés après affichage sur le terrain du présent récépissé et d''un panneau décrivant le projet conforme au modèle réglementaire.



                                                                                                              &ville , le  &datecourrier


                                                                                                              Cachet de la mairie
                                                                                                             Et signature du receveur




(2) Les travaux ne peuvent  être entrepris, conformément au projet décrit dans la demande, qu''à compter de la décision.

Afin de vous évitez d''être en infraction, il vous est recommandé, dans le cas d''une décision  tacite de non-opposition, de vous assurer auprès des services municipaux que  ladite déclaration préalable n''a pas fait l''objet d''une décision d''opposition avant de commencer les travaux. Dans cette éventualité, il vous sera délivré une attestation certifiant qu''aucune décision de non-opposition n''a été prise à votre encontre.

Dans le délai de deux mois à compter de la date de décision tacite, il vous sera notifié éventuellement un arrêté fixant les taxes et participations applicables au projet.

(3) Certains travaux ne peuvent pas être commencés dès la délivrance de [libelle_datd] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas.

(4) Le maire ou le préfet en délivre certificat sur simple demande.

\p
                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
La déclaration préalable peut faire l''objet d''un d''un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DROITS DE TIERS
La déclaration préalable est acquise sous réserve des droits des tiers : elle vérifie la conformité du projet aux règles et servitudes d''urbanisme. Elle ne vérifie pas si le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si le permis respecte les règles d''urbanisme.

VALIDITE
La déclaration préalable est caduque si travaux ne sont effectués dans un délai de deux ans à compter de la date de décision tacite de non-opposition ou si les travaux sont interrompus pendant un délai supérieur d''une année.

&complement1'
WHERE id = 'recepisse_DPC_DP';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

Vous avez déposé le [date_depot_dossier] une demande de [libelle_datd].

Lors de ce dépôt, le récépissé de votre dossier indiquait qu''en cas de silence de l''Administration à la fin du délai d''instruction de droit commun de [delai_dossier]  mois, vous bénéficierez d''un [libelle_datd] tacite.

Le récépissé vous informait également de la possibilité de modification de ce délai dans les conditions fixées au code de l''Urbanisme.

Je vous informe que votre projet rentre dans ce cadre  du fait qu''il est soumis, en raison de son emplacement, de son utilisation ou de sa nature à un régime d''autorisation ou à des prescriptions prévues par une autre législation ou réglementation que le code de l''urbanisme, et qu''il nécessite l''avis d''un service ou d''une commission ne relevant pas de mon autorité.

Par  conséquent, le délai d''instruction pour votre dossier est de  .... mois en application des articles R 423-23  à R 423-33 du code de l''urbanisme.

 Par ailleurs, je vous informe que mes services ne peuvent entreprendre l''instruction de votre demande de [libelle_datd], enregistrée sous les références portées dans le cadre ci dessus, car elle est INCOMPLETE.

Il convient donc que vous me fassiez parvenir les pièces suivantes :
 &complement

Le délai d''instruction notifié ci-dessus commencera à courir à compter de la réception  de l''intégralité des pièces et informations manquantes en mairie d''Arles Service permis de construire - 5, rue du cloître).

Vous disposez de TROIS MOIS  à compter de la réception de la présente lettre, pour faire parvenir à la mairie d''Arles , service Gestion du Territoire - 5, rue du Cloître, l''intégralité des pièces et informations manquantes .

  Le défaut de production de l''ensemble des pièces et informations manquantes dans ce délai de TROIS MOIS , vaudra  DECISION TACITE DE REJET de votre dossier de [libelle_datd]  (art.423-39b du code de l''urbanisme).

Toutefois, si la consultation de l''Architecte des Bâtiments de France est nécessaire pour votre projet et s''il émet un avis défavorable ou un avis favorable assorti de prescriptions avant la fin de votre délai d''instruction, vous ne pourrez plus vous prévaloir d''un permis tacite ( article R 424-3 du code de l''urbanisme). Dans une telle hypothèse, vous en seriez directement informé par les services de l''Architecte des Bâtiments de France( SDAP des Bouches du Rhône-3 rue du Cloître 13200 Arles). Le silence de l''Administration équivaudrait alors à un REFUS.

J''appelle également votre attention sur le fait que votre délai d''instruction pourrait faire l''objet d''une prolongation exceptionnelle conformément aux articles R 423-34 à R 423-37 du code de l''Urbanisme.
Si l''une de ces situations se présente sur votre dossier, vous serez informé par courrier avant l''expiration du délai d''instruction fixé ci-dessus.




Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée



                                                                              &ville , le  [date_rejet_dossier]
                                                                              Le Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat,
                                                                              A la Politique Foncière


                                                                               &nom


Nb : copie de la présente lettre est transmise au représentant de l''Etat.
\p                                          INFORMATION - A LIRE ATTENTIVEMENT



DELAIS ET VOIES DE RECOURS :
le pétitionnaire qui désire contester la décision peut saisir le tribunal administratif compétent d''un recours contentieux dans un délai de DEUX MOIS à partir de la transmission de la décision attaquée. Il peut également saisir d''un recours gracieux l''auteur de la décision hiérarchique, le Ministre chargé de l''Urbanisme ou le préfet pour les [libelle_datd] délivrés au nom de l''état.

Cette démarche prolonge le délai de recours qui doit alors être introduit dans les deux mois suivant la réponse (l''absence de réponse au terme de 2 mois vaut rejet implicite)'
WHERE id = 'piececomplementaire';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

Vous avez déposé le [date_depot_dossier] une demande de [libelle_datd].

Lors de ce dépôt, le récépissé de votre dossier indiquait qu''en cas de silence de l''Administration à la fin du délai d''instruction de droit commun de . . . .  . mois, vous bénéficierez d''un [libelle_datd] tacite.

Une majoration de délai vous a été notifiée.
Un des services consulté au cours de l''instruction de votre dossier sollicite des pièces complémentaires
Il convient donc que vous me fassiez parvenir les pièces suivantes :
 &complement

En l''absence de ces éléments, la demande s''acheminerait vers un refus.

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée



                                                                              &ville , le  &aujourdhui

                                                                              Le Maire  Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat,
                                                                              A la Politique Foncière


                                                                                               &nom


Nb : copie de la présente lettre est transmise au représentant de l''Etat.
\p                                          INFORMATION - A LIRE ATTENTIVEMENT



DELAIS ET VOIES DE RECOURS :
le pétitionnaire qui désire contester la décision peut saisir le tribunal administratif compétent d''un recours contentieux dans un délai de DEUX MOIS à partir de la transmission de la décision attaquée. Il peut également saisir d''un recours gracieux l''auteur de la décision hiérarchique, le Ministre chargé de l''Urbanisme ou le préfet pour les [libelle_datd] délivrés au nom de l''état.

Cette démarche prolonge le délai de recours qui doit alors être introduit dans les deux mois suivant la réponse (l''absence de réponse au terme de 2 mois vaut rejet implicite)'
WHERE id = 'piece_complementaire_service';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,
J''ai l''honneur de vous informer que  votre projet est subordonné à l''accord ou à une autorisation prévu par une autre législation ou fait l''objet d''une évocation par un ministre chargé des sites, de la protection de la nature, des monuments historiques et des espaces protégés.

En conséquence, le délai d''instruction initialement fixé par les articles R 423-23 à R 423-33 du code de l''urbanisme est MODIFIE.

Le délai d''instruction de votre [libelle_datd] est donc prolongé par la loi (Art. R423-34 à R.423-37 du code de l''urbanisme) et la nouvelle date limite d''instruction est fixée au  [date_limite_dossier].

La décision doit donc vous être notifiée avant expiration de ce délai.

En l''absence d''une notification d''une décision à la date du [date_limite_dossier] par :
-  l''architecte des bâtiments de France sur le projet indiquant qu''un permis tacite ne peut être obtenu (article R424-4 du code l''urbanisme)
- par l''autorité compétente (1)

la présente lettre vaudra [libelle_datd] tacite(2)

Dans le délai de deux mois à compter de décision tacite, l''autorité compétente prendra un arrêté fixant les participations applicables au projet.

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée.


                                                                              &ville , le
                                                                              Le Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A LA Politique Foncière



                                                                              &nom




NB : Copie de la présente lettre est transmise au représentant de l''Etat dans les conditions définies aux articles L2131-1 et L2131 -2 du  code des collectivités territoriales.

(1) le maire ou le préfet en délivre certificat sur simple demande
(2) Les travaux ne peuvent être entrepris, conformément au projet décrit dans la demande, qu''à compter de la décision tacite.

TOUTEFOIS, LE PERMIS S''IL EST ILLEGAL PEUT ETRE RETIRE PAR L''AUTORITE COMPETENTE PENDANT LE DELAI LEGAL DE RECOURS CONTENTIEUX.

Aussi, afin de vous éviter d''être en infraction, il vous est recommandé, dans le cas de [libelle_datd] tacite, de vous assurer auprès de mes services de la légalité dudit permis, avant tout commencement de travaux. Dans cette éventualité, il vous sera délivré sous quinzaine une attestation certifiant qu''aucune décision de refus n''a été prise à votre insu.
En effet, si le/la [libelle_datd] tacite était irrégulier, il/elle serait retiré(e) et les tribunaux pourraient vous astreindre de démolir la construction entreprise.
 \p


                                                 INFORMATION-A LIRE ATTENTIVEMENT :


DELAIS ET VOIES DE RECOURS :
Le permis peut faire l''objet d''un recours gracieux ou d un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme et en application du décret n°2008-1353 du 19 décembre 2008, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délais de trois ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.

AFFICHAGE :
Le/la [libelle_datd] doit être affiché sur le terrain, visible de l''extérieur ( voie publique), à compter de la date de la décision tacite et pendant toute la durée du chantier.'
WHERE id = 'prolongationnontacite';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]'
WHERE id = 'majoration_ss_perimetre_mh';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,
J''ai l''honneur de vous informer que votre projet est soumis, en raison de son emplacement, de son utilisation ou de sa nature à un régime d''autorisation ou à des prescriptions prévues par une autre législation ou réglementation que le code de l''urbanisme, et qu''il nécessite l''avis d''un service ou d''une commission ne relevant pas de mon autorité.

En conséquence, le délai d''instruction initialement fixé par l''article R 423-23 du code de l''urbanisme est MODIFIE.
Le délai d''instruction de votre [libelle_datd] est donc majoré par la loi (Art. R423-24 à R.423-33 du code de l''urbanisme) et la nouvelle date limite d''instruction est fixée au  [date_limite_dossier].
La décision doit donc vous être notifiée  avant expiration de ce délai.

Le défaut de notification d''une décision au [date_limite_dossier], vaudra acceptation tacite (1).

&complement

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée.




                                                                              &ville , le
                                                                              Le Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat,
                                                                              A la Politique Foncière




                                                                              &nom



NB : Copie de la présente lettre est transmise au représentant de l''Etat dans les conditions définies aux articles L2131-1 et L2131 -2 du  code des collectivités territoriales.
\p




(1) Les travaux ne peuvent  être entrepris, conformément au projet décrit dans la demande, qu''à compter de la décision tacite.
UN PERMIS ENTACHE D''ILLIGALITE PEUT ETRE RETIRE PAR L''ADMINISTRATION COMPETENTE PENDANT LE DELAI LEGAL DE RECOURS CONTENTIEUX
 Aussi, afin de vous évitez d''être en infraction, il vous est recommandé, dans le cas d''une décision tacite, de vous assurer auprès des services municipaux qu''une décision de refus n''a été prise avant de commencer les travaux. Dans cette éventualité, il vous sera délivré sur simple demande, une attestation (2) certifiant qu''aucune décision de refus n''a été prise à votre encontre.

 En effet, si le permis tacite, était irrégulier, il serait retiré et les tribuneaux pourraient vous astreintdre de remettre les lieux en leur état initial

Dans le délai de deux mois à compter de la date de décision tacite, il vous sera notifié éventuellement un arrêté fixant les taxes et participations applicables au projet.

Certains travaux ne peuvent pas être commencés dès la délivrance de [libelle_datd] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas.

(2) Le maire ou le préfet.

\p
                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
La déclaration préalable ou le permis peut faire l''objet d'' un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.'
WHERE id = 'majorationtacite';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Madame, Monsieur,

Vous avez déposé le [date_depot_dossier] une demande de [libelle_datd].

Lors de ce dépôt, le récépissé de votre dossier indiquait qu''en cas de silence de l''Administration à la fin du délai d''instruction de droit commun de [delai_dossier] mois, vous bénéficierez d''un [libelle_datd] tacite.

Je vous informe que mes services ne peuvent entreprendre l''instruction de votre demande de [libelle_datd], enregistrée sous les références portées dans le cadre ci dessus, car elle est INCOMPLETE.

Il convient donc que vous me fassiez parvenir les pièces suivantes :
 &complement

Le délai d''instruction notifié ci-dessus commencera à courir à compter de la réception  de l''intégralité des pièces et informations manquantes en mairie d''Arles Service Gestion du Territoire - 5, rue du cloître.

Vous disposez de TROIS MOIS  à compter de la réception de la présente lettre, pour faire parvenir à la mairie d''Arles , service Gestion du Territoire - 5, rue du Cloître, l''intégralité des pièces et informations manquantes .

Le défaut de production de l''ensemble des pièces et informations manquantes dans ce délai de TROIS MOIS, vaudra  DECISION TACITE DE REJET de votre dossier de [libelle_datd]  (art.423-39b du code de l''urbanisme).
\p
J''appelle également votre attention sur le fait que votre délai d''instruction pourrait faire l''objet d''une prolongation exceptionnelle conformément aux articles R 423-34 à R 423-37 du code de l''Urbanisme.

Si cette situation se présente sur votre dossier, vous serez informé par courrier avant l''expiration du délai d''instruction fixé ci-dessus.

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée







                                                                              &ville , le
                                                                              Le Maire de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A la Politique Foncière






                                                                              &nom




Nb : copie de la présente lettre est transmise au représentant de l''Etat.
\p                                          INFORMATION - A LIRE ATTENTIVEMENT



DELAIS ET VOIES DE RECOURS :
le pétitionnaire qui désire contester la décision peut saisir le tribunal administratif compétent d''un recours contentieux dans un délai de DEUX MOIS à partir de la transmission de la décision attaquée. Il peut également saisir d''un recours gracieux l''auteur de la décision hiérarchique, le Ministre chargé de l''Urbanisme ou le préfet pour les [libelle_datd] délivrés au nom de l''état.

Cette démarche prolonge le délai de recours qui doit alors être introduit dans les deux mois suivant la réponse (l''absence de réponse au terme de 2 mois vaut rejet implicite)'
WHERE id = 'piececomplementaire_sans_majoration';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]'
WHERE id = 'non_opposition_tacite';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]',
corps = 'Le Maire,

Vu la demande de <b>[libelle_datd]</b> susvisée,
Vu le code de l''urbanisme et notamment ses articles L421-1 et suivants, R421-1 et suivants
&complement
Vu les pièces du dossier

                                             A R R E T E

<b>&complement2</b>







                                                                                                              &ville , le  &datecourrier
                                                                                                              le Maire &delaville


                                                                                                              <b>&nom</b>'
WHERE id = 'arrete';

UPDATE om_lettretype SET 
titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]'
WHERE id = 'attestation_affichage';

---
---Renommage des paramètres selon la convention de nommage
---
UPDATE om_parametre SET libelle = 'ged_code_produit' WHERE libelle = 'code_produit';
UPDATE om_parametre SET libelle = 'option_afficher_division' WHERE libelle = 'afficher_division';
UPDATE om_parametre SET libelle = 'option_numero_unique' WHERE libelle = 'numero_unique';
UPDATE om_parametre SET libelle = 'id_affichage_obligatoire' WHERE libelle = 'affichage_obligatoire';
UPDATE om_parametre SET libelle = 'id_etat_initial_dossier_autorisation' WHERE libelle = 'etat_initial_dossier_autorisation';
