--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a16
--
-- XXX Ce fichier doit être renommé en v3.14.0-a16.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.14.0-a16.sql 3165 2014-10-07 08:35:34Z vpihour $
--------------------------------------------------------------------------------
--Correction d'un paramètre
UPDATE om_parametre 
SET valeur = replace (valeur, 'consultation', 'demande_avis_encours') 
WHERE libelle = 'services_consultes_lien_interne';

-- Ajout de nouveaux droits au qualificateur
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'affichage_reglementaire_attestation',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'affichage_reglementaire_attestation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'affichage_reglementaire_registre',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'affichage_reglementaire_registre' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'delegataire',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'delegataire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'demande_autre_dossier_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_autre_dossier_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'demande_autre_dossier_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_autre_dossier_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'demande_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'demande_dossier_encours_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_dossier_encours_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'demande_dossier_encours_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_dossier_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'demande_nouveau_dossier_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_nouveau_dossier_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'demande_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'demandeur',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demandeur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'petitionnaire',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'menu_guichet_unique',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'menu_guichet_unique' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'petitionnaire_frequent_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'petitionnaire_frequent_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );