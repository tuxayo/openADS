--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a4
--
-- XXX Ce fichier doit être renommé en v3.14.0-a4.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.14.0-a4.sql 2883 2014-03-26 18:31:08Z vpihour $
--------------------------------------------------------------------------------

-- Sousétat instruction :
-- Classement des événements par ordre croissant de date courrier

update om_sousetat
    set om_sql =
    'SELECT evenement.libelle as evenement,
    instruction.lettretype,
    to_char(date_evenement,''DD/MM/YYYY'') as datecourrier
    from &DB_PREFIXEinstruction
    inner join &DB_PREFIXEevenement on evenement.evenement = instruction.evenement
    where dossier = ''&idx''
    order by date_evenement asc'
    where id = 'instruction';

---
--- Ajout du champ quartier à la table dossier
---
ALTER TABLE ONLY dossier ADD COLUMN quartier integer DEFAULT NULL;
COMMENT ON COLUMN dossier.quartier IS 'Quartier du dossier d''instruction';
ALTER TABLE ONLY dossier
    ADD CONSTRAINT dossier_quartier_fkey FOREIGN KEY (quartier) REFERENCES quartier(quartier);

--
-- Ajout du champ de fusion [delai_service] pour les consultations.
--
UPDATE om_requete SET requete = '
SELECT 
    --Coordonnées du service
    service.libelle as libelle_service, 
    service.adresse as adresse_service, 
    service.adresse2 as adresse2_service, 
    service.cp as cp_service, 
    service.ville as ville_service,
    service.delai as delai_service,

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    dossier_autorisation_type_detaille.libelle as libelle_datd, 

    --Adresse du terrain du dossier d''instruction
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,

    --Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as tel_instructeur,

    --Coordonnées du demandeur
    civilite.libelle as civilite_demandeur,
    CASE WHEN demandeur.qualite=''particulier''
        THEN TRIM(CONCAT(demandeur.particulier_nom, '' '', demandeur.particulier_prenom))
        ELSE TRIM(CONCAT(demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_denomination))
    END as nom_demandeur,
    CONCAT(demandeur.numero, '' '', demandeur.voie) as adresse_demandeur,
    demandeur.code_postal as code_postal_demandeur,
    demandeur.localite as ville_demandeur,
    demandeur.personne_morale_denomination as societe_demandeur,

    --Dates importantes du dossier d''instruction
    to_char(dossier.date_depot,''DD/MM/YYYY'') as date_depot_dossier, 
    to_char(dossier.date_rejet,''DD/MM/YYYY'') as date_rejet_dossier, 
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier, 
    to_char(consultation.date_envoi,''DD/MM/YYYY'') as date_envoi_dossier, 
    to_char(instruction.date_evenement,''DD/MM/YYYY'') as date_evenement,
    dossier.delai as delai_limite_decision,

    --Code barres de la consultation
    consultation.code_barres as code_barres_consultation,

    --Données générales des données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    REGEXP_REPLACE(CONCAT(
        CASE
            WHEN donnees_techniques.su_cstr_shon1 IS NULL
            THEN ''''
            ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon2 IS NULL
            THEN ''''
            ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon3 IS NULL
            THEN ''''
            ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon4 IS NULL
            THEN ''''
            ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon5 IS NULL
            THEN ''''
            ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon6 IS NULL
            THEN ''''
            ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon7 IS NULL
            THEN ''''
            ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon8 IS NULL
            THEN ''''
            ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
        END, 
        CASE
            WHEN donnees_techniques.su_cstr_shon9 IS NULL
            THEN ''''
            ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
        END
    ), '' / $'', '''') as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques
    
FROM 


    &DB_PREFIXEconsultation 
    LEFT JOIN &DB_PREFIXEservice 
        ON service.service=consultation.service 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=consultation.dossier 
    LEFT JOIN &DB_PREFIXEdonnees_techniques 
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdossier_autorisation 
        ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation 
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur
    LEFT JOIN &DB_PREFIXEinstruction
        ON dossier.dossier=instruction.dossier 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        
         
WHERE 


    consultation.consultation = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE

',
merge_fields = '
--Coordonnées du service
[libelle_service] 
[adresse_service] 
[adresse2_service] 
[cp_service]    [ville_service]
[delai_service]

--Données générales du dossier d''instruction
[libelle_dossier]    [terrain_references_cadastrales_dossier]
[libelle_datd] 

--Adresse du terrain du dossier d''instruction
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier

--Coordonnées de l''instructeur
[nom_instructeur]
[tel_instructeur]

--Coordonnées du demandeur
[civilite_demandeur]    [nom_demandeur]
[adresse_demandeur]
[code_postal_demandeur]    [ville_demandeur]
[societe_demandeur]

--Dates importantes du dossier d''instruction
[date_depot_dossier] 
[date_rejet_dossier] 
[date_limite_dossier] 
[date_envoi_dossier] 
[date_evenement]
[delai_limite_decision]

--Code barres de la consultation
[code_barres_consultation]

--Données générales des données techniques
[co_projet_desc_donnees_techniques]    [co_tot_log_nb_donnees_techniques]    [co_station_place_nb_donnees_techniques]
[am_projet_desc_donnees_techniques]    [am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
[dm_projet_desc_donnees_techniques]
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]
' WHERE code = 'consultation';

--
-- Ajout des champs de fusion [signature_signataire],
-- [date_decision_dossier_autorisation], [libelle_direction], [etat_evenement],
-- [delai_evenement], [accord_tacite_evenement], [delai_notification_evenement],
-- [avis_decision_evenement], [autorite_competente_evenement], 
-- [libelle_quartier], [description_direction] et de toutes les informations sur
-- les 5 autres pétitionnaires pour les instructions.
--
UPDATE om_requete SET requete = '
SELECT

    --Données générales de l''événement d''instruction
    instruction.complement_om_html as complement_instruction,
    instruction.complement2_om_html as complement2_instruction,
    instruction.code_barres as code_barres_instruction,
    to_char(instruction.date_evenement,''DD/MM/YYYY'') as date_evenement_instruction, 
    om_lettretype.libelle as libelle_om_lettretype,
    instruction.archive_delai as archive_delai_instruction,

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    dossier_autorisation.date_decision as date_decision_da,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    quartier.libelle as libelle_quartier,

    avis_decision.libelle as libelle_avis_decision,

    --Données générales du paramétrage de l''événement
    evenement.etat as etat_evenement,
    evenement.delai as delai_evenement,
    evenement.accord_tacite as accord_tacite_evenement,
    evenement.delai_notification as delai_notification_evenement,
    evenement.avis_decision as avis_decision_evenement,
    evenement.autorite_competente as autorite_competente_evenement,

    --Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    --Adresse du terrain du dossier d''instruction 
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    CASE 
        WHEN dossier.terrain_adresse_bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', dossier.terrain_adresse_bp)
    END as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    CASE 
        WHEN dossier.terrain_adresse_cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', dossier.terrain_adresse_cedex)
    END as terrain_adresse_cedex_dossier,

    arrondissement.libelle as libelle_arrondissement,

    --Coordonnées du pétitionnaire principale
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as nom_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_1.particulier_nom, '' '', petitionnaire_1.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
    END as nom_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_2.particulier_nom, '' '', petitionnaire_2.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
    END as nom_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_3.particulier_nom, '' '', petitionnaire_3.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
    END as nom_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_4.particulier_nom, '' '', petitionnaire_4.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
    END as nom_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_5.particulier_nom, '' '', petitionnaire_5.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
    END as nom_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,

    --Coordonnées du délégataire
    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as nom_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complement_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    CASE 
        WHEN delegataire.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', delegataire.bp)
    END as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    CASE 
        WHEN delegataire.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', delegataire.cedex)
    END as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', petitionnaire_principal.bp)
            END
        ELSE 
            CASE 
                WHEN delegataire.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', delegataire.bp)
            END
    END as bp_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
            END 
        ELSE 
            CASE 
                WHEN delegataire.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', delegataire.cedex)
            END 
    END as cedex_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    --Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    
    --Noms des signataires
    CONCAT(signataire_arrete.prenom, '' '', signataire_arrete.nom) as arrete_signataire,
    signataire_arrete.signature as signature_signataire,
    division.chef as chef_division,
    direction.chef as chef_direction,
    direction.libelle as libelle_direction,
    direction.description as description_direction,
    
    --Données générales des données techniquesDONNÉES TECHNIQUES
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.ope_proj_desc as ope_proj_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    REGEXP_REPLACE(CONCAT(
        CASE
            WHEN donnees_techniques.su_cstr_shon1 IS NULL
            THEN ''''
            ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon2 IS NULL
            THEN ''''
            ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon3 IS NULL
            THEN ''''
            ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon4 IS NULL
            THEN ''''
            ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon5 IS NULL
            THEN ''''
            ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon6 IS NULL
            THEN ''''
            ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon7 IS NULL
            THEN ''''
            ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon8 IS NULL
            THEN ''''
            ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
        END, 
        CASE
            WHEN donnees_techniques.su_cstr_shon9 IS NULL
            THEN ''''
            ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
        END
    ), '' / $'', '''') as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques
    
FROM
    &DB_PREFIXEinstruction
LEFT JOIN
    &DB_PREFIXEsignataire_arrete
    ON instruction.signataire_arrete = signataire_arrete.signataire_arrete
LEFT JOIN
    &DB_PREFIXEom_lettretype
    ON instruction.lettretype = om_lettretype.id and om_lettretype.actif IS TRUE
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        instruction.dossier=dossier.dossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        instruction.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur
LEFT JOIN
    (
    &DB_PREFIXElien_dossier_demandeur AS lien_dossier_delegataire
        JOIN &DB_PREFIXEdemandeur as delegataire
        ON
            lien_dossier_delegataire.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
    )
    ON
        instruction.dossier = lien_dossier_delegataire.dossier AND lien_dossier_delegataire.petitionnaire_principal IS FALSE
LEFT JOIN (
    SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
    FROM &DB_PREFIXElien_dossier_demandeur
    LEFT JOIN &DB_PREFIXEdossier
        ON lien_dossier_demandeur.dossier=dossier.dossier 
        AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
    LEFT JOIN &DB_PREFIXEinstruction
        ON instruction.dossier = dossier.dossier
    WHERE instruction.instruction = &idx
    GROUP BY lien_dossier_demandeur.dossier
) as sub_petitionnaire_autre
ON instruction.dossier = sub_petitionnaire_autre.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5]
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN 
    &DB_PREFIXEdirection
    ON division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN 
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN 
    &DB_PREFIXEquartier
    ON
        dossier.quartier = quartier.quartier
WHERE instruction.instruction = &idx

',
merge_fields = '
--Données générales de l''événement d''instruction
[complement_instruction]
[complement2_instruction]
[code_barres_instruction]
[date_evenement_instruction]
[libelle_om_lettretype]
[archive_delai_instruction]

--Données générales du dossier d''instruction
[libelle_dossier]    [delai_dossier]    [terrain_references_cadastrales_dossier]
[libelle_quartier]
[libelle_da]
[date_decision_da]

[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]

[libelle_avis_decision]

--Données générales du paramétrage de l''événement
[etat_evenement]
[delai_evenement]
[accord_tacite_evenement]
[delai_notification_evenement]
[avis_decision_evenement]
[autorite_competente_evenement]

--Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

--Adresse du terrain du dossier d''instruction 
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]

[libelle_arrondissement]

--Coordonnées du pétitionnaire principale
[nom_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''a 5)
[numero_petitionnaire_1](jusqu''a 5)    [voie_petitionnaire_1](jusqu''a 5)    
[complement_petitionnaire_1](jusqu''a 5)
[lieu_dit_petitionnaire_1](jusqu''a 5)    [bp_petitionnaire_1](jusqu''a 5)
[code_postal_petitionnaire_1](jusqu''a 5)    [localite_petitionnaire_1](jusqu''a 5)    
[cedex_petitionnaire_1](jusqu''a 5)
[pays_petitionnaire_1](jusqu''a 5)

--Coordonnées du délégataire
[nom_delegataire]
[numero_delegataire]    [voie_delegataire]    [complement_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire]

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant]

--Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]

--Noms des signataires
[arrete_signataire]
[signature_signataire]
[chef_division]
[chef_direction]
[libelle_direction]
[description_direction]

--Données générales des données techniquesDONNÉES TECHNIQUES
[co_projet_desc_donnees_techniques]    [co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_projet_desc_donnees_techniques]    [am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
[dm_projet_desc_donnees_techniques]
[ope_proj_desc_donnees_techniques]
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]

' WHERE code = 'instruction';

--
-- Ajout du champ de fusion [libelle_direction], [description_direction] et 
-- de toutes les informations sur les 5 autres pétitionnaires pour les rapports 
-- d'instruction.
--
UPDATE om_requete SET requete = '
SELECT
    --Données générales du rapport d''instruction
    rapport_instruction.dossier_instruction as dossier_instruction_rapport_instruction, 
    analyse_reglementaire as analyse_reglementaire_rapport_instruction,
    description_projet as description_projet_rapport_instruction,
    proposition_decision as proposition_decision_rapport_instruction, 

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier, 
    etat as etat_dossier,
    pos as pos_dossier, 
    servitude as servitude_dossier, 
    dossier.delai as delai_dossier,
    dossier_autorisation_type_detaille.libelle as libelle_datd, 
    avis_decision.libelle as libelle_avis_decision,

    --Adresse du terrain dossier d''instruction
    terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    terrain_adresse_localite as terrain_adresse_localite_dossier,
    terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    terrain_adresse_bp as terrain_adresse_bp_dossier,
    terrain_adresse_cedex as terrain_adresse_cedex_dossier,
    terrain_superficie as terrain_superficie_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    
    --Coordonnées du demandeur
    civilite.code as code_civilite,
    demandeur.particulier_nom as particulier_nom_demandeur,
    demandeur.particulier_prenom as particulier_prenom_demandeur,
    demandeur.personne_morale_denomination as personne_morale_denomination_demandeur,
    demandeur.personne_morale_raison_sociale as personne_morale_raison_sociale_demandeur,
    demandeur.personne_morale_siret as personne_morale_siret_demandeur,
    demandeur.personne_morale_nom as personne_morale_nom_demandeur,
    demandeur.personne_morale_prenom as personne_morale_prenom_demandeur,
    demandeur.numero as numero_demandeur,
    demandeur.voie as voie_demandeur,
    demandeur.complement as complement_demandeur,
    demandeur.lieu_dit as lieu_dit_demandeur,
    demandeur.localite as localite_demandeur,
    demandeur.code_postal as code_postal_demandeur,
    demandeur.bp as bp_demandeur,
    demandeur.cedex as cedex_demandeur,

    --Coordonnées du pétitionnaire 1
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_1.particulier_nom, '' '', petitionnaire_1.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
    END as nom_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_2.particulier_nom, '' '', petitionnaire_2.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
    END as nom_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_3.particulier_nom, '' '', petitionnaire_3.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
    END as nom_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_4.particulier_nom, '' '', petitionnaire_4.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
    END as nom_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_5.particulier_nom, '' '', petitionnaire_5.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
    END as nom_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,
    
    --Nom de l''instructeur
    instructeur.nom as nom_instructeur, 

    --Noms des signataires
    division.chef as chef_division,
    direction.chef as chef_direction,
    direction.libelle as libelle_direction,
    direction.description as description_direction,

    --Données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    REGEXP_REPLACE(CONCAT(
        CASE
            WHEN donnees_techniques.su_cstr_shon1 IS NULL
            THEN ''''
            ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon2 IS NULL
            THEN ''''
            ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon3 IS NULL
            THEN ''''
            ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon4 IS NULL
            THEN ''''
            ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon5 IS NULL
            THEN ''''
            ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon6 IS NULL
            THEN ''''
            ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon7 IS NULL
            THEN ''''
            ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon8 IS NULL
            THEN ''''
            ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
        END, 
        CASE
            WHEN donnees_techniques.su_cstr_shon9 IS NULL
            THEN ''''
            ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
        END
    ), '' / $'', '''') as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques


FROM 

    &DB_PREFIXErapport_instruction 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=rapport_instruction.dossier_instruction 
    LEFT JOIN &DB_PREFIXEavis_decision 
        ON dossier.avis_decision = avis_decision.avis_decision
    LEFT JOIN &DB_PREFIXEdonnees_techniques
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdivision
        ON dossier.division = division.division
    LEFT JOIN &DB_PREFIXEdirection
        ON division.direction = direction.direction
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur
    LEFT JOIN (
        SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
        FROM &DB_PREFIXElien_dossier_demandeur
        LEFT JOIN &DB_PREFIXEdossier
            ON lien_dossier_demandeur.dossier=dossier.dossier 
            AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
        LEFT JOIN &DB_PREFIXErapport_instruction
            ON rapport_instruction.dossier_instruction = dossier.dossier
        WHERE rapport_instruction.rapport_instruction = &idx
        GROUP BY lien_dossier_demandeur.dossier
    ) as sub_petitionnaire_autre
    ON rapport_instruction.dossier_instruction = sub_petitionnaire_autre.dossier
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_1
        ON
            petitionnaire_1.demandeur = petitionnaire_autre[1]
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_2
        ON
            petitionnaire_2.demandeur = petitionnaire_autre[2]
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_3
        ON
            petitionnaire_3.demandeur = petitionnaire_autre[3]
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_4
        ON
            petitionnaire_4.demandeur = petitionnaire_autre[4]
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_5
        ON
            petitionnaire_5.demandeur = petitionnaire_autre[5]
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
    
WHERE 

    rapport_instruction = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE

',
merge_fields = '
--Données générales du rapport d''instruction
[dossier_instruction_rapport_instruction]    [analyse_reglementaire_rapport_instruction]
[description_projet_rapport_instruction]    [proposition_decision_rapport_instruction]

--Données générales du dossier d''instruction
[libelle_dossier]    [etat_dossier]
[pos_dossier]    [servitude_dossier]
[delai_dossier]    [libelle_datd]
[libelle_avis_decision]

--Adresse du terrain dossier d''instruction
[terrain_adresse_voie_numero_dossier]     [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_cedex_dossier]    [terrain_superficie_dossier]
[terrain_references_cadastrales_dossier]

--Coordonnées du demandeur
[code_civilite]
[particulier_nom_demandeur]    [particulier_prenom_demandeur]    
[personne_morale_denomination_demandeur]    [personne_morale_raison_sociale_demandeur]    [personne_morale_siret_demandeur]
[personne_morale_nom_demandeur]    [personne_morale_prenom_demandeur]
[numero_demandeur]    [voie_demandeur]
[complement_demandeur]    [lieu_dit_demandeur]
[code_postal_demandeur]    [localite_demandeur]   [bp_demandeur]    [cedex_demandeur]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''a 5)
[numero_petitionnaire_1](jusqu''a 5)    [voie_petitionnaire_1](jusqu''a 5)    
[complement_petitionnaire_1](jusqu''a 5)
[lieu_dit_petitionnaire_1](jusqu''a 5)    [bp_petitionnaire_1](jusqu''a 5)
[code_postal_petitionnaire_1](jusqu''a 5)    [localite_petitionnaire_1](jusqu''a 5)    
[cedex_petitionnaire_1](jusqu''a 5)
[pays_petitionnaire_1](jusqu''a 5)

--Nom de l''instructeur
[nom_instructeur]

--Noms des signataires
[chef_division]
[chef_direction]
[libelle_direction]
[description_direction]

--Données techniques
[co_projet_desc_donnees_techniques]    [am_projet_desc_donnees_techniques]
[dm_projet_desc_donnees_techniques]    
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]    
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]    
[tab_surface_donnees_techniques]
[co_tot_log_nb_donnees_techniques]     [co_statio_place_nb_donnees_techniques]
' WHERE code = 'rapport_instruction';

--
-- Ajout du champ de fusion [libelle_direction], [description_direction]
-- et de toutes les informations sur les 5 autres pétitionnaires pour les 
-- dossiers.
--
UPDATE om_requete SET requete = '
SELECT

    -- Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,

    avis_decision.libelle as libelle_avis_decision,

    -- Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    -- Noms des signataires
    division.chef as division_chef,
    direction.chef as direction_chef,
    direction.libelle as libelle_direction,
    direction.description as description_direction,

    -- Adresse du terrain du dossier d''instruction
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    CASE 
        WHEN dossier.terrain_adresse_bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', dossier.terrain_adresse_bp)
    END as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    CASE 
        WHEN dossier.terrain_adresse_cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', dossier.terrain_adresse_cedex)
    END as terrain_adresse_cedex_dossier,
    arrondissement.libelle as libelle_arrondissement,

    -- Coordonnées du pétitionnaire principal
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as nom_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_1.particulier_nom, '' '', petitionnaire_1.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
    END as nom_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_2.particulier_nom, '' '', petitionnaire_2.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
    END as nom_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_3.particulier_nom, '' '', petitionnaire_3.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
    END as nom_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_4.particulier_nom, '' '', petitionnaire_4.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
    END as nom_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_5.particulier_nom, '' '', petitionnaire_5.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
    END as nom_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,

    -- Coordonnées du délégataire
    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as nom_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complement_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    CASE 
        WHEN delegataire.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', delegataire.bp)
    END as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    CASE 
        WHEN delegataire.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', delegataire.cedex)
    END as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', petitionnaire_principal.bp)
            END
        ELSE 
            CASE 
                WHEN delegataire.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', delegataire.bp)
            END
    END as bp_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
            END
        ELSE 
            CASE 
                WHEN delegataire.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', delegataire.cedex)
            END
    END as cedex_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    -- Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    
    -- Données générales des données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    REGEXP_REPLACE(CONCAT(
        CASE
            WHEN donnees_techniques.su_cstr_shon1 IS NULL
            THEN ''''
            ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon2 IS NULL
            THEN ''''
            ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon3 IS NULL
            THEN ''''
            ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon4 IS NULL
            THEN ''''
            ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon5 IS NULL
            THEN ''''
            ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon6 IS NULL
            THEN ''''
            ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon7 IS NULL
            THEN ''''
            ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon8 IS NULL
            THEN ''''
            ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
        END, 
        CASE
            WHEN donnees_techniques.su_cstr_shon9 IS NULL
            THEN ''''
            ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
        END
    ), '' / $'', '''') as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques

FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as delegataire
    ON
        lien_dossier_demandeur.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
LEFT JOIN (
    SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
    FROM &DB_PREFIXElien_dossier_demandeur
    LEFT JOIN &DB_PREFIXEdossier
        ON lien_dossier_demandeur.dossier=dossier.dossier 
        AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
    WHERE dossier.dossier = ''&idx''
    GROUP BY lien_dossier_demandeur.dossier
) as sub_petitionnaire_autre
ON dossier.dossier = sub_petitionnaire_autre.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5]
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN
    &DB_PREFIXEdirection
    ON
        division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
WHERE dossier.dossier = ''&idx''

',
merge_fields = '
-- Données générales du dossier d''instruction
[libelle_dossier]    
[libelle_da]    
[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]
[delai_dossier]
[terrain_references_cadastrales_dossier]
[libelle_avis_decision]

-- Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

-- Noms des signataires
[division_chef]
[direction_chef]
[libelle_direction]
[description_direction]

-- Adresse du terrain du dossier d''instruction
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]
[libelle_arrondissement]

-- Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''a 5)
[numero_petitionnaire_1](jusqu''a 5)    [voie_petitionnaire_1](jusqu''a 5)    
[complement_petitionnaire_1](jusqu''a 5)
[lieu_dit_petitionnaire_1](jusqu''a 5)    [bp_petitionnaire_1](jusqu''a 5)
[code_postal_petitionnaire_1](jusqu''a 5)    [localite_petitionnaire_1](jusqu''a 5)    
[cedex_petitionnaire_1](jusqu''a 5)
[pays_petitionnaire_1](jusqu''a 5)

-- Coordonnées du délégataire
[nom_delegataire]
[numero_delegataire]    [voie_delegataire]    [complement_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire]

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant]

-- Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]

-- Données générales des données techniques
[co_projet_desc_donnees_techniques]    [co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_projet_desc_donnees_techniques]    [am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
[dm_projet_desc_donnees_techniques]
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]
' WHERE code = 'dossier';

--
-- Correction d'un bug introduit dans la v3.14.0-a1
-- le paramètre 'option_contrainte_di' s'est crée plusieurs fois
--
DELETE FROM om_parametre
WHERE libelle = 'option_contrainte_di';

INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'), 'option_contrainte_di', 'aucune', 1
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'option_contrainte_di'
    );
    
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'), 'option_referentiel_arrete', 'aucune', 1
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'option_referentiel_arrete'
    );


--
-- Changement type integer en varchar du numéro de l'adresse
-- pour mettre l'ajout de bis, ter et numéros multiples
--

ALTER TABLE demande
ALTER terrain_adresse_voie_numero TYPE character varying(20);

ALTER TABLE dossier
ALTER terrain_adresse_voie_numero TYPE character varying(20);

ALTER TABLE dossier_autorisation
ALTER terrain_adresse_voie_numero TYPE character varying(20);