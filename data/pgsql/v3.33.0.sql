--
-- START - #625 — Interfaçage des tables de paramétrage des pièces numérisées 
--
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'document_numerise_type',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_type' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'document_numerise_type_categorie',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_type_categorie' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );

-- 
-- END - #625 — Interfaçage des tables de paramétrage des pièces numérisées 
--

--
-- START - #626 — Table de paramétrage des types de pièce numérisée et filtrage des affichages
--

-- Ajout du champ "Ajoutable par les instructeurs"
ALTER TABLE document_numerise_type ADD COLUMN ajout_instructeur boolean DEFAULT true NOT NULL;
COMMENT ON COLUMN document_numerise_type.ajout_instructeur IS 'Définit si le type peut être ajouté par un instructeur.';

-- Ajout du champ "Afficher aux services consultés"
ALTER TABLE document_numerise_type ADD COLUMN aff_service_consulte boolean DEFAULT true NOT NULL;
COMMENT ON COLUMN document_numerise_type.aff_service_consulte IS 'Définit si les pièces de ce type sont affichées sur les demandes d''avis.';

-- Ajout du champ "Affiché sur les DA"
ALTER TABLE document_numerise_type ADD COLUMN aff_da boolean DEFAULT true NOT NULL;
COMMENT ON COLUMN document_numerise_type.aff_da IS 'Définit si les pièces de ce type sont affichées sur les dossiers d''autorisation.';

-- Ajout du champ "Date de modification non traitée"
ALTER TABLE document_numerise_type ADD COLUMN synchro_metadonnee boolean DEFAULT false NOT NULL;
COMMENT ON COLUMN document_numerise_type.synchro_metadonnee IS 'Définit si les champs ''aff_da'' et/ou ''aff_service_consulte'' de ce type ont été modifiés (t) ou non (f). Utilisé lors du traitement des métadonnées.';

-- Met à jour l'existant à false
UPDATE document_numerise_type
SET aff_da = 'f',
aff_service_consulte = 'f',
ajout_instructeur = 'f';

-- Méthode permettant de mettre à jour le champ 'aff_da' sur les types existants
CREATE OR REPLACE FUNCTION fn_traitement_existant_filtre_DA() RETURNS integer AS
$BODY$
DECLARE
om_parametre_var VARCHAR;
code_var varchar;
BEGIN

    -- Récupère le contenu du paramètre contenant la liste des codes filtrés
    om_parametre_var := (SELECT valeur FROM om_parametre WHERE libelle = 'da_listing_pieces_filtre')::VARCHAR;

    -- Si le paramètre de filtre des pièces dans le contexte des DA existe
    IF om_parametre_var != ''
    THEN
        -- Pour chaque code
        FOR code_var IN SELECT regexp_split_to_table(om_parametre_var, E';') AS code
        LOOP
            -- Met à jour les types de pièce
            UPDATE document_numerise_type
            SET aff_da = 't'
            WHERE code = code_var;
        END LOOP;
    END IF;

    --
    RETURN 0;
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE;

-- Exécute la méthode
select fn_traitement_existant_filtre_DA();

-- Le champ code de la table document_numerise_type devient unique
ALTER TABLE document_numerise_type ADD CONSTRAINT document_numerise_type_code_key UNIQUE (code);

-- 
-- END - #626 — Table de paramétrage des types de pièce numérisée et filtrage des affichages
--

--
-- START - #627 — Amélioration de l'ajout de pièce numérisé pour les instructeurs
--

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'document_numerise_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );

--
-- END - #627 — Amélioration de l'ajout de pièce numérisé pour les instructeurs
--

--
-- START - #628 — Traitement des métadonnées sur les documents stockés
--

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'document_numerise_traitement_metadonnees',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_traitement_metadonnees' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'document_numerise_traitement_metadonnees',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_traitement_metadonnees' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
    );

--
-- END - #628 — Traitement des métadonnées sur les documents stockés
--
