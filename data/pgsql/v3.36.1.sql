-- MàJ de la version en BDD
UPDATE om_version SET om_version = '3.36.1' WHERE exists(SELECT 1 FROM om_version) = true;
INSERT INTO om_version
SELECT ('3.36.1') WHERE exists(SELECT 1 FROM om_version) = false;