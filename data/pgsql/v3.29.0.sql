--
-- START / TI#486 — Ghost Update sur la 3.28.0 pour ajouter l'onglet "Message(s)" 
--

-- Ajout de la permission de visualiser l'onglet Message(s)
INSERT INTO om_droit (om_droit, libelle, om_profil)
    SELECT nextval('om_droit_seq'), 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR GENERAL')
    WHERE
        NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
        );

INSERT INTO om_droit (om_droit, libelle, om_profil)
    SELECT nextval('om_droit_seq'), 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle='INSTRUCTEUR POLYVALENT')
    WHERE
        NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
        );

INSERT INTO om_droit (om_droit, libelle, om_profil)
    SELECT nextval('om_droit_seq'), 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle='INSTRUCTEUR POLYVALENT COMMUNE')
    WHERE
        NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
        );

--
-- END / TI#486 — Ghost Update sur la 3.28.0 pour ajouter l'onglet "Message(s)" 
--

--
-- Déplacement de app/ vers obj/ - BEGIN
--
UPDATE om_droit SET libelle = 'consultation_suivi_retours_de_consultation' WHERE libelle = 'suivi_retours_de_consultation';
UPDATE om_droit SET libelle = 'consultation_suivi_mise_a_jour_des_dates' WHERE libelle = 'demandes_avis_mise_a_jour_des_dates';
--
-- Déplacement de app/ vers obj/ - END
--

--
-- START / [#8251] Droits insuffisants sur les données techniques depuis la fiche du DA pour le service consulté
--

-- Ajout droit de consultation sur les données techniques pour le profil des
-- services consultés externes
INSERT INTO om_droit (om_droit, libelle, om_profil)
    SELECT nextval('om_droit_seq'), 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle='SERVICE CONSULTÉ')
    WHERE
        NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
        );

-- Ajout droit de consultation sur les données techniques pour le profil des
-- services consultés interne
INSERT INTO om_droit (om_droit, libelle, om_profil)
    SELECT nextval('om_droit_seq'), 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle='SERVICE CONSULTÉ INTERNE')
    WHERE
        NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
        );

--
-- END / [#8251] Droits insuffisants sur les données techniques depuis la fiche du DA pour le service consulté
--
