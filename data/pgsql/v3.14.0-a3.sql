--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a3
--
-- XXX Ce fichier doit être renommé en v3.14.0-a3.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.14.0-a3.sql 2831 2014-03-17 18:09:27Z softime $
--------------------------------------------------------------------------------

--
-- Modification du sous-état bordereau_controle_legalite
-- Ajout filtre sur le groupe = 'ADS'
-- Ajout filtre autorité compétente = 'commune'
-- Récupère sur une période
--
UPDATE om_sousetat SET om_sql = '
SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, ''\n'', 
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEgroupe
    ON
        dossier_autorisation_type.groupe = groupe.groupe
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    LOWER(autorite_competente.code) = ''com''
    AND instruction.date_envoi_controle_legalite >= ''&date_bordereau_debut''
    AND instruction.date_envoi_controle_legalite <= ''&date_bordereau_fin''
    AND LOWER(groupe.code) = ''ads''
ORDER BY
    dossier.dossier
' WHERE id = 'bordereau_controle_legalite';
-- Modification du titre
UPDATE om_sousetat SET titre = '
Edition du &aujourdhui

Résultat du &date_bordereau_debut au &date_bordereau_fin


' WHERE id = 'bordereau_controle_legalite';

--
-- Modification du sous-état bordereau_courriers_signature_maire
-- Ajout filtre sur le groupe = 'ADS'
-- Supprime filtre sur le type d'événement
-- Récupère sur une période
--
UPDATE om_sousetat SET om_sql = '
SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, ''\n'', 
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEgroupe
    ON
        dossier_autorisation_type.groupe = groupe.groupe
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    instruction.date_envoi_signature >= ''&date_bordereau_debut''
    AND instruction.date_envoi_signature <= ''&date_bordereau_fin''
    AND LOWER(groupe.code) = ''ads''
ORDER BY
    dossier.dossier
' WHERE id = 'bordereau_courriers_signature_maire';
-- Modification du titre
UPDATE om_sousetat SET titre = '
Edition du &aujourdhui

Résultat du &date_bordereau_debut au &date_bordereau_fin


' WHERE id = 'bordereau_courriers_signature_maire';

--
-- Modification du sous-état bordereau_avis_maire_prefet
-- Ajout filtre sur le groupe = 'ADS'
-- Modificationdu filtre sur l'autorité compétente = 'Commune pour état'
-- Récupère sur une période
-- Modificationde la date testée : date_envoi_rar
--
UPDATE om_sousetat SET om_sql = '
SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, ''\n'', 
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEgroupe
    ON
        dossier_autorisation_type.groupe = groupe.groupe
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    LOWER(autorite_competente.code) = ''etatmaire''
    AND instruction.date_envoi_rar >= ''&date_bordereau_debut''
    AND instruction.date_envoi_rar <= ''&date_bordereau_fin''
    AND LOWER(groupe.code) = ''ads''
ORDER BY
    dossier.dossier
' WHERE id = 'bordereau_avis_maire_prefet';
-- Modification du titre
UPDATE om_sousetat SET titre = '
Edition du &aujourdhui

Résultat du &date_bordereau_debut au &date_bordereau_fin


' WHERE id = 'bordereau_avis_maire_prefet';

--
-- Modification du sous-état bordereau_decisions
-- Ajout filtre sur le groupe = 'ADS'
-- Récupère sur une période
--
UPDATE om_sousetat SET om_sql = '
SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, ''\n'', 
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEgroupe
    ON
        dossier_autorisation_type.groupe = groupe.groupe
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
WHERE
    LOWER(evenement.type) = ''arrete''
    AND instruction.date_envoi_signature >= ''&date_bordereau_debut''
    AND instruction.date_envoi_signature <= ''&date_bordereau_fin''
    AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    AND LOWER(groupe.code) = ''ads''
ORDER BY
    dossier.dossier
' WHERE id = 'bordereau_decisions';
-- Modification du titre
UPDATE om_sousetat SET titre = '
Edition du &aujourdhui

Résultat du &date_bordereau_debut au &date_bordereau_fin


' WHERE id = 'bordereau_decisions';

-- Modification taille champs table demandeur
ALTER TABLE demandeur
ALTER particulier_nom TYPE character varying(100),
ALTER particulier_prenom TYPE character varying(50),
ALTER voie TYPE character varying(55),
ALTER complement TYPE character varying(50),
ALTER localite TYPE character varying(50),
ALTER courriel TYPE character varying(60),
ALTER telephone_fixe TYPE character varying(20),
ALTER telephone_mobile TYPE character varying(20),
ALTER personne_morale_nom TYPE character varying(50),
ALTER personne_morale_prenom TYPE character varying(50),
ALTER personne_morale_raison_sociale TYPE character varying(50);

-- Ajout champ table demandeur
ALTER TABLE demandeur
ADD fax character varying(20) NULL;
COMMENT ON COLUMN demandeur.fax IS 'Numéro de fax du demandeur';

-- Modification taille champs table architecte
ALTER TABLE architecte
ALTER nom TYPE character varying(50),
ALTER prenom TYPE character varying(50),
ALTER adresse1 TYPE character varying(50),
ALTER adresse2 TYPE character varying(50),
ALTER ville TYPE character varying(50),
ALTER telephone TYPE character varying(20),
ALTER email TYPE character varying(60);

-- Modification taille champ table commission_type
ALTER TABLE commission_type
ALTER lieu_adresse_ligne1 TYPE character varying(150);

-- Modification taille champ table instructeur
ALTER TABLE instructeur
ALTER telephone TYPE character varying(20);

-- Modification taille champ table lot
ALTER TABLE lot
ALTER libelle TYPE character varying(250);

--
-- Supprime les caractères de contrôle ASCII des états et lettres type
--
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'01'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'02'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'03'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'04'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'05'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'06'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'07'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'08'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'09'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0A'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0B'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0C'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0D'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0E'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0F'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'10'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'11'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'12'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'13'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'14'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'15'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'16'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'17'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'18'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'19'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1A'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1B'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1C'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1D'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1E'::int), '');
UPDATE om_lettretype SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1F'::int), '');

UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'01'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'02'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'03'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'04'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'05'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'06'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'07'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'08'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'09'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0A'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0B'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0C'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0D'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0E'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0F'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'10'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'11'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'12'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'13'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'14'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'15'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'16'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'17'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'18'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'19'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1A'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1B'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1C'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1D'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1E'::int), '');
UPDATE om_lettretype SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1F'::int), '');

UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'01'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'02'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'03'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'04'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'05'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'06'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'07'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'08'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'09'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0A'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0B'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0C'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0D'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0E'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'0F'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'10'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'11'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'12'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'13'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'14'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'15'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'16'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'17'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'18'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'19'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1A'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1B'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1C'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1D'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1E'::int), '');
UPDATE om_etat SET titre_om_htmletat = replace(titre_om_htmletat, chr(x'1F'::int), '');

UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'01'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'02'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'03'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'04'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'05'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'06'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'07'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'08'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'09'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0A'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0B'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0C'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0D'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0E'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'0F'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'10'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'11'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'12'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'13'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'14'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'15'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'16'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'17'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'18'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'19'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1A'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1B'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1C'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1D'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1E'::int), '');
UPDATE om_etat SET corps_om_htmletatex = replace(corps_om_htmletatex, chr(x'1F'::int), '');

-- Suppression des lettretype "standard" dans instruction
UPDATE instruction SET lettretype='' WHERE lettretype='standard';