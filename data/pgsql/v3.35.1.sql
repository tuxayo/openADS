--
-- START / [#8665] Manque de droit pour visualiser le fichier d'une consultation (profil qualificateur)
--

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'consultation_fichier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

--
-- END / [#8665] Manque de droit pour visualiser le fichier d'une consultation (profil qualificateur)
--

--
-- START / [#8669] Ajout d'une permission pour la correction des dossiers non instruit
--

-- Ajout des nouvelles permissions sur la correction d'un dossier non instruit

-- dossier_instruction_corriger
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );

-- dossier_instruction_mes_clotures_corriger
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );

-- dossier_instruction_mes_encours_corriger
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );

-- dossier_instruction_tous_clotures_corriger
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );

-- dossier_instruction_tous_encours_corriger
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_corriger',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_corriger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );

-- Suppression des permissions de régénération du recepisse

-- INSTRUCTEUR
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR');
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR');
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR');
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR');
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR');

-- INSTRUCTEUR SERVICE
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE');
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE');
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE');
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE');
DELETE FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_regenerate_recepisse' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE');

--
-- END / [#8669] Ajout d'une permission pour la correction des dossiers non instruit
--
