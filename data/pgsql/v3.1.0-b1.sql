--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.1.0-b1
--
-- @package openfoncier
-- @version SVN : $Id: v3.1.0-b1.sql 1804 2013-04-30 14:45:17Z fmichon $
--------------------------------------------------------------------------------

--
-- Contenu de la table om_droit
--
INSERT INTO om_droit (om_droit, om_profil) VALUES

('menu_application', '1'),
('menu_export', '1'),
('menu_parametrage', '1'),
('menu_sig', '1'),
('menu_administration', '1'),

('om_sig_map', '4'),
('om_sig_map_comp', '4'),
('om_sig_map_wms', '4'),
('om_sig_wms', '4'),

('parcelle_lot', '4'),
('servitude_surfacique', '4'),
('servitude_point', '4'),
('servitude_ligne', '4'),

('parametre', '4'),
('num_dossier', '4'),
('regle', '4'),

('edition_d', '2'),
('edition', '2'),
('reqmo', '2'),
('export_pc', '2'),
('export_dp', '2'),
('export_sitadel', '2'),

('instruction', '2'),
('consultation', '3'),
('terrain', '3'),
('blocnote', '3'),
('destination_shon', '3'),
('modificatif', '3'),

('dossier', '3')

;

delete from om_droit where om_droit='om_sig_point';


-- mise a jour OM
-- v4.2.0 -> v4.3.0

ALTER TABLE om_droit ALTER COLUMN om_profil DROP DEFAULT;

ALTER TABLE om_utilisateur ALTER COLUMN om_utilisateur DROP DEFAULT;
ALTER TABLE om_utilisateur ALTER COLUMN nom DROP DEFAULT;
ALTER TABLE om_utilisateur ALTER COLUMN email DROP DEFAULT;
ALTER TABLE om_utilisateur ALTER COLUMN login DROP DEFAULT;
ALTER TABLE om_utilisateur ALTER COLUMN pwd DROP DEFAULT;
ALTER TABLE om_utilisateur ALTER COLUMN om_profil DROP DEFAULT;
ALTER TABLE om_utilisateur ALTER COLUMN om_type SET DEFAULT 'DB';

ALTER TABLE om_utilisateur ADD CONSTRAINT om_utilisateur_login_key UNIQUE (login);

ALTER TABLE om_etat ALTER COLUMN titreattribut SET DEFAULT '';
ALTER TABLE om_etat ALTER COLUMN corpsattribut SET DEFAULT '';
ALTER TABLE om_etat ALTER COLUMN footerattribut SET DEFAULT '';
ALTER TABLE om_etat ALTER COLUMN sousetat SET DEFAULT '';
ALTER TABLE om_etat ALTER COLUMN actif TYPE boolean USING CASE WHEN actif='Oui' THEN true ELSE false END;
ALTER TABLE om_etat ALTER actif DROP NOT NULL;


ALTER TABLE om_sousetat ALTER COLUMN titreattribut SET DEFAULT '';
ALTER TABLE om_sousetat ALTER COLUMN actif TYPE boolean USING CASE WHEN actif='Oui' THEN true ELSE false END;
ALTER TABLE om_sousetat ALTER actif DROP NOT NULL;
ALTER TABLE om_sousetat ALTER entete_orientation TYPE varchar(100);

ALTER TABLE om_lettretype ALTER COLUMN titreattribut SET DEFAULT '';
ALTER TABLE om_lettretype ALTER COLUMN corpsattribut SET DEFAULT '';
ALTER TABLE om_lettretype ALTER COLUMN actif TYPE boolean USING CASE WHEN actif='Oui' THEN true ELSE false END;
ALTER TABLE om_lettretype ALTER actif DROP NOT NULL;

ALTER TABLE avis ALTER libelle DROP DEFAULT;
ALTER TABLE bible ALTER libelle DROP DEFAULT;
ALTER TABLE bible ALTER contenu SET NOT NULL;
ALTER TABLE evenement ALTER libelle DROP DEFAULT;
ALTER TABLE etat ALTER libelle DROP DEFAULT;
ALTER TABLE transition ALTER etat SET NOT NULL;
ALTER TABLE transition ALTER action SET NOT NULL;
ALTER TABLE action ALTER libelle DROP DEFAULT;
ALTER TABLE parametre ALTER actif DROP NOT NULL;
ALTER TABLE rivoli ALTER libelle DROP DEFAULT;
ALTER TABLE nature ALTER libelle DROP DEFAULT;
ALTER TABLE destination ALTER libelle DROP DEFAULT;
ALTER TABLE categorie_demandeur ALTER libelle DROP DEFAULT;
ALTER TABLE travaux ALTER libelle DROP DEFAULT;
ALTER TABLE service ALTER libelle DROP DEFAULT;
ALTER TABLE architecte ALTER nom DROP DEFAULT;
ALTER TABLE architecte ALTER note DROP NOT NULL;
ALTER TABLE proprietaire ALTER civilite DROP DEFAULT;
ALTER TABLE proprietaire ALTER proprietaire SET NOT NULL;
ALTER TABLE pos ALTER libelle DROP DEFAULT;
ALTER TABLE servitude_surfacique ALTER libelle SET NOT NULL;
ALTER TABLE servitude_ligne ALTER libelle SET NOT NULL;
ALTER TABLE servitude_point ALTER libelle SET NOT NULL;
ALTER TABLE parcelle_lot ALTER lotissement SET NOT NULL;
ALTER TABLE dossier ALTER date_depot SET NOT NULL;

ALTER TABLE instruction ALTER complement DROP NOT NULL;
ALTER TABLE instruction ALTER complement2 DROP NOT NULL;
ALTER TABLE instruction ALTER evenement SET NOT NULL;
ALTER TABLE instruction ALTER datecourrier SET NOT NULL;

ALTER TABLE consultation ALTER dossier SET NOT NULL;
ALTER TABLE consultation ALTER service SET NOT NULL;
ALTER TABLE consultation ALTER date_envoi SET NOT NULL;

ALTER TABLE blocnote ALTER note SET NOT NULL;
ALTER TABLE blocnote ALTER categorie DROP DEFAULT;

-- Ajout des contraintes de clés étrangères sur les tables métiers

UPDATE evenement SET nature = NULL WHERE nature ='';
ALTER TABLE evenement ADD CONSTRAINT evenement_nature_fkey FOREIGN KEY (nature) REFERENCES nature (nature);
UPDATE evenement SET action = NULL WHERE action ='';
ALTER TABLE evenement ADD CONSTRAINT evenement_action_fkey FOREIGN KEY (action) REFERENCES action (action);
UPDATE evenement SET etat = NULL WHERE etat ='';
ALTER TABLE evenement ADD CONSTRAINT evenement_etat_fkey FOREIGN KEY (etat) REFERENCES etat (etat);
UPDATE evenement SET avis = NULL WHERE avis ='';
ALTER TABLE evenement ADD CONSTRAINT evenement_avis_fkey FOREIGN KEY (avis) REFERENCES avis (avis);

ALTER TABLE bible ADD CONSTRAINT bible_nature_fkey FOREIGN KEY (nature) REFERENCES nature (nature);

ALTER TABLE travaux ADD CONSTRAINT travaux_nature_fkey FOREIGN KEY (nature) REFERENCES nature (nature);

ALTER TABLE proprietaire ADD CONSTRAINT proprietaire_pkey PRIMARY KEY (proprietaire);
ALTER TABLE proprietaire ADD CONSTRAINT proprietaire_civilite_fkey FOREIGN KEY (civilite) REFERENCES civilite (civilite);

ALTER TABLE parcelle ALTER rivoli DROP DEFAULT;
ALTER TABLE parcelle ALTER rivoli DROP NOT NULL;
ALTER TABLE parcelle ADD CONSTRAINT parcelle_rivoli_fkey FOREIGN KEY (rivoli) REFERENCES rivoli (rivoli);
ALTER TABLE parcelle ADD CONSTRAINT parcelle_proprietaire_fkey FOREIGN KEY (proprietaire) REFERENCES proprietaire (proprietaire);
ALTER TABLE parcelle ADD CONSTRAINT parcelle_pos_fkey FOREIGN KEY (pos) REFERENCES pos (pos);

ALTER TABLE ONLY om_widget
    ADD CONSTRAINT om_widget_om_profil_fkey FOREIGN KEY (om_profil) REFERENCES om_profil(om_profil);

ALTER TABLE dossier ALTER rivoli DROP DEFAULT;
ALTER TABLE dossier ALTER rivoli DROP NOT NULL;
ALTER TABLE dossier ALTER pos DROP DEFAULT;
ALTER TABLE dossier ALTER pos DROP NOT NULL;
ALTER TABLE dossier ALTER parcelle DROP DEFAULT;
ALTER TABLE dossier ALTER parcelle DROP NOT NULL;
ALTER TABLE dossier ADD CONSTRAINT dossier_rivoli_fkey FOREIGN KEY (rivoli) REFERENCES rivoli (rivoli);
ALTER TABLE dossier ADD CONSTRAINT dossier_pos_fkey FOREIGN KEY (pos) REFERENCES pos (pos);
ALTER TABLE dossier ADD CONSTRAINT dossier_parcelle_lot_fkey FOREIGN KEY (parcelle_lot) REFERENCES parcelle_lot (parcelle_lot);
ALTER TABLE dossier ADD CONSTRAINT dossier_parcelle_fkey FOREIGN KEY (parcelle) REFERENCES parcelle (parcelle);

ALTER TABLE instruction ADD CONSTRAINT instruction_action_fkey FOREIGN KEY (action) REFERENCES action (action);
ALTER TABLE instruction ADD CONSTRAINT instruction_etat_fkey FOREIGN KEY (etat) REFERENCES etat (etat);
ALTER TABLE instruction ADD CONSTRAINT instruction_avis_fkey FOREIGN KEY (avis) REFERENCES avis (avis);

ALTER TABLE terrain ADD CONSTRAINT terrain_parcelle_fkey FOREIGN KEY (parcelle) REFERENCES parcelle (parcelle);

ALTER TABLE destination_shon ALTER destination SET NOT NULL;

ALTER TABLE regle ALTER message DROP DEFAULT;
ALTER TABLE regle ALTER valeur DROP DEFAULT;
ALTER TABLE regle ALTER champ DROP DEFAULT;
ALTER TABLE regle ALTER operateur DROP DEFAULT;
ALTER TABLE regle ALTER sens  DROP DEFAULT;
ALTER TABLE regle ALTER ordre DROP DEFAULT;

ALTER TABLE om_profil ALTER COLUMN om_profil DROP DEFAULT;

ALTER TABLE om_widget ALTER COLUMN om_profil SET NOT NULL;

ALTER TABLE dossier ALTER COLUMN nature SET NOT NULL;




---
--- Gestion des profils non hiérarchiques / Mise à jour OBLIGATOIRE om 4.3.0
---

---
ALTER TABLE om_droit DROP CONSTRAINT om_droit_pkey;
ALTER TABLE om_droit DROP CONSTRAINT om_droit_om_profil_fkey;
ALTER TABLE om_utilisateur DROP CONSTRAINT om_utilisateur_om_profil_fkey;
ALTER TABLE om_widget DROP CONSTRAINT om_widget_om_profil_fkey;
ALTER TABLE om_profil DROP CONSTRAINT om_profil_pkey;

ALTER TABLE om_droit RENAME COLUMN om_droit TO om_droit_old;
ALTER TABLE om_droit RENAME COLUMN om_profil TO om_profil_old;

CREATE SEQUENCE om_droit_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER TABLE om_droit ADD COLUMN om_droit integer NOT NULL DEFAULT nextval('om_droit_seq'::regclass);
ALTER TABLE om_droit ADD COLUMN libelle character varying(100);
ALTER TABLE om_droit ADD COLUMN om_profil integer;

UPDATE om_droit SET libelle=om_droit_old;
UPDATE om_droit SET om_profil=om_profil_old::integer;
---
ALTER TABLE om_profil RENAME COLUMN om_profil TO om_profil_old;
ALTER TABLE om_profil RENAME COLUMN libelle TO libelle_old;

CREATE SEQUENCE om_profil_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER TABLE om_profil ADD COLUMN om_profil integer NOT NULL DEFAULT nextval('om_profil_seq'::regclass);
ALTER TABLE om_profil ADD COLUMN libelle character varying(100);
ALTER TABLE om_profil ADD COLUMN hierarchie integer NOT NULL DEFAULT 0 ;

UPDATE om_profil SET libelle=libelle_old;
UPDATE om_profil SET hierarchie=om_profil_old::integer;
UPDATE om_droit SET om_profil=(select om_profil.om_profil from om_profil where hierarchie=om_droit.om_profil_old::integer);
---
ALTER TABLE om_utilisateur RENAME COLUMN om_profil TO om_profil_old;
ALTER TABLE om_utilisateur ADD COLUMN om_profil integer;

UPDATE om_utilisateur SET om_profil=(select om_profil.om_profil from om_profil where hierarchie=om_utilisateur.om_profil_old::integer);
---
ALTER TABLE om_widget RENAME COLUMN om_profil TO om_profil_old;
ALTER TABLE om_widget ADD COLUMN om_profil integer;

UPDATE om_widget SET om_profil=(select om_profil.om_profil from om_profil where hierarchie=om_widget.om_profil_old::integer);
---
ALTER TABLE om_droit ALTER COLUMN libelle SET NOT NULL;
ALTER TABLE om_droit ALTER COLUMN om_profil SET NOT NULL;
ALTER TABLE om_profil ALTER COLUMN libelle SET NOT NULL;
ALTER TABLE om_utilisateur ALTER COLUMN om_profil SET NOT NULL;
ALTER TABLE om_widget ALTER COLUMN om_profil SET NOT NULL;




ALTER TABLE om_droit DROP COLUMN om_droit_old;
ALTER TABLE om_droit DROP COLUMN om_profil_old;
ALTER TABLE om_profil DROP COLUMN libelle_old;
ALTER TABLE om_profil DROP COLUMN om_profil_old;
ALTER TABLE om_utilisateur DROP COLUMN om_profil_old;
ALTER TABLE om_widget DROP COLUMN om_profil_old;

---

ALTER TABLE ONLY om_droit
    ADD CONSTRAINT om_droit_pkey PRIMARY KEY (om_droit);

ALTER TABLE ONLY om_profil
    ADD CONSTRAINT om_profil_pkey PRIMARY KEY (om_profil);

ALTER TABLE ONLY om_utilisateur
    ADD CONSTRAINT om_utilisateur_om_profil_fkey FOREIGN KEY (om_profil) REFERENCES om_profil(om_profil);

ALTER TABLE ONLY om_droit
    ADD CONSTRAINT om_droit_om_profil_fkey FOREIGN KEY (om_profil) REFERENCES om_profil(om_profil);

ALTER TABLE ONLY om_widget
    ADD CONSTRAINT om_widget_om_profil_fkey FOREIGN KEY (om_profil) REFERENCES om_profil(om_profil);

ALTER TABLE ONLY om_droit
    ADD CONSTRAINT om_droit_libelle_om_profil_key UNIQUE (libelle, om_profil);

ALTER SEQUENCE om_profil_seq OWNED BY om_profil.om_profil;
ALTER SEQUENCE om_droit_seq OWNED BY om_droit.om_droit;

ALTER TABLE om_droit ALTER COLUMN om_droit DROP DEFAULT;
ALTER TABLE om_profil ALTER COLUMN om_profil DROP DEFAULT;
ALTER SEQUENCE om_utilisateur_seq OWNED BY om_utilisateur.om_utilisateur;
ALTER SEQUENCE om_parametre_seq OWNED BY om_parametre.om_parametre;

---