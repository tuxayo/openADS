--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a15
--
-- XXX Ce fichier doit être renommé en v3.14.0-a15.sql au moment de la release
--
-- @package nomApplication
-- @version SVN : $Id: v3.14.0-a15.sql 3153 2014-09-15 16:37:30Z vpihour $
--------------------------------------------------------------------------------
--Mise à jour des paramètres pour supprimer les informations inutiles
UPDATE om_parametre 
SET valeur = replace(valeur, '/openads/', '/openads/scr/form.php?obj=consultation&action=3&idx=') 
WHERE libelle = 'services_consultes_lien_externe';
UPDATE om_parametre 
SET valeur = replace(valeur, '/openads/', '/openads/scr/form.php?obj=consultation&action=3&idx=') 
WHERE libelle = 'services_consultes_lien_interne';

--Ajout d'un paramètre
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES
	(nextval('om_parametre_seq'), 'id_evenement_bordereau_avis_maire_prefet', '', 1);

--Mise à jour d'un bordereau
UPDATE om_sousetat SET om_sql = 'SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' \ '',
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEgroupe
    ON
        dossier_autorisation_type.groupe = groupe.groupe
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur.demandeur
    AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    evenement.retour IS FALSE
    AND (LOWER(autorite_competente.code) = ''etatmaire''
      OR LOWER(autorite_competente.code) = ''etat'')
    AND instruction.date_envoi_rar >= ''&date_bordereau_debut''
    AND instruction.date_envoi_rar <= ''&date_bordereau_fin''
    AND LOWER(groupe.code) = ''ads''
    AND instruction.evenement = &id_evenement_bordereau_avis_maire_prefet
ORDER BY
    dossier.dossier' 
WHERE libelle = 'bordereau_avis_maire_prefet';