--
-- PostgreSQL database dump
--

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SET check_function_bodies = false;
-- SET client_min_messages = warning;

-- SET search_path = openads, pg_catalog;

--
-- Data for Name: document_numerise_type_categorie; Type: TABLE DATA; Schema: openads; Owner: postgres
--

INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle) VALUES (1, 'Définition Générale');
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle) VALUES (2, 'Rendu/Insertion');
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle) VALUES (3, 'Accessibilité/Sécurité');
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle) VALUES (4, 'Autre');
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle) VALUES (5, 'Arrêté');
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle) VALUES (6, 'Avis Obligatoires');
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle) VALUES (7, 'Document');
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle) VALUES (8, 'Daact');
INSERT INTO document_numerise_type_categorie (document_numerise_type_categorie, libelle) VALUES (9, 'Dossier Complet');


--
-- Data for Name: document_numerise_type; Type: TABLE DATA; Schema: openads; Owner: postgres
--

INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (29, 'CCO', 'certificat conformité totale lotissement', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (30, 'CCP', 'certificat conformité partielle lotissement', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (31, 'COM', 'passage en commission', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (62, 'INC', 'incomplétude', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (63, 'INCCS', 'incomplétude changement usage', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (64, 'IRA', 'irrecevabilité d''annulation', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (65, 'IRN', 'irrecevabilité de transfert de nom', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (66, 'IRO', 'irrecevabilité de prorogation', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (67, 'IRR', 'irrecevabilité', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (68, 'LIC', 'lettre d''information complémentaire', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (69, 'LMA', 'lettre de mise en attente', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (70, 'MDR', 'mise en demeure de retrait', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (71, 'NDL', 'notification de délai', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (72, 'PDL', 'prolongation de délai', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (73, 'PRL', 'prolongation de delais', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (74, 'RAA', 'rapport d''annulation', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (75, 'RAM', 'rapport modificatif', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (76, 'RAN', 'rapport de transfert de nom', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (77, 'RAO', 'rapport de prorogation', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (78, 'RAP', 'rapport ( ou avis )', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (79, 'RAR', 'rapport de conformité', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (80, 'RDA', 'refus de la déclaration attestant l''achèvement et la conformité des travaux', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (81, 'RIN', 'lettre rappel incomplétude', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (33, 'DGCUB', 'toutes les pièces du certificat d''urbanisme opérationnel', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (34, 'DGIMPA', 'Imprimé de demande de permis d''aménager', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (35, 'DGIMPC', 'Imprimé de demande de permis de construire', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (36, 'DGIMPD', 'Imprimé de demande de permis de démolir', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (37, 'DGPA01', 'plan de situation', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (38, 'DGPA02', 'notice descriptive terrain et projet d''aménagement prévu', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (39, 'DGPA03', 'plan de l''état actuel du terrain à aménager et de ses abords', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (40, 'DGPA04', 'plan de composition d''ensemble du projet', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (41, 'DGPA05', 'vues et coupes du projet dans le profil du terrain naturel', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (42, 'DGPA08', 'programme et plans des travaux d''équipement du lotissement', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (43, 'DGPA10', 'projet de règlement du lotissement', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (44, 'DGPC01', 'plan de situation', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (45, 'DGPC02', 'plan de masse des constructions à édifier ou à modifier', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (46, 'DGPC03', 'plan en coupe du terrain et de la construction', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (47, 'DGPC04', 'notice décrivant le terrain et présentant le projet', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (48, 'DGPC32', 'plan de division du terrain', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (49, 'DGPD01', 'plan de situation', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (50, 'DGPD02', 'plan de masse des constructions à démolir ou à conserver', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (51, 'DGPD03', 'photographie du ou des bâtiments à démolir', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (52, 'DGPD04', 'notice expliquant pourquoi la conservation du bâtiment ne peut plus être assurée (démolition totale)', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (53, 'DGPD05', 'photographies des façades et toitures du bâtiment et des dispositions intérieures (démolition totale)', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (54, 'DGPD06', 'notice expliquant pourquoi la conservation du bâtiment ne peut plus être assurée (démolition partielle)', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (55, 'DGPD07', 'photographies des façades et toitures du bâtiment et de ses dispositions intérieures (démolition partielle)', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (56, 'DGPD08', 'descriptif des moyens mis en oeuvre pour éviter toute atteinte aux parties conservées du bâtiment', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (57, 'DGPD09', 'photographies de l''ensemble des parties extérieures et intérieures du bâtiment adossées à l''immeuble classé', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (58, 'DGPD10', 'descriptif des moyens mis en oeuvre pour éviter toute atteinte à l''immeuble classé', 1, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (82, 'RIPA06', 'photographies du terrain', 2, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (83, 'RIPA07', 'photographie du terrain dans le paysage lointain', 2, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (84, 'RIPA09', 'document graphique des hypothèses d''implantation des bâtiments', 2, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (85, 'RIPA17', 'plan de masse des constructions à édifier ou à modifier', 2, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (86, 'RIPA18', 'plan des façades et des toitures', 2, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (87, 'RIPA19', 'plan en coupe du terrain et de la construction', 2, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (88, 'RIPC05', 'plan des façades et des toitures', 2, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (89, 'RIPC06', 'insertion graphique et photographies du terrain', 2, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (90, 'RIPC07', 'photographie du terrain dans l''environnement proche', 2, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (91, 'RIPC08', 'photographie du terrain dans le paysage lointain', 2, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (12, 'ASPA27', 'étude de sécurité', 3, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (13, 'ASPA49', 'récépissé de dépôt en préfecture de la demande d''autorisation IGH', 3, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (14, 'ASPA50', 'dossier spécifique accessibilité ERP', 3, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (15, 'ASPA51', 'dossier sécurité ERP', 3, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (16, 'ASPC16', 'étude de sécurité', 3, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (17, 'ASPC38', 'récépissé de dépôt en préfecture de la demande d''autorisation IGH', 3, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (18, 'ASPC39', 'dossier spécifique accessibilité ERP', 3, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (19, 'ASPC40', 'dossier sécurité ERP', 3, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (20, 'AUTG', 'toutes les pièces composant le dossier (A0)', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (21, 'AUTP', 'toutes les pièces composant le dossier (A3/A4)', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (22, 'AUTPAG', 'autres pièces composant le dossier (A0)', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (23, 'AUTPAP', 'autres pièces composant le dossier (A3/A4)', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (24, 'AUTPCG', 'autres pièces composant le dossier (A0)', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (25, 'AUTPCP', 'autres pièces composant le dossier (A3/A4)', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (26, 'AUTPDG', 'autres pièces composant le dossier (A0)', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (27, 'AUTPDP', 'autres pièces composant le dossier (A3/A4)', 4, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (1, 'APA', 'arrêté participations', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (2, 'ARA', 'arrêté d''annulation', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (3, 'ARD', 'arrêté de différé de travaux', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (4, 'ARE', 'arrêté rectificatif', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (5, 'ARM', 'arrêté modificatif', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (6, 'ARN', 'arrêté de transfert de nom', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (7, 'ARO', 'arrêté de prorogation', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (8, 'ARR', 'arrêté de conformité', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (9, 'ARRT', 'arrêté', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (10, 'ART', 'arrêté retour préfecture', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (11, 'ARV', 'arrêté de vente par anticipation', 5, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (28, 'AVIS', 'avis obligatoires', 6, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (59, 'DOC', 'déclaration d''ouverture de chantier', 7, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (32, 'DAACT', 'déclaration attestant l''achèvement et la conformité des travaux', 8, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (60, 'DOS01', 'autres pièces composant le dossier délivré (A3/A4)', 9, true, true, true);
INSERT INTO document_numerise_type (document_numerise_type, code, libelle, document_numerise_type_categorie, aff_service_consulte, aff_da, synchro_metadonnee) VALUES (61, 'DOS02', 'autres pièces composant le dossier délivré (A0)', 9, true, true, true);


--
-- Name: document_numerise_type_categorie_seq; Type: SEQUENCE SET; Schema: openads; Owner: postgres
--

SELECT pg_catalog.setval('document_numerise_type_categorie_seq', 10, false);


--
-- Name: document_numerise_type_seq; Type: SEQUENCE SET; Schema: openads; Owner: postgres
--

SELECT pg_catalog.setval('document_numerise_type_seq', 92, false);


--
-- PostgreSQL database dump complete
--

