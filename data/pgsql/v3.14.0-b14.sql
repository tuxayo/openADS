-- Type du champ demande / références cadastrales :
-- suppression du varchar(100) car taille insuffisante
ALTER TABLE demande
ALTER terrain_references_cadastrales TYPE text;

-- Ajout des numéros d'année sur deux chiffres dans les dossiers d'instruction
UPDATE dossier SET annee = to_char(date_demande, 'YY');