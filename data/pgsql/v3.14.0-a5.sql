--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a5
--
--
--
-- @package nomApplication
-- @version SVN : $Id: v3.14.0-a5.sql 2913 2014-04-03 09:28:23Z vpihour $
--------------------------------------------------------------------------------

--
-- Changement du nombre de caractères BP et CEDEX dans la table dossier
-- 5 à 15 (comme dans demande et dossier_autorisation)
--

ALTER TABLE dossier
ALTER terrain_adresse_bp TYPE character varying(15),
ALTER terrain_adresse_cedex TYPE character varying(15);

--
-- Modification de la variable adresse_direction_RAR en 
-- adresse_direction_urbanisme_RAR si celle-ci existe
--
UPDATE om_parametre SET libelle = 'adresse_direction_urbanisme_RAR'
    WHERE libelle = 'adresse_direction_RAR';
-- Création de la variable si celle-ci n'existe pas
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'), 'adresse_direction_urbanisme_RAR', '', 1
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'adresse_direction_urbanisme_RAR'
    );
-- Ajout de la bonne description
UPDATE om_parametre SET valeur = 'DIRECTION DU DÉVELOPPEMENT URBAIN
SERVICE DES AUTORISATIONS D''URBANISME
40, Rue Fauchier
13233 MARSEILLE'
    WHERE libelle = 'adresse_direction_urbanisme_RAR';