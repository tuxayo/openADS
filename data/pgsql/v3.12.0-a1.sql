--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.12.0-a1
--
-- XXX Ce fichier doit être renommé en v3.11.0-a1.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.12.0-a1.sql 2595 2013-12-05 18:14:26Z nhaye $
-------------------------------------------------------------------------------

--
-- Commentaires des tables
--

-- action
COMMENT ON TABLE action IS 'Action effectuée lors de l''ajout d''un événement d''instruction';
COMMENT ON COLUMN action.action IS 'Identifiant unique';
COMMENT ON COLUMN action.libelle IS 'Libellé de l''action';
COMMENT ON COLUMN action.regle_etat IS 'Règle de calcul de l''état des dossiers d''instruction';
COMMENT ON COLUMN action.regle_delai IS 'Règle de calcul du délai des dossiers d''instruction';
COMMENT ON COLUMN action.regle_accord_tacite IS 'Règle de calcul de l''accord tacite des dossiers d''instruction';
COMMENT ON COLUMN action.regle_avis IS 'Règle de calcul de l''avis des dossiers d''instruction';
COMMENT ON COLUMN action.regle_date_limite IS 'Règle de calcul de la date limite des dossiers d''instruction';
COMMENT ON COLUMN action.regle_date_notification_delai IS 'Règle de calcul de la date de notification de délai des dossiers d''instruction';
COMMENT ON COLUMN action.regle_date_complet IS 'Règle de calcul de la date de complétude des dossiers d''instruction';
COMMENT ON COLUMN action.regle_date_validite IS 'Règle de calcul de date de validité des dossiers d''instruction';
COMMENT ON COLUMN action.regle_date_decision IS 'Règle de calcul de date de décision des dossiers d''instruction';
COMMENT ON COLUMN action.regle_date_chantier IS 'Règle de calcul de date d''ouverture de chantier chantier';
COMMENT ON COLUMN action.regle_date_achevement IS 'Règle de calcul de date d''achèvement de travaux';
COMMENT ON COLUMN action.regle_date_conformite IS 'Règle de calcul de date de conformité des dossiers d''instruction';
COMMENT ON COLUMN action.regle_date_rejet IS 'Règle de calcul de la date de rejet des dossiers d''instruction';
COMMENT ON COLUMN action.regle_date_dernier_depot IS 'Règle de calcul de la date de dernier dépôt des dossiers d''instruction';
COMMENT ON COLUMN action.regle_date_limite_incompletude IS 'Règle de calcul de la date limite d''incomplétude des dossiers d''instruction';
COMMENT ON COLUMN action.regle_delai_incompletude IS 'Règle de calcul du délai d''incomplétude des dossiers d''instruction';
COMMENT ON COLUMN action.regle_autorite_competente IS 'Règle de calcul de l''autorité compétente des dossiers d''instruction';

-- affectation_automatique
COMMENT ON TABLE affectation_automatique IS 'Les dossiers d''instruction sont affectés aux instructeurs en fonction du type détaillé de dossier d''autorisation, arrondissement, quartier et section (dans cette priorité)';
COMMENT ON COLUMN affectation_automatique.affectation_automatique IS 'Identifiant unique';
COMMENT ON COLUMN affectation_automatique.arrondissement IS 'Arrondissement affecté à l''instructeur';
COMMENT ON COLUMN affectation_automatique.quartier IS 'Quartier affecté à l''instructeur';
COMMENT ON COLUMN affectation_automatique.section IS 'Section affectée à l''instructeur';
COMMENT ON COLUMN affectation_automatique.instructeur IS 'Identifiant de l''instructeur';
COMMENT ON COLUMN affectation_automatique.dossier_autorisation_type_detaille IS 'Type détaillé de dossier d''autorisation affectés à l''instructeur';

-- architecte

COMMENT ON TABLE architecte IS 'Coordonnées d''un architecte';
COMMENT ON COLUMN architecte.architecte IS 'Identifiant unique';
COMMENT ON COLUMN architecte.nom IS 'Nom de famille de l''architecte';
COMMENT ON COLUMN architecte.prenom IS 'Prénom de l''architecte';
COMMENT ON COLUMN architecte.adresse1 IS 'Adresse de l''architecte';
COMMENT ON COLUMN architecte.adresse2 IS 'Complément d''adresse de l''architecte';
COMMENT ON COLUMN architecte.cp IS 'Code postal de l''adresse de l''architecte';
COMMENT ON COLUMN architecte.ville IS 'Ville de l''adresse de l''architecte';
COMMENT ON COLUMN architecte.pays IS 'Pays de résidence de l''architecte';
COMMENT ON COLUMN architecte.inscription IS 'Numéro d''inscription au conseil national de l''ordre des architectes de l''architecte';
COMMENT ON COLUMN architecte.telephone IS 'Téléphone de l''architecte';
COMMENT ON COLUMN architecte.fax IS 'Fax de l''architecte';
COMMENT ON COLUMN architecte.email IS 'Email de l''architecte';
COMMENT ON COLUMN architecte.note IS 'Note à propos de l''architecte';
COMMENT ON COLUMN architecte.frequent IS 'Permet de faire apparaître l''architecte dans la recherche sur les architectes dans les données techniques';
COMMENT ON COLUMN architecte.nom_cabinet IS 'Nom du cabinet dans lequel travail l''architecte';

-- arrondissement
COMMENT ON TABLE arrondissement IS 'Division territoriale, administrative de la ville';
COMMENT ON COLUMN arrondissement.arrondissement IS 'Identifiant unique';
COMMENT ON COLUMN arrondissement.libelle IS 'Libellé de l''arrondissement';
COMMENT ON COLUMN arrondissement.code_postal IS 'Code postal de l''arrondissement';
COMMENT ON COLUMN arrondissement.code_impots IS 'Code impôt de l''arrondissement, notamment utilisé pour les références cadastrales afin de récupérer l''adresse depuis un SIG';

-- autorite_competente
COMMENT ON TABLE autorite_competente IS 'Autorité compétente pour rendre une décision sur une instruction';
COMMENT ON COLUMN autorite_competente.autorite_competente IS 'Identifiant unique';
COMMENT ON COLUMN autorite_competente.code IS 'Code de l''autorité compétente';
COMMENT ON COLUMN autorite_competente.libelle IS 'Libellé de l''autorité compétente';
COMMENT ON COLUMN autorite_competente.description IS 'Description de l''autorité compétente';

-- avis_consultation
COMMENT ON TABLE avis_consultation IS 'Avis décidé après une consultation';
COMMENT ON COLUMN avis_consultation.avis_consultation IS 'Identifiant unique';
COMMENT ON COLUMN avis_consultation.libelle IS 'Libellé de l''avis';
COMMENT ON COLUMN avis_consultation.abrege IS 'Abréviation de l''avis';
COMMENT ON COLUMN avis_consultation.om_validite_debut IS 'Date de début de validité de l''avis';
COMMENT ON COLUMN avis_consultation.om_validite_fin IS 'Date de fin de validité de l''avis';

-- avis_decision
COMMENT ON TABLE avis_decision IS 'Avis décidé après l''instruction d''un dossier d''instruction';
COMMENT ON COLUMN avis_decision.libelle IS 'Libellé de l''avis';
COMMENT ON COLUMN avis_decision.typeavis IS 'Type d''avis à rendre';
COMMENT ON COLUMN avis_decision.sitadel IS 'Type de mouvement sitadel';
COMMENT ON COLUMN avis_decision.sitadel_motif IS 'Motif de refus sitadel';
COMMENT ON COLUMN avis_decision.avis_decision IS 'Identifiant unique';

-- bible
COMMENT ON TABLE bible IS 'Bible qui regroupe des textes applicables sur les événements d''instruction';
COMMENT ON COLUMN bible.bible IS 'Identifiant unique';
COMMENT ON COLUMN bible.libelle IS 'Libellé du texte de la bible';
COMMENT ON COLUMN bible.evenement IS 'Événement sur lequel le texte de la bible est applicable';
COMMENT ON COLUMN bible.contenu IS 'Texte de la bible';
COMMENT ON COLUMN bible.complement IS 'Complément de l''événement d''instruction sur lequel s''applique le texte de la bible';
COMMENT ON COLUMN bible.automatique IS 'Applique automatiquement le texte de la bible sur la lettre type';
COMMENT ON COLUMN bible.dossier_autorisation_type IS 'Type du dossier d''autorisation sur lequel le texte de la bible est applicable';

-- blocnote
COMMENT ON TABLE blocnote IS 'Annotation concernant le dossier d''instruction lié';
COMMENT ON COLUMN blocnote.blocnote IS 'Identifiant unique';
COMMENT ON COLUMN blocnote.categorie IS 'Catégorie de la note';
COMMENT ON COLUMN blocnote.note IS 'Contenu de la note';
COMMENT ON COLUMN blocnote.dossier IS 'Dossier d''instruction lié';

-- cerfa
COMMENT ON TABLE cerfa IS 'Configuration de l''affichage des formulaires CERFA';
COMMENT ON COLUMN cerfa.cerfa IS 'Identifiant unique';
COMMENT ON COLUMN cerfa.libelle IS 'Libellé du formulaire CERFA';
COMMENT ON COLUMN cerfa.code IS 'Code du formulaire CERFA';
COMMENT ON COLUMN cerfa.om_validite_debut IS 'Début de validité du formulaire CERFA';
COMMENT ON COLUMN cerfa.om_validite_fin IS 'Fin de validité du formulaire CERFA';
COMMENT ON COLUMN cerfa.am_lotiss IS 'Va afficher le champ am_lotiss';
COMMENT ON COLUMN cerfa.am_autre_div IS 'Va afficher le champ am_autre_div';
COMMENT ON COLUMN cerfa.am_camping IS 'Va afficher le champ am_camping';
COMMENT ON COLUMN cerfa.am_caravane IS 'Va afficher le champ am_caravane';
COMMENT ON COLUMN cerfa.am_carav_duree IS 'Va afficher le champ am_carav_duree';
COMMENT ON COLUMN cerfa.am_statio IS 'Va afficher le champ am_statio';
COMMENT ON COLUMN cerfa.am_statio_cont IS 'Va afficher le champ am_statio_cont';
COMMENT ON COLUMN cerfa.am_affou_exhau IS 'Va afficher le champ am_affou_exhau';
COMMENT ON COLUMN cerfa.am_affou_exhau_sup IS 'Va afficher le champ am_affou_exhau_sup';
COMMENT ON COLUMN cerfa.am_affou_prof IS 'Va afficher le champ am_affou_prof';
COMMENT ON COLUMN cerfa.am_exhau_haut IS 'Va afficher le champ am_exhau_haut';
COMMENT ON COLUMN cerfa.am_coupe_abat IS 'Va afficher le champ am_coupe_abat';
COMMENT ON COLUMN cerfa.am_prot_plu IS 'Va afficher le champ am_prot_plu';
COMMENT ON COLUMN cerfa.am_prot_muni IS 'Va afficher le champ am_prot_muni';
COMMENT ON COLUMN cerfa.am_mobil_voyage IS 'Va afficher le champ am_mobil_voyage';
COMMENT ON COLUMN cerfa.am_aire_voyage IS 'Va afficher le champ am_aire_voyage';
COMMENT ON COLUMN cerfa.am_rememb_afu IS 'Va afficher le champ am_rememb_afu';
COMMENT ON COLUMN cerfa.am_parc_resid_loi IS 'Va afficher le champ am_parc_resid_loi';
COMMENT ON COLUMN cerfa.am_sport_moto IS 'Va afficher le champ am_sport_moto';
COMMENT ON COLUMN cerfa.am_sport_attrac IS 'Va afficher le champ am_sport_attrac';
COMMENT ON COLUMN cerfa.am_sport_golf IS 'Va afficher le champ am_sport_golf';
COMMENT ON COLUMN cerfa.am_mob_art IS 'Va afficher le champ am_mob_art';
COMMENT ON COLUMN cerfa.am_modif_voie_esp IS 'Va afficher le champ am_modif_voie_esp';
COMMENT ON COLUMN cerfa.am_plant_voie_esp IS 'Va afficher le champ am_plant_voie_esp';
COMMENT ON COLUMN cerfa.am_chem_ouv_esp IS 'Va afficher le champ am_chem_ouv_esp';
COMMENT ON COLUMN cerfa.am_agri_peche IS 'Va afficher le champ am_agri_peche';
COMMENT ON COLUMN cerfa.am_crea_voie IS 'Va afficher le champ am_crea_voie';
COMMENT ON COLUMN cerfa.am_modif_voie_exist IS 'Va afficher le champ am_modif_voie_exist';
COMMENT ON COLUMN cerfa.am_crea_esp_sauv IS 'Va afficher le champ am_crea_esp_sauv';
COMMENT ON COLUMN cerfa.am_crea_esp_class IS 'Va afficher le champ am_crea_esp_class';
COMMENT ON COLUMN cerfa.am_projet_desc IS 'Va afficher le champ am_projet_desc';
COMMENT ON COLUMN cerfa.am_terr_surf IS 'Va afficher le champ am_terr_surf';
COMMENT ON COLUMN cerfa.am_tranche_desc IS 'Va afficher le champ am_tranche_desc';
COMMENT ON COLUMN cerfa.am_lot_max_nb IS 'Va afficher le champ am_lot_max_nb';
COMMENT ON COLUMN cerfa.am_lot_max_shon IS 'Va afficher le champ am_lot_max_shon';
COMMENT ON COLUMN cerfa.am_lot_cstr_cos IS 'Va afficher le champ am_lot_cstr_cos';
COMMENT ON COLUMN cerfa.am_lot_cstr_plan IS 'Va afficher le champ am_lot_cstr_plan';
COMMENT ON COLUMN cerfa.am_lot_cstr_vente IS 'Va afficher le champ am_lot_cstr_vente';
COMMENT ON COLUMN cerfa.am_lot_fin_diff IS 'Va afficher le champ am_lot_fin_diff';
COMMENT ON COLUMN cerfa.am_lot_consign IS 'Va afficher le champ am_lot_consign';
COMMENT ON COLUMN cerfa.am_lot_gar_achev IS 'Va afficher le champ am_lot_gar_achev';
COMMENT ON COLUMN cerfa.am_lot_vente_ant IS 'Va afficher le champ am_lot_vente_ant';
COMMENT ON COLUMN cerfa.am_empl_nb IS 'Va afficher le champ am_empl_nb';
COMMENT ON COLUMN cerfa.am_tente_nb IS 'Va afficher le champ am_tente_nb';
COMMENT ON COLUMN cerfa.am_carav_nb IS 'Va afficher le champ am_carav_nb';
COMMENT ON COLUMN cerfa.am_mobil_nb IS 'Va afficher le champ am_mobil_nb';
COMMENT ON COLUMN cerfa.am_pers_nb IS 'Va afficher le champ am_pers_nb';
COMMENT ON COLUMN cerfa.am_empl_hll_nb IS 'Va afficher le champ am_empl_hll_nb';
COMMENT ON COLUMN cerfa.am_hll_shon IS 'Va afficher le champ am_hll_shon';
COMMENT ON COLUMN cerfa.am_periode_exploit IS 'Va afficher le champ am_periode_exploit';
COMMENT ON COLUMN cerfa.am_exist_agrand IS 'Va afficher le champ am_exist_agrand';
COMMENT ON COLUMN cerfa.am_exist_date IS 'Va afficher le champ am_exist_date';
COMMENT ON COLUMN cerfa.am_exist_num IS 'Va afficher le champ am_exist_num';
COMMENT ON COLUMN cerfa.am_exist_nb_avant IS 'Va afficher le champ am_exist_nb_avant';
COMMENT ON COLUMN cerfa.am_exist_nb_apres IS 'Va afficher le champ am_exist_nb_apres';
COMMENT ON COLUMN cerfa.am_coupe_bois IS 'Va afficher le champ am_coupe_bois';
COMMENT ON COLUMN cerfa.am_coupe_parc IS 'Va afficher le champ am_coupe_parc';
COMMENT ON COLUMN cerfa.am_coupe_align IS 'Va afficher le champ am_coupe_align';
COMMENT ON COLUMN cerfa.am_coupe_ess IS 'Va afficher le champ am_coupe_ess';
COMMENT ON COLUMN cerfa.am_coupe_age IS 'Va afficher le champ am_coupe_age';
COMMENT ON COLUMN cerfa.am_coupe_dens IS 'Va afficher le champ am_coupe_dens';
COMMENT ON COLUMN cerfa.am_coupe_qual IS 'Va afficher le champ am_coupe_qual';
COMMENT ON COLUMN cerfa.am_coupe_trait IS 'Va afficher le champ am_coupe_trait';
COMMENT ON COLUMN cerfa.am_coupe_autr IS 'Va afficher le champ am_coupe_autr';
COMMENT ON COLUMN cerfa.co_archi_recours IS 'Va afficher le champ co_archi_recours';
COMMENT ON COLUMN cerfa.co_cstr_nouv IS 'Va afficher le champ co_cstr_nouv';
COMMENT ON COLUMN cerfa.co_cstr_exist IS 'Va afficher le champ co_cstr_exist';
COMMENT ON COLUMN cerfa.co_cloture IS 'Va afficher le champ co_cloture';
COMMENT ON COLUMN cerfa.co_elec_tension IS 'Va afficher le champ co_elec_tension';
COMMENT ON COLUMN cerfa.co_div_terr IS 'Va afficher le champ co_div_terr';
COMMENT ON COLUMN cerfa.co_projet_desc IS 'Va afficher le champ co_projet_desc';
COMMENT ON COLUMN cerfa.co_anx_pisc IS 'Va afficher le champ co_anx_pisc';
COMMENT ON COLUMN cerfa.co_anx_gara IS 'Va afficher le champ co_anx_gara';
COMMENT ON COLUMN cerfa.co_anx_veran IS 'Va afficher le champ co_anx_veran';
COMMENT ON COLUMN cerfa.co_anx_abri IS 'Va afficher le champ co_anx_abri';
COMMENT ON COLUMN cerfa.co_anx_autr IS 'Va afficher le champ co_anx_autr';
COMMENT ON COLUMN cerfa.co_anx_autr_desc IS 'Va afficher le champ co_anx_autr_desc';
COMMENT ON COLUMN cerfa.co_tot_log_nb IS 'Va afficher le champ co_tot_log_nb';
COMMENT ON COLUMN cerfa.co_tot_ind_nb IS 'Va afficher le champ co_tot_ind_nb';
COMMENT ON COLUMN cerfa.co_tot_coll_nb IS 'Va afficher le champ co_tot_coll_nb';
COMMENT ON COLUMN cerfa.co_mais_piece_nb IS 'Va afficher le champ co_mais_piece_nb';
COMMENT ON COLUMN cerfa.co_mais_niv_nb IS 'Va afficher le champ co_mais_niv_nb';
COMMENT ON COLUMN cerfa.co_fin_lls_nb IS 'Va afficher le champ co_fin_lls_nb';
COMMENT ON COLUMN cerfa.co_fin_aa_nb IS 'Va afficher le champ co_fin_aa_nb';
COMMENT ON COLUMN cerfa.co_fin_ptz_nb IS 'Va afficher le champ co_fin_ptz_nb';
COMMENT ON COLUMN cerfa.co_fin_autr_nb IS 'Va afficher le champ co_fin_autr_nb';
COMMENT ON COLUMN cerfa.co_fin_autr_desc IS 'Va afficher le champ co_fin_autr_desc';
COMMENT ON COLUMN cerfa.co_mais_contrat_ind IS 'Va afficher le champ co_mais_contrat_ind';
COMMENT ON COLUMN cerfa.co_uti_pers IS 'Va afficher le champ co_uti_pers';
COMMENT ON COLUMN cerfa.co_uti_vente IS 'Va afficher le champ co_uti_vente';
COMMENT ON COLUMN cerfa.co_uti_loc IS 'Va afficher le champ co_uti_loc';
COMMENT ON COLUMN cerfa.co_uti_princ IS 'Va afficher le champ co_uti_princ';
COMMENT ON COLUMN cerfa.co_uti_secon IS 'Va afficher le champ co_uti_secon';
COMMENT ON COLUMN cerfa.co_resid_agees IS 'Va afficher le champ co_resid_agees';
COMMENT ON COLUMN cerfa.co_resid_etud IS 'Va afficher le champ co_resid_etud';
COMMENT ON COLUMN cerfa.co_resid_tourism IS 'Va afficher le champ co_resid_tourism';
COMMENT ON COLUMN cerfa.co_resid_hot_soc IS 'Va afficher le champ co_resid_hot_soc';
COMMENT ON COLUMN cerfa.co_resid_soc IS 'Va afficher le champ co_resid_soc';
COMMENT ON COLUMN cerfa.co_resid_hand IS 'Va afficher le champ co_resid_hand';
COMMENT ON COLUMN cerfa.co_resid_autr IS 'Va afficher le champ co_resid_autr';
COMMENT ON COLUMN cerfa.co_resid_autr_desc IS 'Va afficher le champ co_resid_autr_desc';
COMMENT ON COLUMN cerfa.co_foyer_chamb_nb IS 'Va afficher le champ co_foyer_chamb_nb';
COMMENT ON COLUMN cerfa.co_log_1p_nb IS 'Va afficher le champ co_log_1p_nb';
COMMENT ON COLUMN cerfa.co_log_2p_nb IS 'Va afficher le champ co_log_2p_nb';
COMMENT ON COLUMN cerfa.co_log_3p_nb IS 'Va afficher le champ co_log_3p_nb';
COMMENT ON COLUMN cerfa.co_log_4p_nb IS 'Va afficher le champ co_log_4p_nb';
COMMENT ON COLUMN cerfa.co_log_5p_nb IS 'Va afficher le champ co_log_5p_nb';
COMMENT ON COLUMN cerfa.co_log_6p_nb IS 'Va afficher le champ co_log_6p_nb';
COMMENT ON COLUMN cerfa.co_bat_niv_nb IS 'Va afficher le champ co_bat_niv_nb';
COMMENT ON COLUMN cerfa.co_trx_exten IS 'Va afficher le champ co_trx_exten';
COMMENT ON COLUMN cerfa.co_trx_surelev IS 'Va afficher le champ co_trx_surelev';
COMMENT ON COLUMN cerfa.co_trx_nivsup IS 'Va afficher le champ co_trx_nivsup';
COMMENT ON COLUMN cerfa.co_demont_periode IS 'Va afficher le champ co_demont_periode';
COMMENT ON COLUMN cerfa.co_sp_transport IS 'Va afficher le champ co_sp_transport';
COMMENT ON COLUMN cerfa.co_sp_enseign IS 'Va afficher le champ co_sp_enseign';
COMMENT ON COLUMN cerfa.co_sp_act_soc IS 'Va afficher le champ co_sp_act_soc';
COMMENT ON COLUMN cerfa.co_sp_ouvr_spe IS 'Va afficher le champ co_sp_ouvr_spe';
COMMENT ON COLUMN cerfa.co_sp_sante IS 'Va afficher le champ co_sp_sante';
COMMENT ON COLUMN cerfa.co_sp_culture IS 'Va afficher le champ co_sp_culture';
COMMENT ON COLUMN cerfa.co_statio_avt_nb IS 'Va afficher le champ co_statio_avt_nb';
COMMENT ON COLUMN cerfa.co_statio_apr_nb IS 'Va afficher le champ co_statio_apr_nb';
COMMENT ON COLUMN cerfa.co_statio_adr IS 'Va afficher le champ co_statio_adr';
COMMENT ON COLUMN cerfa.co_statio_place_nb IS 'Va afficher le champ co_statio_place_nb';
COMMENT ON COLUMN cerfa.co_statio_tot_surf IS 'Va afficher le champ co_statio_tot_surf';
COMMENT ON COLUMN cerfa.co_statio_tot_shob IS 'Va afficher le champ co_statio_tot_shob';
COMMENT ON COLUMN cerfa.co_statio_comm_cin_surf IS 'Va afficher le champ co_statio_comm_cin_surf';
COMMENT ON COLUMN cerfa.tab_surface IS 'Va afficher le champ tab_surface';
COMMENT ON COLUMN cerfa.dm_constr_dates IS 'Va afficher le champ dm_constr_dates';
COMMENT ON COLUMN cerfa.dm_total IS 'Va afficher le champ dm_total';
COMMENT ON COLUMN cerfa.dm_partiel IS 'Va afficher le champ dm_partiel';
COMMENT ON COLUMN cerfa.dm_projet_desc IS 'Va afficher le champ dm_projet_desc';
COMMENT ON COLUMN cerfa.dm_tot_log_nb IS 'Va afficher le champ dm_tot_log_nb';
COMMENT ON COLUMN cerfa.tax_surf_tot IS 'Va afficher le champ tax_surf_tot';
COMMENT ON COLUMN cerfa.tax_surf IS 'Va afficher le champ tax_surf';
COMMENT ON COLUMN cerfa.tax_surf_suppr_mod IS 'Va afficher le champ tax_surf_suppr_mod';
COMMENT ON COLUMN cerfa.tab_tax_su_princ IS 'Va afficher le champ tab_tax_su_princ';
COMMENT ON COLUMN cerfa.tab_tax_su_heber IS 'Va afficher le champ tab_tax_su_heber';
COMMENT ON COLUMN cerfa.tab_tax_su_secon IS 'Va afficher le champ tab_tax_su_secon';
COMMENT ON COLUMN cerfa.tab_tax_su_tot IS 'Va afficher le champ tab_tax_su_tot';
COMMENT ON COLUMN cerfa.tax_ext_pret IS 'Va afficher le champ tax_ext_pret';
COMMENT ON COLUMN cerfa.tax_ext_desc IS 'Va afficher le champ tax_ext_desc';
COMMENT ON COLUMN cerfa.tax_surf_tax_exist_cons IS 'Va afficher le champ tax_surf_tax_exist_cons';
COMMENT ON COLUMN cerfa.tax_log_exist_nb IS 'Va afficher le champ tax_log_exist_nb';
COMMENT ON COLUMN cerfa.tax_trx_presc_ppr IS 'Va afficher le champ tax_trx_presc_ppr';
COMMENT ON COLUMN cerfa.tax_monu_hist IS 'Va afficher le champ tax_monu_hist';
COMMENT ON COLUMN cerfa.tax_comm_nb IS 'Va afficher le champ tax_comm_nb';
COMMENT ON COLUMN cerfa.tab_tax_su_non_habit_surf IS 'Va afficher le champ tab_tax_su_non_habit_surf';
COMMENT ON COLUMN cerfa.tab_tax_am IS 'Va afficher le champ tab_tax_am';
COMMENT ON COLUMN cerfa.vsd_surf_planch_smd IS 'Va afficher le champ vsd_surf_planch_smd';
COMMENT ON COLUMN cerfa.vsd_unit_fonc_sup IS 'Va afficher le champ vsd_unit_fonc_sup';
COMMENT ON COLUMN cerfa.vsd_unit_fonc_constr_sup IS 'Va afficher le champ vsd_unit_fonc_constr_sup';
COMMENT ON COLUMN cerfa.vsd_val_terr IS 'Va afficher le champ vsd_val_terr';
COMMENT ON COLUMN cerfa.vsd_const_sxist_non_dem_surf IS 'Va afficher le champ vsd_const_sxist_non_dem_surf';
COMMENT ON COLUMN cerfa.vsd_rescr_fisc IS 'Va afficher le champ vsd_rescr_fisc';
COMMENT ON COLUMN cerfa.pld_val_terr IS 'Va afficher le champ pld_val_terr';
COMMENT ON COLUMN cerfa.pld_const_exist_dem IS 'Va afficher le champ pld_const_exist_dem';
COMMENT ON COLUMN cerfa.pld_const_exist_dem_surf IS 'Va afficher le champ pld_const_exist_dem_surf';
COMMENT ON COLUMN cerfa.code_cnil IS 'Va afficher le champ code_cnil';
COMMENT ON COLUMN cerfa.terr_juri_titul IS 'Va afficher le champ terr_juri_titul';
COMMENT ON COLUMN cerfa.terr_juri_lot IS 'Va afficher le champ terr_juri_lot';
COMMENT ON COLUMN cerfa.terr_juri_zac IS 'Va afficher le champ terr_juri_zac';
COMMENT ON COLUMN cerfa.terr_juri_afu IS 'Va afficher le champ terr_juri_afu';
COMMENT ON COLUMN cerfa.terr_juri_pup IS 'Va afficher le champ terr_juri_pup';
COMMENT ON COLUMN cerfa.terr_juri_oin IS 'Va afficher le champ terr_juri_oin';
COMMENT ON COLUMN cerfa.terr_juri_desc IS 'Va afficher le champ terr_juri_desc';
COMMENT ON COLUMN cerfa.terr_div_surf_etab IS 'Va afficher le champ terr_div_surf_etab';
COMMENT ON COLUMN cerfa.terr_div_surf_av_div IS 'Va afficher le champ terr_div_surf_av_div';
COMMENT ON COLUMN cerfa.doc_date IS 'Va afficher le champ doc_date';
COMMENT ON COLUMN cerfa.doc_tot_trav IS 'Va afficher le champ doc_tot_trav';
COMMENT ON COLUMN cerfa.doc_tranche_trav IS 'Va afficher le champ doc_tranche_trav';
COMMENT ON COLUMN cerfa.doc_tranche_trav_desc IS 'Va afficher le champ doc_tranche_trav_desc';
COMMENT ON COLUMN cerfa.doc_surf IS 'Va afficher le champ doc_surf';
COMMENT ON COLUMN cerfa.doc_nb_log IS 'Va afficher le champ doc_nb_log';
COMMENT ON COLUMN cerfa.doc_nb_log_indiv IS 'Va afficher le champ doc_nb_log_indiv';
COMMENT ON COLUMN cerfa.doc_nb_log_coll IS 'Va afficher le champ doc_nb_log_coll';
COMMENT ON COLUMN cerfa.doc_nb_log_lls IS 'Va afficher le champ doc_nb_log_lls';
COMMENT ON COLUMN cerfa.doc_nb_log_aa IS 'Va afficher le champ doc_nb_log_aa';
COMMENT ON COLUMN cerfa.doc_nb_log_ptz IS 'Va afficher le champ doc_nb_log_ptz';
COMMENT ON COLUMN cerfa.doc_nb_log_autre IS 'Va afficher le champ doc_nb_log_autre';
COMMENT ON COLUMN cerfa.daact_date IS 'Va afficher le champ daact_date';
COMMENT ON COLUMN cerfa.daact_date_chgmt_dest IS 'Va afficher le champ daact_date_chgmt_dest';
COMMENT ON COLUMN cerfa.daact_tot_trav IS 'Va afficher le champ daact_tot_trav';
COMMENT ON COLUMN cerfa.daact_tranche_trav IS 'Va afficher le champ daact_tranche_trav';
COMMENT ON COLUMN cerfa.daact_tranche_trav_desc IS 'Va afficher le champ daact_tranche_trav_desc';
COMMENT ON COLUMN cerfa.daact_surf IS 'Va afficher le champ daact_surf';
COMMENT ON COLUMN cerfa.daact_nb_log IS 'Va afficher le champ daact_nb_log';
COMMENT ON COLUMN cerfa.daact_nb_log_indiv IS 'Va afficher le champ daact_nb_log_indiv';
COMMENT ON COLUMN cerfa.daact_nb_log_coll IS 'Va afficher le champ daact_nb_log_coll';
COMMENT ON COLUMN cerfa.daact_nb_log_lls IS 'Va afficher le champ daact_nb_log_lls';
COMMENT ON COLUMN cerfa.daact_nb_log_aa IS 'Va afficher le champ daact_nb_log_aa';
COMMENT ON COLUMN cerfa.daact_nb_log_ptz IS 'Va afficher le champ daact_nb_log_ptz';
COMMENT ON COLUMN cerfa.daact_nb_log_autre IS 'Va afficher le champ daact_nb_log_autre';
COMMENT ON COLUMN cerfa.am_div_mun IS 'Va afficher le champ am_div_mun';
COMMENT ON COLUMN cerfa.co_perf_energ IS 'Va afficher le champ co_perf_energ';
COMMENT ON COLUMN cerfa.architecte IS 'Va afficher le champ architecte';
COMMENT ON COLUMN cerfa.co_statio_avt_shob IS 'Va afficher le champ co_statio_avt_shob';
COMMENT ON COLUMN cerfa.co_statio_apr_shob IS 'Va afficher le champ co_statio_apr_shob';
COMMENT ON COLUMN cerfa.co_statio_avt_surf IS 'Va afficher le champ co_statio_avt_surf';
COMMENT ON COLUMN cerfa.co_statio_apr_surf IS 'Va afficher le champ co_statio_apr_surf';
COMMENT ON COLUMN cerfa.co_trx_amgt IS 'Va afficher le champ co_trx_amgt';
COMMENT ON COLUMN cerfa.co_modif_aspect IS 'Va afficher le champ co_modif_aspect';
COMMENT ON COLUMN cerfa.co_modif_struct IS 'Va afficher le champ co_modif_struct';
COMMENT ON COLUMN cerfa.co_ouvr_elec IS 'Va afficher le champ co_ouvr_elec';
COMMENT ON COLUMN cerfa.co_ouvr_infra IS 'Va afficher le champ co_ouvr_infra';
COMMENT ON COLUMN cerfa.co_trx_imm IS 'Va afficher le champ co_trx_imm';
COMMENT ON COLUMN cerfa.co_cstr_shob IS 'Va afficher le champ co_cstr_shob';
COMMENT ON COLUMN cerfa.am_voyage_deb IS 'Va afficher le champ am_voyage_deb';
COMMENT ON COLUMN cerfa.am_voyage_fin IS 'Va afficher le champ am_voyage_fin';
COMMENT ON COLUMN cerfa.am_modif_amgt IS 'Va afficher le champ am_modif_amgt';
COMMENT ON COLUMN cerfa.am_lot_max_shob IS 'Va afficher le champ am_lot_max_shob';
COMMENT ON COLUMN cerfa.mod_desc IS 'Va afficher le champ mod_desc';
COMMENT ON COLUMN cerfa.tr_total IS 'Va afficher le champ tr_total';
COMMENT ON COLUMN cerfa.tr_partiel IS 'Va afficher le champ tr_partiel';
COMMENT ON COLUMN cerfa.tr_desc IS 'Va afficher le champ tr_desc';
COMMENT ON COLUMN cerfa.avap_co_elt_pro IS 'Va afficher le champ avap_co_elt_pro';
COMMENT ON COLUMN cerfa.avap_nouv_haut_surf IS 'Va afficher le champ avap_nouv_haut_surf';
COMMENT ON COLUMN cerfa.avap_co_clot IS 'Va afficher le champ avap_co_clot';
COMMENT ON COLUMN cerfa.avap_aut_coup_aba_arb IS 'Va afficher le champ avap_aut_coup_aba_arb';
COMMENT ON COLUMN cerfa.avap_ouv_infra IS 'Va afficher le champ avap_ouv_infra';
COMMENT ON COLUMN cerfa.avap_aut_inst_mob IS 'Va afficher le champ avap_aut_inst_mob';
COMMENT ON COLUMN cerfa.avap_aut_plant IS 'Va afficher le champ avap_aut_plant';
COMMENT ON COLUMN cerfa.avap_aut_auv_elec IS 'Va afficher le champ avap_aut_auv_elec';
COMMENT ON COLUMN cerfa.tax_dest_loc_tr IS 'Va afficher le champ tax_dest_loc_tr';

-- civilite
COMMENT ON TABLE civilite IS 'Titre de civilité';
COMMENT ON COLUMN civilite.civilite IS 'Identifiant unique';
COMMENT ON COLUMN civilite.code IS 'Code de la civilité';
COMMENT ON COLUMN civilite.libelle IS 'Libellé de la civilité';
COMMENT ON COLUMN civilite.om_validite_debut IS 'Date de début de validité de la civilité';
COMMENT ON COLUMN civilite.om_validite_fin IS 'Date de fin de validité de la civilité';

-- commission
COMMENT ON TABLE commission IS 'Demande de passage en commission';
COMMENT ON COLUMN commission.commission IS 'Identifiant unique';
COMMENT ON COLUMN commission.code IS 'Code de la commission';
COMMENT ON COLUMN commission.commission_type IS 'Type de la commission';
COMMENT ON COLUMN commission.libelle IS 'Libellé de la commission';
COMMENT ON COLUMN commission.date_commission IS 'Date de passage de la commission';
COMMENT ON COLUMN commission.heure_commission IS 'Heure de passage de la commission';
COMMENT ON COLUMN commission.lieu_adresse_ligne1 IS 'Adresse de passage de la commission';
COMMENT ON COLUMN commission.lieu_adresse_ligne2 IS 'Complément d''adresse de passage de la commission';
COMMENT ON COLUMN commission.lieu_salle IS 'Salle de passage de la commission';
COMMENT ON COLUMN commission.listes_de_diffusion IS 'Liste des emails à notifier pour le compte rendu de la commission';
COMMENT ON COLUMN commission.participants IS 'Participants à la commission';
COMMENT ON COLUMN commission.om_fichier_commission_ordre_jour IS 'Identifiant du fichier de l''ordre du jour de la commission finalisé dans le système de stockage';
COMMENT ON COLUMN commission.om_final_commission_ordre_jour IS 'Permet de savoir si le fichier de l''ordre du jour de la commission est finalisé';
COMMENT ON COLUMN commission.om_fichier_commission_compte_rendu IS 'Identifiant du fichier du compte rendu de la commission finalisé dans le système de stockage';
COMMENT ON COLUMN commission.om_final_commission_compte_rendu IS 'Permet de savoir si le fichier du compte rendu de la commission est finalisé';


-- commission_type

COMMENT ON TABLE commission_type IS 'Les types de commission';
COMMENT ON COLUMN commission_type.commission_type IS 'Identifiant unique';
COMMENT ON COLUMN commission_type.code IS 'Code du type de commission';
COMMENT ON COLUMN commission_type.libelle IS 'Libellé du type de commission';
COMMENT ON COLUMN commission_type.lieu_adresse_ligne1 IS 'Adresse des commissions de ce type';
COMMENT ON COLUMN commission_type.lieu_adresse_ligne2 IS 'Complément d''adresse des commissions de ce type';
COMMENT ON COLUMN commission_type.lieu_salle IS 'Salle où ont lieu les commissions de ce type';
COMMENT ON COLUMN commission_type.listes_de_diffusion IS 'Liste de diffusion des commissions de ce type';
COMMENT ON COLUMN commission_type.participants IS 'Participants aux commissions de ce type';
COMMENT ON COLUMN commission_type.corps_du_courriel IS 'Corps du courriel diffusé à la liste de diffusion des commissions de ce type';
COMMENT ON COLUMN commission_type.om_validite_debut IS 'Date de début de validité de ce type de commission';
COMMENT ON COLUMN commission_type.om_validite_fin IS 'Date de fin de validité de ce type de commission';

-- consultation
COMMENT ON TABLE consultation IS 'Demande de consultation d''un dossier d''instruction à un service';
COMMENT ON COLUMN consultation.consultation IS 'Identifiant unique';
COMMENT ON COLUMN consultation.dossier IS 'Dossier d''instruction sur lequel il y a la demande de consultation';
COMMENT ON COLUMN consultation.date_envoi IS 'Date d''envoi de la consultation';
COMMENT ON COLUMN consultation.date_retour IS 'Date de retour de la consultation';
COMMENT ON COLUMN consultation.date_limite IS 'Date limite pour la consultation';
COMMENT ON COLUMN consultation.service IS 'Service demandé pour la consultation';
COMMENT ON COLUMN consultation.avis_consultation IS 'Avis de la consultation';
COMMENT ON COLUMN consultation.date_reception IS 'Date de réception de la consultation';
COMMENT ON COLUMN consultation.motivation IS 'Motivation du service pour l''avis rendu sur la consultation';
COMMENT ON COLUMN consultation.fichier IS 'Document joint à la réponse de consultation';
COMMENT ON COLUMN consultation.lu IS 'Indique que la demande de consultation a été lue';
COMMENT ON COLUMN consultation.code_barres IS 'Code barres de la consultation';
COMMENT ON COLUMN consultation.om_fichier_consultation IS 'Document résumé de la consultation';
COMMENT ON COLUMN consultation.om_final_consultation IS 'Document résumé de la consultation finalisée';

-- demande
COMMENT ON TABLE demande IS 'Demande saisie par le guichet unique';
COMMENT ON COLUMN demande.demande IS 'Identifiant unique';
COMMENT ON COLUMN demande.dossier_autorisation_type_detaille IS 'Type détaillé du dossier d''autorisation (PCI, AZ, AT, ...) à sa création';
COMMENT ON COLUMN demande.demande_type IS 'Type de la demande, influe sur le type du dossier d''instruction (initial, modificatif, transfert, ...) à sa création';
COMMENT ON COLUMN demande.dossier_instruction IS 'Dossier d''instruction de la demande';
COMMENT ON COLUMN demande.dossier_autorisation IS 'Dossier d''autorisation de la demande';
COMMENT ON COLUMN demande.date_demande IS 'Date de la demande';
COMMENT ON COLUMN demande.terrain_references_cadastrales IS 'Références cadastrales de l''adresse du terrain';
COMMENT ON COLUMN demande.terrain_adresse_voie_numero IS 'Numéro de la voie de l''adresse du terrain';
COMMENT ON COLUMN demande.terrain_adresse_voie IS 'Nom de la voie de l''adresse du terrain';
COMMENT ON COLUMN demande.terrain_adresse_lieu_dit IS 'Lieu dit de l''adresse du terrain';
COMMENT ON COLUMN demande.terrain_adresse_localite IS 'Ville de l''adresse du terrain';
COMMENT ON COLUMN demande.terrain_adresse_code_postal IS 'Code postal de l''adresse du terrain';
COMMENT ON COLUMN demande.terrain_adresse_bp IS 'Boîte postale de l''adresse du terrain';
COMMENT ON COLUMN demande.terrain_adresse_cedex IS 'Cedex de l''adresse du terrain';
COMMENT ON COLUMN demande.terrain_superficie IS 'Superficie du terrain';
COMMENT ON COLUMN demande.instruction_recepisse IS 'Récipissé de l''instruction lors de la création de la demande';
COMMENT ON COLUMN demande.arrondissement IS 'Arrondissement du terrain de la demande';

-- demande_nature
COMMENT ON TABLE demande_nature IS 'Nature de la demande';
COMMENT ON COLUMN demande_nature.demande_nature IS 'Identifiant unique';
COMMENT ON COLUMN demande_nature.code IS 'Code de la nature de la demande';
COMMENT ON COLUMN demande_nature.libelle IS 'Libellé de la nature de la demande';
COMMENT ON COLUMN demande_nature.description IS 'Description de la nature de la demande';

-- demande_type
COMMENT ON TABLE demande_type IS 'Type de la demande';
COMMENT ON COLUMN demande_type.demande_type IS 'Identifiant unique';
COMMENT ON COLUMN demande_type.code IS 'Code du type de la demande';
COMMENT ON COLUMN demande_type.libelle IS 'Libellé du type de la demande';
COMMENT ON COLUMN demande_type.description IS 'Description du type de la demande';
COMMENT ON COLUMN demande_type.demande_nature IS 'Nature de la demande';
COMMENT ON COLUMN demande_type.groupe IS 'Groupe du type de la demande. Permet de filtrer les choix du champ dossier_autorisation_type_detaille';
COMMENT ON COLUMN demande_type.dossier_instruction_type IS 'Type du dossier d''instruction créé par la demande';
COMMENT ON COLUMN demande_type.dossier_autorisation_type_detaille IS 'Type détaillé du dossier d''autorisation crée par la demande. Permet de filtrer les choix du champ dossier_instruction_type';
COMMENT ON COLUMN demande_type.contraintes IS 'Permet de définir si les informations des demandeurs du dossier d''instruction précédent doivent être récupérées';
COMMENT ON COLUMN demande_type.qualification IS 'Le dossier d''instruction crée doit être à qualifier';
COMMENT ON COLUMN demande_type.evenement IS 'Événement à instancier lors de la création de la demande';

-- demandeur

COMMENT ON TABLE demandeur IS 'Pétitionnaire et délégataire de chaque dossier d''instruction';
COMMENT ON COLUMN demandeur.demandeur IS 'Identifiant unique';
COMMENT ON COLUMN demandeur.type_demandeur IS 'Type de demandeur : pétitionnaire ou délégataire';
COMMENT ON COLUMN demandeur.qualite IS 'Qualité du demandeur : particulier ou personne morale';
COMMENT ON COLUMN demandeur.particulier_nom IS 'Nom du demandeur de qualité particulier';
COMMENT ON COLUMN demandeur.particulier_prenom IS 'Prénom du demandeur de qualité particulier';
COMMENT ON COLUMN demandeur.particulier_date_naissance IS 'Date de naissance du demandeur de qualité particulier';
COMMENT ON COLUMN demandeur.particulier_commune_naissance IS 'Commune de naissance du demandeur de qualité particulier';
COMMENT ON COLUMN demandeur.particulier_departement_naissance IS 'Département de naissance du demandeur de qualité particulier';
COMMENT ON COLUMN demandeur.personne_morale_denomination IS 'Dénomination du demandeur de qualité personne marole';
COMMENT ON COLUMN demandeur.personne_morale_raison_sociale IS 'Raison sociale du demandeur de qualité personne morale';
COMMENT ON COLUMN demandeur.personne_morale_siret IS 'SIRET du demandeur de qualité personne morale';
COMMENT ON COLUMN demandeur.personne_morale_categorie_juridique IS 'Catégorie juridique du demandeur de qualité personne morale';
COMMENT ON COLUMN demandeur.personne_morale_nom IS 'Nom du demandeur de qualité personne morale';
COMMENT ON COLUMN demandeur.personne_morale_prenom IS 'Prénom du demandeur de qualité personne morale';
COMMENT ON COLUMN demandeur.numero IS 'Numéro de voie de l''adresse du demandeur';
COMMENT ON COLUMN demandeur.voie IS 'Voie de l''adresse du demandeur';
COMMENT ON COLUMN demandeur.complement IS 'Complément d''adresse du demandeur';
COMMENT ON COLUMN demandeur.lieu_dit IS 'Lieu-dit de l''adresse du demandeur';
COMMENT ON COLUMN demandeur.localite IS 'Localité de l''adresse du demandeur';
COMMENT ON COLUMN demandeur.code_postal IS 'Code postal de l''adresse du demandeur';
COMMENT ON COLUMN demandeur.bp IS 'Boîte postale de l''adresse du demandeur';
COMMENT ON COLUMN demandeur.cedex IS 'Cedex de l''adresse du demandeur';
COMMENT ON COLUMN demandeur.pays IS 'Pays de résidence du demandeur';
COMMENT ON COLUMN demandeur.division_territoriale IS 'Division territoriale de résidence du demandeur';
COMMENT ON COLUMN demandeur.telephone_fixe IS 'Numéro de téléphone fixe du demandeur';
COMMENT ON COLUMN demandeur.telephone_mobile IS 'Numéro de téléphone mobile du demandeur';
COMMENT ON COLUMN demandeur.indicatif IS 'Indicatif du pays de résidence du demandeur';
COMMENT ON COLUMN demandeur.courriel IS 'Adresse mail du demandeur';
COMMENT ON COLUMN demandeur.notification IS 'Notification par mail du demandeur';
COMMENT ON COLUMN demandeur.frequent IS 'Demandeur fréquent';
COMMENT ON COLUMN demandeur.particulier_civilite IS 'Civilité du demandeur de qualité particulier';
COMMENT ON COLUMN demandeur.personne_morale_civilite IS 'Civilité du demandeur de qualité personne morale';

-- direction
COMMENT ON TABLE direction IS 'Direction des divisions, nécessaire aussi pour obtenir le nom du responsable de service dans certains documents';
COMMENT ON COLUMN direction.direction IS 'Identifiant unique';
COMMENT ON COLUMN direction.code IS 'Code de la direction';
COMMENT ON COLUMN direction.libelle IS 'Libellé de la direction';
COMMENT ON COLUMN direction.description IS 'Description de la direction';
COMMENT ON COLUMN direction.chef IS 'Nom et prénom du chef de la direction';
COMMENT ON COLUMN direction.om_validite_debut IS 'Date de début de validité de la direction';
COMMENT ON COLUMN direction.om_validite_fin IS 'Date de fin de validité de la direction';

-- division
COMMENT ON TABLE division IS 'Division regroupant les instructeurs';
COMMENT ON COLUMN division.division IS 'Identifiant unique';
COMMENT ON COLUMN division.code IS 'Code de la division';
COMMENT ON COLUMN division.libelle IS 'Libellé de la division';
COMMENT ON COLUMN division.description IS 'Description de la division';
COMMENT ON COLUMN division.chef IS 'Nom et prénom du chef de la division';
COMMENT ON COLUMN division.direction IS 'Direction à laquelle est rattachée la division';
COMMENT ON COLUMN division.om_validite_debut IS 'Date de début de validité de la division';
COMMENT ON COLUMN division.om_validite_fin IS 'Date de fin de validité de la division';

-- document_numerise
COMMENT ON TABLE document_numerise IS 'Document numérisé rattaché à un dossier d''instruction';
COMMENT ON COLUMN document_numerise.document_numerise IS 'Identifiant unique';
COMMENT ON COLUMN document_numerise.uid IS 'Identifiant unique du fichier, compose aussi son chemin';
COMMENT ON COLUMN document_numerise.dossier IS 'Dossier d''instruction auquel est rattaché le document';
COMMENT ON COLUMN document_numerise.nom_fichier IS 'Nom du document';
COMMENT ON COLUMN document_numerise.date_creation IS 'Date de création du document';
COMMENT ON COLUMN document_numerise.document_numerise_type IS 'Type du document';

-- document_numerise_type
COMMENT ON TABLE document_numerise_type IS 'Type de document numérisé';
COMMENT ON COLUMN document_numerise_type.document_numerise_type IS 'Identifiant unique';
COMMENT ON COLUMN document_numerise_type.code IS 'Code du type de document numérisé';
COMMENT ON COLUMN document_numerise_type.libelle IS 'Libellé du type de document numérisé';
COMMENT ON COLUMN document_numerise_type.document_numerise_type_categorie IS 'Catégorie du type de document numérisé';

-- document_numerise_type_categorie
COMMENT ON TABLE document_numerise_type_categorie IS 'Catégorie du type de document numérisé';
COMMENT ON COLUMN document_numerise_type_categorie.document_numerise_type_categorie IS 'Identifiant unique';
COMMENT ON COLUMN document_numerise_type_categorie.libelle IS 'Libellé de la catégorie du type de document numérisé';

-- donnees_techniques
COMMENT ON TABLE donnees_techniques IS 'Données techniques';
COMMENT ON COLUMN donnees_techniques.donnees_techniques IS 'Identifiant unique';
COMMENT ON COLUMN donnees_techniques.dossier_instruction IS 'Dossier d''instruction auquel sont rattachés les données techniques';
COMMENT ON COLUMN donnees_techniques.lot IS 'Lot sur lequel sont rattachés les données techniques';
COMMENT ON COLUMN donnees_techniques.am_lotiss IS 'Lotissement';
COMMENT ON COLUMN donnees_techniques.am_autre_div IS 'Remembrement réalisé par une association foncière urbaine libre';
COMMENT ON COLUMN donnees_techniques.am_camping IS 'Terrain de camping';
COMMENT ON COLUMN donnees_techniques.am_caravane IS 'Installation d’une caravane en dehors d’un terrain de camping ou d’un parc résidentiel de loisirs';
COMMENT ON COLUMN donnees_techniques.am_carav_duree IS 'Durée annuelle d’installation (en mois)';
COMMENT ON COLUMN donnees_techniques.am_statio IS 'Aires de stationnement ouvertes au public, dépôts de véhicules et garages collectifs de caravanes ou de résidences mobiles de loisirs';
COMMENT ON COLUMN donnees_techniques.am_statio_cont IS 'Contenance de l''aire de stationnement (nombre d’unités)';
COMMENT ON COLUMN donnees_techniques.am_affou_exhau IS 'Travaux d’affouillements ou d’exhaussements du sol';
COMMENT ON COLUMN donnees_techniques.am_affou_exhau_sup IS 'Superficie (en m2) des travaux d’affouillements ou d’exhaussements du sol';
COMMENT ON COLUMN donnees_techniques.am_affou_prof IS 'Profondeur pour les travaux affouillements du sol';
COMMENT ON COLUMN donnees_techniques.am_exhau_haut IS 'Hauteur pour les travaux exhaussements du sol';
COMMENT ON COLUMN donnees_techniques.am_coupe_abat IS 'Coupe et abattage d’arbres';
COMMENT ON COLUMN donnees_techniques.am_prot_plu IS 'Modification ou suppression d’un élément protégé par un plan local d’urbanisme ou document d’urbanisme en tenant lieu (plan d’occupation des sols, plan de sauvegarde et de mise en valeur, plan d’aménagement de zone)';
COMMENT ON COLUMN donnees_techniques.am_prot_muni IS 'Modification ou suppression d’un élément protégé par une délibération du conseil municipal';
COMMENT ON COLUMN donnees_techniques.am_mobil_voyage IS 'Installation d’une résidence mobile constituant l’habitat permanent des gens du voyage pendant plus de trois mois consécutifs';
COMMENT ON COLUMN donnees_techniques.am_aire_voyage IS 'Aire d’accueil des gens du voyage';
COMMENT ON COLUMN donnees_techniques.am_rememb_afu IS 'Travaux ayant pour effet de modifier l’aménagement des abords d’un bâtiment situé dans un secteur sauvegardé';
COMMENT ON COLUMN donnees_techniques.am_parc_resid_loi IS 'Parc résidentiel de loisirs ou village de vacances';
COMMENT ON COLUMN donnees_techniques.am_sport_moto IS 'Aménagement d’un terrain pour la pratique de sports ou de loisirs motorisés';
COMMENT ON COLUMN donnees_techniques.am_sport_attrac IS 'Aménagement d’un parc d’attraction ou d’une aire de jeux et de sports';
COMMENT ON COLUMN donnees_techniques.am_sport_golf IS 'Aménagement d’un golf';
COMMENT ON COLUMN donnees_techniques.am_mob_art IS 'Installation de mobilier urbain, d’œuvre d’art';
COMMENT ON COLUMN donnees_techniques.am_modif_voie_esp IS 'Modification de voie ou espace publics';
COMMENT ON COLUMN donnees_techniques.am_plant_voie_esp IS 'Plantations effectuées sur les voies ou espaces publics';
COMMENT ON COLUMN donnees_techniques.am_chem_ouv_esp IS 'Chemin piétonnier ou objet mobilier destiné à l’accueil ou à l’information du public, lorsqu’ils sont nécessaires à la gestion ou à l’ouverture au public de ces espaces ou milieux';
COMMENT ON COLUMN donnees_techniques.am_agri_peche IS 'Aménagement nécessaire à l’exercice des activités agricoles, de pêche et de culture marine ou lacustres, conchylicoles, pastorales et forestières';
COMMENT ON COLUMN donnees_techniques.am_crea_voie IS 'Création d’une voie';
COMMENT ON COLUMN donnees_techniques.am_modif_voie_exist IS 'Travaux ayant pour effet de modifier les caractéristiques d’une voie existante';
COMMENT ON COLUMN donnees_techniques.am_crea_esp_sauv IS 'Création d’un espace public (secteur sauvegardé)';
COMMENT ON COLUMN donnees_techniques.am_crea_esp_class IS 'Création d’un espace public (site classé)';
COMMENT ON COLUMN donnees_techniques.am_projet_desc IS 'Description de votre projet de construction';
COMMENT ON COLUMN donnees_techniques.am_terr_surf IS 'Superficie du (ou des) terrain(s) à aménager (en m²)';
COMMENT ON COLUMN donnees_techniques.am_tranche_desc IS 'Si les travaux sont réalisés par tranches, en préciser le nombre et leur contenu';
COMMENT ON COLUMN donnees_techniques.am_lot_max_nb IS 'Nombre maximum de lots projetés';
COMMENT ON COLUMN donnees_techniques.am_lot_max_shon IS 'Surface de plancher maximale envisagée (en m²) des lots';
COMMENT ON COLUMN donnees_techniques.am_lot_cstr_cos IS 'Par application du coefficient d’occupation du sol (COS) à chaque lot';
COMMENT ON COLUMN donnees_techniques.am_lot_cstr_plan IS 'Conformément aux plans ou tableaux joints à la présente demande';
COMMENT ON COLUMN donnees_techniques.am_lot_cstr_vente IS 'La constructibilité sera déterminée à la vente de chaque lot. Dans ce cas, le lotisseur devra fournir un certificat aux constructeurs.';
COMMENT ON COLUMN donnees_techniques.am_lot_fin_diff IS 'Le projet fait l’objet d’une demande de travaux de finition différés';
COMMENT ON COLUMN donnees_techniques.am_lot_consign IS 'Consignation en compte bloqué';
COMMENT ON COLUMN donnees_techniques.am_lot_gar_achev IS 'Garantie financière d’achèvement des travaux';
COMMENT ON COLUMN donnees_techniques.am_lot_vente_ant IS 'Le projet fait l’objet d’une demande de vente ou location de lots par anticipation';
COMMENT ON COLUMN donnees_techniques.am_empl_nb IS 'Nombre maximum d’emplacements réservés aux tentes, caravanes ou résidences mobiles de loisirs';
COMMENT ON COLUMN donnees_techniques.am_tente_nb IS 'Nombre maximum d’emplacements réservés aux tentes';
COMMENT ON COLUMN donnees_techniques.am_carav_nb IS 'Nombre maximum d’emplacements réservés aux caravanes ';
COMMENT ON COLUMN donnees_techniques.am_mobil_nb IS 'Nombre maximum d’emplacements réservés aux mobiles de loisirs';
COMMENT ON COLUMN donnees_techniques.am_pers_nb IS 'Nombre maximal de personnes accueillies';
COMMENT ON COLUMN donnees_techniques.am_empl_hll_nb IS 'Nombre d’emplacements réservés aux HLL';
COMMENT ON COLUMN donnees_techniques.am_hll_shon IS 'Surface de plancher prévue, réservée aux HLL';
COMMENT ON COLUMN donnees_techniques.am_periode_exploit IS 'Lorsque le terrain est destiné à une exploitation saisonnière, veuillez préciser la (ou les) période(s) d’exploitation';
COMMENT ON COLUMN donnees_techniques.am_exist_agrand IS 'Agrandissement ou ré-aménagement d’une structure existante';
COMMENT ON COLUMN donnees_techniques.am_exist_date IS 'Date et/ou numéro de l’autorisation';
COMMENT ON COLUMN donnees_techniques.am_exist_num IS 'Nombre d’emplacements';
COMMENT ON COLUMN donnees_techniques.am_exist_nb_avant IS 'Nombre d’emplacements avant agrandissement ou ré-aménagement';
COMMENT ON COLUMN donnees_techniques.am_exist_nb_apres IS 'Nombre d’emplacements après agrandissement ou ré-aménagement';
COMMENT ON COLUMN donnees_techniques.am_coupe_bois IS 'Description de l''environnement : bois ou forêt';
COMMENT ON COLUMN donnees_techniques.am_coupe_parc IS 'Description de l''environnement : parc';
COMMENT ON COLUMN donnees_techniques.am_coupe_align IS 'Description de l''environnement : alignement (espaces verts urbains)';
COMMENT ON COLUMN donnees_techniques.am_coupe_ess IS 'Nature du boisement : Essences';
COMMENT ON COLUMN donnees_techniques.am_coupe_age IS 'Nature du boisement : Age';
COMMENT ON COLUMN donnees_techniques.am_coupe_dens IS 'Nature du boisement : Densité';
COMMENT ON COLUMN donnees_techniques.am_coupe_qual IS 'Nature du boisement : Qualité';
COMMENT ON COLUMN donnees_techniques.am_coupe_trait IS 'Nature du boisement : Traitement';
COMMENT ON COLUMN donnees_techniques.am_coupe_autr IS 'Nature du boisement : Autres';
COMMENT ON COLUMN donnees_techniques.co_archi_recours IS 'Vous avez eu recours à un architecte';
COMMENT ON COLUMN donnees_techniques.co_cstr_nouv IS 'Nouvelle construction';
COMMENT ON COLUMN donnees_techniques.co_cstr_exist IS 'Travaux ou changement de destination sur une construction existante';
COMMENT ON COLUMN donnees_techniques.co_cloture IS 'Travaux clôturés';
COMMENT ON COLUMN donnees_techniques.co_elec_tension IS 'Puissance électrique nécessaire à votre projet';
COMMENT ON COLUMN donnees_techniques.co_div_terr IS 'Le terrain doit être divisé en propriété ou en jouissance avant l’achèvement de la (ou des) construction(s)';
COMMENT ON COLUMN donnees_techniques.co_projet_desc IS 'Courte description de votre projet ou de vos travaux';
COMMENT ON COLUMN donnees_techniques.co_anx_pisc IS 'Complément construction : Piscine';
COMMENT ON COLUMN donnees_techniques.co_anx_gara IS 'Complément construction : Garage';
COMMENT ON COLUMN donnees_techniques.co_anx_veran IS 'Complément construction : Véranda';
COMMENT ON COLUMN donnees_techniques.co_anx_abri IS 'Complément construction : Abri de jardin';
COMMENT ON COLUMN donnees_techniques.co_anx_autr IS 'Complément construction : Autres annexes à l’habitation';
COMMENT ON COLUMN donnees_techniques.co_anx_autr_desc IS 'Informations complémentaires sur les autres annexes à l’habitation';
COMMENT ON COLUMN donnees_techniques.co_tot_log_nb IS 'Nombre total de logements créés';
COMMENT ON COLUMN donnees_techniques.co_tot_ind_nb IS 'Nombre total de logements individuels créés';
COMMENT ON COLUMN donnees_techniques.co_tot_coll_nb IS 'Nombre total de logements collectifs créés';
COMMENT ON COLUMN donnees_techniques.co_mais_piece_nb IS 'Nombre de pièces de la maison';
COMMENT ON COLUMN donnees_techniques.co_mais_niv_nb IS 'Nombre de niveaux de la maison';
COMMENT ON COLUMN donnees_techniques.co_fin_lls_nb IS 'Répartition du nombre total de logement créés par type de financement : Logement Locatif Social';
COMMENT ON COLUMN donnees_techniques.co_fin_aa_nb IS 'Répartition du nombre total de logement créés par type de financement : Accession Sociale (hors prêt à taux zéro)';
COMMENT ON COLUMN donnees_techniques.co_fin_ptz_nb IS 'Répartition du nombre total de logement créés par type de financement : Prêt à taux zéro';
COMMENT ON COLUMN donnees_techniques.co_fin_autr_nb IS 'Répartition du nombre total de logement créés par type de financement : Autres financements';
COMMENT ON COLUMN donnees_techniques.co_fin_autr_desc IS 'Description des autres financements';
COMMENT ON COLUMN donnees_techniques.co_mais_contrat_ind IS 'Souscrit un contrat de construction de maison individuelle';
COMMENT ON COLUMN donnees_techniques.co_uti_pers IS 'Occupation personnelle ou en compte propre';
COMMENT ON COLUMN donnees_techniques.co_uti_vente IS 'Mode d''utilisation principale des logements : Pour la vente';
COMMENT ON COLUMN donnees_techniques.co_uti_loc IS 'Mode d''utilisation principale des logements : Pour la location';
COMMENT ON COLUMN donnees_techniques.co_uti_princ IS 'Occupation personnelle : Résidence principale';
COMMENT ON COLUMN donnees_techniques.co_uti_secon IS 'Occupation personnelle : Résidence secondaire';
COMMENT ON COLUMN donnees_techniques.co_resid_agees IS 'Projet foyer ou résidence : Résidence pour personnes âgées';
COMMENT ON COLUMN donnees_techniques.co_resid_etud IS 'Projet foyer ou résidence : Résidence pour étudiants';
COMMENT ON COLUMN donnees_techniques.co_resid_tourism IS 'Projet foyer ou résidence : Résidence de tourisme';
COMMENT ON COLUMN donnees_techniques.co_resid_hot_soc IS 'Projet foyer ou résidence : Résidence hôtelière à vocation sociale';
COMMENT ON COLUMN donnees_techniques.co_resid_soc IS 'Projet foyer ou résidence : Résidence sociale';
COMMENT ON COLUMN donnees_techniques.co_resid_hand IS 'Projet foyer ou résidence : Résidence pour personnes handicapées';
COMMENT ON COLUMN donnees_techniques.co_resid_autr IS 'Projet foyer ou résidence : Résidence autres';
COMMENT ON COLUMN donnees_techniques.co_resid_autr_desc IS 'Précision sur résidence autres';
COMMENT ON COLUMN donnees_techniques.co_foyer_chamb_nb IS 'Nombre de chambres créées en foyer ou dans un hébergement d’un autre type';
COMMENT ON COLUMN donnees_techniques.co_log_1p_nb IS 'Repartition du nombre de logements créés selon le nombre de pièces : 1 pièce';
COMMENT ON COLUMN donnees_techniques.co_log_2p_nb IS 'Repartition du nombre de logements créés selon le nombre de pièces : 2 pièces';
COMMENT ON COLUMN donnees_techniques.co_log_3p_nb IS 'Repartition du nombre de logements créés selon le nombre de pièces : 3 pièces';
COMMENT ON COLUMN donnees_techniques.co_log_4p_nb IS 'Repartition du nombre de logements créés selon le nombre de pièces : 4 pièces';
COMMENT ON COLUMN donnees_techniques.co_log_5p_nb IS 'Repartition du nombre de logements créés selon le nombre de pièces : 5 pièces';
COMMENT ON COLUMN donnees_techniques.co_log_6p_nb IS 'Repartition du nombre de logements créés selon le nombre de pièces : 6 pièces';
COMMENT ON COLUMN donnees_techniques.co_bat_niv_nb IS 'Nombre de niveaux du bâtiment le plus élevé';
COMMENT ON COLUMN donnees_techniques.co_trx_exten IS 'Travaux comprennent notamment : Extension';
COMMENT ON COLUMN donnees_techniques.co_trx_surelev IS 'Travaux comprennent notamment : Surélévation';
COMMENT ON COLUMN donnees_techniques.co_trx_nivsup IS 'Travaux comprennent notamment : Création de niveaux supplémentaires';
COMMENT ON COLUMN donnees_techniques.co_demont_periode IS 'Période(s) de l’année durant laquelle (lesquelles) la construction doit être démontée';
COMMENT ON COLUMN donnees_techniques.co_sp_transport IS 'Destination des constructions futures en cas de réalisation au bénéfice d''un service public ou d''intérêt collectif : Transport';
COMMENT ON COLUMN donnees_techniques.co_sp_enseign IS 'Destination des constructions futures en cas de réalisation au bénéfice d''un service public ou d''intérêt collectif : Enseignement et recherche';
COMMENT ON COLUMN donnees_techniques.co_sp_act_soc IS 'Destination des constructions futures en cas de réalisation au bénéfice d''un service public ou d''intérêt collectif : Action sociale';
COMMENT ON COLUMN donnees_techniques.co_sp_ouvr_spe IS 'Destination des constructions futures en cas de réalisation au bénéfice d''un service public ou d''intérêt collectif : Ouvrage spécial';
COMMENT ON COLUMN donnees_techniques.co_sp_sante IS 'Destination des constructions futures en cas de réalisation au bénéfice d''un service public ou d''intérêt collectif : Santé';
COMMENT ON COLUMN donnees_techniques.co_sp_culture IS 'Destination des constructions futures en cas de réalisation au bénéfice d''un service public ou d''intérêt collectif : Culture et loisir';
COMMENT ON COLUMN donnees_techniques.co_statio_avt_nb IS 'Nombre de places de stationnement avant réalisation du projet';
COMMENT ON COLUMN donnees_techniques.co_statio_apr_nb IS 'Nombre de places de stationnement après réalisation du projet';
COMMENT ON COLUMN donnees_techniques.co_statio_adr IS 'Adresse(s) des aires de stationnement';
COMMENT ON COLUMN donnees_techniques.co_statio_place_nb IS 'Nombre de places affectées au projet, aménagées ou réservées en dehors du terrain sur lequel est situé le projet';
COMMENT ON COLUMN donnees_techniques.co_statio_tot_surf IS 'Surface totale affectée au stationnement';
COMMENT ON COLUMN donnees_techniques.co_statio_tot_shob IS 'Surface bâtie affectée au stationnement';
COMMENT ON COLUMN donnees_techniques.co_statio_comm_cin_surf IS 'Emprise au sol des surfaces, bâties ou non, affectées au stationnement';
COMMENT ON COLUMN donnees_techniques.su_avt_shon1 IS 'Tableau des surface, ligne : Habitation, colonne : Surface existante avant travaux (A)';
COMMENT ON COLUMN donnees_techniques.su_avt_shon2 IS 'Tableau des surface, ligne : Hébergement hôtelier, colonne : Surface existante avant travaux (A)';
COMMENT ON COLUMN donnees_techniques.su_avt_shon3 IS 'Tableau des surface, ligne : Bureau, colonne : Surface existante avant travaux (A)';
COMMENT ON COLUMN donnees_techniques.su_avt_shon4 IS 'Tableau des surface, ligne : Commerce, colonne : Surface existante avant travaux (A)';
COMMENT ON COLUMN donnees_techniques.su_avt_shon5 IS 'Tableau des surface, ligne : Artisanat, colonne : Surface existante avant travaux (A)';
COMMENT ON COLUMN donnees_techniques.su_avt_shon6 IS 'Tableau des surface, ligne : Industrie, colonne : Surface existante avant travaux (A)';
COMMENT ON COLUMN donnees_techniques.su_avt_shon7 IS 'Tableau des surface, ligne : Exploitation agricole ou forestière, colonne : Surface existante avant travaux (A)';
COMMENT ON COLUMN donnees_techniques.su_avt_shon8 IS 'Tableau des surface, ligne : Entrepôt, colonne : Surface existante avant travaux (A)';
COMMENT ON COLUMN donnees_techniques.su_avt_shon9 IS 'Tableau des surface, ligne : Service public ou d''intérêt collectif, colonne : Surface existante avant travaux (A)';
COMMENT ON COLUMN donnees_techniques.su_cstr_shon1 IS 'Tableau des surface, ligne : Habitation, colonne : Surface créée (B)';
COMMENT ON COLUMN donnees_techniques.su_cstr_shon2 IS 'Tableau des surface, ligne : Hébergement hôtelier, colonne : Surface créée (B)';
COMMENT ON COLUMN donnees_techniques.su_cstr_shon3 IS 'Tableau des surface, ligne : Bureau, colonne : Surface créée (B)';
COMMENT ON COLUMN donnees_techniques.su_cstr_shon4 IS 'Tableau des surface, ligne : Commerce, colonne : Surface créée (B)';
COMMENT ON COLUMN donnees_techniques.su_cstr_shon5 IS 'Tableau des surface, ligne : Artisanat, colonne : Surface créée (B)';
COMMENT ON COLUMN donnees_techniques.su_cstr_shon6 IS 'Tableau des surface, ligne : Industrie, colonne : Surface créée (B)';
COMMENT ON COLUMN donnees_techniques.su_cstr_shon7 IS 'Tableau des surface, ligne : Exploitation agricole ou forestière, colonne : Surface créée (B)';
COMMENT ON COLUMN donnees_techniques.su_cstr_shon8 IS 'Tableau des surface, ligne : Entrepôt, colonne : Surface créée (B)';
COMMENT ON COLUMN donnees_techniques.su_cstr_shon9 IS 'Tableau des surface, ligne : Service public ou d''intérêt collectif, colonne : Surface créée (B)';
COMMENT ON COLUMN donnees_techniques.su_trsf_shon1 IS 'Tableau des surface, ligne : Habitation, colonne : Surface créée par changement de destination (C)';
COMMENT ON COLUMN donnees_techniques.su_trsf_shon2 IS 'Tableau des surface, ligne : Hébergement hôtelier, colonne : Surface créée par changement de destination (C)';
COMMENT ON COLUMN donnees_techniques.su_trsf_shon3 IS 'Tableau des surface, ligne : Bureau, colonne : Surface créée par changement de destination (C)';
COMMENT ON COLUMN donnees_techniques.su_trsf_shon4 IS 'Tableau des surface, ligne : Commerce, colonne : Surface créée par changement de destination (C)';
COMMENT ON COLUMN donnees_techniques.su_trsf_shon5 IS 'Tableau des surface, ligne : Artisanat, colonne : Surface créée par changement de destination (C)';
COMMENT ON COLUMN donnees_techniques.su_trsf_shon6 IS 'Tableau des surface, ligne : Industrie, colonne : Surface créée par changement de destination (C)';
COMMENT ON COLUMN donnees_techniques.su_trsf_shon7 IS 'Tableau des surface, ligne : Exploitation agricole ou forestière, colonne : Surface créée par changement de destination (C)';
COMMENT ON COLUMN donnees_techniques.su_trsf_shon8 IS 'Tableau des surface, ligne : Entrepôt, colonne : Surface créée par changement de destination (C)';
COMMENT ON COLUMN donnees_techniques.su_trsf_shon9 IS 'Tableau des surface, ligne : Service public ou d''intérêt collectif, colonne : Surface créée par changement de destination (C)';
COMMENT ON COLUMN donnees_techniques.su_demo_shon1 IS 'Tableau des surface, ligne : Habitation, colonne : Surface supprimée (D)';
COMMENT ON COLUMN donnees_techniques.su_demo_shon2 IS 'Tableau des surface, ligne : Hébergement hôtelier, colonne : Surface supprimée (D)';
COMMENT ON COLUMN donnees_techniques.su_demo_shon3 IS 'Tableau des surface, ligne : Bureau, colonne : Surface supprimée (D)';
COMMENT ON COLUMN donnees_techniques.su_demo_shon4 IS 'Tableau des surface, ligne : Commerce, colonne : Surface supprimée (D)';
COMMENT ON COLUMN donnees_techniques.su_demo_shon5 IS 'Tableau des surface, ligne : Artisanat, colonne : Surface supprimée (D)';
COMMENT ON COLUMN donnees_techniques.su_demo_shon6 IS 'Tableau des surface, ligne : Industrie, colonne : Surface supprimée (D)';
COMMENT ON COLUMN donnees_techniques.su_demo_shon7 IS 'Tableau des surface, ligne : Exploitation agricole ou forestière, colonne : Surface supprimée (D)';
COMMENT ON COLUMN donnees_techniques.su_demo_shon8 IS 'Tableau des surface, ligne : Entrepôt, colonne : Surface supprimée (D)';
COMMENT ON COLUMN donnees_techniques.su_demo_shon9 IS 'Tableau des surface, ligne : Service public ou d''intérêt collectif, colonne : Surface supprimée (D)';
COMMENT ON COLUMN donnees_techniques.su_chge_shon1 IS 'Tableau des surface, ligne : Habitation, colonne : Surface supprimée par changement de destination (E)';
COMMENT ON COLUMN donnees_techniques.su_chge_shon2 IS 'Tableau des surface, ligne : Hébergement hôtelier, colonne : Surface supprimée par changement de destination (E)';
COMMENT ON COLUMN donnees_techniques.su_chge_shon3 IS 'Tableau des surface, ligne : Bureau, colonne : Surface supprimée par changement de destination (E)';
COMMENT ON COLUMN donnees_techniques.su_chge_shon4 IS 'Tableau des surface, ligne : Commerce, colonne : Surface supprimée par changement de destination (E)';
COMMENT ON COLUMN donnees_techniques.su_chge_shon5 IS 'Tableau des surface, ligne : Artisanat, colonne : Surface supprimée par changement de destination (E)';
COMMENT ON COLUMN donnees_techniques.su_chge_shon6 IS 'Tableau des surface, ligne : Industrie, colonne : Surface supprimée par changement de destination (E)';
COMMENT ON COLUMN donnees_techniques.su_chge_shon7 IS 'Tableau des surface, ligne : Exploitation agricole ou forestière, colonne : Surface supprimée par changement de destination (E)';
COMMENT ON COLUMN donnees_techniques.su_chge_shon8 IS 'Tableau des surface, ligne : Entrepôt, colonne : Surface supprimée par changement de destination (E)';
COMMENT ON COLUMN donnees_techniques.su_chge_shon9 IS 'Tableau des surface, ligne : Service public ou d''intérêt collectif, colonne : Surface supprimée par changement de destination (E)';
COMMENT ON COLUMN donnees_techniques.su_sup_shon1 IS 'Tableau des surface, ligne : Habitation, colonne : ?';
COMMENT ON COLUMN donnees_techniques.su_sup_shon2 IS 'Tableau des surface, ligne : Hébergement hôtelier, colonne : ?';
COMMENT ON COLUMN donnees_techniques.su_sup_shon3 IS 'Tableau des surface, ligne : Bureau, colonne : ?';
COMMENT ON COLUMN donnees_techniques.su_sup_shon4 IS 'Tableau des surface, ligne : Commerce, colonne : ?';
COMMENT ON COLUMN donnees_techniques.su_sup_shon5 IS 'Tableau des surface, ligne : Artisanat, colonne : ?';
COMMENT ON COLUMN donnees_techniques.su_sup_shon6 IS 'Tableau des surface, ligne : Industrie, colonne : ?';
COMMENT ON COLUMN donnees_techniques.su_sup_shon7 IS 'Tableau des surface, ligne : Exploitation agricole ou forestière, colonne : ?';
COMMENT ON COLUMN donnees_techniques.su_sup_shon8 IS 'Tableau des surface, ligne : Entrepôt, colonne : ?';
COMMENT ON COLUMN donnees_techniques.su_sup_shon9 IS 'Tableau des surface, ligne : Service public ou d''intérêt collectif, colonne : ';
COMMENT ON COLUMN donnees_techniques.su_tot_shon1 IS 'Tableau des surface, ligne : Habitation, colonne : Surface totale = (a)+(B)+(C)-(D)-(E)';
COMMENT ON COLUMN donnees_techniques.su_tot_shon2 IS 'Tableau des surface, ligne : Hébergement hôtelier, colonne : Surface totale = (a)+(B)+(C)-(D)-(E)';
COMMENT ON COLUMN donnees_techniques.su_tot_shon3 IS 'Tableau des surface, ligne : Bureau, colonne : Surface totale = (a)+(B)+(C)-(D)-(E)';
COMMENT ON COLUMN donnees_techniques.su_tot_shon4 IS 'Tableau des surface, ligne : Commerce, colonne : Surface totale = (a)+(B)+(C)-(D)-(E)';
COMMENT ON COLUMN donnees_techniques.su_tot_shon5 IS 'Tableau des surface, ligne : Artisanat, colonne : Surface totale = (a)+(B)+(C)-(D)-(E)';
COMMENT ON COLUMN donnees_techniques.su_tot_shon6 IS 'Tableau des surface, ligne : Industrie, colonne : Surface totale = (a)+(B)+(C)-(D)-(E)';
COMMENT ON COLUMN donnees_techniques.su_tot_shon7 IS 'Tableau des surface, ligne : Exploitation agricole ou forestière, colonne : Surface totale = (a)+(B)+(C)-(D)-(E)';
COMMENT ON COLUMN donnees_techniques.su_tot_shon8 IS 'Tableau des surface, ligne : Entrepôt, colonne : Surface totale = (a)+(B)+(C)-(D)-(E)';
COMMENT ON COLUMN donnees_techniques.su_tot_shon9 IS 'Tableau des surface, ligne : Service public ou d''intérêt collectif, colonne : Surface totale = (a)+(B)+(C)-(D)-(E)';
COMMENT ON COLUMN donnees_techniques.su_avt_shon_tot IS 'Tableau des surface, ligne : Surfaces totales, colonne : Surface existante avant travaux (A)';
COMMENT ON COLUMN donnees_techniques.su_cstr_shon_tot IS 'Tableau des surface, ligne : Surfaces totales, colonne : Surface créée (B)';
COMMENT ON COLUMN donnees_techniques.su_trsf_shon_tot IS 'Tableau des surface, ligne : Surfaces totales, colonne : Surface créée par changement de destination (C)';
COMMENT ON COLUMN donnees_techniques.su_chge_shon_tot IS 'Tableau des surface, ligne : Surfaces totales, colonne : Surface supprimée (D)';
COMMENT ON COLUMN donnees_techniques.su_demo_shon_tot IS 'Tableau des surface, ligne : Surfaces totales, colonne : Surface supprimée par changement de destination (E)';
COMMENT ON COLUMN donnees_techniques.su_sup_shon_tot IS 'Tableau des surface, ligne : Surfaces totales, colonne : ';
COMMENT ON COLUMN donnees_techniques.su_tot_shon_tot IS 'Tableau des surface, ligne : Surfaces totales, colonne : Surface totale = (a)+(B)+(C)-(D)-(E)';
COMMENT ON COLUMN donnees_techniques.dm_constr_dates IS 'Date(s) approximative(s) à laquelle le ou les bâtiments dont la démolition est envisagée ont été construits';
COMMENT ON COLUMN donnees_techniques.dm_total IS 'Démolition totale';
COMMENT ON COLUMN donnees_techniques.dm_partiel IS 'Démolition partielle';
COMMENT ON COLUMN donnees_techniques.dm_projet_desc IS 'En cas de démolition partielle, veuillez décrire les travaux qui seront, le cas échéant, effectués sur les constructions restantes';
COMMENT ON COLUMN donnees_techniques.dm_tot_log_nb IS 'Nombre de logement démolis';
COMMENT ON COLUMN donnees_techniques.tax_surf_tot IS 'Superficie totale du (ou des) terrain(s) (en m²)';
COMMENT ON COLUMN donnees_techniques.tax_surf IS 'Superficie du (ou des) terrain(s) à aménager (en m²)';
COMMENT ON COLUMN donnees_techniques.tax_surf_suppr_mod IS 'Surface taxable supprimée par la modification (en m²)';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_log_nb1 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Ne bénéficiant pas de prêt aidé, colonne : Nombre de logements avant modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_log_nb2 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''un PLAI ou LLTS, colonne : Nombre de logements avant modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_log_nb3 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''autre prêts aidés (PLUS, LES, PLSA, PLS, LLS), colonne : Nombre de logements avant modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_log_nb4 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''un prêt à taux zéro plus (PTZ+), colonne : Nombre de logements avant modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_log_nb_tot1 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Ne bénéficiant pas de prêt aidé, colonne : Nombre total de logement après modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_log_nb_tot2 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''un PLAI ou LLTS, colonne : Nombre total de logement après modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_log_nb_tot3 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''autre prêts aidés (PLUS, LES, PLSA, PLS, LLS), colonne : Nombre total de logement après modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_log_nb_tot4 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''un prêt à taux zéro plus (PTZ+), colonne : Nombre total de logement après modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf1 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Ne bénéficiant pas de prêt aidé, colonne : Surfaces créées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf2 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''un PLAI ou LLTS, colonne : Surfaces créées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf3 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''autre prêts aidés (PLUS, LES, PLSA, PLS, LLS), colonne :Surfaces créées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf4 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''un prêt à taux zéro plus (PTZ+), colonne :Surfaces créées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf_sup1 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Ne bénéficiant pas de prêt aidé, colonne : Surfaces supprimées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf_sup2 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''un PLAI ou LLTS, colonne : Surfaces supprimées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf_sup3 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''autre prêts aidés (PLUS, LES, PLSA, PLS, LLS), colonne : Surfaces supprimées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_princ_surf_sup4 IS 'Locaux à usage d''habitation principale et leurs annexes, ligne : Bénéficiant d''un prêt à taux zéro plus (PTZ+), colonne : Surfaces supprimées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_log_nb1 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Ne bénéficiant pas d''un prêt aidé, colonne : Nombre de logements avant modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_log_nb2 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Bénéficiant d''un PLAI ou LLTS, colonne : Nombre de logements avant modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_log_nb3 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Bénéficiant d''autres prêts aidés, colonne : Nombre de logements avant modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_log_nb_tot1 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Ne bénéficiant pas d''un prêt aidé, colonne : Nombre total de logement après modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_log_nb_tot2 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Bénéficiant d''un PLAI ou LLTS, colonne : Nombre total de logement après modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_log_nb_tot3 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Bénéficiant d''autres prêts aidés, colonne : Nombre total de logement après modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_surf1 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Ne bénéficiant pas d''un prêt aidé, colonne : Surfaces créées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_surf2 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Bénéficiant d''un PLAI ou LLTS, colonne : Surfaces créées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_surf3 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Bénéficiant d''autres prêts aidés, colonne : Surfaces créées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_surf_sup1 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Ne bénéficiant pas d''un prêt aidé, colonne : Surfaces supprimées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_surf_sup2 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Bénéficiant d''un PLAI ou LLTS, colonne : Surfaces supprimées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_heber_surf_sup3 IS 'Locaux à usage d''habitation et leurs annexes, ligne : Bénéficiant d''autres prêts aidés, colonne : Surfaces supprimées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_secon_log_nb IS 'Locaux à usage d''habitation secondaire et leurs annexes, colonne : Nombre de logements avant modifications';
COMMENT ON COLUMN donnees_techniques.tax_su_tot_log_nb IS 'Nombre total de logements, colonne : Nombre de logements avant modifications';
COMMENT ON COLUMN donnees_techniques.tax_su_secon_log_nb_tot IS 'Locaux à usage d''habitation secondaire et leurs annexes, colonne : Nombre total de logements après modifications';
COMMENT ON COLUMN donnees_techniques.tax_su_tot_log_nb_tot IS 'Nombre total de logements, colonne : Nombre total de logements après modifications';
COMMENT ON COLUMN donnees_techniques.tax_su_secon_surf IS 'Locaux à usage d''habitation secondaire et leurs annexes, colonne : Surfaces créées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_tot_surf IS 'Nombre total de logements, colonne : Surfaces créées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_secon_surf_sup IS 'Locaux à usage d''habitation secondaire et leurs annexes, colonne : Surfaces supprimées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_su_tot_surf_sup IS 'Nombre total de logements, colonne : Surfaces supprimées par la modification';
COMMENT ON COLUMN donnees_techniques.tax_ext_pret IS 'Aide pour la réalisation des travaux';
COMMENT ON COLUMN donnees_techniques.tax_ext_desc IS 'Description de l''aide pour la réalisation des travaux';
COMMENT ON COLUMN donnees_techniques.tax_surf_tax_exist_cons IS 'Conserver la surface taxable existante';
COMMENT ON COLUMN donnees_techniques.tax_log_exist_nb IS 'Quel est le nombre de logements existants ?';
COMMENT ON COLUMN donnees_techniques.tax_am_statio_ext IS 'Nombre de places de stationnement situées à l’extérieur de la construction';
COMMENT ON COLUMN donnees_techniques.tax_sup_bass_pisc IS 'Superficie du bassin de la piscine';
COMMENT ON COLUMN donnees_techniques.tax_empl_ten_carav_mobil_nb IS 'Nombre d’emplacements de tentes, de caravanes et de résidences mobiles de loisirs';
COMMENT ON COLUMN donnees_techniques.tax_empl_hll_nb IS 'Nombre d’emplacements pour les habitations légères de loisirs';
COMMENT ON COLUMN donnees_techniques.tax_eol_haut_nb IS 'Nombre d’éoliennes dont la hauteur est supérieure à 12 m';
COMMENT ON COLUMN donnees_techniques.tax_pann_volt_sup IS 'Superficie des panneaux photo-voltaïques posés au sol';
COMMENT ON COLUMN donnees_techniques.tax_am_statio_ext_sup IS 'Nombre de places de stationnement situées à l’extérieur de la construction';
COMMENT ON COLUMN donnees_techniques.tax_sup_bass_pisc_sup IS 'Superficie du bassin de la piscine';
COMMENT ON COLUMN donnees_techniques.tax_empl_ten_carav_mobil_nb_sup IS 'Nombre d’emplacements de tentes, de caravanes et de résidences mobiles de loisirs';
COMMENT ON COLUMN donnees_techniques.tax_empl_hll_nb_sup IS 'Nombre d’emplacements pour les habitations légères de loisirs';
COMMENT ON COLUMN donnees_techniques.tax_eol_haut_nb_sup IS 'Nombre d’éoliennes dont la hauteur est supérieure à 12 m';
COMMENT ON COLUMN donnees_techniques.tax_pann_volt_sup_sup IS 'Superficie des panneaux photo-voltaïques posés au sol';
COMMENT ON COLUMN donnees_techniques.tax_trx_presc_ppr IS 'Les travaux projetés sont-ils réalisés suite à des prescriptions résultant d’un Plan de Prévention des Risques naturels, technologiques ou miniers ?';
COMMENT ON COLUMN donnees_techniques.tax_monu_hist IS 'La construction projetée concerne t-elle un immeuble classé pasrmi les monuments historiques ou inscrit à l’inventaire des monuments historiques ?';
COMMENT ON COLUMN donnees_techniques.tax_comm_nb IS 'Nombre de commerces dont la surface de vente est inférieure à 400 m²';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf1 IS 'Surfaces taxables non destinés à l''habitation, ligne : Commerce inférieur à 400m², colonne : Surfaces créées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf2 IS 'Surfaces taxables non destinés à l''habitation, ligne : Locaux industriels et leurs annexes, colonne : Surfaces créées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf3 IS 'Surfaces taxables non destinés à l''habitation, ligne : Locaux artisanaux et leurs annexes, colonne : Surfaces créées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf4 IS 'Surfaces taxables non destinés à l''habitation, ligne : Entrepôt et hangars, colonne : Surfaces créées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf5 IS 'Surfaces taxables non destinés à l''habitation, ligne : Parcs de stationnement, colonne : Surfaces créées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf6 IS 'Surfaces taxables non destinés à l''habitation, ligne : Exploitation et coopératives agricoles, colonne : Surfaces créées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf7 IS 'Surfaces taxables non destinés à l''habitation, ligne : Centres équestres, colonne : Surfaces créées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_sup1 IS 'Surfaces taxables non destinés à l''habitation, ligne : Commerce inférieur à 400m², colonne : Surfaces supprimées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_sup2 IS 'Surfaces taxables non destinés à l''habitation, ligne : Locaux industriels et leurs annexes, colonne : Surfaces supprimées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_sup3 IS 'Surfaces taxables non destinés à l''habitation, ligne : Locaux artisanaux et leurs annexes, colonne : Surfaces supprimées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_sup4 IS 'Surfaces taxables non destinés à l''habitation, ligne : Entrepôt et hangars, colonne : Surfaces supprimées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_sup5 IS 'Surfaces taxables non destinés à l''habitation, ligne : Parcs de stationnement, colonne : Surfaces supprimées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_sup6 IS 'Surfaces taxables non destinés à l''habitation, ligne : Exploitation et coopératives agricoles, colonne : Surfaces supprimées';
COMMENT ON COLUMN donnees_techniques.tax_su_non_habit_surf_sup7 IS 'Surfaces taxables non destinés à l''habitation, ligne : Centres équestres, colonne : Surfaces supprimées';
COMMENT ON COLUMN donnees_techniques.vsd_surf_planch_smd IS 'La surface de plancher de la construction projetée est-elle égale ou supérieure au seuil minimal de densité';
COMMENT ON COLUMN donnees_techniques.vsd_unit_fonc_sup IS 'La superficie de votre unité foncière';
COMMENT ON COLUMN donnees_techniques.vsd_unit_fonc_constr_sup IS 'La superficie de l’unité foncière effectivement constructible';
COMMENT ON COLUMN donnees_techniques.vsd_val_terr IS 'La valeur du m² de terrain nu et libre';
COMMENT ON COLUMN donnees_techniques.vsd_const_sxist_non_dem_surf IS 'Les surfaces de plancher des constructions existantes non destinées à être démolies (en m²)';
COMMENT ON COLUMN donnees_techniques.vsd_rescr_fisc IS 'Si vous avez bénéficié avant le dépôt de votre demande d’un rescrit fiscal, indiquez sa date';
COMMENT ON COLUMN donnees_techniques.pld_val_terr IS 'Valeur du m² de terrain nu et libre';
COMMENT ON COLUMN donnees_techniques.pld_const_exist_dem IS 'Des constructions existantes sur votre terrain avant le 1er avril 1976 ont été démolies';
COMMENT ON COLUMN donnees_techniques.pld_const_exist_dem_surf IS 'Si oui, indiquez ici la surface de plancher démolie';
COMMENT ON COLUMN donnees_techniques.code_cnil IS 'code CNIL';
COMMENT ON COLUMN donnees_techniques.terr_juri_titul IS 'Titulaire d’un certificat d’urbanisme pour le terrain';
COMMENT ON COLUMN donnees_techniques.terr_juri_lot IS 'Le terrain est situé dans un lotissement';
COMMENT ON COLUMN donnees_techniques.terr_juri_zac IS 'Le terrain situé dans une Zone d’Aménagement Concertée (Z.A.C.)';
COMMENT ON COLUMN donnees_techniques.terr_juri_afu IS 'Le terrain fait partie d’un remembrement urbain (Association Foncière Urbain)';
COMMENT ON COLUMN donnees_techniques.terr_juri_pup IS 'Le terrain est situé dans un périmètre ayant fait l’objet d’une convention de Projet Urbain Partenarial (P.U.P)';
COMMENT ON COLUMN donnees_techniques.terr_juri_oin IS 'Le projet est situé dans le périmètre d’une Opération d’Intérêt National (O.I.N)';
COMMENT ON COLUMN donnees_techniques.terr_juri_desc IS 'Si votre terrain est concerné par l’un des cas ci-dessus, veuillez préciser, si vous les connaissez, les dates de décision ou d’autorisation, les numéros et les dénominations ';
COMMENT ON COLUMN donnees_techniques.terr_div_surf_etab IS 'surface des constructions déjà établies sur l’autre partie du terrain (en m2)';
COMMENT ON COLUMN donnees_techniques.terr_div_surf_av_div IS 'et la superficie du terrain avant division (en m²)';
COMMENT ON COLUMN donnees_techniques.doc_date IS 'Date ouverture chantier';
COMMENT ON COLUMN donnees_techniques.doc_tot_trav IS 'Pour la totalité des travaux';
COMMENT ON COLUMN donnees_techniques.doc_tranche_trav IS 'Pour une tranche des travaux';
COMMENT ON COLUMN donnees_techniques.doc_tranche_trav_desc IS 'Veuillez préciser quels sont les aménagements ou constructions commencés';
COMMENT ON COLUMN donnees_techniques.doc_surf IS 'Surface créée';
COMMENT ON COLUMN donnees_techniques.doc_nb_log IS 'Nombre de logements commencés';
COMMENT ON COLUMN donnees_techniques.doc_nb_log_indiv IS 'Nombre de logements individuels commencés';
COMMENT ON COLUMN donnees_techniques.doc_nb_log_coll IS 'Nombre de logements collectifs commencés';
COMMENT ON COLUMN donnees_techniques.doc_nb_log_lls IS 'Logement Locatif Social';
COMMENT ON COLUMN donnees_techniques.doc_nb_log_aa IS 'Accession Sociale';
COMMENT ON COLUMN donnees_techniques.doc_nb_log_ptz IS 'Prêt à taux zéro';
COMMENT ON COLUMN donnees_techniques.doc_nb_log_autre IS 'Autres financements';
COMMENT ON COLUMN donnees_techniques.daact_date IS 'Date achèvement chantier';
COMMENT ON COLUMN donnees_techniques.daact_date_chgmt_dest IS 'Date changement de destination';
COMMENT ON COLUMN donnees_techniques.daact_tot_trav IS 'Pour la totalité des travaux';
COMMENT ON COLUMN donnees_techniques.daact_tranche_trav IS 'Pour une tranche des travaux';
COMMENT ON COLUMN donnees_techniques.daact_tranche_trav_desc IS 'Veuillez préciser quels sont les aménagements ou constructions achevés';
COMMENT ON COLUMN donnees_techniques.daact_surf IS 'Surface créée';
COMMENT ON COLUMN donnees_techniques.daact_nb_log IS 'Nombre de logements terminés';
COMMENT ON COLUMN donnees_techniques.daact_nb_log_indiv IS 'Nombre de logements individuels terminés';
COMMENT ON COLUMN donnees_techniques.daact_nb_log_coll IS 'Nombre de logements collectifs terminés';
COMMENT ON COLUMN donnees_techniques.daact_nb_log_lls IS 'Logement Locatif Social ';
COMMENT ON COLUMN donnees_techniques.daact_nb_log_aa IS 'Accession sociale (hors prêt à taux zéro) ';
COMMENT ON COLUMN donnees_techniques.daact_nb_log_ptz IS 'Prêt à taux zéro';
COMMENT ON COLUMN donnees_techniques.daact_nb_log_autre IS 'Autres financements';
COMMENT ON COLUMN donnees_techniques.dossier_autorisation IS 'Dossier d''autorisation concernés par les données techniques';
COMMENT ON COLUMN donnees_techniques.am_div_mun IS 'Division foncière située dans une partie de la commune délimitée par le conseil municipal';
COMMENT ON COLUMN donnees_techniques.co_perf_energ IS 'Niveau de performance énergétique';
COMMENT ON COLUMN donnees_techniques.architecte IS 'Vous avez eu recours à un architecte';
COMMENT ON COLUMN donnees_techniques.co_statio_avt_shob IS 'Surface hors œuvre brute des aires bâties de stationnement en m² avant réalisation';
COMMENT ON COLUMN donnees_techniques.co_statio_apr_shob IS 'Surface hors œuvre brute des aires bâties de stationnement en m² après réalisation';
COMMENT ON COLUMN donnees_techniques.co_statio_avt_surf IS 'Surface de l’emprise au sol des aires non bâties de stationnement en m² avant construction';
COMMENT ON COLUMN donnees_techniques.co_statio_apr_surf IS 'Surface de l’emprise au sol des aires non bâties de stationnement en m² après réalisation';
COMMENT ON COLUMN donnees_techniques.co_trx_amgt IS '';
COMMENT ON COLUMN donnees_techniques.co_modif_aspect IS '';
COMMENT ON COLUMN donnees_techniques.co_modif_struct IS '';
COMMENT ON COLUMN donnees_techniques.co_ouvr_elec IS '';
COMMENT ON COLUMN donnees_techniques.co_ouvr_infra IS '';
COMMENT ON COLUMN donnees_techniques.co_trx_imm IS '';
COMMENT ON COLUMN donnees_techniques.co_cstr_shob IS '';
COMMENT ON COLUMN donnees_techniques.am_voyage_deb IS '';
COMMENT ON COLUMN donnees_techniques.am_voyage_fin IS '';
COMMENT ON COLUMN donnees_techniques.am_modif_amgt IS '';
COMMENT ON COLUMN donnees_techniques.am_lot_max_shob IS 'indiquez la surface hors œuvre brute (SHOB) maximale envisagée';
COMMENT ON COLUMN donnees_techniques.mod_desc IS 'Descriptiondes modifications apportées à votre projet';
COMMENT ON COLUMN donnees_techniques.tr_total IS 'transfert total';
COMMENT ON COLUMN donnees_techniques.tr_partiel IS 'transfert partiel';
COMMENT ON COLUMN donnees_techniques.tr_desc IS 'description de la (ou des) partie(s) tranférée(s)';
COMMENT ON COLUMN donnees_techniques.avap_co_elt_pro IS 'Modification ou suppression d''un élément protégé par une délibération du conseil municipal.';
COMMENT ON COLUMN donnees_techniques.avap_nouv_haut_surf IS 'Construction nouvelle de moins de  12m de hauteur et dont la surface hors œuvre brute ne dépasse 2m²';
COMMENT ON COLUMN donnees_techniques.avap_co_clot IS 'Clôture';
COMMENT ON COLUMN donnees_techniques.avap_aut_coup_aba_arb IS 'Coupe et abattage d''arbres';
COMMENT ON COLUMN donnees_techniques.avap_ouv_infra IS 'Ouvrage d''infrastructure';
COMMENT ON COLUMN donnees_techniques.avap_aut_inst_mob IS 'Installation de mobilier urbain ou d''oeuvre d''art';
COMMENT ON COLUMN donnees_techniques.avap_aut_plant IS 'Plantation effectuée sur voie ou espace public';
COMMENT ON COLUMN donnees_techniques.avap_aut_auv_elec IS 'Ouvrage et accessoires de lignes de distribution électrique';
COMMENT ON COLUMN donnees_techniques.tax_dest_loc_tr IS '';

-- dossier
COMMENT ON TABLE dossier IS 'Dossier d''instruction';
COMMENT ON COLUMN dossier.dossier IS 'Numéro du dossier d''instruction';
COMMENT ON COLUMN dossier.annee IS 'Année de la création du dossier d''instruction';
COMMENT ON COLUMN dossier.etat IS 'État du dossier d''instruction';
COMMENT ON COLUMN dossier.instructeur IS 'Instructeur en charge du dossier d''instruction';
COMMENT ON COLUMN dossier.date_demande IS 'Date de la demande';
COMMENT ON COLUMN dossier.date_depot IS 'Date du dépôt du dossier d''instruction';
COMMENT ON COLUMN dossier.date_complet IS 'Date de complétude du dossier d''instruction';
COMMENT ON COLUMN dossier.date_rejet IS 'Date de rejet du dossier d''instruction';
COMMENT ON COLUMN dossier.date_notification_delai IS 'Date de notification du délai au pétitionnaire';
COMMENT ON COLUMN dossier.delai IS 'Délai servant au calcul de la date de validité';
COMMENT ON COLUMN dossier.date_limite IS 'Date de limite du dossier d''instruction';
COMMENT ON COLUMN dossier.accord_tacite IS 'Soumis à un accord tacite';
COMMENT ON COLUMN dossier.date_decision IS 'Date de décision du dossier d''instruction';
COMMENT ON COLUMN dossier.date_validite IS 'Date de validité du dossier d''instruction';
COMMENT ON COLUMN dossier.date_chantier IS 'Date de début du chantier du dossier d''instruction';
COMMENT ON COLUMN dossier.date_achevement IS 'Date d''achèvement du chantier du dossier d''instruction';
COMMENT ON COLUMN dossier.date_conformite IS 'Date de conformité du dossier d''instruction';
COMMENT ON COLUMN dossier.parcelle IS 'Parcelle du dossier d''instruction';
COMMENT ON COLUMN dossier.pos IS 'Non utilisé';
COMMENT ON COLUMN dossier.sig IS 'Non utilisé';
COMMENT ON COLUMN dossier.batiment_nombre IS 'Non utilisé, voir données techniques';
COMMENT ON COLUMN dossier.logement_nombre IS 'Non utilisé, voir données techniques';
COMMENT ON COLUMN dossier.shon IS 'Non utilisé, voir données techniques';
COMMENT ON COLUMN dossier.shon_calcul IS 'Non utilisé, voir données techniques';
COMMENT ON COLUMN dossier.shob IS 'Non utilisé, voir données techniques';
COMMENT ON COLUMN dossier.lot IS 'Non utilisé';
COMMENT ON COLUMN dossier.hauteur IS 'Non utilisé, voir données techniques';
COMMENT ON COLUMN dossier.piece_nombre IS 'Non utilisé, voir données techniques';
COMMENT ON COLUMN dossier.amenagement IS 'Non utilisé, voir données techniques';
COMMENT ON COLUMN dossier.parcelle_lot IS 'Non utilisé';
COMMENT ON COLUMN dossier.parcelle_lot_lotissement IS 'Non utilisé';
COMMENT ON COLUMN dossier.description IS 'Description des contraintes';
COMMENT ON COLUMN dossier.temp1 IS 'Utilisé dans la bible';
COMMENT ON COLUMN dossier.temp2 IS 'Utilisé dans la bible';
COMMENT ON COLUMN dossier.temp3 IS 'Utilisé dans la bible';
COMMENT ON COLUMN dossier.temp4 IS 'Utilisé dans la bible';
COMMENT ON COLUMN dossier.temp5 IS 'Utilisé dans la bible';
COMMENT ON COLUMN dossier.servitude IS 'Servitudes/Contraintes du dossier d''instruction';
COMMENT ON COLUMN dossier.erp IS 'Dossier d''instruction ERP';
COMMENT ON COLUMN dossier.avis_decision IS 'Avis du dossier d''instruction';
COMMENT ON COLUMN dossier.enjeu_erp IS 'Le dossier d''instruction a un enjeu ERP';
COMMENT ON COLUMN dossier.enjeu_urba IS 'Le dossier d''instruction a un enjeu urbain';
COMMENT ON COLUMN dossier.division IS 'Division à laquelle est rattaché le dossier d''instruction';
COMMENT ON COLUMN dossier.autorite_competente IS 'Autorité compétente du dossier d''instruction';
COMMENT ON COLUMN dossier.a_qualifier IS 'Le dossier d''instruction est à qualifier';
COMMENT ON COLUMN dossier.terrain_references_cadastrales IS 'Références cadastrales du dossier d''instruction';
COMMENT ON COLUMN dossier.terrain_adresse_voie_numero IS 'Numéro de la voie de l''adresse du terrain';
COMMENT ON COLUMN dossier.terrain_adresse_voie IS 'Nom de la voie de l''adresse du terrain';
COMMENT ON COLUMN dossier.terrain_adresse_lieu_dit IS 'Lieu dit de l''adresse du terrain';
COMMENT ON COLUMN dossier.terrain_adresse_localite IS 'Ville de l''adresse du terrain';
COMMENT ON COLUMN dossier.terrain_adresse_code_postal IS 'Code postal de l''adresse du terrain';
COMMENT ON COLUMN dossier.terrain_adresse_bp IS 'Boîte postale de l''adresse du terrain';
COMMENT ON COLUMN dossier.terrain_adresse_cedex IS 'Cedex de l''adresse du terrain';
COMMENT ON COLUMN dossier.terrain_superficie IS 'Superficie du terrain concerné par du dossier d''instruction';
COMMENT ON COLUMN dossier.dossier_autorisation IS 'Dossier d''autorisation du dossier d''instruction';
COMMENT ON COLUMN dossier.dossier_instruction_type IS 'Type du dossier d''instruction';
COMMENT ON COLUMN dossier.date_dernier_depot IS 'Date du dernier dépôt d''événement d''instruction';
COMMENT ON COLUMN dossier.version IS 'Numéro de version du dossier d''instruction, permet de connaitre le parcours du dossier d''autorisation';
COMMENT ON COLUMN dossier.incompletude IS 'Le dossier d''instruction est en incomplétude';
COMMENT ON COLUMN dossier.evenement_suivant_tacite IS 'Événement appliqué en cas de tacicité';
COMMENT ON COLUMN dossier.evenement_suivant_tacite_incompletude IS 'Événement appliqué en cas de tacicité sur un dossier en incomplétude';
COMMENT ON COLUMN dossier.etat_pendant_incompletude IS 'État du dossier d''instruction pendant l''incomplétude';
COMMENT ON COLUMN dossier.date_limite_incompletude IS 'Date limite de l''incomplétude';
COMMENT ON COLUMN dossier.delai_incompletude IS 'Délai servant au calcul de la date lmite d''incomplétude';
COMMENT ON COLUMN dossier.dossier_libelle IS 'Numéro du dossier d''instruction utilisé lors de l''affichage';
COMMENT ON COLUMN dossier.numero_versement_archive IS 'Numéro de versement aux archives du dossier d''instruction';
COMMENT ON COLUMN dossier.duree_validite IS 'Durée de validité du dossier d''instruction (récupéré du paramétrage du type détaillé du dossier d''autorisation lors de sa création)';
COMMENT ON COLUMN dossier.geom IS 'Centroĩde du dossier d''instruction sur le SIG';
COMMENT ON COLUMN dossier.geom1 IS '';

-- dossier_autorisation
COMMENT ON TABLE dossier_autorisation IS 'Dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation.dossier_autorisation IS 'Numéro du dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation.dossier_autorisation_type_detaille IS 'Type détaillé du dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation.exercice IS 'Exercice fiscal du dossier d''autorisation (année sur deux chiffres)';
COMMENT ON COLUMN dossier_autorisation.insee IS 'Code INSEE du dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation.terrain_references_cadastrales IS 'Références cadastrales du dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation.terrain_adresse_voie_numero IS 'Numéro de la voie de l''adresse du terrain';
COMMENT ON COLUMN dossier_autorisation.terrain_adresse_voie IS 'Nom de la voie de l''adresse du terrain';
COMMENT ON COLUMN dossier_autorisation.terrain_adresse_lieu_dit IS 'Lieu dit de l''adresse du terrain';
COMMENT ON COLUMN dossier_autorisation.terrain_adresse_localite IS 'Ville de l''adresse du terrain';
COMMENT ON COLUMN dossier_autorisation.terrain_adresse_code_postal IS 'Code postal de l''adresse du terrain';
COMMENT ON COLUMN dossier_autorisation.terrain_adresse_bp IS 'Boîte postale de l''adresse du terrain';
COMMENT ON COLUMN dossier_autorisation.terrain_adresse_cedex IS 'Cedex de l''adresse du terrain';
COMMENT ON COLUMN dossier_autorisation.terrain_superficie IS 'Superficie du terrain concerné par du dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation.arrondissement IS 'Arrondissement du dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation.depot_initial IS 'Dépôt initial du dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation.erp_numero_batiment IS 'Numéro du bâtiment ERP';
COMMENT ON COLUMN dossier_autorisation.erp_ouvert IS 'Bâtiment ERP ouvert';
COMMENT ON COLUMN dossier_autorisation.erp_date_ouverture IS 'Date d''ouverture du bâtiment ERP';
COMMENT ON COLUMN dossier_autorisation.erp_arrete_decision IS 'Un arrêté de décision a été rendu pour le bâtiment ERP';
COMMENT ON COLUMN dossier_autorisation.erp_date_arrete_decision IS 'Date d''arrêté de décision du bâtiment ERP';
COMMENT ON COLUMN dossier_autorisation.numero_version IS 'Numéro de version du dossier d''instruction en cours';
COMMENT ON COLUMN dossier_autorisation.etat_dossier_autorisation IS 'État du dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation.date_depot IS 'Date de dépôt de la demande';
COMMENT ON COLUMN dossier_autorisation.date_decision IS 'Date de décision du dernier dossier d''instruction accepté';
COMMENT ON COLUMN dossier_autorisation.date_validite IS 'Date de validité du dernier dossier d''instruction accepté';
COMMENT ON COLUMN dossier_autorisation.date_chantier IS 'Date de chantier du dernier dossier d''instruction accepté';
COMMENT ON COLUMN dossier_autorisation.date_achevement IS 'Date d''achèvement du chantier du dernier dossier d''instruction accepté';
COMMENT ON COLUMN dossier_autorisation.avis_decision IS 'Avis du dossier d''autorisation récupéré depuis le dernier dossier d''instruction accepté';
COMMENT ON COLUMN dossier_autorisation.etat_dernier_dossier_instruction_accepte IS 'État du dernier dossier d''instruction accepté';
COMMENT ON COLUMN dossier_autorisation.dossier_autorisation_libelle IS 'Numéro du dossier d''autorisation utilisé lors de l''affichage';

-- dossier_autorisation_parcelle
COMMENT ON TABLE dossier_autorisation_parcelle IS 'Parcelles liées aux dossiers d''autorisation (Permet de découper les références cadastrales d''un dossier d''autorisation)';
COMMENT ON COLUMN dossier_autorisation_parcelle.dossier_autorisation_parcelle IS 'Identifiant unique';
COMMENT ON COLUMN dossier_autorisation_parcelle.dossier_autorisation IS 'Dossier d''autorisation de la parcelle';
COMMENT ON COLUMN dossier_autorisation_parcelle.parcelle IS 'Parcelle liée';
COMMENT ON COLUMN dossier_autorisation_parcelle.libelle IS 'Libellé de la parcelle';

-- dossier_autorisation_type
COMMENT ON TABLE dossier_autorisation_type IS 'Type des dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation_type.dossier_autorisation_type IS 'Identifiant unique';
COMMENT ON COLUMN dossier_autorisation_type.code IS 'Code du type de dossier d''autorisation (PC, PD, PA, ...)';
COMMENT ON COLUMN dossier_autorisation_type.libelle IS 'Libellé du type de dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation_type.description IS 'Description du type de dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation_type.confidentiel IS 'Permet de gérer la confidentialité sur ce type de dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation_type.groupe IS 'Permet de définir le service qui doit traiter ce type de dossier d''autorisation';

-- dossier_autorisation_type_detaille
COMMENT ON TABLE dossier_autorisation_type_detaille IS 'Type détaillé des dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation_type_detaille.dossier_autorisation_type_detaille IS 'Identifiant unique';
COMMENT ON COLUMN dossier_autorisation_type_detaille.code IS 'Code du type détaillé de dossier d''autorisation (PCI, PCA, PD, PA, ...)';
COMMENT ON COLUMN dossier_autorisation_type_detaille.libelle IS 'Libellé du type détaillé de dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation_type_detaille.description IS 'Description du type détaillé de dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation_type_detaille.dossier_autorisation_type IS 'Type de dossier d''autorisation du type détaillé de dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation_type_detaille.cerfa IS 'Permet de définir le CERFA qui doit être appliqué à ce type détaillé de dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation_type_detaille.cerfa_lot IS 'Permet de définir le CERFA qui doit être appliqué aux lots de ce type détaillé de dossier d''autorisation';
COMMENT ON COLUMN dossier_autorisation_type_detaille.duree_validite_parametrage IS 'Permet de calculer la date de péremption d''un dossier d''autorisation';

-- dossier_commission
COMMENT ON TABLE dossier_commission IS 'Demande de passage des dossiers d''instruction en commission';
COMMENT ON COLUMN dossier_commission.dossier_commission IS 'Dossier d''instruction de la demande de passage en commission';
COMMENT ON COLUMN dossier_commission.dossier IS 'Identifiant unique';
COMMENT ON COLUMN dossier_commission.commission_type IS 'Type de commission (Commission Technique d''Urbanisme, ...)';
COMMENT ON COLUMN dossier_commission.date_souhaitee IS 'Date de passage en commission souhaitée';
COMMENT ON COLUMN dossier_commission.motivation IS 'Motivation de la demande de passage en commission';
COMMENT ON COLUMN dossier_commission.commission IS 'Identifiant de la commission effective';
COMMENT ON COLUMN dossier_commission.avis IS 'Avis rendu par la commission';
COMMENT ON COLUMN dossier_commission.lu IS 'Permet de définir que l''instructeur a lu l''avis rendu';

-- dossier_instruction_type
COMMENT ON TABLE dossier_instruction_type IS 'Type de dossier d''instruction (initial, modificatif, transfert, ...)';
COMMENT ON COLUMN dossier_instruction_type.dossier_instruction_type IS 'Identifiant unique';
COMMENT ON COLUMN dossier_instruction_type.code IS 'Code du type de dossier d''instruction';
COMMENT ON COLUMN dossier_instruction_type.libelle IS 'Libellé du type de dossier d''instruction';
COMMENT ON COLUMN dossier_instruction_type.description IS 'Description du type de dossier d''instruction';
COMMENT ON COLUMN dossier_instruction_type.dossier_autorisation_type_detaille IS 'Identifiant du type détaillé de dossier d''autorisation';
COMMENT ON COLUMN dossier_instruction_type.suffixe IS 'Permet de définir si un suffixe doit être appliqué au numéro de dossier d''instruction';
COMMENT ON COLUMN dossier_instruction_type.mouvement_sitadel IS 'Mouvement SITADEL associé';

-- dossier_message
COMMENT ON TABLE dossier_message IS 'Messages liés aux dossiers';
COMMENT ON COLUMN dossier_message.dossier_message IS 'Identifiant unique';
COMMENT ON COLUMN dossier_message.dossier IS 'Dossier d''instruction de ce messages';
COMMENT ON COLUMN dossier_message.type IS 'Type de message';
COMMENT ON COLUMN dossier_message.emetteur IS 'Service émetteur du message';
COMMENT ON COLUMN dossier_message.date_emission IS 'Date d''envoi du message';
COMMENT ON COLUMN dossier_message.lu IS 'Permet de définir que l''instructeur a lu le message';
COMMENT ON COLUMN dossier_message.contenu IS 'Contenu du message';

-- dossier_parcelle
COMMENT ON TABLE dossier_parcelle IS 'Parcelles liées aux dossiers d''instruction (Permet de découper les références cadastrales d''un dossier d''instruction)';
COMMENT ON COLUMN dossier_parcelle.dossier_parcelle IS 'Identifiant unique';
COMMENT ON COLUMN dossier_parcelle.dossier IS 'Dossier d''instruction de la pzrcelle';
COMMENT ON COLUMN dossier_parcelle.parcelle IS 'Parcelle liée';
COMMENT ON COLUMN dossier_parcelle.libelle IS 'Libellé de la parcelle';

-- etat
COMMENT ON TABLE etat IS 'États des dossiers d''instruction';
COMMENT ON COLUMN etat.etat IS 'Identifiant unique';
COMMENT ON COLUMN etat.libelle IS 'Libellé de l''état';
COMMENT ON COLUMN etat.statut IS 'Statut de l''état (encours, clôture)';

-- etat_dossier_autorisation
COMMENT ON TABLE etat_dossier_autorisation IS 'États des dossiers d''autorisation';
COMMENT ON COLUMN etat_dossier_autorisation.etat_dossier_autorisation IS 'Identifiant unique';
COMMENT ON COLUMN etat_dossier_autorisation.libelle IS 'Libellé de l''état';

-- evenement
COMMENT ON TABLE evenement IS 'Paramétrage des événements d''instruction';
COMMENT ON COLUMN evenement.evenement IS 'Identifiant unique';
COMMENT ON COLUMN evenement.libelle IS 'Libellé de l''événement';
COMMENT ON COLUMN evenement.action IS 'Action à effectuer après l''ajout d''un événement';
COMMENT ON COLUMN evenement.etat IS 'État à appliquer au dossier d''instruction';
COMMENT ON COLUMN evenement.delai IS 'Permet de calculer la durée de validité de l''instruction';
COMMENT ON COLUMN evenement.accord_tacite IS 'Permet de définir le type d''accord à la fin du délai limite d''instruction';
COMMENT ON COLUMN evenement.delai_notification IS 'Délai de notification avant que l''accord tacite soit appliqué';
COMMENT ON COLUMN evenement.lettretype IS 'Courrier associé à l''événement';
COMMENT ON COLUMN evenement.consultation IS 'Consultation liée à l''événement';
COMMENT ON COLUMN evenement.avis_decision IS 'Avis sur la décision du dossier d''instruction';
COMMENT ON COLUMN evenement.restriction IS 'Formule optionnelle permettant de refuser la validation du formulaire d’ajout d’événement d’instruction si le résultat de la formule est faux';
COMMENT ON COLUMN evenement.type IS 'Permet de qualifier un type d’événement. Les valeurs disponibles sont : “arrêté” pour permettre une gestion propre aux arrêtés, ou “incomplétude” ou “majoration de délais” pour permettre certains calculs dans les tableaux de bord de l’instructeur';
COMMENT ON COLUMN evenement.evenement_retour_ar IS 'Événement déclenché par un retour AR';
COMMENT ON COLUMN evenement.evenement_suivant_tacite IS 'Événement déclenché par une decision tacite';
COMMENT ON COLUMN evenement.evenement_retour_signature IS 'Événement déclenché par un retour de signature';
COMMENT ON COLUMN evenement.autorite_competente IS 'Permet de définir l''autorité compétente du dossier d''instruction';
COMMENT ON COLUMN evenement.retour IS 'Permet de définir s''il s''agit d''un événement de retour d''AR ou de signature';

-- genre
COMMENT ON TABLE genre IS 'Genre des services d''affectation des dossiers d''instruction (URBA, ERP)';
COMMENT ON COLUMN genre.genre IS 'Identifiant unique';
COMMENT ON COLUMN genre.code IS 'Code du genre de services (URBA, ERP)';
COMMENT ON COLUMN genre.libelle IS 'Libellé du service';
COMMENT ON COLUMN genre.description IS 'Description du service';

-- groupe
COMMENT ON TABLE groupe IS 'Services d''affectation des dossiers d''instruction (URBA, ERP)';
COMMENT ON COLUMN groupe.groupe IS 'Identifiant unique';
COMMENT ON COLUMN groupe.code IS 'Code du service (ADS, CTX, CU, ERP, ...)';
COMMENT ON COLUMN groupe.libelle IS 'Libellé du service';
COMMENT ON COLUMN groupe.description IS 'Description du service';
COMMENT ON COLUMN groupe.genre IS 'Genre d''appartenance du service';

-- instructeur
COMMENT ON TABLE instructeur IS 'Instructeur de dossier d''instruction';
COMMENT ON COLUMN instructeur.instructeur IS 'Identifiant unique';
COMMENT ON COLUMN instructeur.nom IS 'Nom de l''instructeur';
COMMENT ON COLUMN instructeur.telephone IS 'Téléphone de l''instructeur';
COMMENT ON COLUMN instructeur.division IS 'Division de l''instructeur';
COMMENT ON COLUMN instructeur.om_utilisateur IS 'Utilisateur correspondant à l''instructeur';
COMMENT ON COLUMN instructeur.om_validite_debut IS 'Date de début de validité de l''instructeur';
COMMENT ON COLUMN instructeur.om_validite_fin IS 'Date de fin de validité de l''instructeur';

-- instruction
COMMENT ON TABLE instruction IS 'Événements d''instruction de chaque dossier d''instruction';
COMMENT ON COLUMN instruction.instruction IS 'Identifiant unique';
COMMENT ON COLUMN instruction.destinataire IS 'Numéro de dossier d''instruction cible';
COMMENT ON COLUMN instruction.date_evenement IS 'Date de l''événement d''instruction';
COMMENT ON COLUMN instruction.evenement IS 'Événement du workflow lié à l''événement d''instruction';
COMMENT ON COLUMN instruction.lettretype IS 'Identifiant du courrier lié';
COMMENT ON COLUMN instruction.complement IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement2 IS 'Second complément d''information';
COMMENT ON COLUMN instruction.dossier IS 'Numéro de dossier d''instruction cible';
COMMENT ON COLUMN instruction.action IS 'Action du workflow appliquée au dossier d''instruction';
COMMENT ON COLUMN instruction.delai IS 'Délai de majoration appliqué au dossier d''instruction';
COMMENT ON COLUMN instruction.etat IS 'État appliqué au dossier d''instruction';
COMMENT ON COLUMN instruction.accord_tacite IS 'Permet de définir si un accord tacite est trouvé à la fin du délai d''instruction';
COMMENT ON COLUMN instruction.delai_notification IS 'Délai de notification de l''instruction';
COMMENT ON COLUMN instruction.archive_delai IS 'Valeur du dossier avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_date_complet IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_date_rejet IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_date_limite IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_date_notification_delai IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_accord_tacite IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_etat IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_date_decision IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_avis IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_date_validite IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_date_achevement IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_date_chantier IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_date_conformite IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.complement3 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement4 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement5 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement6 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement7 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement8 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement9 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement10 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement11 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement12 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement13 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement14 IS 'Complément d''information';
COMMENT ON COLUMN instruction.complement15 IS 'Complément d''information';
COMMENT ON COLUMN instruction.avis_decision IS 'Decision sur le dossier d''instruction après ajout de l''événement';
COMMENT ON COLUMN instruction.date_finalisation_courrier IS 'Date de finalisation du courrier';
COMMENT ON COLUMN instruction.date_envoi_signature IS 'Date d''envoi du courrier pour signature par l''autorité compétente';
COMMENT ON COLUMN instruction.date_retour_signature IS 'Date de retour du courrier après signature';
COMMENT ON COLUMN instruction.date_envoi_rar IS 'Date d''envoi du courrier aux pétitionnaires';
COMMENT ON COLUMN instruction.date_retour_rar IS 'Date de retour d''accusé de réception du courrier';
COMMENT ON COLUMN instruction.date_envoi_controle_legalite IS 'Date d''envoi du courrier au contrôle de légalité';
COMMENT ON COLUMN instruction.date_retour_controle_legalite IS 'Date de retour du courrier après contrôle de légalité';
COMMENT ON COLUMN instruction.signataire_arrete IS 'Identifiant du signataire de l''arrêté';
COMMENT ON COLUMN instruction.numero_arrete IS 'Numéro de l''arrêté dans le référenciel arrêté';
COMMENT ON COLUMN instruction.archive_date_dernier_depot IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_incompletude IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_evenement_suivant_tacite IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_evenement_suivant_tacite_incompletude IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_etat_pendant_incompletude IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_date_limite_incompletude IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.archive_delai_incompletude IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.code_barres IS 'Numéro du code barres correspondant à l''instruction';
COMMENT ON COLUMN instruction.om_fichier_instruction IS 'Identifiant du fichier finalisé dans le système de stockage';
COMMENT ON COLUMN instruction.om_final_instruction IS 'Permet de savoir si le fichier est finalisé';
COMMENT ON COLUMN instruction.document_numerise IS 'Lien vers les documents numérisés';
COMMENT ON COLUMN instruction.archive_autorite_competente IS 'Valeur du dossier d''instruction avant ajout de l''événement d''instruction';
COMMENT ON COLUMN instruction.autorite_competente IS 'Lien vers l''autorité compétente affectée au dossier d''instruction';
COMMENT ON COLUMN instruction.duree_validite_parametrage IS 'Durée de validité du dossier d''autorisation récupérée depuis le paramétrage';
COMMENT ON COLUMN instruction.duree_validite IS 'Durée de validité du dossier d''autorisation depuis le dépôt initial';

-- lien_demande_demandeur
COMMENT ON TABLE lien_demande_demandeur IS 'Liaison entre les demandeurs et les demandes';
COMMENT ON COLUMN lien_demande_demandeur.lien_demande_demandeur IS 'Identifiant unique';
COMMENT ON COLUMN lien_demande_demandeur.petitionnaire_principal IS 'Le demandeur lié à la demande est le pétitionnaire principal';
COMMENT ON COLUMN lien_demande_demandeur.demande IS 'Demande liée au demandeur';
COMMENT ON COLUMN lien_demande_demandeur.demandeur IS 'Demandeur de la demande';

-- lien_demande_type_etat_dossier_autorisation
COMMENT ON TABLE lien_demande_type_etat_dossier_autorisation IS 'Liaison entre les types de demande et les états des dossiers d''autorisation';
COMMENT ON COLUMN lien_demande_type_etat_dossier_autorisation.lien_demande_type_etat_dossier_autorisation IS 'Identifiant unique';
COMMENT ON COLUMN lien_demande_type_etat_dossier_autorisation.demande_type IS 'Type de demande';
COMMENT ON COLUMN lien_demande_type_etat_dossier_autorisation.etat_dossier_autorisation IS 'État du dossier d''autorisation';

-- lien_dossier_autorisation_demandeur
COMMENT ON TABLE lien_dossier_autorisation_demandeur IS 'Liaison entre les demandeurs et les dossiers d''autorisation';
COMMENT ON COLUMN lien_dossier_autorisation_demandeur.lien_dossier_autorisation_demandeur IS 'Identifiant unique';
COMMENT ON COLUMN lien_dossier_autorisation_demandeur.petitionnaire_principal IS 'Le demandeur lié au dossier d''autorisation est le pétitionnaire principal';
COMMENT ON COLUMN lien_dossier_autorisation_demandeur.dossier_autorisation IS 'Dossier d''autorisation lié au demandeur';
COMMENT ON COLUMN lien_dossier_autorisation_demandeur.demandeur IS 'Demandeur lié au dossier d''autorisation';

-- lien_dossier_demandeur
COMMENT ON TABLE lien_dossier_demandeur IS 'Liaison entre les demandeurs et les dossiers d''instruction';
COMMENT ON COLUMN lien_dossier_demandeur.lien_dossier_demandeur IS 'Identifiant unique';
COMMENT ON COLUMN lien_dossier_demandeur.petitionnaire_principal IS 'Le demandeur lié au dossier d''instruction est le pétitionnaire principal';
COMMENT ON COLUMN lien_dossier_demandeur.dossier IS 'Dossier d''instruction lié au demandeur';
COMMENT ON COLUMN lien_dossier_demandeur.demandeur IS 'Demandeur lié au dossier d''instruction';

-- lien_dossier_instruction_type_evenement
COMMENT ON TABLE lien_dossier_instruction_type_evenement IS 'Liaison entre les types de dossier d''instruction et les événements';
COMMENT ON COLUMN lien_dossier_instruction_type_evenement.lien_dossier_instruction_type_evenement IS 'Identifiant unique';
COMMENT ON COLUMN lien_dossier_instruction_type_evenement.dossier_instruction_type IS 'Type du dossier d''instruction lié à l''événement';
COMMENT ON COLUMN lien_dossier_instruction_type_evenement.evenement IS 'Événement lié au dossier d''instruction';

-- lien_lot_demandeur
COMMENT ON TABLE lien_lot_demandeur IS 'Liaison entre les demandeurs et les lots';
COMMENT ON COLUMN lien_lot_demandeur.lien_lot_demandeur IS 'Identifiant unique';
COMMENT ON COLUMN lien_lot_demandeur.lot IS 'Identifiant du lot';
COMMENT ON COLUMN lien_lot_demandeur.demandeur IS 'Demandeur lié au dossier d''instruction';
COMMENT ON COLUMN lien_lot_demandeur.petitionnaire_principal IS 'Le demandeur lié au dossier d''instruction est le pétitionnaire principal';

-- lien_service_om_utilisateur
COMMENT ON TABLE lien_service_om_utilisateur IS 'Affectation de services aux utilisateurs';
COMMENT ON COLUMN lien_service_om_utilisateur.lien_service_om_utilisateur IS 'Identifiant unique';
COMMENT ON COLUMN lien_service_om_utilisateur.om_utilisateur IS 'Identifiant de l''utilisateur';
COMMENT ON COLUMN lien_service_om_utilisateur.service IS 'Identifiant du service';

-- lien_service_service_categorie
COMMENT ON TABLE lien_service_service_categorie IS 'Affectation de services aux utilisateurs';
COMMENT ON COLUMN lien_service_service_categorie.lien_service_service_categorie IS 'Identifiant unique';
COMMENT ON COLUMN lien_service_service_categorie.service_categorie IS 'Identifiant de catégorie de service';
COMMENT ON COLUMN lien_service_service_categorie.service IS 'Identifiant du service';

-- lot
COMMENT ON TABLE lot IS 'Lots de chaque dossier';
COMMENT ON COLUMN lot.lot IS 'Identifiant unique';
COMMENT ON COLUMN lot.libelle IS 'Libellé du lot';
COMMENT ON COLUMN lot.dossier_autorisation IS 'Identifiant du dossier d''autorisation lié au lot';
COMMENT ON COLUMN lot.dossier IS 'Identifiant du dossier d''instruction lié au lot';

-- om_collectivite
COMMENT ON TABLE om_collectivite IS 'Ville utilisant openADS';
COMMENT ON COLUMN om_collectivite.om_collectivite IS 'Identifiant unique';
COMMENT ON COLUMN om_collectivite.libelle IS 'Libellé de la ville';
COMMENT ON COLUMN om_collectivite.niveau IS 'Niveau de la collectivité (1 = mono collectivité, 2 = gère plusieurs autres collectivité)';

-- om_dashboard
COMMENT ON TABLE om_dashboard IS 'Paramétrage du tableau de bord par profil';
COMMENT ON COLUMN om_dashboard.om_dashboard IS 'Identifiant unique';
COMMENT ON COLUMN om_dashboard.om_profil IS 'Profil auquel on affecte le tableau de ville';
COMMENT ON COLUMN om_dashboard.bloc IS 'Bloc de positionnement du widget';
COMMENT ON COLUMN om_dashboard.position IS 'Position du widget dans le bloc';
COMMENT ON COLUMN om_dashboard.om_widget IS 'Identifiant du widget';

-- om_droit
COMMENT ON TABLE om_droit IS 'Paramétrage des droits';
COMMENT ON COLUMN om_droit.om_droit IS 'Identifiant unique';
COMMENT ON COLUMN om_droit.libelle IS 'Libellé du droit';
COMMENT ON COLUMN om_droit.om_profil IS 'Type de profil auquel est lié le droit';

-- om_etat
COMMENT ON TABLE om_etat IS 'Paramétrage des états';
COMMENT ON COLUMN om_etat.om_etat IS 'Identifiant unique';
COMMENT ON COLUMN om_etat.om_collectivite IS 'Identifiant de la collectivité liée à l''état';
COMMENT ON COLUMN om_etat.id IS 'Identifiant de l''état';
COMMENT ON COLUMN om_etat.libelle IS 'Libellé de l''état';
COMMENT ON COLUMN om_etat.actif IS 'Défini si l''état est actif';
COMMENT ON COLUMN om_etat.orientation IS 'Défini l''orientation de la page';
COMMENT ON COLUMN om_etat.format IS 'Défini le format de la page';
COMMENT ON COLUMN om_etat.logo IS 'Défini le logo d''entête';
COMMENT ON COLUMN om_etat.logoleft IS 'Position du logo à gauche';
COMMENT ON COLUMN om_etat.logotop IS 'Position du logo en haut';
COMMENT ON COLUMN om_etat.titre IS 'Bloc de titre';
COMMENT ON COLUMN om_etat.titreleft IS 'Position du titre à gauche';
COMMENT ON COLUMN om_etat.titretop IS 'Position du titre en haut';
COMMENT ON COLUMN om_etat.titrelargeur IS 'Largeur du titre';
COMMENT ON COLUMN om_etat.titrehauteur IS 'Hauteur du titre';
COMMENT ON COLUMN om_etat.titrefont IS 'Police du titre';
COMMENT ON COLUMN om_etat.titreattribut IS 'Attribut du titre (gras, italique, souligné)';
COMMENT ON COLUMN om_etat.titretaille IS 'Taille de la police du titre';
COMMENT ON COLUMN om_etat.titrebordure IS 'Défini si les bordures du titre sont affichées';
COMMENT ON COLUMN om_etat.titrealign IS 'Alignement du texte du titre';
COMMENT ON COLUMN om_etat.corps IS 'Bloc de corps';
COMMENT ON COLUMN om_etat.corpsleft IS 'Position du corps à gauche';
COMMENT ON COLUMN om_etat.corpstop IS 'Position du corps en haut';
COMMENT ON COLUMN om_etat.corpslargeur IS 'Largeur du corps';
COMMENT ON COLUMN om_etat.corpshauteur IS 'Hauteur du corps';
COMMENT ON COLUMN om_etat.corpsfont IS 'Police du texte du corps';
COMMENT ON COLUMN om_etat.corpsattribut IS 'Attribut du corps (gras, italique, souligné)';
COMMENT ON COLUMN om_etat.corpstaille IS 'Taille de la police du corps';
COMMENT ON COLUMN om_etat.corpsbordure IS 'Défini si les bordures du titre sont affichées';
COMMENT ON COLUMN om_etat.corpsalign IS 'Alignement du texte du corps';
COMMENT ON COLUMN om_etat.om_sql IS 'Identifiant de la requête permettant de récupérer les champs de fusion de l''état';
COMMENT ON COLUMN om_etat.sousetat IS 'Identifiant des sous-états de l''état';
COMMENT ON COLUMN om_etat.se_font IS 'Police du texte des sous-états';
COMMENT ON COLUMN om_etat.se_margeleft IS 'Marge à gauche des sous-états';
COMMENT ON COLUMN om_etat.se_margetop IS 'Marge haute des sous-états';
COMMENT ON COLUMN om_etat.se_margeright IS 'Marge de droite des sous-états';
COMMENT ON COLUMN om_etat.se_couleurtexte IS 'Couleur du texte des sous-états';

-- om_lettretype
COMMENT ON TABLE om_lettretype IS 'Paramétrage des lettre-types';
COMMENT ON COLUMN om_lettretype.om_lettretype IS 'Identifiant unique';
COMMENT ON COLUMN om_lettretype.om_collectivite IS 'Identifiant de la collectivité liée à la lettre-type';
COMMENT ON COLUMN om_lettretype.id IS 'Identifiant de la lettre-type';
COMMENT ON COLUMN om_lettretype.libelle IS 'Libellé de la lettre-type';
COMMENT ON COLUMN om_lettretype.actif IS 'Défini si la lettre-type est active';
COMMENT ON COLUMN om_lettretype.orientation IS 'Défini l''orientation de la page';
COMMENT ON COLUMN om_lettretype.format IS 'Défini le format de la page';
COMMENT ON COLUMN om_lettretype.logo IS 'Défini le logo d''entête';
COMMENT ON COLUMN om_lettretype.logoleft IS 'Position du logo à gauche';
COMMENT ON COLUMN om_lettretype.logotop IS 'Position du logo en haut';
COMMENT ON COLUMN om_lettretype.titre IS 'Bloc de titre';
COMMENT ON COLUMN om_lettretype.titreleft IS 'Position du titre à gauche';
COMMENT ON COLUMN om_lettretype.titretop IS 'Position du titre en haut';
COMMENT ON COLUMN om_lettretype.titrelargeur IS 'Largeur du titre';
COMMENT ON COLUMN om_lettretype.titrehauteur IS 'Hauteur du titre';
COMMENT ON COLUMN om_lettretype.titrefont IS 'Police du titre';
COMMENT ON COLUMN om_lettretype.titreattribut IS 'Attribut du titre (gras, italique, souligné)';
COMMENT ON COLUMN om_lettretype.titretaille IS 'Taille de la police du titre';
COMMENT ON COLUMN om_lettretype.titrebordure IS 'Défini si les bordures du titre sont affichées';
COMMENT ON COLUMN om_lettretype.titrealign IS 'Alignement du texte du titre';
COMMENT ON COLUMN om_lettretype.corps IS 'Bloc de corps';
COMMENT ON COLUMN om_lettretype.corpsleft IS 'Position du corps à gauche';
COMMENT ON COLUMN om_lettretype.corpstop IS 'Position du corps en haut';
COMMENT ON COLUMN om_lettretype.corpslargeur IS 'Largeur du corps';
COMMENT ON COLUMN om_lettretype.corpshauteur IS 'Hauteur du corps';
COMMENT ON COLUMN om_lettretype.corpsfont IS 'Police du texte du corps';
COMMENT ON COLUMN om_lettretype.corpsattribut IS 'Attribut du corps (gras, italique, souligné)';
COMMENT ON COLUMN om_lettretype.corpstaille IS 'Taille de la font du corps';
COMMENT ON COLUMN om_lettretype.corpsbordure IS 'Défini si les bordures du titre sont affichées';
COMMENT ON COLUMN om_lettretype.corpsalign IS 'Alignement du texte du corps';
COMMENT ON COLUMN om_lettretype.om_sql IS 'Identifiant de la requête permettant de récupérer les champs de fusion de la lettre-type';

-- om_logo
COMMENT ON TABLE om_logo IS 'Paramétrage des logos de lettre-types et états';
COMMENT ON COLUMN om_logo.om_logo IS 'Identifiant unique';
COMMENT ON COLUMN om_logo.id IS 'Identifiant du logo';
COMMENT ON COLUMN om_logo.libelle IS 'Libellé du logo';
COMMENT ON COLUMN om_logo.description IS 'Description du logo';
COMMENT ON COLUMN om_logo.fichier IS 'Fichier de l''image';
COMMENT ON COLUMN om_logo.resolution IS 'Résolution de l''image';
COMMENT ON COLUMN om_logo.actif IS 'Défini si le logo est utilisable dans les éditions';
COMMENT ON COLUMN om_logo.om_collectivite IS 'Identifiant de la collectivité liée au logo';

-- om_parametre
COMMENT ON TABLE om_parametre IS 'Paramétrage de l''application';
COMMENT ON COLUMN om_parametre.om_parametre IS 'Identifiant unique';
COMMENT ON COLUMN om_parametre.libelle IS 'Libellé du paramètre';
COMMENT ON COLUMN om_parametre.valeur IS 'Valeur du paramètre';
COMMENT ON COLUMN om_parametre.om_collectivite IS 'Collectivité utilisant le paramètre';

-- om_profil
COMMENT ON TABLE om_profil IS 'Type de profil des utilisateurs';
COMMENT ON COLUMN om_profil.om_profil IS  'Identifiant unique';
COMMENT ON COLUMN om_profil.libelle IS 'Libellé du profil';
COMMENT ON COLUMN om_profil.hierarchie IS 'Permet de rendre hiérarchique certains profils';
COMMENT ON COLUMN om_profil.om_validite_debut IS 'Date de début de validité du profil';
COMMENT ON COLUMN om_profil.om_validite_fin IS 'Date de fin de validité du profil';

-- om_requete
COMMENT ON TABLE om_requete IS 'Paramétrage des requêtes utilisées par les lettre-types et les états';
COMMENT ON COLUMN om_requete.om_requete IS 'Identifiant unique';
COMMENT ON COLUMN om_requete.code IS 'Code de la requête';
COMMENT ON COLUMN om_requete.libelle IS 'Libellé de la requête';
COMMENT ON COLUMN om_requete.description IS 'Description de la requête';
COMMENT ON COLUMN om_requete.requete IS 'Requête SQL';
COMMENT ON COLUMN om_requete.merge_fields IS 'Champs de fusion';

-- om_sig_map
COMMENT ON TABLE om_sig_map IS 'Table utile au SIG interne';

-- om_sig_map_comp
COMMENT ON TABLE om_sig_map_comp IS 'Table utile au SIG interne';

-- om_sig_map_wms
COMMENT ON TABLE om_sig_map_wms IS 'Table utile au SIG interne';

-- om_sig_wms
COMMENT ON TABLE om_sig_wms IS 'Table utile au SIG interne';

-- om_sousetat
COMMENT ON TABLE om_sousetat IS 'Types de profil des utilisateurs';
COMMENT ON COLUMN om_sousetat.om_sousetat IS 'Identifiant unique';
COMMENT ON COLUMN om_sousetat.om_collectivite IS 'Identifiant de la collectivité liée à la lettre-type';
COMMENT ON COLUMN om_sousetat.id IS 'Identifiant du sous-état';
COMMENT ON COLUMN om_sousetat.libelle IS 'Libellé du sous-état';
COMMENT ON COLUMN om_sousetat.actif IS 'Défini si le sous-état est utilisable';
COMMENT ON COLUMN om_sousetat.titre IS 'Titre affiché dans le sous-état';
COMMENT ON COLUMN om_sousetat.titrehauteur IS 'Hauteur du titre en cm';
COMMENT ON COLUMN om_sousetat.titrefont IS 'Font du texte du titre';
COMMENT ON COLUMN om_sousetat.titreattribut IS 'Attribut du texte du titre (italique, souligné, gras)';
COMMENT ON COLUMN om_sousetat.titretaille IS 'Taille du texte du titre';
COMMENT ON COLUMN om_sousetat.titrebordure IS 'Affiche ou non les bordures sur le titre';
COMMENT ON COLUMN om_sousetat.titrealign IS 'Alignement du texte du titre';
COMMENT ON COLUMN om_sousetat.titrefond IS 'Affiche ou non une couleur de fond au titre';
COMMENT ON COLUMN om_sousetat.titrefondcouleur IS 'Couleur de fond du titre';
COMMENT ON COLUMN om_sousetat.titretextecouleur IS 'Couleur du texte du titre';
COMMENT ON COLUMN om_sousetat.intervalle_debut IS 'Début du titre';
COMMENT ON COLUMN om_sousetat.intervalle_fin IS 'Fin du titre';
COMMENT ON COLUMN om_sousetat.entete_flag IS 'Défini si le tableau contient une ligne d''entête';
COMMENT ON COLUMN om_sousetat.entete_fond IS 'Défini si l''entête du tableau à une couleur de fond';
COMMENT ON COLUMN om_sousetat.entete_orientation IS 'Orientation du texte dans les entêtes';
COMMENT ON COLUMN om_sousetat.entete_hauteur IS 'Hauteur de la ligne d''entête';
COMMENT ON COLUMN om_sousetat.entetecolone_bordure IS 'Affichage ou non de chaque bordure des cellules d''entête';
COMMENT ON COLUMN om_sousetat.entetecolone_align IS 'Alignement du texte dans chaque cellule d''entête';
COMMENT ON COLUMN om_sousetat.entete_fondcouleur IS 'Couleur de fond de l''entête';
COMMENT ON COLUMN om_sousetat.entete_textecouleur IS 'Couleur du texte de l''entête';
COMMENT ON COLUMN om_sousetat.tableau_largeur IS 'Largeur du tableau';
COMMENT ON COLUMN om_sousetat.tableau_bordure IS 'Défini si on affiche les bordures du tableau';
COMMENT ON COLUMN om_sousetat.tableau_fontaille IS 'Taille du texte du tableau';
COMMENT ON COLUMN om_sousetat.bordure_couleur IS 'Couleur des bordures du tableau';
COMMENT ON COLUMN om_sousetat.se_fond1 IS 'Couleur de fond du tableau';
COMMENT ON COLUMN om_sousetat.se_fond2 IS 'Seconde couleur de fond du tableau';
COMMENT ON COLUMN om_sousetat.cellule_fond IS 'Défini si les cellules du tableau ont une couleur de fond';
COMMENT ON COLUMN om_sousetat.cellule_hauteur IS 'Hauteur des cellules';
COMMENT ON COLUMN om_sousetat.cellule_largeur IS 'Largeur des cellules';
COMMENT ON COLUMN om_sousetat.cellule_bordure_un IS 'Bordure des cellules';
COMMENT ON COLUMN om_sousetat.cellule_bordure IS 'Bordure des cellules';
COMMENT ON COLUMN om_sousetat.cellule_align IS 'Alignement du texte dans chaque cellule';
COMMENT ON COLUMN om_sousetat.cellule_fond_total IS 'Défini si la ligne des totaux a une couleur de fond';
COMMENT ON COLUMN om_sousetat.cellule_fontaille_total IS 'Taille du texte de la ligne des totaux';
COMMENT ON COLUMN om_sousetat.cellule_hauteur_total IS 'Hauteur de la ligne des totaux';
COMMENT ON COLUMN om_sousetat.cellule_fondcouleur_total IS 'Couleur de fond de la ligne des totaux';
COMMENT ON COLUMN om_sousetat.cellule_bordure_total IS 'Défini les bordures de la ligne des totaux';
COMMENT ON COLUMN om_sousetat.cellule_align_total IS 'Alignement du texte de la ligne des totaux';
COMMENT ON COLUMN om_sousetat.cellule_fond_moyenne IS 'Défini si la ligne des moyennes contient une couleur de fond';
COMMENT ON COLUMN om_sousetat.cellule_fontaille_moyenne IS 'Taille du texte de la ligne des moyennes';
COMMENT ON COLUMN om_sousetat.cellule_hauteur_moyenne IS 'Hauteur de la ligne des moyennes';
COMMENT ON COLUMN om_sousetat.cellule_fondcouleur_moyenne IS 'Couleur de fond de la ligne des moyennes';
COMMENT ON COLUMN om_sousetat.cellule_bordure_moyenne IS 'Défini les bordures de la ligne des moyennes';
COMMENT ON COLUMN om_sousetat.cellule_align_moyenne IS 'Alignement du texte de la ligne des moyennes';
COMMENT ON COLUMN om_sousetat.cellule_fond_nbr IS 'Defini si une couleur de fond du compte de ligne est affichée';
COMMENT ON COLUMN om_sousetat.cellule_fontaille_nbr IS 'Taille du texte du compte de lignes';
COMMENT ON COLUMN om_sousetat.cellule_hauteur_nbr IS 'Hauteur du compte de nombre de lignes';
COMMENT ON COLUMN om_sousetat.cellule_fondcouleur_nbr IS 'Couleur de fond du compte de nombre de lignes';
COMMENT ON COLUMN om_sousetat.cellule_bordure_nbr IS 'Défini les bordures du compte de lignes';
COMMENT ON COLUMN om_sousetat.cellule_align_nbr IS 'Alignement du texte du compte de lignes';
COMMENT ON COLUMN om_sousetat.cellule_numerique IS 'Formatage du texte de chaque cellule du tableau';
COMMENT ON COLUMN om_sousetat.cellule_total IS 'Formatage du texte de chaque cellule des totaux';
COMMENT ON COLUMN om_sousetat.cellule_moyenne IS 'Formatage du texte de chaque cellule des moyennes';
COMMENT ON COLUMN om_sousetat.cellule_compteur IS 'Formatage du texte de chaque cellule du compteur';
COMMENT ON COLUMN om_sousetat.om_sql IS 'Requête SQL permettant de récupérer les données à afficher';

-- om_utilisateur
COMMENT ON TABLE om_utilisateur IS 'Utilisateurs';
COMMENT ON COLUMN om_utilisateur.om_utilisateur IS 'Identifiant unique';
COMMENT ON COLUMN om_utilisateur.nom IS 'Nom de l''utilisateur';
COMMENT ON COLUMN om_utilisateur.email IS 'Mail de l''utilisateur';
COMMENT ON COLUMN om_utilisateur.login IS 'Identifiant de l''utilisateur';
COMMENT ON COLUMN om_utilisateur.pwd IS 'Mot de passe de l''utilisateur';
COMMENT ON COLUMN om_utilisateur.om_collectivite IS 'Collectivité de l''utilisateur';
COMMENT ON COLUMN om_utilisateur.om_type IS 'Type de l''utilisateur (LDAP = récupéré depuis un LDAP, DB = crée depuis l''application)';
COMMENT ON COLUMN om_utilisateur.om_profil IS 'Profil de l''utilisateur';

-- om_version
COMMENT ON TABLE om_version IS 'Version de l''application';
COMMENT ON COLUMN om_version.om_version IS 'Version de l''application';

-- om_widget
COMMENT ON TABLE om_widget IS 'Widgets pour les tableaux de bord des profils';
COMMENT ON COLUMN om_widget.om_widget IS 'Identifiant unique';
COMMENT ON COLUMN om_widget.libelle IS 'Libellé du widget';
COMMENT ON COLUMN om_widget.lien IS 'Lien qui pointe vers le widget (peut être vers une URL ou un fichier)';
COMMENT ON COLUMN om_widget.texte IS 'Texte affiché dans le widget';
COMMENT ON COLUMN om_widget.type IS 'Type du widget (''web'' si pointe vers une URL ou ''file'' si pointe vers un fichier)';

-- parcelle
COMMENT ON TABLE parcelle IS 'Liste des parcelles (portion de terrain)';
COMMENT ON COLUMN parcelle.parcelle IS 'Numéro de la parcelle';
COMMENT ON COLUMN parcelle.debut IS 'Début de la parcelle';
COMMENT ON COLUMN parcelle.rivoli IS 'Code rivoli (code par commune pour les voies, lieux-dits et les ensembles immobiliers)';
COMMENT ON COLUMN parcelle.proprietaire IS 'Proprietaire de la parcelle';
COMMENT ON COLUMN parcelle.pos IS 'Plan d''occupation des sols';
COMMENT ON COLUMN parcelle.surface IS 'Surface de la parcelle';
COMMENT ON COLUMN parcelle.section IS 'Section de la parcelle';
COMMENT ON COLUMN parcelle.commune IS 'Commune de la parcelle';
COMMENT ON COLUMN parcelle.geom IS 'Centroîde de la parcelle';

-- parcelle_lot
COMMENT ON TABLE parcelle_lot IS 'Parcelle affectée aux lotissements';
COMMENT ON COLUMN parcelle_lot.parcelle_lot IS 'Identifiant unique';
COMMENT ON COLUMN parcelle_lot.lotissement IS 'Description du lotissement';
COMMENT ON COLUMN parcelle_lot.numero IS 'Numéro du lotissement';
COMMENT ON COLUMN parcelle_lot.surface IS 'Surface du lotissement';
COMMENT ON COLUMN parcelle_lot.geom IS 'Positionnement géographique de la parcelle';

-- pos
COMMENT ON TABLE pos IS 'Plan d''occupation des sols';
COMMENT ON COLUMN pos.pos IS 'Identifiant unique';
COMMENT ON COLUMN pos.libelle IS 'Libellé du POS';
COMMENT ON COLUMN pos.geom IS 'Position géographique du POS';

-- proprietaire
COMMENT ON TABLE proprietaire IS 'Propriétaire de parcelle';
COMMENT ON COLUMN proprietaire.proprietaire IS 'Identifiant unique';
COMMENT ON COLUMN proprietaire.nom IS 'Nom du propriétaire';
COMMENT ON COLUMN proprietaire.prenom IS 'Prénom du propriétaire';
COMMENT ON COLUMN proprietaire.adresse1 IS 'Adresse du propriétaire';
COMMENT ON COLUMN proprietaire.adresse2 IS 'Complément d''adresse du propriétaire';
COMMENT ON COLUMN proprietaire.cp IS 'Code postal de l''adresse du propriétaire';
COMMENT ON COLUMN proprietaire.ville IS 'Ville de l''adresse du propriétaire';
COMMENT ON COLUMN proprietaire.civilite IS 'Civilité du propriétaire';

-- quartier
COMMENT ON TABLE quartier IS 'Liste des quartiers';
COMMENT ON COLUMN quartier.quartier IS 'Identifiant unique';
COMMENT ON COLUMN quartier.arrondissement IS 'Arrondissement du quartier';
COMMENT ON COLUMN quartier.code_impots IS 'Code impôt du quartier';
COMMENT ON COLUMN quartier.libelle IS 'Libellé du quartier';

-- rapport_instruction
COMMENT ON TABLE rapport_instruction IS 'Rapport d''instruction d''un dossier d''instruction';
COMMENT ON COLUMN rapport_instruction.rapport_instruction IS 'Identifiant unique';
COMMENT ON COLUMN rapport_instruction.dossier_instruction IS 'Dossier d''instruction sur lequel il y a le rapport d''instruction';
COMMENT ON COLUMN rapport_instruction.analyse_reglementaire IS 'Analyse réglementaire de l''instructeur sur le dossier d''instruction';
COMMENT ON COLUMN rapport_instruction.description_projet IS 'Description du projet du dossier d''instruction';
COMMENT ON COLUMN rapport_instruction.proposition_decision IS 'Décision proposée par l''instructeur pour le dossier d''instruction';
COMMENT ON COLUMN rapport_instruction.om_fichier_rapport_instruction IS 'Identifiant du fichier finalisé dans le système de stockage';
COMMENT ON COLUMN rapport_instruction.om_final_rapport_instruction IS 'Permet de savoir si le fichier est finalisé';

-- regle
COMMENT ON TABLE regle IS 'Règles de contrôle de champ';
COMMENT ON COLUMN regle.regle IS 'Identifiant unique';
COMMENT ON COLUMN regle.sens IS 'Sens de la règle (''plus'' ou ''moins'')';
COMMENT ON COLUMN regle.ordre IS 'Ordre de la règle';
COMMENT ON COLUMN regle.controle IS 'Contrôle effectué par la règle';
COMMENT ON COLUMN regle.id IS 'Identifiant unique';
COMMENT ON COLUMN regle.champ IS 'Champ sur lequel la règle doit être appliquée';
COMMENT ON COLUMN regle.operateur IS 'Opérateur utilisé par la règle (>, <, >=, <=)';
COMMENT ON COLUMN regle.valeur IS 'Valeur à comparer';
COMMENT ON COLUMN regle.message IS 'Message explicatif de la règle';

-- rivoli
COMMENT ON TABLE rivoli IS 'Code rivoli (code par commune pour les voies, lieux-dits et les ensembles immobiliers)';
COMMENT ON COLUMN rivoli.rivoli IS 'Identifiant unique';
COMMENT ON COLUMN rivoli.libelle IS 'Code rivoli';

-- service
COMMENT ON TABLE service IS 'Services consultés lors de l''instruction d''un dossier';
COMMENT ON COLUMN service.service IS 'Identifiant unique';
COMMENT ON COLUMN service.abrege IS 'Code du service';
COMMENT ON COLUMN service.libelle IS 'Libellé du service';
COMMENT ON COLUMN service.adresse IS 'Adresse du service';
COMMENT ON COLUMN service.adresse2 IS 'Complément d''adresse du service';
COMMENT ON COLUMN service.cp IS 'Code postal de l''adresse du service';
COMMENT ON COLUMN service.ville IS 'Ville de l''adresse du service';
COMMENT ON COLUMN service.email IS 'Email du service';
COMMENT ON COLUMN service.delai IS 'Delai de consultation au service';
COMMENT ON COLUMN service.consultation_papier IS 'La consultation nécessite l''envoi d''un courrier';
COMMENT ON COLUMN service.notification_email IS 'La consultation nécessite l''envoi d''un email';
COMMENT ON COLUMN service.om_validite_debut IS 'Date de début de validité du service';
COMMENT ON COLUMN service.om_validite_fin IS 'Date de fin de validité du service';
COMMENT ON COLUMN service.type_consultation IS 'Type de consultation (pour conformité, avis attendu, pour information)';

-- service_categorie
COMMENT ON TABLE service_categorie IS 'Catégorie des services';
COMMENT ON COLUMN service_categorie.service_categorie IS 'Identifiant unique';
COMMENT ON COLUMN service_categorie.libelle IS 'Libellé de la catégorie du service';

-- servitude_ligne
COMMENT ON TABLE servitude_ligne IS 'Table utile au SIG interne';

-- servitude_point
COMMENT ON TABLE servitude_point IS 'Table utile au SIG interne';

-- servitude_surfacique
COMMENT ON TABLE servitude_surfacique IS 'Table utile au SIG interne';

-- sig_elyx
COMMENT ON TABLE sig_elyx IS 'Table spécifique au SIG Elyx';
COMMENT ON COLUMN sig_elyx.sig_elyx IS 'Identifiant unique';
COMMENT ON COLUMN sig_elyx.dossier IS 'Numéro de dossier d''instruction';
COMMENT ON COLUMN sig_elyx.date_verif_parcelle IS 'Dernière date à laquelle les parcelles ont été vérifiées dans le SIG Elyx';
COMMENT ON COLUMN sig_elyx.etat_verif_parcelle IS 'Dernier état après vérification des parcelles dans le SIG Elyx (''true'' s''il n''y a pas eu d''erreur, ''false'' sinon)';
COMMENT ON COLUMN sig_elyx.message_verif_parcelle IS 'Dernier message affiché à l''utilisateur après vérification des parcelles dans le SIG Elyx';
COMMENT ON COLUMN sig_elyx.date_calcul_emprise IS 'Dernière date à laquelle l''emprise a été calculé dans le SIG Elyx';
COMMENT ON COLUMN sig_elyx.etat_calcul_emprise IS 'Dernier état après le calcul de l''emprise dans le SIG Elyx (''true'' s''il n''y a pas eu d''erreur, ''false'' sinon)';
COMMENT ON COLUMN sig_elyx.message_calcul_emprise IS 'Dernier message affiché à l''utilisateur après le calcul de l''emprise dans le SIG Elyx';
COMMENT ON COLUMN sig_elyx.date_dessin_emprise IS 'Dernière date à laquelle l''emprise a été dessiné dans le SIG Elyx';
COMMENT ON COLUMN sig_elyx.etat_dessin_emprise IS 'Dernier état après le dessin de l''emprise dans le SIG Elyx (''true'' s''il n''y a pas eu d''erreur, ''false'' sinon)';
COMMENT ON COLUMN sig_elyx.message_dessin_emprise IS 'Dernier message affiché à l''utilisateur après le dessin de l''emprise dans le SIG Elyx';
COMMENT ON COLUMN sig_elyx.date_calcul_centroide IS 'Dernière date à laquelle le centroïde a été calculé dans le SIG Elyx';
COMMENT ON COLUMN sig_elyx.etat_calcul_centroide IS 'Dernier état après le calcul du centroïde dans le SIG Elyx (''true'' s''il n''y a pas eu d''erreur, ''false'' sinon)';
COMMENT ON COLUMN sig_elyx.message_calcul_centroide IS 'Dernier message affiché à l''utilisateur après le calcul du centroïde dans le SIG Elyx';
COMMENT ON COLUMN sig_elyx.date_recup_contrainte IS 'Dernière date à laquelle les contraintes ont été récupérées depuis le SIG Elyx';
COMMENT ON COLUMN sig_elyx.etat_recup_contrainte IS 'Dernier état après récupération des contraintes depuis le SIG Elyx (''true'' s''il n''y a pas eu d''erreur, ''false'' sinon)';
COMMENT ON COLUMN sig_elyx.message_recup_contrainte IS 'Dernier message affiché à l''utilisateur après la récupération des contraintes depuis le SIG Elyx';
COMMENT ON COLUMN sig_elyx.terrain_references_cadastrales_archive IS 'Références cadastrales récupérées depuis le dossier d''instruction. Mise à jour seulement lorsque les références cadastrales du dossier d''instruction ont été vérifiées';

-- signataire_arrete
COMMENT ON TABLE signataire_arrete IS 'Personne accréditée à signer les arrêtés';
COMMENT ON COLUMN signataire_arrete.signataire_arrete IS 'Identifiant unique';
COMMENT ON COLUMN signataire_arrete.civilite IS 'Civilité du signataire';
COMMENT ON COLUMN signataire_arrete.nom IS 'Nom du signataire';
COMMENT ON COLUMN signataire_arrete.prenom IS 'Prénom du signataire';
COMMENT ON COLUMN signataire_arrete.qualite IS 'Qualité du signataire (Maire, Prefet, ...)';
COMMENT ON COLUMN signataire_arrete.signature IS 'Signature du signataire';
COMMENT ON COLUMN signataire_arrete.defaut IS 'Signataire par défaut';
COMMENT ON COLUMN signataire_arrete.om_validite_debut IS 'Date de début de validité du signataire';
COMMENT ON COLUMN signataire_arrete.om_validite_fin IS 'Date de fin de validité du signataire';

-- transition
COMMENT ON TABLE transition IS 'Table de liaisons entre les états et les événements';
COMMENT ON COLUMN transition.transition IS 'Identifiant unique';
COMMENT ON COLUMN transition.etat IS 'États des dossiers d''instruction';
COMMENT ON COLUMN transition.evenement IS 'Paramétrage des événements d''instruction';

--
-- Ajout du code sitadel de l'autorité compétente
--

ALTER TABLE autorite_competente ADD COLUMN autorite_competente_sitadel integer;
COMMENT ON COLUMN autorite_competente.autorite_competente_sitadel IS 'Code sitadel de l''autorité compétente';

UPDATE autorite_competente SET autorite_competente_sitadel=1;
UPDATE autorite_competente SET autorite_competente_sitadel=2 WHERE code = 'ETAT';

ALTER TABLE autorite_competente ALTER COLUMN autorite_competente_sitadel SET NOT NULL;

--
-- Ajout de sous-états pour les export DTTE
--
INSERT INTO om_sousetat VALUES (nextval('om_sousetat_seq'), 1, 'dossier_premier_depot_dttm', 'Dossier 1er dépôt DTTM', 't', 'Répertoire des premiers dossiers DDE', '8', 'helvetica', 'B', '12', '0', 'C', '0', '255-255-255', '0-0-0', '5', '0', '1', '1', '0|0|0|0', '20', '0|0|0|0', 'C|C|C|C', '114-195-243', '0-0-0', '297', '0', '11', '0-0-0', '243-246-246', '255-255-255', '1', '10', '46|22|100|100', 'TLB|TLB|TLB|TLBR', 'TLB|LTB|LTB|LTBR', 'L|L|L|L', '1', '0', '15', '196-213-213', 'L|L|L|L', 'L|L|C', '1', '0', '5', '212-219-219', 'L|L|L|L', 'L|L|L|C', '1', '0', '15', '255-255-255', 'L|L|L|L', 'L|R|C', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'Récup dans le reqmo');
INSERT INTO om_sousetat VALUES (nextval('om_sousetat_seq'), 1, 'dossier_depots_division', 'Répertoire des dépôts et des dépôts de pièces du jour pour la division', true, 'Répertoire des dépôts et des dépôts de pièces du jour pour la division', 8, 'helvetica', 'B', 12, '0', 'C', '0', '255-255-255', '0-0-0', 5, 0, '1', '1', '0|90|0|0|0', 20, '0|0|0|0|0', 'C|C|C|C|C', '114-195-243', '0-0-0', 297, '0', 11, '0-0-0', '243-246-246', '255-255-255', '1', 10, '46|10|24|98|98', 'TLB|TLB|TLB|TLB|TLBR', 'TLB|LTB|LTB|LTB|LTBR', 'L|L|L|L|L', '1', 0, 15, '196-213-213', 'L|L|L|L|L', 'L|L|C', '1', 0, 5, '196-213-213', 'L|L|L|L|L', 'L|L|L|L|C', '1', 0, 15, '255-255-255', 'L|L|L|L|L', 'L|R|C', '999|999|999|999|999', '0|0|0|0|0', '0|0|0|0|0', '0|0|0|0|0', 'Récup dans le reqmo');
INSERT INTO om_sousetat VALUES (nextval('om_sousetat_seq'), 1, 'dossier_transmission_dttm_signature_prefet', 'Bordereau transmission DTTM pour signature préfet', 't', 'Bordereau transmission DTTM pour signature préfet', '8', 'helvetica', 'B', '12', '0', 'C', '0', '255-255-255', '0-0-0', '5', '0', '1', '1', '0|0|0|0', '20', '0|0|0|0', 'C|C|C|C', '114-195-243', '0-0-0', '297', '0', '11', '0-0-0', '243-246-246', '255-255-255', '1', '10', '46|22|100|100', 'TLB|TLB|TLB|TLBR', 'TLB|LTB|LTB|LTBR', 'L|L|L|L', '1', '0', '15', '196-213-213', 'L|L|L|L', 'L|L|C', '1', '0', '5', '212-219-219', 'L|L|L|L', 'L|L|L|C', '1', '0', '15', '255-255-255', 'L|L|L|L', 'L|R|C', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'Récup dans le reqmo');
