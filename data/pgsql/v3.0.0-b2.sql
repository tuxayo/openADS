--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.3.0-b2
--
-- @package openfoncier
-- @version SVN : $Id: v3.0.0-b2.sql 1804 2013-04-30 14:45:17Z fmichon $
--------------------------------------------------------------------------------

CREATE TABLE om_version (
  om_version character varying(100) NOT NULL
) ;
INSERT INTO om_version (om_version) VALUES ('3.0.0-b2');

INSERT INTO om_profil VALUES (99, 'NON UTILISE');
INSERT INTO om_droit VALUES ('directory', 99);
