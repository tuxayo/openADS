--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.15.0-rc4
--
-- XXX Ce fichier doit être renommé en v3.15.0-rc4.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.15.0-rc4.sql 4665 2015-04-28 19:44:32Z tbenita $
--------------------------------------------------------------------------------

--
-- Ajout des droits de consultation sur le fichier
--
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'consultation_om_fichier_consultation_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_om_fichier_consultation_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
    );

INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'consultation_om_fichier_consultation_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_om_fichier_consultation_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
    );

--
-- START / Ajout des droits pour le suivi
--

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_suivi_bordereaux_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_bordereaux_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_suivi_envoi_lettre_rar_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_envoi_lettre_rar_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_suivi_mise_a_jour_des_dates_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_mise_a_jour_des_dates_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_suivi_bordereaux_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_bordereaux_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_suivi_envoi_lettre_rar_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_envoi_lettre_rar_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_suivi_mise_a_jour_des_dates_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_mise_a_jour_des_dates_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );


-- Supprimer les anciens droits
DELETE FROM om_droit WHERE libelle = 'suivi_bordereaux';
DELETE FROM om_droit WHERE libelle = 'suivi_envoi_lettre_rar';
DELETE FROM om_droit WHERE libelle = 'suivi_mise_a_jour_des_dates';

--
-- END / Ajout des droits pour le suivi
--

--
-- Ajout du paramètre 'option_instructeur_division_numero_dossier' par défaut à false.
-- Permet de mettre la division de l'instructeur dans le numéro du dossier.
--
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'), 'option_instructeur_division_numero_dossier', 'false', 1
FROM om_parametre
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'option_instructeur_division_numero_dossier'
    )
LIMIT 1;