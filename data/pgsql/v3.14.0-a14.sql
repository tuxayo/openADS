--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a14
--
-- XXX Ce fichier doit être renommé en v3.14.0-a14.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.14.0-a14.sql 3130 2014-08-11 15:31:19Z vpihour $
--------------------------------------------------------------------------------
--Ajout de droits au profil visudadi
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'instruction_om_fichier_instruction_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_om_fichier_instruction_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
    );

--Ajout d'un nouvel état
INSERT INTO etat (etat, libelle, statut)
  SELECT 'accepte_tacite','dossier accepté tacitement', 'cloture'
  WHERE
    NOT EXISTS (
        SELECT etat FROM etat WHERE UPPER(libelle) = UPPER('accepté tacite') OR 
            etat = 'accepte_tacite' OR etat = 'accepter_tacite'
    );

--Assignation de valeur correcte aux paramètres 'option_'
--option_sig
UPDATE om_parametre 
SET valeur = 'aucun' 
WHERE om_parametre = (SELECT om_parametre 
FROM om_parametre 
WHERE libelle = 'option_sig' and 
valeur != 'sig_externe' AND valeur != 'sig_interne' AND valeur != 'aucun');

INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
  SELECT nextval('om_parametre_seq'),'option_sig', 'aucun', 1
  WHERE
    NOT EXISTS (
        SELECT om_parametre 
        FROM om_parametre 
        WHERE libelle = 'option_sig' and 
        (valeur = 'sig_externe' OR valeur = 'sig_interne' OR valeur = 'aucun')
    );

--option_referentiel_arrete
UPDATE om_parametre 
SET valeur = 'false' 
WHERE om_parametre = (SELECT om_parametre 
FROM om_parametre 
WHERE libelle = 'option_referentiel_arrete' and 
valeur != 'true' AND valeur != 'false');

INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
  SELECT nextval('om_parametre_seq'),'option_referentiel_arrete', 'false', 1
  WHERE
    NOT EXISTS (
        SELECT om_parametre 
        FROM om_parametre 
        WHERE libelle = 'option_referentiel_arrete' and 
        (valeur = 'true' OR valeur = 'false')
    );

--option_referentiel_arrete
UPDATE om_parametre 
SET valeur = 'false' 
WHERE om_parametre = (SELECT om_parametre 
FROM om_parametre 
WHERE libelle = 'option_referentiel_arrete' and 
valeur != 'true' AND valeur != 'false');

INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
  SELECT nextval('om_parametre_seq'),'option_referentiel_arrete', 'false', 1
  WHERE
    NOT EXISTS (
        SELECT om_parametre 
        FROM om_parametre 
        WHERE libelle = 'option_referentiel_arrete' and 
        (valeur = 'true' OR valeur = 'false')
    );

--option_localisation
UPDATE om_parametre 
SET valeur = 'false' 
WHERE om_parametre = (SELECT om_parametre 
FROM om_parametre 
WHERE libelle = 'option_localisation' and 
valeur != 'true' AND valeur != 'false');

INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
  SELECT nextval('om_parametre_seq'),'option_localisation', 'false', 1
  WHERE
    NOT EXISTS (
        SELECT om_parametre 
        FROM om_parametre 
        WHERE libelle = 'option_localisation' and 
        (valeur = 'true' OR valeur = 'false')
    );

--option_contrainte_di
UPDATE om_parametre 
SET valeur = 'aucun' 
WHERE om_parametre = (SELECT om_parametre 
FROM om_parametre 
WHERE libelle = 'option_contrainte_di' and 
valeur NOT LIKE '%liste_groupe%' AND valeur NOT LIKE '%liste_ssgroupe%' AND 
valeur NOT LIKE '%service_consulte%' AND valeur != 'aucun');

INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
  SELECT nextval('om_parametre_seq'),'option_contrainte_di', 'aucun', 1
  WHERE
    NOT EXISTS (
        SELECT om_parametre 
        FROM om_parametre 
        WHERE libelle = 'option_contrainte_di' and 
        (valeur LIKE '%liste_groupe%' OR valeur LIKE '%liste_ssgroupe%' OR 
        valeur LIKE '%service_consulte%' OR valeur = 'aucun')
    );

--option_afficher_division
UPDATE om_parametre 
SET valeur = 'false' 
WHERE om_parametre = (SELECT om_parametre 
FROM om_parametre 
WHERE libelle = 'option_afficher_division' and 
valeur != 'true' AND valeur != 'false');

INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
  SELECT nextval('om_parametre_seq'),'option_afficher_division', 'false', 1
  WHERE
    NOT EXISTS (
        SELECT om_parametre 
        FROM om_parametre 
        WHERE libelle = 'option_afficher_division' and 
        (valeur = 'true' OR valeur = 'false')
    );