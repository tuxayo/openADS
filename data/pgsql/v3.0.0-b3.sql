--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.3.0-b3
--
-- @package openfoncier
-- @version SVN : $Id: v3.0.0-b3.sql 1804 2013-04-30 14:45:17Z fmichon $
--------------------------------------------------------------------------------

-- schema
-- SET search_path = openfoncier, pg_catalog;

UPDATE om_version SET om_version = '3.0.0-b3';
