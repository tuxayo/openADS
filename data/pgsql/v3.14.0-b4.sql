--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-b4
--
-- XXX Ce fichier doit être renommé en v3.14.0-b4.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.14.0-b4.sql 3220 2014-12-17 19:41:13Z vpihour $
--------------------------------------------------------------------------------

----
-- BEGIN  / OM REQUETE
--
-- Ajout de champs pour la gestion objet,
-- dont un not null d'où gestion de l'existant.
-- Puis ajout nouvelle requête objet.
----

-- Modification de la structure
ALTER TABLE om_requete
    ADD type character varying(200) NULL,
    ADD classe character varying(200) NULL,
    ADD methode character varying(200) NULL;
COMMENT ON COLUMN om_requete.type IS 'Requête SQL ou objet ?';
COMMENT ON COLUMN om_requete.classe IS 'Nom de(s) la classe(s) contenant la méthode';
COMMENT ON COLUMN om_requete.methode IS 'Méthode (de la première classe si plusieurs définies) fournissant les champs de fusion. Si non spécifiée appel à une méthode générique';
-- Modification des données pour respect de la nouvelle contrainte
UPDATE om_requete SET type = 'sql';
-- Ajout de la nouvelle contrainte
ALTER TABLE om_requete
    ALTER type SET NOT NULL;

----
-- END  / OM REQUETE
----
-- Ajout d'un paramètre indiquant que la commune est divisée en arrondissement
INSERT INTO om_parametre VALUES (nextval('om_parametre_seq'), 'option_arrondissement', 'false', 1); 

-- Supression du paramètre d'un paramètre si non utilisé
DELETE FROM om_parametre WHERE libelle = 'id_evenement_bordereau_avis_maire_prefet' AND valeur='';

--Ajout d'un paramètre
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'), 'id_evenement_bordereau_avis_maire_prefet', '-1', 1
WHERE
    NOT EXISTS (
        SELECT libelle FROM om_parametre WHERE libelle = 'id_evenement_bordereau_avis_maire_prefet'
    );

--Mise à jour d'un bordereau
UPDATE om_sousetat SET om_sql = 'SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' \ '',
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEgroupe
    ON
        dossier_autorisation_type.groupe = groupe.groupe
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    evenement.retour IS FALSE
    AND (LOWER(autorite_competente.code) = ''etatmaire''
      OR LOWER(autorite_competente.code) = ''etat'')
    AND instruction.date_envoi_rar >= ''&date_bordereau_debut''
    AND instruction.date_envoi_rar <= ''&date_bordereau_fin''
    AND LOWER(groupe.code) = ''ads''
    AND lien_dossier_demandeur.petitionnaire_principal IS TRUE 
    AND instruction.evenement = ''&id_evenement_bordereau_avis_maire_prefet''
ORDER BY
    dossier.dossier'
WHERE id = 'bordereau_avis_maire_prefet';

-- Ajout de droits sur les consultations pour les divisionnaires
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_consulter', 
    (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        WHERE NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_definaliser', 
    (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        WHERE NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'consultation_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_fichier_telecharger', 
    (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        WHERE NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_fichier_voir', 
    (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        WHERE NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_voir' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_finaliser', 
    (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        WHERE NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'consultation_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_supprimer', 
    (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        WHERE NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'consultation_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_tab', 
    (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        WHERE NOT EXISTS (
            SELECT om_droit FROM om_droit WHERE libelle = 'consultation_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
        );

-- Suppression des droits sur le guichet unique au divisionnaire
DELETE FROM om_droit WHERE libelle = 'menu_guichet_unique' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE');