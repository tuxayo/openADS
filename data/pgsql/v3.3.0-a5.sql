--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.3.0-a5
--
-- @package openfoncier
-- @version SVN : $Id: v3.3.0-a5.sql 1804 2013-04-30 14:45:17Z fmichon $
--------------------------------------------------------------------------------

UPDATE dossier_autorisation SET etat_dossier_autorisation = 7 WHERE etat_dossier_autorisation IS NULL;