----
-- BEGIN / droits onglet "Pièce(s)"
----
-- tout contexte
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'document_numerise_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE')
    );
-- ajout de pièces depuis l'onglet
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'document_numerise_add',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_add' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'document_numerise_add',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_add' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE')
    );
-- contexte demande d'avis
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'demande_avis_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_avis_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'demande_avis_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_avis_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
    );
-- contexte dossier d'autorisation
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE

    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_autorisation_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
-- contexte dossier d'instruction
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );
INSERT INTO
    om_droit (om_droit, libelle, om_profil)
SELECT
    nextval('om_droit_seq'),'dossier_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
----
-- END / droits onglet "Pièce(s)"
----

-- Ajout des droits concernant la finalisation des instructions
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_finaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_finaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
    );
SELECT nextval('om_droit_seq'),'instruction_finaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );
SELECT nextval('om_droit_seq'),'instruction_finaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_definaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_definaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
    );
SELECT nextval('om_droit_seq'),'instruction_definaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );
SELECT nextval('om_droit_seq'),'instruction_definaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );