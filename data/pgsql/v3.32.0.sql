

--
-- BEGIN - Refonte de l'interface avec le référentiel ERP
--

DELETE FROM om_parametre WHERE libelle LIKE 'erp_%';
DELETE FROM om_parametre WHERE libelle LIKE 'option_erp';

ALTER TABLE dossier ADD COLUMN interface_referentiel_erp BOOLEAN;

--
-- END - Refonte de l'interface avec le référentiel ERP
--

-- 
-- START - [#8452] Amélioration des champs de fusion
--

-- om_requete instruction
UPDATE om_requete
SET requete = '
SELECT

    --Données générales de l''événement d''instruction
    
    instruction.complement_om_html as complement_instruction,
    instruction.complement2_om_html as complement2_instruction,
    instruction.complement3_om_html as complement3_instruction,
    instruction.complement4_om_html as complement4_instruction,
    instruction.code_barres as code_barres_instruction,
    to_char(instruction.date_evenement,''DD/MM/YYYY'') as date_evenement_instruction, 
    om_lettretype.libelle as libelle_om_lettretype,
    instruction.archive_delai as archive_delai_instruction,

    --Données générales du dossier d''instruction

    dossier.dossier_libelle as libelle_dossier,
    dossier.dossier as code_barres_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    to_char(dossier_autorisation.date_decision, ''DD/MM/YYYY'') as date_decision_da,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    dossier.terrain_superficie as terrain_superficie_dossier,
    quartier.libelle as libelle_quartier,

    avis_decision.libelle as libelle_avis_decision,

    dossier_autorisation.cle_acces_citoyen,

    --Données générales du paramétrage de l''événement

    evenement.libelle as libelle_evenement,
    evenement.etat as etat_evenement,
    evenement.delai as delai_evenement,
    evenement.accord_tacite as accord_tacite_evenement,
    evenement.delai_notification as delai_notification_evenement,
    evenement.avis_decision as avis_decision_evenement,
    evenement.autorite_competente as autorite_competente_evenement,

    --Coordonnées de l''instructeur

    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    --Adresse du terrain du dossier d''instruction

    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    CASE 
        WHEN dossier.terrain_adresse_bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', dossier.terrain_adresse_bp)
    END as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    CASE 
        WHEN dossier.terrain_adresse_cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', dossier.terrain_adresse_cedex)
    END as terrain_adresse_cedex_dossier,

    arrondissement.libelle as libelle_arrondissement,

    --Taxe d''aménagement du dossier d''instruction

    CASE
        WHEN tax_secteur = 1 THEN taxe_amenagement.tx_comm_secteur_1
        WHEN tax_secteur = 2 THEN taxe_amenagement.tx_comm_secteur_2
        WHEN tax_secteur = 3 THEN taxe_amenagement.tx_comm_secteur_3
        WHEN tax_secteur = 4 THEN taxe_amenagement.tx_comm_secteur_4
        WHEN tax_secteur = 5 THEN taxe_amenagement.tx_comm_secteur_5
        WHEN tax_secteur = 6 THEN taxe_amenagement.tx_comm_secteur_6
        WHEN tax_secteur = 7 THEN taxe_amenagement.tx_comm_secteur_7
        WHEN tax_secteur = 8 THEN taxe_amenagement.tx_comm_secteur_8
        WHEN tax_secteur = 9 THEN taxe_amenagement.tx_comm_secteur_9
        WHEN tax_secteur = 10 THEN taxe_amenagement.tx_comm_secteur_10
        WHEN tax_secteur = 11 THEN taxe_amenagement.tx_comm_secteur_11
        WHEN tax_secteur = 12 THEN taxe_amenagement.tx_comm_secteur_12
        WHEN tax_secteur = 13 THEN taxe_amenagement.tx_comm_secteur_13
        WHEN tax_secteur = 14 THEN taxe_amenagement.tx_comm_secteur_14
        WHEN tax_secteur = 15 THEN taxe_amenagement.tx_comm_secteur_15
        WHEN tax_secteur = 16 THEN taxe_amenagement.tx_comm_secteur_16
        WHEN tax_secteur = 17 THEN taxe_amenagement.tx_comm_secteur_17
        WHEN tax_secteur = 18 THEN taxe_amenagement.tx_comm_secteur_18
        WHEN tax_secteur = 19 THEN taxe_amenagement.tx_comm_secteur_19
        WHEN tax_secteur = 20 THEN taxe_amenagement.tx_comm_secteur_20
    END as tax_taux_secteur,
    dossier.tax_secteur as tax_numero_secteur,
    dossier.tax_mtn_part_commu as tax_montant_part_communale,
    dossier.tax_mtn_part_depart as tax_montant_part_departementale,
    dossier.tax_mtn_part_reg as tax_montant_part_regionale,
    dossier.tax_mtn_total as tax_montant_total,

    --Coordonnées du pétitionnaire principal

    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
    END as nom_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_principal_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_nom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_denomination
    END as denomination_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire principal initial

    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_initial_civilite.libelle, petitionnaire_principal_initial.particulier_nom, petitionnaire_principal_initial.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_principal_initial.personne_morale_nom IS NOT NULL OR petitionnaire_principal_initial.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_initial.personne_morale_raison_sociale, petitionnaire_principal_initial.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_initial_civilite.libelle, petitionnaire_principal_initial.personne_morale_nom, petitionnaire_principal_initial.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_principal_initial.personne_morale_raison_sociale, '' '', petitionnaire_principal_initial.personne_morale_denomination))
            END
    END as nom_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier'' OR petitionnaire_principal_initial.personne_morale_nom IS NOT NULL OR petitionnaire_principal_initial.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_principal_initial_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN petitionnaire_principal_initial.particulier_nom
        ELSE
            CASE WHEN petitionnaire_principal_initial.personne_morale_nom IS NOT NULL
                THEN petitionnaire_principal_initial.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN petitionnaire_principal_initial.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_principal_initial.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal_initial.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal_initial.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_principal_initial,
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal_initial.personne_morale_denomination
    END as denomination_petitionnaire_principal_initial,
    petitionnaire_principal_initial.numero as numero_petitionnaire_principal_initial,
    petitionnaire_principal_initial.voie as voie_petitionnaire_principal_initial,
    petitionnaire_principal_initial.complement as complement_petitionnaire_principal_initial,
    petitionnaire_principal_initial.lieu_dit as lieu_dit_petitionnaire_principal_initial,
    CASE 
        WHEN petitionnaire_principal_initial.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal_initial.bp)
    END as bp_petitionnaire_principal_initial,
    petitionnaire_principal_initial.code_postal as code_postal_petitionnaire_principal_initial,
    petitionnaire_principal_initial.localite as localite_petitionnaire_principal_initial,
    CASE 
        WHEN petitionnaire_principal_initial.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal_initial.cedex)
    END as cedex_petitionnaire_principal_initial,
    petitionnaire_principal_initial.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1

    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_1_civilite.libelle, petitionnaire_1.particulier_nom, petitionnaire_1.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_1.personne_morale_raison_sociale, petitionnaire_1.personne_morale_denomination, ''représenté(e) par'', petitionnaire_1_civilite.libelle, petitionnaire_1.personne_morale_nom, petitionnaire_1.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
            END
    END as nom_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier'' OR petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_1_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_nom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL
                THEN petitionnaire_1.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_1.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_denomination
    END as denomination_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2

    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_2_civilite.libelle, petitionnaire_2.particulier_nom, petitionnaire_2.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_2.personne_morale_raison_sociale, petitionnaire_2.personne_morale_denomination, ''représenté(e) par'', petitionnaire_2_civilite.libelle, petitionnaire_2.personne_morale_nom, petitionnaire_2.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
            END
    END as nom_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier'' OR petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_2_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_nom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL
                THEN petitionnaire_2.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_2.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_denomination
    END as denomination_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3

    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_3_civilite.libelle, petitionnaire_3.particulier_nom, petitionnaire_3.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_3.personne_morale_raison_sociale, petitionnaire_3.personne_morale_denomination, ''représenté(e) par'', petitionnaire_3_civilite.libelle, petitionnaire_3.personne_morale_nom, petitionnaire_3.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
            END
    END as nom_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier'' OR petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_3_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_nom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL
                THEN petitionnaire_3.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_3.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_denomination
    END as denomination_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4

    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_4_civilite.libelle, petitionnaire_4.particulier_nom, petitionnaire_4.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_4.personne_morale_raison_sociale, petitionnaire_4.personne_morale_denomination, ''représenté(e) par'', petitionnaire_4_civilite.libelle, petitionnaire_4.personne_morale_nom, petitionnaire_4.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
            END
    END as nom_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier'' OR petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_4_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_nom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL
                THEN petitionnaire_4.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_4.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_denomination
    END as denomination_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5

    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_5_civilite.libelle, petitionnaire_5.particulier_nom, petitionnaire_5.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_5.personne_morale_raison_sociale, petitionnaire_5.personne_morale_denomination, ''représenté(e) par'', petitionnaire_5_civilite.libelle, petitionnaire_5.personne_morale_nom, petitionnaire_5.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
            END
    END as nom_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier'' OR petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_5_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_nom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL
                THEN petitionnaire_5.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_5.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_denomination
    END as denomination_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,

    --Coordonnées du délégataire

    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', delegataire_civilite.libelle, delegataire.particulier_nom, delegataire.particulier_prenom))
        ELSE
            CASE WHEN delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', delegataire.personne_morale_raison_sociale, delegataire.personne_morale_denomination, ''représenté(e) par'', delegataire_civilite.libelle, delegataire.personne_morale_nom, delegataire.personne_morale_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_delegataire,
    CASE WHEN delegataire.qualite=''particulier'' OR delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
        THEN delegataire_civilite.libelle
        ELSE ''''
    END as civilite_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN delegataire.particulier_nom
        ELSE
            CASE WHEN delegataire.personne_morale_nom IS NOT NULL
                THEN delegataire.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN delegataire.particulier_prenom
        ELSE
            CASE WHEN delegataire.personne_morale_prenom IS NOT NULL
                THEN delegataire.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN ''''
        ELSE delegataire.personne_morale_raison_sociale
    END as raison_sociale_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN ''''
        ELSE delegataire.personne_morale_denomination
    END as denomination_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complement_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    CASE 
        WHEN delegataire.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', delegataire.bp)
    END as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    CASE 
        WHEN delegataire.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', delegataire.cedex)
    END as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT_WS('' '', delegataire_civilite.libelle, delegataire.particulier_nom, delegataire.particulier_prenom))
                ELSE
                    CASE WHEN delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                        THEN TRIM(CONCAT_WS('' '', delegataire.personne_morale_raison_sociale, delegataire.personne_morale_denomination, ''représenté(e) par'', delegataire_civilite.libelle, delegataire.personne_morale_nom, delegataire.personne_morale_prenom))
                        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
                    END
            END
    END as nom_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal_civilite.libelle
                ELSE ''''
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier'' OR delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                THEN delegataire_civilite.libelle
                ELSE ''''
            END
    END as civilite_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN petitionnaire_principal.particulier_nom
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                        THEN petitionnaire_principal.personne_morale_nom
                        ELSE ''''
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN delegataire.particulier_nom
                ELSE
                    CASE WHEN delegataire.personne_morale_nom IS NOT NULL
                        THEN delegataire.personne_morale_nom
                        ELSE ''''
                    END
            END
    END as nom_particulier_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN petitionnaire_principal.particulier_prenom
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                        THEN petitionnaire_principal.personne_morale_prenom
                        ELSE ''''
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN delegataire.particulier_prenom
                ELSE
                    CASE WHEN delegataire.personne_morale_prenom IS NOT NULL
                        THEN delegataire.personne_morale_prenom
                        ELSE ''''
                    END
            END
    END as prenom_particulier_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN ''''
                ELSE petitionnaire_principal.personne_morale_raison_sociale
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN ''''
                ELSE delegataire.personne_morale_raison_sociale
            END
    END as raison_sociale_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN ''''
                ELSE petitionnaire_principal.personne_morale_denomination
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN ''''
                ELSE delegataire.personne_morale_denomination
            END
    END as denomination_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', petitionnaire_principal.bp)
            END
        ELSE 
            CASE 
                WHEN delegataire.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', delegataire.bp)
            END
    END as bp_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
            END 
        ELSE 
            CASE 
                WHEN delegataire.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', delegataire.cedex)
            END 
    END as cedex_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    --Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_complet, ''DD/MM/YYYY'') as date_completude,
    to_char(dossier.date_dernier_depot, ''DD/MM/YYYY'') as date_dernier_depot,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    CASE WHEN dossier.incomplet_notifie IS TRUE AND dossier.incompletude IS TRUE 
        THEN to_char(dossier.date_limite_incompletude ,''DD/MM/YYYY'') 
        ELSE to_char(dossier.date_limite ,''DD/MM/YYYY'')
    END as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    to_char(dossier.date_notification_delai,''DD/MM/YYYY'') as date_notification_delai_dossier,
    
    --Noms des signataires
    TRIM(CONCAT(signataire_civilite.libelle, '' '',signataire_arrete.prenom, '' '', signataire_arrete.nom)) as arrete_signataire,
    TRIM(CONCAT(signataire_arrete.qualite, '' '', signataire_arrete.signature)) as signature_signataire,
    division.chef as chef_division,
    direction.chef as chef_direction,
    direction.libelle as libelle_direction,
    direction.description as description_direction,
    
    --Données générales des données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.ope_proj_desc as ope_proj_desc_donnees_techniques,
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_cstr_shon_tot
        ELSE donnees_techniques.su_cstr_shon_tot
    END as su_cstr_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_demo_shon_tot
        ELSE donnees_techniques.su_demo_shon_tot
    END as su_demo_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN
            REGEXP_REPLACE(CONCAT(
                CASE WHEN donnees_techniques.su2_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation agricole - '', donnees_techniques.su2_cstr_shon1, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation forestière - '', donnees_techniques.su2_cstr_shon2, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Logement - '', donnees_techniques.su2_cstr_shon3, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement - '', donnees_techniques.su2_cstr_shon4, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Artisanat et commerce de détail - '', donnees_techniques.su2_cstr_shon5, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Restauration - '', donnees_techniques.su2_cstr_shon6, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Commerce de gros - '', donnees_techniques.su2_cstr_shon7, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Activités de services où s''''effectue l''''accueil d''''une clientèle - '', donnees_techniques.su2_cstr_shon8, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement hôtelier et touristique - '', donnees_techniques.su2_cstr_shon9, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon10 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Cinéma - '', donnees_techniques.su2_cstr_shon10, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon11 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux et bureaux accueillant du public des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon11, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon12 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux techniques et industriels des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon12, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon13 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Établissements d''''enseignement, de santé et d''''action sociale - '', donnees_techniques.su2_cstr_shon13, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon14 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Salles d''''art et de spectacles - '', donnees_techniques.su2_cstr_shon14, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon15 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Équipements sportifs - '', donnees_techniques.su2_cstr_shon15, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon16 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Autres équipements recevant du public - '', donnees_techniques.su2_cstr_shon16, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon17 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Industrie - '', donnees_techniques.su2_cstr_shon17, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon18 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Entrepôt - '', donnees_techniques.su2_cstr_shon18, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon19 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Bureau - '', donnees_techniques.su2_cstr_shon19, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon20 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Centre de congrès et d''''exposition - '', donnees_techniques.su2_cstr_shon20, '' m²'')
                END
            ), '' / $'', '''')
        ELSE
            REGEXP_REPLACE(CONCAT(
                CASE
                    WHEN donnees_techniques.su_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
                END, 
                CASE
                    WHEN donnees_techniques.su_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
                END
            ), '' / $'', '''')
    END as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_tot_shon_tot
        ELSE donnees_techniques.su_tot_shon_tot
    END as su_tot_shon_tot_donnees_techniques,

    -- Données techniques pour les AT
    CONCAT_WS('', '',
        CASE WHEN donnees_techniques.erp_cstr_neuve IS TRUE THEN ''construction neuve'' END,
        CASE WHEN donnees_techniques.erp_trvx_acc IS TRUE THEN ''travaux de mise en conformité totale aux règles d’accessibilité'' END,
        CASE WHEN donnees_techniques.erp_extension IS TRUE THEN ''extension'' END,
        CASE WHEN donnees_techniques.erp_rehab IS TRUE THEN ''réhabilitation'' END,
        CASE WHEN donnees_techniques.erp_trvx_am IS TRUE THEN ''travaux d’aménagement (remplacement de revêtements, rénovation électrique, création d’une rampe, par exemple)'' END,
        CASE WHEN donnees_techniques.erp_vol_nouv_exist IS TRUE THEN ''création de volumes nouveaux dans des volumes existants (modification du cloisonnement, par exemple)'' END
    ) as at_type_travaux,
    donnees_techniques.erp_public_eff_tot as at_effectif_public_total,
    CONCAT_WS('' - '', erp_categorie.libelle, erp_categorie.description) as at_categorie_etablissement,
    CONCAT_WS('' - '', erp_type.libelle, erp_type.description) as at_type_etablissement,
    
    --Bordereau d''envoi au maire
    CASE
        WHEN evenement.type = ''arrete''
        THEN CONCAT(''transmission d''''une proposition de décision sur '', evenement.libelle)
        ELSE CONCAT(''transmission d''''un courrier d''''instruction sur '', evenement.libelle)
    END as objet_bordereau_envoi_maire
    
FROM
    &DB_PREFIXEinstruction
LEFT JOIN
    &DB_PREFIXEsignataire_arrete
    ON instruction.signataire_arrete = signataire_arrete.signataire_arrete
LEFT JOIN
    &DB_PREFIXEcivilite as signataire_civilite
    ON signataire_arrete.civilite = signataire_civilite.civilite
LEFT JOIN
    &DB_PREFIXEom_lettretype
    ON instruction.lettretype = om_lettretype.id and om_lettretype.actif IS TRUE
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        instruction.dossier=dossier.dossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        instruction.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite as petitionnaire_principal_civilite
    ON
        petitionnaire_principal.particulier_civilite = petitionnaire_principal_civilite.civilite OR petitionnaire_principal.personne_morale_civilite = petitionnaire_principal_civilite.civilite
LEFT JOIN
    &DB_PREFIXElien_dossier_autorisation_demandeur
    ON
        dossier.dossier_autorisation = lien_dossier_autorisation_demandeur.dossier_autorisation AND lien_dossier_autorisation_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal_initial
    ON
        lien_dossier_autorisation_demandeur.demandeur = petitionnaire_principal_initial.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite as petitionnaire_principal_initial_civilite
    ON
        petitionnaire_principal_initial.particulier_civilite = petitionnaire_principal_initial_civilite.civilite OR petitionnaire_principal_initial.personne_morale_civilite = petitionnaire_principal_initial_civilite.civilite
LEFT JOIN
    (
    &DB_PREFIXElien_dossier_demandeur AS lien_dossier_delegataire
        JOIN &DB_PREFIXEdemandeur as delegataire
        ON
            lien_dossier_delegataire.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
    )
    ON
        instruction.dossier = lien_dossier_delegataire.dossier AND lien_dossier_delegataire.petitionnaire_principal IS FALSE
LEFT JOIN
    &DB_PREFIXEcivilite as delegataire_civilite
    ON
        delegataire.particulier_civilite = delegataire_civilite.civilite OR delegataire.personne_morale_civilite = delegataire_civilite.civilite
LEFT JOIN (
    SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
    FROM &DB_PREFIXElien_dossier_demandeur
    LEFT JOIN &DB_PREFIXEdossier
        ON lien_dossier_demandeur.dossier=dossier.dossier 
        AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
    LEFT JOIN &DB_PREFIXEinstruction
        ON instruction.dossier = dossier.dossier
    WHERE instruction.instruction = &idx
    GROUP BY lien_dossier_demandeur.dossier
) as sub_petitionnaire_autre
ON instruction.dossier = sub_petitionnaire_autre.dossier
LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1] AND petitionnaire_1.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_1_civilite
    ON
        petitionnaire_1.particulier_civilite = petitionnaire_1_civilite.civilite OR petitionnaire_1.personne_morale_civilite = petitionnaire_1_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2] AND petitionnaire_2.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_2_civilite
    ON
        petitionnaire_2.particulier_civilite = petitionnaire_2_civilite.civilite OR petitionnaire_2.personne_morale_civilite = petitionnaire_2_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3] AND petitionnaire_3.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_3_civilite
    ON
        petitionnaire_3.particulier_civilite = petitionnaire_3_civilite.civilite OR petitionnaire_3.personne_morale_civilite = petitionnaire_3_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4] AND petitionnaire_4.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_4_civilite
    ON
        petitionnaire_4.particulier_civilite = petitionnaire_4_civilite.civilite OR petitionnaire_4.personne_morale_civilite = petitionnaire_4_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5] AND petitionnaire_5.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_5_civilite
    ON
        petitionnaire_5.particulier_civilite = petitionnaire_5_civilite.civilite OR petitionnaire_5.personne_morale_civilite = petitionnaire_5_civilite.civilite
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN 
    &DB_PREFIXEdirection
    ON division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN 
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN 
    &DB_PREFIXEquartier
    ON
        dossier.quartier = quartier.quartier
LEFT JOIN
    &DB_PREFIXEtaxe_amenagement
    ON
        dossier.om_collectivite = taxe_amenagement.om_collectivite
LEFT JOIN
    &DB_PREFIXEerp_categorie
    ON
        donnees_techniques.erp_class_cat = erp_categorie.erp_categorie
LEFT JOIN
    &DB_PREFIXEerp_type
    ON
        donnees_techniques.erp_class_type = erp_type.erp_type
WHERE instruction.instruction = &idx
',
merge_fields = '
--Données générales de l''événement d''instruction
[complement_instruction]
[complement2_instruction]
[complement3_instruction]
[complement4_instruction]
[code_barres_instruction]
[date_evenement_instruction]
[libelle_om_lettretype]
[archive_delai_instruction]

--Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]    [delai_dossier]    [terrain_references_cadastrales_dossier]
[terrain_superficie_dossier]
[libelle_quartier]
[libelle_da]

[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]

[libelle_avis_decision]

--Données générales du paramétrage de l''événement
[libelle_evenement]
[etat_evenement]
[delai_evenement]
[accord_tacite_evenement]
[delai_notification_evenement]
[avis_decision_evenement]
[autorite_competente_evenement]
[cle_acces_citoyen]

--Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

--Adresse du terrain du dossier d''instruction 
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]

[libelle_arrondissement]

--Taxe d''aménagement du dossier d''instruction
[tax_taux_secteur]
[tax_numero_secteur]
[tax_montant_part_communale]
[tax_montant_part_departementale]
[tax_montant_part_regionale]
[tax_montant_total]

--Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[civilite_petitionnaire_principal]
[nom_particulier_petitionnaire_principal]
[prenom_particulier_petitionnaire_principal]
[raison_sociale_petitionnaire_principal]
[denomination_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées du pétitionnaire principal initial
[nom_petitionnaire_principal_initial]
[civilite_petitionnaire_principal_initial]
[nom_particulier_petitionnaire_principal_initial]
[prenom_particulier_petitionnaire_principal_initial]
[raison_sociale_petitionnaire_principal_initial]
[denomination_petitionnaire_principal_initial]
[numero_petitionnaire_principal_initial]    [voie_petitionnaire_principal_initial]    [complement_petitionnaire_principal_initial]
[lieu_dit_petitionnaire_principal_initial]    [bp_petitionnaire_principal_initial]
[code_postal_petitionnaire_principal_initial]    [localite_petitionnaire_principal_initial]    [cedex_petitionnaire_principal_initial]
[pays_petitionnaire_principal_initial]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''à 5)
[civilite_petitionnaire_1](jusqu''à 5)
[nom_particulier_petitionnaire_1](jusqu''à 5)
[prenom_particulier_petitionnaire_1](jusqu''à 5)
[raison_sociale_petitionnaire_1](jusqu''à 5)
[denomination_petitionnaire_1](jusqu''à 5)
[numero_petitionnaire_1](jusqu''à 5)    [voie_petitionnaire_1](jusqu''à 5)    
[complement_petitionnaire_1](jusqu''à 5)
[lieu_dit_petitionnaire_1](jusqu''à 5)    [bp_petitionnaire_1](jusqu''à 5)
[code_postal_petitionnaire_1](jusqu''à 5)    [localite_petitionnaire_1](jusqu''à 5)    
[cedex_petitionnaire_1](jusqu''à 5)
[pays_petitionnaire_1](jusqu''à 5)

--Coordonnées du délégataire
[nom_delegataire]
[civilite_delegataire]
[nom_particulier_delegataire]Fsu_
[prenom_particulier_delegataire]
[raison_sociale_delegataire]
[denomination_delegataire]
[numero_delegataire]    [voie_delegataire]    [complement_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire]

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[civilite_correspondant]
[nom_particulier_correspondant]
[prenom_particulier_correspondant]
[raison_sociale_correspondant]
[denomination_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant]

--Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_completude]
[date_dernier_depot]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]
[date_notification_delai_dossier]
[date_decision_da]

--Noms des signataires
[arrete_signataire]
[signature_signataire]
[chef_division]
[chef_direction]
[libelle_direction]
[description_direction]

--Données générales des données techniques
[co_projet_desc_donnees_techniques]    [am_projet_desc_donnees_techniques]
[dm_projet_desc_donnees_techniques]    [ope_proj_desc_donnees_techniques]
[projet_desc_donnees_techniques]
[co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
-- Les données techniques suivantes concernent le tableau des surfaces
-- Elles récupèrent les valeurs du tableau composé des sous-destinations si au
-- moins une valeur de celui-ci est saisie
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]
[su_tot_shon_tot_donnees_techniques]

--Données techniques des AT
[at_type_travaux]
[at_effectif_public_total]
[at_categorie_etablissement]
[at_type_etablissement]

--Bordereau envoi au maire
[objet_bordereau_envoi_maire]
'
WHERE code = 'instruction';

-- om_requete dossier
UPDATE om_requete
SET requete ='
SELECT

    -- Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    dossier.dossier as code_barres_dossier,
    etat.libelle as etat_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,

    avis_decision.libelle as libelle_avis_decision,

    -- Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    -- Noms des signataires
    division.chef as division_chef,
    direction.chef as direction_chef,
    direction.libelle as libelle_direction,
    direction.description as description_direction,

    -- Adresse du terrain du dossier d''instruction
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    CASE 
        WHEN dossier.terrain_adresse_bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', dossier.terrain_adresse_bp)
    END as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    CASE 
        WHEN dossier.terrain_adresse_cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', dossier.terrain_adresse_cedex)
    END as terrain_adresse_cedex_dossier,
    arrondissement.libelle as libelle_arrondissement,

    -- Nom et prénom de l''architecte
    CONCAT(architecte.prenom||'' '', architecte.nom) as architecte,

    --Taxe d''aménagement du dossier d''instruction
    CASE
        WHEN tax_secteur = 1 THEN taxe_amenagement.tx_comm_secteur_1
        WHEN tax_secteur = 2 THEN taxe_amenagement.tx_comm_secteur_2
        WHEN tax_secteur = 3 THEN taxe_amenagement.tx_comm_secteur_3
        WHEN tax_secteur = 4 THEN taxe_amenagement.tx_comm_secteur_4
        WHEN tax_secteur = 5 THEN taxe_amenagement.tx_comm_secteur_5
        WHEN tax_secteur = 6 THEN taxe_amenagement.tx_comm_secteur_6
        WHEN tax_secteur = 7 THEN taxe_amenagement.tx_comm_secteur_7
        WHEN tax_secteur = 8 THEN taxe_amenagement.tx_comm_secteur_8
        WHEN tax_secteur = 9 THEN taxe_amenagement.tx_comm_secteur_9
        WHEN tax_secteur = 10 THEN taxe_amenagement.tx_comm_secteur_10
        WHEN tax_secteur = 11 THEN taxe_amenagement.tx_comm_secteur_11
        WHEN tax_secteur = 12 THEN taxe_amenagement.tx_comm_secteur_12
        WHEN tax_secteur = 13 THEN taxe_amenagement.tx_comm_secteur_13
        WHEN tax_secteur = 14 THEN taxe_amenagement.tx_comm_secteur_14
        WHEN tax_secteur = 15 THEN taxe_amenagement.tx_comm_secteur_15
        WHEN tax_secteur = 16 THEN taxe_amenagement.tx_comm_secteur_16
        WHEN tax_secteur = 17 THEN taxe_amenagement.tx_comm_secteur_17
        WHEN tax_secteur = 18 THEN taxe_amenagement.tx_comm_secteur_18
        WHEN tax_secteur = 19 THEN taxe_amenagement.tx_comm_secteur_19
        WHEN tax_secteur = 20 THEN taxe_amenagement.tx_comm_secteur_20
    END as tax_taux_secteur,
    dossier.tax_secteur as tax_numero_secteur,
    dossier.tax_mtn_part_commu as tax_montant_part_communale,
    dossier.tax_mtn_part_depart as tax_montant_part_departementale,
    dossier.tax_mtn_part_reg as tax_montant_part_regionale,
    dossier.tax_mtn_total as tax_montant_total,

    -- Coordonnées du pétitionnaire principal
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '',petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
    END as nom_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_principal_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_nom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_denomination
    END as denomination_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_1_civilite.libelle, petitionnaire_1.particulier_nom, petitionnaire_1.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_1.personne_morale_raison_sociale, petitionnaire_1.personne_morale_denomination, ''représenté(e) par'', petitionnaire_1_civilite.libelle, petitionnaire_1.personne_morale_nom, petitionnaire_1.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
            END
    END as nom_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier'' OR petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_1_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_nom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL
                THEN petitionnaire_1.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_1.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_denomination
    END as denomination_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_2_civilite.libelle, petitionnaire_2.particulier_nom, petitionnaire_2.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_2.personne_morale_raison_sociale, petitionnaire_2.personne_morale_denomination, ''représenté(e) par'', petitionnaire_2_civilite.libelle, petitionnaire_2.personne_morale_nom, petitionnaire_2.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
            END
    END as nom_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier'' OR petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_2_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_nom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL
                THEN petitionnaire_2.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_2.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_denomination
    END as denomination_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_3_civilite.libelle, petitionnaire_3.particulier_nom, petitionnaire_3.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_3.personne_morale_raison_sociale, petitionnaire_3.personne_morale_denomination, ''représenté(e) par'', petitionnaire_3_civilite.libelle, petitionnaire_3.personne_morale_nom, petitionnaire_3.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
            END
    END as nom_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier'' OR petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_3_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_nom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL
                THEN petitionnaire_3.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_3.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_denomination
    END as denomination_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_4_civilite.libelle, petitionnaire_4.particulier_nom, petitionnaire_4.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_4.personne_morale_raison_sociale, petitionnaire_4.personne_morale_denomination, ''représenté(e) par'', petitionnaire_4_civilite.libelle, petitionnaire_4.personne_morale_nom, petitionnaire_4.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
            END
    END as nom_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier'' OR petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_4_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_nom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL
                THEN petitionnaire_4.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_4.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_denomination
    END as denomination_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_5_civilite.libelle, petitionnaire_5.particulier_nom, petitionnaire_5.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_5.personne_morale_raison_sociale, petitionnaire_5.personne_morale_denomination, ''représenté(e) par'', petitionnaire_5_civilite.libelle, petitionnaire_5.personne_morale_nom, petitionnaire_5.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
            END
    END as nom_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier'' OR petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_5_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_nom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL
                THEN petitionnaire_5.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_5.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_denomination
    END as denomination_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,

    -- Coordonnées du délégataire
    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '',delegataire_civilite.libelle, delegataire.particulier_nom, delegataire.particulier_prenom))
        ELSE
            CASE WHEN delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', delegataire.personne_morale_raison_sociale, delegataire.personne_morale_denomination, ''représenté(e) par'', delegataire_civilite.libelle, delegataire.personne_morale_nom, delegataire.personne_morale_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_delegataire,
    CASE WHEN delegataire.qualite=''particulier'' OR delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
        THEN delegataire_civilite.libelle
        ELSE ''''
    END as civilite_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN delegataire.particulier_nom
        ELSE
            CASE WHEN delegataire.personne_morale_nom IS NOT NULL
                THEN delegataire.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN delegataire.particulier_prenom
        ELSE
            CASE WHEN delegataire.personne_morale_prenom IS NOT NULL
                THEN delegataire.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN ''''
        ELSE delegataire.personne_morale_raison_sociale
    END as raison_sociale_delegataire,
    CASE WHEN delegataire.qualite=''particulier''
        THEN ''''
        ELSE delegataire.personne_morale_denomination
    END as denomination_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complement_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    CASE 
        WHEN delegataire.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', delegataire.bp)
    END as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    CASE 
        WHEN delegataire.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', delegataire.cedex)
    END as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    CASE
        WHEN delegataire.qualite IS NULL
        THEN
                CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
                    ELSE
                        CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                            THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                            ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
                        END
                END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT_WS('' '', delegataire_civilite.libelle, delegataire.particulier_nom, delegataire.particulier_prenom))
                ELSE
                    CASE WHEN delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                        THEN TRIM(CONCAT_WS('' '', delegataire.personne_morale_raison_sociale, delegataire.personne_morale_denomination, ''représenté(e) par'', delegataire_civilite.libelle, delegataire.personne_morale_nom, delegataire.personne_morale_prenom))
                        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
                    END
            END
    END as nom_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal_civilite.libelle
                ELSE ''''
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier'' OR delegataire.personne_morale_nom IS NOT NULL OR delegataire.personne_morale_prenom IS NOT NULL
                THEN delegataire_civilite.libelle
                ELSE ''''
            END
    END as civilite_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN petitionnaire_principal.particulier_nom
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                        THEN petitionnaire_principal.personne_morale_nom
                        ELSE ''''
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN delegataire.particulier_nom
                ELSE
                    CASE WHEN delegataire.personne_morale_nom IS NOT NULL
                        THEN delegataire.personne_morale_nom
                        ELSE ''''
                    END
            END
    END as nom_particulier_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN petitionnaire_principal.particulier_prenom
                ELSE
                    CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                        THEN petitionnaire_principal.personne_morale_prenom
                        ELSE ''''
                    END
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN delegataire.particulier_prenom
                ELSE
                    CASE WHEN delegataire.personne_morale_prenom IS NOT NULL
                        THEN delegataire.personne_morale_prenom
                        ELSE ''''
                    END
            END
    END as prenom_particulier_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN ''''
                ELSE petitionnaire_principal.personne_morale_raison_sociale
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN ''''
                ELSE delegataire.personne_morale_raison_sociale
            END
    END as raison_sociale_correspondant,

    CASE WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                THEN ''''
                ELSE petitionnaire_principal.personne_morale_denomination
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN ''''
                ELSE delegataire.personne_morale_denomination
            END
    END as denomination_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', petitionnaire_principal.bp)
            END
        ELSE 
            CASE 
                WHEN delegataire.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', delegataire.bp)
            END
    END as bp_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
            END
        ELSE 
            CASE 
                WHEN delegataire.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', delegataire.cedex)
            END
    END as cedex_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    -- Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_complet, ''DD/MM/YYYY'') as date_completude,
    to_char(dossier.date_dernier_depot, ''DD/MM/YYYY'') as date_dernier_depot,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    CASE WHEN dossier.incomplet_notifie IS TRUE AND dossier.incompletude IS TRUE 
        THEN to_char(dossier.date_limite_incompletude ,''DD/MM/YYYY'') 
        ELSE to_char(dossier.date_limite ,''DD/MM/YYYY'')
    END as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    
    -- Données générales des données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.ope_proj_desc as ope_proj_desc_donnees_techniques,
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
        -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_cstr_shon_tot
        ELSE donnees_techniques.su_cstr_shon_tot
    END as su_cstr_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_demo_shon_tot
        ELSE donnees_techniques.su_demo_shon_tot
    END as su_demo_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN
            REGEXP_REPLACE(CONCAT(
                CASE WHEN donnees_techniques.su2_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation agricole - '', donnees_techniques.su2_cstr_shon1, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation forestière - '', donnees_techniques.su2_cstr_shon2, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Logement - '', donnees_techniques.su2_cstr_shon3, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement - '', donnees_techniques.su2_cstr_shon4, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Artisanat et commerce de détail - '', donnees_techniques.su2_cstr_shon5, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Restauration - '', donnees_techniques.su2_cstr_shon6, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Commerce de gros - '', donnees_techniques.su2_cstr_shon7, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Activités de services où s''''effectue l''''accueil d''''une clientèle - '', donnees_techniques.su2_cstr_shon8, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement hôtelier et touristique - '', donnees_techniques.su2_cstr_shon9, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon10 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Cinéma - '', donnees_techniques.su2_cstr_shon10, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon11 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux et bureaux accueillant du public des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon11, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon12 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux techniques et industriels des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon12, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon13 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Établissements d''''enseignement, de santé et d''''action sociale - '', donnees_techniques.su2_cstr_shon13, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon14 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Salles d''''art et de spectacles - '', donnees_techniques.su2_cstr_shon14, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon15 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Équipements sportifs - '', donnees_techniques.su2_cstr_shon15, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon16 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Autres équipements recevant du public - '', donnees_techniques.su2_cstr_shon16, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon17 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Industrie - '', donnees_techniques.su2_cstr_shon17, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon18 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Entrepôt - '', donnees_techniques.su2_cstr_shon18, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon19 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Bureau - '', donnees_techniques.su2_cstr_shon19, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon20 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Centre de congrès et d''''exposition - '', donnees_techniques.su2_cstr_shon20, '' m²'')
                END
            ), '' / $'', '''')
        ELSE
            REGEXP_REPLACE(CONCAT(
                CASE
                    WHEN donnees_techniques.su_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
                END, 
                CASE
                    WHEN donnees_techniques.su_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
                END
            ), '' / $'', '''')
    END as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_tot_shon_tot
        ELSE donnees_techniques.su_tot_shon_tot
    END as su_tot_shon_tot_donnees_techniques,

    -- Données techniques pour les AT
    CONCAT_WS('', '',
        CASE WHEN donnees_techniques.erp_cstr_neuve IS TRUE THEN ''construction neuve'' END,
        CASE WHEN donnees_techniques.erp_trvx_acc IS TRUE THEN ''travaux de mise en conformité totale aux règles d’accessibilité'' END,
        CASE WHEN donnees_techniques.erp_extension IS TRUE THEN ''extension'' END,
        CASE WHEN donnees_techniques.erp_rehab IS TRUE THEN ''réhabilitation'' END,
        CASE WHEN donnees_techniques.erp_trvx_am IS TRUE THEN ''travaux d’aménagement (remplacement de revêtements, rénovation électrique, création d’une rampe, par exemple)'' END,
        CASE WHEN donnees_techniques.erp_vol_nouv_exist IS TRUE THEN ''création de volumes nouveaux dans des volumes existants (modification du cloisonnement, par exemple)'' END
    ) as at_type_travaux,
    donnees_techniques.erp_public_eff_tot as at_effectif_public_total,
    CONCAT_WS('' - '', erp_categorie.libelle, erp_categorie.description) as at_categorie_etablissement,
    CONCAT_WS('' - '', erp_type.libelle, erp_type.description) as at_type_etablissement

FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite as petitionnaire_principal_civilite
    ON
        petitionnaire_principal.particulier_civilite = petitionnaire_principal_civilite.civilite OR petitionnaire_principal.personne_morale_civilite = petitionnaire_principal_civilite.civilite
LEFT JOIN
    (
    &DB_PREFIXElien_dossier_demandeur AS lien_dossier_delegataire
        JOIN &DB_PREFIXEdemandeur as delegataire
        ON
            lien_dossier_delegataire.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
    )
    ON
        dossier.dossier = lien_dossier_delegataire.dossier AND lien_dossier_delegataire.petitionnaire_principal IS FALSE
LEFT JOIN
    &DB_PREFIXEcivilite as delegataire_civilite
    ON
        delegataire.particulier_civilite = delegataire_civilite.civilite OR delegataire.personne_morale_civilite = delegataire_civilite.civilite
LEFT JOIN (
    SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
    FROM &DB_PREFIXElien_dossier_demandeur
    LEFT JOIN &DB_PREFIXEdossier
        ON lien_dossier_demandeur.dossier=dossier.dossier 
        AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
    WHERE dossier.dossier = ''&idx''
    GROUP BY lien_dossier_demandeur.dossier
) as sub_petitionnaire_autre
ON dossier.dossier = sub_petitionnaire_autre.dossier
LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1] AND petitionnaire_1.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_1_civilite
    ON
        petitionnaire_1.particulier_civilite = petitionnaire_1_civilite.civilite OR petitionnaire_1.personne_morale_civilite = petitionnaire_1_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2] AND petitionnaire_2.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_2_civilite
    ON
        petitionnaire_2.particulier_civilite = petitionnaire_2_civilite.civilite OR petitionnaire_2.personne_morale_civilite = petitionnaire_2_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3] AND petitionnaire_3.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_3_civilite
    ON
        petitionnaire_3.particulier_civilite = petitionnaire_3_civilite.civilite OR petitionnaire_3.personne_morale_civilite = petitionnaire_3_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4] AND petitionnaire_4.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_4_civilite
    ON
        petitionnaire_4.particulier_civilite = petitionnaire_4_civilite.civilite OR petitionnaire_4.personne_morale_civilite = petitionnaire_4_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5] AND petitionnaire_5.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_5_civilite
    ON
        petitionnaire_5.particulier_civilite = petitionnaire_5_civilite.civilite OR petitionnaire_5.personne_morale_civilite = petitionnaire_5_civilite.civilite
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN
    &DB_PREFIXEdirection
    ON
        division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN
    &DB_PREFIXEarchitecte
    ON
    donnees_techniques.architecte = architecte.architecte
LEFT JOIN
    &DB_PREFIXEtaxe_amenagement
    ON
        dossier.om_collectivite = taxe_amenagement.om_collectivite
LEFT JOIN
    &DB_PREFIXEerp_categorie
    ON
        donnees_techniques.erp_class_cat = erp_categorie.erp_categorie
LEFT JOIN
    &DB_PREFIXEerp_type
    ON
        donnees_techniques.erp_class_type = erp_type.erp_type
WHERE dossier.dossier = ''&idx''
',
merge_fields = '
-- Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]
[etat_dossier]    [libelle_da]
[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]
[delai_dossier]
[terrain_references_cadastrales_dossier]
[libelle_avis_decision]

-- Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

-- Noms des signataires
[division_chef]
[direction_chef]
[libelle_direction]
[description_direction]

-- Adresse du terrain du dossier d''instruction
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]
[libelle_arrondissement]

-- Nom et prénom de l''architecte
[architecte]

--Taxe d''aménagement du dossier d''instruction
[tax_taux_secteur]
[tax_numero_secteur]
[tax_montant_part_communale]
[tax_montant_part_departementale]
[tax_montant_part_regionale]
[tax_montant_total]

-- Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[civilite_petitionnaire_principal]
[nom_particulier_petitionnaire_principal]
[prenom_particulier_petitionnaire_principal]
[raison_sociale_petitionnaire_principal]
[denomination_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''à 5)
[civilite_petitionnaire_1](jusqu''à 5)
[nom_particulier_petitionnaire_1](jusqu''à 5)
[prenom_particulier_petitionnaire_1](jusqu''à 5)
[raison_sociale_petitionnaire_1](jusqu''à 5)
[denomination_petitionnaire_1](jusqu''à 5)
[numero_petitionnaire_1](jusqu''à 5)    [voie_petitionnaire_1](jusqu''à 5)    
[complement_petitionnaire_1](jusqu''à 5)
[lieu_dit_petitionnaire_1](jusqu''à 5)    [bp_petitionnaire_1](jusqu''à 5)
[code_postal_petitionnaire_1](jusqu''à 5)    [localite_petitionnaire_1](jusqu''à 5)    
[cedex_petitionnaire_1](jusqu''à 5)
[pays_petitionnaire_1](jusqu''à 5)

-- Coordonnées du délégataire
[nom_delegataire]
[civilite_delegataire]
[nom_particulier_delegataire]
[prenom_particulier_delegataire]
[raison_sociale_delegataire]
[denomination_delegataire]
[numero_delegataire]    [voie_delegataire]    [complement_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire]

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[civilite_correspondant]
[nom_particulier_correspondant]
[prenom_particulier_correspondant]
[raison_sociale_correspondant]
[denomination_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant]

-- Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_completude]
[date_dernier_depot]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]

-- Données générales des données techniques
[co_projet_desc_donnees_techniques]    [am_projet_desc_donnees_techniques]
[dm_projet_desc_donnees_techniques]    [ope_proj_desc_donnees_techniques]
[projet_desc_donnees_techniques]
[co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
-- Les données techniques suivantes concernent le tableau des surfaces
-- Elles récupèrent les valeurs du tableau composé des sous-destinations si au
-- moins une valeur de celui-ci est saisie
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]
[su_tot_shon_tot_donnees_techniques]

--Données techniques des AT
[at_type_travaux]
[at_effectif_public_total]
[at_categorie_etablissement]
[at_type_etablissement]
'
WHERE code = 'dossier';

-- om_requete rapport_instruction
UPDATE om_requete
SET requete = '
SELECT

    --Données générales du rapport d''instruction
    rapport_instruction.dossier_instruction as dossier_instruction_rapport_instruction, 
    analyse_reglementaire_om_html as analyse_reglementaire_rapport_instruction,
    description_projet_om_html as description_projet_rapport_instruction,
    proposition_decision as proposition_decision_rapport_instruction, 

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier, 
    dossier.dossier as code_barres_dossier,
    etat.libelle as etat_dossier,
    pos as pos_dossier, 
    servitude as servitude_dossier, 
    dossier.delai as delai_dossier,
    dossier_autorisation_type_detaille.libelle as libelle_datd, 
    avis_decision.libelle as libelle_avis_decision,

    --Adresse du terrain dossier d''instruction
    terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    terrain_adresse_localite as terrain_adresse_localite_dossier,
    terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    terrain_adresse_bp as terrain_adresse_bp_dossier,
    terrain_adresse_cedex as terrain_adresse_cedex_dossier,
    terrain_superficie as terrain_superficie_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,

    --Taxe d''aménagement du dossier d''instruction
    CASE
        WHEN tax_secteur = 1 THEN taxe_amenagement.tx_comm_secteur_1
        WHEN tax_secteur = 2 THEN taxe_amenagement.tx_comm_secteur_2
        WHEN tax_secteur = 3 THEN taxe_amenagement.tx_comm_secteur_3
        WHEN tax_secteur = 4 THEN taxe_amenagement.tx_comm_secteur_4
        WHEN tax_secteur = 5 THEN taxe_amenagement.tx_comm_secteur_5
        WHEN tax_secteur = 6 THEN taxe_amenagement.tx_comm_secteur_6
        WHEN tax_secteur = 7 THEN taxe_amenagement.tx_comm_secteur_7
        WHEN tax_secteur = 8 THEN taxe_amenagement.tx_comm_secteur_8
        WHEN tax_secteur = 9 THEN taxe_amenagement.tx_comm_secteur_9
        WHEN tax_secteur = 10 THEN taxe_amenagement.tx_comm_secteur_10
        WHEN tax_secteur = 11 THEN taxe_amenagement.tx_comm_secteur_11
        WHEN tax_secteur = 12 THEN taxe_amenagement.tx_comm_secteur_12
        WHEN tax_secteur = 13 THEN taxe_amenagement.tx_comm_secteur_13
        WHEN tax_secteur = 14 THEN taxe_amenagement.tx_comm_secteur_14
        WHEN tax_secteur = 15 THEN taxe_amenagement.tx_comm_secteur_15
        WHEN tax_secteur = 16 THEN taxe_amenagement.tx_comm_secteur_16
        WHEN tax_secteur = 17 THEN taxe_amenagement.tx_comm_secteur_17
        WHEN tax_secteur = 18 THEN taxe_amenagement.tx_comm_secteur_18
        WHEN tax_secteur = 19 THEN taxe_amenagement.tx_comm_secteur_19
        WHEN tax_secteur = 20 THEN taxe_amenagement.tx_comm_secteur_20
    END as tax_taux_secteur,
    dossier.tax_secteur as tax_numero_secteur,
    dossier.tax_mtn_part_commu as tax_montant_part_communale,
    dossier.tax_mtn_part_depart as tax_montant_part_departementale,
    dossier.tax_mtn_part_reg as tax_montant_part_regionale,
    dossier.tax_mtn_total as tax_montant_total,
    
    --Coordonnées du demandeur
    civilite.code as code_civilite,
    demandeur.particulier_nom as particulier_nom_demandeur,
    demandeur.particulier_prenom as particulier_prenom_demandeur,
    demandeur.personne_morale_denomination as personne_morale_denomination_demandeur,
    demandeur.personne_morale_raison_sociale as personne_morale_raison_sociale_demandeur,
    demandeur.personne_morale_siret as personne_morale_siret_demandeur,
    demandeur.personne_morale_nom as personne_morale_nom_demandeur,
    demandeur.personne_morale_prenom as personne_morale_prenom_demandeur,
    demandeur.numero as numero_demandeur,
    demandeur.voie as voie_demandeur,
    demandeur.complement as complement_demandeur,
    demandeur.lieu_dit as lieu_dit_demandeur,
    demandeur.localite as localite_demandeur,
    demandeur.code_postal as code_postal_demandeur,
    demandeur.bp as bp_demandeur,
    demandeur.cedex as cedex_demandeur,

    -- Coordonnées du pétitionnaire principal
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
    END as nom_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_principal_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_nom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_denomination
    END as denomination_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_1_civilite.libelle, petitionnaire_1.particulier_nom, petitionnaire_1.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_1.personne_morale_raison_sociale, petitionnaire_1.personne_morale_denomination, ''représenté(e) par'', petitionnaire_1_civilite.libelle, petitionnaire_1.personne_morale_nom, petitionnaire_1.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
            END
    END as nom_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier'' OR petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_1_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_nom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL
                THEN petitionnaire_1.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_1.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_denomination
    END as denomination_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_2_civilite.libelle, petitionnaire_2.particulier_nom, petitionnaire_2.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_2.personne_morale_raison_sociale, petitionnaire_2.personne_morale_denomination, ''représenté(e) par'', petitionnaire_2_civilite.libelle, petitionnaire_2.personne_morale_nom, petitionnaire_2.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
            END
    END as nom_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier'' OR petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_2_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_nom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL
                THEN petitionnaire_2.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_2.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_denomination
    END as denomination_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_3_civilite.libelle, petitionnaire_3.particulier_nom, petitionnaire_3.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_3.personne_morale_raison_sociale, petitionnaire_3.personne_morale_denomination, ''représenté(e) par'', petitionnaire_3_civilite.libelle, petitionnaire_3.personne_morale_nom, petitionnaire_3.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
            END
    END as nom_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier'' OR petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_3_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_nom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL
                THEN petitionnaire_3.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_3.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_denomination
    END as denomination_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_4_civilite.libelle, petitionnaire_4.particulier_nom, petitionnaire_4.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_4.personne_morale_raison_sociale, petitionnaire_4.personne_morale_denomination, ''représenté(e) par'', petitionnaire_4_civilite.libelle, petitionnaire_4.personne_morale_nom, petitionnaire_4.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
            END
    END as nom_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier'' OR petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_4_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_nom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL
                THEN petitionnaire_4.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_4.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_denomination
    END as denomination_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_5_civilite.libelle, petitionnaire_5.particulier_nom, petitionnaire_5.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_5.personne_morale_raison_sociale, petitionnaire_5.personne_morale_denomination, ''représenté(e) par'', petitionnaire_5_civilite.libelle, petitionnaire_5.personne_morale_nom, petitionnaire_5.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
            END
    END as nom_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier'' OR petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_5_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_nom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL
                THEN petitionnaire_5.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_5.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_denomination
    END as denomination_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,
    
    --Nom de l''instructeur
    instructeur.nom as nom_instructeur, 

    --Noms des signataires
    division.chef as chef_division,
    direction.chef as chef_direction,
    direction.libelle as libelle_direction,
    direction.description as description_direction,

    -- Données générales des données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.ope_proj_desc as ope_proj_desc_donnees_techniques,
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_cstr_shon_tot
        ELSE donnees_techniques.su_cstr_shon_tot
    END as su_cstr_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_demo_shon_tot
        ELSE donnees_techniques.su_demo_shon_tot
    END as su_demo_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN
            REGEXP_REPLACE(CONCAT(
                CASE WHEN donnees_techniques.su2_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation agricole - '', donnees_techniques.su2_cstr_shon1, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation forestière - '', donnees_techniques.su2_cstr_shon2, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Logement - '', donnees_techniques.su2_cstr_shon3, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement - '', donnees_techniques.su2_cstr_shon4, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Artisanat et commerce de détail - '', donnees_techniques.su2_cstr_shon5, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Restauration - '', donnees_techniques.su2_cstr_shon6, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Commerce de gros - '', donnees_techniques.su2_cstr_shon7, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Activités de services où s''''effectue l''''accueil d''''une clientèle - '', donnees_techniques.su2_cstr_shon8, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement hôtelier et touristique - '', donnees_techniques.su2_cstr_shon9, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon10 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Cinéma - '', donnees_techniques.su2_cstr_shon10, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon11 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux et bureaux accueillant du public des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon11, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon12 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux techniques et industriels des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon12, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon13 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Établissements d''''enseignement, de santé et d''''action sociale - '', donnees_techniques.su2_cstr_shon13, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon14 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Salles d''''art et de spectacles - '', donnees_techniques.su2_cstr_shon14, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon15 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Équipements sportifs - '', donnees_techniques.su2_cstr_shon15, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon16 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Autres équipements recevant du public - '', donnees_techniques.su2_cstr_shon16, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon17 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Industrie - '', donnees_techniques.su2_cstr_shon17, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon18 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Entrepôt - '', donnees_techniques.su2_cstr_shon18, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon19 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Bureau - '', donnees_techniques.su2_cstr_shon19, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon20 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Centre de congrès et d''''exposition - '', donnees_techniques.su2_cstr_shon20, '' m²'')
                END
            ), '' / $'', '''')
        ELSE
            REGEXP_REPLACE(CONCAT(
                CASE
                    WHEN donnees_techniques.su_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
                END, 
                CASE
                    WHEN donnees_techniques.su_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
                END
            ), '' / $'', '''')
    END as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_tot_shon_tot
        ELSE donnees_techniques.su_tot_shon_tot
    END as su_tot_shon_tot_donnees_techniques

FROM

    &DB_PREFIXErapport_instruction 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=rapport_instruction.dossier_instruction 
    LEFT JOIN &DB_PREFIXEavis_decision 
        ON dossier.avis_decision = avis_decision.avis_decision
    LEFT JOIN &DB_PREFIXEetat
        ON dossier.etat = etat.etat
    LEFT JOIN &DB_PREFIXEdonnees_techniques
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdivision
        ON dossier.division = division.division
    LEFT JOIN &DB_PREFIXEdirection
        ON division.direction = direction.direction
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_principal
        ON lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_principal_civilite
        ON petitionnaire_principal.personne_morale_civilite=petitionnaire_principal_civilite.civilite OR petitionnaire_principal.particulier_civilite=petitionnaire_principal_civilite.civilite
    LEFT JOIN (
        SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
        FROM &DB_PREFIXElien_dossier_demandeur
        LEFT JOIN &DB_PREFIXEdossier
            ON lien_dossier_demandeur.dossier=dossier.dossier 
            AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
        LEFT JOIN &DB_PREFIXErapport_instruction
            ON rapport_instruction.dossier_instruction = dossier.dossier
        WHERE rapport_instruction.rapport_instruction = &idx
        GROUP BY lien_dossier_demandeur.dossier
    ) as sub_petitionnaire_autre
    ON rapport_instruction.dossier_instruction = sub_petitionnaire_autre.dossier
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1] AND petitionnaire_1.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_1_civilite
    ON
        petitionnaire_1.particulier_civilite = petitionnaire_1_civilite.civilite OR petitionnaire_1.personne_morale_civilite = petitionnaire_1_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2] AND petitionnaire_2.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_2_civilite
    ON
        petitionnaire_2.particulier_civilite = petitionnaire_2_civilite.civilite OR petitionnaire_2.personne_morale_civilite = petitionnaire_2_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3] AND petitionnaire_3.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_3_civilite
    ON
        petitionnaire_3.particulier_civilite = petitionnaire_3_civilite.civilite OR petitionnaire_3.personne_morale_civilite = petitionnaire_3_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4] AND petitionnaire_4.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_4_civilite
    ON
        petitionnaire_4.particulier_civilite = petitionnaire_4_civilite.civilite OR petitionnaire_4.personne_morale_civilite = petitionnaire_4_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5] AND petitionnaire_5.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_5_civilite
    ON
        petitionnaire_5.particulier_civilite = petitionnaire_5_civilite.civilite OR petitionnaire_5.personne_morale_civilite = petitionnaire_5_civilite.civilite
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
    LEFT JOIN
        &DB_PREFIXEtaxe_amenagement
        ON
            dossier.om_collectivite = taxe_amenagement.om_collectivite
WHERE rapport_instruction = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
',
merge_fields = '
--Données générales du rapport d''instruction
[dossier_instruction_rapport_instruction]    [analyse_reglementaire_rapport_instruction]
[description_projet_rapport_instruction]    [proposition_decision_rapport_instruction]

--Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]    [etat_dossier]
[pos_dossier]    [servitude_dossier]
[delai_dossier]    [libelle_datd]
[libelle_avis_decision]

--Adresse du terrain dossier d''instruction
[terrain_adresse_voie_numero_dossier]     [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_cedex_dossier]    [terrain_superficie_dossier]
[terrain_references_cadastrales_dossier]

--Taxe d''aménagement du dossier d''instruction
[tax_taux_secteur]
[tax_numero_secteur]
[tax_montant_part_communale]
[tax_montant_part_departementale]
[tax_montant_part_regionale]
[tax_montant_total]

--Coordonnées du demandeur
[code_civilite]
[particulier_nom_demandeur]    [particulier_prenom_demandeur]    
[personne_morale_denomination_demandeur]    [personne_morale_raison_sociale_demandeur]    [personne_morale_siret_demandeur]
[personne_morale_nom_demandeur]    [personne_morale_prenom_demandeur]
[numero_demandeur]    [voie_demandeur]
[complement_demandeur]    [lieu_dit_demandeur]
[code_postal_demandeur]    [localite_demandeur]   [bp_demandeur]    [cedex_demandeur]

--Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[civilite_petitionnaire_principal]
[nom_particulier_petitionnaire_principal]
[prenom_particulier_petitionnaire_principal]
[raison_sociale_petitionnaire_principal]
[denomination_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''à 5)
[civilite_petitionnaire_1](jusqu''à 5)
[nom_particulier_petitionnaire_1](jusqu''à 5)
[prenom_particulier_petitionnaire_1](jusqu''à 5)
[raison_sociale_petitionnaire_1](jusqu''à 5)
[denomination_petitionnaire_1](jusqu''à 5)
[numero_petitionnaire_1](jusqu''à 5)    [voie_petitionnaire_1](jusqu''à 5)    
[complement_petitionnaire_1](jusqu''à 5)
[lieu_dit_petitionnaire_1](jusqu''à 5)    [bp_petitionnaire_1](jusqu''à 5)
[code_postal_petitionnaire_1](jusqu''à 5)    [localite_petitionnaire_1](jusqu''à 5)    
[cedex_petitionnaire_1](jusqu''à 5)
[pays_petitionnaire_1](jusqu''à 5)

--Nom de l''instructeur
[nom_instructeur]

--Noms des signataires
[chef_division]
[chef_direction]
[libelle_direction]
[description_direction]

-- Données générales des données techniques
[co_projet_desc_donnees_techniques]    [am_projet_desc_donnees_techniques]
[dm_projet_desc_donnees_techniques]    [ope_proj_desc_donnees_techniques]
[projet_desc_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]    
[co_tot_log_nb_donnees_techniques]     [co_statio_place_nb_donnees_techniques]
-- Les données techniques suivantes concernent le tableau des surfaces
-- Elles récupèrent les valeurs du tableau composé des sous-destinations si au
-- moins une valeur de celui-ci est saisie
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]    
[tab_surface_donnees_techniques]
[su_tot_shon_tot_donnees_techniques]
'
WHERE code = 'rapport_instruction';

-- om_requete consultation
UPDATE om_requete
SET requete = '
SELECT 
    --Coordonnées du service
    service.libelle as libelle_service, 
    service.adresse as adresse_service, 
    service.adresse2 as adresse2_service, 
    service.cp as cp_service, 
    service.ville as ville_service,
    service.delai as delai_service,
    CASE WHEN LOWER(service.delai_type) = LOWER(''jour'')
        THEN ''jour(s)''
        ELSE ''mois''
    END as delai_type_service,

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    dossier.dossier as code_barres_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    dossier_autorisation_type_detaille.libelle as libelle_datd, 

    --Adresse du terrain du dossier d''instruction
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,

    --Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as tel_instructeur,
    instructeur_utilisateur.email as email_instructeur,

    --Coordonnées du demandeur
    civilite.libelle as civilite_demandeur,
    CASE WHEN demandeur.qualite=''particulier''
        THEN TRIM(CONCAT(demandeur.particulier_nom, '' '', demandeur.particulier_prenom))
        ELSE TRIM(CONCAT(demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_denomination))
    END as nom_demandeur,
    CONCAT(demandeur.numero, '' '', demandeur.voie) as adresse_demandeur,
    demandeur.complement as complement_adresse_demandeur,
    demandeur.lieu_dit as lieu_dit_demandeur,
    demandeur.code_postal as code_postal_demandeur,
    demandeur.localite as ville_demandeur,
    demandeur.personne_morale_denomination as societe_demandeur,

    -- Coordonnées du pétitionnaire principal
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
    END as nom_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_principal_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_nom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_denomination
    END as denomination_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Dates importantes du dossier d''instruction
    to_char(dossier.date_depot,''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_complet, ''DD/MM/YYYY'') as date_completude,
    to_char(dossier.date_dernier_depot, ''DD/MM/YYYY'') as date_dernier_depot,
    to_char(dossier.date_rejet,''DD/MM/YYYY'') as date_rejet_dossier,
    CASE WHEN dossier.incomplet_notifie IS TRUE AND dossier.incompletude IS TRUE
        THEN to_char(dossier.date_limite_incompletude ,''DD/MM/YYYY'')
        ELSE to_char(dossier.date_limite ,''DD/MM/YYYY'')
    END as date_limite_dossier,
    to_char(consultation.date_envoi,''DD/MM/YYYY'') as date_envoi_dossier,
    to_char(instruction.date_evenement,''DD/MM/YYYY'') as date_evenement,
    dossier.delai as delai_limite_decision,

    --Code barres de la consultation
    consultation.code_barres as code_barres_consultation,

    --Données générales des données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.ope_proj_desc as ope_proj_desc_donnees_techniques,
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_cstr_shon_tot
        ELSE donnees_techniques.su_cstr_shon_tot
    END as su_cstr_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_demo_shon_tot
        ELSE donnees_techniques.su_demo_shon_tot
    END as su_demo_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN
            REGEXP_REPLACE(CONCAT(
                CASE WHEN donnees_techniques.su2_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation agricole - '', donnees_techniques.su2_cstr_shon1, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation forestière - '', donnees_techniques.su2_cstr_shon2, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Logement - '', donnees_techniques.su2_cstr_shon3, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement - '', donnees_techniques.su2_cstr_shon4, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Artisanat et commerce de détail - '', donnees_techniques.su2_cstr_shon5, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Restauration - '', donnees_techniques.su2_cstr_shon6, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Commerce de gros - '', donnees_techniques.su2_cstr_shon7, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Activités de services où s''''effectue l''''accueil d''''une clientèle - '', donnees_techniques.su2_cstr_shon8, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement hôtelier et touristique - '', donnees_techniques.su2_cstr_shon9, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon10 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Cinéma - '', donnees_techniques.su2_cstr_shon10, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon11 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux et bureaux accueillant du public des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon11, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon12 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux techniques et industriels des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon12, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon13 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Établissements d''''enseignement, de santé et d''''action sociale - '', donnees_techniques.su2_cstr_shon13, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon14 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Salles d''''art et de spectacles - '', donnees_techniques.su2_cstr_shon14, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon15 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Équipements sportifs - '', donnees_techniques.su2_cstr_shon15, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon16 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Autres équipements recevant du public - '', donnees_techniques.su2_cstr_shon16, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon17 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Industrie - '', donnees_techniques.su2_cstr_shon17, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon18 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Entrepôt - '', donnees_techniques.su2_cstr_shon18, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon19 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Bureau - '', donnees_techniques.su2_cstr_shon19, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon20 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Centre de congrès et d''''exposition - '', donnees_techniques.su2_cstr_shon20, '' m²'')
                END
            ), '' / $'', '''')
        ELSE
            REGEXP_REPLACE(CONCAT(
                CASE
                    WHEN donnees_techniques.su_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
                END, 
                CASE
                    WHEN donnees_techniques.su_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
                END
            ), '' / $'', '''')
    END as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_tot_shon_tot
        ELSE donnees_techniques.su_tot_shon_tot
    END as su_tot_shon_tot_donnees_techniques,

    -- Données techniques pour les AT
    CONCAT_WS('', '',
        CASE WHEN donnees_techniques.erp_cstr_neuve IS TRUE THEN ''construction neuve'' END,
        CASE WHEN donnees_techniques.erp_trvx_acc IS TRUE THEN ''travaux de mise en conformité totale aux règles d’accessibilité'' END,
        CASE WHEN donnees_techniques.erp_extension IS TRUE THEN ''extension'' END,
        CASE WHEN donnees_techniques.erp_rehab IS TRUE THEN ''réhabilitation'' END,
        CASE WHEN donnees_techniques.erp_trvx_am IS TRUE THEN ''travaux d’aménagement (remplacement de revêtements, rénovation électrique, création d’une rampe, par exemple)'' END,
        CASE WHEN donnees_techniques.erp_vol_nouv_exist IS TRUE THEN ''création de volumes nouveaux dans des volumes existants (modification du cloisonnement, par exemple)'' END
    ) as at_type_travaux,
    donnees_techniques.erp_public_eff_tot as at_effectif_public_total,
    CONCAT_WS('' - '', erp_categorie.libelle, erp_categorie.description) as at_categorie_etablissement,
    CONCAT_WS('' - '', erp_type.libelle, erp_type.description) as at_type_etablissement
    
FROM 

    &DB_PREFIXEconsultation 
    LEFT JOIN &DB_PREFIXEservice 
        ON service.service=consultation.service 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=consultation.dossier 
    LEFT JOIN &DB_PREFIXEdonnees_techniques 
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdossier_autorisation 
        ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation 
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur
    LEFT JOIN &DB_PREFIXEom_utilisateur as instructeur_utilisateur
        ON instructeur_utilisateur.om_utilisateur=instructeur.om_utilisateur
    LEFT JOIN &DB_PREFIXEinstruction
        ON dossier.dossier=instruction.dossier 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite
    LEFT JOIN &DB_PREFIXEdemandeur as petitionnaire_principal
        ON lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    LEFT JOIN &DB_PREFIXEcivilite as petitionnaire_principal_civilite
        ON petitionnaire_principal.personne_morale_civilite=petitionnaire_principal_civilite.civilite OR petitionnaire_principal.particulier_civilite=petitionnaire_principal_civilite.civilite
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
    LEFT JOIN
        &DB_PREFIXEerp_categorie
        ON
            donnees_techniques.erp_class_cat = erp_categorie.erp_categorie
    LEFT JOIN
        &DB_PREFIXEerp_type
        ON
            donnees_techniques.erp_class_type = erp_type.erp_type
WHERE consultation.consultation = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
',
merge_fields = '
--Coordonnées du service
[libelle_service] 
[adresse_service] 
[adresse2_service] 
[cp_service]    [ville_service]
[delai_service]
[delai_type_service]

--Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]
[terrain_references_cadastrales_dossier]
[libelle_datd] 

--Adresse du terrain du dossier d''instruction
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]     [terrain_adresse_code_postal_dossier]
[terrain_adresse_localite_dossier]

--Coordonnées de l''instructeur
[nom_instructeur]
[tel_instructeur]
[email_instructeur]

--Coordonnées du demandeur
[civilite_demandeur]    [nom_demandeur]
[adresse_demandeur]
[complement_adresse_demandeur]    [lieu_dit_demandeur]
[code_postal_demandeur]    [ville_demandeur]
[societe_demandeur]

--Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[civilite_petitionnaire_principal]
[nom_particulier_petitionnaire_principal]
[prenom_particulier_petitionnaire_principal]
[raison_sociale_petitionnaire_principal]
[denomination_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_completude]
[date_dernier_depot]
[date_rejet_dossier]
[date_limite_dossier]
[date_envoi_dossier]
[date_evenement]
[delai_limite_decision]

--Code barres de la consultation
[code_barres_consultation]

--Données générales des données techniques
[co_projet_desc_donnees_techniques]    [am_projet_desc_donnees_techniques]
[dm_projet_desc_donnees_techniques]    [ope_proj_desc_donnees_techniques]
[projet_desc_donnees_techniques]
[co_tot_log_nb_donnees_techniques]    [co_station_place_nb_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
-- Les données techniques suivantes concernent le tableau des surfaces
-- Elles récupèrent les valeurs du tableau composé des sous-destinations si au
-- moins une valeur de celui-ci est saisie
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]
[su_tot_shon_tot_donnees_techniques]

--Données techniques des AT
[at_type_travaux]
[at_effectif_public_total]
[at_categorie_etablissement]
[at_type_etablissement]
'
WHERE code = 'consultation';

-- 
-- END - [#8452] Amélioration des champs de fusion
--

--
-- START - TI#607 — Ajouter les droits manquants aux profils de Marseille pour le migration sur le trunk 
--

--
-- Profil Chef de service
--
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_geolocalisation_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_modifier_bypass',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );

-- Profil Administrateur technique et fonctionnel
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'commission_mes_retours',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'commission_mes_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'commission_demandes_passage',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'commission_demandes_passage' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'commission_demandes_passage_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'commission_demandes_passage_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL')
    );

--
-- Profil Cellule suivi
--
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'menu_instruction',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'menu_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_donnees_techniques_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );

DELETE FROM om_droit WHERE libelle = 'dossier_commission' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI');

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'blocnote',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'blocnote' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );

--
-- Droits manquants profil qualificateur
-- 
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'lot',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'lot' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'commission',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'commission' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'commission_demandes_passage',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'commission_demandes_passage' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'commission_demandes_passage_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'commission_demandes_passage_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'commission_type_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'commission_type_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'blocnote',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'blocnote' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'sitadel_export_sitadel',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'sitadel_export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'export_sitadel',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'versement_archives',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'versement_archives' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'consultation_suivi_mise_a_jour_des_dates',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_suivi_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'consultation_suivi_retours_de_consultation',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_suivi_retours_de_consultation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'menu_suivi',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'menu_suivi' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_suivi_mise_a_jour_des_dates',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_suivi_mise_a_jour_des_dates_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_mise_a_jour_des_dates_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_suivi_envoi_lettre_rar',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_envoi_lettre_rar' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_suivi_bordereaux',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_bordereaux' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_bordereau_envoi_maire',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_bordereau_envoi_maire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'menu_instruction',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'menu_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_ajouter_instruction',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_ajouter_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_modifier_instruction',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_modifier_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_modifier_lu',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_modifier_lu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_supprimer_instruction',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_supprimer_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_commission_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'petitionnaire_frequent',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
-- SUPPRESSION DES DROITS SUR LES PIECES AUTRES QUE CONSULTER SI PROFIL NON ADMIN
DELETE FROM om_droit
WHERE libelle IN (
    'document_numerise_ajouter_bypass',
    'document_numerise_modifier_bypass',
    'document_numerise_supprimer_bypass',
    'document_numerise_ajouter',
    'document_numerise_modifier',
    'document_numerise_supprimer')
AND om_profil IN (
    (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'),
    (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'),
    (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'),
    (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE')
);

--
-- END - TI#607 — Ajouter les droits manquants aux profils de Marseille pour le migration sur le trunk 
--


--
-- Droits manquants profil service consulté interne
-- 
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'menu_instruction',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'menu_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_document_numerise',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE')
    );

--
-- Droits manquants profil instructeur
-- 
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'architecte_frequent',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'architecte_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );

-- Droits obsolètes qui étaient utilisés par le script app/
DELETE FROM om_droit WHERE libelle = 'rapport_instruction_rediger';
DELETE FROM om_droit WHERE libelle = 'rapport_instruction_rediger_bypass';

--
-- Droit manquant profil chef de service
-- 
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_rapport_instruction_rediger',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );

--
-- BEGIN - cleanup old openfoncier
--

--
DROP TABLE parcelle CASCADE;
--DROP SEQUENCE parcelle_lot_seq CASCADE;
DROP TABLE proprietaire CASCADE;
--DROP SEQUENCE proprietaire_seq CASCADE;

DROP TABLE servitude_ligne CASCADE;
--DROP SEQUENCE servitude_ligne_seq CASCADE;
DROP TABLE servitude_point CASCADE;
--DROP SEQUENCE servitude_point_seq CASCADE;
DROP TABLE servitude_surfacique CASCADE;
--DROP SEQUENCE servitude_surfacique_seq CASCADE;
DROP TABLE parcelle_lot CASCADE;
--DROP SEQUENCE parcelle_lot_seq CASCADE;
DROP TABLE pos CASCADE;
--DROP SEQUENCE parcelle_lot_seq CASCADE;
DROP TABLE rivoli CASCADE;
--
--ALTER TABLE parcelle DROP COLUMN pos;
--
ALTER TABLE dossier DROP COLUMN servitude;
ALTER TABLE dossier DROP COLUMN pos;
ALTER TABLE dossier DROP COLUMN parcelle;
ALTER TABLE dossier DROP COLUMN parcelle_lot;
ALTER TABLE dossier DROP COLUMN lot;
ALTER TABLE dossier DROP COLUMN sig;
ALTER TABLE dossier DROP COLUMN parcelle_lot_lotissement;
ALTER TABLE dossier DROP COLUMN batiment_nombre;
ALTER TABLE dossier DROP COLUMN logement_nombre;
ALTER TABLE dossier DROP COLUMN shon;
ALTER TABLE dossier DROP COLUMN shon_calcul;
ALTER TABLE dossier DROP COLUMN shob;
ALTER TABLE dossier DROP COLUMN hauteur;
ALTER TABLE dossier DROP COLUMN piece_nombre;
ALTER TABLE dossier DROP COLUMN amenagement;
ALTER TABLE dossier DROP COLUMN temp1;
ALTER TABLE dossier DROP COLUMN temp2;
ALTER TABLE dossier DROP COLUMN temp3;
ALTER TABLE dossier DROP COLUMN temp4;
ALTER TABLE dossier DROP COLUMN temp5;
--

-- om_requete rapport_instruction
UPDATE om_requete
SET requete = '
SELECT

    --Données générales du rapport d''instruction
    rapport_instruction.dossier_instruction as dossier_instruction_rapport_instruction, 
    analyse_reglementaire_om_html as analyse_reglementaire_rapport_instruction,
    description_projet_om_html as description_projet_rapport_instruction,
    proposition_decision as proposition_decision_rapport_instruction, 

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier, 
    dossier.dossier as code_barres_dossier,
    etat.libelle as etat_dossier,
    dossier.delai as delai_dossier,
    dossier_autorisation_type_detaille.libelle as libelle_datd, 
    avis_decision.libelle as libelle_avis_decision,

    --Adresse du terrain dossier d''instruction
    terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    terrain_adresse_localite as terrain_adresse_localite_dossier,
    terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    terrain_adresse_bp as terrain_adresse_bp_dossier,
    terrain_adresse_cedex as terrain_adresse_cedex_dossier,
    terrain_superficie as terrain_superficie_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,

    --Taxe d''aménagement du dossier d''instruction
    CASE
        WHEN tax_secteur = 1 THEN taxe_amenagement.tx_comm_secteur_1
        WHEN tax_secteur = 2 THEN taxe_amenagement.tx_comm_secteur_2
        WHEN tax_secteur = 3 THEN taxe_amenagement.tx_comm_secteur_3
        WHEN tax_secteur = 4 THEN taxe_amenagement.tx_comm_secteur_4
        WHEN tax_secteur = 5 THEN taxe_amenagement.tx_comm_secteur_5
        WHEN tax_secteur = 6 THEN taxe_amenagement.tx_comm_secteur_6
        WHEN tax_secteur = 7 THEN taxe_amenagement.tx_comm_secteur_7
        WHEN tax_secteur = 8 THEN taxe_amenagement.tx_comm_secteur_8
        WHEN tax_secteur = 9 THEN taxe_amenagement.tx_comm_secteur_9
        WHEN tax_secteur = 10 THEN taxe_amenagement.tx_comm_secteur_10
        WHEN tax_secteur = 11 THEN taxe_amenagement.tx_comm_secteur_11
        WHEN tax_secteur = 12 THEN taxe_amenagement.tx_comm_secteur_12
        WHEN tax_secteur = 13 THEN taxe_amenagement.tx_comm_secteur_13
        WHEN tax_secteur = 14 THEN taxe_amenagement.tx_comm_secteur_14
        WHEN tax_secteur = 15 THEN taxe_amenagement.tx_comm_secteur_15
        WHEN tax_secteur = 16 THEN taxe_amenagement.tx_comm_secteur_16
        WHEN tax_secteur = 17 THEN taxe_amenagement.tx_comm_secteur_17
        WHEN tax_secteur = 18 THEN taxe_amenagement.tx_comm_secteur_18
        WHEN tax_secteur = 19 THEN taxe_amenagement.tx_comm_secteur_19
        WHEN tax_secteur = 20 THEN taxe_amenagement.tx_comm_secteur_20
    END as tax_taux_secteur,
    dossier.tax_secteur as tax_numero_secteur,
    dossier.tax_mtn_part_commu as tax_montant_part_communale,
    dossier.tax_mtn_part_depart as tax_montant_part_departementale,
    dossier.tax_mtn_part_reg as tax_montant_part_regionale,
    dossier.tax_mtn_total as tax_montant_total,
    
    --Coordonnées du demandeur
    civilite.code as code_civilite,
    demandeur.particulier_nom as particulier_nom_demandeur,
    demandeur.particulier_prenom as particulier_prenom_demandeur,
    demandeur.personne_morale_denomination as personne_morale_denomination_demandeur,
    demandeur.personne_morale_raison_sociale as personne_morale_raison_sociale_demandeur,
    demandeur.personne_morale_siret as personne_morale_siret_demandeur,
    demandeur.personne_morale_nom as personne_morale_nom_demandeur,
    demandeur.personne_morale_prenom as personne_morale_prenom_demandeur,
    demandeur.numero as numero_demandeur,
    demandeur.voie as voie_demandeur,
    demandeur.complement as complement_demandeur,
    demandeur.lieu_dit as lieu_dit_demandeur,
    demandeur.localite as localite_demandeur,
    demandeur.code_postal as code_postal_demandeur,
    demandeur.bp as bp_demandeur,
    demandeur.cedex as cedex_demandeur,

    -- Coordonnées du pétitionnaire principal
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_principal.personne_morale_raison_sociale, petitionnaire_principal.personne_morale_denomination, ''représenté(e) par'', petitionnaire_principal_civilite.libelle, petitionnaire_principal.personne_morale_nom, petitionnaire_principal.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
    END as nom_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier'' OR petitionnaire_principal.personne_morale_nom IS NOT NULL OR petitionnaire_principal.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_principal_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_nom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_nom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN petitionnaire_principal.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_principal.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_principal.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_principal,
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_principal.personne_morale_denomination
    END as denomination_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_1_civilite.libelle, petitionnaire_1.particulier_nom, petitionnaire_1.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_1.personne_morale_raison_sociale, petitionnaire_1.personne_morale_denomination, ''représenté(e) par'', petitionnaire_1_civilite.libelle, petitionnaire_1.personne_morale_nom, petitionnaire_1.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
            END
    END as nom_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier'' OR petitionnaire_1.personne_morale_nom IS NOT NULL OR petitionnaire_1.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_1_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_nom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_nom IS NOT NULL
                THEN petitionnaire_1.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN petitionnaire_1.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_1.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_1.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_1,
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_1.personne_morale_denomination
    END as denomination_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_2_civilite.libelle, petitionnaire_2.particulier_nom, petitionnaire_2.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_2.personne_morale_raison_sociale, petitionnaire_2.personne_morale_denomination, ''représenté(e) par'', petitionnaire_2_civilite.libelle, petitionnaire_2.personne_morale_nom, petitionnaire_2.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
            END
    END as nom_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier'' OR petitionnaire_2.personne_morale_nom IS NOT NULL OR petitionnaire_2.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_2_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_nom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_nom IS NOT NULL
                THEN petitionnaire_2.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN petitionnaire_2.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_2.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_2.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_2,
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_2.personne_morale_denomination
    END as denomination_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_3_civilite.libelle, petitionnaire_3.particulier_nom, petitionnaire_3.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_3.personne_morale_raison_sociale, petitionnaire_3.personne_morale_denomination, ''représenté(e) par'', petitionnaire_3_civilite.libelle, petitionnaire_3.personne_morale_nom, petitionnaire_3.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
            END
    END as nom_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier'' OR petitionnaire_3.personne_morale_nom IS NOT NULL OR petitionnaire_3.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_3_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_nom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_nom IS NOT NULL
                THEN petitionnaire_3.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN petitionnaire_3.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_3.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_3.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_3,
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_3.personne_morale_denomination
    END as denomination_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_4_civilite.libelle, petitionnaire_4.particulier_nom, petitionnaire_4.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_4.personne_morale_raison_sociale, petitionnaire_4.personne_morale_denomination, ''représenté(e) par'', petitionnaire_4_civilite.libelle, petitionnaire_4.personne_morale_nom, petitionnaire_4.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
            END
    END as nom_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier'' OR petitionnaire_4.personne_morale_nom IS NOT NULL OR petitionnaire_4.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_4_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_nom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_nom IS NOT NULL
                THEN petitionnaire_4.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN petitionnaire_4.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_4.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_4.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_4,
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_4.personne_morale_denomination
    END as denomination_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_5_civilite.libelle, petitionnaire_5.particulier_nom, petitionnaire_5.particulier_prenom))
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN TRIM(CONCAT_WS('' '', petitionnaire_5.personne_morale_raison_sociale, petitionnaire_5.personne_morale_denomination, ''représenté(e) par'', petitionnaire_5_civilite.libelle, petitionnaire_5.personne_morale_nom, petitionnaire_5.personne_morale_prenom))
                ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
            END
    END as nom_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier'' OR petitionnaire_5.personne_morale_nom IS NOT NULL OR petitionnaire_5.personne_morale_prenom IS NOT NULL
        THEN petitionnaire_5_civilite.libelle
        ELSE ''''
    END as civilite_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_nom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_nom IS NOT NULL
                THEN petitionnaire_5.personne_morale_nom
                ELSE ''''
            END
    END as nom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN petitionnaire_5.particulier_prenom
        ELSE
            CASE WHEN petitionnaire_5.personne_morale_prenom IS NOT NULL
                THEN petitionnaire_5.personne_morale_prenom
                ELSE ''''
            END
    END as prenom_particulier_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_raison_sociale
    END as raison_sociale_petitionnaire_5,
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN ''''
        ELSE petitionnaire_5.personne_morale_denomination
    END as denomination_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,
    
    --Nom de l''instructeur
    instructeur.nom as nom_instructeur, 

    --Noms des signataires
    division.chef as chef_division,
    direction.chef as chef_direction,
    direction.libelle as libelle_direction,
    direction.description as description_direction,

    --Données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.ope_proj_desc as ope_proj_desc_donnees_techniques,
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_cstr_shon_tot
        ELSE donnees_techniques.su_cstr_shon_tot
    END as su_cstr_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_demo_shon_tot
        ELSE donnees_techniques.su_demo_shon_tot
    END as su_demo_shon_tot_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN
            REGEXP_REPLACE(CONCAT(
                CASE WHEN donnees_techniques.su2_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation agricole - '', donnees_techniques.su2_cstr_shon1, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Exploitation forestière - '', donnees_techniques.su2_cstr_shon2, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Logement - '', donnees_techniques.su2_cstr_shon3, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement - '', donnees_techniques.su2_cstr_shon4, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Artisanat et commerce de détail - '', donnees_techniques.su2_cstr_shon5, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Restauration - '', donnees_techniques.su2_cstr_shon6, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Commerce de gros - '', donnees_techniques.su2_cstr_shon7, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Activités de services où s''''effectue l''''accueil d''''une clientèle - '', donnees_techniques.su2_cstr_shon8, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Hébergement hôtelier et touristique - '', donnees_techniques.su2_cstr_shon9, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon10 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Cinéma - '', donnees_techniques.su2_cstr_shon10, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon11 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux et bureaux accueillant du public des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon11, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon12 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Locaux techniques et industriels des administrations publiques et assimilés - '', donnees_techniques.su2_cstr_shon12, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon13 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Établissements d''''enseignement, de santé et d''''action sociale - '', donnees_techniques.su2_cstr_shon13, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon14 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Salles d''''art et de spectacles - '', donnees_techniques.su2_cstr_shon14, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon15 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Équipements sportifs - '', donnees_techniques.su2_cstr_shon15, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon16 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Autres équipements recevant du public - '', donnees_techniques.su2_cstr_shon16, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon17 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Industrie - '', donnees_techniques.su2_cstr_shon17, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon18 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Entrepôt - '', donnees_techniques.su2_cstr_shon18, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon19 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Bureau - '', donnees_techniques.su2_cstr_shon19, '' m² / '')
                END,
                CASE WHEN donnees_techniques.su2_cstr_shon20 IS NULL
                    THEN ''''
                    ELSE CONCAT (''Centre de congrès et d''''exposition - '', donnees_techniques.su2_cstr_shon20, '' m²'')
                END
            ), '' / $'', '''')
        ELSE
            REGEXP_REPLACE(CONCAT(
                CASE
                    WHEN donnees_techniques.su_cstr_shon1 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon2 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon3 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon4 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon5 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon6 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon7 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
                END,
                CASE
                    WHEN donnees_techniques.su_cstr_shon8 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
                END, 
                CASE
                    WHEN donnees_techniques.su_cstr_shon9 IS NULL
                    THEN ''''
                    ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
                END
            ), '' / $'', '''')
    END as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques,
    -- Si une valeur est saisie dans la deuxième version du tableau des surfaces
    -- alors on récupère seulement ses valeurs
    CASE WHEN su2_avt_shon1 IS NOT NULL
        OR su2_avt_shon2 IS NOT NULL
        OR su2_avt_shon3 IS NOT NULL
        OR su2_avt_shon4 IS NOT NULL
        OR su2_avt_shon5 IS NOT NULL
        OR su2_avt_shon6 IS NOT NULL
        OR su2_avt_shon7 IS NOT NULL
        OR su2_avt_shon8 IS NOT NULL
        OR su2_avt_shon9 IS NOT NULL
        OR su2_avt_shon10 IS NOT NULL
        OR su2_avt_shon11 IS NOT NULL
        OR su2_avt_shon12 IS NOT NULL
        OR su2_avt_shon13 IS NOT NULL
        OR su2_avt_shon14 IS NOT NULL
        OR su2_avt_shon15 IS NOT NULL
        OR su2_avt_shon16 IS NOT NULL
        OR su2_avt_shon17 IS NOT NULL
        OR su2_avt_shon18 IS NOT NULL
        OR su2_avt_shon19 IS NOT NULL
        OR su2_avt_shon20 IS NOT NULL
        OR su2_cstr_shon1 IS NOT NULL
        OR su2_cstr_shon2 IS NOT NULL
        OR su2_cstr_shon3 IS NOT NULL
        OR su2_cstr_shon4 IS NOT NULL
        OR su2_cstr_shon5 IS NOT NULL
        OR su2_cstr_shon6 IS NOT NULL
        OR su2_cstr_shon7 IS NOT NULL
        OR su2_cstr_shon8 IS NOT NULL
        OR su2_cstr_shon9 IS NOT NULL
        OR su2_cstr_shon10 IS NOT NULL
        OR su2_cstr_shon11 IS NOT NULL
        OR su2_cstr_shon12 IS NOT NULL
        OR su2_cstr_shon13 IS NOT NULL
        OR su2_cstr_shon14 IS NOT NULL
        OR su2_cstr_shon15 IS NOT NULL
        OR su2_cstr_shon16 IS NOT NULL
        OR su2_cstr_shon17 IS NOT NULL
        OR su2_cstr_shon18 IS NOT NULL
        OR su2_cstr_shon19 IS NOT NULL
        OR su2_cstr_shon20 IS NOT NULL
        OR su2_chge_shon1 IS NOT NULL
        OR su2_chge_shon2 IS NOT NULL
        OR su2_chge_shon3 IS NOT NULL
        OR su2_chge_shon4 IS NOT NULL
        OR su2_chge_shon5 IS NOT NULL
        OR su2_chge_shon6 IS NOT NULL
        OR su2_chge_shon7 IS NOT NULL
        OR su2_chge_shon8 IS NOT NULL
        OR su2_chge_shon9 IS NOT NULL
        OR su2_chge_shon10 IS NOT NULL
        OR su2_chge_shon11 IS NOT NULL
        OR su2_chge_shon12 IS NOT NULL
        OR su2_chge_shon13 IS NOT NULL
        OR su2_chge_shon14 IS NOT NULL
        OR su2_chge_shon15 IS NOT NULL
        OR su2_chge_shon16 IS NOT NULL
        OR su2_chge_shon17 IS NOT NULL
        OR su2_chge_shon18 IS NOT NULL
        OR su2_chge_shon19 IS NOT NULL
        OR su2_chge_shon20 IS NOT NULL
        OR su2_demo_shon1 IS NOT NULL
        OR su2_demo_shon2 IS NOT NULL
        OR su2_demo_shon3 IS NOT NULL
        OR su2_demo_shon4 IS NOT NULL
        OR su2_demo_shon5 IS NOT NULL
        OR su2_demo_shon6 IS NOT NULL
        OR su2_demo_shon7 IS NOT NULL
        OR su2_demo_shon8 IS NOT NULL
        OR su2_demo_shon9 IS NOT NULL
        OR su2_demo_shon10 IS NOT NULL
        OR su2_demo_shon11 IS NOT NULL
        OR su2_demo_shon12 IS NOT NULL
        OR su2_demo_shon13 IS NOT NULL
        OR su2_demo_shon14 IS NOT NULL
        OR su2_demo_shon15 IS NOT NULL
        OR su2_demo_shon16 IS NOT NULL
        OR su2_demo_shon17 IS NOT NULL
        OR su2_demo_shon18 IS NOT NULL
        OR su2_demo_shon19 IS NOT NULL
        OR su2_demo_shon20 IS NOT NULL
        OR su2_sup_shon1 IS NOT NULL
        OR su2_sup_shon2 IS NOT NULL
        OR su2_sup_shon3 IS NOT NULL
        OR su2_sup_shon4 IS NOT NULL
        OR su2_sup_shon5 IS NOT NULL
        OR su2_sup_shon6 IS NOT NULL
        OR su2_sup_shon7 IS NOT NULL
        OR su2_sup_shon8 IS NOT NULL
        OR su2_sup_shon9 IS NOT NULL
        OR su2_sup_shon10 IS NOT NULL
        OR su2_sup_shon11 IS NOT NULL
        OR su2_sup_shon12 IS NOT NULL
        OR su2_sup_shon13 IS NOT NULL
        OR su2_sup_shon14 IS NOT NULL
        OR su2_sup_shon15 IS NOT NULL
        OR su2_sup_shon16 IS NOT NULL
        OR su2_sup_shon17 IS NOT NULL
        OR su2_sup_shon18 IS NOT NULL
        OR su2_sup_shon19 IS NOT NULL
        OR su2_sup_shon20 IS NOT NULL
        THEN donnees_techniques.su2_tot_shon_tot
        ELSE donnees_techniques.su_tot_shon_tot
    END as su_tot_shon_tot_donnees_techniques

FROM

    &DB_PREFIXErapport_instruction 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=rapport_instruction.dossier_instruction 
    LEFT JOIN &DB_PREFIXEavis_decision 
        ON dossier.avis_decision = avis_decision.avis_decision
    LEFT JOIN &DB_PREFIXEetat
        ON dossier.etat = etat.etat
    LEFT JOIN &DB_PREFIXEdonnees_techniques
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdivision
        ON dossier.division = division.division
    LEFT JOIN &DB_PREFIXEdirection
        ON division.direction = direction.direction
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_principal
        ON lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_principal_civilite
        ON petitionnaire_principal.personne_morale_civilite=petitionnaire_principal_civilite.civilite OR petitionnaire_principal.particulier_civilite=petitionnaire_principal_civilite.civilite
    LEFT JOIN (
        SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
        FROM &DB_PREFIXElien_dossier_demandeur
        LEFT JOIN &DB_PREFIXEdossier
            ON lien_dossier_demandeur.dossier=dossier.dossier 
            AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
        LEFT JOIN &DB_PREFIXErapport_instruction
            ON rapport_instruction.dossier_instruction = dossier.dossier
        WHERE rapport_instruction.rapport_instruction = &idx
        GROUP BY lien_dossier_demandeur.dossier
    ) as sub_petitionnaire_autre
    ON rapport_instruction.dossier_instruction = sub_petitionnaire_autre.dossier
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1] AND petitionnaire_1.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_1_civilite
    ON
        petitionnaire_1.particulier_civilite = petitionnaire_1_civilite.civilite OR petitionnaire_1.personne_morale_civilite = petitionnaire_1_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2] AND petitionnaire_2.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_2_civilite
    ON
        petitionnaire_2.particulier_civilite = petitionnaire_2_civilite.civilite OR petitionnaire_2.personne_morale_civilite = petitionnaire_2_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3] AND petitionnaire_3.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_3_civilite
    ON
        petitionnaire_3.particulier_civilite = petitionnaire_3_civilite.civilite OR petitionnaire_3.personne_morale_civilite = petitionnaire_3_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4] AND petitionnaire_4.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_4_civilite
    ON
        petitionnaire_4.particulier_civilite = petitionnaire_4_civilite.civilite OR petitionnaire_4.personne_morale_civilite = petitionnaire_4_civilite.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5] AND petitionnaire_5.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as petitionnaire_5_civilite
    ON
        petitionnaire_5.particulier_civilite = petitionnaire_5_civilite.civilite OR petitionnaire_5.personne_morale_civilite = petitionnaire_5_civilite.civilite
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
    LEFT JOIN
        &DB_PREFIXEtaxe_amenagement
        ON
            dossier.om_collectivite = taxe_amenagement.om_collectivite
WHERE rapport_instruction = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
',
merge_fields = '
--Données générales du rapport d''instruction
[dossier_instruction_rapport_instruction]    [analyse_reglementaire_rapport_instruction]
[description_projet_rapport_instruction]    [proposition_decision_rapport_instruction]

--Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]    [etat_dossier]
[delai_dossier]    [libelle_datd]
[libelle_avis_decision]

--Adresse du terrain dossier d''instruction
[terrain_adresse_voie_numero_dossier]     [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_cedex_dossier]    [terrain_superficie_dossier]
[terrain_references_cadastrales_dossier]

--Taxe d''aménagement du dossier d''instruction
[tax_taux_secteur]
[tax_numero_secteur]
[tax_montant_part_communale]
[tax_montant_part_departementale]
[tax_montant_part_regionale]
[tax_montant_total]

--Coordonnées du demandeur
[code_civilite]
[particulier_nom_demandeur]    [particulier_prenom_demandeur]    
[personne_morale_denomination_demandeur]    [personne_morale_raison_sociale_demandeur]    [personne_morale_siret_demandeur]
[personne_morale_nom_demandeur]    [personne_morale_prenom_demandeur]
[numero_demandeur]    [voie_demandeur]
[complement_demandeur]    [lieu_dit_demandeur]
[code_postal_demandeur]    [localite_demandeur]   [bp_demandeur]    [cedex_demandeur]

--Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[civilite_petitionnaire_principal]
[nom_particulier_petitionnaire_principal]
[prenom_particulier_petitionnaire_principal]
[raison_sociale_petitionnaire_principal]
[denomination_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''à 5)
[civilite_petitionnaire_1](jusqu''à 5)
[nom_particulier_petitionnaire_1](jusqu''à 5)
[prenom_particulier_petitionnaire_1](jusqu''à 5)
[raison_sociale_petitionnaire_1](jusqu''à 5)
[denomination_petitionnaire_1](jusqu''à 5)
[numero_petitionnaire_1](jusqu''à 5)    [voie_petitionnaire_1](jusqu''à 5)    
[complement_petitionnaire_1](jusqu''à 5)
[lieu_dit_petitionnaire_1](jusqu''à 5)    [bp_petitionnaire_1](jusqu''à 5)
[code_postal_petitionnaire_1](jusqu''à 5)    [localite_petitionnaire_1](jusqu''à 5)    
[cedex_petitionnaire_1](jusqu''à 5)
[pays_petitionnaire_1](jusqu''à 5)

--Nom de l''instructeur
[nom_instructeur]

--Noms des signataires
[chef_division]
[chef_direction]
[libelle_direction]
[description_direction]

--Données techniques
[co_projet_desc_donnees_techniques]    [am_projet_desc_donnees_techniques]
[dm_projet_desc_donnees_techniques]    [ope_proj_desc_donnees_techniques]
[projet_desc_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]    
[co_tot_log_nb_donnees_techniques]     [co_statio_place_nb_donnees_techniques]
-- Les données techniques suivantes concernent le tableau des surfaces
-- Elles récupèrent les valeurs du tableau composé des sous-destinations si au
-- moins une valeur de celui-ci est saisie
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]    
[tab_surface_donnees_techniques]
[su_tot_shon_tot_donnees_techniques]
'
WHERE code = 'rapport_instruction';

--
-- END - cleanup old openfoncier
--

--
-- START - [#8538] Réinitialisation des permissions
--

--
-- Suppression des droits
--

DELETE FROM om_droit;

--
-- Ajout des droits
--

INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1, 'action', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (2, 'action_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (3, 'affectation_automatique', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (4, 'affectation_automatique', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (5, 'affectation_automatique', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (6, 'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (7, 'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (8, 'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (9, 'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (10, 'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (11, 'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (12, 'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (13, 'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (14, 'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (15, 'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (16, 'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (17, 'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (18, 'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (19, 'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (20, 'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (21, 'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (22, 'architecte', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (23, 'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (24, 'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (25, 'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (26, 'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (27, 'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (28, 'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (29, 'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (30, 'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (31, 'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (32, 'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (33, 'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (34, 'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (35, 'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (36, 'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (37, 'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (38, 'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (39, 'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (40, 'architecte_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (41, 'architecte_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (42, 'architecte_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (43, 'architecte_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (44, 'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (45, 'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (46, 'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (47, 'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (48, 'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (49, 'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (50, 'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (51, 'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (52, 'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (53, 'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (54, 'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (55, 'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (56, 'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (57, 'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (58, 'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (59, 'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (60, 'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (61, 'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (62, 'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (63, 'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (64, 'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (65, 'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (66, 'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (67, 'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (68, 'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (69, 'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (70, 'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (71, 'architecte_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (72, 'arrondissement', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (73, 'arrondissement', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (74, 'arrondissement', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (75, 'autorite_competente', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (76, 'autorite_competente', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (77, 'autorite_competente_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (78, 'avis_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (79, 'avis_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (80, 'avis_decision', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (81, 'bible', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (82, 'bible', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (83, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (84, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (85, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (86, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (87, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (88, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (89, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (90, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (91, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (92, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (93, 'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (94, 'blocnote_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (95, 'blocnote_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (96, 'blocnote_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (97, 'blocnote_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (98, 'blocnote_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (99, 'blocnote_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (100, 'blocnote_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (101, 'blocnote_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (102, 'blocnote_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (103, 'blocnote_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (104, 'blocnote_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (105, 'blocnote_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (106, 'blocnote_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (107, 'blocnote_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (108, 'blocnote_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (109, 'blocnote_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (110, 'bordereau_envoi_maire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (111, 'bordereau_envoi_maire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (112, 'bordereau_envoi_maire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (113, 'cerfa', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (114, 'civilite', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (115, 'civilite', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (116, 'commission', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (117, 'commission', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (118, 'commission', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (119, 'commission', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (120, 'commission', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (121, 'commission_demandes_passage', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (122, 'commission_demandes_passage', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (123, 'commission_demandes_passage', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (124, 'commission_demandes_passage', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (125, 'commission_demandes_passage', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (126, 'commission_demandes_passage_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (127, 'commission_demandes_passage_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (128, 'commission_demandes_passage_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (129, 'commission_demandes_passage_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (130, 'commission_demandes_passage_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (131, 'commission_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (132, 'commission_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (133, 'commission_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (134, 'commission_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (135, 'commission_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (136, 'commission_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (137, 'commission_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (138, 'commission_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (139, 'commission_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (140, 'commission_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (141, 'commission_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (142, 'commission_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (143, 'commission_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (144, 'commission_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (145, 'commission_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (146, 'commission_type_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (147, 'commission_type_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (148, 'commission_type_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (149, 'commission_type_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (150, 'consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (151, 'consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (152, 'consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (153, 'consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (154, 'consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (155, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (156, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (157, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (158, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (159, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (160, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (161, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (162, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (163, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (164, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (165, 'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (166, 'consultation_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (167, 'consultation_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (168, 'consultation_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (169, 'consultation_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (170, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (171, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (172, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (173, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (174, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (175, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (176, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (177, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (178, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (179, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (180, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (181, 'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (182, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (183, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (184, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (185, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (186, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (187, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (188, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (189, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (190, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (191, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (192, 'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (193, 'consultation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (194, 'consultation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (195, 'consultation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (196, 'consultation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (197, 'consultation_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (198, 'consultation_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (199, 'consultation_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (200, 'consultation_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (201, 'consultation_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (202, 'consultation_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (203, 'consultation_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (204, 'consultation_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (205, 'consultation_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (206, 'consultation_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (207, 'consultation_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (208, 'consultation_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (209, 'consultation_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (210, 'consultation_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (211, 'consultation_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (212, 'consultation_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (213, 'consultation_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (214, 'consultation_fichier_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (215, 'consultation_fichier_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (216, 'consultation_fichier_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (217, 'consultation_fichier_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (218, 'consultation_fichier_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (219, 'consultation_fichier_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (220, 'consultation_fichier_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (221, 'consultation_fichier_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (222, 'consultation_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (223, 'consultation_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (224, 'consultation_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (225, 'consultation_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (226, 'consultation_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (227, 'consultation_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (228, 'consultation_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (229, 'consultation_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (230, 'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (231, 'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (232, 'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (233, 'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (234, 'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (235, 'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (236, 'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (237, 'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (238, 'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (239, 'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (240, 'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (241, 'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (242, 'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (243, 'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (244, 'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (245, 'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (246, 'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (247, 'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (248, 'consultation_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (249, 'consultation_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (250, 'consultation_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (251, 'consultation_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (252, 'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (253, 'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (254, 'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (255, 'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (256, 'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (257, 'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (258, 'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (259, 'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (260, 'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (261, 'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (262, 'consultation_modifier_fichier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (263, 'consultation_modifier_fichier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (264, 'consultation_modifier_fichier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (265, 'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (266, 'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (267, 'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (268, 'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (269, 'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (270, 'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (271, 'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (272, 'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (273, 'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (274, 'consultation_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (275, 'consultation_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (276, 'consultation_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (277, 'consultation_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (278, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (279, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (280, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (281, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (282, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (283, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (284, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (285, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (286, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (287, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (288, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (289, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (290, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (291, 'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (292, 'consultation_retour_avis_service', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (293, 'consultation_retour_avis_service', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (294, 'consultation_retour_avis_service', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (295, 'consultation_retour_avis_service', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (296, 'consultation_retour_avis_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (297, 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (298, 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (299, 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (300, 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (301, 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (302, 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (303, 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (304, 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (305, 'consultation_saisir_date_envoi', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (306, 'consultation_saisir_date_envoi', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (307, 'consultation_saisir_date_envoi', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (308, 'consultation_saisir_date_envoi', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (309, 'consultation_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (310, 'consultation_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (311, 'consultation_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (312, 'consultation_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (313, 'consultation_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (314, 'consultation_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (315, 'consultation_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (316, 'consultation_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (317, 'consultation_suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (318, 'consultation_suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (319, 'consultation_suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (320, 'consultation_suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (321, 'consultation_suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (322, 'consultation_suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (323, 'consultation_suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (324, 'consultation_suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (325, 'consultation_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (326, 'consultation_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (327, 'consultation_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (328, 'consultation_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (329, 'consultation_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (330, 'consultation_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (331, 'consultation_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (332, 'consultation_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (333, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (334, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (335, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (336, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (337, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (338, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (339, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (340, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (341, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (342, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (343, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (344, 'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (345, 'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (346, 'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (347, 'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (348, 'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (349, 'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (350, 'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (351, 'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (352, 'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (353, 'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (354, 'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (355, 'contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (356, 'contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (357, 'contrainte_synchronisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (358, 'copie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (359, 'copie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (360, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (361, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (362, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (363, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (364, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (365, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (366, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (367, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (368, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (369, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (370, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (371, 'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (372, 'demande', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (373, 'demande_affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (374, 'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (375, 'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (376, 'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (377, 'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (378, 'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (379, 'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (380, 'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (381, 'demande_affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (382, 'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (383, 'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (384, 'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (385, 'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (386, 'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (387, 'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (388, 'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (389, 'demande_autre_dossier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (390, 'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (391, 'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (392, 'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (393, 'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (394, 'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (395, 'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (396, 'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (397, 'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (398, 'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (399, 'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (400, 'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (401, 'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (402, 'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (403, 'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (404, 'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (405, 'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (406, 'demande_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (407, 'demande_avis_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (408, 'demande_avis_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (409, 'demande_avis_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (410, 'demande_avis_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (411, 'demande_avis_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (412, 'demande_avis_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (413, 'demande_avis_encours', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (414, 'demande_avis_encours', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (415, 'demande_avis_encours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (416, 'demande_avis_encours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (417, 'demande_avis_encours_demarquer', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (418, 'demande_avis_encours_demarquer', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (419, 'demande_avis_encours_demarquer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (420, 'demande_avis_encours_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (421, 'demande_avis_encours_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (422, 'demande_avis_encours_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (423, 'demande_avis_encours_marquer', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (424, 'demande_avis_encours_marquer', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (425, 'demande_avis_encours_marquer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (426, 'demande_avis_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (427, 'demande_avis_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (428, 'demande_avis_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (429, 'demande_avis_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (430, 'demande_avis_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (431, 'demande_avis_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (432, 'demande_avis_passee', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (433, 'demande_avis_passee', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (434, 'demande_avis_passee', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (435, 'demande_avis_passee', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (436, 'demande_avis_passee_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (437, 'demande_avis_passee_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (438, 'demande_avis_passee_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (439, 'demande_avis_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (440, 'demande_avis_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (441, 'demande_avis_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (442, 'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (443, 'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (444, 'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (445, 'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (446, 'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (447, 'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (448, 'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (449, 'demande_dossier_encours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (450, 'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (451, 'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (452, 'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (453, 'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (454, 'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (455, 'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (456, 'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (457, 'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (458, 'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (459, 'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (460, 'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (461, 'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (462, 'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (463, 'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (464, 'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (465, 'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (466, 'demande_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (467, 'demande_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (468, 'demande_nature', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (469, 'demande_nature', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (470, 'demande_nouveau_dossier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (471, 'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (472, 'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (473, 'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (474, 'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (475, 'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (476, 'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (477, 'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (478, 'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (479, 'demande_nouveau_dossier_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (480, 'demande_nouveau_dossier_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (481, 'demande_nouveau_dossier_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (482, 'demande_nouveau_dossier_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (483, 'demande_nouveau_dossier_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (484, 'demande_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (485, 'demande_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (486, 'demande_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (487, 'demande_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (488, 'demande_recuperer_adresse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (489, 'demande_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (490, 'demande_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (491, 'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (492, 'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (493, 'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (494, 'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (495, 'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (496, 'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (497, 'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (498, 'demande_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (499, 'demande_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (500, 'demande_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (501, 'demande_type_show_checklist', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (502, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (503, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (504, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (505, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (506, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (507, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (508, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (509, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (510, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (511, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (512, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (513, 'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (514, 'demandeur_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (515, 'demandeur_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (516, 'demandeur_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (517, 'demandeur_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (518, 'direction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (519, 'direction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (520, 'direction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (521, 'directory', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (522, 'directory', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (523, 'division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (524, 'division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (525, 'division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (526, 'document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (527, 'document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (528, 'document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (529, 'document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (530, 'document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (531, 'document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (532, 'document_numerise_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (533, 'document_numerise_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (534, 'document_numerise_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (535, 'document_numerise_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (536, 'document_numerise_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (537, 'document_numerise_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (538, 'document_numerise_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (539, 'document_numerise_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (540, 'document_numerise_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (541, 'document_numerise_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (542, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (543, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (544, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (545, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (546, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (547, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (548, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (549, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (550, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (551, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (552, 'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (553, 'document_numerise_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (554, 'document_numerise_type_categorie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (555, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (556, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (557, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (558, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (559, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (560, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (561, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (562, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (563, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (564, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (565, 'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (566, 'donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (567, 'donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (568, 'donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (569, 'donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (570, 'donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (571, 'donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (572, 'donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (573, 'donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (574, 'donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (575, 'donnees_techniques_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (576, 'donnees_techniques_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (577, 'donnees_techniques_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (578, 'donnees_techniques_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (579, 'donnees_techniques_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (580, 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (581, 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (582, 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (583, 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (584, 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (585, 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (586, 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (587, 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (588, 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (589, 'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (590, 'donnees_techniques_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (591, 'donnees_techniques_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (592, 'donnees_techniques_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (593, 'donnees_techniques_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (594, 'donnees_techniques_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (595, 'donnees_techniques_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (596, 'donnees_techniques_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (597, 'donnees_techniques_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (598, 'donnees_techniques_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (599, 'donnees_techniques_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (600, 'donnees_techniques_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (601, 'donnees_techniques_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (602, 'dossier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (603, 'dossier_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (604, 'dossier_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (605, 'dossier_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (606, 'dossier_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (607, 'dossier_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (608, 'dossier_autorisation_avis_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (609, 'dossier_autorisation_avis_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (610, 'dossier_autorisation_avis_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (611, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (612, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (613, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (614, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (615, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (616, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (617, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (618, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (619, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (620, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (621, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (622, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (623, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (624, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (625, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (626, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (627, 'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (628, 'dossier_autorisation_display_da_di', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (629, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (630, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (631, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (632, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (633, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (634, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (635, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (636, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (637, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (638, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (639, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (640, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (641, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (642, 'dossier_autorisation_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (643, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (644, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (645, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (646, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (647, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (648, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (649, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (650, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (651, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (652, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (653, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (654, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (655, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (656, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (657, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (658, 'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (659, 'dossier_autorisation_parcelle', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (660, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (661, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (662, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (663, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (664, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (665, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (666, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (667, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (668, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (669, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (670, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (671, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (672, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (673, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (674, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (675, 'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (676, 'dossier_autorisation_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (677, 'dossier_autorisation_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (678, 'dossier_autorisation_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (679, 'dossier_autorisation_type_detaille', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (680, 'dossier_autorisation_type_detaille', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (681, 'dossier_autorisation_type_detaille', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (682, 'dossier_commission', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (683, 'dossier_commission', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (684, 'dossier_commission', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (685, 'dossier_commission', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (686, 'dossier_commission', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (687, 'dossier_commission', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (688, 'dossier_commission', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (689, 'dossier_commission_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (690, 'dossier_commission_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (691, 'dossier_commission_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (692, 'dossier_commission_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (693, 'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (694, 'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (695, 'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (696, 'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (697, 'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (698, 'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (699, 'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (700, 'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (701, 'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (702, 'dossier_commission_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (703, 'dossier_commission_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (704, 'dossier_commission_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (705, 'dossier_commission_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (706, 'dossier_commission_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (707, 'dossier_commission_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (708, 'dossier_commission_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (709, 'dossier_commission_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (710, 'dossier_commission_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (711, 'dossier_commission_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (712, 'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (713, 'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (714, 'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (715, 'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (716, 'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (717, 'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (718, 'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (719, 'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (720, 'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (721, 'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (722, 'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (723, 'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (724, 'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (725, 'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (726, 'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (727, 'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (728, 'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (729, 'dossier_commission_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (730, 'dossier_commission_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (731, 'dossier_commission_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (732, 'dossier_commission_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (733, 'dossier_commission_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (734, 'dossier_commission_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (735, 'dossier_commission_supprimer_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (736, 'dossier_commission_supprimer_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (737, 'dossier_commission_supprimer_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (738, 'dossier_commission_supprimer_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (739, 'dossier_commission_supprimer_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (740, 'dossier_commission_supprimer_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (741, 'dossier_commission_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (742, 'dossier_commission_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (743, 'dossier_commission_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (744, 'dossier_commission_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (745, 'dossier_commission_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (746, 'dossier_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (747, 'dossier_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (748, 'dossier_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (749, 'dossier_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (750, 'dossier_contrainte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (751, 'dossier_contrainte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (752, 'dossier_contrainte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (753, 'dossier_contrainte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (754, 'dossier_contrainte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (755, 'dossier_contrainte_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (756, 'dossier_contrainte_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (757, 'dossier_contrainte_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (758, 'dossier_contrainte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (759, 'dossier_contrainte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (760, 'dossier_contrainte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (761, 'dossier_contrainte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (762, 'dossier_contrainte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (763, 'dossier_contrainte_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (764, 'dossier_contrainte_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (765, 'dossier_contrainte_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (766, 'dossier_contrainte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (767, 'dossier_contrainte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (768, 'dossier_contrainte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (769, 'dossier_contrainte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (770, 'dossier_contrainte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (771, 'dossier_contrainte_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (772, 'dossier_contrainte_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (773, 'dossier_contrainte_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (774, 'dossier_contrainte_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (775, 'dossier_contrainte_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (776, 'dossier_contrainte_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (777, 'dossier_contrainte_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (778, 'dossier_contrainte_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (779, 'dossier_contrainte_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (780, 'dossier_contrainte_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (781, 'dossier_contrainte_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (782, 'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (783, 'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (784, 'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (785, 'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (786, 'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (787, 'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (788, 'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (789, 'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (790, 'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (791, 'dossier_contrainte_tab_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (792, 'dossier_contrainte_tab_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (793, 'dossier_contrainte_tab_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (794, 'dossier_document', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (795, 'dossier_document', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (796, 'dossier_document', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (797, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (798, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (799, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (800, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (801, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (802, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (803, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (804, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (805, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (806, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (807, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (808, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (809, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (810, 'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (811, 'dossier_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (812, 'dossier_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (813, 'dossier_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (814, 'dossier_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (815, 'dossier_geolocalisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (816, 'dossier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (817, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (818, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (819, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (820, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (821, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (822, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (823, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (824, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (825, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (826, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (827, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (828, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (829, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (830, 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (831, 'dossier_instruction_display_da_di', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (832, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (833, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (834, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (835, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (836, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (837, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (838, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (839, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (840, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (841, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (842, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (843, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (844, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (845, 'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (846, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (847, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (848, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (849, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (850, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (851, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (852, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (853, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (854, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (855, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (856, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (857, 'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (858, 'dossier_instruction_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (859, 'dossier_instruction_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (860, 'dossier_instruction_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (861, 'dossier_instruction_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (862, 'dossier_instruction_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (863, 'dossier_instruction_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (864, 'dossier_instruction_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (865, 'dossier_instruction_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (866, 'dossier_instruction_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (867, 'dossier_instruction_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (868, 'dossier_instruction_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (869, 'dossier_instruction_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (870, 'dossier_instruction_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (871, 'dossier_instruction_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (872, 'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (873, 'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (874, 'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (875, 'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (876, 'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (877, 'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (878, 'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (879, 'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (880, 'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (881, 'dossier_instruction_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (882, 'dossier_instruction_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (883, 'dossier_instruction_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (884, 'dossier_instruction_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (885, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (886, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (887, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (888, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (889, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (890, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (891, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (892, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));;
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (893, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (894, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (895, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (896, 'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (897, 'dossier_instruction_mes_clotures', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (898, 'dossier_instruction_mes_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (899, 'dossier_instruction_mes_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (900, 'dossier_instruction_mes_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (901, 'dossier_instruction_mes_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (902, 'dossier_instruction_mes_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (903, 'dossier_instruction_mes_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (904, 'dossier_instruction_mes_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (905, 'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (906, 'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (907, 'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (908, 'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (909, 'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (910, 'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (911, 'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (912, 'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (913, 'dossier_instruction_mes_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (914, 'dossier_instruction_mes_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (915, 'dossier_instruction_mes_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (916, 'dossier_instruction_mes_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (917, 'dossier_instruction_mes_clotures_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (918, 'dossier_instruction_mes_clotures_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (919, 'dossier_instruction_mes_clotures_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (920, 'dossier_instruction_mes_clotures_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (921, 'dossier_instruction_mes_clotures_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (922, 'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (923, 'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (924, 'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (925, 'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (926, 'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (927, 'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (928, 'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (929, 'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (930, 'dossier_instruction_mes_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (931, 'dossier_instruction_mes_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (932, 'dossier_instruction_mes_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (933, 'dossier_instruction_mes_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (934, 'dossier_instruction_mes_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (935, 'dossier_instruction_mes_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (936, 'dossier_instruction_mes_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (937, 'dossier_instruction_mes_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (938, 'dossier_instruction_mes_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (939, 'dossier_instruction_mes_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (940, 'dossier_instruction_mes_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (941, 'dossier_instruction_mes_clotures_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (942, 'dossier_instruction_mes_clotures_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (943, 'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (944, 'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (945, 'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (946, 'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (947, 'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (948, 'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (949, 'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (950, 'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (951, 'dossier_instruction_mes_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (952, 'dossier_instruction_mes_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (953, 'dossier_instruction_mes_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (954, 'dossier_instruction_mes_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (955, 'dossier_instruction_mes_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (956, 'dossier_instruction_mes_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (957, 'dossier_instruction_mes_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (958, 'dossier_instruction_mes_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (959, 'dossier_instruction_mes_clotures_regenerer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (960, 'dossier_instruction_mes_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (961, 'dossier_instruction_mes_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (962, 'dossier_instruction_mes_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (963, 'dossier_instruction_mes_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (964, 'dossier_instruction_mes_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (965, 'dossier_instruction_mes_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (966, 'dossier_instruction_mes_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (967, 'dossier_instruction_mes_encours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (968, 'dossier_instruction_mes_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (969, 'dossier_instruction_mes_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (970, 'dossier_instruction_mes_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (971, 'dossier_instruction_mes_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (972, 'dossier_instruction_mes_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (973, 'dossier_instruction_mes_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (974, 'dossier_instruction_mes_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (975, 'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (976, 'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (977, 'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (978, 'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (979, 'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (980, 'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (981, 'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (982, 'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (983, 'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (984, 'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (985, 'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (986, 'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (987, 'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (988, 'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (989, 'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (990, 'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (991, 'dossier_instruction_mes_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (992, 'dossier_instruction_mes_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (993, 'dossier_instruction_mes_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (994, 'dossier_instruction_mes_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (995, 'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (996, 'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (997, 'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (998, 'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (999, 'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1000, 'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1001, 'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1002, 'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1003, 'dossier_instruction_mes_encours_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1004, 'dossier_instruction_mes_encours_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1005, 'dossier_instruction_mes_encours_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1006, 'dossier_instruction_mes_encours_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1007, 'dossier_instruction_mes_encours_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1008, 'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1009, 'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1010, 'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1011, 'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1012, 'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1013, 'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1014, 'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1015, 'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1016, 'dossier_instruction_mes_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1017, 'dossier_instruction_mes_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1018, 'dossier_instruction_mes_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1019, 'dossier_instruction_mes_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1020, 'dossier_instruction_mes_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1021, 'dossier_instruction_mes_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1022, 'dossier_instruction_mes_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1023, 'dossier_instruction_mes_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1024, 'dossier_instruction_mes_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1025, 'dossier_instruction_mes_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1026, 'dossier_instruction_mes_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1027, 'dossier_instruction_mes_encours_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1028, 'dossier_instruction_mes_encours_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1029, 'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1030, 'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1031, 'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1032, 'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1033, 'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1034, 'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1035, 'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1036, 'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1037, 'dossier_instruction_mes_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1038, 'dossier_instruction_mes_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1039, 'dossier_instruction_mes_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1040, 'dossier_instruction_mes_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1041, 'dossier_instruction_mes_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1042, 'dossier_instruction_mes_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1043, 'dossier_instruction_mes_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1044, 'dossier_instruction_mes_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1045, 'dossier_instruction_mes_encours_regenerer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1046, 'dossier_instruction_mes_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1047, 'dossier_instruction_mes_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1048, 'dossier_instruction_mes_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1049, 'dossier_instruction_mes_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1050, 'dossier_instruction_mes_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1051, 'dossier_instruction_mes_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1052, 'dossier_instruction_mes_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1053, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1054, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1055, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1056, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1057, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1058, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1059, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1060, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1061, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1062, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1063, 'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1064, 'dossier_instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1065, 'dossier_instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1066, 'dossier_instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1067, 'dossier_instruction_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1068, 'dossier_instruction_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1069, 'dossier_instruction_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1070, 'dossier_instruction_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1071, 'dossier_instruction_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1072, 'dossier_instruction_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1073, 'dossier_instruction_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1074, 'dossier_instruction_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1075, 'dossier_instruction_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1076, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1077, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1078, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1079, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1080, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1081, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1082, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1083, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1084, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1085, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1086, 'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1087, 'dossier_instruction_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1088, 'dossier_instruction_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1089, 'dossier_instruction_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1090, 'dossier_instruction_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1091, 'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1092, 'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1093, 'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1094, 'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1095, 'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1096, 'dossier_instruction_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1097, 'dossier_instruction_regenerer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1098, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1099, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1100, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1101, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1102, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1103, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1104, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1105, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1106, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1107, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1108, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1109, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1110, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1111, 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1112, 'dossier_instruction_tous_clotures', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1113, 'dossier_instruction_tous_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1114, 'dossier_instruction_tous_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1115, 'dossier_instruction_tous_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1116, 'dossier_instruction_tous_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1117, 'dossier_instruction_tous_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1118, 'dossier_instruction_tous_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1119, 'dossier_instruction_tous_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1120, 'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1121, 'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1122, 'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1123, 'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1124, 'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1125, 'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1126, 'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1127, 'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1128, 'dossier_instruction_tous_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1129, 'dossier_instruction_tous_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1130, 'dossier_instruction_tous_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1131, 'dossier_instruction_tous_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1132, 'dossier_instruction_tous_clotures_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1133, 'dossier_instruction_tous_clotures_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1134, 'dossier_instruction_tous_clotures_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1135, 'dossier_instruction_tous_clotures_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1136, 'dossier_instruction_tous_clotures_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1137, 'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1138, 'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1139, 'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1140, 'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1141, 'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1142, 'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1143, 'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1144, 'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1145, 'dossier_instruction_tous_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1146, 'dossier_instruction_tous_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1147, 'dossier_instruction_tous_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1148, 'dossier_instruction_tous_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1149, 'dossier_instruction_tous_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1150, 'dossier_instruction_tous_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1151, 'dossier_instruction_tous_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1152, 'dossier_instruction_tous_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1153, 'dossier_instruction_tous_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1154, 'dossier_instruction_tous_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1155, 'dossier_instruction_tous_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1156, 'dossier_instruction_tous_clotures_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1157, 'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1158, 'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1159, 'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1160, 'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1161, 'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1162, 'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1163, 'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1164, 'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1165, 'dossier_instruction_tous_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1166, 'dossier_instruction_tous_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1167, 'dossier_instruction_tous_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1168, 'dossier_instruction_tous_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1169, 'dossier_instruction_tous_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1170, 'dossier_instruction_tous_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1171, 'dossier_instruction_tous_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1172, 'dossier_instruction_tous_clotures_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1173, 'dossier_instruction_tous_clotures_regenerer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1174, 'dossier_instruction_tous_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1175, 'dossier_instruction_tous_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1176, 'dossier_instruction_tous_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1177, 'dossier_instruction_tous_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1178, 'dossier_instruction_tous_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1179, 'dossier_instruction_tous_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1180, 'dossier_instruction_tous_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1181, 'dossier_instruction_tous_encours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1182, 'dossier_instruction_tous_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1183, 'dossier_instruction_tous_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1184, 'dossier_instruction_tous_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1185, 'dossier_instruction_tous_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1186, 'dossier_instruction_tous_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1187, 'dossier_instruction_tous_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1188, 'dossier_instruction_tous_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1189, 'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1190, 'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1191, 'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1192, 'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1193, 'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1194, 'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1195, 'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1196, 'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1197, 'dossier_instruction_tous_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1198, 'dossier_instruction_tous_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1199, 'dossier_instruction_tous_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1200, 'dossier_instruction_tous_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1201, 'dossier_instruction_tous_encours_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1202, 'dossier_instruction_tous_encours_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1203, 'dossier_instruction_tous_encours_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1204, 'dossier_instruction_tous_encours_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1205, 'dossier_instruction_tous_encours_generer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1206, 'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1207, 'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1208, 'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1209, 'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1210, 'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1211, 'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1212, 'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1213, 'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1214, 'dossier_instruction_tous_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1215, 'dossier_instruction_tous_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1216, 'dossier_instruction_tous_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1217, 'dossier_instruction_tous_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1218, 'dossier_instruction_tous_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1219, 'dossier_instruction_tous_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1220, 'dossier_instruction_tous_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1221, 'dossier_instruction_tous_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1222, 'dossier_instruction_tous_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1223, 'dossier_instruction_tous_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1224, 'dossier_instruction_tous_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1225, 'dossier_instruction_tous_encours_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1226, 'dossier_instruction_tous_encours_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1227, 'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1228, 'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1229, 'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1230, 'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1231, 'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1232, 'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1233, 'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1234, 'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1235, 'dossier_instruction_tous_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1236, 'dossier_instruction_tous_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1237, 'dossier_instruction_tous_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1238, 'dossier_instruction_tous_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1239, 'dossier_instruction_tous_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1240, 'dossier_instruction_tous_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1241, 'dossier_instruction_tous_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1242, 'dossier_instruction_tous_encours_regenerate_recepisse', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1243, 'dossier_instruction_tous_encours_regenerer_cle_acces_citoyen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1244, 'dossier_instruction_tous_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1245, 'dossier_instruction_tous_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1246, 'dossier_instruction_tous_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1247, 'dossier_instruction_tous_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1248, 'dossier_instruction_tous_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1249, 'dossier_instruction_tous_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1250, 'dossier_instruction_tous_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1251, 'dossier_instruction_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1252, 'dossier_instruction_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1253, 'dossier_instruction_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1254, 'dossier_message', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1255, 'dossier_message_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1256, 'dossier_message_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1257, 'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1258, 'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1259, 'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1260, 'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1261, 'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1262, 'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1263, 'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1264, 'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1265, 'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1266, 'dossier_message_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1267, 'dossier_message_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1268, 'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1269, 'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1270, 'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1271, 'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1272, 'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1273, 'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1274, 'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1275, 'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1276, 'dossier_message_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1277, 'dossier_message_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1278, 'dossier_message_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1279, 'dossier_message_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1280, 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1281, 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1282, 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1283, 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1284, 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1285, 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1286, 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1287, 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1288, 'dossier_message_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1289, 'dossier_modifier_division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1290, 'dossier_modifier_division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1291, 'dossier_modifier_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1292, 'dossier_modifier_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1293, 'dossier_parcelle', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1294, 'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1295, 'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1296, 'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1297, 'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1298, 'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1299, 'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1300, 'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1301, 'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1302, 'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1303, 'dossier_qualifier_qualificateur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1304, 'dossier_qualifier_qualificateur_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1305, 'dossier_qualifier_qualificateur_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1306, 'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1307, 'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1308, 'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1309, 'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1310, 'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1311, 'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1312, 'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1313, 'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1314, 'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1315, 'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1316, 'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1317, 'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1318, 'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1319, 'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1320, 'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1321, 'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1322, 'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1323, 'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1324, 'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1325, 'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1326, 'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1327, 'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1328, 'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1329, 'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1330, 'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1331, 'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1332, 'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1333, 'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1334, 'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1335, 'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1336, 'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1337, 'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1338, 'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1339, 'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1340, 'edition', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1341, 'etat', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1342, 'etat_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1343, 'etat_dossier_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1344, 'etat_dossier_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1345, 'etat_dossier_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1346, 'evenement', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1347, 'evenement_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1348, 'evenement_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1349, 'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1350, 'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1351, 'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1352, 'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1353, 'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1354, 'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1355, 'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1356, 'gen', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1357, 'genre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1358, 'genre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1359, 'genre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1360, 'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1361, 'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1362, 'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1363, 'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1364, 'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1365, 'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1366, 'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1367, 'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1368, 'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1369, 'groupe', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1370, 'groupe', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1371, 'groupe', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1372, 'import', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1373, 'import', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1374, 'instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1375, 'instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1376, 'instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1377, 'instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1378, 'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1379, 'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1380, 'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1381, 'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1382, 'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1383, 'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1384, 'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1385, 'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1386, 'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1387, 'instruction_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1388, 'instruction_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1389, 'instruction_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1390, 'instruction_bordereau_envoi_maire', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1391, 'instruction_bordereau_envoi_maire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1392, 'instruction_bordereau_envoi_maire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1393, 'instruction_changer_decision', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1394, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1395, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1396, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1397, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1398, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1399, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1400, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1401, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1402, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1403, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1404, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1405, 'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1406, 'instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1407, 'instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1408, 'instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1409, 'instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1410, 'instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1411, 'instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1412, 'instruction_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1413, 'instruction_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1414, 'instruction_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1415, 'instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1416, 'instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1417, 'instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1418, 'instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1419, 'instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1420, 'instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1421, 'instruction_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1422, 'instruction_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1423, 'instruction_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1424, 'instruction_modification_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1425, 'instruction_modification_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1426, 'instruction_modification_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1427, 'instruction_modification_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1428, 'instruction_modification_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1429, 'instruction_modification_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1430, 'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1431, 'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1432, 'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1433, 'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1434, 'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1435, 'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1436, 'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1437, 'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1438, 'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1439, 'instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1440, 'instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1441, 'instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1442, 'instruction_notifier_commune', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1443, 'instruction_notifier_commune', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1444, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1445, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1446, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1447, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1448, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1449, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1450, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1451, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1452, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1453, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1454, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1455, 'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1456, 'instruction_suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1457, 'instruction_suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1458, 'instruction_suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1459, 'instruction_suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1460, 'instruction_suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1461, 'instruction_suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1462, 'instruction_suivi_bordereaux_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1463, 'instruction_suivi_bordereaux_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1464, 'instruction_suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1465, 'instruction_suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1466, 'instruction_suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1467, 'instruction_suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1468, 'instruction_suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1469, 'instruction_suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1470, 'instruction_suivi_envoi_lettre_rar_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1471, 'instruction_suivi_envoi_lettre_rar_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1472, 'instruction_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1473, 'instruction_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1474, 'instruction_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1475, 'instruction_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1476, 'instruction_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1477, 'instruction_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1478, 'instruction_suivi_mise_a_jour_des_dates_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1479, 'instruction_suivi_mise_a_jour_des_dates_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1480, 'instruction_suivi_mise_a_jour_des_dates_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1481, 'instruction_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1482, 'instruction_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1483, 'instruction_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1484, 'instruction_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1485, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1486, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1487, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1488, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1489, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1490, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1491, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1492, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1493, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1494, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1495, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1496, 'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1497, 'lien_demande_demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1498, 'lien_demande_type_etat', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1499, 'lien_dossier_autorisation_demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1500, 'lien_dossier_demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1501, 'lien_dossier_instruction_type_evenement', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1502, 'lien_lot_demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1503, 'lien_service_om_utilisateur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1504, 'lien_service_om_utilisateur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1505, 'lien_service_om_utilisateur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1506, 'lien_service_service_categorie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1507, 'lien_service_service_categorie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1508, 'lien_service_service_categorie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1509, 'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1510, 'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1511, 'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1512, 'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1513, 'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1514, 'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1515, 'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1516, 'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1517, 'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1518, 'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1519, 'lot_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1520, 'lot_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1521, 'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1522, 'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1523, 'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1524, 'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1525, 'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1526, 'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1527, 'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1528, 'lot_editer_donnees_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1529, 'lot_editer_donnees_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1530, 'lot_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1531, 'lot_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1532, 'lot_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1533, 'lot_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1534, 'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1535, 'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1536, 'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1537, 'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1538, 'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1539, 'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1540, 'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1541, 'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1542, 'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1543, 'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1544, 'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1545, 'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1546, 'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1547, 'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1548, 'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1549, 'lot_transferer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1550, 'lot_transferer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1551, 'menu_administration', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1552, 'menu_administration', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1553, 'menu_administration', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1554, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1555, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1556, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1557, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1558, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1559, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1560, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1561, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1562, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1563, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1564, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1565, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1566, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1567, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1568, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1569, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1570, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1571, 'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1572, 'menu_demande_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1573, 'menu_demande_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1574, 'menu_demande_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1575, 'menu_demande_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1576, 'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1577, 'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1578, 'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1579, 'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1580, 'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1581, 'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1582, 'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1583, 'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1584, 'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1585, 'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1586, 'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1587, 'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1588, 'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1589, 'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1590, 'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1591, 'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1592, 'menu_guichet_unique_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1593, 'menu_guichet_unique_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1594, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1595, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1596, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1597, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1598, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'SERVICE CONSULTÉ INTERNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1599, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1600, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1601, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1602, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1603, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1604, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1605, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1606, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1607, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1608, 'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1609, 'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1610, 'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1611, 'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1612, 'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1613, 'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1614, 'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1615, 'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1616, 'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1617, 'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1618, 'menu_parametrage', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1619, 'menu_parametrage', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1620, 'menu_parametrage', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1621, 'menu_qualification_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1622, 'menu_sig', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1623, 'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1624, 'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1625, 'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1626, 'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1627, 'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1628, 'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1629, 'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1630, 'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1631, 'menu_suivi_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1632, 'messages', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1633, 'messages_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1634, 'messages_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1635, 'messages_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1636, 'messages_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1637, 'messages_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1638, 'messages_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1639, 'messages_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1640, 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1641, 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1642, 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1643, 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1644, 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1645, 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1646, 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1647, 'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1648, 'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1649, 'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1650, 'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1651, 'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1652, 'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1653, 'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1654, 'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1655, 'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1656, 'om_collectivite', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1657, 'om_collectivite', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1658, 'om_collectivite', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1659, 'om_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1660, 'om_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1661, 'om_dbform', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1662, 'om_droit', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1663, 'om_droit', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1664, 'om_etat', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1665, 'om_etat', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1666, 'om_etat', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1667, 'om_formulaire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1668, 'om_lettretype', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1669, 'om_lettretype', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1670, 'om_lettretype', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1671, 'om_logo', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1672, 'om_logo', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1673, 'om_parametre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1674, 'om_parametre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1675, 'om_parametre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1676, 'om_profil', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1677, 'om_profil', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1678, 'om_requete', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1679, 'om_requete', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1680, 'om_sig_extent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1681, 'om_sig_flux', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1682, 'om_sig_map', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1683, 'om_sig_map_comp', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1684, 'om_sig_map_flux', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1685, 'om_sousetat', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1686, 'om_sousetat', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1687, 'om_sousetat', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1688, 'om_table', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1689, 'om_utilisateur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1690, 'om_utilisateur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1691, 'om_utilisateur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1692, 'om_widget', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1693, 'om_widget', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1694, 'parcelle', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1695, 'parcelle_lot', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1696, 'password', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1697, 'password', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1698, 'password', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1699, 'password', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1700, 'password', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1701, 'pdf_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1702, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1703, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1704, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1705, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1706, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1707, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1708, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1709, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1710, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1711, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1712, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1713, 'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1714, 'petitionnaire_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1715, 'petitionnaire_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1716, 'petitionnaire_frequent_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1717, 'petitionnaire_frequent_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1718, 'petitionnaire_frequent_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1719, 'petitionnaire_frequent_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1720, 'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1721, 'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1722, 'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1723, 'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1724, 'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1725, 'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1726, 'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1727, 'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1728, 'petitionnaire_frequent_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1729, 'petitionnaire_frequent_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1730, 'petitionnaire_frequent_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1731, 'petitionnaire_frequent_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1732, 'petitionnaire_frequent_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1733, 'petitionnaire_frequent_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1734, 'petitionnaire_frequent_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1735, 'petitionnaire_frequent_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1736, 'petitionnaire_frequent_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1737, 'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1738, 'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1739, 'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1740, 'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1741, 'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1742, 'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1743, 'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1744, 'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1745, 'phase', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1746, 'pos', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1747, 'profil_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1748, 'profil_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1749, 'profil_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1750, 'profil_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1751, 'profil_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1752, 'profil_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1753, 'profil_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1754, 'proprietaire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1755, 'qualification_menu', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1756, 'qualification_menu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1757, 'qualification_menu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1758, 'qualification_menu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1759, 'qualification_menu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1760, 'quartier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1761, 'quartier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1762, 'quartier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1763, 'rapport_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1764, 'rapport_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1765, 'rapport_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1766, 'rapport_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1767, 'rapport_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1768, 'rapport_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1769, 'rapport_instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1770, 'rapport_instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1771, 'rapport_instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1772, 'rapport_instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1773, 'rapport_instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1774, 'rapport_instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1775, 'rapport_instruction_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1776, 'rapport_instruction_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1777, 'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1778, 'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1779, 'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1780, 'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1781, 'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1782, 'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1783, 'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1784, 'rapport_instruction_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1785, 'rapport_instruction_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1786, 'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1787, 'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1788, 'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1789, 'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1790, 'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1791, 'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1792, 'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1793, 'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1794, 'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1795, 'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1796, 'rapport_instruction_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1797, 'rapport_instruction_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1798, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1799, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1800, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1801, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1802, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1803, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1804, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1805, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1806, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1807, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1808, 'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1809, 'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1810, 'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1811, 'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1812, 'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1813, 'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1814, 'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1815, 'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1816, 'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1817, 'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1818, 'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1819, 'rapport_instruction_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1820, 'rapport_instruction_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1821, 'rapport_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1822, 'rapport_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1823, 'rapport_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1824, 'rapport_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1825, 'rapport_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1826, 'rapport_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1827, 'rapport_instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1828, 'rapport_instruction_om_fichier_rapport_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1829, 'rapport_instruction_om_fichier_rapport_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1830, 'rapport_instruction_om_fichier_rapport_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1831, 'rapport_instruction_om_fichier_rapport_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1832, 'rapport_instruction_om_fichier_rapport_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1833, 'rapport_instruction_om_fichier_rapport_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1834, 'rapport_instruction_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1835, 'rapport_instruction_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1836, 'rapport_instruction_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1837, 'regle', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1838, 'reqmo', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1839, 'reqmo_pilot', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1840, 'reqmo_pilot', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1841, 'reqmo_pilot', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1842, 'reqmo_pilot', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1843, 'reqmo_pilot', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1844, 'reqmo_pilot', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1845, 'reqmo_pilot', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1846, 'rivoli', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1847, 'service', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1848, 'service', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1849, 'service', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1850, 'service_categorie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1851, 'service_categorie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1852, 'service_categorie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1853, 'servitude_ligne', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1854, 'servitude_point', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1855, 'servitude_surfacique', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1856, 'signataire_arrete', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1857, 'signataire_arrete', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1858, 'signataire_arrete', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1859, 'sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1860, 'sitadel_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1861, 'sitadel_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1862, 'sitadel_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1863, 'sitadel_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1864, 'sitadel_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1865, 'sitadel_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1866, 'suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1867, 'suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1868, 'suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1869, 'suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1870, 'suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1871, 'suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1872, 'suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1873, 'taxe_amenagement', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1874, 'transition', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1875, 'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1876, 'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1877, 'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1878, 'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1879, 'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1880, 'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1881, 'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1882, 'workflows', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'));
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1883, 'workflows', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL'));

--
-- Positionnement de la séquence
--

SELECT pg_catalog.setval('om_droit_seq', 1884, false);


--
-- END - [#8538] Réinitialisation des permissions
--
