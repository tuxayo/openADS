--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a11
--
-- XXX Ce fichier doit être renommé en v3.14.0-a11.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.14.0-a11.sql 3062 2014-06-04 20:27:03Z vpihour $
--------------------------------------------------------------------------------

-- Ajout d'un index sur le champ dossier_autorisation de la table dossier
CREATE INDEX dossier_dossier_autorisation ON dossier (dossier_autorisation);

-- Ajout des droits de géolocalisation
-- aux profils DIVISIONNAIRE et CHEF DE SERVICE
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'geolocalisation_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_localiser-sig-externe',(SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_localiser-sig-externe' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'geolocalisation_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_localiser-sig-externe',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_localiser-sig-externe' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );

-- Ajout d'un booleen incomplet_notifie
ALTER TABLE dossier ADD COLUMN incomplet_notifie boolean DEFAULT false;
COMMENT ON COLUMN dossier.incomplet_notifie IS 'Définit si le dossier est en état incomplet notifié';
ALTER TABLE instruction ADD COLUMN archive_incomplet_notifie boolean DEFAULT false;
COMMENT ON COLUMN instruction.archive_incomplet_notifie IS 'Archive du champ dossier.incomplet_notifie';