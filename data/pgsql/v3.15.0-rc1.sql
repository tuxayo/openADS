-- lien om_collectivite sur le dossier d'instruction
ALTER TABLE openads.dossier
  ADD COLUMN om_collectivite integer NOT NULL DEFAULT 1;
ALTER TABLE openads.dossier
  ADD FOREIGN KEY (om_collectivite) REFERENCES openads.om_collectivite (om_collectivite) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN openads.dossier.om_collectivite IS 'lien vers la collectivité concernée';

-- lien om_collectivite sur le dossier d'autorisation
ALTER TABLE openads.dossier_autorisation
  ADD COLUMN om_collectivite integer NOT NULL DEFAULT 1;
ALTER TABLE openads.dossier_autorisation
  ADD FOREIGN KEY (om_collectivite) REFERENCES openads.om_collectivite (om_collectivite) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN openads.dossier_autorisation.om_collectivite IS 'lien vers la collectivité concernée';

-- lien om_collectivite sur la demande
ALTER TABLE openads.demande
  ADD COLUMN om_collectivite integer NOT NULL DEFAULT 1;
ALTER TABLE openads.demande
  ADD FOREIGN KEY (om_collectivite) REFERENCES openads.om_collectivite (om_collectivite) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN openads.demande.om_collectivite IS 'lien vers la collectivité concernée';

-- lien om_collectivite sur le service
ALTER TABLE openads.service
  ADD COLUMN om_collectivite integer NOT NULL DEFAULT 1;
ALTER TABLE openads.service
  ADD FOREIGN KEY (om_collectivite) REFERENCES openads.om_collectivite (om_collectivite) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN openads.service.om_collectivite IS 'lien vers la collectivité concernée';

-- lien om_collectivite sur la contrainte
ALTER TABLE openads.contrainte
  ADD COLUMN om_collectivite integer NOT NULL DEFAULT 1;
ALTER TABLE openads.contrainte
  ADD FOREIGN KEY (om_collectivite) REFERENCES openads.om_collectivite (om_collectivite) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN openads.contrainte.om_collectivite IS 'lien vers la collectivité concernée';

-- lien om_collectivite sur les affectations automatiques
ALTER TABLE openads.affectation_automatique
  ADD COLUMN om_collectivite integer NOT NULL DEFAULT 1;
ALTER TABLE openads.affectation_automatique
  ADD FOREIGN KEY (om_collectivite) REFERENCES openads.om_collectivite (om_collectivite) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN openads.affectation_automatique.om_collectivite IS 'lien vers la collectivité concernée';

-- ajout d'une commune multi
INSERT INTO openads.om_collectivite(om_collectivite, libelle, niveau) VALUES ('2', 'agglo', 2);
-- passage de 'admin' à l'agglo
UPDATE openads.om_utilisateur SET om_collectivite='2' WHERE login='admin';

-- Ajout du droit pour consulter l'attestion réglementaire
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_attestation_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_attestation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_attestation_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_attestation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_attestation_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_attestation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );

-- Ajout du droit pour consulter le registre réglementaire
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_registre_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_registre_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_registre_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_registre_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_registre_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_registre_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );

----
-- Ajout du paramètre option_sig pour la multicollectivité
----
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'),'option_sig', 'aucun', (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2')
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'option_sig' AND om_collectivite = (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2')
    );

-- Lien om_collectivite sur le demandeur
ALTER TABLE demandeur
  ADD COLUMN om_collectivite integer NOT NULL DEFAULT 1;
ALTER TABLE demandeur
  ADD FOREIGN KEY (om_collectivite) REFERENCES om_collectivite(om_collectivite) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN demandeur.om_collectivite IS 'Lien vers la collectivité concernée.';

----
-- Ajout du paramètre id_etat_initial_dossier_autorisation pour la multicollectivité
----
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'),'id_etat_initial_dossier_autorisation', '1', (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2')
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'id_etat_initial_dossier_autorisation' AND om_collectivite = (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2')
    );

----
-- Passage en multicollectivité des paramètres option_erp, option_referentiel_arrete, département, commune et région
----
UPDATE om_parametre SET om_collectivite = (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2')
WHERE libelle = 'option_erp' OR libelle = 'option_referentiel_arrete' OR libelle = 'departement' OR libelle = 'commune' OR libelle = 'region';

-- Lien om_collectivite sur les signataires
ALTER TABLE signataire_arrete
  ADD COLUMN om_collectivite integer NOT NULL DEFAULT 1;
ALTER TABLE signataire_arrete
  ADD FOREIGN KEY (om_collectivite) REFERENCES om_collectivite(om_collectivite) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN signataire_arrete.om_collectivite IS 'Lien vers la collectivité concernée.';

-- Lien om_collectivite sur les directions
ALTER TABLE direction
  ADD COLUMN om_collectivite integer NOT NULL DEFAULT 1;
ALTER TABLE direction
  ADD FOREIGN KEY (om_collectivite) REFERENCES om_collectivite(om_collectivite) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN direction.om_collectivite IS 'Lien vers la collectivité concernée.';

-- Lien om_collectivite sur les bibles
ALTER TABLE bible
  ADD COLUMN om_collectivite integer NOT NULL DEFAULT 1;
ALTER TABLE bible
  ADD FOREIGN KEY (om_collectivite) REFERENCES om_collectivite(om_collectivite) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN bible.om_collectivite IS 'Lien vers la collectivité concernée.';

UPDATE om_lettretype SET om_collectivite = (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2');
UPDATE om_etat SET om_collectivite = (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2');
UPDATE om_sousetat SET om_collectivite = (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2');
UPDATE signataire_arrete SET om_collectivite = (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2');


-- Supprime les valeurs par défaut des champs om_collectivite de toutes les
-- tables
ALTER TABLE demande ALTER COLUMN om_collectivite DROP DEFAULT;
ALTER TABLE demandeur ALTER COLUMN om_collectivite DROP DEFAULT;
ALTER TABLE affectation_automatique ALTER COLUMN om_collectivite DROP DEFAULT;
ALTER TABLE bible ALTER COLUMN om_collectivite DROP DEFAULT;
ALTER TABLE contrainte ALTER COLUMN om_collectivite DROP DEFAULT;
ALTER TABLE direction ALTER COLUMN om_collectivite DROP DEFAULT;
ALTER TABLE dossier ALTER COLUMN om_collectivite DROP DEFAULT;
ALTER TABLE dossier_autorisation ALTER COLUMN om_collectivite DROP DEFAULT;
ALTER TABLE service ALTER COLUMN om_collectivite DROP DEFAULT;
ALTER TABLE signataire_arrete ALTER COLUMN om_collectivite DROP DEFAULT;
