--
-- START - [#8623] Problèmes de droits sur les profils
--

-- Profil INSTRUCTEUR
-- Instruction :
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'instruction_supprimer', 3);
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'instruction_supprimer', 11);
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'instruction_supprimer', 14);
-- Consultation :
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'consultation_modifier', 3);
-- Documents numérisés
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'document_numerise_ajouter', 14);
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'document_numerise_ajouter', 11);

-- Profil QUALIFICATEUR
-- Instruction :
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'instruction_finaliser', 7);
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'instruction_definaliser', 7);
-- Consultation :
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'consultation_modifier', 7);
-- Documents numérisés
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'document_numerise_ajouter', 7);

-- Profil SUIVI
-- Commission
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'dossier_commission_modifier_lu', 5);
-- Instruction
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'instruction_om_fichier_instruction_telecharger', 5);

-- Profil ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'consultation_supprimer_avec_avis_rendu', 19);
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'consultation_supprimer_avec_avis_rendu', 16);

--
-- END - [#8623] Problèmes de droits sur les profils
--
