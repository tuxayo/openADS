--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a13
--
-- XXX Ce fichier doit être renommé en v3.14.0-a13.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.14.0-a13.sql 3099 2014-07-10 18:42:13Z vpihour $
--------------------------------------------------------------------------------
-- Mise à niveau 4.4.0
ALTER TABLE om_collectivite ALTER COLUMN om_collectivite TYPE integer;
ALTER TABLE om_droit ALTER COLUMN om_droit TYPE integer;
ALTER TABLE om_utilisateur ALTER COLUMN om_utilisateur TYPE integer;
ALTER TABLE om_utilisateur ALTER COLUMN om_collectivite TYPE integer;
ALTER TABLE om_etat ALTER COLUMN om_etat TYPE integer;
ALTER TABLE om_etat ALTER COLUMN om_collectivite TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN om_lettretype TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN om_collectivite TYPE integer;
ALTER TABLE om_parametre ALTER COLUMN om_parametre TYPE integer;
ALTER TABLE om_parametre ALTER COLUMN om_collectivite TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN om_sousetat TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN om_collectivite TYPE integer;

ALTER TABLE om_etat ALTER COLUMN logoleft TYPE integer;
ALTER TABLE om_etat ALTER COLUMN logotop TYPE integer;
ALTER TABLE om_etat ALTER COLUMN titreleft TYPE integer;
ALTER TABLE om_etat ALTER COLUMN titretop TYPE integer;
ALTER TABLE om_etat ALTER COLUMN titrelargeur TYPE integer;
ALTER TABLE om_etat ALTER COLUMN titrehauteur TYPE integer;

ALTER TABLE om_lettretype ALTER COLUMN logoleft TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN logotop TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN titreleft TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN titretop TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN titrelargeur TYPE integer;
ALTER TABLE om_lettretype ALTER COLUMN titrehauteur TYPE integer;

ALTER TABLE om_sousetat ALTER COLUMN titrehauteur TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN titretaille TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN intervalle_debut TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN intervalle_fin TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN entete_hauteur TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN tableau_largeur TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN tableau_fontaille TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_hauteur TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_fontaille_total TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_hauteur_total TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_fontaille_moyenne TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_hauteur_moyenne TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_fontaille_nbr TYPE integer;
ALTER TABLE om_sousetat ALTER COLUMN cellule_hauteur_nbr TYPE integer;

ALTER TABLE om_sig_map ALTER COLUMN actif TYPE boolean USING CASE WHEN actif='Oui' THEN true ELSE false END;
ALTER TABLE om_parametre ALTER COLUMN libelle TYPE character varying(100);

--Ajout de droits au profil visudadi
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'dossier_instruction_localiser-sig-externe',(SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_localiser-sig-externe' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'donnees_techniques_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'consultation_om_fichier_consultation_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_om_fichier_consultation_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'consultation_fichier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
    );

-- Suppression de droits au profil chef de service
DELETE FROM om_droit
WHERE libelle = 'demande_autre_dossier_ajouter' AND
  om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE');

DELETE FROM om_droit
WHERE libelle = 'demande_autre_dossier_tab' AND
      om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE');

DELETE FROM om_droit
WHERE libelle = 'demande_dossier_encours_ajouter' AND
      om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE');

DELETE FROM om_droit
WHERE libelle = 'demande_dossier_encours_tab' AND
      om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE');

DELETE FROM om_droit
WHERE libelle = 'demande_nouveau_dossier_ajouter' AND
      om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE');

DELETE FROM om_droit
WHERE libelle = 'menu_guichet_unique' AND
      om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE');

-- Ajout des arrondissements dans les dossiers d'autorisation
UPDATE dossier_autorisation SET
  arrondissement =
    (SELECT arrondissement
     FROM arrondissement
     WHERE code_postal = terrain_adresse_code_postal);

-- Ajout d'un nouveau sous-état : commission
INSERT INTO om_sousetat VALUES (nextval('om_sousetat_seq'), 1, 'commission', 'Liste des commissions / dossier', true, 'Commission', 8, 'helvetica', 'B', 10, '0', 'L', '0', '243-246-246', '0-0-0', 10, 5, '1', '1', '0|0|0|0', 8, 'TLB|LTBR|LTBR|LTBR', 'C|C|C|C', '145-184-184', '0-0-0', 195, '1', 8, '0-0-0', '243-246-246', '255-255-255', '1', 10, '55|25|20|95', 'LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR', 'L|C|C|L', '1', 8, 15, '196-213-213', 'TBL|TBLR|TBLR|TBLR', 'L|C|C|C', '1', 8, 5, '196-213-213', 'BTL|BTLR|BTLR|BTLR', 'L|C|C|C', '1', 8, 10, '255-255-255', 'TBL|TBLR|TBLR|TBLR', 'L|C|C|C', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT
    commission_type.libelle,
    commission.code,
    TO_CHAR(commission.date_commission,''DD/MM/YYYY'') as date_commission,
    dossier_commission.avis
FROM &DB_PREFIXEdossier_commission
LEFT JOIN &DB_PREFIXEcommission
ON
dossier_commission.commission = commission.commission
LEFT JOIN &DB_PREFIXEcommission_type
ON
commission.commission_type = commission_type.commission_type
WHERE dossier_commission.dossier = ''&idx'' and dossier_commission.commission IS NOT NULL');

-- Ajout du sous-etat commission dans l'état du récapitulatif du dossier
UPDATE om_etat SET corps_om_htmletatex = '<p style="text-align: justify;"><span style="font-size: 10px;"><span style="font-family: times;"><span style="font-style: italic;">Surface hors- oeuvre      brute : <span style="font-weight: bold;">[shob] m2</span>     nette : <span style="font-weight: bold;">[shon] m2</span>   hauteur :   <span style="font-weight: bold;">[hauteur] m </span><br />Nb de logements : <span style="font-weight: bold;">[co_tot_log_nb_donnees_techniques]</span>     Nb de bâtiments : <span style="font-weight: bold;">[batiment_nombre]</span><br /><br />Travaux       : <span style="font-weight: bold;">[travaux]</span><br /><br />Date de dépot : <span style="font-weight: bold;">[date_depot_dossier]</span><br />Date limite de notification : <span style="font-weight: bold;">[date_notification_delai]</span><br />Date limite d''instruction: <span style="font-weight: bold;">[date_limite_dossier]</span></span></span></span></p>
<p><br /><span id="instruction" class="mce_sousetat">instruction</span><br /><span id="consultation" class="mce_sousetat">consultation</span></p>
<p><span id="commission" class="mce_sousetat">Liste des commissions / dossier</span></p>
<p> </p>' WHERE id = 'dossier';

-- Ajout du champ de fusion architecte

UPDATE om_requete SET
requete = 'SELECT

    -- Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,

    avis_decision.libelle as libelle_avis_decision,

    -- Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    -- Noms des signataires
    division.chef as division_chef,
    direction.chef as direction_chef,
    direction.libelle as libelle_direction,
    direction.description as description_direction,

    -- Adresse du terrain du dossier d''instruction
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    CASE 
        WHEN dossier.terrain_adresse_bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', dossier.terrain_adresse_bp)
    END as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    CASE 
        WHEN dossier.terrain_adresse_cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', dossier.terrain_adresse_cedex)
    END as terrain_adresse_cedex_dossier,
    arrondissement.libelle as libelle_arrondissement,

    -- Nom et prénom de l''architecte
    CONCAT(architecte.prenom||'' '', architecte.nom) as architecte,

    -- Coordonnées du pétitionnaire principal
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as nom_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_1.particulier_nom, '' '', petitionnaire_1.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
    END as nom_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_2.particulier_nom, '' '', petitionnaire_2.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
    END as nom_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_3.particulier_nom, '' '', petitionnaire_3.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
    END as nom_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_4.particulier_nom, '' '', petitionnaire_4.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
    END as nom_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_5.particulier_nom, '' '', petitionnaire_5.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
    END as nom_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,

    -- Coordonnées du délégataire
    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as nom_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complement_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    CASE 
        WHEN delegataire.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', delegataire.bp)
    END as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    CASE 
        WHEN delegataire.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', delegataire.cedex)
    END as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', petitionnaire_principal.bp)
            END
        ELSE 
            CASE 
                WHEN delegataire.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', delegataire.bp)
            END
    END as bp_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
            END
        ELSE 
            CASE 
                WHEN delegataire.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', delegataire.cedex)
            END
    END as cedex_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    -- Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    
    -- Données générales des données techniques
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    REGEXP_REPLACE(CONCAT(
        CASE
            WHEN donnees_techniques.su_cstr_shon1 IS NULL
            THEN ''''
            ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon2 IS NULL
            THEN ''''
            ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon3 IS NULL
            THEN ''''
            ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon4 IS NULL
            THEN ''''
            ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon5 IS NULL
            THEN ''''
            ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon6 IS NULL
            THEN ''''
            ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon7 IS NULL
            THEN ''''
            ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon8 IS NULL
            THEN ''''
            ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
        END, 
        CASE
            WHEN donnees_techniques.su_cstr_shon9 IS NULL
            THEN ''''
            ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
        END
    ), '' / $'', '''') as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques

FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as delegataire
    ON
        lien_dossier_demandeur.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
LEFT JOIN (
    SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
    FROM &DB_PREFIXElien_dossier_demandeur
    LEFT JOIN &DB_PREFIXEdossier
        ON lien_dossier_demandeur.dossier=dossier.dossier 
        AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
    WHERE dossier.dossier = ''&idx''
    GROUP BY lien_dossier_demandeur.dossier
) as sub_petitionnaire_autre
ON dossier.dossier = sub_petitionnaire_autre.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4]
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5]
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN
    &DB_PREFIXEdirection
    ON
        division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN
    &DB_PREFIXEarchitecte
    ON
    donnees_techniques.architecte = architecte.architecte
WHERE dossier.dossier = ''&idx''

',
merge_fields = '-- Données générales du dossier d''instruction
[libelle_dossier]    
[libelle_da]    
[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]
[delai_dossier]
[terrain_references_cadastrales_dossier]
[libelle_avis_decision]

-- Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

-- Noms des signataires
[division_chef]
[direction_chef]
[libelle_direction]
[description_direction]

-- Adresse du terrain du dossier d''instruction
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]
[libelle_arrondissement]

-- Nom et prénom de l''architecte
[architecte]

-- Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''a 5)
[numero_petitionnaire_1](jusqu''a 5)    [voie_petitionnaire_1](jusqu''a 5)    
[complement_petitionnaire_1](jusqu''a 5)
[lieu_dit_petitionnaire_1](jusqu''a 5)    [bp_petitionnaire_1](jusqu''a 5)
[code_postal_petitionnaire_1](jusqu''a 5)    [localite_petitionnaire_1](jusqu''a 5)    
[cedex_petitionnaire_1](jusqu''a 5)
[pays_petitionnaire_1](jusqu''a 5)

-- Coordonnées du délégataire
[nom_delegataire]
[numero_delegataire]    [voie_delegataire]    [complement_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire]

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant]

-- Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]

-- Données générales des données techniques
[co_projet_desc_donnees_techniques]    [co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_projet_desc_donnees_techniques]    [am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
[dm_projet_desc_donnees_techniques]
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]
'
WHERE code = 'dossier';

UPDATE om_sousetat SET
om_sql = 'SELECT
    row_number() OVER(ORDER BY dossier_commission.dossier DESC) cel1,
    CONCAT(
        ''Dossier:      '', dossier.dossier_libelle, ''    '', ''Rap.  '', instructeur.nom, ''
'',
        ''Demandeur :   '', CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom) WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' '', demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret) END,''
'',
        CONCAT(demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp) , ''
'',
        ''Adresse travaux : '', TRIM(CONCAT(dossier.terrain_adresse_voie_numero,'' '',dossier.terrain_adresse_voie,'' '', dossier.terrain_adresse_lieu_dit,'' '', dossier.terrain_adresse_code_postal,'' '', dossier.terrain_adresse_localite,'' '', dossier.terrain_adresse_bp,'' '', dossier.terrain_adresse_cedex)), ''
'',
        ''Quartier:     '', arrondissement.libelle, ''
'',
        ''Dépot:        '', to_char(dossier.date_depot,''DD/MM/YYYY''), ''         Délai :'', to_char(dossier.date_limite,''DD/MM/YYYY''), ''
'',
        ''Opération:    '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_projet_desc WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN CONCAT(donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc) WHEN dossier_autorisation_type_detaille.code=''PD'' THEN    donnees_techniques.dm_projet_desc WHEN dossier_autorisation_type_detaille.code=''PA'' THEN donnees_techniques.am_projet_desc WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN CONCAT(donnees_techniques.am_projet_desc, donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc)  END, ''
'',
        ''SHON:         '', donnees_techniques.su_tot_shon_tot, ''     Nbr logements: '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PD'' THEN donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb END, ''
'',
        ''Architecte:        '', architecte.prenom||'' '', architecte.nom,''
'',
        ''nb log T1: '', donnees_techniques.co_log_1p_nb, '' T2: '', donnees_techniques.co_log_2p_nb, '' T3: '', donnees_techniques.co_log_3p_nb, '' T4: '', donnees_techniques.co_log_4p_nb, '' T5: '', donnees_techniques.co_log_5p_nb, '' T6: '', donnees_techniques.co_log_6p_nb, '' Nbr places stationnement: '', donnees_techniques.co_statio_place_nb, ''
''
    ) as cel2,
    '''' as motivation
FROM
    &DB_PREFIXEcommission
LEFT JOIN
    &DB_PREFIXEdossier_commission
    ON
        commission.commission=dossier_commission.commission
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        dossier_commission.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur
    ON
        lien_dossier_demandeur.demandeur = demandeur.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        demandeur.particulier_civilite = civilite.civilite OR demandeur.personne_morale_civilite = civilite.civilite
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
        dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN
    &DB_PREFIXEarchitecte
    ON
    donnees_techniques.architecte = architecte.architecte
WHERE
    commission.commission = &idx'
WHERE id = 'commission_ordre_jour';

UPDATE om_sousetat SET
om_sql = 'SELECT
    row_number() OVER(ORDER BY dossier_commission.dossier DESC) cel1,
    CONCAT(
        ''Dossier:      '', dossier.dossier_libelle, ''    '', ''Rap.  '', instructeur.nom, ''
'',
        ''Demandeur :   '', CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom) WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' '', demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret) END,''
'',
        CONCAT(demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp) , ''
'',
        ''Adresse travaux : '', TRIM(CONCAT(dossier.terrain_adresse_voie_numero,'' '',dossier.terrain_adresse_voie,'' '', dossier.terrain_adresse_lieu_dit,'' '', dossier.terrain_adresse_code_postal,'' '', dossier.terrain_adresse_localite,'' '', dossier.terrain_adresse_bp,'' '', dossier.terrain_adresse_cedex)), ''
'',
        ''Quartier:     '', arrondissement.libelle, ''
'',
        ''Dépot:        '', to_char(dossier.date_depot,''DD/MM/YYYY''), ''         Délai :'', to_char(dossier.date_limite,''DD/MM/YYYY''), ''
'',
        ''Opération:    '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_projet_desc WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN CONCAT(donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc) WHEN dossier_autorisation_type_detaille.code=''PD'' THEN    donnees_techniques.dm_projet_desc WHEN dossier_autorisation_type_detaille.code=''PA'' THEN donnees_techniques.am_projet_desc WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN CONCAT(donnees_techniques.am_projet_desc, donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc)  END, ''
'',
        ''SHON:         '', donnees_techniques.su_tot_shon_tot, ''     Nbr logements: '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PD'' THEN donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb END, ''
'',
        ''Architecte:        '', architecte.prenom||'' '', architecte.nom,''
'',
        ''nb log T1: '', donnees_techniques.co_log_1p_nb, '' T2: '', donnees_techniques.co_log_2p_nb, '' T3: '', donnees_techniques.co_log_3p_nb, '' T4: '', donnees_techniques.co_log_4p_nb, '' T5: '', donnees_techniques.co_log_5p_nb, '' T6: '', donnees_techniques.co_log_6p_nb, '' Nbr places stationnement: '', donnees_techniques.co_statio_place_nb, ''
''
    ) as cel2,
    dossier_commission.motivation as motivation
FROM
    &DB_PREFIXEcommission
LEFT JOIN
    &DB_PREFIXEdossier_commission
    ON
        commission.commission=dossier_commission.commission
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        dossier_commission.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur
    ON
        lien_dossier_demandeur.demandeur = demandeur.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        demandeur.particulier_civilite = civilite.civilite OR demandeur.personne_morale_civilite = civilite.civilite
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
        dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN
    &DB_PREFIXEarchitecte
    ON
    donnees_techniques.architecte = architecte.architecte
WHERE
    commission.commission = &idx'
WHERE id = 'commission_proposition_ordre_jour';

UPDATE om_sousetat SET
om_sql = 'SELECT
    row_number() OVER(ORDER BY dossier_commission.dossier DESC) cel1,
    CONCAT(
        ''Dossier:      '', dossier.dossier_libelle, ''    '', ''Rap.  '', instructeur.nom, ''
'',
        ''Demandeur :   '', CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom) WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' '', demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret) END,''
'',
        CONCAT(demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp) , ''
'',
        ''Adresse travaux : '', TRIM(CONCAT(dossier.terrain_adresse_voie_numero,'' '',dossier.terrain_adresse_voie,'' '', dossier.terrain_adresse_lieu_dit,'' '', dossier.terrain_adresse_code_postal,'' '', dossier.terrain_adresse_localite,'' '', dossier.terrain_adresse_bp,'' '', dossier.terrain_adresse_cedex)), ''
'',
        ''Quartier:     '', arrondissement.libelle, ''
'',
        ''Dépot:        '', to_char(dossier.date_depot,''DD/MM/YYYY''), ''         Délai :'', to_char(dossier.date_limite,''DD/MM/YYYY''), ''
'',
        ''Opération:    '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_projet_desc WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN CONCAT(donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc) WHEN dossier_autorisation_type_detaille.code=''PD'' THEN    donnees_techniques.dm_projet_desc WHEN dossier_autorisation_type_detaille.code=''PA'' THEN donnees_techniques.am_projet_desc WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN CONCAT(donnees_techniques.am_projet_desc, donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc)  END, ''
'',
        ''SHON:         '', donnees_techniques.su_tot_shon_tot, ''     Nbr logements: '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PD'' THEN donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb END, ''
'',
        ''Architecte:        '', architecte.prenom||'' '', architecte.nom,''
'',
        ''nb log T1: '', donnees_techniques.co_log_1p_nb, '' T2: '', donnees_techniques.co_log_2p_nb, '' T3: '', donnees_techniques.co_log_3p_nb, '' T4: '', donnees_techniques.co_log_4p_nb, '' T5: '', donnees_techniques.co_log_5p_nb, '' T6: '', donnees_techniques.co_log_6p_nb, '' Nbr places stationnement: '', donnees_techniques.co_statio_place_nb, ''
''
    ) as cel2,
    dossier_commission.avis as motivation
FROM
    &DB_PREFIXEcommission
LEFT JOIN
    &DB_PREFIXEdossier_commission
    ON
        commission.commission=dossier_commission.commission
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        dossier_commission.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
        AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur
    ON
        lien_dossier_demandeur.demandeur = demandeur.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        demandeur.particulier_civilite = civilite.civilite OR demandeur.personne_morale_civilite = civilite.civilite
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
        dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN
    &DB_PREFIXEarchitecte
    ON
    donnees_techniques.architecte = architecte.architecte
WHERE
    commission.commission = &idx'
WHERE id = 'commission_compte_rendu';