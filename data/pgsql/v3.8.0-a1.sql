--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.8.0-a1
--
-- XXX Ce fichier doit être renommé en v3.8.0-a1.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id$
-------------------------------------------------------------------------------

---
--- Ajout de mail pour le service "Direction de l'Eau et de l'Assainissement"
--- pour les tests
---
UPDATE service SET email = 'support@atreal.fr', notification_email = 't' WHERE libelle = 'Direction de l''Eau et de l''Assainissement';

-- Ajout de la clé étrangère dossier dans la table lot
ALTER TABLE lot ADD COLUMN dossier 	character varying(20);
ALTER TABLE ONLY lot 
    ADD CONSTRAINT lot_dossier_fkey FOREIGN KEY (dossier) REFERENCES dossier(dossier);
-- Mise à jour des données existantes
UPDATE lot SET dossier = (SELECT dossier FROM lien_dossier_lot WHERE lien_dossier_lot.lot = lot.lot);
-- suppression de la table lien_dossier_lot
DROP TABLE lien_dossier_lot;

---
--- Possibilité de modifier l'autorité compétente via un évènement d'instruction
---
-- Ajout du champ autorite_competente dans la table evenement
ALTER TABLE evenement ADD COLUMN autorite_competente integer DEFAULT NULL;
ALTER TABLE ONLY evenement 
    ADD CONSTRAINT evenement_autorite_competente_fkey FOREIGN KEY (autorite_competente)
    REFERENCES autorite_competente(autorite_competente);
-- Ajout de son champ archive dans la table instruction
ALTER TABLE instruction ADD COLUMN archive_autorite_competente integer DEFAULT NULL;
-- Ajout du champ autorite_competente dans la table instruction
ALTER TABLE instruction ADD COLUMN autorite_competente integer DEFAULT NULL;
ALTER TABLE ONLY instruction 
    ADD CONSTRAINT instruction_autorite_competente_fkey FOREIGN KEY (autorite_competente)
    REFERENCES autorite_competente(autorite_competente);
-- Ajout du champ regle_autorite_competente dans la table action
ALTER TABLE action ADD COLUMN regle_autorite_competente character varying(60) DEFAULT NULL;


---
--- Modification des om_sousetat concernant la modification du champ complement
--- de la table dossier, dossier_autorisation et demande
---
UPDATE om_sousetat SET om_sql = 
'SELECT
    dossier.dossier as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, ''\n'', 
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    instruction.date_evenement = (SELECT MAX(instr.date_evenement) FROM &DB_PREFIXEinstruction instr WHERE instr.dossier = dossier.dossier) AND
    (LOWER(evenement.type) != ''arrete'' OR LOWER(evenement.type) IS NULL ) AND
    instruction.date_envoi_signature = ''&date_bordereau''
ORDER BY
    dossier.dossier'
WHERE id = 'bordereau_courriers_signature_maire';

---
--- Ajout d'un libelle pour les table dossier et dossier_autorisation pour un 
--- identifiant plus lisible par les utilisateurs
---
ALTER TABLE dossier_autorisation ADD COLUMN dossier_autorisation_libelle character varying(20);
ALTER TABLE dossier ADD COLUMN dossier_libelle character varying(25);

---
--- Utilisation du nouveau champ dossier_libelle et dossier_autorisation_libelle
--- dans les tables om_requête, om_sousetat et om_lettretype
---
UPDATE om_requete SET requete =
'SELECT 


    service.libelle as service, 
    service.adresse, 
    service.adresse2, 
    service.cp, 
    service.ville, 
    dossier.dossier_libelle,
    replace(dossier.terrain_references_cadastrales, '';'', '' ''),
    dossier_autorisation_type_detaille.libelle as nature, 
    civilite.libelle as demandeur_civilite, 
    to_char(dossier.date_depot,''DD/MM/YYYY'') as date_depot, 
    to_char(dossier.date_rejet,''DD/MM/YYYY'') as date_rejet, 
    to_char(consultation.date_envoi,''DD/MM/YYYY'') as date_envoi, 
    consultation.code_barres as code_barres,
    donnees_techniques.co_projet_desc,
    donnees_techniques.am_projet_desc,
    donnees_techniques.dm_projet_desc,
    donnees_techniques.am_lot_max_nb,
    donnees_techniques.am_lot_max_shon,
    donnees_techniques.su_cstr_shon_tot,
    donnees_techniques.su_demo_shon_tot,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9),
    donnees_techniques.co_tot_log_nb,
    donnees_techniques.co_statio_place_nb
    
    
FROM 


    &DB_PREFIXEconsultation 
    LEFT JOIN &DB_PREFIXEservice 
        ON service.service=consultation.service 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=consultation.dossier 
    LEFT JOIN &DB_PREFIXEdonnees_techniques 
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdossier_autorisation 
        ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation 
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur 
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        
         
WHERE 


    consultation.consultation = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE'
WHERE code ='consultation';

UPDATE om_requete SET requete =
'SELECT
    --
    -- INFORMATIONS CONCERNANT LE DOSSIER
    --

    dossier.dossier_libelle as dossier,
    dossier_autorisation.dossier_autorisation_libelle,

    dossier_autorisation_type_detaille.code as code_type_da_det,
    dossier_autorisation_type_detaille.libelle as libelle_type_da_det,
    dossier_autorisation_type.code as code_type_da,
    dossier_autorisation_type.libelle as libelle_type_da,
    dossier_instruction_type.code as code_type_di,
    dossier_instruction_type.libelle as libelle_type_di,
    dossier.delai as delai_di,
    replace(dossier.terrain_references_cadastrales, '';'', '' ''),

    avis_decision.libelle as decision,

    --
    -- INSTRUCTEUR
    --

    instructeur.nom as instructeur_nom,
    instructeur.telephone as instructeur_telephone,
    division.code as instructeur_division,
    om_utilisateur.email as instructeur_email,

    --
    -- SIGNATAIRES
    --
    division.chef as division_chef,
    direction.chef as direction_chef,

    --
    -- ADRESSE TERRAIN
    --

    dossier.terrain_adresse_voie_numero,
    dossier.terrain_adresse_voie as adresse_voie,
    dossier.terrain_adresse_lieu_dit,
    dossier.terrain_adresse_bp,
    dossier.terrain_adresse_code_postal,
    dossier.terrain_adresse_localite as terrain_adresse_ville,
    dossier.terrain_adresse_cedex,

    arrondissement.libelle as arrondissement,

    --
    -- PETITIONNAIRE PRINCIPAL
    --

    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as petitionnaire_principal_nom,
    petitionnaire_principal.numero as petitionnaire_principal_numero,
    petitionnaire_principal.voie as petitionnaire_principal_voie,
    petitionnaire_principal.complement as petitionnaire_principal_complement,
    petitionnaire_principal.lieu_dit as petitionnaire_principal_lieu_dit,
    petitionnaire_principal.bp as petitionnaire_principal_bp,
    petitionnaire_principal.code_postal as petitionnaire_principal_code_postal,
    petitionnaire_principal.localite as petitionnaire_principal_ville,
    petitionnaire_principal.cedex as petitionnaire_principal_cedex,
    petitionnaire_principal.pays as petitionnaire_principal_pays,

    --
    -- DELEGATAIRE
    --

    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as delegataire_nom,
    delegataire.numero as delegataire_numero,
    delegataire.voie as delegataire_voie,
    delegataire.complement as delegataire_complement,
    delegataire.lieu_dit as delegataire_lieu_dit,
    delegataire.bp as delegataire_bp,
    delegataire.code_postal as delegataire_code_postal,
    delegataire.localite as delegataire_ville,
    delegataire.cedex as delegataire_cedex,
    delegataire.pays as delegataire_pays,

    --
    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    --

    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as correspondant_nom,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as correspondant_numero,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as correspondant_voie,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as correspondant_complement,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as correspondant_lieu_dit,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.bp
        ELSE delegataire.bp
    END as correspondant_bp,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as correspondant_code_postal,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as correspondant_ville,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.cedex
        ELSE delegataire.cedex
    END as correspondant_cedex,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as correspondant_pays,

    --
    -- DATES DI
    --

    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite,
    
    --
    -- DONNÉES TECHNIQUES
    --
    donnees_techniques.co_projet_desc,
    donnees_techniques.am_projet_desc,
    donnees_techniques.dm_projet_desc,
    donnees_techniques.am_lot_max_nb,
    donnees_techniques.am_lot_max_shon,
    donnees_techniques.su_cstr_shon_tot,
    donnees_techniques.su_demo_shon_tot,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9),
    donnees_techniques.co_tot_log_nb,
    donnees_techniques.co_statio_place_nb

FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as delegataire
    ON
        lien_dossier_demandeur.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN
    &DB_PREFIXEdirection
    ON
        division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
WHERE dossier.dossier = ''&idx'''
WHERE code ='dossier';

UPDATE om_requete SET requete =
'SELECT

    --
    -- CHAMPS DE L''EVENEMENT D''INSTRUCTION

    instruction.complement,
    instruction.complement2,
    instruction.code_barres as code_barres,
    om_lettretype.libelle as instruction_objet,

    --
    -- INFORMATIONS CONCERNANT LE DOSSIER
    --

    dossier.dossier_libelle as dossier,
    dossier_autorisation.dossier_autorisation_libelle,

    dossier_autorisation_type_detaille.code as code_type_da_det,
    dossier_autorisation_type_detaille.libelle as libelle_type_da_det,
    dossier_autorisation_type.code as code_type_da,
    dossier_autorisation_type.libelle as libelle_type_da,
    dossier_instruction_type.code as code_type_di,
    dossier_instruction_type.libelle as libelle_type_di,
    dossier.delai as delai_di,
    replace(dossier.terrain_references_cadastrales, '';'', '' ''),

    avis_decision.libelle as decision,

    --
    -- INSTRUCTEUR
    --

    instructeur.nom as instructeur_nom,
    instructeur.telephone as instructeur_telephone,
    division.code as instructeur_division,
    om_utilisateur.email as instructeur_email,

    --
    -- ADRESSE TERRAIN
    --

    dossier.terrain_adresse_voie_numero,
    dossier.terrain_adresse_voie as adresse_voie,
    dossier.terrain_adresse_lieu_dit,
    dossier.terrain_adresse_bp,
    dossier.terrain_adresse_code_postal,
    dossier.terrain_adresse_localite as terrain_adresse_ville,
    dossier.terrain_adresse_cedex,

    arrondissement.libelle as arrondissement,

    --
    -- PETITIONNAIRE PRINCIPAL
    --

    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as petitionnaire_principal_nom,
    petitionnaire_principal.numero as petitionnaire_principal_numero,
    petitionnaire_principal.voie as petitionnaire_principal_voie,
    petitionnaire_principal.complement as petitionnaire_principal_complement,
    petitionnaire_principal.lieu_dit as petitionnaire_principal_lieu_dit,
    petitionnaire_principal.bp as petitionnaire_principal_bp,
    petitionnaire_principal.code_postal as petitionnaire_principal_code_postal,
    petitionnaire_principal.localite as petitionnaire_principal_ville,
    petitionnaire_principal.cedex as petitionnaire_principal_cedex,
    petitionnaire_principal.pays as petitionnaire_principal_pays,

    --
    -- DELEGATAIRE
    --

    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as delegataire_nom,
    delegataire.numero as delegataire_numero,
    delegataire.voie as delegataire_voie,
    delegataire.complement as delegataire_complement,
    delegataire.lieu_dit as delegataire_lieu_dit,
    delegataire.bp as delegataire_bp,
    delegataire.code_postal as delegataire_code_postal,
    delegataire.localite as delegataire_ville,
    delegataire.cedex as delegataire_cedex,
    delegataire.pays as delegataire_pays,

    --
    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    --

    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as correspondant_nom,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as correspondant_numero,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as correspondant_voie,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as correspondant_complement,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as correspondant_lieu_dit,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.bp
        ELSE delegataire.bp
    END as correspondant_bp,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as correspondant_code_postal,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as correspondant_ville,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.cedex
        ELSE delegataire.cedex
    END as correspondant_cedex,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as correspondant_pays,

    --
    -- DATES DI
    --

    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite,
    
    --
    -- SIGNATAIRES
    --
    CONCAT(signataire_arrete.prenom, '' '', signataire_arrete.nom) as signataire_arrete,
    division.chef as division_chef,
    direction.chef as direction_chef,
    
    --
    -- DONNÉES TECHNIQUES
    --
    donnees_techniques.co_projet_desc,
    donnees_techniques.am_projet_desc,
    donnees_techniques.dm_projet_desc,
    donnees_techniques.am_lot_max_nb,
    donnees_techniques.am_lot_max_shon,
    donnees_techniques.su_cstr_shon_tot,
    donnees_techniques.su_demo_shon_tot,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9),
    donnees_techniques.co_tot_log_nb,
    donnees_techniques.co_statio_place_nb
    
FROM
    &DB_PREFIXEinstruction
LEFT JOIN
    &DB_PREFIXEsignataire_arrete
    ON instruction.signataire_arrete = signataire_arrete.signataire_arrete
LEFT JOIN
    &DB_PREFIXEom_lettretype
    ON instruction.lettretype = om_lettretype.id and om_lettretype.actif IS TRUE
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        instruction.dossier=dossier.dossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        instruction.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur
LEFT JOIN
    (
    &DB_PREFIXElien_dossier_demandeur AS lien_dossier_delegataire
        JOIN &DB_PREFIXEdemandeur as delegataire
        ON
            lien_dossier_delegataire.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
    )
    ON
        instruction.dossier = lien_dossier_delegataire.dossier AND lien_dossier_delegataire.petitionnaire_principal IS FALSE
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN 
    &DB_PREFIXEdirection
    ON division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
WHERE instruction.instruction = &idx'
WHERE code ='instruction';

UPDATE om_requete SET requete =
'SELECT

 
    rapport_instruction.dossier_instruction, analyse_reglementaire, description_projet, proposition_decision, 
    dossier.dossier_libelle as d, etat, pos, servitude, terrain_adresse_voie_numero, dossier.terrain_adresse_voie as dc, terrain_adresse_lieu_dit, terrain_adresse_localite, terrain_adresse_code_postal, terrain_adresse_bp, terrain_adresse_cedex, terrain_superficie, dossier.delai, replace(dossier.terrain_references_cadastrales, '';'', '' ''),
    particulier_nom, particulier_prenom, personne_morale_denomination, personne_morale_raison_sociale, personne_morale_siret, personne_morale_nom, personne_morale_prenom, numero, voie, demandeur.complement as deco, lieu_dit, localite, code_postal, bp, cedex, instructeur.nom, 
    dossier_autorisation_type_detaille.libelle as dl, 
    civilite.code as cc,
    avis_decision.libelle as avis,
    division.chef as division_chef,
    direction.chef as direction_chef,
    donnees_techniques.co_projet_desc,
    donnees_techniques.am_projet_desc,
    donnees_techniques.dm_projet_desc,
    donnees_techniques.am_lot_max_nb,
    donnees_techniques.am_lot_max_shon,
    donnees_techniques.su_cstr_shon_tot,
    donnees_techniques.su_demo_shon_tot,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9),
    donnees_techniques.co_tot_log_nb,
    donnees_techniques.co_statio_place_nb


FROM 

    &DB_PREFIXErapport_instruction 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=rapport_instruction.dossier_instruction 
    LEFT JOIN &DB_PREFIXEavis_decision 
        ON dossier.avis_decision = avis_decision.avis_decision
    LEFT JOIN &DB_PREFIXEdonnees_techniques
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdivision
        ON dossier.division = division.division
    LEFT JOIN &DB_PREFIXEdirection
        ON division.direction = direction.direction
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur 
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
    
WHERE 

    rapport_instruction = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE'
WHERE code ='rapport_instruction';

UPDATE om_sousetat SET om_sql =
'SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, ''\n'', 
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    instruction.date_evenement = (SELECT MAX(instr.date_evenement) FROM &DB_PREFIXEinstruction instr WHERE instr.dossier = dossier.dossier) AND
    (LOWER(evenement.type) != ''arrete'' OR LOWER(evenement.type) IS NULL ) AND
    instruction.date_envoi_signature = ''&date_bordereau''
ORDER BY
    dossier.dossier'
WHERE id ='bordereau_courriers_signature_maire';

UPDATE om_sousetat SET om_sql =
'SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, ''\n'', 
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    instruction.date_evenement = (SELECT MAX(instr.date_evenement) FROM &DB_PREFIXEinstruction instr WHERE instr.dossier = dossier.dossier) AND
    LOWER(autorite_competente.code) = ''etat'' AND
    instruction.date_envoi_controle_legalite = ''&date_bordereau''
ORDER BY
    dossier.dossier'
WHERE id ='bordereau_avis_maire_prefet';

UPDATE om_sousetat SET om_sql =
'SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, ''\n'', 
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
LEFT JOIN
    &DB_PREFIXEautorite_competente
    ON
        autorite_competente.autorite_competente = dossier.autorite_competente
WHERE
    instruction.date_evenement = (SELECT MAX(instr.date_evenement) FROM &DB_PREFIXEinstruction instr WHERE instr.dossier = dossier.dossier) AND
    LOWER(autorite_competente.code) = ''etat'' AND
    instruction.date_envoi_controle_legalite = ''&date_bordereau''
ORDER BY
    dossier.dossier'
WHERE id ='bordereau_controle_legalite';

UPDATE om_sousetat SET om_sql =
'SELECT
    dossier.dossier_libelle as dossier,
    evenement.libelle as evenement,
    CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.localite, '' '', demandeur.code_postal, '' '', demandeur.cedex, '' '', demandeur.bp)
         WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, ''\n'', 
                                                              demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret, '' '', demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp)
    END as coordonnees_demandeur,
    CONCAT ( dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, '' '', dossier.terrain_adresse_lieu_dit, '' '', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite, '' '', dossier.terrain_adresse_cedex, '' '', dossier.terrain_adresse_bp) as coordonnees_terrain
FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXEinstruction
    ON
        instruction.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        lien_dossier_demandeur.dossier = dossier.dossier
LEFT JOIn
    &DB_PREFIXEdemandeur
    ON
        demandeur.demandeur = lien_dossier_demandeur
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        civilite.civilite = demandeur.particulier_civilite OR civilite.civilite = demandeur.personne_morale_civilite
WHERE
    instruction.date_evenement = (SELECT MAX(instr.date_evenement) FROM &DB_PREFIXEinstruction instr WHERE instr.dossier = dossier.dossier) AND
    LOWER(evenement.type) = ''arrete'' AND
    instruction.date_envoi_signature = ''&date_bordereau'' AND
    lien_dossier_demandeur.petitionnaire_principal IS TRUE
ORDER BY
    dossier.dossier'
WHERE id ='bordereau_decisions';

UPDATE om_sousetat SET om_sql =
'SELECT
    row_number() OVER(ORDER BY dossier_commission.dossier DESC) cel1,
    CONCAT(
        ''Dossier:      '', dossier.dossier_libelle, ''    '', ''Rap.  '', instructeur.nom, ''
'',
        ''Demandeur :   '', CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom) WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' '', demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret) END,''
'',
        CONCAT(demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp) , ''
'',
        ''Adresse travaux : '', TRIM(CONCAT(dossier.terrain_adresse_voie_numero,'' '',dossier.terrain_adresse_voie,'' '', dossier.terrain_adresse_lieu_dit,'' '', dossier.terrain_adresse_code_postal,'' '', dossier.terrain_adresse_localite,'' '', dossier.terrain_adresse_bp,'' '', dossier.terrain_adresse_cedex)), ''
'',
        ''Quartier:     '', arrondissement.libelle, ''
'',
        ''Dépot:        '', to_char(dossier.date_depot,''DD/MM/YYYY''), ''         Délai :'', to_char(dossier.date_limite,''DD/MM/YYYY''), ''
'',
        ''Opération:    '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_projet_desc WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN CONCAT(donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc) WHEN dossier_autorisation_type_detaille.code=''PD'' THEN    donnees_techniques.dm_projet_desc WHEN dossier_autorisation_type_detaille.code=''PA'' THEN donnees_techniques.am_projet_desc WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN CONCAT(donnees_techniques.am_projet_desc, donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc)  END, ''
'',
        ''SHON:         '', donnees_techniques.su_tot_shon_tot, ''     Nbr logements: '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PD'' THEN donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb END, ''
'',
        ''nb log T1: '', donnees_techniques.co_log_1p_nb, '' T2: '', donnees_techniques.co_log_2p_nb, '' T3: '', donnees_techniques.co_log_3p_nb, '' T4: '', donnees_techniques.co_log_4p_nb, '' T5: '', donnees_techniques.co_log_5p_nb, '' T6: '', donnees_techniques.co_log_6p_nb, '' Nbr places stationnement: '', donnees_techniques.co_statio_place_nb, ''
''
    ) as cel2,
    dossier_commission.avis as motivation
FROM
    &DB_PREFIXEcommission
LEFT JOIN
    &DB_PREFIXEdossier_commission
    ON
        commission.commission=dossier_commission.commission
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        dossier_commission.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur
    ON
        lien_dossier_demandeur.demandeur = demandeur.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        demandeur.particulier_civilite = civilite.civilite OR demandeur.personne_morale_civilite = civilite.civilite
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
        dossier.dossier = donnees_techniques.dossier_instruction
WHERE
    commission.commission = &idx'
WHERE id ='commission_compte_rendu';

UPDATE om_sousetat SET om_sql =
'SELECT
    row_number() OVER(ORDER BY dossier_commission.dossier DESC) cel1,
    CONCAT(
        ''Dossier:      '', dossier.dossier_libelle, ''    '', ''Rap.  '', instructeur.nom, ''
'',
        ''Demandeur :   '', CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom) WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' '', demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret) END,''
'',
        CONCAT(demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp) , ''
'',
        ''Adresse travaux : '', TRIM(CONCAT(dossier.terrain_adresse_voie_numero,'' '',dossier.terrain_adresse_voie,'' '', dossier.terrain_adresse_lieu_dit,'' '', dossier.terrain_adresse_code_postal,'' '', dossier.terrain_adresse_localite,'' '', dossier.terrain_adresse_bp,'' '', dossier.terrain_adresse_cedex)), ''
'',
        ''Quartier:     '', arrondissement.libelle, ''
'',
        ''Dépot:        '', to_char(dossier.date_depot,''DD/MM/YYYY''), ''         Délai :'', to_char(dossier.date_limite,''DD/MM/YYYY''), ''
'',
        ''Opération:    '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_projet_desc WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN CONCAT(donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc) WHEN dossier_autorisation_type_detaille.code=''PD'' THEN    donnees_techniques.dm_projet_desc WHEN dossier_autorisation_type_detaille.code=''PA'' THEN donnees_techniques.am_projet_desc WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN CONCAT(donnees_techniques.am_projet_desc, donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc)  END, ''
'',
        ''SHON:         '', donnees_techniques.su_tot_shon_tot, ''     Nbr logements: '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PD'' THEN donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb END, ''
'',
        ''nb log T1: '', donnees_techniques.co_log_1p_nb, '' T2: '', donnees_techniques.co_log_2p_nb, '' T3: '', donnees_techniques.co_log_3p_nb, '' T4: '', donnees_techniques.co_log_4p_nb, '' T5: '', donnees_techniques.co_log_5p_nb, '' T6: '', donnees_techniques.co_log_6p_nb, '' Nbr places stationnement: '', donnees_techniques.co_statio_place_nb, ''
''
    ) as cel2,
    '''' as motivation
FROM
    &DB_PREFIXEcommission
LEFT JOIN
    &DB_PREFIXEdossier_commission
    ON
        commission.commission=dossier_commission.commission
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        dossier_commission.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur
    ON
        lien_dossier_demandeur.demandeur = demandeur.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        demandeur.particulier_civilite = civilite.civilite OR demandeur.personne_morale_civilite = civilite.civilite
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
        dossier.dossier = donnees_techniques.dossier_instruction
WHERE
    commission.commission = &idx'
WHERE id ='commission_ordre_jour';

UPDATE om_sousetat SET om_sql =
'SELECT
    row_number() OVER(ORDER BY dossier_commission.dossier DESC) cel1,
    CONCAT(
        ''Dossier:      '', dossier.dossier_libelle, ''    '', ''Rap.  '', instructeur.nom, ''
'',
        ''Demandeur :   '', CASE WHEN demandeur.qualite=''particulier'' THEN CONCAT(civilite.code, '' '', demandeur.particulier_nom, '' '', demandeur.particulier_prenom) WHEN demandeur.qualite=''personne_morale'' THEN CONCAT(civilite.code, '' '', demandeur.personne_morale_nom, '' '', demandeur.personne_morale_prenom, '' '', demandeur.personne_morale_denomination, '' '', demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_siret) END,''
'',
        CONCAT(demandeur.numero, '' '', demandeur.voie, '' '', demandeur.complement, '' '', demandeur.lieu_dit, '' '', demandeur.code_postal, '' '', demandeur.localite, '' '', demandeur.cedex, '' '', demandeur.bp) , ''
'',
        ''Adresse travaux : '', TRIM(CONCAT(dossier.terrain_adresse_voie_numero,'' '',dossier.terrain_adresse_voie,'' '', dossier.terrain_adresse_lieu_dit,'' '', dossier.terrain_adresse_code_postal,'' '', dossier.terrain_adresse_localite,'' '', dossier.terrain_adresse_bp,'' '', dossier.terrain_adresse_cedex)), ''
'',
        ''Quartier:     '', arrondissement.libelle, ''
'',
        ''Dépot:        '', to_char(dossier.date_depot,''DD/MM/YYYY''), ''         Délai :'', to_char(dossier.date_limite,''DD/MM/YYYY''), ''
'',
        ''Opération:    '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_projet_desc WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN CONCAT(donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc) WHEN dossier_autorisation_type_detaille.code=''PD'' THEN    donnees_techniques.dm_projet_desc WHEN dossier_autorisation_type_detaille.code=''PA'' THEN donnees_techniques.am_projet_desc WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN CONCAT(donnees_techniques.am_projet_desc, donnees_techniques.co_projet_desc, donnees_techniques.dm_projet_desc)  END, ''
'',
        ''SHON:         '', donnees_techniques.su_tot_shon_tot, ''     Nbr logements: '', CASE WHEN dossier_autorisation_type_detaille.code=''PCI'' OR dossier_autorisation_type_detaille.code=''AZ'' THEN donnees_techniques.co_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PCA'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''PD'' THEN donnees_techniques.dm_tot_log_nb WHEN dossier_autorisation_type_detaille.code=''DAT'' THEN donnees_techniques.co_tot_log_nb + donnees_techniques.dm_tot_log_nb END, ''
'',
        ''nb log T1: '', donnees_techniques.co_log_1p_nb, '' T2: '', donnees_techniques.co_log_2p_nb, '' T3: '', donnees_techniques.co_log_3p_nb, '' T4: '', donnees_techniques.co_log_4p_nb, '' T5: '', donnees_techniques.co_log_5p_nb, '' T6: '', donnees_techniques.co_log_6p_nb, '' Nbr places stationnement: '', donnees_techniques.co_statio_place_nb, ''
''
    ) as cel2,
    dossier_commission.motivation as motivation
FROM
    &DB_PREFIXEcommission
LEFT JOIN
    &DB_PREFIXEdossier_commission
    ON
        commission.commission=dossier_commission.commission
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        dossier_commission.dossier = dossier.dossier
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur
    ON
        lien_dossier_demandeur.demandeur = demandeur.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEcivilite
    ON
        demandeur.particulier_civilite = civilite.civilite OR demandeur.personne_morale_civilite = civilite.civilite
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
        dossier.dossier = donnees_techniques.dossier_instruction
WHERE
    commission.commission = &idx'
WHERE id ='commission_proposition_ordre_jour';

UPDATE om_sousetat SET om_sql =
'SELECT

    CONCAT(dossier_autorisation_type.libelle, ''

'',dossier.dossier_libelle) as dossier,

    CONCAT(''Dépôt le '', to_char(dossier.date_depot,''DD/MM/YYYY''), ''
Notifié le '', COALESCE(to_char(dossier.date_complet, ''DD/MM/YYYY''), ''inconu'')) as dates,

    CASE
        WHEN petitionnaire_principal.qualite=''particulier''

            THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom, ''
'', (CASE WHEN petitionnaire_principal.complement IS NULL THEN ''''
         ELSE CONCAT(petitionnaire_principal.complement, ''
'') END), petitionnaire_principal.numero,'' '', petitionnaire_principal.voie,''
'', (CASE WHEN petitionnaire_principal.bp IS NULL THEN ''''
         ELSE CONCAT(petitionnaire_principal.bp,''
'') END), petitionnaire_principal.code_postal,'' '', petitionnaire_principal.localite, '' '', petitionnaire_principal.cedex, '' '', petitionnaire_principal.lieu_dit
                                          ))
    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination, ''
'', (CASE WHEN petitionnaire_principal.complement IS NULL THEN ''''
         ELSE CONCAT(petitionnaire_principal.complement, ''
'') END), petitionnaire_principal.numero,'' '', petitionnaire_principal.voie,''
'', (CASE WHEN petitionnaire_principal.bp IS NULL THEN ''''
         ELSE CONCAT(petitionnaire_principal.bp, ''
'') END), petitionnaire_principal.code_postal,'' '', petitionnaire_principal.localite, '' '', petitionnaire_principal.cedex, '' '', petitionnaire_principal.lieu_dit                                      ))
    END as demandeur,

    CONCAT(dossier.terrain_adresse_voie_numero, '' '', dossier.terrain_adresse_voie, ''
'', dossier.terrain_adresse_code_postal, '' '', dossier.terrain_adresse_localite,''
Arrondissement : '', arrondissement.libelle) as terrain,


    CONCAT(''superficie : '', '' '', ''
nombre de logements : '', '' '') as informations,

    CONCAT(''Délai '', delai, '' mois
Date limite le '', COALESCE(to_char(date_limite, ''DD/MM/YYYY''), ''inconu'')) as limite

FROM
  &DB_PREFIXEdossier
  LEFT JOIN &DB_PREFIXEdossier_autorisation
    ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
  LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille
    ON dossier_autorisation.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
  LEFT JOIN &DB_PREFIXEdossier_autorisation_type
    ON dossier_autorisation_type_detaille.dossier_autorisation_type=dossier_autorisation_type.dossier_autorisation_type
  LEFT JOIN &DB_PREFIXElien_dossier_demandeur
    ON dossier.dossier = lien_dossier_demandeur.dossier
  LEFT JOIN &DB_PREFIXEdemandeur as petitionnaire_principal
    ON lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
  LEFT JOIN &DB_PREFIXEavis_decision
    ON dossier.avis_decision=avis_decision.avis_decision
  LEFT JOIN &DB_PREFIXEarrondissement
    ON dossier.terrain_adresse_code_postal = arrondissement.code_postal
  LEFT JOIN &DB_PREFIXEdonnees_techniques
    ON donnees_techniques.dossier_instruction=dossier.dossier

WHERE
    (select e.statut from &DB_PREFIXEetat e where e.etat = dossier.etat ) = ''encours''

ORDER BY
    dossier_autorisation_type.libelle, arrondissement.libelle'
WHERE id ='registre_dossiers_affichage_reglementaire';

UPDATE om_sousetat SET om_sql =
'SELECT  ''Commune &commune''||'' ''||''Dossier ''||dossier_libelle as dossier,
 ''Depot ''||to_char(dossier.date_depot,''DD/MM/YYYY'')||'' Notifie le  ''||COALESCE(to_char(dossier.date_complet,''DD/MM/YYYY''),''inconu'') as date_dp_n,
COALESCE(demandeur_civilite,''sans'')||'' ''||demandeur_nom||'' ''||demandeur_adresse||'' ''||demandeur_cp||''  ''||demandeur_ville||'' Parcelle ''||dossier.parcelle as nom_adresse_demandeur,
dossier.terrain_numero||'' ''||dossier.terrain_numero_complement||'' ''||dossier.terrain_adresse||'' ''||dossier.terrain_adresse_complement||''
 ''||dossier.terrain_cp||''  ''||dossier.terrain_ville||'' ''||dossier.travaux.libelle as adresse_terrain_travaux,
''shon  ''||dossier.shon||'' shob ''||dossier.shob as SN_SB,
dossier.terrain_surface as superficie,
dossier.logement_nombre as nbr_logement,
COALESCE(avis_decision.libelle,''inconu'') as avis_decision,
''Decision''||COALESCE(to_char(dossier.date_decision,''DD/MM/YYYY''),''inconu'')||'' Limite ''||COALESCE(to_char(dossier.date_limite,''DD/MM/YYYY''),''inconu'') as date_dc_l,
dossier.delai||'' mois'' as delai, '' '' as date_affichage_decision, '' '' as DOC_DAT_Conformite
from dossier left join travaux on dossier.travaux=travaux.travaux left join avis_decision on dossier.avis_decision=avis_decision.avis_decision
 where dossier.nature=''&nature'' AND (date_decision>=''&datedebut''
 AND date_decision<=''&datefin'')
ORDER BY dossier'
WHERE id ='registre_dossiers_decisions_par_type_par_date';

UPDATE om_sousetat SET om_sql =
'SELECT  ''Commune &commune''||'' ''||''Dossier ''||dossier_libelle as dossier,
to_char(dossier.date_depot,''DD/MM/YYYY'') as date_depot,
COALESCE(demandeur_civilite,''sans'')||'' ''||demandeur_nom as nom_demandeur,
dossier.terrain_numero||'' ''||dossier.terrain_numero_complement||'' ''||dossier.terrain_adresse||'' ''||dossier.terrain_adresse_complement||''
 ''||dossier.terrain_cp||''  ''||dossier.terrain_ville as adresse_terrain,
dossier.shon,
dossier.logement_nombre as nb_logt
from dossier
 where dossier.nature=''&nature'' AND (date_depot>=''&datedebut''
 AND date_depot<=''&datefin'')
ORDER BY dossier'
WHERE id ='registre_dossiers_depots_par_type_par_date';

UPDATE om_sousetat SET om_sql =
'SELECT  ''Commune &commune''||'' ''||''Dossier ''||dossier_libelle as dossier,
 ''Depot ''||to_char(dossier.date_depot,''DD/MM/YYYY'')||'' Notifie le  ''||COALESCE(to_char(dossier.date_complet,''DD/MM/YYYY''),''inconu'') as date_dp_n,
COALESCE(demandeur_civilite,''sans'')||'' ''||demandeur_nom||'' ''||demandeur_adresse||'' ''||demandeur_cp||''  ''||demandeur_ville||'' Parcelle ''||parcelle as nom_adresse_demandeur,
dossier.terrain_numero||'' ''||dossier.terrain_numero_complement||'' ''||dossier.terrain_adresse||'' ''||dossier.terrain_adresse_complement||''
 ''||dossier.terrain_cp||''  ''||dossier.terrain_ville||'' ''||travaux.libelle as adresse_terrain_travaux,
''shon  ''||dossier.shon||'' shob ''||dossier.shob as SN_SB,
dossier.terrain_surface as superficie,
dossier.logement_nombre as nbr_logement,
COALESCE(avis_decision.libelle,''inconu'') as avis_decision,
''Decision''||COALESCE(to_char(dossier.date_decision,''DD/MM/YYYY''),''inconu'')||'' Limite ''||COALESCE(to_char(dossier.date_limite,''DD/MM/YYYY''),''inconu'') as date_dc_l,
dossier.delai||'' mois'' as delai, '' '' as date_affichage_decision, '' '' as DOC_DAT_Conformite
from dossier left join travaux on dossier.travaux=travaux.travaux left join avis_decision on dossier.avis_decision=avis_decision.avis_decision
 where dossier.nature=''&nature'' AND (date_depot>=''&datedebut''
 AND date_depot<=''&datefin'')
ORDER BY dossier'
WHERE id ='registre_dossiers_par_type_par_date';

---
--- Permet de remplir le champ dossier_autorisation_libelle des dossiers 
--- d'autorisations existants
---
UPDATE dossier_autorisation 
SET dossier_autorisation_libelle = CONCAT(substr(dossier_autorisation.dossier_autorisation, 0, strpos(dossier_autorisation.dossier_autorisation, '013055')), ' ', '013055', ' ', substr(dossier_autorisation.dossier_autorisation, strpos(dossier_autorisation.dossier_autorisation, '013055')+6, 2), ' ', substr(dossier_autorisation.dossier_autorisation, strpos(dossier_autorisation.dossier_autorisation, '013055')+8))
FROM dossier_autorisation as da
LEFT JOIN dossier_autorisation_type_detaille
    ON da.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN dossier_autorisation_type
    ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
WHERE dossier_autorisation.dossier_autorisation_libelle IS NULL;

---
--- Permet de remplir le champ dossier_libelle des dossiers d'instructions existants
---
UPDATE dossier 
SET dossier_libelle = CONCAT(substr(dossier.dossier, 0, strpos(dossier.dossier, '013055')), ' ', '013055', ' ', substr(dossier.dossier, strpos(dossier.dossier, '013055')+6, 2), ' ', substr(dossier.dossier, strpos(dossier.dossier, '013055')+8))
FROM dossier as d
LEFT JOIN dossier_autorisation
    ON d.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN dossier_autorisation_type_detaille
    ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN dossier_autorisation_type
    ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
WHERE dossier.dossier_libelle IS NULL;


---
--- Modification de la liaison entre les cerfa et les types de dossiers
---

-- Ajout des colonnes de cerfa à la table dossier_autorisation_type_detaille
ALTER TABLE dossier_autorisation_type_detaille ADD COLUMN cerfa integer;

ALTER TABLE dossier_autorisation_type_detaille ADD COLUMN cerfa_lot integer;

-- Mise à jour de ces champs avec le cerfa des type de demandes initiales
UPDATE
    dossier_autorisation_type_detaille
SET
    cerfa = dit.cerfa,
    cerfa_lot = dit.cerfa_lot
FROM
    dossier_autorisation_type_detaille as datd
INNER JOIN
    dossier_instruction_type as dit
ON
    datd.dossier_autorisation_type_detaille = dit.dossier_autorisation_type_detaille
AND
   dit.code='P';

-- Suppression des anciennes colonnes de la table dossier_instruction_type
ALTER TABLE dossier_instruction_type DROP COLUMN cerfa;

ALTER TABLE dossier_instruction_type DROP COLUMN cerfa_lot;

-- Ajout des liens entre la table cerfa et dossier_autorisation_type_detaille
ALTER TABLE ONLY dossier_autorisation_type_detaille 
    ADD CONSTRAINT dossier_autorisation_type_detaille_cerfa_fkey FOREIGN KEY (cerfa)
    REFERENCES cerfa(cerfa);
ALTER TABLE ONLY dossier_autorisation_type_detaille 
    ADD CONSTRAINT dossier_autorisation_type_detaille_cerfa_lot_fkey FOREIGN KEY (cerfa_lot)
    REFERENCES cerfa(cerfa);

ALTER TABLE donnees_techniques ADD COLUMN dossier_autorisation character varying(20) NULL;
ALTER TABLE ONLY donnees_techniques 
    ADD CONSTRAINT dossier_autorisation_donnees_techniques_fkey FOREIGN KEY (dossier_autorisation)
    REFERENCES dossier_autorisation(dossier_autorisation);


INSERT INTO "donnees_techniques" ("donnees_techniques", "dossier_instruction", "lot", "avap_co_elt_pro", "avap_nouv_haut_surf", "avap_co_clot", "avap_aut_coup_aba_arb", "avap_ouv_infra", "avap_aut_inst_mob", "avap_aut_plant", "avap_aut_auv_elec", "tr_total", "tr_partiel", "tr_desc", "am_lotiss", "am_autre_div", "am_camping", "am_caravane", "am_carav_duree", "am_statio", "am_statio_cont", "am_affou_exhau", "am_affou_exhau_sup", "am_affou_prof", "am_exhau_haut", "am_coupe_abat", "am_prot_plu", "am_prot_muni", "am_mobil_voyage", "am_voyage_deb", "am_voyage_fin", "am_aire_voyage", "am_rememb_afu", "am_parc_resid_loi", "am_sport_moto", "am_sport_attrac", "am_sport_golf", "am_mob_art", "am_modif_voie_esp", "am_plant_voie_esp", "am_chem_ouv_esp", "am_agri_peche", "am_crea_voie", "am_modif_voie_exist", "am_crea_esp_sauv", "am_modif_amgt", "am_crea_esp_class", "am_projet_desc", "am_terr_surf", "am_tranche_desc", "am_lot_max_nb", "am_lot_max_shon", "am_lot_max_shob", "am_lot_cstr_cos", "am_lot_cstr_plan", "am_lot_cstr_vente", "am_lot_fin_diff", "am_lot_consign", "am_lot_gar_achev", "am_lot_vente_ant", "am_empl_nb", "am_tente_nb", "am_carav_nb", "am_mobil_nb", "am_pers_nb", "am_empl_hll_nb", "am_hll_shon", "am_periode_exploit", "am_exist_agrand", "am_exist_date", "am_exist_num", "am_exist_nb_avant", "am_exist_nb_apres", "am_coupe_bois", "am_coupe_parc", "am_coupe_align", "am_coupe_ess", "am_coupe_age", "am_coupe_dens", "am_coupe_qual", "am_coupe_trait", "am_coupe_autr", "co_archi_recours", "co_cstr_nouv", "co_cstr_exist", "co_modif_aspect", "co_modif_struct", "co_cloture", "co_ouvr_elec", "co_elec_tension", "co_ouvr_infra", "co_trx_imm", "co_div_terr", "co_projet_desc", "co_cstr_shob", "co_anx_pisc", "co_anx_gara", "co_anx_veran", "co_anx_abri", "co_anx_autr", "co_anx_autr_desc", "co_tot_log_nb", "co_tot_ind_nb", "co_tot_coll_nb", "co_mais_piece_nb", "co_mais_niv_nb", "co_fin_lls_nb", "co_fin_aa_nb", "co_fin_ptz_nb", "co_fin_autr_nb", "co_fin_autr_desc", "co_mais_contrat_ind", "co_uti_pers", "co_uti_vente", "co_uti_loc", "co_uti_princ", "co_uti_secon", "co_resid_agees", "co_resid_etud", "co_resid_tourism", "co_resid_hot_soc", "co_resid_soc", "co_resid_hand", "co_resid_autr", "co_resid_autr_desc", "co_foyer_chamb_nb", "co_log_1p_nb", "co_log_2p_nb", "co_log_3p_nb", "co_log_4p_nb", "co_log_5p_nb", "co_log_6p_nb", "co_bat_niv_nb", "co_trx_exten", "co_trx_surelev", "co_trx_nivsup", "co_trx_amgt", "co_demont_periode", "co_sp_transport", "co_sp_enseign", "co_sp_act_soc", "co_sp_ouvr_spe", "co_sp_sante", "co_sp_culture", "co_statio_avt_nb", "co_statio_apr_nb", "co_statio_avt_shob", "co_statio_apr_shob", "co_statio_avt_surf", "co_statio_apr_surf", "co_statio_adr", "co_statio_place_nb", "co_statio_tot_surf", "co_statio_tot_shob", "co_statio_comm_cin_surf", "su_avt_shon1", "su_avt_shon2", "su_avt_shon3", "su_avt_shon4", "su_avt_shon5", "su_avt_shon6", "su_avt_shon7", "su_avt_shon8", "su_avt_shon9", "su_cstr_shon1", "su_cstr_shon2", "su_cstr_shon3", "su_cstr_shon4", "su_cstr_shon5", "su_cstr_shon6", "su_cstr_shon7", "su_cstr_shon8", "su_cstr_shon9", "su_trsf_shon1", "su_trsf_shon2", "su_trsf_shon3", "su_trsf_shon4", "su_trsf_shon5", "su_trsf_shon6", "su_trsf_shon7", "su_trsf_shon8", "su_trsf_shon9", "su_chge_shon1", "su_chge_shon2", "su_chge_shon3", "su_chge_shon4", "su_chge_shon5", "su_chge_shon6", "su_chge_shon7", "su_chge_shon8", "su_chge_shon9", "su_demo_shon1", "su_demo_shon2", "su_demo_shon3", "su_demo_shon4", "su_demo_shon5", "su_demo_shon6", "su_demo_shon7", "su_demo_shon8", "su_demo_shon9", "su_sup_shon1", "su_sup_shon2", "su_sup_shon3", "su_sup_shon4", "su_sup_shon5", "su_sup_shon6", "su_sup_shon7", "su_sup_shon8", "su_sup_shon9", "su_tot_shon1", "su_tot_shon2", "su_tot_shon3", "su_tot_shon4", "su_tot_shon5", "su_tot_shon6", "su_tot_shon7", "su_tot_shon8", "su_tot_shon9", "su_avt_shon_tot", "su_cstr_shon_tot", "su_trsf_shon_tot", "su_chge_shon_tot", "su_demo_shon_tot", "su_sup_shon_tot", "su_tot_shon_tot", "dm_constr_dates", "dm_total", "dm_partiel", "dm_projet_desc", "dm_tot_log_nb", "tax_surf_tot", "tax_surf", "tax_surf_suppr_mod", "tax_dest_loc_tr", "tax_su_princ_log_nb1", "tax_su_princ_log_nb2", "tax_su_princ_log_nb3", "tax_su_princ_log_nb4", "tax_su_princ_log_nb_tot1", "tax_su_princ_log_nb_tot2", "tax_su_princ_log_nb_tot3", "tax_su_princ_log_nb_tot4", "tax_su_princ_surf1", "tax_su_princ_surf2", "tax_su_princ_surf3", "tax_su_princ_surf4", "tax_su_princ_surf_sup1", "tax_su_princ_surf_sup2", "tax_su_princ_surf_sup3", "tax_su_princ_surf_sup4", "tax_su_heber_log_nb1", "tax_su_heber_log_nb2", "tax_su_heber_log_nb3", "tax_su_heber_log_nb_tot1", "tax_su_heber_log_nb_tot2", "tax_su_heber_log_nb_tot3", "tax_su_heber_surf1", "tax_su_heber_surf2", "tax_su_heber_surf3", "tax_su_heber_surf_sup1", "tax_su_heber_surf_sup2", "tax_su_heber_surf_sup3", "tax_su_secon_log_nb", "tax_su_tot_log_nb", "tax_su_secon_log_nb_tot", "tax_su_tot_log_nb_tot", "tax_su_secon_surf", "tax_su_tot_surf", "tax_su_secon_surf_sup", "tax_su_tot_surf_sup", "tax_ext_pret", "tax_ext_desc", "tax_surf_tax_exist_cons", "tax_log_exist_nb", "tax_am_statio_ext", "tax_sup_bass_pisc", "tax_empl_ten_carav_mobil_nb", "tax_empl_hll_nb", "tax_eol_haut_nb", "tax_pann_volt_sup", "tax_am_statio_ext_sup", "tax_sup_bass_pisc_sup", "tax_empl_ten_carav_mobil_nb_sup", "tax_empl_hll_nb_sup", "tax_eol_haut_nb_sup", "tax_pann_volt_sup_sup", "tax_trx_presc_ppr", "tax_monu_hist", "tax_comm_nb", "tax_su_non_habit_surf1", "tax_su_non_habit_surf2", "tax_su_non_habit_surf3", "tax_su_non_habit_surf4", "tax_su_non_habit_surf5", "tax_su_non_habit_surf6", "tax_su_non_habit_surf7", "tax_su_non_habit_surf_sup1", "tax_su_non_habit_surf_sup2", "tax_su_non_habit_surf_sup3", "tax_su_non_habit_surf_sup4", "tax_su_non_habit_surf_sup5", "tax_su_non_habit_surf_sup6", "tax_su_non_habit_surf_sup7", "vsd_surf_planch_smd", "vsd_unit_fonc_sup", "vsd_unit_fonc_constr_sup", "vsd_val_terr", "vsd_const_sxist_non_dem_surf", "vsd_rescr_fisc", "pld_val_terr", "pld_const_exist_dem", "pld_const_exist_dem_surf", "code_cnil", "co_archi_nom", "co_archi_prenom", "co_archi_adr_num", "co_archi_adr_voie", "co_archi_adr_lieu_dit", "co_archi_adr_localite", "co_archi_adr_cp", "co_archi_adr_bp", "co_archi_adr_cedex", "co_archi_no_incri", "co_archi_cg", "co_archi_tel1", "co_archi_tel2", "co_archi_mail", "terr_juri_titul", "terr_juri_lot", "terr_juri_zac", "terr_juri_afu", "terr_juri_pup", "terr_juri_oin", "terr_juri_desc", "terr_div_surf_etab", "terr_div_surf_av_div", "doc_date", "doc_tot_trav", "doc_tranche_trav", "doc_tranche_trav_desc", "doc_surf", "doc_nb_log", "doc_nb_log_indiv", "doc_nb_log_coll", "doc_nb_log_lls", "doc_nb_log_aa", "doc_nb_log_ptz", "doc_nb_log_autre", "daact_date", "daact_date_chgmt_dest", "daact_tot_trav", "daact_tranche_trav", "daact_tranche_trav_desc", "daact_surf", "daact_nb_log", "daact_nb_log_indiv", "daact_nb_log_coll", "daact_nb_log_lls", "daact_nb_log_aa", "daact_nb_log_ptz", "daact_nb_log_autre", "mod_desc", "dossier_autorisation") VALUES 
(1, 'PC0130551200002P0',    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL),
(2, 'AT0130551200001P0',    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL),
(3, 'AZ0130551200001P0',    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL),
(4, 'CU0130551200001P0',    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL),
(5, 'DP0130551200001P0',    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL),
(6, 'PA0130551200001P0',    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL),
(7, 'PC0130551200001P0',    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL),
(8, 'PD0130551200001P0',    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL),
(9, NULL,    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'PC0130551200002'),
(10, NULL,    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'AT0130551200001'),
(11, NULL,    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'AZ0130551200001'),
(12, NULL,    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'CU0130551200001'),
(13, NULL,    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'DP0130551200001'),
(14, NULL,    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'PA0130551200001'),
(15, NULL,    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'PC0130551200001'),
(16, NULL,    NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'PD0130551200001'),
(17, NULL,    1,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL),
(18, NULL,    2,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   '', NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    'f',    'f',    'f',    NULL,   'f',    'f',    'f',    '', NULL,   'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    'f',    'f',    '', 'f',    'f',    'f',    'f',    'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', 'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    NULL,   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    'nesaispas',    '', NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   'f',    'f',    '', NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '', NULL);

UPDATE "cerfa" SET "om_validite_fin" = '2013-12-31' WHERE "cerfa" = '2';

---
--- Ajouts des nouveaux profils
---
COPY om_profil (om_profil, libelle, hierarchie, om_validite_debut, om_validite_fin) FROM stdin;
8	SERVICE CONSULTÉ INTERNE	0	\N	\N
9	VISUALISATION DA et DI	0	\N	\N
10	VISUALISATION DA	0	\N	\N
11	DIVISIONNAIRE	0	\N	\N
12	CHEF DE SERVICE	0	\N	\N
\.

SELECT pg_catalog.setval('om_profil_seq', 13, false);

---
--- Suppression d'un droit
---
DELETE FROM om_droit WHERE om_droit = 115;
---
--- Ajouts des droits
---
COPY om_droit (om_droit, libelle, om_profil) FROM stdin;
162	document_numerise_view	6
163	document_numerise_uid_telecharger	6
164	dossier_instruction_tab	6
165	dossier_instruction_consulter	6
166	document_numerise_tab	6
167	document_numerise_tab	5
168	document_numerise_uid_telecharger	5
169	dossier_instruction_tab	5
170	dossier_instruction_consulter	5
171	document_numerise_view	5
172	instruction_tab	5
173	instruction_consulter	5
174	consultation_tab	5
175	lot_tab	5
176	lot_consulter	5
177	demande_avis_tab	4
178	demande_avis_consulter	4
179	demande_avis_exporter	4
182	menu_autorisation	4
183	menu_demande_avis	8
184	demande_avis_encours	8
185	demande_avis_passee	8
186	demande_avis_encours_exporter	8
187	demande_avis_passee_exporter	8
188	consultation_retour_avis_service	8
189	consultation_modifier	8
190	dossier_document	8
191	document_numerise_tab	8
192	document_numerise_view	8
193	document_numerise_uid_telecharger	8
194	demande_avis_tab	8
195	demande_avis_consulter	8
196	demande_avis_exporter	8
199	menu_autorisation	8
198	dossier_autorisation_consulter	8
197	dossier_autorisation_tab	8
181	dossier_autorisation_avis_consulter	4
180	dossier_autorisation_avis_tab	4
200	dossier_autorisation_consulter	4
201	menu_autorisation	9
202	dossier_autorisation_tab	9
203	dossier_autorisation_consulter	9
204	document_numerise_view	9
205	document_numerise_uid_telecharger	9
206	document_numerise_tab	9
207	dossier_instruction_tab	9
208	dossier_instruction_consulter	9
209	instruction_tab	9
210	instruction_consulter	9
211	consultation_tab	9
212	consultation_consulter	9
213	lot_tab	9
214	lot_consulter	9
219	blocnote_tab	9
220	blocnote_consulter	9
218	dossier_commission_consulter	9
217	dossier_commission_tab	9
215	dossier_message_tab	9
216	dossier_message_consulter	9
221	menu_instruction	9
222	menu_autorisation	10
223	dossier_autorisation_tab	10
224	dossier_autorisation_consulter	10
225	document_numerise_view	10
226	document_numerise_uid_telecharger	10
227	document_numerise_tab	10
228	demande_nouveau_dossier_ajouter	3
229	demande_dossier_existant_tab	3
230	demande_dossier_existant_ajouter	3
231	menu_instruction	11
232	dossier_instruction_mes_encours_tab	11
233	dossier_instruction_mes_encours_modifier	11
234	dossier_instruction_mes_encours_consulter	11
235	dossier_instruction_tous_encours_tab	11
236	dossier_instruction_tous_encours_modifier	11
237	dossier_instruction_tous_encours_consulter	11
238	dossier_instruction_mes_clotures_tab	11
239	dossier_instruction_mes_clotures_modifier	11
240	dossier_instruction_mes_clotures_consulter	11
241	dossier_instruction_tous_clotures_tab	11
242	dossier_instruction_tous_clotures_modifier	11
243	dossier_instruction_tous_clotures_consulter	11
244	dossier_instruction_tab	11
245	dossier_instruction_modifier	11
246	dossier_instruction_consulter	11
247	messages_mes_retours	11
248	messages_tous_retours	11
249	dossier_message_consulter	11
250	dossier_message_modifier_lu	11
251	dossier_message_tab	11
252	commission_mes_retours	11
253	commission_tous_retours	11
254	dossier_commission	11
255	dossier_commission_modifier_lu	11
256	instruction_tab	11
257	instruction_ajouter	11
258	instruction_modifier	11
259	instruction_consulter	11
260	blocnote	11
261	consultation	11
262	consultation_ajouter	11
263	consultation_mes_retours	11
264	consultation_tous_retours	11
265	consultation_modifier_lu	11
266	consultation_modifier_date_reception	11
267	consultation_consulter_autre_que_retour_avis	11
268	rapport_instruction	11
269	rapport_instruction_rediger	11
270	dossiers_limites	11
271	dossiers_limites_tab	11
272	recherche_direct	11
273	recherche_direct_tab	11
274	dossiers_evenement_incomplet_majoration	11
275	rapport_instruction_edition	11
276	lot	11
277	lot_transferer	11
278	donnees_techniques	11
279	dossier_qualifier	11
280	dossier_qualifier_tab	11
281	menu_autorisation	11
282	dossier_autorisation	11
283	dossier_autorisation_tab	11
284	dossier_autorisation_consulter	11
285	demandeur	11
286	petitionnaire	11
287	delegataire	11
288	profil_instructeur	11
289	document_numerise_tab	11
290	document_numerise_view	11
291	document_numerise_ajouter	11
292	document_numerise_uid_telecharger	11
293	rapport_instruction_finaliser	11
294	rapport_instruction_definaliser	11
295	dossier_instruction_mes_encours_finaliser	11
296	dossier_instruction_mes_encours_definaliser	11
297	instruction_om_fichier_instruction_telecharger	11
298	consultation_om_fichier_consultation_telecharger	11
299	commission_om_fichier_commission_ordre_jour_telecharger	11
300	commission_om_fichier_commission_compte_rendu_telecharger	11
301	demande_nouveau_dossier_ajouter	11
302	demande_dossier_existant_tab	11
303	demande_dossier_existant_ajouter	11
304	menu_instruction	12
305	dossier_instruction_mes_encours_tab	12
306	dossier_instruction_mes_encours_modifier	12
307	dossier_instruction_mes_encours_consulter	12
308	dossier_instruction_tous_encours_tab	12
309	dossier_instruction_tous_encours_modifier	12
310	dossier_instruction_tous_encours_consulter	12
311	dossier_instruction_mes_clotures_tab	12
312	dossier_instruction_mes_clotures_modifier	12
313	dossier_instruction_mes_clotures_consulter	12
314	dossier_instruction_tous_clotures_tab	12
315	dossier_instruction_tous_clotures_modifier	12
316	dossier_instruction_tous_clotures_consulter	12
317	dossier_instruction_tab	12
318	dossier_instruction_modifier	12
319	dossier_instruction_consulter	12
320	messages_mes_retours	12
321	messages_tous_retours	12
322	dossier_message_consulter	12
323	dossier_message_modifier_lu	12
324	dossier_message_tab	12
325	commission_mes_retours	12
326	commission_tous_retours	12
327	dossier_commission	12
328	dossier_commission_modifier_lu	12
329	instruction_tab	12
330	instruction_ajouter	12
331	instruction_modifier	12
332	instruction_consulter	12
333	blocnote	12
334	consultation	12
335	consultation_ajouter	12
336	consultation_mes_retours	12
337	consultation_tous_retours	12
338	consultation_modifier_lu	12
339	consultation_modifier_date_reception	12
340	consultation_consulter_autre_que_retour_avis	12
341	rapport_instruction	12
342	rapport_instruction_rediger	12
343	dossiers_limites	12
344	dossiers_limites_tab	12
345	recherche_direct	12
346	recherche_direct_tab	12
347	dossiers_evenement_incomplet_majoration	12
348	rapport_instruction_edition	12
349	lot	12
350	lot_transferer	12
351	donnees_techniques	12
352	dossier_qualifier	12
353	dossier_qualifier_tab	12
354	menu_autorisation	12
355	dossier_autorisation	12
356	dossier_autorisation_tab	12
357	dossier_autorisation_consulter	12
358	demandeur	12
359	petitionnaire	12
360	delegataire	12
361	profil_instructeur	12
362	document_numerise_tab	12
363	document_numerise_view	12
364	document_numerise_ajouter	12
365	document_numerise_uid_telecharger	12
366	rapport_instruction_finaliser	12
367	rapport_instruction_definaliser	12
368	dossier_instruction_mes_encours_finaliser	12
369	dossier_instruction_mes_encours_definaliser	12
370	instruction_om_fichier_instruction_telecharger	12
371	consultation_om_fichier_consultation_telecharger	12
372	commission_om_fichier_commission_ordre_jour_telecharger	12
373	commission_om_fichier_commission_compte_rendu_telecharger	12
374	demande_nouveau_dossier_ajouter	12
375	demande_dossier_existant_tab	12
376	demande_dossier_existant_ajouter	12
378	dossier_instruction_tab	7
379	dossier_instruction_modifier	7
380	dossier_instruction_consulter	7
381	instruction_tab	7
383	instruction_modifier	7
384	instruction_consulter	7
385	consultation_tab	7
386	consultation_consulter	7
387	consultation_mes_retours	7
388	consultation_tous_retours	7
389	consultation_modifier_lu	7
390	consultation_modifier_date_reception	7
391	consultation_consulter_autre_que_retour_avis	7
392	rapport_instruction	7
393	rapport_instruction_rediger	7
394	rapport_instruction_edition	7
395	menu_autorisation	7
397	document_numerise_tab	7
398	document_numerise_view	7
399	document_numerise_uid_telecharger	7
400	rapport_instruction_finaliser	7
401	rapport_instruction_definaliser	7
402	instruction_om_fichier_instruction_telecharger	7
403	consultation_om_fichier_consultation_telecharger	7
404	dossier_qualifier_qualificateur_tab	7
405	dossier_qualifier_qualificateur_consulter	7
408	dossier_autorisation_tab	7
410	dossier_autorisation_consulter	7
411	menu_guichet_unique	3
412	menu_guichet_unique	12
413	consultation_om_fichier_consultation_telecharger	5
414	dossier_commission_possible	5
415	menu_guichet_unique	11
416	dossier_commission_ajouter_instruction	3
417	dossier_commission_modifier_instruction	3
418	dossier_commission_supprimer_instruction	3
419	dossier_commission_ajouter_instruction	12
420	dossier_commission_modifier_instruction	12
421	dossier_commission_supprimer_instruction	12
422	dossier_commission_ajouter_instruction	11
423	dossier_commission_modifier_instruction	11
424	dossier_commission_supprimer_instruction	11
425	dossiers_tous_limites	11
426	dossiers_tous_limites	12
\.

SELECT pg_catalog.setval('om_droit_seq', 427, false);

---
--- Ajout des widgets
---
COPY om_widget (om_widget, om_collectivite, libelle, lien, texte, type) FROM stdin;
12	1	Infos profil	infos_profil		file
13	1	Redirection	redirection		file
\.

SELECT pg_catalog.setval('om_widget_seq', 14, false);

---
--- om_dashboard
---
COPY om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) FROM stdin;
12	3	C1	3	5
13	3	C2	3	6
14	11	C2	1	8
15	7	C2	1	11
16	7	C1	1	12
19	11	C1	1	12
21	4	C1	1	13
22	8	C1	1	13
23	12	C2	1	8
24	12	C1	1	12
\.

SELECT pg_catalog.setval('om_dashboard_seq', 25, false);

---
--- Modifie le Tableau de bord de l'instructeur
---
UPDATE om_dashboard SET om_widget = 12 WHERE om_dashboard = 2;

---
--- Ajout d'utilisateurs
---
COPY om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) FROM stdin;
11	Service consulté interne	support@atreal.fr	consuint	41f5107d08bf85431bf518a48315008b	1	db	8
12	Visualisation DA et DI	support@atreal.fr	visudadi	f5482d356ab55aae5edf5e6146f44644	1	db	9
13	Visualisation DA	support@atreal.fr	visuda	f8d59fd856bf844a90f0968e8acd8cac	1	db	10
14	Qualificateur	support@atreal.fr	qualif	c7f6e8776497fc37bc20c6eae9db1c9d	1	db	7
15	Divisionnaire	support@atreal.fr	divi	a8124c681eeedd309a78db78c6d087b9	1	db	11
16	Chef de service	support@atreal.fr	chef	cbb4581ba3ada1ddef9b431eef2660ce	1	db	12
\.

SELECT pg_catalog.setval('om_utilisateur_seq', 17, false);

---
--- Ajout du divisionnaire dans la table instructeur
---
INSERT INTO instructeur VALUES (4, 'Pierre Martin', NULL, 22, 15, NULL, NULL);

SELECT pg_catalog.setval('instructeur_seq', 5, false);
UPDATE "cerfa" SET "om_validite_fin" = '2013-12-31' WHERE "cerfa" = '2';

ALTER TABLE cerfa ADD COLUMN am_div_mun boolean;
ALTER TABLE donnees_techniques ADD COLUMN am_div_mun boolean;

ALTER TABLE cerfa ADD COLUMN co_perf_energ boolean;
ALTER TABLE donnees_techniques ADD COLUMN co_perf_energ character varying(40);

UPDATE "cerfa" SET "om_validite_fin" = '2013-12-31' WHERE "cerfa" = '2';

--
--Ajout de deux champ dans la table architecte et ajout d'une clé étrangère dans la table données techniques
--
ALTER TABLE architecte ADD COLUMN frequent boolean;
ALTER TABLE architecte ADD COLUMN nom_cabinet character varying(100);
ALTER TABLE cerfa ADD COLUMN architecte boolean;
ALTER TABLE donnees_techniques ADD COLUMN architecte integer;
ALTER TABLE ONLY donnees_techniques 
    ADD CONSTRAINT donnees_techniques_architecte_fkey FOREIGN KEY (architecte) REFERENCES architecte(architecte);

--
-- Suppression des champs devenus inutiles dans la table cerfa et donnnées techniques
--
ALTER TABLE cerfa DROP COLUMN co_archi_nom, DROP COLUMN co_archi_prenom, DROP COLUMN co_archi_adr_num, DROP COLUMN co_archi_adr_voie, DROP COLUMN co_archi_adr_lieu_dit, DROP COLUMN co_archi_adr_localite, DROP COLUMN co_archi_adr_cp, DROP COLUMN co_archi_adr_bp, DROP COLUMN co_archi_adr_cedex, DROP COLUMN co_archi_no_incri, DROP COLUMN co_archi_cg, DROP COLUMN co_archi_tel1, DROP COLUMN co_archi_tel2, DROP COLUMN co_archi_mail;
ALTER TABLE donnees_techniques DROP COLUMN co_archi_nom, DROP COLUMN co_archi_prenom, DROP COLUMN co_archi_adr_num, DROP COLUMN co_archi_adr_voie, DROP COLUMN co_archi_adr_lieu_dit, DROP COLUMN co_archi_adr_localite, DROP COLUMN co_archi_adr_cp, DROP COLUMN co_archi_adr_bp, DROP COLUMN co_archi_adr_cedex, DROP COLUMN co_archi_no_incri, DROP COLUMN co_archi_cg, DROP COLUMN co_archi_tel1, DROP COLUMN co_archi_tel2, DROP COLUMN co_archi_mail;
   
--
-- Données de test pour les architectes
--
INSERT INTO architecte VALUES (nextval('architecte_seq'), 'Mercier', 'Paul', '113 boulevard de pont de vivaux', '', '13010', 'Marseille', 'France', '054645', '0497856235', '0497856236', 'paul.mercier@architecte.fr', '', true);
INSERT INTO architecte VALUES (nextval('architecte_seq'), 'Mercier', 'Jean', '113 boulevard de pont de vivaux', '', '13010', 'Marseille', 'France', '56454', '0491352689', '0491352685', 'jean.mercier@architecte.fr', '', true);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_frequent_tab', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_frequent_consulter', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_frequent', 2);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_ajouter', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_modifier', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_consulter', 3);
--
-- Données de tests pour les architectes dans un cerfa
--
UPDATE cerfa set architecte = TRUE WHERE libelle = 'cerfa de test n°1';

--
-- Données de test pour les architectes
--
ALTER TABLE dossier ADD COLUMN numero_versement_archive character varying(100);

--
-- Suppression des colonnes de cerfa et données techniques non utilisées
--
ALTER TABLE cerfa DROP COLUMN co_statio_avt_shob;
ALTER TABLE cerfa DROP COLUMN co_statio_apr_shob;
ALTER TABLE cerfa DROP COLUMN co_statio_avt_surf;
ALTER TABLE cerfa DROP COLUMN co_statio_apr_surf;
ALTER TABLE cerfa DROP COLUMN co_trx_amgt;
ALTER TABLE cerfa DROP COLUMN co_modif_aspect;
ALTER TABLE cerfa DROP COLUMN co_modif_struct;
ALTER TABLE cerfa DROP COLUMN co_ouvr_elec;
ALTER TABLE cerfa DROP COLUMN co_ouvr_infra;
ALTER TABLE cerfa DROP COLUMN co_trx_imm;
ALTER TABLE cerfa DROP COLUMN co_cstr_shob;
ALTER TABLE cerfa DROP COLUMN am_voyage_deb;
ALTER TABLE cerfa DROP COLUMN am_voyage_fin;
ALTER TABLE cerfa DROP COLUMN am_modif_amgt;
ALTER TABLE cerfa DROP COLUMN am_lot_max_shob;
ALTER TABLE cerfa DROP COLUMN mod_desc;
ALTER TABLE cerfa DROP COLUMN tr_total;
ALTER TABLE cerfa DROP COLUMN tr_partiel;
ALTER TABLE cerfa DROP COLUMN tr_desc;
ALTER TABLE cerfa DROP COLUMN avap_co_elt_pro;
ALTER TABLE cerfa DROP COLUMN avap_nouv_haut_surf;
ALTER TABLE cerfa DROP COLUMN avap_co_clot;
ALTER TABLE cerfa DROP COLUMN avap_aut_coup_aba_arb;
ALTER TABLE cerfa DROP COLUMN avap_ouv_infra;
ALTER TABLE cerfa DROP COLUMN avap_aut_inst_mob;
ALTER TABLE cerfa DROP COLUMN avap_aut_plant;
ALTER TABLE cerfa DROP COLUMN avap_aut_auv_elec;
ALTER TABLE cerfa DROP COLUMN tax_dest_loc_tr;

ALTER TABLE donnees_techniques DROP COLUMN co_statio_avt_shob;
ALTER TABLE donnees_techniques DROP COLUMN co_statio_apr_shob;
ALTER TABLE donnees_techniques DROP COLUMN co_statio_avt_surf;
ALTER TABLE donnees_techniques DROP COLUMN co_statio_apr_surf;
ALTER TABLE donnees_techniques DROP COLUMN co_trx_amgt;
ALTER TABLE donnees_techniques DROP COLUMN co_modif_aspect;
ALTER TABLE donnees_techniques DROP COLUMN co_modif_struct;
ALTER TABLE donnees_techniques DROP COLUMN co_ouvr_elec;
ALTER TABLE donnees_techniques DROP COLUMN co_ouvr_infra;
ALTER TABLE donnees_techniques DROP COLUMN co_trx_imm;
ALTER TABLE donnees_techniques DROP COLUMN co_cstr_shob;
ALTER TABLE donnees_techniques DROP COLUMN am_voyage_deb;
ALTER TABLE donnees_techniques DROP COLUMN am_voyage_fin;
ALTER TABLE donnees_techniques DROP COLUMN am_modif_amgt;
ALTER TABLE donnees_techniques DROP COLUMN am_lot_max_shob;
ALTER TABLE donnees_techniques DROP COLUMN mod_desc;
ALTER TABLE donnees_techniques DROP COLUMN tr_total;
ALTER TABLE donnees_techniques DROP COLUMN tr_partiel;
ALTER TABLE donnees_techniques DROP COLUMN tr_desc;
ALTER TABLE donnees_techniques DROP COLUMN avap_co_elt_pro;
ALTER TABLE donnees_techniques DROP COLUMN avap_nouv_haut_surf;
ALTER TABLE donnees_techniques DROP COLUMN avap_co_clot;
ALTER TABLE donnees_techniques DROP COLUMN avap_aut_coup_aba_arb;
ALTER TABLE donnees_techniques DROP COLUMN avap_ouv_infra;
ALTER TABLE donnees_techniques DROP COLUMN avap_aut_inst_mob;
ALTER TABLE donnees_techniques DROP COLUMN avap_aut_plant;
ALTER TABLE donnees_techniques DROP COLUMN avap_aut_auv_elec;
ALTER TABLE donnees_techniques DROP COLUMN tax_dest_loc_tr;

---
--- Ajout du champ duree_validite_parametrage dans la table dossier_autorisation_type_detaille
---
ALTER TABLE dossier_autorisation_type_detaille ADD COLUMN duree_validite_parametrage integer DEFAULT 0 NOT NULL;

---
--- Ajout de l'état "Périmé" pour les dossiers d'autorisations
---
INSERT INTO etat_dossier_autorisation VALUES (nextval('etat_dossier_autorisation_seq'), 'Périmé');

---
--- Augmentation du nombre de caractères des champs contenant le numéro de DI
---
ALTER TABLE instruction ALTER COLUMN dossier TYPE character varying(30);
ALTER TABLE donnees_techniques ALTER COLUMN dossier_instruction TYPE character varying(30);
ALTER TABLE demande ALTER COLUMN dossier_instruction TYPE character varying(30);
ALTER TABLE lien_dossier_demandeur ALTER COLUMN dossier TYPE character varying(30);

---
--- Ajout d'une instruction de dossier accepter à une date antérieur sur le dossier
--- PA 013055 12 00001P0 afin de pouvoir lui appliquer l'état "Périmé"

--- instruction
INSERT INTO instruction VALUES
(10,    'PA0130551200001P0',    '2010-08-01',   30, 'arrete',   '', '', 'PA0130551200001P0',    'acceptation',  24, 'accepter', 'Non',  0,  2,  '2012-12-17',   NULL,   '2013-02-17',   '2013-01-17',   'Oui',  'notifier', NULL,   '', NULL,   NULL,   NULL,   NULL,   '', '', '', '', '', '', '', '', '', '', '', '', '', 8,  NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   NULL,   '2012-12-17',   'f',    NULL,   NULL,   NULL,   NULL,   NULL,   '110000000010', NULL,   'f',    NULL,   1,  NULL);

--- dossier
UPDATE dossier SET
dossier = 'PA0130551200001P0',   annee = '  ',   etat = 'accepter', instructeur = 1,  date_demande = '2012-12-17',   date_depot = '2012-12-17',   date_complet = '2012-12-17',   date_rejet = NULL,   date_notification_delai = '2013-01-17',   delai = 2,  date_limite='2013-02-17',   accord_tacite='Oui',  date_decision='2010-08-01',   date_validite='2012-08-01',   date_chantier=NULL,   date_achevement=NULL,   date_conformite=NULL,   parcelle=NULL,   pos=NULL, sig='',   batiment_nombre=NULL,   logement_nombre=NULL,   shon=NULL,   shon_calcul=NULL,   shob=NULL,   lot=NULL,   hauteur=NULL,   amenagement='', parcelle_lot=NULL,   parcelle_lot_lotissement='', description='', temp1='', temp2='', temp3='', temp4='', temp5='', servitude=NULL,   erp='f',    avis_decision=8,  enjeu_erp='f',    enjeu_urba='f',    division=22, autorite_competente=1,  a_qualifier='f',    terrain_references_cadastrales=NULL,   terrain_adresse_voie_numero=NULL,   terrain_adresse_voie=NULL,   terrain_adresse_lieu_dit=NULL,   terrain_adresse_localite=NULL,   terrain_adresse_code_postal=NULL,   terrain_adresse_bp=NULL,   terrain_adresse_cedex=NULL,   terrain_superficie=NULL,   dossier_autorisation='PA0130551200001',  dossier_instruction_type=16, date_dernier_depot='2012-12-17',   version=0,  incompletude='f',    evenement_suivant_tacite=NULL,   evenement_suivant_tacite_incompletude=NULL,   etat_pendant_incompletude=NULL,   date_limite_incompletude=NULL,   delai_incompletude=NULL,   geom=NULL,   geom1=NULL,   dossier_libelle='PA 013055 12 00001P0', numero_versement_archive=NULL WHERE dossier = 'PA0130551200001P0';

--- dossier_autorisation
UPDATE dossier_autorisation SET
dossier_autorisation='PA0130551200001', dossier_autorisation_type_detaille=4,  exercice=NULL,   insee=NULL,   terrain_references_cadastrales=NULL,   terrain_adresse_voie_numero=NULL,   terrain_adresse_voie=NULL,   terrain_adresse_lieu_dit=NULL,   terrain_adresse_localite=NULL,   terrain_adresse_code_postal=NULL,   terrain_adresse_bp=NULL,   terrain_adresse_cedex=NULL,   terrain_superficie=NULL,   arrondissement=NULL,   depot_initial='2012-12-17',   erp_numero_batiment=NULL,   erp_ouvert=NULL,   erp_date_ouverture=NULL,   erp_arrete_decision=NULL,   erp_date_arrete_decision=NULL,   numero_version=0,  etat_dossier_autorisation=2,  date_depot='2012-12-17',   date_decision='2010-08-01',   date_validite='2012-08-01',   date_chantier=NULL,   date_achevement=NULL,   avis_decision=8,  etat_dernier_dossier_instruction_accepte=2,  dossier_autorisation_libelle='PA 013055 12 00001' WHERE dossier_autorisation = 'PA0130551200001';

---
--- Ajout des droits sur les architectes pour les divisionnaires et les chef de service
---
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_frequent_tab', 11);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_frequent_consulter', 11);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_ajouter', 11);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_modifier', 11);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_consulter', 11);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_frequent_tab', 12);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_frequent_consulter', 12);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_ajouter', 12);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_modifier', 12);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'architecte_consulter', 12);

---
--- Augmentation du nombre de caractères des champs contenant le numéro de DI (suite)
---
ALTER TABLE blocnote ALTER COLUMN dossier TYPE character varying(30);
ALTER TABLE consultation ALTER COLUMN dossier TYPE character varying(30);
ALTER TABLE document_numerise ALTER COLUMN dossier TYPE character varying(30);
ALTER TABLE dossier ALTER COLUMN dossier TYPE character varying(30);
ALTER TABLE dossier_commission ALTER COLUMN dossier TYPE character varying(30);
ALTER TABLE dossier_message ALTER COLUMN dossier TYPE character varying(30);
ALTER TABLE lot ALTER COLUMN dossier TYPE character varying(30);
ALTER TABLE terrain ALTER COLUMN dossier TYPE character varying(30);
ALTER TABLE rapport_instruction ALTER COLUMN dossier_instruction TYPE character varying(30);

---
--- Ajout des champs de fusion
---
UPDATE om_requete SET merge_fields = '[aucune_valeur]' WHERE code = 'aucune';
UPDATE om_requete SET merge_fields = '[libelle] [date_commission] [lieu_salle] [heure_commission] [lieu_adresse_ligne1] [lieu_adresse_ligne2]', requete = 'SELECT
    libelle as libelle, 
    to_char(date_commission,''DD/MM/YYYY'') as date_commission, 
    lieu_salle as lieu_salle, 
    heure_commission as heure_commission, 
    lieu_adresse_ligne1 as lieu_adresse_ligne1, 
    lieu_adresse_ligne2 as lieu_adresse_ligne2
FROM
    &DB_PREFIXEcommission
WHERE
    commission = &idx' WHERE code = 'commission';
UPDATE om_requete SET merge_fields = '[libelle_service] [adresse_service] [adresse2_service] [cp_service] [ville_service] [libelle_dossier] [terrain_references_cadastrales_dossier] [nature] [civilite_demandeur] [nom_demandeur] [adresse_demandeur] [code_postal_demandeur] [ville_demandeur] [societe_demandeur] [societe_demandeur] [date_depot_dossier] [date_rejet_dossier] [date_envoi_dossier] [code_barres_consultation] [co_prejet_desc_donnees_techniques] [am_projet_desc_donnees_techniques] [dm_projet_desc_donnees_techniques] [am_lot_max_nb_donnees_techniques] [am_lot_max_shon_donnees_techniques] [su_cstr_shon_tot_donnees_techniques] [su_demo_shon_tot_donnees_techniques] [tab_surface_donnees_techniques] [co_tot_log_nb_donnees_techniques] [co_station_place_nb_donnees_techniques] [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] ', requete = 'SELECT 

    service.libelle as libelle_service, 
    service.adresse as adresse_service, 
    service.adresse2 as adresse2_service, 
    service.cp as cp_service, 
    service.ville as ville_service, 
    dossier.dossier_libelle as libelle_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    dossier_autorisation_type_detaille.libelle as nature, 
    civilite.libelle as civilite_demandeur,
    CASE WHEN demandeur.qualite=''particulier''
        THEN TRIM(CONCAT(demandeur.particulier_nom, '' '', demandeur.particulier_prenom))
        ELSE TRIM(CONCAT(demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_denomination))
    END as nom_demandeur,
    CONCAT(demandeur.numero, '' '', demandeur.voie) as adresse_demandeur,
    demandeur.code_postal as code_postal_demandeur,
    demandeur.localite as ville_demandeur,
    demandeur.personne_morale_denomination as societe_demandeur,
    to_char(dossier.date_depot,''DD/MM/YYYY'') as date_depot_dossier, 
    to_char(dossier.date_rejet,''DD/MM/YYYY'') as date_rejet_dossier, 
    to_char(consultation.date_envoi,''DD/MM/YYYY'') as date_envoi_dossier, 
    consultation.code_barres as code_barres_consultation,
    donnees_techniques.co_projet_desc as co_prejet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9) as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_station_place_nb_donnees_techniques,
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier
    
FROM 


    &DB_PREFIXEconsultation 
    LEFT JOIN &DB_PREFIXEservice 
        ON service.service=consultation.service 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=consultation.dossier 
    LEFT JOIN &DB_PREFIXEdonnees_techniques 
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdossier_autorisation 
        ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation 
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        
         
WHERE 


    consultation.consultation = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE' WHERE code = 'consultation';
UPDATE om_requete SET requete = 'SELECT

    rapport_instruction.dossier_instruction as dossier_instruction_rapport_instruction, 
    analyse_reglementaire as analyse_reglementaire_rapport_instruction,
    description_projet as description_projet_rapport_instruction,
    proposition_decision as proposition_decision_rapport_instruction, 
    dossier.dossier_libelle as libelle_dossier, 
    etat as etat_dossier,
    pos as pos_dossier, 
    servitude as servitude_dossier, 
    terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    terrain_adresse_localite as terrain_adresse_localite_dossier,
    terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    terrain_adresse_bp as terrain_adresse_bp_dossier,
    terrain_adresse_cedex as terrain_adresse_cedex_dossier,
    terrain_superficie as terrain_superficie_dossier,
    dossier.delai as delai_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    particulier_nom as particulier_nom_demandeur,
    particulier_prenom as particulier_prenom_demandeur,
    personne_morale_denomination as personne_morale_denomination_demandeur,
    personne_morale_raison_sociale as personne_morale_raison_sociale_demandeur,
    personne_morale_siret as personne_morale_siret_demandeur,
    personne_morale_nom as personne_morale_nom_demandeur,
    personne_morale_prenom as personne_morale_prenom_demandeur,
    numero as numero_demandeur,
    voie as voie_demandeur,
    demandeur.complement as complement_demandeur,
    lieu_dit as lieu_dit_demandeur,
    localite as localite_demandeur,
    code_postal as code_postal_demandeur,
    bp as bp_demandeur,
    cedex as cedex_demandeur,
    instructeur.nom as nom_instructeur, 
    dossier_autorisation_type_detaille.libelle as libelle_datd, 
    civilite.code as code_civilite,
    avis_decision.libelle as libelle_avis_decision,
    division.chef as chef_division,
    direction.chef as chef_direction,
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9) as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques


FROM 

    &DB_PREFIXErapport_instruction 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=rapport_instruction.dossier_instruction 
    LEFT JOIN &DB_PREFIXEavis_decision 
        ON dossier.avis_decision = avis_decision.avis_decision
    LEFT JOIN &DB_PREFIXEdonnees_techniques
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdivision
        ON dossier.division = division.division
    LEFT JOIN &DB_PREFIXEdirection
        ON division.direction = direction.direction
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur 
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN dossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
    
WHERE 

    rapport_instruction = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE', merge_fields = '[dossier_instruction_rapport_instruction] [analyse_reglementaire_rapport_instruction] [description_projet_rapport_instruction] [proposition_decision_rapport_instruction] [libelle_dossier] [etat_dossier] [pos_dossier] [servitude_dossier] [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_cedex_dossier] [terrain_superficie_dossier] [delai_dossier] [terrain_references_cadastrales_dossier] [particulier_nom_demandeur] [particulier_prenom_demandeur] [personne_morale_denomination_demandeur] [personne_morale_raison_sociale_demandeur] [personne_morale_siret_demandeur] [personne_morale_nom_demandeur] [personne_morale_prenom_demandeur] [numero_demandeur] [voie_demandeur] [complement_demandeur] [lieu_dit_demandeur] [localite_demandeur] [code_postal_demandeur] [bp_demandeur] [cedex_demandeur] [nom_instructeur] [libelle_datd] [code_civilite] [libelle_avis_decision] [chef_division] [chef_direction] [co_projet_desc_donnees_techniques] [am_projet_desc_donnees_techniques] [dm_projet_desc_donnees_techniques] [am_lot_max_nb_donnees_techniques] [am_lot_max_shon_donnees_techniques] [su_cstr_shon_tot_donnees_techniques] [su_demo_shon_tot_donnees_techniques] [tab_surface_donnees_techniques] [co_tot_log_nb_donnees_techniques] [co_statio_place_nb_donnees_techniques]' WHERE code = 'rapport_instruction';
UPDATE om_requete SET requete = 'SELECT

    --
    -- CHAMPS DE L''EVENEMENT D''INSTRUCTION

    instruction.complement as complement_instruction,
    instruction.complement2 as complement2_instruction,
    instruction.code_barres as code_barres_instruction,
    om_lettretype.libelle as libelle_om_lettretype,

    --
    -- INFORMATIONS CONCERNANT LE DOSSIER
    --

    dossier.dossier_libelle as libelle_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_dossier_autorisation,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,

    avis_decision.libelle as libelle_avis_decision,

    --
    -- INSTRUCTEUR
    --

    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    --
    -- ADRESSE TERRAIN
    --

    dossier.terrain_adresse_voie_numero as terrain_adresse_vois_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    dossier.terrain_adresse_bp as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    dossier.terrain_adresse_cedex as terrain_adresse_cedex_dossier,

    arrondissement.libelle as libelle_arrondissement,

    --
    -- PETITIONNAIRE PRINCIPAL
    --

    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as nom_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    petitionnaire_principal.bp as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    petitionnaire_principal.cedex as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --
    -- DELEGATAIRE
    --

    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as nom_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complemennt_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    delegataire.bp as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    delegataire.cedex as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    --
    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    --

    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.bp
        ELSE delegataire.bp
    END as bp_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.cedex
        ELSE delegataire.cedex
    END as cedex_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    --
    -- DATES DI
    --

    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    
    --
    -- SIGNATAIRES
    --
    CONCAT(signataire_arrete.prenom, '' '', signataire_arrete.nom) as arrete_signataire,
    division.chef as chef_division,
    direction.chef as chef_direction,
    
    --
    -- DONNÉES TECHNIQUES
    --
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9) as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques
    
FROM
    &DB_PREFIXEinstruction
LEFT JOIN
    &DB_PREFIXEsignataire_arrete
    ON instruction.signataire_arrete = signataire_arrete.signataire_arrete
LEFT JOIN
    &DB_PREFIXEom_lettretype
    ON instruction.lettretype = om_lettretype.id and om_lettretype.actif IS TRUE
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        instruction.dossier=dossier.dossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        instruction.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur
LEFT JOIN
    (
    &DB_PREFIXElien_dossier_demandeur AS lien_dossier_delegataire
        JOIN &DB_PREFIXEdemandeur as delegataire
        ON
            lien_dossier_delegataire.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
    )
    ON
        instruction.dossier = lien_dossier_delegataire.dossier AND lien_dossier_delegataire.petitionnaire_principal IS FALSE
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN 
    &DB_PREFIXEdirection
    ON division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
WHERE instruction.instruction = &idx', merge_fields = '[complement_instruction] [complement2_instruction] [code_barres_instruction] [libelle_om_lettretype] [libelle_dossier] [libelle_dossier_autorisation] [code_datd] [libelle_datd] [code_dat] [libelle_dat] [code_dit] [libelle_dit] [delai_dossier] [terrain_references_cadastrales_dossier] [libelle_avis_decision] [nom_instructeur] [telephone_instructeur] [division_instructeur] [email_instructeur] [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier] [libelle_arrondissement] [nom_petitionnaire_principal] [numero_petitionnaire_principal] [voie_petitionnaire_principal] [complement_petitionnaire_principal] [lieu_dit_petitionnaire_principal] [bp_petitionnaire_principal] [code_postal_petitionnaire_principal] [localite_petitionnaire_principal] [cedex_petitionnaire_principal] [pays_petitionnaire_principal] [nom_delegataire] [numero_delegataire] [voie_delegataire] [complemennt_delegataire] [lieu_dit_delegataire] [bp_delegataire] [code_postal_delegataire] [ville_delegataire] [cedex_delegataire] [pays_delegataire] [nom_correspondant] [numero_correspondant] [voie_correspondant] [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant] [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant] [date_depot_dossier] [date_decision_dossier] [date_limite_dossier] [date_achevement_dossier] [date_conformite_dossier] [arrete_signataire] [chef_division] [chef_direction] [co_projet_desc_donnees_techniques] [am_projet_desc_donnees_techniques] [dm_projet_desc_donnees_techniques] [am_lot_max_nb_donnees_techniques] [am_lot_max_shon_donnees_techniques] [su_cstr_shon_tot_donnees_techniques] [su_demo_shon_tot_donnees_techniques] [tab_surface_donnees_techniques] [co_tot_log_nb_donnees_techniques] [co_statio_place_nb_donnees_techniques]' WHERE code = 'instruction';
UPDATE om_requete SET requete = 'SELECT
    --
    -- INFORMATIONS CONCERNANT LE DOSSIER
    --

    dossier.dossier_libelle as libelle_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,

    avis_decision.libelle as libelle_avis_decision,

    --
    -- INSTRUCTEUR
    --

    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    --
    -- SIGNATAIRES
    --
    division.chef as division_chef,
    direction.chef as direction_chef,

    --
    -- ADRESSE TERRAIN
    --

    dossier.terrain_adresse_voie_numero as terrain_adresse_vois_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    dossier.terrain_adresse_bp as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    dossier.terrain_adresse_cedex as terrain_adresse_cedex_dossier,

    arrondissement.libelle as libelle_arrondissement,

    --
    -- PETITIONNAIRE PRINCIPAL
    --

    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as nom_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    petitionnaire_principal.bp as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    petitionnaire_principal.cedex as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --
    -- DELEGATAIRE
    --

    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as nom_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complemennt_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    delegataire.bp as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    delegataire.cedex as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    --
    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    --

    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.bp
        ELSE delegataire.bp
    END as bp_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.cedex
        ELSE delegataire.cedex
    END as cedex_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    --
    -- DATES DI
    --

    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    
    --
    -- DONNÉES TECHNIQUES
    --
    donnees_techniques.co_projet_desc as co_projet_desc_donnees_techniques,
    donnees_techniques.am_projet_desc as am_projet_desc_donnees_techniques,
    donnees_techniques.dm_projet_desc as dm_projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    CONCAT(''Habitation : '', donnees_techniques.su_trsf_shon1, 
    '' - Hébergement hôtelier : '', donnees_techniques.su_trsf_shon2, 
    '' - Bureaux : '', donnees_techniques.su_trsf_shon3, 
    '' - Commerce : '', donnees_techniques.su_trsf_shon4,
    '' - Artisanat : '', donnees_techniques.su_trsf_shon5,
    '' - Industrie : '', donnees_techniques.su_trsf_shon6,
    '' - Exploitation agricole ou forestière : '', donnees_techniques.su_trsf_shon7,
    '' - Entrepôt : '', donnees_techniques.su_trsf_shon8,
    '' - Service public ou d''''intérêt collectif : '', donnees_techniques.su_trsf_shon9) as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques

FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as delegataire
    ON
        lien_dossier_demandeur.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN
    &DB_PREFIXEdirection
    ON
        division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
WHERE dossier.dossier = ''&idx''', merge_fields = '[libelle_dossier] [libelle_da] [code_datd] [libelle_datd] [code_dat] [libelle_dat] [code_dit] [libelle_dit] [delai_dossier] [terrain_references_cadastrales_dossier] [libelle_avis_decision] [nom_instructeur] [telephone_instructeur] [division_instructeur] [email_instructeur] [division_chef] [direction_chef] [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier] [libelle_arrondissement] [nom_petitionnaire_principal] [numero_petitionnaire_principal] [voie_petitionnaire_principal] [complement_petitionnaire_principal] [lieu_dit_petitionnaire_principal] [bp_petitionnaire_principal] [code_postal_petitionnaire_principal] [localite_petitionnaire_principal] [cedex_petitionnaire_principal] [pays_petitionnaire_principal] [nom_delegataire] [numero_delegataire] [voie_delegataire] [complemennt_delegataire] [lieu_dit_delegataire] [bp_delegataire] [code_postal_delegataire] [ville_delegataire] [cedex_delegataire] [pays_delegataire] [nom_correspondant] [numero_correspondant] [voie_correspondant] [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant] [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant] [date_depot_dossier] [date_decision_dossier] [date_limite_dossier] [date_achevement_dossier] [date_conformite_dossier] [co_projet_desc_donnees_techniques] [am_projet_desc_donnees_techniques] [dm_projet_desc_donnees_techniques] [am_lot_max_nb_donnees_techniques] [am_lot_max_shon_donnees_techniques] [su_cstr_shon_tot_donnees_techniques] [su_demo_shon_tot_donnees_techniques] [tab_surface_donnees_techniques] [co_tot_log_nb_donnees_techniques] [co_statio_place_nb_donnees_techniques]' WHERE code = 'dossier';
---
--- Mise à jour des états
---
UPDATE om_etat SET titre = '[libelle_service]
[adresse_service]
[adresse2_service]
[cp_service] [ville_service]', corps = '<b>[nature]</b>
Avis sollicité
____________________________________________________________________________________________________________
  Dossier numéro        <b>&departement &commune  [libelle_dossier]</b>  déposé le <b>[date_depot_dossier]</b>
      
                       par         <b>[civilite_demandeur] [nom_demandeur]</b>
        demeurant à         [adresse_demandeur]   [code_postal_demandeur] - [ville_demandeur]
                société         [societe_demandeur]
        sur le terrain        <b>[terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]  [terrain_adresse_code_postal_dossier] -  [terrain_adresse_localite_dossier]</b>
____________________________________________________________________________________________________________
  PJ en communication : 1 exemplaire du dossier







Le respect de la réglementation en vigueur, notamment en matière de délai de réponse ou de décision de l''administration, me conduit à attirer votre attention sur le fait qu''en l absence de réponse dans un délai de 1 mois à dater de la réception de la demande jointe, votre service est reputé émettre un avis favorable sur ladite demande.

Il conviendra cependant, même dans cette éventualité, de me faire retour de l''exemplaire communiqué, dans les meilleurs délais.
 
Je vous prie d''agréer, Monsieur, l''expression de ma considération distinguée.                




                                                                                                              &ville, le [date_envoi_dossier]
                                                                                                              le Maire &delaville


                                                                                                              &nom



RÉFÉRENCES
PJ EN RETOUR : l''exemplaire du dossier communiqué' WHERE id = 'consultation_avec_avis_attendu';
UPDATE om_etat SET titre = '[libelle_service]
[adresse_service]
[adresse2_service]
[cp_service] [ville_service]', corps = '<b>[nature]</b>
Pour information
____________________________________________________________________________________________________________
  Dossier numéro        <b>&departement &commune  [libelle_dossier]</b>  déposé le <b>[date_depot_dossier]</b>
      
                       par         <b>[civilite_demandeur] [nom_demandeur]</b>
        demeurant à         [adresse_demandeur]   [code_postal_demandeur] - [ville_demandeur]
                société         [societe_demandeur]
        sur le terrain        <b>[terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]  [terrain_adresse_code_postal_dossier] -  [terrain_adresse_localite_dossier]</b>
____________________________________________________________________________________________________________
  PJ en communication : 1 exemplaire du dossier







Le respect de la réglementation en vigueur, notamment en matière de délai de réponse ou de décision de l''administration, me conduit à attirer votre attention sur le fait qu''en l absence de réponse dans un délai de 1 mois à dater de la réception de la demande jointe, votre service est reputé émettre un avis favorable sur ladite demande.

Il conviendra cependant, même dans cette éventualité, de me faire retour de l''exemplaire communiqué, dans les meilleurs délais.

Je vous prie d''agréer, [civilite_demandeur] [nom_demandeur], l''expression de ma considération distinguée.                




                                                                                                              &ville, le [date_envoi_dossier]
                                                                                                              le Maire &delaville


                                                                                                              &nom



RÉFÉRENCES
PJ EN RETOUR : l''exemplaire du dossier communiqué' WHERE id = 'consultation_pour_information';
UPDATE om_etat set titre = '[libelle_service]
[adresse_service]
[adresse2_service]
[cp_service] [ville_service]', corps= '<b>[nature]</b>
Avis solicité pour conformité
____________________________________________________________________________________________________________
  Dossier numéro        <b>&departement &commune  [libelle_dossier]</b>  déposé le <b>[date_depot_dossier]</b>
      
                       par         <b>[civilite_demandeur] [nom_demandeur]</b>
        demeurant à         [adresse_demandeur]   [code_postal_demandeur] - [ville_demandeur]
                société         [societe_demandeur]
        sur le terrain        <b>[terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]  [terrain_adresse_code_postal_dossier] -  [terrain_adresse_localite_dossier]</b>
____________________________________________________________________________________________________________
  PJ en communication : 1 exemplaire du dossier







Le respect de la réglementation en vigueur, notamment en matière de délai de réponse ou de décision de l''administration, me conduit à attirer votre attention sur le fait qu''en l absence de réponse dans un délai de 1 mois à dater de la réception de la demande jointe, votre service est réputé émettre un avis favorable sur ladite demande.

Il conviendra cependant, même dans cette éventualité, de me faire retour de l''exemplaire communiqué, dans les meilleurs délais.
 
Je vous prie d''agréer, Monsieur, l''expression de ma considération distinguée.                




                                                                                                              &ville, le [date_envoi_dossier]
                                                                                                              le Maire &delaville


                                                                                                              &nom



RÉFÉRENCES
PJ EN RETOUR : l''exemplaire du dossier communiqué' WHERE id = 'consultation_pour_conformite';
UPDATE om_etat SET titre = '&ville
[libelle_datd] : [etat_dossier]', corps = 'DOSSIER N [dossier_instruction_rapport_instruction]
NOM du DEMANDEUR TITULAIRE
[code_civilite] [particulier_nom] [particulier_prenom_demandeur] [personne_morale_nom_demandeur] [personne_morale_nom_demandeur]
[personne_morale_denomination_demandeur] [personne_morale_raison_sociale_demandeur] [personne_morale_siret_demandeur]

ADRESSE DU DEMANDEUR TITULAIRE: [numero_demandeur] [voie_demandeur] [complement_demandeur] [lieu_dit_demandeur] - [code_postal_demandeur] [localite_demandeur] [cedex_demandeur] [bp_demandeur]

NATURE DES TRAVAUX: [tl]


ADRESSE DES TRAVAUX: [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] - [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier] [terrain_adresse_bp_dossier]

DESTIONATION en m2 : [terrain_superficie_dossier]        SECTEUR POS :         [pos_dossier]

[servitude_dossier]

[description_projet_rapport_instruction]

[analyse_reglementaire_rapport_instruction]

&rapport_instruction_consultation

En consequence, il est propose : [proposition_decision_rapport_instruction]

Fait le: &aujourdhui         Vu le:

                         Le technicien instructeur                         Le chef de subdivision


                         [chef_division]' WHERE id = 'rapport_instruction';
UPDATE om_etat set titre = '[libelle_datd]
RECAPITULATIF AU &aujourdhui
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Surface hors- oeuvre      brute : <b>[shob] m2</b>     nette : <b>[shon] m2</b>   hauteur :   <b>[hauteur] m </b>
Nb de logements : <b>[co_tot_log_nb_donnees_techniques]</b>     Nb de bâtiments : <b>[batiment_nombre]</b>

Travaux       : <b>[travaux]</b>

Date de dépot : <b>[date_depot_dossier]</b>
Date limite de notification : <b>[date_notification_delai]</b>
Date limite d''instruction: <b>[date_limite_dossier]</b>' WHERE id = 'dossier';

---
---Mise à jour des lettres-types
---
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

Vous avez déposé le [date_depot_dossier] une demande de [nature].

Lors de ce dépôt, le récépissé de votre dossier indiquait qu''en cas de silence de l''Administration à la fin du délai d''instruction de droit commun de . . . .  . mois, vous bénéficierez d''un [nature] tacite.

Une majoration de délai vous a été notifiée.
Un des services consulté au cours de l''instruction de votre dossier sollicite des pièces complémentaires
Il convient donc que vous me fassiez parvenir les pièces suivantes :
 &complement

En l''absence de ces éléments, la demande s''acheminerait vers un refus.

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée



                                                                              &ville , le  [date]

                                                                              Le Maire  Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat,
                                                                              A la Politique Foncière


                                                                                               &nom


Nb : copie de la présente lettre est transmise au représentant de l''Etat.
\\p                                          INFORMATION - A LIRE ATTENTIVEMENT



DELAIS ET VOIES DE RECOURS :
le pétitionnaire qui désire contester la décision peut saisir le tribunal administratif compétent d''un recours contentieux dans un délai de DEUX MOIS à partir de la transmission de la décision attaquée. Il peut également saisir d''un recours gracieux l''auteur de la décision hiérarchique, le Ministre chargé de l''Urbanisme ou le préfet pour les [nature] délivrés au nom de l''état.

Cette démarche prolonge le délai de recours qui doit alors être introduit dans les deux mois suivant la réponse (l''absence de réponse au terme de 2 mois vaut rejet implicite)' WHERE id = 'piece_complementaire_service';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]' WHERE id = 'arrete';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]' WHERE id = 'attestation_affichage';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

Vous avez déposé ce jour une demande de [nature]. Le délai d''instruction de votre dossier est de [delai_dossier] mois. En l''absence de courrier de la part de l''administration dans ce délai, vous bénéficierez d''un [nature] tacite(1).

* Toutefois, dans le mois qui suit le dépôt de votre dossier, l''administration peut vous écrire :
   - soit pour vous avertir qu''un autre délai est applicable, lorsque le code de l''urbanisme l''a prévu pour permettre les consultations nécessaires (si votre projet nécessite la consultation d''autres services ...)
   - soit pour vous indiquer qu''il manque une ou plusieurs pièces à votre dossier,
   - soit pour vous informer que votre projet  correspond à un des cas où un accord tacite n''est pas possible.

* Si vous recevez une telle lettre avant la fin du premier mois, celle-ci remplacera le premier récépissé.

* Si vous n''avez rien reçu à la fin du premier mois suivant le dépôt, le délai de [delai_dossier] mois ne pourra être modifié. Sans aucun courrier de l''administration à l''issue de ce délai de [delai_dossier] mois, vous pourrez commencer les travaux (2) quinze jours après la date à laquelle le [nature] tacite est acquis. Vous devrez péalablement avoir :
   - adressé au maire, en 3 exemplaires, une déclaration d''ouverture de chantier (vous trouverez un modèle de déclaration CERFA n° 13407 01 à la mairie ou sur le site internet urbanisme du gouvernement)
   - affiché sur le terrain ce recepissé sur lequel la mairie a mis son cachet pour attester la date de dépôt ;
   - installé sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie publique décrivant le projet (vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux).

* ATTENTION : le permis n''est définitif qu''en l''absence de recours ou de retrait :
   - dans un délai de 2 mois à compter de l''affichage sur le terrain, sa légalité peut être contestée par un tiers. Dans ce cas l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours.
    - dans un délai de 3 mois après la date de [nature], l''autorité compétente peut le retirer, si elle l''estime illégal. Elle est tenue de vous en informer préalablement et de vous permettre de répondre à ses observations.
\\p
Le projet ayant fait l''objet de la demande de [nature] dont les références sont indiquées dans le cadre ci-dessus déposé à la mairie le [date_depot_dossier] fera l''objet d''un permis tacite (1) à défaut de réponse de l''administration [delai_dossier] mois après cette date. Les travaux (2) pourront alors être exécutés après affichage sur le terrain du présent récépissé et d''un panneau décrivant le projet conforme au modèle réglementaire.





                                                                                                              &ville , le  &datecourrier

                                                                                            Cachet de la mairie et signature du receveur








(1) Permis tacite : afin de vous éviter d''être en infraction, il vous est recommandé, dans le cas d''un permis tacite, de vous assurer auprès de l''administration de la légalité dudit permis avant tout commencement de travaux. Sur simple demande de votre part, le maire pourra alors vous délivrer une attestation certifiant qu''aucune décision de refus n''a été prise à votre insu.
(2) Certains travaux ne peuvent pas être commencés dès la délivrance de [nature] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas.







                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
Le permis tacite peut faire l''objet d''un recours gracieux ou d''un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.' WHERE id = 'recepisse_PD';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]' WHERE id = 'non_opposition_tacite';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]' WHERE id = 'standard';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]' WHERE id = 'dossier_irrecevable';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

Vous avez déposé (1) ce jour des pièces complémentaires concernant le dossier de [nature] dont les références sont indiqués dans les cadre ci-dessus.

Le délai d''instruction de votre dossier est de 1 mois. En l''absence de courrier de la part de l''administration dans ce délai, vous bénéficierez d''une décision de non-opposition aux travaux ou aménagements décrits dans la déclaration préalable (2).

* Toutefois, dans le mois qui suit le dépôt de pièces complémentaires, l''administration peut vous écrire :
    - soit pour vous avertir qu''un autre délai est applicable, lorsque le code de l''ubanisme l''a prévu pour permettre les consultations nécessaires (si votre projet nécessite la consultation s''autres services ...)
   - soit pour vous indiquer qu''il manque encore une ou plusieurs pièces à votre dossier (1)
   - soit pour vous informer que votre projet correspond à un des cas où un permis tacite n''est pas possible.
Si vous recevez une telle lettre  avant la fin du premier mois, celle-ci remplacera et annulera le présent récépissé.

* Si vous n''avez rien reçu à la fin du premier mois suivant le dépôt des pièces complémentaires, le délai de 1 mois ne pourra plus être modifié. Si aucun courrier de l''administration ne vous est parvenu à l''issu du délai de 1 mois, vous pourrez commencer les travaux (3) après avoir respecté les consignes suivantes :
- adresser au maire, en 3 exemplaires, une déclaration d''ouverture de chantier (vous trouverez un modèle de déclaration CERFA n° 13407 01 à la mairie ou sur le site internet urbanisme du gouvernement)
   - afficher sur le terrain ce récépissé sur lequel la mairie a mis son cachet pour attester la date de dépôt ;
   - installer sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie publique décrivant le projet (vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux).

ATTENTION : le permis n''est définitif qu''en l''absence de recours ou de retrait :
   - dans un délai de 2 mois à compter de l''affichage sur le terrain, sa légalité peut être contestée par un tiers. Dans ce cas l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours.
    - dans un délai de 3 mois après la date de [nature], l''autorité compétente peut la retirer, si elle l''estime illégal. Elle est tenue de vous en informer préalablement et de vous permettre de répondre à ses observations.

Le projet ayant fait l''objet de la demande de [nature] dont les références sont indiquées dans le cadre ci-dessus déposé à la mairie le [date_depot_dossier] fera l''objet d''un permis tacite (3) à défaut de réponse de l''administration [delai_dossier] mois après cette date. Les travaux pourront alors être exécutés après affichage sur le terrain du présent récépissé et d''un panneau décrivant le projet conforme au modèle réglementaire.



                                                                                                              &ville , le  &datecourrier


                                                                                                              Cachet de la mairie
                                                                                                             Et signature du receveur




(2) Les travaux ne peuvent  être entrepris, conformément au projet décrit dans la demande, qu''à compter de la décision.

Afin de vous évitez d''être en infraction, il vous est recommandé, dans le cas d''une décision  tacite de non-opposition, de vous assurer auprès des services municipaux que  ladite déclaration préalable n''a pas fait l''objet d''une décision d''opposition avant de commencer les travaux. Dans cette éventualité, il vous sera délivré une attestation certifiant qu''aucune décision de non-opposition n''a été prise à votre encontre.

Dans le délai de deux mois à compter de la date de décision tacite, il vous sera notifié éventuellement un arrêté fixant les taxes et participations applicables au projet.

(3) Certains travaux ne peuvent pas être commencés dès la délivrance de [nature] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas.

(4) Le maire ou le préfet en délivre certificat sur simple demande.

\\p
                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
La déclaration préalable peut faire l''objet d''un d''un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DROITS DE TIERS
La déclaration préalable est acquise sous réserve des droits des tiers : elle vérifie la conformité du projet aux règles et servitudes d''urbanisme. Elle ne vérifie pas si le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si le permis respecte les règles d''urbanisme.

VALIDITE
La déclaration préalable est caduque si travaux ne sont effectués dans un délai de deux ans à compter de la date de décision tacite de non-opposition ou si les travaux sont interrompus pendant un délai supérieur d''une année.

&complement1' WHERE id = 'recepisse_DPC_DP';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

Vous avez déposé ce jour une [nature] à des travaux ou aménagements non soumis à permis. Le délai d''instruction de votre dossier est de [delai_dossier] mois. En l''absence de courrier de la part de l''administration dans ce délai, vous bénéficierez d''une décision de non-opposition à ces travaux ou aménagements (1).

* Toutefois, dans le mois qui suit le dépôt de votre dossier, l''administration peut vous écrire:
    - soit pour vous avertir qu''un autre délai est applicable, lorsque le code de l''urbanisme l''a prévu pour permettre les consultations nécessaires (si votre projet nécessite la consultation d''autres services ...)
    - soit pour vous indiquer qu''il manque une ou plusieurs pièces à votre dossier,

* Si vous recevez une telle lettre avant la fin du mois qui suit le dépôt de votre déclaration, celle ci remplacera le présent récépissé.

* Si vous n''avez rien reçu à la fin du premier mois suivant le dépôt, le délai de [delai_dossier] mois ne pourra être modifié. Sans courrier de l''administration à l''issu de ce délai de [delai_dossier] mois, vous pourrez commencer les travaux (2) après avoir :
  - affiché sur le terrain ce recepissé sur lequel la mairie a mis son cachet pour attester la date de dépôt ;
  - installé sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie puiblique décrivant le projet (Vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux)

* Attention : la décision de non-opposition n''est définitive qu''en l''absence de recours. En effet, dans un délai de 2 mois à compter de l''affichage sur le terrain, sa légalité peut être contestée par un tiers devant le tribunal administratif compétent. Dans ce cas l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours (L424-5).

A défaut de réponse de l''administration dans un délai de [delai_dossier], à compter du [date_depot_dossier] (date de dépôt en mairie de la [nature] dont les références sont indiquées dans le cadre ci-dessus) votre  [nature] fera l''objet d''une décision de non-opposition (1). Les travaux ou aménagements (2)pourront alors être exécutés après affichage sur le terrain du présent récépissé et d''un panneau décrivant le projet conforme au modèle réglementaire.



                                                                                                              &ville , le  &datecourrier

                                                                                            Cachet de la Mairie et signature du receveur



(1) Décision de non-opposition : afin de vous éviter d''être en infraction, il vous est recommandé de vous assurer auprès de l''administration de la légalité dudit permis avant tout commencement de travaux. Sur simple demande de votre part, le maire pourra alors vous délivrer une attestation certifiant qu''aucune décision de refus n''a été prise à votre insu. En effet, si le projet était irrégulier, les tribunaux pourraient vous astreindrede remettre les lieux en leur état inital.
(2) certains travaux ne peuvent pas débuter dès la décision de non-opposition. C''est le cas des travaux de coupe et abattage des arbres, des transformations de logement en un autre usage dans les communes de plus de 200.000 habitants et dans les départements de Paris, des hauts de seine, de la Seine Saint Denis et du Val de Marne, ou des installations classées pour la protection de l''environnement. Vous pouvez vérifier auprès des services municipaux que votre projet n''entre pas dans ces cas.





                                     INFORMATIONS A LIRE ATTENTIVEMENT :





DELAIS ET VOIES DE RECOURS :
La décision de non-opposition peut faire l''objet d''un recours gracieux ou d''un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.


&complement' WHERE id = 'recepisse_DP';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,


Vous avez déposé une demande de [nature], le [date_depot_dossier] pour un projet de [travaux] ; [temp1], situé [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]  [terrain_adresse_code_postal_dossier] -  [terrain_adresse_localite_dossier].

Le récépissé de dépôt de votre dossier indiquait qu''en cas de silence de l''Administration à l''expiration du délai d''instruction de droit commun de 3 mois,  vous bénéficieriez d''un permis tacite.

Toutefois ce même récépissé vous informait également que, dans le mois suivant le dépôt  de votre dossier, l''Administration pouvait vous écrire :

-soit pour vous avertir qu''un autre délai d''instruction est applicable, en vertu du code de l''urbanisme
-soit pour vous indiquer qu''il manque une ou plusieurs pièces à votre dossier
-soit pour vous informer que votre projet correspond à un des cas où un permis tacite n''est pas possible.

Je vous informe que le délai d''instruction de votre projet doit effectivement être modifié :

MODIFICATION DU DELAI D''INSTRUCTION DE LA DEMANDE DE PERMIS DE CONSTRUIRE :

Votre demande de permis de construire porte sur un projet situé dans le périmètre de protection des immeubles classés ou inscrits au titre des Monuments Historiques.

Conformément à l''article R423-28 b du code de l''urbanisme, le délai d''instruction de votre permis de construire est donc porté à 6 mois à compter de la réception en mairie d''un dossier complet suivant l''article R423-19 du code de l''urbanisme.

Ce délai annule et remplace le délai de droit commun de 3 mois, qui figure sur le récépissé de dépôt de votre demande de permis de construire.

Conformément aux articles R423-34 à R423-37 du code de l''urbanisme, votre délai d''instruction pourra faire l''objet d''une prorogation exceptionnelle. Cette dernière vous sera notifiée avant l''expiration du délai d''instruction.
Je vous informe également que votre dossier n''est pas complet.


DEMANDE DE PIECES MANQUANTES DANS LE DOSSIER DE DEMANDE DE PERMIS DE CONSTRUIRE :

Conformément à l''article R423-38 du code de l''urbanisme, je vous informe que, après examen des pièces jointes à votre demande de permis de construire les pièces suivantes sont manquantes ou insuffisantes. Il convient donc que vous me fassiez parvenir les pièces suivantes :

&complement


Conformément à l''article R423-39 du code de l''urbanisme, je vous informe que :

*Les pièces manquantes doivent être adressées à la mairie dans le délai de 3 mois à compter de la réception du présent courrier (Hôtel de ville service permis de construire BP 90196 13637 Arles cedex).
*A défaut de production de l''ensemble des pièces manquantes dans ce délai, votre demande de permis de construire fera l''objet d''une décision tacite de rejet
*Le délai d''instruction commencera à courir à compter de la réception des pièces manquantes par la mairie


CAS OU UN PERMIS TACITE N''EST PAS POSSIBLE

L''article R424-3 du code de l''urbanisme prévoit que, par exception au b de l''article R424-1, dans les cas où la décision est soumise à l''accord de l''Architecte des Bâtiments de France (ABF), le défaut de notification d''une décision expresse dans le délai d''instruction vaut décision implicite de rejet  lorsque, dans le délai de 4 mois suivant la réception de la demande d''avis, l''Architecte des Bâtiments de France notifie un avis défavorable ou un avis favorable assorti de prescriptions sur le projet. Par conséquent si vous recevez un courrier de l''ABF vous informant qu''il a émis un avis défavorable ou un avis favorable assorti de prescriptions sur votre projet, un permis de construire tacite n''est pas possible. Si alors aucune décision ne vous est envoyée dans le délai de 6 mois à compter du dépôt de toutes les pièces manquantes en mairie, vous pourrez considérer que votre demande est refusée, en application de l''article R424-3 du code de l''urbanisme.

Si vous ne recevez pas un tel courrier de l''ABF et qu''aucune décision ne vous est envoyée dans le délai de 6 mois à compter du dépôt de toutes les pièces manquantes en mairie, votre projet fera l''objet d''un permis de construire tacite.


En cas d''obtention d''un permis tacite (1), vous pourrez alors commencer les travaux (2) après avoir :
          - adressé au maire en trois exemplaires, une déclaration d''ouverture de chantier(vous trouverez un modèle de déclaration CERFA n°13407*01 à la mairie ou sur le site internet : http://developpement-durable.gouv.fr/
          - affiché sur le terrain le présent courrier ;
          - installé sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie publique décrivant le projet. Vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux)
ATTENTION  : LE PERMIS N''EST DEFINITIF QU''EN L''ABSENCE DE RECOURS OU DE RETRAIT :
          - Dans le délai de deux mois à compter de son affichage sur le terrain, sa légalité peut être contestée par un tiers. Dans ce cas, l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours.
          - dans le délai de trois mois après la date du permis, l''autorité compétente peut le retirer , si elle l''estime illégal. Elle est tenue de vous en informer préalablement et de vous permettre de répondre à ses observations.

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée




                                                                              &ville , le  &datecourrier
                                                                              David GRZYB
                                                                              Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A la Politique Foncière



(1) Le maire en délivre certificat sur simple demande.
(2) Certains travaux ne peuvent pas être commencés dès la délivrance du permis et doivent être différés : c''est le cas du permis de démolir, ou des travaux en site inscrit, ainsi que des travaux faisant l''objet de prescriptions au titre de l''archéologie préventive.


                            INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
La déclaration préalable ou le permis peut faire l''objet d'' un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.' WHERE id = 'majoration_DPC_hors_SS';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,


Vous avez déposé une demande de [nature], le [date_depot_dossier] pour un projet de [travaux] ; [temp1], situé [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]  [terrain_adresse_code_postal_dossier] -  [terrain_adresse_localite_dossier].

Le récépissé de dépôt de votre dossier indiquait qu''en cas de silence de l''Administration à l''expiration du délai d''instruction de droit commun de trois mois,  vous bénéficieriez d''un permis tacite.

Toutefois ce même récépissé vous informait également que, dans le mois suivant le dépôt  de votre dossier, l''Administration pouvait vous écrire :

-soit pour vous avertir qu''un autre délai d''instruction est applicable, en vertu du code de l''urbanisme
-soit pour vous indiquer qu''il manque une ou plusieurs pièces à votre dossier
-soit pour vous informer que votre projet correspond à un des cas où un permis tacite n''est pas possible.

Je vous informe que le délai d''instruction de votre projet doit effectivement être modifié :

MODIFICATION DU DELAI D''INSTRUCTION DE LA DEMANDE DE PERMIS DE CONSTRUIRE :

Votre projet porte sur la construction d''un immeuble de grande hauteur tel que défini aux articles R122-2 et R122-3 du code de la Construction et de l''Habitation.

Conformément aux articles L123-1 et R123-1 du code de l''environnement, la construction d''un immeuble de grande hauteur est soumise à enquête publique.

Conformément  à l''article R423-20 du code de l''urbanisme, par exception à l''article R423-19 du même code, le délai d''instruction d''un dossier complet part de la réception par l''autorité compétente du rapport du commissaire enquêteur ou de la commission d''enquête.

Conformément à l''article R423-32 du code de l''urbanisme, le délai d''instruction de votre demande de permis de construire est porté à deux mois à compter de la réception par la Ville du rapport du commissaire enquêteur ou de la commission d''enquête

Ce délai annule et remplace le délai de droit commun de 3 mois, qui figure sur le récépissé de dépôt de votre demande de permis de construire.

Conformément  aux articles R423-34 à R423-37 du code de l''urbanisme, votre délai d''instruction pourra faire l''objet d''une prorogation exceptionnelle. Cette dernière vous sera notifiée avant l''expiration du délai d''instruction.

Je vous informe également que votre dossier n''est pas complet :


DEMANDE DE PIECES MANQUANTES DANS LE DOSSIER DE DEMANDE DE PERMIS DE CONSTRUIRE :

Conformément à l''article R423-38 du code de l''urbanisme, je vous informe que, après examen des pièces jointes à votre demande de permis de construire les pièces suivantes sont manquantes ou insuffisantes. Il convient donc que vous me fassiez parvenir les pièces suivantes :

&complement


Conformément à l''article R423-39 du code de l''urbanisme, je vous informe que :

- les pièces manquantes doivent être adressées à la mairie dans le délai de 3 mois à compter de la réception du présent courrier (Hôtel de ville service permis de construire BP 90196 13637 Arles cedex).
- A défaut de production de l''ensemble des pièces manquantes dans ce délai, votre demande de permis de construire fera l''objet d''une décision tacite de rejet



CAS OU UN PERMIS TACITE N''EST PAS POSSIBLE

Enfin, conformément  à l''article R424-2 du code de l''urbanisme, le défaut de notification d''une décision expresse dans le délai d''instruction vaut décision implicite de rejet


                                                                              &ville , le  &datecourrier
                                                                              David GRZYB
                                                                              Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A la Politique Foncière





                            INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
La déclaration préalable ou le permis peut faire l''objet d'' un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.' WHERE id = 'majoration_IGH';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

J''ai l''honneur de vous informer que votre projet est soumis, en raison de son emplacement, de son utilisation ou de sa nature à un régime d''autorisation ou à des prescriptions prévues par une autre législation ou réglementation que le code de l''urbanisme, et qu''il nécessite l''avis d''un service ou d''une commission ne relevant pas de mon autorité.

En conséquence, le délai d''instruction initialement fixé par l''article R 423-23 du code de l''urbanisme est  MODIFIE.

Le délai d''instruction de votre [nature] est donc majoré par la loi (Art. R423-24 à R.423-33 du code de l''urbanisme) et la nouvelle date limite d''instruction est fixée au  [date_limite_dossier].
La décision doit donc vous être notifiée  avant expiration de ce délai.

En l''absence d''une notification d''une décision par l''autorité compétente la présente lettre vaudra    [nature] tacite.

Dans le délai de deux mois à compter de la décision tacite, l''autorité compétente pourra prendre un arrêté fixant les participations applicables au projet.

&complement

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée.





                                                                             &ville , le
                                                                              Le Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A la Politique Foncière


                                                                              &nom



NB : Copie de la présente lettre est transmise au représentant de l''Etat dans les conditions définies aux articles L2131-1 et L2131 -2 du  code des collectivités territoriales.
\\p





(1) Les travaux ne peuvent  être entrepris, conformément au projet décrit dans la demande, qu''à compter de la décision tacite.
UN PERMIS ENTACHE D''ILLIGALITE PEUT ETRE RETIRE PAR L''ADMINISTRATION COMPETENTE PENDANT LE DELAI LEGAL DE RECOURS CONTENTIEUX
 Aussi, afin de vous évitez d''être en infraction, il vous est recommandé, dans le cas d''une décision tacite, de vous assurer auprès des services municipaux qu''une décision de refus n''a été prise avant de commencer les travaux. Dans cette éventualité, il vous sera délivré sur simple demande, une attestation (2) certifiant qu''aucune décision de refus n''a été prise à votre encontre.

 En effet, si le permis tacite, était irrégulier, il serait retiré et les tribuneaux pourraient vous astreindre de remettre les lieux en leur état initial

Dans le délai de deux mois à compter de la date de décision tacite, il vous sera notifié éventuellement un arrêté fixant les taxes et participations applicables au projet.

Certains travaux ne peuvent pas être commencés dès la délivrance de [nature] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas.

(2) Le maire ou le préfet.

\\p
                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
La déclaration préalable ou le permis peut faire l''objet d'' un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.' WHERE id = 'majoration';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = '' WHERE id = 'majoration_ss_perimetre_mh';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,
J''ai l''honneur de vous informer que votre projet est soumis, en raison de son emplacement, de son utilisation ou de sa nature à un régime d''autorisation ou à des prescriptions prévues par une autre législation ou réglementation que le code de l''urbanisme, et qu''il nécessite l''avis d''un service ou d''une commission ne relevant pas de mon autorité.

En conséquence, le délai d''instruction initialement fixé par l''article R 423-23 du code de l''urbanisme est MODIFIE.
Le délai d''instruction de votre [nature] est donc majoré par la loi (Art. R423-24 à R.423-33 du code de l''urbanisme) et la nouvelle date limite d''instruction est fixée au  [date_limite_dossier].
La décision doit donc vous être notifiée  avant expiration de ce délai.

Le défaut de notification d''une décision au [date_limite_dossier], vaudra acceptation tacite (1).

&complement

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée.




                                                                              &ville , le
                                                                              Le Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat,
                                                                              A la Politique Foncière




                                                                              &nom



NB : Copie de la présente lettre est transmise au représentant de l''Etat dans les conditions définies aux articles L2131-1 et L2131 -2 du  code des collectivités territoriales.
\\p




(1) Les travaux ne peuvent  être entrepris, conformément au projet décrit dans la demande, qu''à compter de la décision tacite.
UN PERMIS ENTACHE D''ILLIGALITE PEUT ETRE RETIRE PAR L''ADMINISTRATION COMPETENTE PENDANT LE DELAI LEGAL DE RECOURS CONTENTIEUX
 Aussi, afin de vous évitez d''être en infraction, il vous est recommandé, dans le cas d''une décision tacite, de vous assurer auprès des services municipaux qu''une décision de refus n''a été prise avant de commencer les travaux. Dans cette éventualité, il vous sera délivré sur simple demande, une attestation (2) certifiant qu''aucune décision de refus n''a été prise à votre encontre.

 En effet, si le permis tacite, était irrégulier, il serait retiré et les tribuneaux pourraient vous astreintdre de remettre les lieux en leur état initial

Dans le délai de deux mois à compter de la date de décision tacite, il vous sera notifié éventuellement un arrêté fixant les taxes et participations applicables au projet.

Certains travaux ne peuvent pas être commencés dès la délivrance de [nature] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas.

(2) Le maire ou le préfet.

\\p
                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
La déclaration préalable ou le permis peut faire l''objet d'' un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.' WHERE id = 'majorationtacite';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]' WHERE id = 'majoration_enquete_publique';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

Vous avez déposé le [date_depot_dossier] une demande de [nature].

Lors de ce dépôt, le récépissé de votre dossier indiquait qu''en cas de silence de l''Administration à la fin du délai d''instruction de droit commun de [delai_dossier]  mois, vous bénéficierez d''un [nature] tacite.

Le récépissé vous informait également de la possibilité de modification de ce délai dans les conditions fixées au code de l''Urbanisme.

Je vous informe que votre projet rentre dans ce cadre  du fait qu''il est soumis, en raison de son emplacement, de son utilisation ou de sa nature à un régime d''autorisation ou à des prescriptions prévues par une autre législation ou réglementation que le code de l''urbanisme, et qu''il nécessite l''avis d''un service ou d''une commission ne relevant pas de mon autorité.

Par  conséquent, le délai d''instruction pour votre dossier est de  .... mois en application des articles R 423-23  à R 423-33 du code de l''urbanisme.

 Par ailleurs, je vous informe que mes services ne peuvent entreprendre l''instruction de votre demande de [nature], enregistrée sous les références portées dans le cadre ci dessus, car elle est INCOMPLETE.

Il convient donc que vous me fassiez parvenir les pièces suivantes :
 &complement

Le délai d''instruction notifié ci-dessus commencera à courir à compter de la réception  de l''intégralité des pièces et informations manquantes en mairie d''Arles Service permis de construire - 5, rue du cloître).

Vous disposez de TROIS MOIS  à compter de la réception de la présente lettre, pour faire parvenir à la mairie d''Arles , service Gestion du Territoire - 5, rue du Cloître, l''intégralité des pièces et informations manquantes .

  Le défaut de production de l''ensemble des pièces et informations manquantes dans ce délai de TROIS MOIS , vaudra  DECISION TACITE DE REJET de votre dossier de [nature]  (art.423-39b du code de l''urbanisme).

Toutefois, si la consultation de l''Architecte des Bâtiments de France est nécessaire pour votre projet et s''il émet un avis défavorable ou un avis favorable assorti de prescriptions avant la fin de votre délai d''instruction, vous ne pourrez plus vous prévaloir d''un permis tacite ( article R 424-3 du code de l''urbanisme). Dans une telle hypothèse, vous en seriez directement informé par les services de l''Architecte des Bâtiments de France( SDAP des Bouches du Rhône-3 rue du Cloître 13200 Arles). Le silence de l''Administration équivaudrait alors à un REFUS.

J''appelle également votre attention sur le fait que votre délai d''instruction pourrait faire l''objet d''une prolongation exceptionnelle conformément aux articles R 423-34 à R 423-37 du code de l''Urbanisme.
Si l''une de ces situations se présente sur votre dossier, vous serez informé par courrier avant l''expiration du délai d''instruction fixé ci-dessus.




Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée



                                                                              &ville , le  [date_rejet_dossier]
                                                                              Le Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat,
                                                                              A la Politique Foncière


                                                                               &nom


Nb : copie de la présente lettre est transmise au représentant de l''Etat.
\\p                                          INFORMATION - A LIRE ATTENTIVEMENT



DELAIS ET VOIES DE RECOURS :
le pétitionnaire qui désire contester la décision peut saisir le tribunal administratif compétent d''un recours contentieux dans un délai de DEUX MOIS à partir de la transmission de la décision attaquée. Il peut également saisir d''un recours gracieux l''auteur de la décision hiérarchique, le Ministre chargé de l''Urbanisme ou le préfet pour les [nature] délivrés au nom de l''état.

Cette démarche prolonge le délai de recours qui doit alors être introduit dans les deux mois suivant la réponse (l''absence de réponse au terme de 2 mois vaut rejet implicite)' WHERE id = 'piececomplementaire';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Le Maire,


Vu la demande susvisée
Vu le Code de l''Urbanisme, notamment ses articles L 421-1 et L 421-6, R 421-1 et R 421-14 à R 421-16.
Vu le permis de construire N° &departement &commune  [libelle_dossier] délivré le  [date_decision_dossier]



                                                         A R R E T E

ARTICLE 1:	Le permis de construire n° &departement &commune  [libelle_dossier]     délivré le   [date_decision_dossier]  à [civilite_demandeur] [nom_demandeur]  est retiré.

&complement



                                                                                                 &ville , le  &datecourrier
                                                                                                 le Maire &delaville
                                                                                                 &titreelu



                                                                                                              &nom' WHERE id = 'retrait';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

Vous avez déposé (1) ce jour des pièces complémentaires concernant le dossier de [nature] dont les références sont indiqués dans les cadre ci-dessus.

Le délai d''instruction de votre dossier est de [delai_dossier]mois. En l''absence Si de courrier de la part de l''administration dans ce délai, vous bénéficierez d''un permis tacite (2).

* Toutefois, dans le mois qui suit le dépôt de pièces complémentaires, l''administration peut vous écrire :
    - soit pour vous avertir qu''un autre délai est applicable, lorsque le code de l''ubanisme l''a prévu pour permettre les consultations nécessaires (si votre projet nécessite la consultation s''autres services ...)
   - soit pour vous indiquer qu''il manque encore une ou plusieurs pièces à votre dossier (1)
   - soit pour vous informer que votre projet correspond à un des cas où un permis tacite n''est pas possible.

Si vou srecevez une telle lettreavant la fin du premier mois, celle-ci remplacera et annulera le présent récépissé.

Si vous n''avez rien reçu à la fin du premier mois suivant le dépôt des pièces complémentaires, le délai de [delai_dossier] mois ne pourra plus être modifié. Si aucun courrier de l''administration ne vous est parvenu à l''issu du délai de [delai_dossier] mois, vous pourrez commencer les travaux (3) après avoir respecté les consignes suivantes :
  - adresser au maire, en 3 exemplaires, une déclaration d''ouverture de chantier (vous trouverez un modèle de déclaration CERFA n° 13407 01 à la mairie ou sur le site internet urbanisme du gouvernement)
   - afficher sur le terrain ce récépissé sur lequel la mairie a mis son cachet pour attester la date de dépôt ;
   - installer sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie publique décrivant le projet (vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux).

* ATTENTION : le permis n''est définitif qu''en l''absence de recours ou de retrait :
   - dans un délai de 2 mois à compter de l''affichage sur le terrain, sa légalité peut être contestée par un tiers. Dans ce cas l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours.
    - dans un délai de 3 mois après la date de [nature], l''autorité compétente peut le retirer, si elle l''estime illégal. Elle est tenue de vous en informer préalablement et de vous permettre de répondre à ses observations.

Le projet ayant fait l''objet de la demande de [nature] dont les références sont indiquées dans le cadre ci-dessus déposé à la mairie le [date_depot_dossier] fera l''objet d''un permis tacite (2) à défaut de réponse de l''administration [delai_dossier] mois après cette date. Les travaux (3) pourront alors être exécutés après affichage sur le terrain du présent récépissé et d''un panneau décrivant le projet conforme au modèle réglementaire.



                                                                                                              &ville , le  &datecourrier


                                                                                            Cachet de la mairie et signature du receveur



(1) le présent récepissé ne préjuge en aucune façon de la recevabilité des documents déposés.
Le défaut de production de l''ensemble des pièces manquantes dans le délai de TROIS MOIS à compter de la date de la première lettre de l''administration vous indiquant que votre dossier est incomplet, vaudra décision tacite de REJET de la demande.

(2) Permis tacite : afin de vous évitez dêtre en infraction, il vous est recommandé, dans le cas d''un permis tacite, de vous assurer auprès des services municipaux de la légalité dudit permis avant de commencer les travaux. Dans cette éventualité, il vous sera délivré sous quinzaine, une attestation certifiant qu''aucune décision de refus n''a été prise à votre encontre.
En effet, si le permis tacite était irrégulier, il serait retiré et les tribunaux pourraient vous astreindre de remettre en son état initial le projet.
Dans le délai de deux mois à compter de la date de décision tacite, il vous sera éventuellement notifié un arrêté fixant les taxes et participations applicables au projet.

(3) Certains travaux ne peuvent pas être commencés dès la délivrance de [nature] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas.

\\p
                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
Le permis peut faire l''objet d''un recours gracieux ou d''un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de deux mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délai de deux ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.

&complement1' WHERE id = 'recepisse_DPC';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

Vous avez déposé ce jour une demande de [nature]. Le délai d''instruction de votre dossier est de [delai_dossier] mois. en l''absence de courrier de la part de l''administration dans ce délai, vous bénéficierez d''un [nature] tacite(1).

* Toutefois, dans le mois qui suit le dépôt de votre dossier, l''administration peut vous écrire :
   - soit pour vous avertir qu''un autre délai est applicable, lorsque le code de l''urbanisme l''a prévu pour permettre les consultations nécessaires (si votre projet nécessite la consultation d''autres services ...)
   - soit pour vous indiquer qu''il manque une ou plusieurs pièces à votre dossier,
   - soit pour vous informer que votre projet  correspond à un des cas où un accord tacite n''est pas possible.

* Si vous recevez une telle lettre avant la fin du premier mois, celle-ci remplacera le premier récépissé.

* Si vous n''avez rien reçu à la fin du premier mois suivant le dépôt, le délai de [delai_dossier] mois ne pourra être modifié. Si aucun courrier de l''administration ne vous est parvenu à l''issu de ce délai de [delai_dossier] mois, vous pourrez commencer les travaux (2) après avoir :
   - adresser au maire, en 3 exemplaires, une déclaration d''ouverture de chantier (vous trouverez un modèle de déclaration CERFA n° 13407 01 à la mairie ou sur le site internet urbanisme du gouvernement)
   - afficher sur le terrain ce recepissé sur lequel la mairie a mis son cachet pour attester la date de dépôt ;
   - installer sur le terrain, pendant toute la durée du chantier, un panneau visible de la voie publique décrivant le projet (vous trouverez le modèle de panneau à la mairie, sur le site internet urbanisme du gouvernement, ainsi que dans la plupart des magasins de matériaux).

* ATTENTION : le permis n''est définitif qu''en l''absence de recours ou de retrait :
   - dans un délai de 2 mois à compter de l''affichage sur le terrain, sa légalité peut être contestée par un tiers. Dans ce cas l''auteur du recours est tenu de vous en informer au plus tard quinze jours après le dépôt du recours.
    - dans un délai de 3 mois après la date de [nature], l''autorité compétente peut le retirer, si elle l''estime illégal. Elle est tenue de vous en informer préalablement et de vous permettre de répondre à ses observations.
\\p
Le projet ayant fait l''objet de la demande de [nature] dont les références sont indiquées dans le cadre ci-dessus déposé à la mairie le [date_depot_dossier] fera l''objet d''un permis tacite (1) à défaut de réponse de l''administration [delai_dossier] mois après cette date. Les travaux (2) pourront alors être exécutés après affichage sur le terrain du présent récépissé et d''un panneau décrivant le projet conforme au modèle réglementaire.





                                                                                                              &ville , le  &datecourrier

                                                                                            Cachet de la mairie et signature du receveur








(1) Permis tacite : afin de vous éviter d''être en infraction, il vous est recommandé, dans le cas d''un permis tacite, de vous assurer auprès de l''administration de la légalité dudit permis avant tout commencement de travaux. Sur simple demande de votre part,le maire pourra alors vous délivrer une attestation certifiant qu''aucune décision de refus n''a été prise à votre insu. En effet, si le premis tacite était irrégulier, il serait retiré et les tribunaux pourraient vous astreindre à remettre les lieux en leur état initial.
(2) Certains travaux ne peuvent pas être commencés dès la délivrance de [nature] et doivent être différés : c''est le cas des travaux situés dans un site classé. Vous pouvez vérifier auprès de la mairie que votre projet ne rentre pas dans ce cas







                                          INFORMATIONS A LIRE ATTENTIVEMENT:

DELAIS ET VOIES DE RECOURS :
Le permis tacite peut faire l''objet d''un recours gracieux ou d''un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de deux mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme et en application du décret n°2008-1353 du 19 décembre 2008, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délais de trois ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.' WHERE id = 'recepisse_1';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

Il est donné décharge ce jour du dépôt de la demande de certificat d''urbanisme de simple information générale (article L 410-1 du code de l''urbanisme) dont les références sont indiqués dans le cadre ci-dessus.

Le délai d''instruction de cette demande de Certificat d''Urbanisme est fixé par la loi (art R.410-9a du code de l''urbanisme) a UN MOIS.

En l absence d''une notification de décision à compter du [date_limite_dossier], le présent récepissé vaudra Certificat d''Urbanisme d''information tacite(*)

&complement1


                                                                                                 &ville, le  &datecourrier
                                                                                Cachet de la mairie et signature du receveur



(*) le certificat d''urbanisme tacite d''information indique les dispositions d''urbanisme, les limitations administratives au droit de propriété et la liste des taxes et participations d''urbanisme applicables au terrain en vigueur à la date de la décision tacite.


\\p
                                          INFORMATIONS A LIRE ATTENTIVEMENT :

DUREE DE VALIDITE :
Les règles applicables sont acquises durant dix huit mois à compter de la décision tacite. Les dites règles ne peuvent être remises en cause.
Toutefois, les dispositions relatives à la préservation de la sécurité ou de la salubrite publique seront applicables, même si elles sont intervenues après la date du certificat d''urbanisme tacite.

ATTENTION : Passé ce délai, aucune garantie au maintien des règles d''urbanisme indiquées dans le certificat d''urbanisme ne vous est assurée.

PROLONGATION DE VALIDITE :
Le certificat d''urbanisme peut être prorogé par période d''une année sur demande présentée deux mois au moins avant l''expiration du délai de validité, si les prescriptions d''urbanisme, les servitudes administratives de tout ordre et le régime des taxes et participations applicables au terrain n''est pas changé.


&complement2' WHERE id = 'recepisse_CU';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

J''ai l''honneur de vous informer que  votre projet est subordonné à l''accord ou à une autorisation prévu par une autre législation ou fait l''objet d''une évocation par un ministre chargé des sites, de la protection de la nature, des monuments historiques et des espaces protégés.

En conséquence, le délai d''instruction initialement fixé par les articles R 423-23 à R 423-33 du code de l''urbanisme est ANNULE.

Le délai d''instruction de votre [nature] est donc prolongé par la loi (Art. R423-34 à R.423-37 du code de l''urbanisme) et la nouvelle date limite d''instruction est fixée au  [date_limite_dossier].


La décision doit donc vous être notifiée avant expiration de ce délai.

En application des articles R 423-44 et R 424-2 ou R 424-3 du code de l''urbanisme, le défaut de notification d''une décision à la date du [date_limite_dossier], vaudra REFUS de [nature].

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée.


                                                                                                 &ville , le  &datecourrier
                                                                                                 le Maire &delaville
                                                                                                 &titreelu



                                                                                                              &nom



NB : Copie de la présente lettre est transmise au représentant de l''Etat dans les conditions définies aux articles L2131-1 et L2131 -2 du  code des collectivités territoriales.

p

                                       INFORMATION - A LIRE ATTENTIVEMENT

DELAIS ET VOIES DE RECOURS :
Le pétitionnaire qui désire contester la décision peut saisir le tribunal administratif compétent d''un recours contentieux dans le délai de DEUX MOIS à partir de la transmission de la décision attaquée. Il peut également saisir d''un recours gracieux l''auteur de la décision hiérarchique le Ministre chargé de l''urbanisme ou le préfet pour les  permis délivrés au nom de l''Etat.
Cette démarche prolonge le délai de recours qui doit alors être introduit dans les deux mois suivant la réponse (l''absence de réponse au terme de deux mois vaut rejet implicite)' WHERE id = 'prolongation';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,
J''ai l''honneur de vous informer que  votre projet est subordonné à l''accord ou à une autorisation prévu par une autre législation ou fait l''objet d''une évocation par un ministre chargé des sites, de la protection de la nature, des monuments historiques et des espaces protégés.

En conséquence, le délai d''instruction initialement fixé par les articles R 423-23 à R 423-33 du code de l''urbanisme est MODIFIE.

Le délai d''instruction de votre [nature] est donc prolongé par la loi (Art. R423-34 à R.423-37 du code de l''urbanisme) et la nouvelle date limite d''instruction est fixée au  [date_limite_dossier].

La décision doit donc vous être notifiée avant expiration de ce délai.

En l''absence d''une notification d''une décision à la date du [date_limite_dossier] par :
-  l''architecte des bâtiments de France sur le projet indiquant qu''un permis tacite ne peut être obtenu (article R424-4 du code l''urbanisme)
- par l''autorité compétente (1)

la présente lettre vaudra [nature] tacite(2)

Dans le délai de deux mois à compter de décision tacite, l''autorité compétente prendra un arrêté fixant les participations applicables au projet.

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée.


                                                                              &ville , le
                                                                              Le Maire Adjoint de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A LA Politique Foncière






                                                                              &nom




NB : Copie de la présente lettre est transmise au représentant de l''Etat dans les conditions définies aux articles L2131-1 et L2131 -2 du  code des collectivités territoriales.

(1) le maire ou le préfet en délivre certificat sur simple demande
(2) Les travaux ne peuvent être entrepris, conformément au projet décrit dans la demande, qu''à compter de la décision tacite.

TOUTEFOIS, LE PERMIS S''IL EST ILLEGAL PEUT ETRE RETIRE PAR L''AUTORITE COMPETENTE PENDANT LE DELAI LEGAL DE RECOURS CONTENTIEUX.

Aussi, afin de vous éviter d''être en infraction, il vous est recommandé, dans le cas de [nature] tacite, de vous assurer auprès de mes services de la légalité dudit permis, avant tout commencement de travaux. Dans cette éventualité, il vous sera délivré sous quinzaine une attestation certifiant qu''aucune décision de refus n''a été prise à votre insu.
En effet, si le/la [nature] tacite était irrégulier, il/elle serait retiré(e) et les tribunaux pourraient vous astreindre de démolir la construction entreprise.
 \\p


                                                 INFORMATION-A LIRE ATTENTIVEMENT :


DELAIS ET VOIES DE RECOURS :
Le permis peut faire l''objet d''un recours gracieux ou d un recours contentieux dans un délai de deux mois à compter du premier jour d''une période continue de 2 mois d''affichage sur le terrain d''un panneau décrivant le projet et visible de la voie publique (art R 600-2 du code de l''urbanisme)
L''auteur du recours est tenu, à peine d''irrecevabilité, de notifier copie de celui-ci à l''auteur de la décision et au titulaire de l''autorisation (art R 600-1 du code de l''urbanisme)

DUREE DE VALIDITE DU PERMIS : conformément à l''article R.424-17 du code de l''urbanisme et en application du décret n°2008-1353 du 19 décembre 2008, l''autorisation est périmée si les travaux ne sont pas entrepris dans le délais de trois ans à compter de sa notification au(x) bénéficiaire(s). IL en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. En cas de recours le délai de validité du permis et suspendu jusqu''au prononcé d''une décision juridictionnelle irrévocable. L''autorisation peut être prorogée par périodes d''une année si les prescriptions d''urbanisme, les servitudes d''urbanisme de tous ordres et le régime des taxes et participations n''ont pas évolué. Vous pouvez présenter une demande de prorogation sur papier libre, accompagnée de l''autorisation pour laquelle vous demandez la prorogation, au moins deux mois avant l''expiration du délai de validité.

L''autorisation est délivrée sous réserve du DROIT DES TIERS : elle a pour objet de vérifier la conformité du projet aux règles et servitudes d''urbanisme. Elle n''a pas pour objet de vérifier que le projet respecte les autres réglementations et les règles de droit privé. Toute personne s''estimant lésée par la méconnaissance du droit de propriété ou d''autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si l''autorisation respecte les règles d''urbanisme.

Les obligations du (ou des) bénéficiaire(s) de l''autorisation : il doit souscrire l''assurance de dommages prévue par l''article L242-1 du code des assurances.

AFFICHAGE :
Le/la [nature] doit être affiché sur le terrain, visible de l''extérieur ( voie publique), à compter de la date de la décision tacite et pendant toute la durée du chantier.' WHERE id = 'prolongationnontacite';
UPDATE om_lettretype SET titre = '[libelle_datd]
[libelle_om_lettretype]
______________________________________________________________________
Dossier numéro         [libelle_dossier]  déposé le [date_depot_dossier]
par                              [nom_petitionnaire_principal]

Correspondant :        [nom_correspondant]
                                    [numero_correspondant] [voie_correspondant]
                                    [complement_correspondant] [lieu_dit_correspondant] [bp_correspondant]
                                    [code_postal_correspondant] [ville_correspondant] [cedex_correspondant] [pays_correspondant]

sur le terrain              [terrain_adresse_vois_numero_dossier] [terrain_adresse_voie_dossier] [terrain_adresse_lieu_dit_dossier] [terrain_adresse_bp_dossier] [terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]
arrondissement          [libelle_arrondissement]
______________________________________________________________________
Dossier suivi par  [nom_instructeur] - [telephone_instructeur] - [division_instructeur] - [email_instructeur]', corps = 'Madame, Monsieur,

Vous avez déposé le [date_depot_dossier] une demande de [nature].

Lors de ce dépôt, le récépissé de votre dossier indiquait qu''en cas de silence de l''Administration à la fin du délai d''instruction de droit commun de [delai_dossier] mois, vous bénéficierez d''un [nature] tacite.

Je vous informe que mes services ne peuvent entreprendre l''instruction de votre demande de [nature], enregistrée sous les références portées dans le cadre ci dessus, car elle est INCOMPLETE.

Il convient donc que vous me fassiez parvenir les pièces suivantes :
 &complement

Le délai d''instruction notifié ci-dessus commencera à courir à compter de la réception  de l''intégralité des pièces et informations manquantes en mairie d''Arles Service Gestion du Territoire - 5, rue du cloître.

Vous disposez de TROIS MOIS  à compter de la réception de la présente lettre, pour faire parvenir à la mairie d''Arles , service Gestion du Territoire - 5, rue du Cloître, l''intégralité des pièces et informations manquantes .

Le défaut de production de l''ensemble des pièces et informations manquantes dans ce délai de TROIS MOIS, vaudra  DECISION TACITE DE REJET de votre dossier de [nature]  (art.423-39b du code de l''urbanisme).
\\p
J''appelle également votre attention sur le fait que votre délai d''instruction pourrait faire l''objet d''une prolongation exceptionnelle conformément aux articles R 423-34 à R 423-37 du code de l''Urbanisme.

Si cette situation se présente sur votre dossier, vous serez informé par courrier avant l''expiration du délai d''instruction fixé ci-dessus.

Je vous prie d''agréer, Madame, Monsieur, l''expression de ma considération distinguée







                                                                              &ville , le
                                                                              Le Maire de la ville d''&ville
                                                                              Délégué à l''Urbanisme, l''Habitat
                                                                              A la Politique Foncière






                                                                              &nom




Nb : copie de la présente lettre est transmise au représentant de l''Etat.
\\p                                          INFORMATION - A LIRE ATTENTIVEMENT



DELAIS ET VOIES DE RECOURS :
le pétitionnaire qui désire contester la décision peut saisir le tribunal administratif compétent d''un recours contentieux dans un délai de DEUX MOIS à partir de la transmission de la décision attaquée. Il peut également saisir d''un recours gracieux l''auteur de la décision hiérarchique, le Ministre chargé de l''Urbanisme ou le préfet pour les [nature] délivrés au nom de l''état.

Cette démarche prolonge le délai de recours qui doit alors être introduit dans les deux mois suivant la réponse (l''absence de réponse au terme de 2 mois vaut rejet implicite)' WHERE id = 'piececomplementaire_sans_majoration';
ALTER TABLE rapport_instruction ALTER COLUMN dossier_instruction TYPE character varying(30);

---
--- Ajout du champ duree_validite à la table dossier
---
ALTER TABLE dossier ADD COLUMN duree_validite integer DEFAULT 0 NOT NULL;

---
--- Ajout du champ duree_validite et duree_validite_parametrage à la table instruction
---
ALTER TABLE instruction ADD COLUMN duree_validite_parametrage integer DEFAULT 0 NOT NULL;
ALTER TABLE instruction ADD COLUMN duree_validite integer DEFAULT 0 NOT NULL;

---
---
---
INSERT INTO om_parametre VALUES (nextval('om_parametre_seq'), 'champ_remplacement_lettretype', '&destinataire;&aujourdhui;&complement;&complement2;&datecourrier;#&complement#;#&complement2#;#&complement3#;#&complement4#;#&complement5#;#&complement6#;#&complement7#;#&complement8#;#&complement9#;#&complement10#;#&complement11#;#&complement12#;#&complement13#;#&complement14#;#&complement15#;', 1);
INSERT INTO om_parametre VALUES (nextval('om_parametre_seq'), 'champ_remplacement_etat', '&datedebut;&datefin;&date_bordereau;&nature;&rapport_instruction_consultation;&date_bordereau;&aujourdhui;&jourSemaine;&jourSemaine;', 1);
