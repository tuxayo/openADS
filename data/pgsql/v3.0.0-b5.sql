--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.0.0-b5
--
-- @package openfoncier
-- @version SVN : $Id: v3.0.0-b5.sql 1804 2013-04-30 14:45:17Z fmichon $
--------------------------------------------------------------------------------

-- version atreal - greasque

-- schema
-- SET search_path = openfoncier, pg_catalog;

-- complement travaux
alter table dossier add travaux_complement character varying(100);

-- mise à jour des civilites
insert into civilite(civilite) values ('M.');
insert into civilite(civilite) values ('Mlle');
update dossier set demandeur_civilite='M.' where demandeur_civilite='Mr';
update dossier set demandeur_civilite='Mlle' where demandeur_civilite='Melle';
update dossier set delegataire_civilite='M.' where delegataire_civilite='Mr';
update dossier set delegataire_civilite='Mlle' where delegataire_civilite='Melle';
update proprietaire set civilite='M.' where civilite = 'Mr';
update proprietaire set civilite='Mlle' where civilite = 'Melle';
delete from civilite where civilite='Mr';
delete from civilite where civilite='Melle';


-- version
update om_version set om_version='3.0.0-b5';

-- ajout de complement d'adresses dans le dossier
alter table dossier add demandeur_adresse_complement character varying(39) NOT NULL default '';
alter table dossier add delegataire_adresse_complement character varying(39) NOT NULL default '';

-- table service
alter table service add adresse2 character varying(39) NOT NULL default '';

