--
-- START / Les délais de consultation peuvent être en jour
--

-- Ajout du champ delai_type dans la table service
ALTER TABLE service ADD COLUMN delai_type character varying(100) DEFAULT 'mois';
COMMENT ON COLUMN service.delai_type IS 'Type de délai (mois ou jour)';

-- Modification de la requête consultation
UPDATE om_requete
SET requete = '
SELECT 
    --Coordonnées du service
    service.libelle as libelle_service, 
    service.adresse as adresse_service, 
    service.adresse2 as adresse2_service, 
    service.cp as cp_service, 
    service.ville as ville_service,
    service.delai as delai_service,
    CASE WHEN LOWER(service.delai_type) = LOWER(''jour'')
        THEN ''jour(s)''
        ELSE ''mois''
    END as delai_type_service,

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    dossier.dossier as code_barres_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    dossier_autorisation_type_detaille.libelle as libelle_datd, 

    --Adresse du terrain du dossier d''instruction
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,

    --Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as tel_instructeur,

    --Coordonnées du demandeur
    civilite.libelle as civilite_demandeur,
    CASE WHEN demandeur.qualite=''particulier''
        THEN TRIM(CONCAT(demandeur.particulier_nom, '' '', demandeur.particulier_prenom))
        ELSE TRIM(CONCAT(demandeur.personne_morale_raison_sociale, '' '', demandeur.personne_morale_denomination))
    END as nom_demandeur,
    CONCAT(demandeur.numero, '' '', demandeur.voie) as adresse_demandeur,
    demandeur.code_postal as code_postal_demandeur,
    demandeur.localite as ville_demandeur,
    demandeur.personne_morale_denomination as societe_demandeur,

    --Dates importantes du dossier d''instruction
    to_char(dossier.date_depot,''DD/MM/YYYY'') as date_depot_dossier, 
    to_char(dossier.date_rejet,''DD/MM/YYYY'') as date_rejet_dossier, 
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier, 
    to_char(consultation.date_envoi,''DD/MM/YYYY'') as date_envoi_dossier, 
    to_char(instruction.date_evenement,''DD/MM/YYYY'') as date_evenement,
    dossier.delai as delai_limite_decision,

    --Code barres de la consultation
    consultation.code_barres as code_barres_consultation,

    --Données générales des données techniques
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    REGEXP_REPLACE(CONCAT(
        CASE
            WHEN donnees_techniques.su_cstr_shon1 IS NULL
            THEN ''''
            ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon2 IS NULL
            THEN ''''
            ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon3 IS NULL
            THEN ''''
            ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon4 IS NULL
            THEN ''''
            ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon5 IS NULL
            THEN ''''
            ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon6 IS NULL
            THEN ''''
            ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon7 IS NULL
            THEN ''''
            ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon8 IS NULL
            THEN ''''
            ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
        END, 
        CASE
            WHEN donnees_techniques.su_cstr_shon9 IS NULL
            THEN ''''
            ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
        END
    ), '' / $'', '''') as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques
    
FROM 


    &DB_PREFIXEconsultation 
    LEFT JOIN &DB_PREFIXEservice 
        ON service.service=consultation.service 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=consultation.dossier 
    LEFT JOIN &DB_PREFIXEdonnees_techniques 
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdossier_autorisation 
        ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation 
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur
    LEFT JOIN &DB_PREFIXEinstruction
        ON dossier.dossier=instruction.dossier 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
        
         
WHERE 


    consultation.consultation = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
',
merge_fields = '
--Coordonnées du service
[libelle_service] 
[adresse_service] 
[adresse2_service] 
[cp_service]    [ville_service]
[delai_service]
[delai_type_service]

--Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]    [terrain_references_cadastrales_dossier]
[libelle_datd] 

--Adresse du terrain du dossier d''instruction
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier

--Coordonnées de l''instructeur
[nom_instructeur]
[tel_instructeur]

--Coordonnées du demandeur
[civilite_demandeur]    [nom_demandeur]
[adresse_demandeur]
[code_postal_demandeur]    [ville_demandeur]
[societe_demandeur]

--Dates importantes du dossier d''instruction
[date_depot_dossier] 
[date_rejet_dossier] 
[date_limite_dossier] 
[date_envoi_dossier] 
[date_evenement]
[delai_limite_decision]

--Code barres de la consultation
[code_barres_consultation]

--Données générales des données techniques
[projet_desc_donnees_techniques]
[co_tot_log_nb_donnees_techniques]    [co_station_place_nb_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]
'
WHERE code = 'consultation';

-- consultation_MAIRE_papier
UPDATE om_etat
SET corps_om_htmletatex = '
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>[libelle_datd]</span><br /><span style=''text-decoration: underline;''><span style=''font-family: helvetica; font-size: 10pt;''>pour Avis demandé                                                                                                                                                            </span></span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Objet</span> : Demande d''avis sur DOSSIER N° : <span class=''mce_maj'' style=''font-weight: bold;''>[libelle_dossier]</span></span></p>
<p> </p>
<p><span style=''font-size: 10pt;''><span style=''font-family: helvetica;''><span style=''text-decoration: underline;''>P.J</span> : 1 dossier <span style=''font-weight: bold;''>(<span class=''mce_maj''>à retourner, accompagné du présent document</span> <span class=''mce_min''>dans un délai de [delai_service] [delai_type_service])</span><span class=''mce_maj''><br /></span></span></span><span style=''font-family: helvetica;''><span class=''mce_maj''><br /></span></span></span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>Monsieur,</span></p>
<p> </p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>J''ai<span class=''mce_min''> l''honneur de vous faire connaître que la demande citée en objet a été déposée dans notre service.</span></span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>Je<span class=''mce_min''> vous prie de bien vouloir me faire parvenir votre avis sur ce dossier dans les meilleurs délais.</span></span></p>
<p> </p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>D<span class=''mce_min''>ate<span style=''font-weight: bold;''> limite de réponse : [delai_service] [delai_type_service]</span> à dater de la réception de ce courrier.</span></span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''> </span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>V<span class=''mce_min''>euillez agréer, Monsieur, l''assurance de mes sentiments distingués.</span></span></p>
<p style=''text-align: justify;''> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Votre<span class=''mce_min''> avis sera adressé à  </span><span style=''font-weight: bold;''><span class=''mce_maj''>Ville &delaville -</span> <span class=''mce_min''>&amp;adresse_direction_urbanisme</span></span></span></p>
<hr />
<p><span style=''font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Date de réception par le service <span class=''mce_maj''><span style=''font-weight: bold;''>[libelle_service]</span> </span>: ..........................<br /></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Signature :</span></p>
<p style=''text-align: justify;''><span class=''mce_min'' style=''font-size: 10pt; font-family: helvetica;''><br /></span><span class=''mce_min''><span style=''font-size: 9px;''><span style=''font-family: helvetica;''><br /></span></span></span></p>
<hr />
<p><span style=''font-size: 10pt;''> <br pagebreak=''true'' /><br /></span></p>
<table style=''width: 100%;'' border=''1'' cellpadding=''5''>
<tbody>
<tr>
<td style=''text-align: center;''>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 12pt; font-weight: bold;''>[libelle_service]</span></p>
<p><span style=''font-size: 10pt;''> </span></p>
</td>
</tr>
<tr>
<td style=''text-align: center;''>
<p><span style=''text-decoration: underline; font-weight: bold; font-size: 12pt;''><span class=''mce_maj'' style=''font-family: helvetica;''>avis</span></span></p>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>dossier de [libelle_datd]</span></p>
</td>
</tr>
<tr>
<td>
<p style=''text-align: center;''><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>dossier n° [libelle_dossier]</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Nature des travaux</span> : <span class=''field_value pre''>[co_projet_desc_donnees_techniques]</span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Transmis le : <span class=''field_value pre''>[date_envoi_dossier] </span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>par : <span class=''field_value pre''>[nom_instructeur]</span></span></p>
<p><span class=''field_value pre'' style=''font-family: helvetica; font-size: 10pt;''>Tel : <span class=''field_value pre''>[tel_instructeur]</span></span></p>
</td>
</tr>
<tr>
<td>
<p><span style=''font-family: helvetica; font-size: 8pt;''> </span></p>
<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 18pt; font-family: helvetica; background-color: #c0c0c0;''>AVIS</span></p>
<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 10pt;''>favorable                                défavorable</span></p>
<p style=''text-align: center;''><span class=''mce_min''><em><span style=''font-size: 8pt;''>(rayer la mention inutile)</span></em></span></p>
<p style=''text-align: center;''><span style=''font-size: 8pt;''><span style=''font-family: helvetica;''>est donné au projet de : </span><span style=''font-family: helvetica;''><span class=''field_value pre mce_maj''>[co_projet_desc_donnees_techniques]</span></span><span style=''font-family: helvetica;''><br /></span></span></p>
</td>
</tr>
<tr>
<td>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Observations</span> :</span></p>
<p><span style=''font-size: 10pt;''> </span></p>
<p><span style=''font-size: 10pt;''>  </span></p>
<p><span style=''font-size: 10pt;''> </span></p>
</td>
</tr>
<tr>
<td>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Votre avis sera adressé à <span class=''field_value pre''><span class=''mce_maj''>Ville &delaville</span> - &amp;adresse_direction_urbanisme</span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>&ville le .........................................                            <span style=''font-family: helvetica;''>Signature :</span><br /></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
</td>
</tr>
<tr>
<td>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>Dossier de [libelle_datd]</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Numéro :       [libelle_dossier]    </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Demandeur</span> : [civilite_demandeur] <span class=''mce_maj''>[nom_demandeur]</span></span> - <span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>[adresse_demandeur] - [code_postal_demandeur] [ville_demandeur] [societe_demandeur]</span><br /><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Adresse des  travaux</span> :</span><br /><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>[terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]</span><br /><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>[terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier]</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Nature des travaux</span> : [co_projet_desc_donnees_techniques]</span><br /><span style=''font-family: helvetica; font-size: 10pt;''>PLU : &amp;contraintes(liste_groupe=ZONES DU PLU)</span><br /><span style=''font-family: helvetica; font-size: 10pt;''>Destination/Surface de plancher en m² : [tab_surface_donnees_techniques]</span></p>
</td>
</tr>
<tr>
<td>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>P.J</span> :  <span class=''mce_maj''>1 dossier <span style=''font-weight: bold;''>(à retourner, accompagné du présent document)</span></span></span></p>
</td>
</tr>
</tbody>
</table>
'
WHERE id = 'consultation_MAIRE_papier';

-- consultation_CRU_avis_attendu
UPDATE om_etat
SET corps_om_htmletatex = '
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Objet</span> : Demande d''avis sur DOSSIER N° : <span class=''mce_maj'' style=''font-weight: bold;''>[libelle_dossier]</span></span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>P.J</span> : 1 dossier <span class=''mce_maj''>(</span><span class=''mce_maj'' style=''font-weight: bold;''>à retourner, accompagné du présent document</span><span class=''mce_min''> DANS UN DéLAI DE [delai_service] [delai_type_service]</span><span class=''mce_maj''>)</span></span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Date limite de<span style=''font-weight: bold;''> réponse : [delai_service]</span> [delai_type_service] à dater de la réception de ce courrier.</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Veuillez nous faire parvenir vos observations sur ce projet dans le délai visé ci-dessus.</span></p>
<p> </p>
<table border=''1''>
<tbody>
<tr>
<td colspan=''2''>
<p style=''text-align: center;''><span style=''font-size: 12pt;''><span style=''font-family: helvetica;''>Avis de :</span> <span class=''mce_maj'' style=''font-weight: bold; font-family: helvetica;''>[libelle_service]</span></span></p>
</td>
</tr>
<tr>
<td>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 12pt; font-family: helvetica;''>favorable               défavorable</span></p>
<p style=''text-align: center;''><em><span style=''font-size: 8pt;''>(rayer la mention inutile)</span></em></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>est donné au projet de : <span class=''mce_maj''>[co_projet_desc_donnees_techniques]</span></span></p>
</td>
<td rowspan=''2''>
<p><span class=''mce_maj'' style=''font-size: 10pt;''><span style=''text-decoration: underline;''>Observations éventuelles</span> :</span></p>
</td>
</tr>
<tr>
<td>
<p> </p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>Date : .................................</span></p>
<p><span style=''font-size: 10pt; font-family: helvetica; font-weight: bold;''>Pour le Maire, l''Adjoint Délégué </span></p>
<p> </p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span style=''font-size: 10pt; font-family: helvetica; font-weight: bold;''>Julien RUAS</span></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span class=''mce_maj'' style=''text-decoration: underline; font-weight: bold;''>Observations</span> :</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Le terrain du projet est intéressé par le(s) risque(s) suivant(s) :</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>&amp;contraintes(liste_groupe=servitudes d''utilites publiques applicables;liste_ssgroupe=risques)</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>&amp;contraintes(liste_groupe=autres servitudes;liste_ssgroupe=risques)</span></p>
'
WHERE id = 'consultation_CRU_avis_attendu';

-- consultation_MAIRE_CS
UPDATE om_etat
SET corps_om_htmletatex = '
<p><span style=''font-family: helvetica; font-size: 10pt;''>Demande d''avis sur DOSSIER N° : <span style=''font-weight: bold;''>[libelle_dossier]</span></span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>P.J : 1 dossier<span style=''font-weight: bold;''> (à retourner, accompagné du présent document dans un délai de [delai_service] [delai_type_service])</span></span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Madame, Monsieur,</span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>J''ai l''honneur de vous faire connaître que la demande citée en objet a été déposée dans notre service.</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Je vous prie de bien vouloir me faire parvenir votre avis sur ce dossier dans les meilleurs délais.</span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Date <span style=''font-weight: bold;''>limite de réponse : [delai_service]</span> [delai_type_service] à dater de la réception de ce courrier.</span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Veuillez agréer, Madame, Monsieur, l''assurance de mes sentiments distingués.</span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Votre avis sera adressé à la<span class=''mce_maj'' style=''font-weight: bold;''> direction de l''aménagement et de l''habitat - &amp;adresse_SAH</span><br /></span></p>
<hr />
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Date de réception par le service <span style=''font-weight: bold;''><span class=''mce_maj''>[adresse_service]</span> : ..........................</span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Signature :</span></p>
<p> </p>
<p> </p>
<p> </p>
<p style=''text-align: center;''><span style=''font-size: 14pt; font-weight: bold;''><em><span class=''mce_maj'' style=''font-family: helvetica;''><span class=''mce_maj''><span style=''font-size: 12pt;''><br /></span></span></span></em></span><span style=''font-family: helvetica; font-size: 10pt;''><br pagebreak=''true'' /></span></p>
<table style=''width: 93%; margin-left: auto; margin-right: auto;'' border=''1'' cellpadding=''3''>
<tbody>
<tr>
<td style=''text-align: center;''>
<p><span class=''mce_maj'' style=''font-size: 12pt; font-weight: bold; font-family: helvetica;''>[adresse_service]</span></p>
</td>
</tr>
<tr>
<td style=''text-align: center;''>
<p><span style=''font-family: helvetica; font-size: 12pt;''><span class=''mce_maj''>avis</span></span></p>
<p><span style=''font-weight: bold;''> <span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>dossier de [libelle_datd]</span></span></p>
</td>
</tr>
<tr>
<td style=''text-align: center;''>
<p><span class=''mce_maj'' style=''font-size: 8pt; font-family: helvetica;''>dossier n° [libelle_dossier]</span></p>
<p><span class=''mce_maj mce_codebarre'' style=''font-size: 8pt; font-family: helvetica;''>    [code_barres_consultation]</span></p>
<p><span class=''mce_min'' style=''font-family: helvetica; font-size: 8pt;''> Transmis le : [date_envoi_dossier]</span></p>
<p><span class=''mce_min'' style=''font-family: helvetica; font-size: 8pt;''>par : </span><span class=''mce_maj'' style=''font-family: helvetica; font-size: 8pt;''>[nom_instructeur]</span></p>
<p><span class=''mce_min'' style=''font-family: helvetica; font-size: 8pt;''>Tel : [tel_instructeur]</span></p>
</td>
</tr>
<tr>
<td style=''text-align: center;''>
<p><span style=''font-family: helvetica;''>  <br /></span></p>
<p><span style=''background-color: #c0c0c0; font-weight: bold; font-size: 18pt; font-family: helvetica;''>AVIS</span></p>
<p><span style=''font-family: helvetica;''> </span></p>
<p><span class=''mce_maj'' style=''font-size: 10pt; font-family: helvetica;''>favorable                                                                           défavorable</span></p>
<p><span style=''font-family: helvetica; font-size: 8pt;''>(rayer la mention inutile)</span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span class=''mce_min''>est donné au projet de :</span><span class=''mce_maj''>  [ope_proj_desc_donnees_techniques][co_projet_desc_donnees_techniques]<br /></span></span></p>
</td>
</tr>
<tr>
<td style=''text-align: center;''>
<p style=''text-align: left;''><span class=''mce_maj'' style=''text-decoration: underline; font-family: helvetica; font-size: 10pt;''>Observations</span><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''> :</span></p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p style=''text-align: left;''><span style=''font-family: helvetica; font-size: 10pt;''>&ville le ......................................... Signature : </span></p>
<p> <span style=''font-family: helvetica;''> <br /></span></p>
</td>
</tr>
<tr>
<td style=''text-align: center;''>
<p style=''text-align: left;''><span style=''font-size: 10pt;''><span class=''mce_maj'' style=''background-color: #c0c0c0;''><span style=''font-family: helvetica; background-color: #ffffff;''>V</span><span class=''mce_min'' style=''font-family: helvetica;''><span style=''background-color: #ffffff;''>otre avis sera adressé à</span> la </span></span><span class=''mce_maj'' style=''font-family: helvetica;''><span style=''background-color: #c0c0c0;''>DIRECTION DE L’AMÉNAGEMENT ET DE L''HABITAT - &amp;adresse_SAH</span></span></span></p>
</td>
</tr>
<tr>
<td style=''text-align: center;''>
<p style=''text-align: left;''><span class=''mce_maj'' style=''font-family: helvetica; font-size: 8pt;''><span style=''font-weight: bold;''><span class=''mce_maj''>Dossier de [libelle_datd] </span><span class=''mce_min''>: </span> [libelle_dossier]</span></span></p>
<p style=''text-align: left;''><span style=''font-family: helvetica; font-size: 8pt;''><span style=''text-decoration: underline;''>Objet de la demande</span> :<span class=''mce_maj'' style=''font-weight: bold;''> [ope_proj_desc_donnees_techniques] [co_projet_desc_donnees_techniques]<br /></span></span></p>
<p style=''text-align: left;''><span style=''font-family: helvetica; font-size: 8pt;''><span style=''text-decoration: underline;''>Demandeur</span> :</span></p>
<p style=''text-align: left;''><span class=''mce_maj'' style=''font-family: helvetica; font-size: 8pt; font-weight: bold;''>[civilite_demandeur] [nom_demandeur]</span><br /><span class=''mce_maj'' style=''font-family: helvetica; font-size: 8pt; font-weight: bold;''>[adresse_demandeur]</span></p>
<p style=''text-align: left;''><span class=''mce_maj'' style=''font-family: helvetica; font-size: 8pt; font-weight: bold;''>[code_postal_demandeur] [ville_demandeur] [societe_demandeur]</span><br /><span style=''font-family: helvetica; font-size: 8pt;''><span style=''text-decoration: underline;''>Adresse des travaux</span> :</span><br /><span class=''mce_maj'' style=''font-family: helvetica; font-size: 8pt; font-weight: bold;''>[terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]</span><br /><span class=''mce_maj'' style=''font-family: helvetica; font-size: 8pt; font-weight: bold;''>[terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier]</span></p>
</td>
</tr>
<tr>
<td style=''text-align: left;''><span class=''mce_maj'' style=''font-family: helvetica; font-size: 8pt;''>P.J : 1 dossier (<span style=''font-weight: bold;''>à retourner, accompagné du présent document</span>)</span></td>
</tr>
</tbody>
</table>
'
WHERE id = 'consultation_MAIRE_CS';

-- consultation_pour_conformite
UPDATE om_etat
SET corps_om_htmletatex = '
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>[libelle_datd]</span><br /><span style=''text-decoration: underline;''><span style=''font-family: helvetica; font-size: 10pt;''>pour Avis demandé - pour conformité                                                                                                                                    </span></span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Objet</span> : Demande d''avis sur DOSSIER N° : <span style=''font-weight: bold;''>[libelle_dossier]</span></span></p>
<p><br /><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>P.J</span> : 1 dossier (<span class=''mce_maj'' style=''font-weight: bold;''>à retourner, accompagné du présent document</span> <span class=''mce_min''>DANS UN DéLAI DE [delai_service]</span> [delai_type_service])</span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Date limite de <span style=''font-weight: bold;''>réponse : [delai_service]</span> [delai_type_service] à dater de la réception de ce courrier.</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Date limite d''instruction du dossier : [date_limite_dossier].</span></p>
<p> </p>
<p> </p>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''><span style=''font-weight: bold;''>Observations</span></span> :</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Veuillez nous faire parvenir vos observations sur ce projet dans le délai visé ci-dessus.</span></p>
<p> </p>
<table border=''1'' cellpadding=''5''>
<tbody>
<tr>
<td style=''text-align: center;'' colspan=''2''><span style=''font-size: 12pt; font-family: helvetica;''>Avis de<span class=''mce_maj'' style=''font-weight: bold;''> [libelle_service]</span></span></td>
</tr>
<tr>
<td>
<p style=''text-align: center;''><span style=''font-family: helvetica;''> </span></p>
<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 12pt; font-family: helvetica;''>favorable               défavorable</span></p>
<p style=''margin-bottom: 0cm; text-align: center;''><span style=''font-size: xx-small; font-family: helvetica;''><em><span style=''font-size: xx-small;''>(rayer la mention inutile)</span></em></span></p>
<br />
<p style=''text-align: center;''><span style=''font-family: helvetica;''><span style=''font-size: 10pt;''>est donné au projet de : <span class=''mce_maj''><span class=''field_value pre mce_min''><span class=''field_value pre''><span class=''field_value pre''><span class=''field_value pre''><span class=''field_value pre''><span class=''field_value pre''><span class=''field_value pre''> [co_projet_desc_donnees_techniques] </span></span></span></span></span></span></span></span></span></span></p>
<p style=''text-align: center;''><span style=''font-family: helvetica;''>     </span></p>
</td>
<td rowspan=''2''>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span class=''mce_maj'' style=''text-decoration: underline;''>Observations éventuelles</span> :</span><br /><br /></p>
<p><span style=''font-family: helvetica;''> </span></p>
<p><span style=''font-family: helvetica;''> </span></p>
<p><span style=''font-family: helvetica;''> </span></p>
<p><span style=''font-family: helvetica;''> </span></p>
<p><span style=''font-family: helvetica;''> </span></p>
<p><span style=''font-family: helvetica;''> </span></p>
<p><span style=''font-family: helvetica;''> </span></p>
<p><span style=''font-family: helvetica;''> </span></p>
</td>
</tr>
<tr>
<td>
<p><span style=''font-family: helvetica;''> </span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>Date de réception par le service : .................................</span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>Signature :</span></p>
<p><span style=''font-family: helvetica;''> </span></p>
<p><span style=''font-family: helvetica;''> </span></p>
</td>
</tr>
</tbody>
</table>
'
WHERE id = 'consultation_pour_conformite';

-- consultation_ABF_papier
UPDATE om_etat
SET corps_om_htmletatex = '
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Objet</span> : Demande d''avis sur DOSSIER N° : <span class=''mce_maj'' style=''font-weight: bold;''>[libelle_dossier]</span></span></p>
<p> </p>
<p><span style=''font-size: 10pt;''><span style=''font-family: helvetica;''><span style=''text-decoration: underline;''>P.J</span> : 1 dossier <span style=''font-weight: bold;''>(<span class=''mce_maj''>à retourner, accompagné du présent document</span> <span class=''mce_min''>dans un délai de [delai_service] [delai_type_service])</span><span class=''mce_maj''><br /></span></span></span><span style=''font-family: helvetica;''><span class=''mce_maj''><br /></span></span></span><span style=''font-size: 10pt; font-family: helvetica;''><span class=''mce_maj''>m</span>adame, Monsieur,</span></p>
<p> </p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>J''ai<span class=''mce_min''> l''honneur de vous faire connaître que la demande citée en objet a été déposée dans notre service.</span></span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>Je<span class=''mce_min''> vous prie de bien vouloir me faire parvenir votre avis sur ce dossier dans les meilleurs délais.</span></span></p>
<p> </p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>D<span class=''mce_min''>ate<span style=''font-weight: bold;''> limite de réponse : [delai_service] [delai_type_service]</span> à dater de la réception de ce courrier.</span></span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''> </span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>V<span class=''mce_min''>euillez agréer, </span><span class=''mce_maj''>m<span class=''mce_min''>adame, </span>M<span class=''mce_min''>onsieur, l''expression de mes sincères salutations.</span></span></span></p>
<p style=''text-align: justify;''> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Votre<span class=''mce_min''> avis sera adressé à  </span><span style=''font-weight: bold;''><span class=''mce_maj''>Ville &delaville - </span>&amp;adresse_direction_urbanisme</span></span></p>
<hr />
<p><span style=''font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Date de réception par le service <span class=''mce_maj''><span style=''font-weight: bold;''>[libelle_service]</span> </span>: ..........................<br /></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Signature :</span></p>
<p style=''text-align: justify;''><span class=''mce_min''><span style=''font-size: 9px;''><span style=''font-family: helvetica;''> </span></span></span></p>
<p style=''text-align: justify;''> </p>
<hr />
<p> <br pagebreak=''true'' /></p>
<table style=''width: 100%;'' border=''1'' cellpadding=''5''>
<tbody>
<tr>
<td style=''text-align: center;''>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 12pt; font-weight: bold;''>[libelle_service]</span></p>
</td>
</tr>
<tr>
<td style=''text-align: center;''>
<p><span style=''text-decoration: underline; font-weight: bold; font-size: 12pt;''><span class=''mce_maj'' style=''font-family: helvetica;''>avis</span></span></p>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>dossier de [libelle_datd]</span></p>
</td>
</tr>
<tr>
<td>
<p style=''text-align: center;''><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>dossier n° [libelle_dossier]</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Nature des travaux</span> : <span class=''field_value pre''>[co_projet_desc_donnees_techniques]</span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Transmis le : <span class=''field_value pre''>[date_envoi_dossier] </span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>par : <span class=''field_value pre''>[nom_instructeur]</span></span></p>
<p><span class=''field_value pre'' style=''font-family: helvetica; font-size: 10pt;''>Tel : <span class=''field_value pre''>[telephone_instructeur]</span></span></p>
</td>
</tr>
<tr>
<td>
<p><span class=''mce_min'' style=''background-color: #ffffff; font-family: helvetica; font-size: 8pt;''>&amp;contraintes(liste_groupe=Servitudes d''utilites publiques applicables,liste_ssgroupe=historique)</span></p>
</td>
</tr>
<tr>
<td>
<p style=''text-align: left;''><span class=''mce_min'' style=''background-color: #ffffff; font-size: 8pt; font-family: helvetica;''>&amp;contraintes(liste_groupe=Servitudes d''utilites publiques applicables,liste_ssgroupe=risques,naturelle,autres)</span></p>
</td>
</tr>
<tr>
<td>
<p style=''font-weight: normal; text-decoration: none;''><span style=''font-family: helvetica;''><span style=''font-size: xx-small;''><strong><span style=''text-decoration: none;''><span style=''font-weight: normal;''>Vu la Loi du 31 Décembre 1913 sur les Monuments Historiques, modifiée et complétée par la Loi du 25 Février 1943 et notamment les articles 13 bis et 13 ter;</span></span></strong></span><span style=''font-size: xx-small;''><br />Vu les pièces du dossier présenté par le pétitionnaire sous-indiqué.</span></span></p>
<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 14pt; font-family: helvetica; background-color: #ffffff;''>AVIS</span></p>
<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 10pt;''>favorable                                défavorable</span></p>
<p style=''text-align: center;''><span class=''mce_min''><em><span style=''font-size: 8pt;''>(rayer la mention inutile)</span></em></span></p>
<p style=''text-align: center;''><span style=''font-family: helvetica; font-size: 8pt;''><span style=''font-size: 10pt;''>est donné au projet de : </span><span class=''field_value pre mce_maj''><span style=''font-size: 10pt;''>[co_projet_desc_donnees_techniques]</span> </span></span></p>
</td>
</tr>
<tr>
<td>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Observations</span> :</span></p>
<p> </p>
<p> </p>
<p> </p>
</td>
</tr>
<tr>
<td>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Votre avis sera adressé à <span style=''font-weight: bold;''><span class=''mce_maj''>Ville &delaville</span> - &amp;adresse_direction_urbanisme</span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>&ville le .........................................                            <span style=''font-family: helvetica;''>Signature :</span><br /></span></p>
<p> </p>
</td>
</tr>
<tr>
<td>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>Dossier de [libelle_datd]</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Numéro :       [libelle_dossier]    </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Demandeur</span> : [civilite_demandeur] <span class=''mce_maj''>[nom_demandeur]</span></span> - <span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>[adresse_demandeur] - [code_postal_demandeur] [ville_demandeur] [societe_demandeur]</span><br /><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Adresse des  travaux</span> :</span><br /><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>[terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]</span><br /><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>[terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier]</span></p>
<p style=''text-align: left;''><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Nature des travaux</span> : <span style=''font-family: helvetica; font-size: 10pt;''><span class=''field_value pre''>[co_projet_desc_donnees_techniques]</span></span></span><br /><span style=''font-family: helvetica; font-size: 8pt;''>Secteur P.L.U : &amp;contraintes(liste_groupe=Zones Du Plu)</span></p>
<p style=''text-align: left;''><span style=''font-family: helvetica; font-size: 8pt;''>Destination/Surface de plancher en m² : [tab_surface_donnees_techniques]</span></p>
</td>
</tr>
<tr>
<td>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>P.J</span> :  <span class=''mce_maj''>1 dossier <span style=''font-weight: bold;''>(à retourner, accompagné du présent document)</span></span></span></p>
</td>
</tr>
</tbody>
</table>
'
WHERE id = 'consultation_ABF_papier';

-- consultation_ERP_papier
UPDATE om_etat
SET corps_om_htmletatex = '
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Objet</span> : Demande d''avis sur DOSSIER N° : <span class=''mce_maj'' style=''font-weight: bold;''>[libelle_dossier]</span></span></p>
<table style=''width: 92%; margin-left: auto; margin-right: auto;'' border=''1''>
<tbody>
<tr>
<td>
<p><span style=''font-family: helvetica; font-size: 8pt;''>&amp;contraintes(liste_groupe=Zones Du Plu)</span></p>
</td>
<td style=''width: 60%;''>
<p><span style=''font-family: helvetica; font-size: 8pt;''>Cadastre :</span></p>
<p><span style=''font-family: helvetica; font-size: 8pt;''>[terrain_references_cadastrales_dossier]</span></p>
</td>
</tr>
</tbody>
</table>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''font-family: helvetica;''><span style=''text-decoration: underline; font-weight: bold;''>P.J</span>: 1 dossier <span style=''font-weight: bold;''>(<span class=''mce_maj''>à retourner, accompagné du présent document</span> <span class=''mce_min''>dans un délai de [delai_service] [delai_type_service])</span><span class=''mce_maj''><br /></span></span></span>Date limite de <span style=''font-weight: bold;''>réponse : [delai_service] [delai_type_service]</span> à dater de la réception de ce courrier.</span></p>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 8pt;''><strong><span style=''font-weight: bold;''>Le terrain du projet est intéressé par le(s) RISQUE(S) suivants :</span> </strong></span></p>
<p><span style=''font-family: helvetica; font-size: 8pt;''>&amp;contraintes(liste_groupe=servitudes d''utilites publiques applicables;liste_ssgroupe=risques)</span></p>
<p><span style=''font-family: helvetica; font-size: 8pt;''>&amp;contraintes(liste_groupe=autres servitudes;liste_ssgroupe=risques)</span></p>
<p> </p>
<hr />
<p> </p>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt;''>OBSERVATIONS :</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Veuillez nous faire parvenir vos observations sur ce projet dans les meilleurs délais.</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Date de réception par le service [libelle_service] : ..........................</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span><br /><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Signature :</span></p>
<p> </p>
<p> </p>
<p> <br pagebreak=''true'' /></p>
<p style=''text-align: center;''><span style=''font-size: 14pt; font-weight: bold;''><em><span class=''mce_maj'' style=''font-family: helvetica;''><span class=''mce_maj''><span style=''font-size: 12pt;''>Ville &delaville</span><br /></span></span></em></span><span class=''mce_maj'' style=''font-family: helvetica; font-size: 12pt; font-weight: bold;''><span class=''mce_maj'' style=''text-align: center; font-family: helvetica;''><span class=''field_value pre''><span class=''field_value pre''><em>&amp;libelle_direction<br /></em></span></span></span></span><span class=''mce_maj'' style=''font-family: helvetica; font-size: 18pt; font-weight: bold;''><span class=''mce_maj'' style=''text-align: center; font-family: helvetica; font-size: 12pt;''><span class=''field_value pre''><span class=''field_value pre''><em><span style=''font-size: 12pt;''>service des autorisations d''urbanisme</span> <br /></em></span></span></span></span><span style=''font-family: helvetica; font-size: 10pt;''>&adresse_voie_sau</span></p>
<table style=''width: 92%; margin-left: auto; margin-right: auto;'' border=''1'' cellpadding=''3''>
<tbody>
<tr>
<td style=''width: 60%;''>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Dossier :       <span class=''mce_maj'' style=''font-weight: bold; font-size: 12pt;''>[libelle_dossier]</span></span><br /><span style=''font-family: helvetica; font-size: 10pt;''>Déposé le :   [date_depot_dossier]</span><br /><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Demandeur</span> : <span style=''font-weight: bold;''>[civilite_demandeur] <span class=''mce_maj''>[nom_demandeur]</span></span></span><br /><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>[adresse_demandeur]</span></p>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''> [code_postal_demandeur] [ville_demandeur] [societe_demandeur]</span><br /><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Adresse des travaux</span> :</span><br /><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>[terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier]</span><br /><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>[terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier]</span><br /><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''><span class=''mce_maj''>N</span></span><span class=''mce_min''><span style=''text-decoration: underline;''>ature des travaux</span> :</span></span><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''> [co_projet_desc_donnees_techniques]</span></p>
</td>
<td>
<p style=''text-align: center;''><span style=''font-size: 10pt; font-family: helvetica;''><span class=''mce_codebarre''>[code_barres_consultation]</span></span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''><span style=''text-decoration: underline;''>Destinataire</span> :</span><br /><span class=''mce_maj'' style=''font-size: 10pt; font-family: helvetica; font-weight: bold;''>[libelle_service]</span><br /><span class=''mce_maj'' style=''font-size: 10pt; font-family: helvetica; font-weight: bold;''>[adresse_service] [adresse2_service]</span><br /><span class=''mce_maj'' style=''font-size: 10pt; font-family: helvetica; font-weight: bold;''>[cp_service] [ville_service]</span></p>
</td>
</tr>
<tr>
<td colspan=''2''><span style=''font-family: helvetica; font-size: 8pt;''>&amp;contraintes(liste_groupe=Zones Du Plu)</span><br /><span style=''font-family: helvetica; font-size: 10pt;''>Destination/Surface de plancher en m² : [tab_surface_donnees_techniques]</span></td>
</tr>
<tr>
<td><span style=''font-family: helvetica; font-size: 8pt;''><span style=''text-decoration: underline;''><em>Transmis le</em></span> : [date_envoi_dossier]</span></td>
<td style=''text-align: right;''>
<p><span style=''font-family: helvetica; font-size: 8pt;''><span style=''text-decoration: underline;''><em>Affaire suivie par</em></span> : [nom_instructeur] - [telephone_instructeur]</span></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline; font-weight: bold;''>Objet</span> : Demande d''avis sur DOSSIER N° :<span style=''font-weight: bold; font-size: 12pt;''> <span class=''mce_maj''>[libelle_dossier]</span></span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''><span style=''font-weight: bold;''>P.J</span></span> : 1 dossier</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Monsieur,</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>J''ai<span class=''mce_min''> l''honneur de vous faire connaître que la demande citée en objet a été déposée dans notre service.</span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Je<span class=''mce_min''> vous prie de bien vouloir me faire parvenir votre avis sur ce dossier dans les meilleurs délais.</span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>D<span class=''mce_min''>ate limite de réponse :<span style=''font-weight: bold;''> [delai_service] [delai_type_service] à dater de la réception de ce courrier.</span></span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>D<span class=''mce_min''>ate limite d''instruction du dossier :  <span style=''font-weight: bold;''>[date_limite_dossier]</span></span><span style=''font-weight: bold;''>.</span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>V<span class=''mce_min''>euillez agréer, Monsieur, l''assurance de mes sentiments distingués.</span></span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Votre<span class=''mce_min''> avis sera adressé à  </span><span class=''mce_maj''>Ville &delaville -</span> <span class=''mce_maj''>&amp;adresse_direction_urbanisme</span></span></p>
<hr />
<table border=''1'' cellpadding=''5''>
<tbody>
<tr>
<td>
<p> </p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>La demande a été reçue dans notre service en date du …………………....                   </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>L’établissement recevant du public est classé en    .............................. catégorie.                   </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''> </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>La présente demande est <em>(rayer la mention inutile)</em> :                                </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>                                         * Complète                                                                                </span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>                                         * Incomplète pour les motifs suivants :</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''>          </span></p>
</td>
</tr>
</tbody>
</table>
'
WHERE id = 'consultation_ERP_papier';

--
-- END / Les délais de consultation peuvent être en jour
--


-- Ajout de champs de fusion dans la requête instruction
UPDATE om_requete
SET requete = 'SELECT

    --Données générales de l''événement d''instruction
    instruction.complement_om_html as complement_instruction,
    instruction.complement2_om_html as complement2_instruction,
    instruction.complement3_om_html as complement3_instruction,
    instruction.complement4_om_html as complement4_instruction,
    instruction.code_barres as code_barres_instruction,
    to_char(instruction.date_evenement,''DD/MM/YYYY'') as date_evenement_instruction, 
    om_lettretype.libelle as libelle_om_lettretype,
    instruction.archive_delai as archive_delai_instruction,

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    dossier.dossier as code_barres_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    to_char(dossier_autorisation.date_decision, ''DD/MM/YYYY'') as date_decision_da,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    dossier.terrain_superficie as terrain_superficie_dossier,
    quartier.libelle as libelle_quartier,

    avis_decision.libelle as libelle_avis_decision,

    --Données générales du paramétrage de l''événement
    evenement.libelle as libelle_evenement,
    evenement.etat as etat_evenement,
    evenement.delai as delai_evenement,
    evenement.accord_tacite as accord_tacite_evenement,
    evenement.delai_notification as delai_notification_evenement,
    evenement.avis_decision as avis_decision_evenement,
    evenement.autorite_competente as autorite_competente_evenement,

    --Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    --Adresse du terrain du dossier d''instruction 
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    CASE 
        WHEN dossier.terrain_adresse_bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', dossier.terrain_adresse_bp)
    END as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    CASE 
        WHEN dossier.terrain_adresse_cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', dossier.terrain_adresse_cedex)
    END as terrain_adresse_cedex_dossier,

    arrondissement.libelle as libelle_arrondissement,

    --Coordonnées du pétitionnaire principale
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as nom_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire principal initial
    CASE WHEN petitionnaire_principal_initial.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_initial_civilite.libelle, petitionnaire_principal_initial.particulier_nom, petitionnaire_principal_initial.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal_initial.personne_morale_raison_sociale, '' '', petitionnaire_principal_initial.personne_morale_denomination))
    END as nom_petitionnaire_principal_initial,
    petitionnaire_principal_initial.numero as numero_petitionnaire_principal_initial,
    petitionnaire_principal_initial.voie as voie_petitionnaire_principal_initial,
    petitionnaire_principal_initial.complement as complement_petitionnaire_principal_initial,
    petitionnaire_principal_initial.lieu_dit as lieu_dit_petitionnaire_principal_initial,
    CASE 
        WHEN petitionnaire_principal_initial.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal_initial.bp)
    END as bp_petitionnaire_principal_initial,
    petitionnaire_principal_initial.code_postal as code_postal_petitionnaire_principal_initial,
    petitionnaire_principal_initial.localite as localite_petitionnaire_principal_initial,
    CASE 
        WHEN petitionnaire_principal_initial.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal_initial.cedex)
    END as cedex_petitionnaire_principal_initial,
    petitionnaire_principal_initial.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_1.civilite, petitionnaire_1.particulier_nom, petitionnaire_1.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
    END as nom_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_2.civilite, petitionnaire_2.particulier_nom, petitionnaire_2.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
    END as nom_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_3.civilite, petitionnaire_3.particulier_nom, petitionnaire_3.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
    END as nom_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_4.civilite, petitionnaire_4.particulier_nom, petitionnaire_4.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
    END as nom_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_5.civilite, petitionnaire_5.particulier_nom, petitionnaire_5.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
    END as nom_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,

    --Coordonnées du délégataire
    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', delegataire_civilite.libelle, delegataire.particulier_nom, delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as nom_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complement_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    CASE 
        WHEN delegataire.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', delegataire.bp)
    END as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    CASE 
        WHEN delegataire.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', delegataire.cedex)
    END as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as civilite_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT_WS('' '', petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', petitionnaire_principal.bp)
            END
        ELSE 
            CASE 
                WHEN delegataire.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', delegataire.bp)
            END
    END as bp_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
            END 
        ELSE 
            CASE 
                WHEN delegataire.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', delegataire.cedex)
            END 
    END as cedex_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    --Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    to_char(dossier.date_notification_delai,''DD/MM/YYYY'') as date_notification_delai_dossier,
    
    --Noms des signataires
    TRIM(CONCAT(signataire_civilite.libelle, '' '',signataire_arrete.prenom, '' '', signataire_arrete.nom)) as arrete_signataire,
    TRIM(CONCAT(signataire_arrete.qualite, '' '', signataire_arrete.signature)) as signature_signataire,
    division.chef as chef_division,
    direction.chef as chef_direction,
    direction.libelle as libelle_direction,
    direction.description as description_direction,
    
    --Données générales des données techniquesDONNÉES TECHNIQUES
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    REGEXP_REPLACE(CONCAT(
        CASE
            WHEN donnees_techniques.su_cstr_shon1 IS NULL
            THEN ''''
            ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon2 IS NULL
            THEN ''''
            ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon3 IS NULL
            THEN ''''
            ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon4 IS NULL
            THEN ''''
            ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon5 IS NULL
            THEN ''''
            ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon6 IS NULL
            THEN ''''
            ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon7 IS NULL
            THEN ''''
            ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon8 IS NULL
            THEN ''''
            ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
        END, 
        CASE
            WHEN donnees_techniques.su_cstr_shon9 IS NULL
            THEN ''''
            ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
        END
    ), '' / $'', '''') as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques,
    
    --Bordereau d''envoi au maire
    CASE
        WHEN evenement.type = ''arrete''
        THEN CONCAT(''transmission d''''une proposition de décision sur '', evenement.libelle)
        ELSE CONCAT(''transmission d''''un courrier d''''instruction sur '', evenement.libelle)
    END as objet_bordereau_envoi_maire
    
FROM
    &DB_PREFIXEinstruction
LEFT JOIN
    &DB_PREFIXEsignataire_arrete
    ON instruction.signataire_arrete = signataire_arrete.signataire_arrete
LEFT JOIN
    &DB_PREFIXEcivilite as signataire_civilite
    ON signataire_arrete.civilite = signataire_civilite.civilite
LEFT JOIN
    &DB_PREFIXEom_lettretype
    ON instruction.lettretype = om_lettretype.id and om_lettretype.actif IS TRUE
LEFT JOIN
    &DB_PREFIXEdossier
    ON
        instruction.dossier=dossier.dossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        instruction.dossier = lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite as petitionnaire_principal_civilite
    ON
        petitionnaire_principal.particulier_civilite = petitionnaire_principal_civilite.civilite OR petitionnaire_principal.personne_morale_civilite = petitionnaire_principal_civilite.civilite
LEFT JOIN
    &DB_PREFIXElien_dossier_autorisation_demandeur
    ON
        dossier.dossier_autorisation = lien_dossier_autorisation_demandeur.dossier_autorisation AND lien_dossier_autorisation_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal_initial
    ON
        lien_dossier_autorisation_demandeur.demandeur = petitionnaire_principal_initial.demandeur
LEFT JOIN
    &DB_PREFIXEcivilite as petitionnaire_principal_initial_civilite
    ON
        petitionnaire_principal_initial.particulier_civilite = petitionnaire_principal_initial_civilite.civilite OR petitionnaire_principal_initial.personne_morale_civilite = petitionnaire_principal_initial_civilite.civilite
LEFT JOIN
    (
    &DB_PREFIXElien_dossier_demandeur AS lien_dossier_delegataire
        JOIN &DB_PREFIXEdemandeur as delegataire
        ON
            lien_dossier_delegataire.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire''
    )
    ON
        instruction.dossier = lien_dossier_delegataire.dossier AND lien_dossier_delegataire.petitionnaire_principal IS FALSE
LEFT JOIN
    &DB_PREFIXEcivilite as delegataire_civilite
    ON
        delegataire.particulier_civilite = delegataire_civilite.civilite OR delegataire.personne_morale_civilite = delegataire_civilite.civilite
LEFT JOIN (
    SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
    FROM &DB_PREFIXElien_dossier_demandeur
    LEFT JOIN &DB_PREFIXEdossier
        ON lien_dossier_demandeur.dossier=dossier.dossier 
        AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
    LEFT JOIN &DB_PREFIXEinstruction
        ON instruction.dossier = dossier.dossier
    WHERE instruction.instruction = &idx
    GROUP BY lien_dossier_demandeur.dossier
) as sub_petitionnaire_autre
ON instruction.dossier = sub_petitionnaire_autre.dossier
LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1] AND petitionnaire_1.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_1
    ON
        petitionnaire_1.particulier_civilite = civilite_1.civilite OR petitionnaire_1.personne_morale_civilite = civilite_1.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2] AND petitionnaire_2.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_2
    ON
        petitionnaire_2.particulier_civilite = civilite_2.civilite OR petitionnaire_2.personne_morale_civilite = civilite_2.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3] AND petitionnaire_3.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_3
    ON
        petitionnaire_3.particulier_civilite = civilite_3.civilite OR petitionnaire_3.personne_morale_civilite = civilite_3.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4] AND petitionnaire_4.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_4
    ON
        petitionnaire_4.particulier_civilite = civilite_4.civilite OR petitionnaire_4.personne_morale_civilite = civilite_4.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5] AND petitionnaire_5.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_5
    ON
        petitionnaire_5.particulier_civilite = civilite_5.civilite OR petitionnaire_5.personne_morale_civilite = civilite_5.civilite
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN 
    &DB_PREFIXEdirection
    ON division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN 
    &DB_PREFIXEevenement
    ON
        instruction.evenement = evenement.evenement
LEFT JOIN 
    &DB_PREFIXEquartier
    ON
        dossier.quartier = quartier.quartier
WHERE instruction.instruction = &idx',
merge_fields = '--Données générales de l''événement d''instruction
[complement_instruction]
[complement2_instruction]
[complement3_instruction]
[complement4_instruction]
[code_barres_instruction]
[date_evenement_instruction]
[libelle_om_lettretype]
[archive_delai_instruction]

--Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]    [delai_dossier]    [terrain_references_cadastrales_dossier]
[terrain_superficie_dossier]
[libelle_quartier]
[libelle_da]
[date_decision_da]

[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]

[libelle_avis_decision]

--Données générales du paramétrage de l''événement
[libelle_evenement]
[etat_evenement]
[delai_evenement]
[accord_tacite_evenement]
[delai_notification_evenement]
[avis_decision_evenement]
[autorite_competente_evenement]

--Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

--Adresse du terrain du dossier d''instruction 
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]

[libelle_arrondissement]

--Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées du pétitionnaire principal initial
[nom_petitionnaire_principal_initial]
[numero_petitionnaire_principal_initial]    [voie_petitionnaire_principal_initial]    [complement_petitionnaire_principal_initial]
[lieu_dit_petitionnaire_principal_initial]    [bp_petitionnaire_principal_initial]
[code_postal_petitionnaire_principal_initial]    [localite_petitionnaire_principal_initial]    [cedex_petitionnaire_principal_initial]
[pays_petitionnaire_principal_initial]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''a 5)
[numero_petitionnaire_1](jusqu''a 5)    [voie_petitionnaire_1](jusqu''a 5)    
[complement_petitionnaire_1](jusqu''a 5)
[lieu_dit_petitionnaire_1](jusqu''a 5)    [bp_petitionnaire_1](jusqu''a 5)
[code_postal_petitionnaire_1](jusqu''a 5)    [localite_petitionnaire_1](jusqu''a 5)    
[cedex_petitionnaire_1](jusqu''a 5)
[pays_petitionnaire_1](jusqu''a 5)

--Coordonnées du délégataire
[nom_delegataire]
[numero_delegataire]    [voie_delegataire]    [complement_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire]

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant]

--Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]
[date_notification_delai_dossier]

--Noms des signataires
[arrete_signataire]
[signature_signataire]
[chef_division]
[chef_direction]
[libelle_direction]
[description_direction]

--Données générales des données techniques
[projet_desc_donnees_techniques]
[co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]

--Bordereau envoi au maire
[objet_bordereau_envoi_maire]'
WHERE code = 'instruction';

-- om_requete dossier
UPDATE om_requete SET requete ='
SELECT

    -- Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier,
    dossier.dossier as code_barres_dossier,
    dossier_autorisation.dossier_autorisation_libelle as libelle_da,

    dossier_autorisation_type_detaille.code as code_datd,
    dossier_autorisation_type_detaille.libelle as libelle_datd,
    dossier_autorisation_type.code as code_dat,
    dossier_autorisation_type.libelle as libelle_dat,
    dossier_instruction_type.code as code_dit,
    dossier_instruction_type.libelle as libelle_dit,
    dossier.delai as delai_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,

    avis_decision.libelle as libelle_avis_decision,

    -- Coordonnées de l''instructeur
    instructeur.nom as nom_instructeur,
    instructeur.telephone as telephone_instructeur,
    division.code as division_instructeur,
    om_utilisateur.email as email_instructeur,

    -- Noms des signataires
    division.chef as division_chef,
    direction.chef as direction_chef,
    direction.libelle as libelle_direction,
    direction.description as description_direction,

    -- Adresse du terrain du dossier d''instruction
    dossier.terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    dossier.terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    CASE 
        WHEN dossier.terrain_adresse_bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', dossier.terrain_adresse_bp)
    END as terrain_adresse_bp_dossier,
    dossier.terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    dossier.terrain_adresse_localite as terrain_adresse_localite_dossier,
    CASE 
        WHEN dossier.terrain_adresse_cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', dossier.terrain_adresse_cedex)
    END as terrain_adresse_cedex_dossier,
    arrondissement.libelle as libelle_arrondissement,

    -- Nom et prénom de l''architecte
    CONCAT(architecte.prenom||'' '', architecte.nom) as architecte,

    -- Coordonnées du pétitionnaire principal
    CASE WHEN petitionnaire_principal.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '',petitionnaire_principal_civilite.libelle, petitionnaire_principal.particulier_nom, petitionnaire_principal.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
    END as nom_petitionnaire_principal,
    petitionnaire_principal.numero as numero_petitionnaire_principal,
    petitionnaire_principal.voie as voie_petitionnaire_principal,
    petitionnaire_principal.complement as complement_petitionnaire_principal,
    petitionnaire_principal.lieu_dit as lieu_dit_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_principal.bp)
    END as bp_petitionnaire_principal,
    petitionnaire_principal.code_postal as code_postal_petitionnaire_principal,
    petitionnaire_principal.localite as localite_petitionnaire_principal,
    CASE 
        WHEN petitionnaire_principal.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
    END as cedex_petitionnaire_principal,
    petitionnaire_principal.pays as pays_petitionnaire_principal,

    --Coordonnées du pétitionnaire 1
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_1.civilite, petitionnaire_1.particulier_nom, petitionnaire_1.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
    END as nom_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_2.civilite, petitionnaire_2.particulier_nom, petitionnaire_2.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
    END as nom_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_3.civilite, petitionnaire_3.particulier_nom, petitionnaire_3.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
    END as nom_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_4.civilite, petitionnaire_4.particulier_nom, petitionnaire_4.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
    END as nom_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_5.civilite, petitionnaire_5.particulier_nom, petitionnaire_5.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
    END as nom_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,

    -- Coordonnées du délégataire
    CASE
        WHEN delegataire.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '',delegataire_civilite.libelle, delegataire.particulier_nom, delegataire.particulier_prenom))
        ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
    END as nom_delegataire,
    delegataire.numero as numero_delegataire,
    delegataire.voie as voie_delegataire,
    delegataire.complement as complement_delegataire,
    delegataire.lieu_dit as lieu_dit_delegataire,
    CASE 
        WHEN delegataire.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', delegataire.bp)
    END as bp_delegataire,
    delegataire.code_postal as code_postal_delegataire,
    delegataire.localite as ville_delegataire,
    CASE 
        WHEN delegataire.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', delegataire.cedex)
    END as cedex_delegataire,
    delegataire.pays as pays_delegataire,

    -- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
    CASE
        WHEN delegataire.qualite IS NULL
        THEN
            CASE WHEN petitionnaire_principal.qualite=''particulier''
                    THEN TRIM(CONCAT(petitionnaire_principal.particulier_nom, '' '', petitionnaire_principal.particulier_prenom))
                    ELSE TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, '' '', petitionnaire_principal.personne_morale_denomination))
            END
        ELSE
            CASE WHEN delegataire.qualite=''particulier''
                THEN TRIM(CONCAT(delegataire.particulier_nom, '' '', delegataire.particulier_prenom))
                ELSE TRIM(CONCAT(delegataire.personne_morale_raison_sociale, '' '', delegataire.personne_morale_denomination))
            END
    END as nom_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.numero
        ELSE delegataire.numero
    END as numero_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.voie
        ELSE delegataire.voie
    END as voie_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.complement
        ELSE delegataire.complement
    END as complement_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.lieu_dit
        ELSE delegataire.lieu_dit
    END as lieu_dit_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', petitionnaire_principal.bp)
            END
        ELSE 
            CASE 
                WHEN delegataire.bp IS NULL
                THEN ''''
                ELSE CONCAT(''BP '', delegataire.bp)
            END
    END as bp_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.code_postal
        ELSE delegataire.code_postal
    END as code_postal_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.localite
        ELSE delegataire.localite
    END as ville_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN 
            CASE 
                WHEN petitionnaire_principal.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', petitionnaire_principal.cedex)
            END
        ELSE 
            CASE 
                WHEN delegataire.cedex IS NULL
                THEN ''''
                ELSE CONCAT(''CEDEX '', delegataire.cedex)
            END
    END as cedex_correspondant,

    CASE
        WHEN delegataire.qualite IS NULL
        THEN petitionnaire_principal.pays
        ELSE delegataire.pays
    END as pays_correspondant,

    -- Dates importantes du dossier d''instruction
    to_char(dossier.date_depot, ''DD/MM/YYYY'') as date_depot_dossier,
    to_char(dossier.date_decision, ''DD/MM/YYYY'') as date_decision_dossier,
    to_char(dossier.date_limite,''DD/MM/YYYY'') as date_limite_dossier,
    to_char(dossier.date_achevement,''DD/MM/YYYY'') as date_achevement_dossier,
    to_char(dossier.date_conformite,''DD/MM/YYYY'') as date_conformite_dossier,
    
    -- Données générales des données techniques
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    REGEXP_REPLACE(CONCAT(
        CASE
            WHEN donnees_techniques.su_cstr_shon1 IS NULL
            THEN ''''
            ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon2 IS NULL
            THEN ''''
            ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon3 IS NULL
            THEN ''''
            ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon4 IS NULL
            THEN ''''
            ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon5 IS NULL
            THEN ''''
            ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon6 IS NULL
            THEN ''''
            ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon7 IS NULL
            THEN ''''
            ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon8 IS NULL
            THEN ''''
            ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
        END, 
        CASE
            WHEN donnees_techniques.su_cstr_shon9 IS NULL
            THEN ''''
            ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
        END
    ), '' / $'', '''') as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques

FROM
    &DB_PREFIXEdossier
LEFT JOIN
    &DB_PREFIXElien_dossier_demandeur
    ON
        dossier.dossier = lien_dossier_demandeur.dossier
LEFT JOIN
    &DB_PREFIXEdemandeur as petitionnaire_principal
    ON
        lien_dossier_demandeur.demandeur = petitionnaire_principal.demandeur AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN
    &DB_PREFIXEcivilite as petitionnaire_principal_civilite
    ON
        petitionnaire_principal.particulier_civilite = petitionnaire_principal_civilite.civilite OR petitionnaire_principal.personne_morale_civilite = petitionnaire_principal_civilite.civilite
LEFT JOIN
    &DB_PREFIXEdemandeur as delegataire
    ON
        lien_dossier_demandeur.demandeur = delegataire.demandeur AND delegataire.type_demandeur = ''delegataire'' 
LEFT JOIN
    &DB_PREFIXEcivilite as delegataire_civilite
    ON
        delegataire.particulier_civilite = delegataire_civilite.civilite OR delegataire.personne_morale_civilite = delegataire_civilite.civilite
LEFT JOIN (
    SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
    FROM &DB_PREFIXElien_dossier_demandeur
    LEFT JOIN &DB_PREFIXEdossier
        ON lien_dossier_demandeur.dossier=dossier.dossier 
        AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
    WHERE dossier.dossier = ''&idx''
    GROUP BY lien_dossier_demandeur.dossier
) as sub_petitionnaire_autre
ON dossier.dossier = sub_petitionnaire_autre.dossier
LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1] AND petitionnaire_1.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_1
    ON
        petitionnaire_1.particulier_civilite = civilite_1.civilite OR petitionnaire_1.personne_morale_civilite = civilite_1.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2] AND petitionnaire_2.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_2
    ON
        petitionnaire_2.particulier_civilite = civilite_2.civilite OR petitionnaire_2.personne_morale_civilite = civilite_2.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3] AND petitionnaire_3.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_3
    ON
        petitionnaire_3.particulier_civilite = civilite_3.civilite OR petitionnaire_3.personne_morale_civilite = civilite_3.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4] AND petitionnaire_4.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_4
    ON
        petitionnaire_4.particulier_civilite = civilite_4.civilite OR petitionnaire_4.personne_morale_civilite = civilite_4.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5] AND petitionnaire_5.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_5
    ON
        petitionnaire_5.particulier_civilite = civilite_5.civilite OR petitionnaire_5.personne_morale_civilite = civilite_5.civilite
LEFT JOIN
    &DB_PREFIXEdossier_instruction_type
    ON
        dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
LEFT JOIN
    &DB_PREFIXEdossier_autorisation
    ON
        dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type_detaille
    ON
        dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN
    &DB_PREFIXEdossier_autorisation_type
    ON
        dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN
    &DB_PREFIXEinstructeur
    ON
        dossier.instructeur = instructeur.instructeur
LEFT JOIN
    &DB_PREFIXEom_utilisateur
    ON
        om_utilisateur.om_utilisateur = instructeur.om_utilisateur
LEFT JOIN
    &DB_PREFIXEdivision
    ON
        instructeur.division = division.division
LEFT JOIN
    &DB_PREFIXEdirection
    ON
        division.direction = direction.direction
LEFT JOIN
    &DB_PREFIXEarrondissement
    ON
        dossier.terrain_adresse_code_postal = arrondissement.code_postal
LEFT JOIN
    &DB_PREFIXEavis_decision
    ON
        dossier.avis_decision = avis_decision.avis_decision
LEFT JOIN
    &DB_PREFIXEetat
    ON
        dossier.etat = etat.etat
LEFT JOIN
    &DB_PREFIXEdonnees_techniques
    ON
    dossier.dossier = donnees_techniques.dossier_instruction
LEFT JOIN
    &DB_PREFIXEarchitecte
    ON
    donnees_techniques.architecte = architecte.architecte
WHERE dossier.dossier = ''&idx''
', 
merge_fields = '-- Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]
[libelle_da]    
[code_datd]    [libelle_datd]
[code_dat]    [libelle_dat]
[code_dit]    [libelle_dit]
[delai_dossier]
[terrain_references_cadastrales_dossier]
[libelle_avis_decision]

-- Coordonnées de l''instructeur
[nom_instructeur]
[telephone_instructeur]
[division_instructeur]
[email_instructeur]

-- Noms des signataires
[division_chef]
[direction_chef]
[libelle_direction]
[description_direction]

-- Adresse du terrain du dossier d''instruction
[terrain_adresse_voie_numero_dossier]    [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_cedex_dossier]
[libelle_arrondissement]

-- Nom et prénom de l''architecte
[architecte]

-- Coordonnées du pétitionnaire principal
[nom_petitionnaire_principal]
[numero_petitionnaire_principal]    [voie_petitionnaire_principal]    [complement_petitionnaire_principal]
[lieu_dit_petitionnaire_principal]    [bp_petitionnaire_principal]
[code_postal_petitionnaire_principal]    [localite_petitionnaire_principal]    [cedex_petitionnaire_principal]
[pays_petitionnaire_principal]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''a 5)
[numero_petitionnaire_1](jusqu''a 5)    [voie_petitionnaire_1](jusqu''a 5)    
[complement_petitionnaire_1](jusqu''a 5)
[lieu_dit_petitionnaire_1](jusqu''a 5)    [bp_petitionnaire_1](jusqu''a 5)
[code_postal_petitionnaire_1](jusqu''a 5)    [localite_petitionnaire_1](jusqu''a 5)    
[cedex_petitionnaire_1](jusqu''a 5)
[pays_petitionnaire_1](jusqu''a 5)

-- Coordonnées du délégataire
[nom_delegataire]
[numero_delegataire]    [voie_delegataire]    [complement_delegataire]
[lieu_dit_delegataire]    [bp_delegataire]
[code_postal_delegataire]    [ville_delegataire]    [cedex_delegataire]
[pays_delegataire]

-- CORRESPONDANT : destinataire du courrier. Il est le délégataire ou le pétitionnaire principal
[nom_correspondant]
[numero_correspondant]    [voie_correspondant]    [complement_correspondant]
[lieu_dit_correspondant]    [bp_correspondant]
[code_postal_correspondant]    [ville_correspondant]    [cedex_correspondant]
[pays_correspondant]

-- Dates importantes du dossier d''instruction
[date_depot_dossier]
[date_decision_dossier]
[date_limite_dossier]
[date_achevement_dossier]
[date_conformite_dossier]

-- Données générales des données techniques
[projet_desc_donnees_techniques]
[co_tot_log_nb_donnees_techniques]    [co_statio_place_nb_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]
[tab_surface_donnees_techniques]'
WHERE code = 'dossier';

-- om_requete rapport_instruction
UPDATE om_requete SET requete ='
SELECT

    --Données générales du rapport d''instruction
    rapport_instruction.dossier_instruction as dossier_instruction_rapport_instruction, 
    analyse_reglementaire_om_html as analyse_reglementaire_rapport_instruction,
    description_projet_om_html as description_projet_rapport_instruction,
    proposition_decision as proposition_decision_rapport_instruction, 

    --Données générales du dossier d''instruction
    dossier.dossier_libelle as libelle_dossier, 
    dossier.dossier as code_barres_dossier,
    etat as etat_dossier,
    pos as pos_dossier, 
    servitude as servitude_dossier, 
    dossier.delai as delai_dossier,
    dossier_autorisation_type_detaille.libelle as libelle_datd, 
    avis_decision.libelle as libelle_avis_decision,

    --Adresse du terrain dossier d''instruction
    terrain_adresse_voie_numero as terrain_adresse_voie_numero_dossier,
    dossier.terrain_adresse_voie as terrain_adresse_voie_dossier,
    terrain_adresse_lieu_dit as terrain_adresse_lieu_dit_dossier,
    terrain_adresse_localite as terrain_adresse_localite_dossier,
    terrain_adresse_code_postal as terrain_adresse_code_postal_dossier,
    terrain_adresse_bp as terrain_adresse_bp_dossier,
    terrain_adresse_cedex as terrain_adresse_cedex_dossier,
    terrain_superficie as terrain_superficie_dossier,
    replace(dossier.terrain_references_cadastrales, '';'', '' '') as terrain_references_cadastrales_dossier,
    
    --Coordonnées du demandeur
    civilite.code as code_civilite,
    demandeur.particulier_nom as particulier_nom_demandeur,
    demandeur.particulier_prenom as particulier_prenom_demandeur,
    demandeur.personne_morale_denomination as personne_morale_denomination_demandeur,
    demandeur.personne_morale_raison_sociale as personne_morale_raison_sociale_demandeur,
    demandeur.personne_morale_siret as personne_morale_siret_demandeur,
    demandeur.personne_morale_nom as personne_morale_nom_demandeur,
    demandeur.personne_morale_prenom as personne_morale_prenom_demandeur,
    demandeur.numero as numero_demandeur,
    demandeur.voie as voie_demandeur,
    demandeur.complement as complement_demandeur,
    demandeur.lieu_dit as lieu_dit_demandeur,
    demandeur.localite as localite_demandeur,
    demandeur.code_postal as code_postal_demandeur,
    demandeur.bp as bp_demandeur,
    demandeur.cedex as cedex_demandeur,

    --Coordonnées du pétitionnaire 1
    CASE WHEN petitionnaire_1.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_1.civilite, petitionnaire_1.particulier_nom, petitionnaire_1.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_1.personne_morale_raison_sociale, '' '', petitionnaire_1.personne_morale_denomination))
    END as nom_petitionnaire_1,
    petitionnaire_1.numero as numero_petitionnaire_1,
    petitionnaire_1.voie as voie_petitionnaire_1,
    petitionnaire_1.complement as complement_petitionnaire_1,
    petitionnaire_1.lieu_dit as lieu_dit_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_1.bp)
    END as bp_petitionnaire_1,
    petitionnaire_1.code_postal as code_postal_petitionnaire_1,
    petitionnaire_1.localite as localite_petitionnaire_1,
    CASE 
        WHEN petitionnaire_1.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_1.cedex)
    END as cedex_petitionnaire_1,
    petitionnaire_1.pays as pays_petitionnaire_1,

    --Coordonnées du pétitionnaire 2
    CASE WHEN petitionnaire_2.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_2.civilite, petitionnaire_2.particulier_nom, petitionnaire_2.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_2.personne_morale_raison_sociale, '' '', petitionnaire_2.personne_morale_denomination))
    END as nom_petitionnaire_2,
    petitionnaire_2.numero as numero_petitionnaire_2,
    petitionnaire_2.voie as voie_petitionnaire_2,
    petitionnaire_2.complement as complement_petitionnaire_2,
    petitionnaire_2.lieu_dit as lieu_dit_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_2.bp)
    END as bp_petitionnaire_2,
    petitionnaire_2.code_postal as code_postal_petitionnaire_2,
    petitionnaire_2.localite as localite_petitionnaire_2,
    CASE 
        WHEN petitionnaire_2.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_2.cedex)
    END as cedex_petitionnaire_2,
    petitionnaire_2.pays as pays_petitionnaire_2,

    --Coordonnées du pétitionnaire 3
    CASE WHEN petitionnaire_3.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_3.civilite, petitionnaire_3.particulier_nom, petitionnaire_3.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_3.personne_morale_raison_sociale, '' '', petitionnaire_3.personne_morale_denomination))
    END as nom_petitionnaire_3,
    petitionnaire_3.numero as numero_petitionnaire_3,
    petitionnaire_3.voie as voie_petitionnaire_3,
    petitionnaire_3.complement as complement_petitionnaire_3,
    petitionnaire_3.lieu_dit as lieu_dit_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_3.bp)
    END as bp_petitionnaire_3,
    petitionnaire_3.code_postal as code_postal_petitionnaire_3,
    petitionnaire_3.localite as localite_petitionnaire_3,
    CASE 
        WHEN petitionnaire_3.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_3.cedex)
    END as cedex_petitionnaire_3,
    petitionnaire_3.pays as pays_petitionnaire_3,

    --Coordonnées du pétitionnaire 4
    CASE WHEN petitionnaire_4.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_4.civilite, petitionnaire_4.particulier_nom, petitionnaire_4.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_4.personne_morale_raison_sociale, '' '', petitionnaire_4.personne_morale_denomination))
    END as nom_petitionnaire_4,
    petitionnaire_4.numero as numero_petitionnaire_4,
    petitionnaire_4.voie as voie_petitionnaire_4,
    petitionnaire_4.complement as complement_petitionnaire_4,
    petitionnaire_4.lieu_dit as lieu_dit_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_4.bp)
    END as bp_petitionnaire_4,
    petitionnaire_4.code_postal as code_postal_petitionnaire_4,
    petitionnaire_4.localite as localite_petitionnaire_4,
    CASE 
        WHEN petitionnaire_4.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_4.cedex)
    END as cedex_petitionnaire_4,
    petitionnaire_4.pays as pays_petitionnaire_4,

    --Coordonnées du pétitionnaire 5
    CASE WHEN petitionnaire_5.qualite=''particulier''
        THEN TRIM(CONCAT_WS('' '', civilite_5.civilite, petitionnaire_5.particulier_nom, petitionnaire_5.particulier_prenom))
        ELSE TRIM(CONCAT(petitionnaire_5.personne_morale_raison_sociale, '' '', petitionnaire_5.personne_morale_denomination))
    END as nom_petitionnaire_5,
    petitionnaire_5.numero as numero_petitionnaire_5,
    petitionnaire_5.voie as voie_petitionnaire_5,
    petitionnaire_5.complement as complement_petitionnaire_5,
    petitionnaire_5.lieu_dit as lieu_dit_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.bp IS NULL
        THEN ''''
        ELSE CONCAT(''BP '', petitionnaire_5.bp)
    END as bp_petitionnaire_5,
    petitionnaire_5.code_postal as code_postal_petitionnaire_5,
    petitionnaire_5.localite as localite_petitionnaire_5,
    CASE 
        WHEN petitionnaire_5.cedex IS NULL
        THEN ''''
        ELSE CONCAT(''CEDEX '', petitionnaire_5.cedex)
    END as cedex_petitionnaire_5,
    petitionnaire_5.pays as pays_petitionnaire_5,
    
    --Nom de l''instructeur
    instructeur.nom as nom_instructeur, 

    --Noms des signataires
    division.chef as chef_division,
    direction.chef as chef_direction,
    direction.libelle as libelle_direction,
    direction.description as description_direction,

    --Données techniques
    TRIM(CONCAT(
        donnees_techniques.co_projet_desc, '' '',
        donnees_techniques.am_projet_desc, '' '',
        donnees_techniques.dm_projet_desc, '' '',
        donnees_techniques.ope_proj_desc
        )) as projet_desc_donnees_techniques,
    donnees_techniques.am_lot_max_nb as am_lot_max_nb_donnees_techniques,
    donnees_techniques.am_lot_max_shon as am_lot_max_shon_donnees_techniques,
    donnees_techniques.su_cstr_shon_tot as su_cstr_shon_tot_donnees_techniques,
    donnees_techniques.su_demo_shon_tot as su_demo_shon_tot_donnees_techniques,
    REGEXP_REPLACE(CONCAT(
        CASE
            WHEN donnees_techniques.su_cstr_shon1 IS NULL
            THEN ''''
            ELSE CONCAT(''Habitation - '', donnees_techniques.su_cstr_shon1, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon2 IS NULL
            THEN ''''
            ELSE CONCAT(''Hébergement hôtelier - '', donnees_techniques.su_cstr_shon2, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon3 IS NULL
            THEN ''''
            ELSE CONCAT(''Bureaux - '', donnees_techniques.su_cstr_shon3, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon4 IS NULL
            THEN ''''
            ELSE CONCAT(''Commerce - '', donnees_techniques.su_cstr_shon4, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon5 IS NULL
            THEN ''''
            ELSE CONCAT(''Artisanat - '', donnees_techniques.su_cstr_shon5, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon6 IS NULL
            THEN ''''
            ELSE CONCAT(''Industrie - '', donnees_techniques.su_cstr_shon6, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon7 IS NULL
            THEN ''''
            ELSE CONCAT(''Exploitation agricole ou forestière - '', donnees_techniques.su_cstr_shon7, '' m² / '')
        END,
        CASE
            WHEN donnees_techniques.su_cstr_shon8 IS NULL
            THEN ''''
            ELSE CONCAT(''Entrepôt - '', donnees_techniques.su_cstr_shon8, '' m² / '')
        END, 
        CASE
            WHEN donnees_techniques.su_cstr_shon9 IS NULL
            THEN ''''
            ELSE CONCAT(''Service public ou d''''intérêt collectif - '', donnees_techniques.su_cstr_shon9, '' m²'')
        END
    ), '' / $'', '''') as tab_surface_donnees_techniques,
    donnees_techniques.co_tot_log_nb as co_tot_log_nb_donnees_techniques,
    donnees_techniques.co_statio_place_nb as co_statio_place_nb_donnees_techniques


FROM 

    &DB_PREFIXErapport_instruction 
    LEFT JOIN &DB_PREFIXEdossier 
        ON dossier.dossier=rapport_instruction.dossier_instruction 
    LEFT JOIN &DB_PREFIXEavis_decision 
        ON dossier.avis_decision = avis_decision.avis_decision
    LEFT JOIN &DB_PREFIXEdonnees_techniques
        ON dossier.dossier = donnees_techniques.dossier_instruction
    LEFT JOIN &DB_PREFIXEdivision
        ON dossier.division = division.division
    LEFT JOIN &DB_PREFIXEdirection
        ON division.direction = direction.direction
    LEFT JOIN &DB_PREFIXEinstructeur 
        ON instructeur.instructeur=dossier.instructeur 
    LEFT JOIN &DB_PREFIXElien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier 
    LEFT JOIN &DB_PREFIXEdemandeur 
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur
    LEFT JOIN (
        SELECT lien_dossier_demandeur.dossier, array_agg(lien_dossier_demandeur.demandeur ORDER BY lien_dossier_demandeur.demandeur) AS petitionnaire_autre
        FROM &DB_PREFIXElien_dossier_demandeur
        LEFT JOIN &DB_PREFIXEdossier
            ON lien_dossier_demandeur.dossier=dossier.dossier 
            AND lien_dossier_demandeur.petitionnaire_principal IS FALSE
        LEFT JOIN &DB_PREFIXErapport_instruction
            ON rapport_instruction.dossier_instruction = dossier.dossier
        WHERE rapport_instruction.rapport_instruction = &idx
        GROUP BY lien_dossier_demandeur.dossier
    ) as sub_petitionnaire_autre
    ON rapport_instruction.dossier_instruction = sub_petitionnaire_autre.dossier
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_1
    ON
        petitionnaire_1.demandeur = petitionnaire_autre[1] AND petitionnaire_1.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_1
    ON
        petitionnaire_1.particulier_civilite = civilite_1.civilite OR petitionnaire_1.personne_morale_civilite = civilite_1.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_2
    ON
        petitionnaire_2.demandeur = petitionnaire_autre[2] AND petitionnaire_2.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_2
    ON
        petitionnaire_2.particulier_civilite = civilite_2.civilite OR petitionnaire_2.personne_morale_civilite = civilite_2.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_3
    ON
        petitionnaire_3.demandeur = petitionnaire_autre[3] AND petitionnaire_3.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_3
    ON
        petitionnaire_3.particulier_civilite = civilite_3.civilite OR petitionnaire_3.personne_morale_civilite = civilite_3.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_4
    ON
        petitionnaire_4.demandeur = petitionnaire_autre[4] AND petitionnaire_4.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_4
    ON
        petitionnaire_4.particulier_civilite = civilite_4.civilite OR petitionnaire_4.personne_morale_civilite = civilite_4.civilite
    LEFT JOIN
        &DB_PREFIXEdemandeur as petitionnaire_5
    ON
        petitionnaire_5.demandeur = petitionnaire_autre[5] AND petitionnaire_5.type_demandeur != ''delegataire''
    LEFT JOIN
        &DB_PREFIXEcivilite as civilite_5
    ON
        petitionnaire_5.particulier_civilite = civilite_5.civilite OR petitionnaire_5.personne_morale_civilite = civilite_5.civilite
    LEFT JOIN &DB_PREFIXEcivilite 
        ON demandeur.personne_morale_civilite=civilite.civilite OR demandeur.particulier_civilite=civilite.civilite 
    LEFT JOIN &DB_PREFIXEdossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN &DB_PREFIXEdossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
    
WHERE rapport_instruction = &idx AND lien_dossier_demandeur.petitionnaire_principal IS TRUE',
merge_fields = '
--Données générales du rapport d''instruction
[dossier_instruction_rapport_instruction]    [analyse_reglementaire_rapport_instruction]
[description_projet_rapport_instruction]    [proposition_decision_rapport_instruction]

--Données générales du dossier d''instruction
[libelle_dossier]    [code_barres_dossier]    [etat_dossier]
[pos_dossier]    [servitude_dossier]
[delai_dossier]    [libelle_datd]
[libelle_avis_decision]

--Adresse du terrain dossier d''instruction
[terrain_adresse_voie_numero_dossier]     [terrain_adresse_voie_dossier]
[terrain_adresse_lieu_dit_dossier]
[terrain_adresse_code_postal_dossier]    [terrain_adresse_localite_dossier]    [terrain_adresse_bp_dossier]
[terrain_adresse_cedex_dossier]    [terrain_superficie_dossier]
[terrain_references_cadastrales_dossier]

--Coordonnées du demandeur
[code_civilite]
[particulier_nom_demandeur]    [particulier_prenom_demandeur]    
[personne_morale_denomination_demandeur]    [personne_morale_raison_sociale_demandeur]    [personne_morale_siret_demandeur]
[personne_morale_nom_demandeur]    [personne_morale_prenom_demandeur]
[numero_demandeur]    [voie_demandeur]
[complement_demandeur]    [lieu_dit_demandeur]
[code_postal_demandeur]    [localite_demandeur]   [bp_demandeur]    [cedex_demandeur]

--Coordonnées des autres pétitionnaires
[nom_petitionnaire_1](jusqu''a 5)
[numero_petitionnaire_1](jusqu''a 5)    [voie_petitionnaire_1](jusqu''a 5)    
[complement_petitionnaire_1](jusqu''a 5)
[lieu_dit_petitionnaire_1](jusqu''a 5)    [bp_petitionnaire_1](jusqu''a 5)
[code_postal_petitionnaire_1](jusqu''a 5)    [localite_petitionnaire_1](jusqu''a 5)    
[cedex_petitionnaire_1](jusqu''a 5)
[pays_petitionnaire_1](jusqu''a 5)

--Nom de l''instructeur
[nom_instructeur]

--Noms des signataires
[chef_division]
[chef_direction]
[libelle_direction]
[description_direction]

--Données techniques
[projet_desc_donnees_techniques]
[am_lot_max_nb_donnees_techniques]    [am_lot_max_shon_donnees_techniques]    
[su_cstr_shon_tot_donnees_techniques]    [su_demo_shon_tot_donnees_techniques]    
[tab_surface_donnees_techniques]
[co_tot_log_nb_donnees_techniques]     [co_statio_place_nb_donnees_techniques]'

WHERE code = 'rapport_instruction';


UPDATE om_lettretype SET titre_om_htmletat=replace(titre_om_htmletat, 'co_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_lettretype SET titre_om_htmletat=replace(titre_om_htmletat, 'am_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_lettretype SET titre_om_htmletat=replace(titre_om_htmletat, 'dm_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_lettretype SET titre_om_htmletat=replace(titre_om_htmletat, 'ope_proj_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_lettretype SET corps_om_htmletatex=replace(corps_om_htmletatex, 'co_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_lettretype SET corps_om_htmletatex=replace(corps_om_htmletatex, 'am_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_lettretype SET corps_om_htmletatex=replace(corps_om_htmletatex, 'dm_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_lettretype SET corps_om_htmletatex=replace(corps_om_htmletatex, 'ope_proj_desc_donnees_techniques', 'projet_desc_donnees_techniques');

UPDATE om_etat SET titre_om_htmletat=replace(titre_om_htmletat, 'co_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_etat SET titre_om_htmletat=replace(titre_om_htmletat, 'am_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_etat SET titre_om_htmletat=replace(titre_om_htmletat, 'dm_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_etat SET titre_om_htmletat=replace(titre_om_htmletat, 'ope_proj_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_etat SET corps_om_htmletatex=replace(corps_om_htmletatex, 'co_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_etat SET corps_om_htmletatex=replace(corps_om_htmletatex, 'am_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_etat SET corps_om_htmletatex=replace(corps_om_htmletatex, 'dm_projet_desc_donnees_techniques', 'projet_desc_donnees_techniques');
UPDATE om_etat SET corps_om_htmletatex=replace(corps_om_htmletatex, 'ope_proj_desc_donnees_techniques', 'projet_desc_donnees_techniques');

-- agrandissement de la ville du demandeur
ALTER TABLE "demandeur" ALTER "localite" TYPE character varying(250);
ALTER TABLE "demandeur" ALTER "numero" TYPE character varying(10);
