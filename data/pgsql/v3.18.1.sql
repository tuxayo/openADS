-- Remise en forme des séquences
UPDATE om_widget SET om_widget=15 WHERE libelle='Dossiers auxquels on peut proposer une autre décision';
SELECT setval('om_profil_seq', (SELECT max(om_profil)+1 FROM om_profil), false);
SELECT setval('om_droit_seq', (SELECT max(om_droit)+1 FROM om_droit), false);
SELECT setval('om_dashboard_seq', (SELECT max(om_dashboard)+1 FROM om_dashboard), false);


-- Ajout de nouveaux profils
INSERT INTO om_profil (om_profil, libelle, hierarchie, om_validite_debut, om_validite_fin)
SELECT nextval('om_profil_seq'),'GUICHET ET SUIVI',0,NULL ,NULL
WHERE
    NOT EXISTS (
        SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI'
    );

INSERT INTO om_profil (om_profil, libelle, hierarchie, om_validite_debut, om_validite_fin)
SELECT nextval('om_profil_seq'),'ADMINISTRATEUR GENERAL',0 ,NULL ,NULL
WHERE
    NOT EXISTS (
        SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL'
    );

INSERT INTO om_profil (om_profil, libelle, hierarchie, om_validite_debut, om_validite_fin)
SELECT nextval('om_profil_seq'),'INSTRUCTEUR POLYVALENT COMMUNE',0 ,NULL ,NULL
WHERE
    NOT EXISTS (
        SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE'
    );

INSERT INTO om_profil (om_profil, libelle, hierarchie, om_validite_debut, om_validite_fin)
SELECT nextval('om_profil_seq'),'INSTRUCTEUR POLYVALENT',0 ,NULL ,NULL
WHERE
    NOT EXISTS (
        SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT'
    );

-- Ajout des tableaux de bord des nouveaux profils
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='ADMINISTRATEUR GENERAL'), 'C1', 1, 12 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 12 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='ADMINISTRATEUR GENERAL'), 'C2', 2, 10 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 10 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='ADMINISTRATEUR GENERAL'), 'C2', 1, 8 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 8 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='ADMINISTRATEUR GENERAL'), 'C3', 1, 3 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 3 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='ADMINISTRATEUR GENERAL'), 'C1', 2, 9 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 9 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT COMMUNE'), 'C1', 1, 12 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 12 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT COMMUNE'), 'C2', 1, 5 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 5 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT COMMUNE'), 'C3', 1, 14 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 14 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT COMMUNE'), 'C3', 2, 8 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 8 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT COMMUNE'), 'C3', 3, 10 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 10 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT COMMUNE'), 'C1', 3, 3 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 3 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT COMMUNE'), 'C1', 2, 9 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 9 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT COMMUNE'), 'C2', 2, 11 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 11 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='GUICHET ET SUIVI'), 'C1', 1, 12 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 12 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='GUICHET ET SUIVI'), 'C2', 1, 5 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 5 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='GUICHET ET SUIVI'), 'C3', 1, 14 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 14 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='GUICHET ET SUIVI'), 'C1', 2, 9 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 9 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='GUICHET ET SUIVI'), 'C1', 2, 15 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 15 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT COMMUNE'), 'C2', 2, 15 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 15 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT'), 'C1', 1, 12 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 12 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT'), 'C2', 1, 5 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 5 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT'), 'C3', 1, 14 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 14 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT'), 'C3', 2, 8 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 8 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT'), 'C3', 3, 10 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 10 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT'), 'C1', 3, 3 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 3 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT'), 'C1', 2, 9 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 9 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT'), 'C2', 2, 11 
WHERE	
    NOT EXISTS (
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 11 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) SELECT nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle ='INSTRUCTEUR POLYVALENT'), 'C2', 2, 15 
WHERE    
    NOT EXISTS (        
    SELECT om_dashboard FROM om_dashboard WHERE om_widget = 15 AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')    
);

-- Ajout des droits des nouveaux profils









INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'affichage_reglementaire_attestation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'affichage_reglementaire_registre' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter_autre_que_retour_avis' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_fichier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_fichier_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_fichier_voir' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_om_fichier_consultation_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'delegataire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_attestation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_registre_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_autre_dossier_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_autre_dossier_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_dossier_encours_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_dossier_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_nouveau_dossier_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandes_avis_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandes_avis_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandeur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandeur_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandeur_modifier_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_add', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_add' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_uid_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_view', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_view' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_limites' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_limites_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_om_fichier_instruction_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_bordereaux' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_envoi_lettre_rar' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_autorisation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_export' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_guichet_unique' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_guichet_unique_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_guichet_unique_dashboard' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_suivi' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'password', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'password' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_modifier_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_edition' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_om_fichier_rapport_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_om_fichier_rapport_instruction_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'recherche_direct', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'recherche_direct' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'recherche_direct_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'recherche_direct_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'suivi_retours_de_consultation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'versement_archives' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_definaliser_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_finaliser_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_lu_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_voir', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_voir' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_frequent_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_frequent_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'action_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'action_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'affectation_automatique', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'affectation_automatique' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'affichage_reglementaire_attestation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'affichage_reglementaire_registre' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_frequent_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'arrondissement', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'arrondissement' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'autorite_competente_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'autorite_competente_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'bible', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'bible' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'civilite', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'civilite' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'commission', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'commission' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'commission_demandes_passage', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'commission_demandes_passage' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'commission_demandes_passage_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'commission_demandes_passage_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'commission_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'commission_mes_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'commission_om_fichier_commission_compte_rendu_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'commission_om_fichier_commission_compte_rendu_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'commission_om_fichier_commission_ordre_jour_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'commission_om_fichier_commission_ordre_jour_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'commission_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'commission_tous_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'commission_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'commission_type' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'commission_type_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'commission_type_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter_autre_que_retour_avis' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_definaliser_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_finaliser_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_mes_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_date_reception' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_lu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_lu_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_om_fichier_consultation_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_tous_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'contrainte' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'copie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'copie' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'delegataire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_attestation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_registre_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_autre_dossier_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_autre_dossier_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_dossier_encours_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_dossier_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_nouveau_dossier_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandes_avis_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandes_avis_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_type' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandeur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandeur_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandeur_modifier_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'direction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'direction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'directory', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'directory' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_add', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_add' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_uid_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_view', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_view' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_type' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_type_detaille', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_type_detaille' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_ajouter_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_modifier_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_modifier_lu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_possible', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_possible' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_supprimer_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_supprimer_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_tab_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_tab_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_geolocalisation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_localiser-sig-externe' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_geolocalisation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_geolocalisation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_geolocalisation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_geolocalisation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_type', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_type' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_message_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_message_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_modifier_lu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_message_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_modifier_lu_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_message_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_modifier_division', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_modifier_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_modifier_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_modifier_instructeur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_qualifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_qualifier_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_evenement_incomplet_majoration' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_limites' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_limites_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_tous_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_tous_limites' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'etat_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'etat_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'etat_dossier_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'etat_dossier_autorisation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'evenement_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'evenement_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'evenement_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'evenement_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'genre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'genre' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'groupe', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'groupe' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'import', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'import' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instructeur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_definaliser_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_finaliser_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_modification_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_modification_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_om_fichier_instruction_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_bordereaux' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_envoi_lettre_rar' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lien_service_om_utilisateur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lien_service_om_utilisateur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lien_service_service_categorie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lien_service_service_categorie' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_editer_donnees_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_editer_donnees_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_transferer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_transferer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_transferer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_administration', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_administration' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_autorisation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_guichet_unique' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_instruction_dashboard' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_parametrage', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_parametrage' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_suivi' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'messages_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'messages_mes_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'messages_tous_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'om_collectivite', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'om_collectivite' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'om_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'om_dashboard' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'om_etat', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'om_etat' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'om_lettretype', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'om_lettretype' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'om_logo', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'om_logo' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'om_parametre', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'om_parametre' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'om_requete', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'om_requete' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'om_sousetat', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'om_sousetat' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'om_utilisateur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'om_utilisateur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'om_widget', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'om_widget' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'password', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'password' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_modifier_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'profil_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'profil_instructeur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'quartier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'quartier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_definaliser_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_edition' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_finaliser_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'recherche_direct', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'recherche_direct' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'recherche_direct_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'recherche_direct_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'service', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'service' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'service_categorie', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'service_categorie' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'signataire_arrete', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'signataire_arrete' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'suivi_bordereaux' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'suivi_envoi_lettre_rar' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'suivi_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'suivi_retours_de_consultation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'versement_archives' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'workflows', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'workflows' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_om_fichier_rapport_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_om_fichier_rapport_instruction_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_bordereau_envoi_maire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_bordereau_envoi_maire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'bordereau_envoi_maire', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'bordereau_envoi_maire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_export' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'reqmo_pilot', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'reqmo_pilot' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_exporter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_frequent_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'qualification_menu', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'qualification_menu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'affichage_reglementaire_attestation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'affichage_reglementaire_registre' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_frequent_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_frequent_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter_autre_que_retour_avis' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_mes_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_date_reception' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_lu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_om_fichier_consultation_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_tous_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'delegataire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_attestation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_registre_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_autre_dossier_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_autre_dossier_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_dossier_encours_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_dossier_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_nouveau_dossier_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandes_avis_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandes_avis_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandeur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandeur_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandeur_modifier_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_add', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_add' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_uid_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_view', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_view' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_ajouter_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_modifier_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_modifier_lu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_possible', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_possible' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_localiser-sig-externe' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_modifier_lu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_qualifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_qualifier_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_evenement_incomplet_majoration' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_limites' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_limites_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_modification_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_modification_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_om_fichier_instruction_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_bordereaux' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_envoi_lettre_rar' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_transferer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_autorisation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_export' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_guichet_unique' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_instruction_dashboard' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_suivi' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'messages_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'messages_mes_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'messages_tous_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'password', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'password' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_modifier_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'profil_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'profil_instructeur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'qualification_menu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'qualification_menu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_edition' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'recherche_direct', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'recherche_direct' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'recherche_direct_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'recherche_direct_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'reqmo_pilot', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'reqmo_pilot' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'suivi_retours_de_consultation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'versement_archives' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_om_fichier_rapport_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_om_fichier_rapport_instruction_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_definaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_definaliser_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_finaliser_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_finaliser_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_lu_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_lu_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_ajouter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_ajouter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_supprimer_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_supprimer_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_tab_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_tab_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_geolocalisation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_geolocalisation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_geolocalisation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_geolocalisation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_donnees_techniques_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_donnees_techniques_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_geolocalisation_consulter_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_geolocalisation_consulter_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_modifier_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_modifier_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_rapport_instruction_rediger_bypass', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_rapport_instruction_rediger_bypass' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_exporter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_exporter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_consulter_autre_que_retour_avis', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_consulter_autre_que_retour_avis' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_mes_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_date_reception', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_date_reception' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'affichage_reglementaire_attestation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'affichage_reglementaire_attestation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'affichage_reglementaire_registre', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'affichage_reglementaire_registre' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_frequent_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_frequent_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'architecte_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'architecte_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'blocnote', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'blocnote' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_modifier_lu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_om_fichier_consultation_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_om_fichier_consultation_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'consultation_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'consultation_tous_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'delegataire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'delegataire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_attestation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_attestation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_affichage_reglementaire_registre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_affichage_reglementaire_registre_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_autre_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_autre_dossier_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_autre_dossier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_autre_dossier_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_dossier_encours_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_dossier_encours_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_dossier_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_dossier_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_nouveau_dossier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_nouveau_dossier_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandes_avis_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandes_avis_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demande_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demande_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandeur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'demandeur_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'demandeur_modifier_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_add', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_add' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_uid_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'document_numerise_view', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'document_numerise_view' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_autorisation_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_autorisation_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_ajouter_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_ajouter_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_modifier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_modifier_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_modifier_lu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_commission_possible', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_commission_possible' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_contrainte_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_document_numerise', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_document_numerise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_localiser-sig-externe', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_localiser-sig-externe' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_clotures_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_clotures_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_clotures_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_instruction_tous_encours_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_tous_encours_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_message_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_message_modifier_lu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_message_modifier_lu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_qualifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_qualifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_qualifier_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_qualifier_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_evenement_incomplet_majoration', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_evenement_incomplet_majoration' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_limites', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_limites' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossiers_limites_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossiers_limites_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'geolocalisation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'geolocalisation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_modification_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_modification_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_om_fichier_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_om_fichier_instruction_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_bordereaux', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_bordereaux' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_envoi_lettre_rar', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_envoi_lettre_rar' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_suivi_mise_a_jour_des_dates', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_suivi_mise_a_jour_des_dates' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'lot_transferer', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'lot_transferer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_autorisation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_autorisation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_export', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_export' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_guichet_unique', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_guichet_unique' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_instruction', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_instruction' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_instruction_dashboard', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_instruction_dashboard' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'menu_suivi' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'messages_mes_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'messages_mes_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'messages_tous_retours', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'messages_tous_retours' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'password', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'password' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_modifier_frequent', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_modifier_frequent' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'petitionnaire_frequent_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'petitionnaire_frequent_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'profil_instructeur', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'profil_instructeur' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'qualification_menu', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'qualification_menu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_definaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_edition', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_edition' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_rediger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_rediger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'recherche_direct', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'recherche_direct' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'recherche_direct_tab', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'recherche_direct_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'reqmo_pilot', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'reqmo_pilot' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'suivi_retours_de_consultation', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'suivi_retours_de_consultation' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'versement_archives', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'versement_archives' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'rapport_instruction_om_fichier_rapport_instruction_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'rapport_instruction_om_fichier_rapport_instruction_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'instruction_bordereau_envoi_maire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'instruction_bordereau_envoi_maire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'bordereau_envoi_maire', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'bordereau_envoi_maire' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_consulter', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'donnees_techniques_modifier', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'dossier_export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'dossier_export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil) SELECT nextval('om_droit_seq'),'export_sitadel', (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS ( 
       SELECT om_droit FROM om_droit WHERE libelle = 'export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );

-- Ajout du droit pour les exports SITADEL pour les profils administrateur général, 
-- guichet et suivi, instructeur polyvalent, instructeur polyvalent commune et la 
-- cellule suivi
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'sitadel_export_sitadel',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'sitadel_export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'sitadel_export_sitadel',(SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'sitadel_export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET ET SUIVI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'sitadel_export_sitadel',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'sitadel_export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'sitadel_export_sitadel',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'sitadel_export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'sitadel_export_sitadel',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'sitadel_export_sitadel' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );