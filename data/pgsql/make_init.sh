#! /bin/sh
##
# Ce script permet de générer les fichiers sql d'initialisation de la base de
# données pour permettre de publier une nouvelle version facilement
#
# @package openexemple
# @version SVN : $Id: make_init.sh 3255 2015-01-23 10:12:46Z vpihour $
##

# Génération du fichier init.sql
# ce fichier doit être récupéré tel quel d'openmairie_exemple sauf dans le cas 
# d'une version non finalisée d'OM exemple
sudo su postgres -c "pg_dump --column-inserts -U postgres -s -O -n openads -t openads.om_* openads" > init.sql

# Génération du fichier init_metier.sql
sudo su postgres -c "pg_dump --column-inserts -U postgres -s -O -n openads -T openads.om_* openads" > init_metier.sql

# Génération de la version de l'application
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.om_version openads" > init_version.sql

# Génération des fichiers de paramétrage
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.bible openads" > init_parametrage_bible.sql
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.demande_nature -t openads.demande_type openads -t openads.lien_demande_type_etat" > init_parametrage_demandes.sql
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.genre -t openads.groupe -t openads.autorite_competente -t openads.dossier_autorisation_type -t openads.dossier_autorisation_type_detaille -t openads.cerfa -t openads.dossier_instruction_type -t openads.lien_om_profil_groupe -t openads.lien_om_utilisateur_groupe openads" > init_parametrage_dossiers.sql
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.om_requete -t openads.om_etat -t openads.om_lettretype -t openads.om_logo -t openads.om_sousetat openads" > init_parametrage_editions.sql
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.om_droit openads" > init_parametrage_permissions.sql
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.document_numerise_type -t openads.document_numerise_type_categorie openads" > init_parametrage_numerisation.sql
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.om_collectivite -t openads.om_parametre -t openads.om_profil -t openads.om_widget -t openads.om_dashboard -t openads.civilite -t openads.avis_consultation openads" > init_parametrage.sql
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.etat_dossier_autorisation -t openads.etat -t openads.action -t openads.evenement -t openads.lien_dossier_instruction_type_evenement -t openads.avis_decision -t openads.transition openads" > init_parametrage_workflows.sql

# Génération du fichier init_data.sql
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.om_utilisateur -t openads.direction -t openads.division -t openads.instructeur_qualite -t openads.instructeur -t openads.architecte openads" > init_data.sql

# Génération du fichier init_data_complement.sql
sudo su postgres -c "pg_dump --column-inserts -U postgres -a -t openads.service -t openads.donnees_techniques -t openads.document_numerise -t openads.consultation -t openads.service_categorie -t openads.lien_service_om_utilisateur -t openads.lien_service_service_categorie -t openads.arrondissement -t openads.quartier -t openads.affectation_automatique -t openads.dossier_autorisation -t openads.dossier_autorisation_parcelle -t openads.dossier -t openads.dossier_parcelle -t openads.signataire_arrete -t openads.instruction -t openads.demande -t openads.demandeur -t openads.lien_demande_demandeur -t openads.lien_dossier_autorisation_demandeur -t openads.lien_dossier_demandeur -t openads.lot -t openads.lien_dossier_lot -t openads.lien_lot_demandeur -t openads.commission -t openads.commission_type -t openads.dossier_commission -t openads.contrainte -t openads.moyen_retenu_juge -t openads.lien_donnees_techniques_moyen_retenu_juge -t openads.moyen_souleve -t openads.lien_donnees_techniques_moyen_souleve -t openads.objet_recours -t openads.lien_dossier_dossier -t openads.lien_document_numerise_type_instructeur_qualite openads" > init_data_complement.sql

# Suppression du schéma
sed -i "s/CREATE SCHEMA openads;/-- CREATE SCHEMA openads;/g" init*.sql
sed -i "s/^SET/-- SET/g" init*.sql
