--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.3.0-b1
--
-- @package openfoncier
-- @version SVN : $Id: v3.0.0-b1.sql 1804 2013-04-30 14:45:17Z fmichon $
--------------------------------------------------------------------------------

-- ce script contient les modifications de la base version 2.xx
-- schema
-- SET search_path = openfoncier, pg_catalog;

INSERT INTO om_version (om_version) VALUES ('3.0.0-b1');

-- table action = parametrage en dehors du script
ALTER TABLE action ADD regle_etat VARCHAR( 60 );
ALTER TABLE action ADD regle_delai VARCHAR( 60 );
ALTER TABLE action ADD regle_accord_tacite VARCHAR( 60 );
ALTER TABLE action ADD regle_avis VARCHAR( 60 );
ALTER TABLE action ADD regle_date_limite VARCHAR( 60 );
ALTER TABLE action ADD regle_date_notification_delai VARCHAR( 60 );
ALTER TABLE action ADD regle_date_complet VARCHAR( 60 );
ALTER TABLE action ADD regle_date_validite VARCHAR( 60 );
ALTER TABLE action ADD regle_date_decision VARCHAR( 60 );
ALTER TABLE action ADD regle_date_chantier VARCHAR( 60 );
ALTER TABLE action ADD regle_date_achevement VARCHAR( 60 );
ALTER TABLE action ADD regle_date_conformite VARCHAR( 60 );
ALTER TABLE action ADD regle_date_rejet VARCHAR( 60 );
-- dossier servitude et description
ALTER TABLE dossier ADD servitude text;
ALTER TABLE dossier ADD description text;
ALTER TABLE dossier ADD parcelle_lot integer;
ALTER TABLE dossier ADD parcelle_lot_lotissement varchar(60) not null default '';
alter table dossier add objet_dossier_complement character varying(100);
-- parcelle
alter table parcelle drop sig;

-- creation de tables servitude_surfacique, servitude_ligne, servitude_point
CREATE TABLE servitude_surfacique (
  servitude_surfacique integer,
  libelle varchar(20),
  observation varchar(80),
  perimetre integer,
  description text,
  PRIMARY KEY  (servitude_surfacique)
);
CREATE TABLE servitude_ligne (
  servitude_ligne integer,
  libelle varchar(20),
  observation varchar(80),
  perimetre integer,
  description text,
  PRIMARY KEY  (servitude_ligne)
);
CREATE TABLE servitude_point (
  servitude_point integer,
  libelle varchar(20),
  observation varchar(80),
  perimetre integer,
  description text,
  PRIMARY KEY  (servitude_point)
CREATE TABLE parcelle_lot (
  parcelle_lot integer,
  lotissement varchar(50),
  numero varchar(16),
  surface numeric(10,3),
  PRIMARY KEY  (servitude_point)
);

-- creation des sequences servitude_surfacique, servitude_ligne, servitude_point
CREATE SEQUENCE servitude_surfacique_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
CREATE SEQUENCE servitude_ligne_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
CREATE SEQUENCE servitude_point_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
CREATE SEQUENCE parcelle_lot_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
  
  
  -- creation table statistique pour sitadel
CREATE TABLE statistique(
  statistique integer NOT NULL,
  parametre varchar(20) NOT NULL,
  valeur varchar(50) NOT NULL,
  dossier varchar(12) NOT NULL,
  PRIMARY KEY  (statistique)
);
CREATE TABLE parametre(
  parametre varchar(20) NOT NULL,
  libelle varchar(50) NOT NULL,
  actif char(3) NOT NULL,
  sitadel char(3),
  longueur_champ integer,
  type_champ varchar(20),
  valeur_autorisee varchar(50),
  PRIMARY KEY  (parametre)
);
CREATE SEQUENCE statistique_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- contraintes referentielles
ALTER TABLE ONLY statistique
    ADD CONSTRAINT statistique_parametre_fkey FOREIGN KEY (parametre) REFERENCES parametre(parametre);
ALTER TABLE ONLY statistique
    ADD CONSTRAINT statistique_dossier_fkey FOREIGN KEY (dossier) REFERENCES dossier(dossier);
    
-- destination shon
ALTER TABLE destination_shon ADD shon_anterieure float;
ALTER TABLE destination_shon ADD shon_demolie float;
ALTER TABLE destination_shon ADD shon_anterieure_supprimee float;
ALTER TABLE destination_shon ADD shon_nouvelle_transformee float;
ALTER TABLE destination_shon ADD shon_nouvelle float;
ALTER TABLE destination_shon ADD shon_shob_transformee float;


-- travaux mise solde a Non si vide -> obligatoire dans le choix travaux en maj
update travaux set solde = 'Non' where solde = '';
-- parametre nature = T Obligatoire pour evenement, travaux et bible
INSERT INTO nature VALUES ('T', 'Toutes');

