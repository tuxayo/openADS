--
-- START / Développement - Notification pièces numérisées / Ajout d'un message
--

-- Ajout du champ categorie pour la table dossier_message
ALTER TABLE dossier_message ADD COLUMN categorie  character varying(60);
COMMENT ON COLUMN dossier_message.categorie IS 'Carégorie du message (interne ou externe)';

-- Augmentation de la taille du champ emetteur pour la table dossier_message
ALTER TABLE dossier_message ALTER emetteur TYPE character varying(63);

-- Ajout de l'option pour ajouter un message à l'ajout d'une pièce numérisé
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'), 'option_notification_piece_numerisee', 'true', (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2')
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'option_notification_piece_numerisee'
    );

--
-- END / Développement - Notification pièces numérisées / Ajout d'un message
--

--
--  START - Evolution Filtre Paramétrable Widget 'Retours de messages' 
--
--
UPDATE om_widget SET lien='messages_retours' WHERE lien='messages_mes_retours';
--
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='INSTRUCTEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'messages_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='DIVISIONNAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'messages_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'messages_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'messages_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'messages_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='INSTRUCTEUR SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'messages_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'messages_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'messages_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'messages_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'messages_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
--
--  END - Evolution Filtre Paramétrable Widget 'Retours de consultation' 
--

--
--  START - Evolution CAPV - #114 — Mail aux communes
--

-- Ajout du paramètre objet du courriel pour la multicollectivité

INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'),'param_courriel_de_notification_commune_objet_depuis_instruction', '[openADS] Notification de finalisation d''un événement d''instruction', (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2')
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'param_courriel_de_notification_commune_objet_depuis_instruction' AND om_collectivite = (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2')
    );

-- Ajout du paramètre modèle du courriel pour la multicollectivité

INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
SELECT nextval('om_parametre_seq'),'param_courriel_de_notification_commune_modele_depuis_instruction', 'Bonjour,

Un nouvel événement d''instruction vient d''être finalisé concernant le dossier <DOSSIER_INSTRUCTION>. Le détail est disponible ici : 
<URL_INSTRUCTION>

Cordialement.', (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2')
WHERE
    NOT EXISTS (
        SELECT om_parametre FROM om_parametre WHERE libelle = 'param_courriel_de_notification_commune_modele_depuis_instruction' AND om_collectivite = (SELECT om_collectivite FROM om_collectivite WHERE niveau = '2')
    );

-- Ajout du droit de notification aux profils

-- INSTRUCTEUR POLYVALENT
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'instruction_notifier_commune',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_notifier_commune' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );

-- ADMINISTRATEUR GENERAL
INSERT INTO om_droit (om_droit, libelle, om_profil)
  SELECT nextval('om_droit_seq'),'instruction_notifier_commune',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
  WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_notifier_commune' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );

--
--  END - Evolution CAPV - #114 — Mail aux communes
--

--
-- START - #459 — Des lignes de données techniques vides sans aucune liaison remplissent ~10% de la table 
--

-- Suppression de la contrainte pour pouvoir dupliquer la ligne de données techniques, et
-- avoir temporairement 2 lignes liées au même DI.
ALTER TABLE "donnees_techniques"
DROP CONSTRAINT "donnees_techniques_dossier_instruction_unique";

-- Duplication des lignes de données techniques liées à un DA et un DI, en enlevant la
-- liaison au DA. La ligne copiée est donc liée au DI.
INSERT INTO donnees_techniques
SELECT 
nextval('donnees_techniques_seq'),
dossier_instruction,
lot,
am_lotiss,
am_autre_div,
am_camping,
am_caravane,
am_carav_duree,
am_statio,
am_statio_cont,
am_affou_exhau,
am_affou_exhau_sup,
am_affou_prof,
am_exhau_haut,
am_coupe_abat,
am_prot_plu,
am_prot_muni,
am_mobil_voyage,
am_aire_voyage,
am_rememb_afu,
am_parc_resid_loi,
am_sport_moto,
am_sport_attrac,
am_sport_golf,
am_mob_art,
am_modif_voie_esp,
am_plant_voie_esp,
am_chem_ouv_esp,
am_agri_peche,
am_crea_voie,
am_modif_voie_exist,
am_crea_esp_sauv,
am_crea_esp_class,
am_projet_desc,
am_terr_surf,
am_tranche_desc,
am_lot_max_nb,
am_lot_max_shon,
am_lot_cstr_cos,
am_lot_cstr_plan,
am_lot_cstr_vente,
am_lot_fin_diff,
am_lot_consign,
am_lot_gar_achev,
am_lot_vente_ant,
am_empl_nb,
am_tente_nb,
am_carav_nb,
am_mobil_nb,
am_pers_nb,
am_empl_hll_nb,
am_hll_shon,
am_periode_exploit,
am_exist_agrand,
am_exist_date,
am_exist_num,
am_exist_nb_avant,
am_exist_nb_apres,
am_coupe_bois,
am_coupe_parc,
am_coupe_align,
am_coupe_ess,
am_coupe_age,
am_coupe_dens,
am_coupe_qual,
am_coupe_trait,
am_coupe_autr,
co_archi_recours,
co_cstr_nouv,
co_cstr_exist,
co_cloture,
co_elec_tension,
co_div_terr,
co_projet_desc,
co_anx_pisc,
co_anx_gara,
co_anx_veran,
co_anx_abri,
co_anx_autr,
co_anx_autr_desc,
co_tot_log_nb,
co_tot_ind_nb,
co_tot_coll_nb,
co_mais_piece_nb,
co_mais_niv_nb,
co_fin_lls_nb,
co_fin_aa_nb,
co_fin_ptz_nb,
co_fin_autr_nb,
co_fin_autr_desc,
co_mais_contrat_ind,
co_uti_pers,
co_uti_vente,
co_uti_loc,
co_uti_princ,
co_uti_secon,
co_resid_agees,
co_resid_etud,
co_resid_tourism,
co_resid_hot_soc,
co_resid_soc,
co_resid_hand,
co_resid_autr,
co_resid_autr_desc,
co_foyer_chamb_nb,
co_log_1p_nb,
co_log_2p_nb,
co_log_3p_nb,
co_log_4p_nb,
co_log_5p_nb,
co_log_6p_nb,
co_bat_niv_nb,
co_trx_exten,
co_trx_surelev,
co_trx_nivsup,
co_demont_periode,
co_sp_transport,
co_sp_enseign,
co_sp_act_soc,
co_sp_ouvr_spe,
co_sp_sante,
co_sp_culture,
co_statio_avt_nb,
co_statio_apr_nb,
co_statio_adr,
co_statio_place_nb,
co_statio_tot_surf,
co_statio_tot_shob,
co_statio_comm_cin_surf,
su_avt_shon1,
su_avt_shon2,
su_avt_shon3,
su_avt_shon4,
su_avt_shon5,
su_avt_shon6,
su_avt_shon7,
su_avt_shon8,
su_avt_shon9,
su_cstr_shon1,
su_cstr_shon2,
su_cstr_shon3,
su_cstr_shon4,
su_cstr_shon5,
su_cstr_shon6,
su_cstr_shon7,
su_cstr_shon8,
su_cstr_shon9,
su_trsf_shon1,
su_trsf_shon2,
su_trsf_shon3,
su_trsf_shon4,
su_trsf_shon5,
su_trsf_shon6,
su_trsf_shon7,
su_trsf_shon8,
su_trsf_shon9,
su_chge_shon1,
su_chge_shon2,
su_chge_shon3,
su_chge_shon4,
su_chge_shon5,
su_chge_shon6,
su_chge_shon7,
su_chge_shon8,
su_chge_shon9,
su_demo_shon1,
su_demo_shon2,
su_demo_shon3,
su_demo_shon4,
su_demo_shon5,
su_demo_shon6,
su_demo_shon7,
su_demo_shon8,
su_demo_shon9,
su_sup_shon1,
su_sup_shon2,
su_sup_shon3,
su_sup_shon4,
su_sup_shon5,
su_sup_shon6,
su_sup_shon7,
su_sup_shon8,
su_sup_shon9,
su_tot_shon1,
su_tot_shon2,
su_tot_shon3,
su_tot_shon4,
su_tot_shon5,
su_tot_shon6,
su_tot_shon7,
su_tot_shon8,
su_tot_shon9,
su_avt_shon_tot,
su_cstr_shon_tot,
su_trsf_shon_tot,
su_chge_shon_tot,
su_demo_shon_tot,
su_sup_shon_tot,
su_tot_shon_tot,
dm_constr_dates,
dm_total,
dm_partiel,
dm_projet_desc,
dm_tot_log_nb,
tax_surf_tot,
tax_surf,
tax_surf_suppr_mod,
tax_su_princ_log_nb1,
tax_su_princ_log_nb2,
tax_su_princ_log_nb3,
tax_su_princ_log_nb4,
tax_su_princ_log_nb_tot1,
tax_su_princ_log_nb_tot2,
tax_su_princ_log_nb_tot3,
tax_su_princ_log_nb_tot4,
tax_su_princ_surf1,
tax_su_princ_surf2,
tax_su_princ_surf3,
tax_su_princ_surf4,
tax_su_princ_surf_sup1,
tax_su_princ_surf_sup2,
tax_su_princ_surf_sup3,
tax_su_princ_surf_sup4,
tax_su_heber_log_nb1,
tax_su_heber_log_nb2,
tax_su_heber_log_nb3,
tax_su_heber_log_nb_tot1,
tax_su_heber_log_nb_tot2,
tax_su_heber_log_nb_tot3,
tax_su_heber_surf1,
tax_su_heber_surf2,
tax_su_heber_surf3,
tax_su_heber_surf_sup1,
tax_su_heber_surf_sup2,
tax_su_heber_surf_sup3,
tax_su_secon_log_nb,
tax_su_tot_log_nb,
tax_su_secon_log_nb_tot,
tax_su_tot_log_nb_tot,
tax_su_secon_surf,
tax_su_tot_surf,
tax_su_secon_surf_sup,
tax_su_tot_surf_sup,
tax_ext_pret,
tax_ext_desc,
tax_surf_tax_exist_cons,
tax_log_exist_nb,
tax_am_statio_ext,
tax_sup_bass_pisc,
tax_empl_ten_carav_mobil_nb,
tax_empl_hll_nb,
tax_eol_haut_nb,
tax_pann_volt_sup,
tax_am_statio_ext_sup,
tax_sup_bass_pisc_sup,
tax_empl_ten_carav_mobil_nb_sup,
tax_empl_hll_nb_sup,
tax_eol_haut_nb_sup,
tax_pann_volt_sup_sup,
tax_trx_presc_ppr,
tax_monu_hist,
tax_comm_nb,
tax_su_non_habit_surf1,
tax_su_non_habit_surf2,
tax_su_non_habit_surf3,
tax_su_non_habit_surf4,
tax_su_non_habit_surf5,
tax_su_non_habit_surf6,
tax_su_non_habit_surf7,
tax_su_non_habit_surf_sup1,
tax_su_non_habit_surf_sup2,
tax_su_non_habit_surf_sup3,
tax_su_non_habit_surf_sup4,
tax_su_non_habit_surf_sup5,
tax_su_non_habit_surf_sup6,
tax_su_non_habit_surf_sup7,
vsd_surf_planch_smd,
vsd_unit_fonc_sup,
vsd_unit_fonc_constr_sup,
vsd_val_terr,
vsd_const_sxist_non_dem_surf,
vsd_rescr_fisc,
pld_val_terr,
pld_const_exist_dem,
pld_const_exist_dem_surf,
code_cnil,
terr_juri_titul,
terr_juri_lot,
terr_juri_zac,
terr_juri_afu,
terr_juri_pup,
terr_juri_oin,
terr_juri_desc,
terr_div_surf_etab,
terr_div_surf_av_div,
doc_date,
doc_tot_trav,
doc_tranche_trav,
doc_tranche_trav_desc,
doc_surf,
doc_nb_log,
doc_nb_log_indiv,
doc_nb_log_coll,
doc_nb_log_lls,
doc_nb_log_aa,
doc_nb_log_ptz,
doc_nb_log_autre,
daact_date,
daact_date_chgmt_dest,
daact_tot_trav,
daact_tranche_trav,
daact_tranche_trav_desc,
daact_surf,
daact_nb_log,
daact_nb_log_indiv,
daact_nb_log_coll,
daact_nb_log_lls,
daact_nb_log_aa,
daact_nb_log_ptz,
daact_nb_log_autre,
NULL as dossier_autorisation,
am_div_mun,
co_perf_energ,
architecte,
co_statio_avt_shob,
co_statio_apr_shob,
co_statio_avt_surf,
co_statio_apr_surf,
co_trx_amgt,
co_modif_aspect,
co_modif_struct,
co_ouvr_elec,
co_ouvr_infra,
co_trx_imm,
co_cstr_shob,
am_voyage_deb,
am_voyage_fin,
am_modif_amgt,
am_lot_max_shob,
mod_desc,
tr_total,
tr_partiel,
tr_desc,
avap_co_elt_pro,
avap_nouv_haut_surf,
avap_co_clot,
avap_aut_coup_aba_arb,
avap_ouv_infra,
avap_aut_inst_mob,
avap_aut_plant,
avap_aut_auv_elec,
tax_dest_loc_tr,
ope_proj_desc,
tax_surf_tot_cstr,
cerfa,
tax_surf_loc_stat,
tax_su_princ_surf_stat1,
tax_su_princ_surf_stat2,
tax_su_princ_surf_stat3,
tax_su_princ_surf_stat4,
tax_su_secon_surf_stat,
tax_su_heber_surf_stat1,
tax_su_heber_surf_stat2,
tax_su_heber_surf_stat3,
tax_su_tot_surf_stat,
tax_su_non_habit_surf_stat1,
tax_su_non_habit_surf_stat2,
tax_su_non_habit_surf_stat3,
tax_su_non_habit_surf_stat4,
tax_su_non_habit_surf_stat5,
tax_su_non_habit_surf_stat6,
tax_su_non_habit_surf_stat7,
tax_su_parc_statio_expl_comm_surf,
tax_log_ap_trvx_nb,
tax_am_statio_ext_cr,
tax_sup_bass_pisc_cr,
tax_empl_ten_carav_mobil_nb_cr,
tax_empl_hll_nb_cr,
tax_eol_haut_nb_cr,
tax_pann_volt_sup_cr,
tax_surf_loc_arch,
tax_surf_pisc_arch,
tax_am_statio_ext_arch,
tax_empl_ten_carav_mobil_nb_arch,
tax_empl_hll_nb_arch,
tax_eol_haut_nb_arch,
ope_proj_div_co,
ope_proj_div_contr,
tax_desc,
erp_cstr_neuve,
erp_trvx_acc,
erp_extension,
erp_rehab,
erp_trvx_am,
erp_vol_nouv_exist,
erp_loc_eff1,
erp_loc_eff2,
erp_loc_eff3,
erp_loc_eff4,
erp_loc_eff5,
erp_loc_eff_tot,
erp_public_eff1,
erp_public_eff2,
erp_public_eff3,
erp_public_eff4,
erp_public_eff5,
erp_public_eff_tot,
erp_perso_eff1,
erp_perso_eff2,
erp_perso_eff3,
erp_perso_eff4,
erp_perso_eff5,
erp_perso_eff_tot,
erp_tot_eff1,
erp_tot_eff2,
erp_tot_eff3,
erp_tot_eff4,
erp_tot_eff5,
erp_tot_eff_tot,
erp_class_cat,
erp_class_type,
tax_surf_abr_jard_pig_colom,
tax_su_non_habit_abr_jard_pig_colom
FROM donnees_techniques
WHERE dossier_instruction NOTNULL AND dossier_autorisation NOTNULL;

-- On supprime la liaison au DI pour la ligne initiale, toujours liée au DA et au DI
UPDATE donnees_techniques SET dossier_instruction = NULL WHERE dossier_instruction NOTNULL AND dossier_autorisation NOTNULL;

-- Suppression des lignes vides
DELETE FROM donnees_techniques WHERE dossier_instruction IS NULL AND dossier_autorisation IS NULL;

-- Ré-application de la contrainte
ALTER TABLE "donnees_techniques"
ADD CONSTRAINT "donnees_techniques_dossier_instruction_unique" UNIQUE ("dossier_instruction");

--
-- END - #459 — Des lignes de données techniques vides sans aucune liaison remplissent ~10% de la table 
--

--
--  START - Evolution Filtre Paramétrable Widget 'Retours de consultation' 
--
--
UPDATE om_widget SET lien='consultation_retours' WHERE lien='consultation_mes_retours';
--
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='INSTRUCTEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='DIVISIONNAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='INSTRUCTEUR SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR SERVICE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR GENERAL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR GENERAL')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='INSTRUCTEUR POLYVALENT COMMUNE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT COMMUNE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), 'consultation_retours_ma_division', (SELECT om_profil FROM om_profil WHERE libelle='INSTRUCTEUR POLYVALENT')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_retours_ma_division' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR POLYVALENT')
    );
--
--  END - Evolution Filtre Paramétrable Widget 'Retours de consultation' 
--


