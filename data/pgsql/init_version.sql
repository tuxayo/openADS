--
-- PostgreSQL database dump
--

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SET check_function_bodies = false;
-- SET client_min_messages = warning;

-- SET search_path = openads, pg_catalog;

--
-- Data for Name: om_version; Type: TABLE DATA; Schema: openads; Owner: postgres
--

INSERT INTO om_version (om_version) VALUES ('4.4.0');


--
-- PostgreSQL database dump complete
--

