--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a12
--
-- XXX Ce fichier doit être renommé en v3.14.0-a12.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.14.0-a12.sql 3080 2014-06-18 16:11:48Z vpihour $
--------------------------------------------------------------------------------

-- Mise à jour des clés primaires alphanumériques
UPDATE action SET action=trim(both ' ' FROM action);
UPDATE dossier SET dossier=trim(both ' ' FROM dossier);
UPDATE dossier_autorisation SET dossier_autorisation=trim(both ' ' FROM dossier_autorisation);
UPDATE etat SET etat=trim(both ' ' FROM etat);
UPDATE parcelle SET parcelle=trim(both ' ' FROM parcelle);
UPDATE pos SET pos=trim(both ' ' FROM pos);
UPDATE proprietaire SET proprietaire=trim(both ' ' FROM proprietaire);
UPDATE rivoli SET rivoli=trim(both ' ' FROM rivoli);
