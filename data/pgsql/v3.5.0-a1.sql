--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.5.0-a1
--
-- @package openfoncier
-- @version SVN : $Id: v3.5.0-a1.sql 1970 2013-05-30 21:53:25Z fmichon $
--------------------------------------------------------------------------------

-- Ajout du champ de description des modifications (demande de modification d'un permis en cours de validité)
ALTER TABLE cerfa ADD COLUMN mod_desc boolean;

ALTER TABLE donnees_techniques ADD COLUMN mod_desc text;

-- Correction des doublons dans la table lien_demande_type_etat_dossier_autorisation
DELETE FROM lien_demande_type_etat_dossier_autorisation as t1
WHERE EXISTS (SELECT * FROM lien_demande_type_etat_dossier_autorisation AS t2 
                             WHERE t1.lien_demande_type_etat_dossier_autorisation < t2.lien_demande_type_etat_dossier_autorisation
                               AND t1.demande_type = t2.demande_type
                               AND t1.etat_dossier_autorisation = t2.etat_dossier_autorisation);

CREATE UNIQUE INDEX lien_demande_type_etat_dossier_autorisation_uk ON lien_demande_type_etat_dossier_autorisation (demande_type,etat_dossier_autorisation);



-- Mise à jour des champs archive_date_dernier_depot pour les champs dont la valeur est null
UPDATE instruction
SET archive_date_dernier_depot = dossier.date_depot
FROM dossier
WHERE instruction.dossier = dossier.dossier
AND instruction.archive_date_dernier_depot IS NULL;



--Ajout de la table document_numerise_type_categorie
CREATE TABLE document_numerise_type_categorie(
    document_numerise_type_categorie integer NOT NULL,
    libelle character varying(255) NOT NULL,
    CONSTRAINT document_numerise_type_categorie_pkey PRIMARY KEY (document_numerise_type_categorie)
);

CREATE SEQUENCE document_numerise_type_categorie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE document_numerise_type_categorie_seq OWNED BY document_numerise_type_categorie.document_numerise_type_categorie;

INSERT INTO document_numerise_type_categorie VALUES (nextval('document_numerise_type_categorie_seq'), 'Définition Générale');
INSERT INTO document_numerise_type_categorie VALUES (nextval('document_numerise_type_categorie_seq'), 'Rendu/Insertion');
INSERT INTO document_numerise_type_categorie VALUES (nextval('document_numerise_type_categorie_seq'), 'Accessibilité/Sécurité');
INSERT INTO document_numerise_type_categorie VALUES (nextval('document_numerise_type_categorie_seq'), 'Autre');
INSERT INTO document_numerise_type_categorie VALUES (nextval('document_numerise_type_categorie_seq'), 'Arrêté');
INSERT INTO document_numerise_type_categorie VALUES (nextval('document_numerise_type_categorie_seq'), 'Avis Obligatoires');
INSERT INTO document_numerise_type_categorie VALUES (nextval('document_numerise_type_categorie_seq'), 'Document');
INSERT INTO document_numerise_type_categorie VALUES (nextval('document_numerise_type_categorie_seq'), 'Daact');
INSERT INTO document_numerise_type_categorie VALUES (nextval('document_numerise_type_categorie_seq'), 'Dossier Complet');

--Modification table document_numerise
ALTER TABLE document_numerise ADD COLUMN nom_fichier character varying(255) NOT NULL;
ALTER TABLE document_numerise ADD COLUMN date_creation date NOT NULL;
ALTER TABLE document_numerise ADD COLUMN document_numerise_type integer NOT NULL;

ALTER TABLE document_numerise ADD CONSTRAINT document_numerise_dossier_nom_fichier_key UNIQUE (dossier,nom_fichier);

ALTER TABLE document_numerise ADD CONSTRAINT document_numerise_document_numerise_type_fkey FOREIGN KEY (document_numerise_type)
REFERENCES document_numerise_type (document_numerise_type) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

--Modification table document_numerise_type
ALTER TABLE document_numerise_type ADD COLUMN document_numerise_type_categorie integer;

UPDATE document_numerise_type SET document_numerise_type_categorie = 4;
UPDATE document_numerise_type SET document_numerise_type_categorie = 1
    WHERE code like 'DG%';
UPDATE document_numerise_type SET document_numerise_type_categorie = 2
    WHERE code like 'RIP%';
UPDATE document_numerise_type SET document_numerise_type_categorie = 3
    WHERE code like 'ASP%';
UPDATE document_numerise_type SET document_numerise_type_categorie = 4
    WHERE code like 'AUT%';
UPDATE document_numerise_type SET document_numerise_type_categorie = 5
    WHERE code like 'AR%' OR code = 'APA';
UPDATE document_numerise_type SET document_numerise_type_categorie = 6
    WHERE code = 'AVIS';
UPDATE document_numerise_type SET document_numerise_type_categorie = 7
    WHERE code = 'DOC';
UPDATE document_numerise_type SET document_numerise_type_categorie = 8
    WHERE code = 'DAACT';
UPDATE document_numerise_type SET document_numerise_type_categorie = 9
    WHERE code like 'DOS%';

ALTER TABLE document_numerise_type ALTER COLUMN document_numerise_type_categorie SET NOT NULL;

ALTER TABLE document_numerise_type ADD CONSTRAINT document_numerise_type_document_numerise_type_categorie_fkey FOREIGN KEY (document_numerise_type_categorie)
REFERENCES document_numerise_type_categorie (document_numerise_type_categorie) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

-- Agrandissement des champs personne_morale_denomination et personne_morale_raison_sociale de la table demandeur
ALTER TABLE demandeur ALTER COLUMN personne_morale_denomination TYPE varchar(40);
ALTER TABLE demandeur ALTER COLUMN personne_morale_raison_sociale TYPE varchar(40);

-- Modification du widget profil_instructeur (type web -> type file)
insert into om_droit(om_droit, libelle, om_profil) values
(nextval('om_droit_seq'), 'profil_instructeur', 3);
update om_widget set type='file', lien='profil_instructeur', texte='' where libelle='Profil instructeur';

-- Droits sur document_numerise
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'document_numerise_tab', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'document_numerise_view', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'document_numerise_ajouter', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'document_numerise_uid_telecharger', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'document_numerise_tab', 4);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'document_numerise_view', 4);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'document_numerise_uid_telecharger', 4);

-- Modification des tables consultation, instruction, commission et rapport d'instruction
ALTER TABLE instruction ADD COLUMN om_fichier_instruction character varying(64);
ALTER TABLE instruction ADD COLUMN om_final_instruction boolean;

ALTER TABLE consultation ADD COLUMN om_fichier_consultation character varying(64);
ALTER TABLE consultation ADD COLUMN om_final_consultation boolean;

ALTER TABLE commission ADD COLUMN om_fichier_commission_ordre_jour character varying(64);
ALTER TABLE commission ADD COLUMN om_final_commission_ordre_jour boolean;
ALTER TABLE commission ADD COLUMN om_fichier_commission_compte_rendu character varying(64);
ALTER TABLE commission ADD COLUMN om_final_commission_compte_rendu boolean;

ALTER TABLE rapport_instruction ADD COLUMN om_fichier_rapport_instruction character varying(64);
ALTER TABLE rapport_instruction ADD COLUMN om_final_rapport_instruction boolean;

-- Droits pour la finalisation
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'rapport_instruction_finaliser', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'rapport_instruction_definaliser', 3);

INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'dossier_instruction_mes_encours_finaliser', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'dossier_instruction_mes_encours_definaliser', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'instruction_om_fichier_instruction_telecharger', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'instruction_om_fichier_instruction_telecharger', 6);

INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'consultation_om_fichier_consultation_telecharger', 3);

INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'commission_om_fichier_commission_ordre_jour_telecharger', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'commission_om_fichier_commission_compte_rendu_telecharger', 3);
-- Ajout de paramètre pour les métadonnées
INSERT INTO om_parametre VALUES (nextval('om_parametre_seq'), 'code_produit', 'OpenADS', 1);

-- Ajout d'un champ contenant l'uid de l'arrêté dans la table instruction
ALTER TABLE instruction ADD COLUMN document_arrete character varying(64);
