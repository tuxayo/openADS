--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.3.0-a6
--
-- @package openfoncier
-- @version SVN : $Id: v3.3.0-a6.sql 1804 2013-04-30 14:45:17Z fmichon $
--------------------------------------------------------------------------------

ALTER TABLE cerfa ADD COLUMN co_archi_nom boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_prenom boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_adr_num boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_adr_voie boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_adr_lieu_dit boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_adr_localite boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_adr_cp boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_adr_bp boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_adr_cedex boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_no_incri boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_cg boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_tel1 boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_tel2 boolean;
ALTER TABLE cerfa ADD COLUMN co_archi_mail boolean;

ALTER TABLE donnees_techniques ADD COLUMN co_archi_nom character varying(80);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_prenom character varying(80);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_adr_num character varying(10);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_adr_voie character varying(80);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_adr_lieu_dit character varying(80);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_adr_localite character varying(80);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_adr_cp character varying(5);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_adr_bp character varying(3);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_adr_cedex character varying(2);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_no_incri character varying(40);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_cg character varying(80);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_tel1 character varying(10);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_tel2 character varying(10);
ALTER TABLE donnees_techniques ADD COLUMN co_archi_mail character varying(150);