--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.14.0-a1
--
-- XXX Ce fichier doit être renommé en v3.14.0-a1.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.14.0-a1.sql 2766 2014-02-21 17:40:24Z softime $
--------------------------------------------------------------------------------

--
-- Création de la table contrainte
--
CREATE TABLE contrainte (
    contrainte integer NOT NULL,
    numero character varying(250),
    nature character varying(10) NOT NULL,
    groupe character varying(250),
    sousgroupe character varying(250),
    libelle character varying(250) NOT NULL,
    texte text,
    no_ordre integer,
    reference boolean DEFAULT FALSE,
    service_consulte boolean DEFAULT FALSE,
    om_validite_debut date,
    om_validite_fin date
);
-- Séquence de la table contrainte
CREATE SEQUENCE contrainte_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE contrainte_seq OWNED BY 
    contrainte.contrainte;
-- Commentaires de la tabel contrainte
COMMENT ON TABLE contrainte IS 'Table de référence des contraintes';
COMMENT ON COLUMN contrainte.contrainte IS 'Identifiant unique';
COMMENT ON COLUMN contrainte.numero IS 'Numéro unique de contrainte, issu du SIG';
COMMENT ON COLUMN contrainte.nature IS 'Nature de la contrainte POS ou PLU';
COMMENT ON COLUMN contrainte.groupe IS 'Groupe de la contrainte';
COMMENT ON COLUMN contrainte.sousgroupe IS 'Sous-groupe de la contrainte';
COMMENT ON COLUMN contrainte.libelle IS 'Libellé de la contrainte';
COMMENT ON COLUMN contrainte.texte IS 'Texte de la contrainte';
COMMENT ON COLUMN contrainte.no_ordre IS 'Numéro d''ordre d''affichage ou d''impression';
COMMENT ON COLUMN contrainte.reference IS 'Contrainte récupérée depuis le SIG';
COMMENT ON COLUMN contrainte.service_consulte IS 'Contrainte affichée aux services consultés';
COMMENT ON COLUMN contrainte.om_validite_debut IS 'Date de début de validité';
COMMENT ON COLUMN contrainte.om_validite_fin IS 'Date de fin de validité';
-- Contraintes de la table contrainte
ALTER TABLE ONLY contrainte
    ADD CONSTRAINT contrainte_pkey PRIMARY KEY (contrainte);

--
-- Création de la table dossier_contrainte
--
CREATE TABLE dossier_contrainte (
    dossier_contrainte integer NOT NULL,
    dossier character varying(30) NOT NULL,
    contrainte integer NOT NULL,
    texte_complete text,
    reference boolean DEFAULT FALSE
);
-- Séquence de la table dossier_contrainte
CREATE SEQUENCE dossier_contrainte_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE dossier_contrainte_seq OWNED BY 
    dossier_contrainte.dossier_contrainte;
-- Commentaires de la table dossier_contrainte
COMMENT ON TABLE dossier_contrainte IS 'Table des contraintes appliquées au dossier';
COMMENT ON COLUMN dossier_contrainte.dossier_contrainte IS 'Identifiant unique';
COMMENT ON COLUMN dossier_contrainte.dossier IS 'Dossier auquel est appliqué la contrainte';
COMMENT ON COLUMN dossier_contrainte.contrainte IS 'Contrainte appliquée au dossier';
COMMENT ON COLUMN dossier_contrainte.texte_complete IS 'Texte complété de la contrainte';
COMMENT ON COLUMN dossier_contrainte.reference IS 'Texte complété récupéré depuis le SIG';
-- Contraintes de la table dossier_contrainte
ALTER TABLE ONLY dossier_contrainte
    ADD CONSTRAINT dossier_contrainte_pkey PRIMARY KEY (dossier_contrainte);
ALTER TABLE ONLY dossier_contrainte
    ADD CONSTRAINT dossier_contrainte_dossier_fkey FOREIGN KEY (dossier) REFERENCES dossier(dossier);
ALTER TABLE ONLY dossier_contrainte
    ADD CONSTRAINT dossier_contrainte_contrainte_fkey FOREIGN KEY (contrainte) REFERENCES contrainte(contrainte);

--
-- Ajout des droits pour dossier_contrainte
--
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_contrainte',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_contrainte',(SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_contrainte',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );

-- Ajout d'index pour accélérer l'affichage des dossiers
CREATE INDEX donnees_techniques_dossier_instruction_idx ON donnees_techniques(dossier_instruction);
CREATE INDEX dossier_min_date_decision_idx ON dossier(date_decision);
CREATE INDEX lien_dossier_demandeur_dossier_idx ON lien_dossier_demandeur(dossier, petitionnaire_principal);
CREATE INDEX lien_dossier_demandeur_demandeur_idx ON lien_dossier_demandeur(demandeur);
CREATE INDEX dossier_parcelle_dossier_idx ON dossier_parcelle(dossier);
CREATE INDEX dossier_date_depot_idx ON dossier(date_depot, dossier);
CREATE INDEX dossier_date_depot_desc_idx ON dossier(date_depot DESC NULLS LAST, dossier);

--
-- Ajout des droits pour le profil 'QUALIFICATEUR'
--
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'instruction_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'instruction_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'consultation_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'consultation_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_contrainte',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_contrainte' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'donnees_techniques',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'donnees_techniques' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'qualification_menu',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'qualification_menu' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_finaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_finaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_mes_encours_definaliser',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_mes_encours_definaliser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'menu_qualification_dashboard',(SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'menu_qualification_dashboard' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'QUALIFICATEUR')
    );

--
-- Ajout du droit de visualiser le tableau des dossiers d'instruction dans 
-- l'onglet DA d'un DI
--
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'INSTRUCTEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'GUICHET UNIQUE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CELLULE SUIVI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'VISUALISATION DA et DI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'DIVISIONNAIRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CHEF DE SERVICE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'dossier_instruction_display_da_di_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_display_da_di_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR FONCTIONNEL')
    );

--
-- Ajout d'un paramètre dans la table om_parametre pour l'affichage 
--
INSERT INTO om_parametre VALUES (nextval('om_parametre_seq'), 'option_contrainte_di', 'aucune', 1);