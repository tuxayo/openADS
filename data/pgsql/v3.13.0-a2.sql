--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v3.13.0-a2
--
-- XXX Ce fichier doit être renommé en v3.13.0-a2.sql au moment de la release
--
-- @package openfoncier
-- @version SVN : $Id: v3.13.0-a2.sql 2684 2014-01-13 14:13:11Z softime $
--------------------------------------------------------------------------------

-- Suppression des marges des sous-états
ALTER TABLE om_etat DROP COLUMN se_margeleft;
ALTER TABLE om_etat DROP COLUMN se_margetop;
ALTER TABLE om_etat DROP COLUMN se_margeright;

ALTER TABLE om_lettretype DROP COLUMN se_margeleft;
ALTER TABLE om_lettretype DROP COLUMN se_margetop;
ALTER TABLE om_lettretype DROP COLUMN se_margeright;

-- Transformation des entitiés html
UPDATE om_etat set corps_om_htmletatex = replace(corps_om_htmletatex, '&lt;', '<');
UPDATE om_etat set titre_om_htmletat = replace(titre_om_htmletat, '&lt;', '<');
UPDATE om_lettretype set corps_om_htmletatex = replace(corps_om_htmletatex, '&lt;', '<');
UPDATE om_lettretype set titre_om_htmletat = replace(titre_om_htmletat, '&lt;', '<');

UPDATE om_etat set corps_om_htmletatex = replace(corps_om_htmletatex, '&gt;', '>');
UPDATE om_etat set titre_om_htmletat = replace(titre_om_htmletat, '&gt;', '>');
UPDATE om_lettretype set corps_om_htmletatex = replace(corps_om_htmletatex, '&gt;', '>');
UPDATE om_lettretype set titre_om_htmletat = replace(titre_om_htmletat, '&gt;', '>');

UPDATE om_etat set corps_om_htmletatex = replace(corps_om_htmletatex, '&quot;', '"');
UPDATE om_etat set titre_om_htmletat = replace(titre_om_htmletat, '&quot;', '"');
UPDATE om_lettretype set corps_om_htmletatex = replace(corps_om_htmletatex, '&quot;', '"');
UPDATE om_lettretype set titre_om_htmletat = replace(titre_om_htmletat, '&quot;', '"');

UPDATE om_etat set corps_om_htmletatex = replace(corps_om_htmletatex, '&amp;', '&');
UPDATE om_etat set titre_om_htmletat = replace(titre_om_htmletat, '&amp;', '&');
UPDATE om_lettretype set corps_om_htmletatex = replace(corps_om_htmletatex, '&amp;', '&');
UPDATE om_lettretype set titre_om_htmletat = replace(titre_om_htmletat, '&amp;', '&');

UPDATE om_etat set titre_om_htmletat = replace(titre_om_htmletat, '<b>', '<span style="font-weight: bold;">');
UPDATE om_etat set titre_om_htmletat = replace(titre_om_htmletat, '</b>', '</span>');
UPDATE om_etat set corps_om_htmletatex = replace(corps_om_htmletatex, '<b>', '<span style="font-weight: bold;">');
UPDATE om_etat set corps_om_htmletatex = replace(corps_om_htmletatex, '</b>', '</span>');
UPDATE om_lettretype set titre_om_htmletat = replace(titre_om_htmletat, '<b>', '<span style="font-weight: bold;">');
UPDATE om_lettretype set titre_om_htmletat = replace(titre_om_htmletat, '</b>', '</span>');
UPDATE om_lettretype set corps_om_htmletatex = replace(corps_om_htmletatex, '<b>', '<span style="font-weight: bold;">');
UPDATE om_lettretype set corps_om_htmletatex = replace(corps_om_htmletatex, '</b>', '</span>');

--
-- Agrandissement du champ abrege de la table service
-- 
ALTER TABLE service ALTER COLUMN abrege TYPE character varying(10);
