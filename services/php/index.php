<?php
/**
 * Ce fichier permet de faire une redirection vers le fichier index.php à la
 * racine de l'application.
 *
 * @package openfoncier
 * @version SVN : $Id: index.php 4418 2015-02-24 17:30:28Z tbenita $
 */

header("Location: ../index.php");
exit();

?>
