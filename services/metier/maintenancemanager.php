<?php
/**
 * Ce fichier permet de déclarer la classe MaintenanceManager, qui effectue les
 * traitements pour la ressource 'maintenance'.
 *
 * @package openfoncier
 * @version SVN : $Id: maintenancemanager.php 6129 2016-03-08 15:51:07Z stimezouaght $
 */

// Inclusion de la classe de base MetierManager
require_once("./metier/metiermanager.php");

/**
 * Cette classe hérite de la classe MetierManager. Elle permet d'effectuer des
 * traitements pour la ressource 'maintenance' qui ont vocation à être
 * déclenché de manière automatique ou par une application externe. Par
 * exemple, un cron permet chaque soir d'exécuter la synchronisation des
 * utilisateurs de l'application avec l'annuaire LDAP.
 */
class MaintenanceManager extends MetierManager {

    /**
     * @var mixed Ce tableau permet d'associer un module a une méthode,
     * le module (key) est celui reçu dans la requête REST, la méthode (value)
     * est la méthode de cette classe qui va exécuter le traitement
     */
    var $fptrs = array(
        'user' => 'synchronizeUsers',
        'consultation' => 'updateConsultationsStates',
        'instruction' => 'updateDossierTacite',
        'import' => 'importDigitalizedDocuments',
        'purge' => 'purgeDigitalizedDocuments',
        'update_dossier_autorisation' => 'updateDAState',
        'contrainte' => 'synchronisationContraintes',
        'maj_metadonnees_documents_numerises' => 'updateDigitalizedDocumentsMetadata'
    );

    /**
     * Cette méthode permet de gérer l'appel aux différentes méthodes
     * correspondantes au module appelé.
     *
     * @param string $module Le module à exécuter reçu dans la requête
     * @param mixed $data Les données reçues dans la requête
     * @return string Le résultat du traitement
     */
    public function performMaintenance($module, $data) {

        // Si le module n'existe pas dans la liste des modules disponibles
        // alors on ajoute un message d'informations et on retourne un
        // un résultat d'erreur
        if (!in_array($module, array_keys($this->fptrs))) {
            $this->setMessage("Le module demandé n'existe pas");
            return $this->BAD_DATA;
        }

        // Si le module existe dans la liste des modules disponibles
        // alors on appelle la méthode en question et on retourne son résultat
        return call_user_func(array($this, $this->fptrs[$module]), $data);

    }

    /**
     * Cette méthode permet d'effectuer la synchronisation des utilisateurs
     * de l'application avec l'annuaire LDAP paramétré dans l'application.
     * 
     * @param mixed $data Les données reçues dans la requête
     * @return string Le résultat du traitement
     *
     * @todo XXX Faire une getsion des erreurs dans om_application pour
     * permettre d'avoir un retour sur le nombre d'utilisateurs synchronisés
     */    
    public function synchronizeUsers($data) {

        // Si aucune configuration d'annuaire n'est présente, on retourne que
        // tout est OK et que la configuration 'annuaire n'est pas activée.
        if ($this->f->is_option_directory_enabled() !== true) {
            $this->setMessage("L'option 'annuaire' n'est pas configurée.");
            return $this->OK;
        }

        // Initialisation de la synchronisation des utilisateurs LDAP
        $results = $this->f->initSynchronization();

        // Si l'initialisation ne se déroule pas correctement alors on retourne
        // un résultat d'erreur
        if (is_null($results) || $results == false) {
            $this->setMessage("Erreur interne");
            return $this->KO;
        }

        // Application du traitement de synchronisation
        $ret = $this->f->synchronizeUsers($results);

        // Si l'a synchronisation ne se déroule pas correctement alors on
        // retourne un résultat d'erreur
        if ($ret == false) {
            $this->setMessage("Erreur interne");
            return $this->KO;
        }

        // Si l'a synchronisation ne se déroule correctement alors on
        // retourne un résultat OK
        $this->setMessage("Synchronisation terminée.");
        return $this->OK;

    }

    /**
     * Cette méthode permet d'effectuer le traitement de gestion des avis
     * tacites sur les consultations qui ont atteint leur date limite de
     * retour.
     * 
     * @param mixed $data Les données reçues dans la requête
     * @return string Le résultat du traitement
     *
     * @todo Vérifier si il est possible de déplacer la méthode dans
     * l'application
     */
    public function updateConsultationsStates($data) {

        // Nom de la table
        $table_name = 'consultation';

        // Récupération de la référence vers un avis_consultation correspondant
        // au libellé 'Tacite'
        $sql = "SELECT avis_consultation FROM ".DB_PREFIXE."avis_consultation ";
        $sql .= " WHERE libelle = 'Tacite' ";
        $sql .= " AND ((avis_consultation.om_validite_debut IS NULL ";
        $sql .= " AND (avis_consultation.om_validite_fin IS NULL ";
        $sql .= " OR avis_consultation.om_validite_fin > CURRENT_DATE)) ";
        $sql .= " OR (avis_consultation.om_validite_debut <= CURRENT_DATE ";
        $sql .= " AND (avis_consultation.om_validite_fin IS NULL ";
        $sql .= " OR avis_consultation.om_validite_fin > CURRENT_DATE))) ";
        $res = $this->db->query($sql);
        // Logger
        $this->addToLog("updateConsultationsStates(): db->query(\"".$sql."\"", VERBOSE_MODE);
        // Si une erreur de base de données se produit sur cette requête
        // alors on retourne un résultat d'erreur
        if ($this->f->isDatabaseError($res, true)) {
            $this->setMessage("Erreur de base de données.");
            return $this->KO;
        }

        // Récupération de la référence vers un avis_consultation correspondant
        // au libellé 'Tacite'
        $tacite = NULL;
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $tacite = $row["avis_consultation"];
        }
        $res->free();

        // Si la décision n'existe pas dans la base de données alors on ajoute
        // un message d'informations et on retourne un résultat d'erreur
        if (is_null($tacite)) {
            $this->setMessage("L'avis n'existe pas.");
            return $this->KO;
        }

        // Récupération de la liste des consultations correspondantes aux
        // critères du traitement
        $sql = "SELECT consultation, date_limite FROM ".DB_PREFIXE."$table_name ";
        $sql .= " WHERE date_limite < CURRENT_DATE" ;
        $sql .= " AND date_retour IS NULL AND avis_consultation IS NULL ";
        $res = $this->db->query($sql);
        // Logger
        $this->addToLog("updateConsultationsStates(): db->query(\"".$sql."\"", VERBOSE_MODE);
        // Si une erreur de base de données se produit sur cette requête
        // alors on retourne un résultat d'erreur
        if ($this->f->isDatabaseError($res, true)) {
            $this->setMessage("Erreur de base de données.");
            return $this->KO;
        }
        
        // Si aucune consultation n'est concernée par le traitement alors on
        // ajoute un message d'informations et on retourne un résultat 'OK'
        if ($res->numrows() == 0) {
            $this->setMessage("Aucune mise a jour.");
            return $this->OK;
        }
        
        $nb_consultations_maj = 0;
        //Pour chaque consultation correspondantes aux critères
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            $fields = array(
                'avis_consultation' => $tacite,
                'date_retour' => $row["date_limite"],
                'lu' => false,
            );
            //
            $res_update = $this->db->autoExecute(DB_PREFIXE.$table_name,
                                          $fields,
                                          DB_AUTOQUERY_UPDATE,
                                          'consultation = \''.$row["consultation"].'\'');
            // Logger
            $this->addToLog("updateConsultationsStates(): db->autoExecute(\"".DB_PREFIXE.$table_name."\", ".print_r($fields, true).", DB_AUTOQUERY_UPDATE, \"consultation = \"".$row["consultation"]."\"", VERBOSE_MODE);
            // Si une erreur de base de données se produit sur cette requête
            // alors on retourne un résultat d'erreur
            if ($this->f->isDatabaseError($res_update, true)) {
                $this->setMessage("Erreur de base de données.");
                return $this->KO;
            }
            //
            $nb_consultations_maj++;
        }
        // Tout s'est déroulé correctement alors on ajoute un message
        // d'informations et on retourne le résultat 'OK'
        $this->setMessage($nb_consultations_maj." consultations mise(s) à jour.");
        return $this->OK;

    }

    /**
     * Cette méthode permet d'ajouter une instruction sur les dossiers tacites : 
     * - soit les tacites normaux
     * - soit les incomplets ayant un événement tacites
     */
    public function updateDossierTacite() {
        // Liste les dossiers tacites
        // UNION
        // liste des dossiers incomplets tacites
        $sql_dossier_tacite = 
               "SELECT dossier.dossier, dossier.evenement_suivant_tacite
                FROM ".DB_PREFIXE."dossier
                LEFT JOIN ".DB_PREFIXE."instruction ON
                    instruction.dossier=dossier.dossier AND instruction.evenement = dossier.evenement_suivant_tacite
                WHERE dossier.date_limite < CURRENT_DATE
                        AND dossier.accord_tacite = 'Oui'
                        AND dossier.evenement_suivant_tacite IS NOT NULL
                        AND dossier.incomplet_notifie = FALSE
                        AND instruction.instruction IS NULL
                        AND dossier.avis_decision IS NULL
                UNION 
                SELECT dossier.dossier, dossier.evenement_suivant_tacite_incompletude as evenement_suivant_tacite
                FROM ".DB_PREFIXE."dossier
                LEFT JOIN ".DB_PREFIXE."instruction ON
                    instruction.dossier=dossier.dossier AND instruction.evenement = dossier.evenement_suivant_tacite
                WHERE dossier.date_limite_incompletude < CURRENT_DATE
                        AND dossier.accord_tacite = 'Oui'
                        AND dossier.evenement_suivant_tacite_incompletude IS NOT NULL
                        AND dossier.incomplet_notifie = TRUE
                        AND dossier.incompletude = TRUE
                        AND instruction.instruction IS NULL
                        AND dossier.avis_decision IS NULL";

        $res_dossier_tacite = $this->db->query($sql_dossier_tacite);
        if ($this->f->isDatabaseError($res_dossier_tacite, true)) {
            $this->setMessage("Erreur de base de données.");
            return $this->KO;
        }// Inclusion de la classe de base MetierManager
        $nb_maj = 0;
        while ($row_dossier_tacite =& $res_dossier_tacite->fetchRow(DB_FETCHMODE_ASSOC)) {
            // essai d'ajout des donnees dans la base de donnees
            $this->db->autoCommit(false);
            // Si un evenement est configuré suivant tacite
            $valNewInstr = array();
            require_once("../obj/instruction.class.php");
            $instruction = new instruction("]", $this->db, 0);
            // Création d'un tableau avec la liste des champs de l'instruction
            foreach($instruction->champs as $champ) {
                if($champ != "bible_auto") {
                    $valNewInstr[$champ] = ""; 
                }
            }

            // Définition des valeurs de la nouvelle instruction
            $valNewInstr["evenement"] = $row_dossier_tacite["evenement_suivant_tacite"];
            $valNewInstr["dossier"] = $row_dossier_tacite['dossier'];
            $valNewInstr["date_evenement"] = date("d/m/Y");
            $instruction->valF = array();
            $res_ajout = $instruction->ajouter($valNewInstr, $this->db, $this->DEBUG);
            if ($res_ajout === false) {
                $instruction->undoValidation();
                $this->setMessage("Erreur de base de données.");
                return $this->KO;
            } else {
                $this->db->commit();
                $nb_maj++;
            }

        }

        // Si aucune instruction n'est concernée par le traitement alors on
        // ajoute un message d'informations et on retourne un résultat 'OK'
        if ($nb_maj == 0) {
            $this->setMessage("Aucune mise a jour.");
            return $this->OK;
        }

        // Tout s'est déroulé correctement alors on ajoute un message
        // d'informations et on retourne le résultat 'OK'
        $this->setMessage($nb_maj." dossier(s) mis à jour.");
        return $this->OK;
    }

    /**
     * Cette méthode permet de faire l'importation des documents scannés
     * @param  string $data données envoyé en paramètre
     */
    public function importDigitalizedDocuments($data) {

        // Si l'option de numérisation est désactivée
        if ($this->f->is_option_digitalization_folder_enabled() !== true) {
            //
            $this->setMessage(_("L'option de numerisation des dossiers n'est pas activee"));
            return $this->OK;
        }

        //Instance classe DigitalizedDocument
        require_once("../obj/digitalizedDocument.class.php");
        $digitalizedDocument = new DigitalizedDocument($this->f);

        //Chemin des dossiers source et destination
        $path = $data;

        //Si deux données sont présentes dans $data
        if (!empty($path)) {

            //Dossier source
            $pathSrc = isset($path["Todo"])?$path["Todo"]:"";
            //Aucun chemin n'est fourni pour le dossier source, on utilise la
            //configuration
            if($pathSrc===""){
                //Si le répertoire de numérisation n'est pas configuré
                if ($this->f->getParameter("digitalization_folder_path") === null) {
                    //Message erreur
                    $this->setMessage(_("Le repertoire de numerisation n'est pas configure"));
                    return $this->KO;
                }
    
                $pathSrc = $this->f->getParameter("digitalization_folder_path").'Todo/';
            }
            //Si le dossier n'existe pas, on retourne une erreur
            elseif (!is_dir($pathSrc)) {
                //Message erreur
                $this->setMessage(_("Le repertoire de numerisation fourni n'existe pas"));
                return $this->KO;
            }
            
            //Dossier de destination
            $pathDes = isset($path["Done"])?$path["Done"]:"";
            //Aucun chemin n'est fourni pour le dossier de destination, on utilise la
            //configuration
            if($pathDes===""){
                //Si le répertoire de numérisation n'est pas configuré
                if ($this->f->getParameter("digitalization_folder_path") === null) {
                    //Message erreur
                    $this->setMessage(_("Le repertoire de numerisation n'est pas configure"));
                    return $this->KO;
                }
    
                $pathDes = $this->f->getParameter("digitalization_folder_path").'Done/';
            }
            //Si le dossier n'existe pas, on retourne une erreur
            elseif (!is_dir($pathDes)) {
                //Message erreur
                $this->setMessage(_("Le repertoire de numerisation fourni n'existe pas"));
                return $this->KO;
            }
        } else {

            //Si le répertoire de numérisation n'est pas configuré
            if ($this->f->getParameter("digitalization_folder_path") === null) {
                //Message erreur
                $this->setMessage(_("Le repertoire de numerisation n'est pas configure"));
                return $this->KO;
            }

            $pathSrc = $this->f->getParameter("digitalization_folder_path").'Todo/';
            $pathDes = $this->f->getParameter("digitalization_folder_path").'Done/';
        }        

        //
        $repo = scandir($pathSrc);
        $importDone = FALSE;
        $importErrorFilename = array();

        //Si le dossier est ouvert
        if ($repo) {
            //
            foreach ($repo as $key => $folder) {
                
                //Vérifie qu'il s'agit de dossier correct
                if ($folder != '.' && $folder != '..') {
                    
                    //Importation des documents
                    $import = $digitalizedDocument->run_import($pathSrc.'/'.$folder, $pathDes.'/'.$folder, $this->DEBUG);

                    //Si  une importation a été faite
                    if ($import) {
                        $importDone = TRUE;
                    }
                    //Si certain des fichiers ne se sont pas importés
                    if (count($digitalizedDocument->filenameError) > 0){
                        //
                        $importErrorFilename = array_merge($importErrorFilename, $digitalizedDocument->filenameError);
                        unset($digitalizedDocument->filenameError);
                        $digitalizedDocument->filenameError = array();
                    }
                    
                }
            }
        }
        
        //Si des fichiers ne se sont pas importés à cause d'une erreur
        if (count($importErrorFilename)>0){
            $importErrorFilename = sprintf(_("Liste des fichiers en erreur : %s"),implode(',',$importErrorFilename));
        }

        //Message de retour
        ($importDone) ?
            //Il y avait des documents à traiter 
            $this->setMessage(_("Tous les documents ont ete traites").
                ((!is_array($importErrorFilename) && $importErrorFilename!= '')?
					"\n".$importErrorFilename:
					"")) :
            //Il n'y avait aucun document à traiter
            $this->setMessage(_("Aucun document a traiter")) ;
        return $this->OK;
    }

    /**
     * Cette méthode permet de faire la purge des documents scannés
     * @param  string $data données envoyé en paramètre
     */
    public function purgeDigitalizedDocuments($data) {

        // Si l'option de numérisation est désactivée
        if ($this->f->is_option_digitalization_folder_enabled() !== true) {
            //
            $this->setMessage(_("L'option de numerisation des dossiers n'est pas activee"));
            return $this->OK;
        }

        //Instance classe DigitalizedDocument
        require_once("../obj/digitalizedDocument.class.php");
        $digitalizedDocument = new DigitalizedDocument($this->f);

        //Récupération des données
        $path = $data;
        //Dossier à purger
        $path = isset($data["dossier"])?$data["dossier"]:"";
        
        //Si aucun dossier n'est fourni, on utilise le dossier de configuration
        if($path==""){
            
            //Si le répertoire de numérisation n'est pas configuré
            if ($this->f->getParameter("digitalization_folder_path") === NULL) {
                //Message erreur
                $this->setMessage(_("Le repertoire de numerisation n'est pas configure"));
                return $this->KO;
            }

            $path = $this->f->getParameter("digitalization_folder_path").'Done/';
        }
        //Si le dossier n'existe pas, on retourne une erreur
        elseif (!is_dir($path)) {
            //Message erreur
            $this->setMessage(_("Le repertoire de numerisation fourni n'existe pas"));
            return $this->KO;
        }
        
        //Nombre de jour à soustraite à la date du jour
        $nbDay = isset($data["nombre_de_jour"])?$data["nombre_de_jour"]:"";
        //Vérifie que $nbDay soit un entier
        if (!is_numeric($nbDay)) {
            $nbDay = null;
        }

        //
        $repo = scandir($path);

        //Si le dossier est ouvert
        if ($repo) {
            $count_purged_folder = 0;
            $count_purged_files = 0;
            //
            foreach ($repo as $key => $folder) {

                //Vérifie qu'il s'agit de dossier correct
                if ($folder != '.' && $folder != '..') {
                    
                    //Importation des documents
                    $purge = $digitalizedDocument->run_purge($path."/".$folder, $nbDay, $this->DEBUG);

                    //Si la purge n'a pas était faite
                    if ($purge != false) {
                        $count_purged_files += $purge;
                    }
                    $count_purged_folder++;

                }

            }
            //Retourne erreur
            $this->setMessage($count_purged_files." fichier(s) purgé(s) sur ".$count_purged_folder." dossier(s) traité(s)");
            return $this->OK;
        }

        //Retourne erreur
        $this->setMessage("Aucun document a traiter");
        return $this->OK;
    }

    /**
     * Cette fonction permet de mettre à jour l'état des dossiers d'autorisation
     */
    public function updateDAState() {

        // Récupération des DA dont l'état doit passer à "Périmé"
        $sql_da = "SELECT dossier_autorisation.dossier_autorisation

            FROM ".DB_PREFIXE."dossier_autorisation
            LEFT OUTER JOIN (
                    SELECT dossier_autorisation
                    FROM ".DB_PREFIXE."dossier
                    LEFT JOIN ".DB_PREFIXE."dossier_instruction_type
                        ON dossier_instruction_type.dossier_instruction_type = dossier.dossier_instruction_type
                    LEFT JOIN ".DB_PREFIXE."avis_decision
                        ON avis_decision.avis_decision = dossier.avis_decision
                    WHERE code IN ('DOC', 'DAACT') and
                        avis_decision.typeavis = 'F')
                AS dossier_2 on dossier_2.dossier_autorisation = dossier_autorisation.dossier_autorisation
            LEFT JOIN ".DB_PREFIXE."dossier
                ON dossier.dossier_autorisation = dossier_autorisation.dossier_autorisation
            LEFT JOIN ".DB_PREFIXE."dossier_instruction_type
                ON dossier.dossier_instruction_type = dossier_instruction_type.dossier_instruction_type
            LEFT JOIN ".DB_PREFIXE."etat_dossier_autorisation
                ON dossier_autorisation.etat_dossier_autorisation = etat_dossier_autorisation.etat_dossier_autorisation
            LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
                ON dossier_autorisation_type_detaille.dossier_autorisation_type_detaille = dossier_autorisation.dossier_autorisation_type_detaille
            
            WHERE etat_dossier_autorisation.libelle = 'Accordé'
                AND dossier_autorisation.date_decision IS NOT NULL
                AND dossier_autorisation.date_validite < current_date
                AND dossier_2.dossier_autorisation IS NULL
            
            GROUP BY dossier_autorisation.dossier_autorisation";
        $res_da = $this->db->query($sql_da);
        if ($this->f->isDatabaseError($res_da, true)) {
            $this->setMessage(_("Erreur de base de donnees."));
            return $this->KO;
        }

        // Récupération de l'identifiant de l'état "Périmé"
        $sql_etat_perime = "SELECT etat_dossier_autorisation
                            FROM ".DB_PREFIXE."etat_dossier_autorisation 
                            WHERE libelle = 'Périmé'";
        $res_etat_perime = $this->db->getOne($sql_etat_perime);
        if ($this->f->isDatabaseError($res_etat_perime, true)) {
            $this->setMessage(_("Erreur de base de donnees."));
            return $this->KO;
        }

        // Liste des dossiers d'autorisation
        $ids = array();
        while ($row_da =& $res_da->fetchRow(DB_FETCHMODE_ASSOC)) {
            $ids[] = "'".$row_da['dossier_autorisation']."'";
        }

        // Si aucun dossier d'autorisation n'est concerné par le traitement alors on
        // ajoute un message d'informations et on retourne un résultat 'OK'
        if (count($ids) == 0) {
            $this->setMessage(_("Aucune mise a jour."));
            return $this->OK;
        }

        // Exécution du traitement
        // On met à jour tous les dossiers d'autorisations
        $fields = array(
            'etat_dossier_autorisation' => $res_etat_perime,
            'etat_dernier_dossier_instruction_accepte' => $res_etat_perime,
        );
        $res = $this->db->autoExecute(DB_PREFIXE."dossier_autorisation",
                                      $fields,
                                      DB_AUTOQUERY_UPDATE,
                                      'dossier_autorisation IN ('.implode(',', $ids).')');
        // Logger
        $this->addToLog("updateDAState(): db->autoExecute(\"".DB_PREFIXE."dossier_autorisation"."\", ".print_r($fields, true).", DB_AUTOQUERY_UPDATE, \"dossier_autorisation IN (\"".implode(',', $ids)."\")", VERBOSE_MODE);
        // Si une erreur de base de données se produit sur cette requête
        // alors on retourne un résultat d'erreur
        if ($this->f->isDatabaseError($res, true)) {
            $this->setMessage("Erreur de base de données.");
            return $this->KO;
        }

        // Tout s'est déroulé correctement alors on ajoute un message
        // d'informations et on retourne le résultat 'OK'
        $this->setMessage(count($ids)." "._("dossier(s) d'autorisation(s) mis a jour."));
        return $this->OK;

    }

    /**
     * Synchronisation des contraintes depuis le SIG
     * Permet d'ajouter de nouvelle contrainte, de modifier les contraintes
     * existantes et d'archiver les contraintes qui ne sont plus utilisées.
     */
    public function synchronisationContraintes() {

        // Si l'option du SIG est désactivée
        if ($this->f->is_option_sig_enabled() !== true) {
            //
            $this->setMessage(_("L'option du SIG n'est pas activée"));
            return $this->OK;
        }

        // Instancie la classe SynchronisationContrainte
        require_once("../obj/synchronisationContrainte.class.php");

        //
        try {
            $synchronisationContrainte = new SynchronisationContrainte($this->f);
        } catch (geoads_exception $e) {
            //
            $this->addToLog("synchronisationContraintes(): Traitement webservice SIG: ".$e->getMessage(), DEBUG_MODE);
            //
            $this->setMessage("une erreur s'est produite.")." "._("Contactez votre administrateur")."<br />";
            //
            return $this->KO;
        }

        $correct = $synchronisationContrainte->constraint_sync_treatment();
        $output_message = $synchronisationContrainte->get_output_message();

        $display_message = array();
        foreach ($output_message as $key => $value) {
            $temp = "";
            if(isset($value["commune"]) === true) {
                $temp .= $value["commune"]." : ";
            }
            if($value["type"] == 'error') {
                $temp .= " (erreur) ";
            }
            $temp .= strip_tags($value["message"]);
            $display_message[] = $temp;
        }

        $message = $this->setMessage(implode($display_message, ' - '));
        //
        if ($correct == true) {
            //
            return $this->OK;
        }

        // S'il y a une erreur
        if ($correct == false) {
            //
            $this->addToLog("synchronisationContraintes(): ".$message, DEBUG_MODE);
            return $this->KO;
        }
    }



    /**
     * Permet de mettre à jour les métadonnées suivantes de tous les documents numérisés, en
     * fonction du paramétrage des types de documents numérisés dans la BDD :
     * - "affichage DA"
     * - "affichage service consulté"
     */
    public function updateDigitalizedDocumentsMetadata() {

        // Instancie la classe SynchronisationContrainte
        require_once ("../obj/document_numerise_traitement_metadonnees.class.php");

        // Initialisation du retour du traitement
        $documents_en_erreur = false;
        //
        $inst_document_numerise_traitement_metadonnees = new document_numerise_traitement_metadonnees(0);
        $documents_en_erreur = $inst_document_numerise_traitement_metadonnees->metadata_treatment();
        // Si toutes les métadonnées ont été correctement mises à jour
        if (is_array($documents_en_erreur) AND count($documents_en_erreur) === 0) {
            //Tous les documents ont ete traites
            $this->setMessage(_("Tous les documents ont été traités."));
            return $this->OK;
        }

        // Si certaines métadonnées n'ont pas pû être mises à jour
        if (is_array($documents_en_erreur) AND count($documents_en_erreur) !== 0) {

            $display_message = array();
            foreach ($documents_en_erreur as $key => $value) {
                if(isset($value["dossier"]) === true AND isset($value["nom_fichier"]) === true ) {
                    $display_message[] = sprintf(
                        _("Dossier d'instruction n°%s - le document %s n'a pas pu être mis à jour"),
                        $value['dossier'],
                        $value['nom_fichier']
                    );
                }
            }
            $this->setMessage("Liste des fichiers en erreur : ".implode($display_message, ', '));
            return $this->OK;
        }

        // S'il y a une erreur
        if ($documents_en_erreur == false) {
            //
            $this->setMessage("Une erreur s'est produite lors de la mise à jour des métadonnées.", DEBUG_MODE);
            return $this->KO;
        }

    }
}

?>
