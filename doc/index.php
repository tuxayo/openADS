<?php
/**
 * Ce fichier permet de faire une redirection vers la documentation de l'aaplication.
 *
 * @package openmairie_exemple
 * @version SVN : $Id: index.php 4418 2015-02-24 17:30:28Z tbenita $
 */

header("Location: http://docs.openmairie.org/?project=openads&version=4.0&format=html&path=manuel_utilisateur");
exit();

?>

