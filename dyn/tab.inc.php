<?php
/**
 * Ce fichier permet de definir des variables specifiques a passer dans la
 * methode tableau des objets metier
 *
 * @package openmairie_exemple
 * @version SVN : $Id: tab.inc.php 5208 2015-09-23 21:32:51Z fmichon $
 */

/**
 * Exemple : un ecran specifique me permet de passer la date de naissance de
 * l'utilisateur au tableau uniquement lorsque l'objet est "agenda".
 *
 * if ($obj == "agenda") {
 *     $datenaissance = "";
 *     if (isset($_GET['datenaissance'])) {
 *         $datenaissance = $_GET['datenaissance'];
 *     }
 *     $extra_parameters["datenaissance"] = $datenaissance;
 * }
 *
 * Ainsi dans la methode tableau de l'objet en question la valeur de la date
 * de naissance sera accessible
 */

// WIDGET DASHBOARD - widget_dossiers_limites.
// Permet de passer les paramètres du widget dossiers limites
if ($obj == "dossiers_limites") {
    //
    $nombre_de_jours = "";
    if (isset($_GET['nombre_de_jours'])) {
        $nombre_de_jours = $_GET['nombre_de_jours'];
    }
    $extra_parameters["nombre_de_jours"] = $nombre_de_jours;
    //
    $codes_datd = "";
    if (isset($_GET['codes_datd'])) {
        $codes_datd = $_GET['codes_datd'];
    }
    $extra_parameters["codes_datd"] = $codes_datd;
    //
    $filtre = "";
    if (isset($_GET['filtre'])) {
        $filtre = $_GET['filtre'];
    }
    $extra_parameters["filtre"] = $filtre;
}

// Permet de filtrer les dossiers dont on peut modifier la décision
if ($obj == "dossier_instruction") {
    $filtre_decision = false;
    if (isset($_GET['decision']) && $_GET['decision'] == 'true') {
        $filtre_decision = true;
    }
    $extra_parameters["filtre_decision"] = $filtre_decision;
}

// Pour tous les listings des objets suivants, on passe les paramètres des widgets
// correspondants
if ($obj == "dossier_contentieux_contradictoires"
    || $obj == "dossier_contentieux_ait"
    || $obj == "dossier_contentieux_alerte_parquet"
    || $obj == "dossier_contentieux_alerte_visite"
    || $obj == "dossier_contentieux_audience"
    || $obj == "dossier_contentieux_clotures"
    || $obj == "dossier_contentieux_inaffectes"
    || $obj == "dossiers_evenement_incomplet_majoration") {
    //
    $filtre = "";
    if (isset($_GET['filtre'])) {
        $filtre = $_GET['filtre'];
    }
    $extra_parameters["filtre"] = $filtre;
}

// Recherche avancée : complétion uniquement à droite
$options[] = array('type' => 'wildcard', 'left' => '', 'right' => '%');
?>
