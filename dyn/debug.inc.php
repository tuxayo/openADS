<?php
/**
 * Ce fichier contient le parametrage pour le mode debug
 *
 * @package openmairie_exemple
 * @version SVN : $Id: debug.inc.php 5576 2015-12-09 09:53:09Z stimezouaght $
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."om_debug.inc.php";

/**
 *
 */
//define('DEBUG', EXTRA_VERBOSE_MODE);
//define('DEBUG', VERBOSE_MODE);
//define('DEBUG', DEBUG_MODE);
//define('DEBUG', PRODUCTION_MODE);

?>
