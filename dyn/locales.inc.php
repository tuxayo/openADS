<?php
/**
 * Ce fichier contient les parametres d'encodage et d'internationalisation
 *
 * @package openmairie_exemple
 * @version SVN : $Id: locales.inc.php 4789 2015-05-22 15:13:53Z nhaye $
 */

/**
 * Codage des caracteres
 */
define('DBCHARSET', 'UTF8');
define('HTTPCHARSET', 'UTF-8');

/**
 * Pour voir les autres locales disponibles, il faut voir le contenu du dossier
 * locales/ et il faut que cette locale soit installee sur votre systeme
 */
define('LOCALE', 'fr_FR');

/**
 * Le dossier contenant les locales et les fichiers de traduction
 */
define('LOCALES_DIRECTORY', '../locales');

/**
 * Le domaine de traduction
 */
define('DOMAIN', 'openmairie');

?>
