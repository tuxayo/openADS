<?php
/**
 * Ce fichier permet de configurer quels paths vont etre ajoutes a la
 * directive include_path du fichier php.ini
 *
 * @package openmairie_exemple
 * @version SVN : $Id: include.inc.php 4418 2015-02-24 17:30:28Z tbenita $
 */

/**
 * Ce tableau permet de stocker la liste des chemins a ajouter a la directive
 * include_path, vous pouvez modifier ces chemins avec vos propres valeurs si
 * vos chemins ne sont pas deja inclus dans votre installation, par contre si
 * vous avez deja configurer ces chemins dans votre installation vous pouvez
 * commenter les lignes suivantes
 */
$include = array();

/**
 * @todo Remplacer 'getcwd()."/../"' par un moyen plus generique de recuperer
 *       le chemin racine de l'application
 */
// PEAR
array_push($include, getcwd()."/../php/pear");

// DB
array_push($include, getcwd()."/../php/db");

// FPDF
array_push($include, getcwd()."/../php/fpdf");

// PHPMAILER
array_push($include, getcwd()."/../php/phpmailer");

// OPENMAIRIE
//array_push($include, getcwd()."/../php/openmairie");
define("PATH_OPENMAIRIE", getcwd()."/../core/");
define("PATH_BASE_URL", get_base_url());

/**
 * Ici on modifie la valeur de la directive de configuration include_path en
 * fonction du parametrage present dans le fichier dyn/include.inc.php
 */
if (isset($include)) {
    set_include_path(get_include_path().PATH_SEPARATOR.implode(PATH_SEPARATOR, $include));
}

/**
 * Retourne l'URL de la racine d'openADS.
 * Exemple : http://localhost/openads/
 */
function get_base_url() {
    // Récupération du protocole
    $protocol = 'http';
    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    // Récupération du domaine
    $domain = $_SERVER['HTTP_HOST'];
    // Récupération du port
    $port = $_SERVER['SERVER_PORT'];
    $disp_port = ($protocol == 'http' && $port == 80 || $protocol == 'https' && $port == 443) ? '' : ":$port";
    // Construction du chemin restant
    $base_url = str_replace('scr', '', rtrim(dirname($_SERVER['PHP_SELF']), '/\\'));
    //
    return $protocol."://".$domain.$disp_port.$base_url;
}

?>
