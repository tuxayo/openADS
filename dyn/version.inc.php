<?php
/**
 * Ce fichier permet de stocker la version de l'application
 *
 * @package openmairie_exemple
 * @version SVN : $Id: version.inc.php 6181 2016-03-15 17:47:24Z stimezouaght $
 */

/**
 * Le numero de version est affiche en permanence dans le footer
 */
$version = "4.5.0.dev0";

?>
