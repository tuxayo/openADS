<?php
/**
 * Ce fichier permet de configurer quelles actions vont etre disponibles
 * dans le menu.
 *
 * @package openmairie_exemple
 * @version SVN : $Id: menu.inc.php 5935 2016-02-15 08:16:02Z fmichon $
 */

/**
 * $menu est le tableau associatif qui contient tout le menu de
 * l'application, il contient lui meme un tableau par rubrique, puis chaque
 * rubrique contient un tableau par lien
 *
 * Caracteristiques :
 * --- tableau rubrik
 *     - title [obligatoire]
 *     - description (texte qui s'affiche au survol de la rubrique)
 *     - href (contenu du lien href)
 *     - class (classe css qui s'affiche sur la rubrique)
 *     - right [optionnel] (droit que l'utilisateur doit avoir pour visionner
 *                          cette rubrique, si aucun droit n'est mentionne alors
 *                          si aucun lien n'est present dans cette rubrique, on
 *                          ne l'affiche pas)
 *     - links [obligatoire]
 *     - open [optionnel] permet de définir les critères permettant
 *           de conserver la rubrique de menu ouverte.
 *           La définition est une liste de criteres, de type array, contenant des chaines
 *           de type "script|obj" ou "script|" ou "|obj".
 *           S'il y a un unique critere on peut ne pas mettre de array
 *           Si un critere correspond avec l'URL, la rubrique est ouverte.
 *           
 *
 * --- tableau links
 *     - title [obligatoire]
 *     - href [obligatoire] (contenu du lien href)
 *     - class (classe css qui s'affiche sur l'element)
 *     - right (droit que l'utilisateur doit avoir pour visionner cet element)
 *     - target (pour ouvrir le lien dans une nouvelle fenetre)
 *     - open [optionnel] idem à ci-dessus. Les "open" de links sont utilises pour la rubrik :
 *           pas besoin de definir le critere dans rubrik si il est defini dans links
 *           la correspondance rend le lien actif et la rubrique est ouverte
 *           exemples :
 *               open => array("tab.php|users", "form.php|users"),
 *               open => "|users"
 *               open => "script.php|"
 */
//
$menu = array();

// {{{ Rubrique AUTORISATION
//
$rubrik = array(
    "title" => _("Autorisation"),
    "class" => "autorisation",
    "right" => "menu_autorisation",
);
//
$links = array();

$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_autorisation",
    "class" => "dossier_autorisation",
    "title" => _("Dossiers d'autorisation"),
    "right" => array("dossier_autorisation", "dossier_autorisation_tab", ),
    "open" => array("tab.php|dossier_autorisation", "form.php|dossier_autorisation", ),
);

// Lien vers les dossiers d'autorisations qui ont une demande d'avis
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_autorisation_avis",
    "class" => "dossier_autorisation",
    "title" => _("Dossiers d'autorisation"),
    "right" => array(
        "dossier_autorisation_avis",
        "dossier_autorisation_avis_tab",
    ),
    "open" => array("tab.php|dossier_autorisation_avis", "form.php|dossier_autorisation", ),
);

//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique GUICHET UNIQUE
//
$rubrik = array(
    "title" => _("Guichet Unique"),
    "class" => "guichet_unique",
    "right" => "menu_guichet_unique",
);
//
$links = array();
//
$links[] = array(
    "href" => "../scr/dashboard.php",
    "class" => "tableau-de-bord",
    "title" => _("tableau de bord"),
    "right" => "menu_guichet_unique_dashboard",
    "open" => array("dashboard.php|",),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("nouvelle demande"),
    "right" => array(
        "demande",
        "demande_nouveau_dossier_ajouter",
        "demande_dossier_encours_ajouter", "demande_dossier_encours_tab",
        "demande_autre_dossier_ajouter", "demande_autre_dossier_tab",
        "demande_consulter","demande_tab",
    ),
);
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "demande",
        "demande_dossier_encours_ajouter",
        "demande_dossier_encours_ajouter", "demande_dossier_encours_tab",
    ),
);
$links[] = array(
    "href" => "../scr/form.php?obj=demande_nouveau_dossier&amp;action=0&amp;advs_id=&amp;tricol=&amp;valide=&amp;retour=tab&amp;new=",
    "class" => "nouveau-dossier",
    "title" => _("nouveau dossier"),
    "right" => array(
        "demande",
        "demande_nouveau_dossier_ajouter",
    ),
    "open" => array("form.php|demande_nouveau_dossier",),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=demande_dossier_encours",
    "class" => "dossier-existant",
    "title" => _("dossier en cours"),
    "right" => array(
        "demande",
        "demande_dossier_encours_ajouter",
        "demande_dossier_encours_tab",
    ),
    "open" => array("tab.php|demande_dossier_encours", "form.php|demande_dossier_encours"),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=demande_autre_dossier",
    "class" => "autre-dossier",
    "title" => _("autre dossier"),
    "right" => array(
        "demande",
        "demande_autre_dossier_ajouter",
        "demande_autre_dossier_tab",
    ),
    "open" => array("tab.php|demande_autre_dossier", "form.php|demande_autre_dossier"),
);
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "demande",
        "demande_consulter",
        "demande_tab"
    ),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=demande",
    "class" => "pdf",
    "title" => _("recepisse"),
    "right" => array(
        "demande",
        "demande_consulter",
        "demande_tab"
    ),
    "open" => array("tab.php|demande","form.php|demande"),
);
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "petitionnaire_frequent",
        "petitionnaire_frequent_consulter",
        "petitionnaire_frequent_tab"
    ),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=petitionnaire_frequent",
    "class" => "petitionnaire_frequent",
    "title" => _("petitionnaire_frequent"),
    "right" => array(
        "petitionnaire_frequent",
        "petitionnaire_frequent_consulter",
        "petitionnaire_frequent_tab"
    ),
    "open" => array("tab.php|petitionnaire_frequent","form.php|petitionnaire_frequent"),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("affichage reglementaire"),
    "right" => array(
        "affichage_reglementaire_registre",
        "affichage_reglementaire_attestation",
    ),
);
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "affichage_reglementaire_registre",
        "affichage_reglementaire_attestation",
    ),
);
$links[] = array(
    "href" => "../scr/form.php?obj=demande_affichage_reglementaire_registre&action=110&idx=0",
    "class" => "affichage_reglementaire_registre",
    "title" => _("registre"),
    "right" => array(
        "affichage_reglementaire_registre",
    ),
    "open" => array("tab.php|demande_affichage_reglementaire_registre", "form.php|demande_affichage_reglementaire_registre"),
);
$links[] = array(
    "href" => "../scr/form.php?obj=demande_affichage_reglementaire_attestation&action=120&idx=0",
    "class" => "affichage_reglementaire_attestation",
    "title" => _("attestation"),
    "right" => array(
        "affichage_reglementaire_attestation",
    ),
    "open" => array("tab.php|demande_affichage_reglementaire_attestation", "form.php|demande_affichage_reglementaire_attestation"),
);
//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique QUALIFICATION
//
$rubrik = array(
    "title" => _("Qualification"),
    "class" => "qualification",
    "right" => "qualification_menu",
);
//
$links = array();
//
$links[] = array(
    "href" => "../scr/dashboard.php",
    "class" => "tableau-de-bord",
    "title" => _("tableau de bord"),
    "right" => "menu_qualification_dashboard",
    "open" => array("dashboard.php|",),
);

//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_qualifier_qualificateur",
    "class" => "dossier_qualifier_qualificateur",
    "title" => _("dossiers a qualifier"),
    "right" => array(
        "dossier_qualifier_qualificateur",
        "dossier_qualifier_qualificateur_tab",
    ),
    "open" => array("tab.php|dossier_qualifier_qualificateur", "form.php|dossier_instruction", ),
);

//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique INSTRUCTION
//
$rubrik = array(
    "title" => _("instruction"),
    "class" => "instruction",
    "right" => "menu_instruction",
);
//
$links = array();
//
$links[] = array(
    "href" => "../scr/dashboard.php",
    "class" => "tableau-de-bord",
    "title" => _("tableau de bord"),
    "right" => "menu_instruction_dashboard",
    "open" => array("dashboard.php|",),
);
// Catégorie DOSSIERS D'INSTRUCTION
$links[] = array(
    "class" => "category",
    "title" => _("dossiers d'instruction"),
    "right" => array(
        "dossier_instruction_mes_encours", "dossier_instruction_mes_encours_tab",
        "dossier_instruction_tous_encours", "dossier_instruction_tous_encours_tab",
        "dossier_instruction_mes_clotures", "dossier_instruction_mes_clotures_tab",
        "dossier_instruction_tous_clotures", "dossier_instruction_tous_clotures_tab",
        "dossier_instruction", "dossier_instruction_tab",
        "PC_modificatif", "PC_modificatif_tab", 
    ),
);
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_instruction_mes_encours", "dossier_instruction_mes_encours_tab",
        "dossier_instruction_tous_encours", "dossier_instruction_tous_encours_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_mes_encours",
    "class" => "dossier_instruction_mes_encours",
    "title" => _("mes encours"),
    "right" => array("dossier_instruction_mes_encours", "dossier_instruction_mes_encours_tab", ),
    "open" => array("tab.php|dossier_instruction_mes_encours", "form.php|dossier_instruction_mes_encours", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_tous_encours",
    "class" => "dossier_instruction_tous_encours",
    "title" => _("tous les encours"),
    "right" => array("dossier_instruction_tous_encours", "dossier_instruction_tous_encours_tab", ),
    "open" => array("tab.php|dossier_instruction_tous_encours", "form.php|dossier_instruction_tous_encours", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_instruction_mes_clotures", "dossier_instruction_mes_clotures_tab",
        "dossier_instruction_tous_clotures", "dossier_instruction_tous_clotures_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_mes_clotures",
    "class" => "dossier_instruction_mes_clotures",
    "title" => _("mes clotures"),
    "right" => array("dossier_instruction_mes_clotures", "dossier_instruction_mes_clotures_tab", ),
    "open" => array("tab.php|dossier_instruction_mes_clotures", "form.php|dossier_instruction_mes_clotures", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_tous_clotures",
    "class" => "dossier_instruction_tous_clotures",
    "title" => _("tous les clotures"),
    "right" => array("dossier_instruction_tous_clotures", "dossier_instruction_tous_clotures_tab", ),
    "open" => array("tab.php|dossier_instruction_tous_clotures", "form.php|dossier_instruction_tous_clotures", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_instruction", "dossier_instruction_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction",
    "class" => "dossier_instruction_recherche",
    "title" => _("recherche"),
    "right" => array("dossier_instruction", "dossier_instruction_tab", ),
    "open" => array("tab.php|dossier_instruction", "form.php|dossier_instruction", ),
);

// Catégorier Qualification
$links[] = array(
    "class" => "category",
    "title" => _("qualification"),
    "right" => array("dossier_qualifier", "architecte_frequent",),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array("dossier_qualifier", "architecte_frequent", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_qualifier",
    "class" => "dossier_qualifier",
    "title" => _("dossiers a qualifier"),
    "right" => array("dossier_qualifier", "dossier_qualifier_tab", ),
    "open" => array("tab.php|dossier_qualifier", "form.php|dossier_qualifier", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=architecte_frequent",
    "class" => "architecte_frequent",
    "title" => _("architecte_frequent"),
    "right" => array("architecte_frequent", "architecte_frequent_tab", ),
    "open" => array("tab.php|architecte_frequent", "form.php|architecte_frequent", ),
);
// Catégorie CONSULTATIONS
$links[] = array(
    "class" => "category",
    "title" => _("consultations"),
    "right" => array(
        "consultation",
        "consultation_mes_retours",
        "consultation_retours_ma_division",
        "consultation_tous_retours",
    ),
);
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "consultation",
        "consultation_mes_retours",
        "consultation_retours_ma_division",
        "consultation_tous_retours",
    ),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=consultation_mes_retours",
    "class" => "consultation_mes_retours",
    "title" => _("Mes retours"),
    "right" => array(
        "consultation",
        "consultation_mes_retours",
        "consultation_mes_retours_tab",
    ),
    "open" => array("tab.php|consultation_mes_retours", "form.php|consultation_mes_retours", ),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=consultation_retours_ma_division",
    "class" => "consultation_retours_ma_division",
    "title" => _("Retours de ma division"),
    "right" => array(
        "consultation",
        "consultation_retours_ma_division",
        "consultation_retours_ma_division_tab",
    ),
    "open" => array("tab.php|consultation_retours_ma_division", "form.php|consultation_retours_ma_division", ),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=consultation_tous_retours",
    "class" => "consultation_tous_retours",
    "title" => _("Tous les retours"),
    "right" => array(
        "consultation_tous_retours",
        "consultation_tous_retours_tab",
    ),
    "open" => array("tab.php|consultation_tous_retours", "form.php|consultation_tous_retours", ),
);
// Catégorie MESSAGES
$links[] = array(
    "class" => "category",
    "title" => _("Messages"),
    "right" => array(
        "messages",
        "messages_mes_retours",
        "messages_retours_ma_division",
        "messages_tous_retours",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "messages",
        "messages_mes_retours",
        "messages_retours_ma_division",
        "messages_tous_retours",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=messages_mes_retours",
    "class" => "messages_mes_retours",
    "title" => _("Mes messages"),
    "right" => array(
        "messages",
        "messages_mes_retours",
        "messages_mes_retours_tab",
    ),
    "open" => array("tab.php|messages_mes_retours", "form.php|messages_mes_retours", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=messages_retours_ma_division",
    "class" => "messages_retours_ma_division",
    "title" => _("Messages de ma division"),
    "right" => array(
        "messages",
        "messages_retours_ma_division",
        "messages_retours_ma_division_tab",
    ),
    "open" => array("tab.php|messages_retours_ma_division", "form.php|messages_retours_ma_division", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=messages_tous_retours",
    "class" => "messages_tous_retours",
    "title" => _("Tous les messages"),
    "right" => array(
        "messages",
        "messages_tous_retours",
        "messages_tous_retours_tab",
    ),
    "open" => array("tab.php|messages_tous_retours", "form.php|messages_tous_retours", ),
);
// Catégorie COMMISSIONS
$links[] = array(
    "class" => "category",
    "title" => _("commissions"),
    "right" => array(
        "commission_mes_retours",
        "commission_mes_retours_tab",
        "commission_retours_ma_division",
        "commission_retours_ma_division_tab",
        "commission_tous_retours",
        "commission_tous_retours_tab",
    ),
);
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "commission_mes_retours",
        "commission_mes_retours_tab",
        "commission_retours_ma_division",
        "commission_retours_ma_division_tab",
        "commission_tous_retours",
        "commission_tous_retours_tab",
    ),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=commission_mes_retours",
    "class" => "commission_mes_retours",
    "title" => _("Mes retours"),
    "right" => array(
        "commission_mes_retours",
        "commission_mes_retours_tab",
    ),
    "open" => array("tab.php|commission_mes_retours", "form.php|commission_mes_retours", ),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=commission_retours_ma_division",
    "class" => "commission_retours_ma_division",
    "title" => _("Retours de ma division"),
    "right" => array(
        "commission_retours_ma_division",
        "commission_retours_ma_division_tab",
    ),
    "open" => array("tab.php|commission_retours_ma_division", "form.php|commission_retours_ma_division", ),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=commission_tous_retours",
    "class" => "commission_tous_retours",
    "title" => _("Tous les retours"),
    "right" => array(
        "commission_tous_retours",
        "commission_tous_retours_tab",
    ),
    "open" => array("tab.php|commission_tous_retours", "form.php|commission_tous_retours", ),
);

//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique Contentieux
//
$rubrik = array(
    "title" => _("Contentieux"),
    "class" => "contentieux",
    "right" => "menu_contentieux",
);
//
$links = array();
//
$links[] = array(
    "href" => "../scr/dashboard.php",
    "class" => "tableau-de-bord",
    "title" => _("tableau de bord"),
    "right" => "menu_contentieux_dashboard",
    "open" => array("dashboard.php|", "tab.php|dossier_contentieux_contradictoire", "tab.php|dossier_contentieux_ait", "tab.php|dossier_contentieux_audience", "tab.php|dossier_contentieux_clotures", "tab.php|dossier_contentieux_inaffectes", "tab.php|dossier_contentieux_alerte_visite", "tab.php|dossier_contentieux_alerte_parquet", ),
);
// Catégorie Nouvelle demande
$links[] = array(
    "class" => "category",
    "title" => _("Nouvelle demande"),
    "right" => array(
        "demande_nouveau_dossier_contentieux_ajouter",
    ),
);
$links[] = array(
    "href" => "../scr/form.php?obj=demande_nouveau_dossier_contentieux&amp;action=0&amp;advs_id=&amp;tricol=&amp;valide=&amp;retour=tab&amp;new=",
    "class" => "nouveau-dossier",
    "title" => _("nouveau dossier"),
    "right" => array(
        "demande_nouveau_dossier_contentieux_ajouter",
    ),
    "open" => array("form.php|demande_nouveau_dossier_contentieux",),
);
// Catégorie Recours
$links[] = array(
    "class" => "category",
    "title" => _("Recours"),
    "right" => array(
        "dossier_contentieux_mes_recours", "dossier_contentieux_mes_recours_tab", 
        "dossier_contentieux_tous_recours", "dossier_contentieux_tous_recours_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_contentieux_mes_recours",
    "class" => "dossier_contentieux_mes_recours",
    "title" => _("Mes recours"),
    "right" => array("dossier_contentieux_mes_recours", "dossier_contentieux_mes_recours_tab", ),
    "open" => array("tab.php|dossier_contentieux_mes_recours", "form.php|dossier_contentieux_mes_recours", ),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_contentieux_tous_recours",
    "class" => "dossier_contentieux_tous_recours",
    "title" => _("Tous les recours"),
    "right" => array("dossier_contentieux_tous_recours", "dossier_contentieux_tous_recours_tab", ),
    "open" => array("tab.php|dossier_contentieux_tous_recours", "form.php|dossier_contentieux_tous_recours", ),
);
// Catégorie Infractions
$links[] = array(
    "class" => "category",
    "title" => _("Infractions"),
    "right" => array(
        "dossier_contentieux_mes_infractions", "dossier_contentieux_mes_infractions_tab", 
        "dossier_contentieux_toutes_infractions", "dossier_contentieux_toutes_infractions_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_contentieux_mes_infractions",
    "class" => "dossier_contentieux_mes_infractions",
    "title" => _("Mes infractions"),
    "right" => array("dossier_contentieux_mes_infractions", "dossier_contentieux_mes_infractions_tab", ),
    "open" => array("tab.php|dossier_contentieux_mes_infractions", "form.php|dossier_contentieux_mes_infractions", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_contentieux_toutes_infractions",
    "class" => "dossier_contentieux_toutes_infractions",
    "title" => _("Toutes les infractions"),
    "right" => array("dossier_contentieux_toutes_infractions", "dossier_contentieux_toutes_infractions_tab", ),
    "open" => array("tab.php|dossier_contentieux_toutes_infractions", "form.php|dossier_contentieux_toutes_infractions", ),
);
// Catégorie MESSAGES
$links[] = array(
    "class" => "category",
    "title" => _("Messages"),
    "right" => array(
        "messages_contentieux",
        "messages_contentieux_mes_retours",
        "messages_contentieux_retours_ma_division",
        "messages_contentieux_tous_retours",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "messages_contentieux",
        "messages_contentieux_mes_retours",
        "messages_contentieux_retours_ma_division",
        "messages_contentieux_tous_retours",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=messages_contentieux_mes_retours",
    "class" => "messages_contentieux_mes_retours",
    "title" => _("Mes messages"),
    "right" => array(
        "messages_contentieux",
        "messages_contentieux_mes_retours",
        "messages_contentieux_mes_retours_tab",
    ),
    "open" => array("tab.php|messages_contentieux_mes_retours", "form.php|messages_contentieux_mes_retours", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=messages_contentieux_retours_ma_division",
    "class" => "messages_contentieux_retours_ma_division",
    "title" => _("Messages de ma division"),
    "right" => array(
        "messages_contentieux",
        "messages_contentieux_retours_ma_division",
        "messages_contentieux_retours_ma_division_tab",
    ),
    "open" => array("tab.php|messages_contentieux_retours_ma_division", "form.php|messages_contentieux_retours_ma_division", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=messages_contentieux_tous_retours",
    "class" => "messages_contentieux_tous_retours",
    "title" => _("Tous les messages"),
    "right" => array(
        "messages_contentieux",
        "messages_contentieux_tous_retours",
        "messages_contentieux_tous_retours_tab",
    ),
    "open" => array("tab.php|messages_contentieux_tous_retours", "form.php|messages_contentieux_tous_retours", ),
);


//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique SUIVI
//
$rubrik = array(
    "title" => _("Suivi"),
    "class" => "suivi",
    "right" => "menu_suivi",
);
//
$links = array();
//
$links[] = array(
    "href" => "../scr/dashboard.php",
    "class" => "tableau-de-bord",
    "title" => _("tableau de bord"),
    "right" => "menu_suivi_dashboard",
    "open" => array("dashboard.php|",),
);
$links[] = array(
    "class" => "category",
    "title" => _("suivi des pieces"),
    "right" => array(
        "instruction_suivi_retours_de_consultation", "instruction_suivi_mise_a_jour_des_dates",
        "instruction_suivi_envoi_lettre_rar", "instruction_suivi_bordereaux",
        "instruction_suivi_retours_de_consultation_consulter", "instruction_suivi_mise_a_jour_des_dates_consulter",
        "instruction_suivi_envoi_lettre_rar_consulter", "instruction_suivi_bordereaux_consulter",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "instruction_suivi_retours_de_consultation", "instruction_suivi_mise_a_jour_des_dates",
        "instruction_suivi_envoi_lettre_rar", "instruction_suivi_bordereaux",
        "instruction_suivi_retours_de_consultation_consulter", "instruction_suivi_mise_a_jour_des_dates_consulter",
        "instruction_suivi_envoi_lettre_rar_consulter", "instruction_suivi_bordereaux_consulter",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "instruction_suivi_mise_a_jour_des_dates", "instruction_suivi_mise_a_jour_des_dates_consulter", 
    ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=instruction_suivi_mise_a_jour_des_dates&action=170&idx=0",
    "class" => "suivi_mise_a_jour_des_dates",
    "title" => _("Mise a jour des dates"),
    "right" => array("instruction_suivi_mise_a_jour_des_dates", "instruction_suivi_mise_a_jour_des_dates_consulter", ),
    "open" => array("tab.php|instruction_suivi_mise_a_jour_des_dates", "form.php|instruction_suivi_mise_a_jour_des_dates"),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "instruction_suivi_envoi_lettre_rar", "instruction_suivi_envoi_lettre_rar_consulter", 
    ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=instruction_suivi_envoi_lettre_rar&action=160&idx=0",
    "class" => "envoi_lettre_rar",
    "title" => _("envoi lettre RAR"),
    "right" => array("instruction_suivi_envoi_lettre_rar", "instruction_suivi_envoi_lettre_rar_consulter", ),
    "open" => array("tab.php|instruction_suivi_envoi_lettre_rar", "form.php|instruction_suivi_envoi_lettre_rar"),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "instruction_suivi_bordereaux", "instruction_suivi_bordereaux_consulter", 
    ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=instruction_suivi_bordereaux&action=150&idx=0",
    "class" => "bordereaux",
    "title" => _("Bordereaux"),
    "right" => array("instruction_suivi_bordereaux", "instruction_suivi_bordereaux_consulter", ),
    "open" => array("tab.php|instruction_suivi_bordereaux", "form.php|instruction_suivi_bordereaux"),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=bordereau_envoi_maire&action=190&idx=0",
    "class" => "bordereau_envoi_maire",
    "title" => _("Bordereau d'envoi au maire"),
    "right" => array("instruction_bordereau_envoi_maire","bordereau_envoi_maire"),
    "open" => array("form.php|bordereau_envoi_maire",),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("Demandes d'avis"),
    "right" => array(
        "consultation_suivi_mise_a_jour_des_dates", 
        "consultation_suivi_retours_de_consultation",
    ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=consultation&idx=0&action=110",
    "class" => "demandes_avis_mise_a_jour_des_dates",
    "title" => _("Mise a jour des dates"),
    "right" => array("consultation_suivi_mise_a_jour_des_dates", ),
    "open" => array("form.php|consultation[action=110]"),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=consultation&idx=0&action=120",
    "class" => "consultation-retour",
    "title" => _("retours de consultation"),
    "right" => array("consultation_suivi_retours_de_consultation", ),
    "open" => array("form.php|consultation[action=120]", "form.php|consultation[action=100]", ),
);
// Catégorie COMMISSIONS
$links[] = array(
    "class" => "category",
    "title" => _("commissions"),
    "right" => array(
        "commission",
        "commission_tab",
        "commission_demandes_passage",
        "commission_demandes_passage_tab",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "commission",
        "commission_tab",
        "commission_demandes_passage",
        "commission_demandes_passage_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=commission",
    "class" => "commissions",
    "title" => _("gestion"),
    "right" => array(
        "commission",
    ),
    "open" => array("tab.php|commission", "form.php|commission", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=commission_demandes_passage",
    "class" => "commissions-demande-passage",
    "title" => _("demandes"),
    "right" => array(
        "commission",
        "commission_demandes_passage",
    ),
    "open" => array("tab.php|commission_demandes_passage", "form.php|commission_demandes_passage", ),
);
//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique DEMANDES D'AVIS
//
$rubrik = array(
    "title" => _("Demandes d'avis"),
    "class" => "demande_avis",
    "right" => "menu_demande_avis",
);
//
$links = array();
//
$links[] = array(
    "href" => "../scr/dashboard.php",
    "class" => "tableau-de-bord",
    "title" => _("tableau de bord"),
    "right" => "menu_demande_avis_dashboard",
    "open" => array("dashboard.php|",),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "demande_avis_encours", "demande_avis_encours_tab",
        "demande_avis_passee", "demande_avis_passee_tab",
        "demande_avis", "demande_avis_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=demande_avis_encours",
    "class" => "demande_avis_encours",
    "title" => _("Demandes en cours"),
    "right" => array("demande_avis_encours", "demande_avis_encours_tab", ),
    "open" => array("tab.php|demande_avis_encours", "form.php|demande_avis_encours", ),
);

$links[] = array(
    "href" => "../scr/tab.php?obj=demande_avis_passee",
    "class" => "demande_avis_passee",
    "title" => _("Demandes passees"),
    "right" => array("demande_avis_passee", "demande_avis_passee_tab", ),
    "open" => array("tab.php|demande_avis_passee", "form.php|demande_avis_passee", ),
);

$links[] = array(
    "href" => "../scr/tab.php?obj=demande_avis",
    "class" => "demande_avis",
    "title" => _("Exports"),
    "right" => array("demande_avis", "demande_avis_tab", ),
    "open" => array("tab.php|demande_avis", "form.php|demande_avis", ),
);

//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}


// Commentaire de la rubrique EXPORT qui n'est pas prévue d'être opérationnelle
// dans cette version
// {{{ Rubrique EXPORT
//
$rubrik = array(
   "title" => _("export / import"),
   "class" => "edition",
   "right" => "menu_export",
);
//
$links = array();

//
$links[] = array(
   "href" => "../scr/form.php?obj=sitadel&action=6&idx=0",
   "class" => "sitadel",
   "title" => _("export sitadel"),
   "right" => "export_sitadel",
   "open" => "form.php|sitadel",
);
//
$links[] = array(
   "href" => "../app/versement_archives.php",
   "class" => "versement_archives",
   "title" => _("versement aux archives"),
   "right" => "versement_archives",
   "open" => "versement_archives.php|",
);
//
$links[] = array(
   "href" => "../app/reqmo_pilot.php",
   "class" => "reqmo",
   "title" => _("statistiques a la demande"),
   "right" => "reqmo_pilot",
   "open" => "reqmo_pilot.php|",
);
//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}


// {{{ Rubrique PARAMETRAGE
//
$rubrik = array(
    "title" => _("parametrage dossiers"),
    "class" => "parametrage-dossier",
    "right" => "menu_parametrage",
);
//
$links = array();
//
$links[] = array(
    "class" => "category",
    "title" => _("dossiers"),
    "right" => array(
        "dossier_autorisation_type", "dossier_autorisation_type_tab",
        "dossier_autorisation_type_detaille",
        "dossier_autorisation_type_detaille_tab", "dossier_instruction_type",
        "dossier_instruction_type_tab", "cerfa", "cerfa_tab",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "cerfa", "cerfa_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=cerfa",
    "class" => "cerfa",
    "title" => _("cerfa"),
    "right" => array("cerfa", "cerfa_tab", ),
    "open" => array("tab.php|cerfa", "form.php|cerfa", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_autorisation_type", "dossier_autorisation_type_tab",
        "dossier_autorisation_type_detaille",
        "dossier_autorisation_type_detaille_tab", "dossier_instruction_type",
        "dossier_instruction_type_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_autorisation_type",
    "class" => "dossier_autorisation_type",
    "title" => _("type DA"),
    "right" => array("dossier_autorisation_type", "dossier_autorisation_type_tab", ),
    "open" => array("tab.php|dossier_autorisation_type", "form.php|dossier_autorisation_type", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_autorisation_type_detaille",
    "class" => "dossier_autorisation_type_detaille",
    "title" => _("type DA detaille"),
    "right" => array("dossier_autorisation_type_detaille", "dossier_autorisation_type_detaille_tab", ),
    "open" => array("tab.php|dossier_autorisation_type_detaille", "form.php|dossier_autorisation_type_detaille", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_type",
    "class" => "dossier_instruction_type",
    "title" => _("type DI"),
    "right" => array("dossier_instruction_type", "dossier_instruction_type_tab", ),
    "open" => array("tab.php|dossier_instruction_type", "form.php|dossier_instruction_type", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "contrainte", "contrainte_tab",
        "contrainte_souscategorie", "contrainte_souscategorie_tab",
        "contrainte_categorie", "contrainte_categorie_tab"
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=contrainte",
    "class" => "contrainte",
    "title" => _("contrainte"),
    "right" => array("contrainte", "contrainte_tab", ),
    "open" => array(
        "tab.php|contrainte",
        "form.php|contrainte[action=0]",
        "form.php|contrainte[action=1]",
        "form.php|contrainte[action=2]",
        "form.php|contrainte[action=3]",
        ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=contrainte&action=100&idx=0",
    "class" => "contrainte",
    "title" => _("synchronisation des contraintes"),
    "right" => array("contrainte", "contrainte_synchronisation", ),
    "open" => array("form.php|contrainte[action=100]", ),
    "parameters" => array(
        "option_sig" => "sig_externe", 
    ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("demandes"),
    "right" => array(
        "demande_type",
        "demande_type_tab", "demande_nature", "demande_nature_tab",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "demande_type",
        "demande_type_tab", "demande_nature", "demande_nature_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=demande_nature",
    "class" => "demande_nature",
    "title" => _("nature"),
    "right" => array("demande_nature", "demande_nature_tab", ),
    "open" => array("tab.php|demande_nature", "form.php|demande_nature", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=demande_type",
    "class" => "demande_type",
    "title" => _("type"),
    "right" => array("demande_type", "demande_type_tab",),
    "open" => array("tab.php|demande_type", "form.php|demande_type", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("workflows"),
    "right" => array(
        "workflows",
        "action", "action_tab", "etat",
        "etat_tab", "evenement", "evenement_tab", "bible", "bible_tab", "avis_decision", 
        "avis_decision_tab", "avis_consultation", "avis_consultation_tab",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "workflows",
        "action", "action_tab", "etat",
        "etat_tab", "evenement", "evenement_tab", "bible", "bible_tab", "avis_decision",
        "avis_decision_tab", "avis_consultation", "avis_consultation_tab",
    ),
);
//
$links[] = array(
    "href" => "../app/workflows.php",
    "class" => "workflows",
    "title" => _("workflows"),
    "right" => array("workflows", ),
    "open" => array("workflows.php|", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "evenement", "evenement_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=evenement",
    "class" => "evenement",
    "title" => _("evenement"),
    "right" => array("evenement", "evenement_tab", ),
    "open" => array("tab.php|evenement", "form.php|evenement", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "action", "action_tab", "etat",
        "etat_tab", "avis_decision",
        "avis_decision_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etat",
    "class" => "workflow-etat",
    "title" => _("etat"),
    "right" => array("etat", "etat_tab", ),
    "open" => array("tab.php|etat", "form.php|etat", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=avis_decision",
    "class" => "avis_decision",
    "title" => _("avis decision"),
    "right" => array("avis_decision", "avis_decision_tab", ),
    "open" => array("tab.php|avis_decision", "form.php|avis_decision", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=action",
    "class" => "action",
    "title" => _("action"),
    "right" => array("action", "action_tab", ),
    "open" => array("tab.php|action", "form.php|action", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "bible", "bible_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=bible",
    "class" => "bible",
    "title" => _("bible"),
    "right" => array("bible", "bible_tab", ),
    "open" => array("tab.php|bible", "form.php|bible", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("editions"),
    "right" => array(
        "om_etat", "om_etat_tab", "om_sousetat", "om_sousetat_tab",
        "om_lettretype", "om_lettretype_tab", "om_requete", "om_requete_tab",
        "om_logo", "om_logo_tab",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_etat", "om_etat_tab", "om_lettretype", "om_lettretype_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_etat",
    "class" => "om_etat",
    "title" => _("om_etat"),
    "right" => array("om_etat", "om_etat_tab", ),
    "open" => array("tab.php|om_etat", "form.php|om_etat", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_lettretype",
    "class" => "om_lettretype",
    "title" => _("om_lettretype"),
    "right" => array("om_lettretype", "om_lettretype_tab"),
    "open" => array("tab.php|om_lettretype", "form.php|om_lettretype", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_logo", "om_logo_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_logo",
    "class" => "om_logo",
    "title" => _("om_logo"),
    "right" => array("om_logo", "om_logo_tab", ),
    "open" => array("tab.php|om_logo", "form.php|om_logo", ),
);
//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique PARAMETRAGE
//
$rubrik = array(
    "title" => _("parametrage"),
    "class" => "parametrage",
    "right" => "menu_parametrage",
);
//
$links = array();
//
$links[] = array(
    "href" => "../scr/tab.php?obj=civilite",
    "class" => "civilite",
    "title" => _("civilite"),
    "right" => array("civilite", "civilite_tab", ),
    "open" => array("tab.php|civilite", "form.php|civilite", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=arrondissement",
    "class" => "arrondissement",
    "title" => _("arrondissement"),
    "right" => array("arrondissement", "arrondissement_tab", ),
    "open" => array("tab.php|arrondissement", "form.php|arrondissement", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=quartier",
    "class" => "quartier",
    "title" => _("quartier"),
    "right" => array("quartier", "quartier_tab", ),
    "open" => array("tab.php|quartier", "form.php|quartier", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("Organisation"),
    "right" => array(
        "direction", "direction_tab", "division", "division_tab", "instructeur_qualite",
        "instructeur_qualite_tab", "instructeur", "instructeur_tab", "groupe",
        "groupe_tab", "genre", "genre_tab", "signataire_arrete", "signataire_arrete_tab",
        "taxe_amenagement_tab", "taxe_amenagement", 
    ),
);
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "direction", "direction_tab", "division", "division_tab", "instructeur_qualite",
        "instructeur_qualite_tab", "instructeur", "instructeur_tab", "groupe",
        "groupe_tab", "genre", "genre_tab", "signataire_arrete", "signataire_arrete_tab",
        "taxe_amenagement_tab", "taxe_amenagement", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=genre",
    "class" => "genre",
    "title" => _("genre"),
    "right" => array("genre", "genre_tab", ),
    "open" => array("tab.php|genre", "form.php|genre", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=groupe",
    "class" => "groupe",
    "title" => _("groupe"),
    "right" => array("groupe", "groupe_tab", ),
    "open" => array("tab.php|groupe", "form.php|groupe", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=direction",
    "class" => "direction",
    "title" => _("direction"),
    "right" => array("direction", "direction_tab", ),
    "open" => array("tab.php|direction", "form.php|direction", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=division",
    "class" => "division",
    "title" => _("division"),
    "right" => array("division", "division_tab", ),
    "open" => array("tab.php|division", "form.php|division", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=instructeur_qualite",
    "class" => "instructeur_qualite",
    "title" => _("instructeur_qualite"),
    "right" => array("instructeur_qualite", "instructeur_qualite_tab", ),
    "open" => array("tab.php|instructeur_qualite", "form.php|instructeur_qualite", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=instructeur",
    "class" => "instructeur",
    "title" => _("instructeur"),
    "right" => array("instructeur", "instructeur_tab", ),
    "open" => array("tab.php|instructeur", "form.php|instructeur", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=signataire_arrete",
    "class" => "signataire_arrete",
    "title" => _("signataire arrete"),
    "right" => array("signataire_arrete", "signataire_arrete", ),
    "open" => array("tab.php|signataire_arrete", "form.php|signataire_arrete", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=taxe_amenagement",
    "class" => "taxe_amenagement",
    "title" => _("taxes"),
    "right" => array("taxe_amenagement", "taxe_amenagement_tab", ),
    "open" => array("tab.php|taxe_amenagement", "form.php|taxe_amenagement", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("gestion des commissions"),
    "right" => array(
        "commission_type", "commission_type_tab",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "commission_type", "commission_type_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=commission_type",
    "class" => "commission-type",
    "title" => _("commission_type"),
    "right" => array("commission_type", "commission_type_tab", ),
    "open" => array("tab.php|commission_type", "form.php|commission_type", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("gestion des consultations"),
    "right" => array(
        "avis_consultation", "avis_consultation_tab", "service", "service_tab",
        "service_categorie", "service_categorie_tab",
        "lien_service_service_categorie", "lien_service_service_categorie_tab",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "avis_consultation", "avis_consultation_tab", "service", "service_tab",
        "service_categorie", "service_categorie_tab",
        "lien_service_service_categorie", "lien_service_service_categorie_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=avis_consultation",
    "class" => "avis_consultation",
    "title" => _("avis consultation"),
    "right" => array("avis_consultation", "avis_consultation_tab", ),
    "open" => array("tab.php|avis_consultation", "form.php|avis_consultation", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=service",
    "class" => "service",
    "title" => _("service"),
    "right" => array("service", "service_tab", ),
    "open" => array("tab.php|service", "form.php|service", ),
);
$links[] = array(
    "href" => "../scr/tab.php?obj=service_categorie",
    "class" => "service_categorie",
    "title" => _("thematique des services"),
    "right" => array("service_categorie", "service_categorie_tab", ),
    "open" => array("tab.php|service_categorie", "form.php|service_categorie", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("Gestion des dossiers"),
    "right" => array(
        "dossier_autorisation_type", "dossier_autorisation_type_tab",
        "dossier_autorisation_type_detaille",
        "dossier_autorisation_type_detaille_tab", "dossier_instruction_type",
        "dossier_instruction_type_tab",
        "autorite_competente", "autorite_competente_tab",
        "affectation_automatique", "affectation_automatique_tab", 
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_autorisation_type", "dossier_autorisation_type_tab",
        "dossier_autorisation_type_detaille",
        "dossier_autorisation_type_detaille_tab", "dossier_instruction_type",
        "dossier_instruction_type_tab",
        "autorite_competente", "autorite_competente_tab",
        "affectation_automatique", "affectation_automatique_tab", 
        
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etat_dossier_autorisation",
    "class" => "etat_dossier_autorisation",
    "title" => _("etat dossiers autorisations"),
    "right" => array("etat_dossier_autorisation", "etat_dossier_autorisation_tab", ),
    "open" => array("tab.php|etat_dossier_autorisation", "form.php|etat_dossier_autorisation", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=affectation_automatique",
    "class" => "affectation_automatique",
    "title" => _("affectation automatique"),
    "right" => array("affectation_automatique", "affectation_automatique_tab", ),
    "open" => array("tab.php|affectation_automatique", "form.php|affectation_automatique", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=autorite_competente",
    "class" => "autorite_competente",
    "title" => _("autorite")." "._("competente"),
    "right" => array("autorite_competente", "autorite_competente_tab", ),
    "open" => array("tab.php|autorite_competente", "form.php|autorite_competente", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=phase",
    "class" => "phase",
    "title" => _("phase"),
    "right" => array("phase", "phase_tab", ),
    "open" => array("tab.php|phase", "form.php|phase", ),
);

//Gestion des pièces
$links[] = array(
    "class" => "category",
    "title" => _("Gestion des pièces"),
    "right" => array(
        "document_numerise_type_categorie", "document_numerise_type_categorie_tab",
        "document_numerise_type",
        "document_numerise_type_tab", "document_numerise_traitement_metadonnees",
        "document_numerise_traitement_metadonnees_executer", 
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "document_numerise_type_categorie", "document_numerise_type_categorie_tab",
        "document_numerise_type",
        "document_numerise_type_tab", "document_numerise_traitement_metadonnees",
        "document_numerise_traitement_metadonnees_executer", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=document_numerise_type_categorie",
    "class" => "document_numerise_type_categorie",
    "title" => _("Catégorie des pièces"),
    "right" => array(
        "document_numerise_type_categorie",
        "document_numerise_type_categorie_tab",
        ),
    "open" => array(
        "tab.php|document_numerise_type_categorie",
        "form.php|document_numerise_type_categorie",
        ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=document_numerise_type",
    "class" => "document_numerise_type",
    "title" => _("Type des pièces"),
    "right" => array(
        "document_numerise_type",
        "document_numerise_type_tab",
        ),
    "open" => array(
        "tab.php|document_numerise_type",
        "form.php|document_numerise_type[action=0]",
        "form.php|document_numerise_type[action=1]",
        "form.php|document_numerise_type[action=2]",
        "form.php|document_numerise_type[action=3]",
        ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=document_numerise_traitement_metadonnees&action=100&idx=0",
    "class" => "document_numerise_traitement_metadonnees",
    "title" => _("Mise à jour des métadonnées"),
    "description" => _("Mettre à jour les métadonnées de tous les documents numérisés."),
    "right" => array(
        "document_numerise_traitement_metadonnees",
        "document_numerise_traitement_metadonnees_executer",
        ),
    "open" => array("form.php|document_numerise_traitement_metadonnees", ),
);

// Gestion des contentieux
$links[] = array(
    "class" => "category",
    "title" => _("Gestion des contentieux"),
    "right" => array(
        "objet_recours", "objet_recours_tab", "moyen_souleve", "moyen_souleve_tab",
        "moyen_retenu_juge", "moyen_retenu_juge_tab", 
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "objet_recours", "objet_recours_tab", "moyen_souleve", "moyen_souleve_tab",
        "moyen_retenu_juge", "moyen_retenu_juge_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=objet_recours",
    "class" => "objet_recours",
    "title" => _("objet_recours"),
    "right" => array(
        "objet_recours", "objet_recours_tab",
    ),
    "open" => array(
        "tab.php|objet_recours", "form.php|objet_recours",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=moyen_souleve",
    "class" => "moyen_souleve",
    "title" => _("moyen_souleve"),
    "right" => array(
        "moyen_souleve", "moyen_souleve_tab",
    ),
    "open" => array(
        "tab.php|moyen_souleve", "form.php|moyen_souleve",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=moyen_retenu_juge",
    "class" => "moyen_retenu_juge",
    "title" => _("moyen_retenu_juge"),
    "right" => array(
        "moyen_retenu_juge", "moyen_retenu_juge_tab",
    ),
    "open" => array(
        "tab.php|moyen_retenu_juge", "form.php|moyen_retenu_juge",
    ),
);

//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique ADMINISTRATION
//
$rubrik = array(
    "title" => _("administration"),
    "class" => "administration",
    "right" => "menu_administration",
);
//
$links = array();
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_collectivite",
    "class" => "collectivite",
    "title" => _("om_collectivite"),
    "right" => array("om_collectivite", "om_collectivite_tab", ),
    "open" => array("tab.php|om_collectivite", "form.php|om_collectivite", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_parametre",
    "class" => "parametre",
    "title" => _("om_parametre"),
    "right" => array("om_parametre", "om_parametre_tab", ),
    "open" => array("tab.php|om_parametre", "form.php|om_parametre", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("gestion des utilisateurs"),
    "right" => array(
        "om_utilisateur", "om_utilisateur_tab", "om_profil", "om_profil_tab",
        "om_droit", "om_droit_tab", "directory",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_utilisateur", "om_utilisateur_tab", "om_profil", "om_profil_tab",
        "om_droit", "om_droit_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_profil",
    "class" => "profil",
    "title" => _("om_profil"),
    "right" => array("om_profil", "om_profil_tab", ),
    "open" => array("tab.php|om_profil", "form.php|om_profil", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_droit",
    "class" => "droit",
    "title" => _("om_droit"),
    "right" => array("om_droit", "om_droit_tab", ),
    "open" => array("tab.php|om_droit", "form.php|om_droit", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_utilisateur",
    "class" => "utilisateur",
    "title" => _("om_utilisateur"),
    "right" => array("om_utilisateur", "om_utilisateur_tab", ),
    "open" => array(
        "tab.php|om_utilisateur",
        "form.php|om_utilisateur[action=0]",
        "form.php|om_utilisateur[action=1]",
        "form.php|om_utilisateur[action=2]",
        "form.php|om_utilisateur[action=3]",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array("om_utilisateur", "om_utilisateur_synchroniser", ),
    "parameters" => array("isDirectoryOptionEnabled" => true, ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=om_utilisateur&idx=0&action=11",
    "class" => "annuaire",
    "title" => _("annuaire"),
    "right" => array("om_utilisateur", "om_utilisateur_synchroniser", ),
    "open" => array("form.php|om_utilisateur[action=11]", ),
    "parameters" => array("isDirectoryOptionEnabled" => true, ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("tableaux de bord"),
    "right" => array(
        "om_widget", "om_widget_tab", "om_dashboard",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_widget", "om_widget_tab", "om_dashboard",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_widget",
    "class" => "om_widget",
    "title" => _("om_widget"),
    "right" => array("om_widget", "om_widget_tab", ),
    "open" => array("tab.php|om_widget", "form.php|om_widget", ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=om_dashboard&amp;idx=0&amp;action=4",
    "class" => "om_dashboard",
    "title" => _("composition"),
    "right" => array("om_dashboard", ),
    "open" => array("form.php|om_dashboard[action=4]", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("sig"),
    "right" => array(
        "om_sig_map", "om_sig_map_tab", "om_sig_flux", "om_sig_flux_tab", "om_sig_extent", "om_sig_extent_tab",
    ),
    "parameters" => array("option_localisation" => "sig_interne", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_sig_map", "om_sig_map_tab", "om_sig_flux", "om_sig_flux_tab", "om_sig_extent", "om_sig_extent_tab",
    ),
    "parameters" => array("option_localisation" => "sig_interne", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_sig_extent",
    "class" => "om_sig_extent",
    "title" => _("om_sig_extent"),
    "right" => array("om_sig_extent", "om_sig_extent_tab", ),
    "open" => array("tab.php|om_sig_extent", "form.php|om_sig_extent", ),
    "parameters" => array("option_localisation" => "sig_interne", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_sig_map",
    "class" => "om_sig_map",
    "title" => _("om_sig_map"),
    "right" => array("om_sig_map", "om_sig_map_tab", ),
    "open" => array("tab.php|om_sig_map", "form.php|om_sig_map", ),
    "parameters" => array("option_localisation" => "sig_interne", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_sig_flux",
    "class" => "om_sig_flux",
    "title" => _("om_sig_flux"),
    "right" => array("om_sig_flux", "om_sig_flux_tab", ),
    "open" => array("tab.php|om_sig_flux", "form.php|om_sig_flux", ),
    "parameters" => array("option_localisation" => "sig_interne", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("options avancees"),
    "right" => array("import", "gen", "om_requete", "om_requete_tab",
                     "om_sousetat", "om_sousetat_tab",),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "interface_referentiel_erp",
    ),
);
//
$links[] = array(
    "href" => "../app/settings.php?controlpanel=interface_referentiel_erp",
    "class" => "interface_referentiel_erp",
    "title" => _("interface_referentiel_erp"),
    "right" => array("interface_referentiel_erp", ),
    "open" => array("settings.php|[controlpanel=interface_referentiel_erp]", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_sousetat", "om_sousetat_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_sousetat",
    "class" => "om_sousetat",
    "title" => _("om_sousetat"),
    "right" => array("om_sousetat", "om_sousetat_tab", ),
    "open" => array("tab.php|om_sousetat", "form.php|om_sousetat", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array("om_requete", "om_requete_tab", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_requete",
    "class" => "om_requete",
    "title" => _("om_requete"),
    "right" => array("om_requete", "om_requete_tab", ),
    "open" => array("tab.php|om_requete", "form.php|om_requete", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array("import", ),
);
//
$links[] = array(
    "href" => "../scr/import.php",
    "class" => "import",
    "title" => _("Import"),
    "right" => array("import", ),
    "open" => array("import.php|", ),
);
//
$links[] = array(
    "href" => "../app/import_specific.php",
    "class" => "import_specific",
    "title" => _("Import specifique"),
    "right" => array("import", ),
    "open" => array("import_specific.php|", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array("gen", ),
);
//
$links[] = array(
    "title" => _("Generateur"),
    "href" => "../scr/gen.php",
    "class" => "generator",
    "right" => array("gen", ),
    "open" => array(
        "gen.php|","genauto.php|", "gensup.php|", "genfull.php|",
        "genetat.php|", "gensousetat.php|", "genlettretype.php|",
        "genimport.php|",
    ),
);
//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

?>
