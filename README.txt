openADS ReadMe
==============

Avertissement
-------------

openADS est une application sensible, nécessitant un paramétrage précis.
Un mauvais paramétrage peut entrainer le non respect du code de l'urbanisme.
Ni l'équipe du projet openADS ni le chef de projet ne peuvent être tenus
pour responsables d'un éventuel dysfonctionnement comme ceci est précisé dans
la licence jointe. Vous pouvez, si vous le souhaitez, faire appel a un
prestataire spécialisé qui peut fournir support, hot-line, maintenance, et
garantir le fonctionnement en environnement de production.


Description
-----------

openADS est un logiciel libre de gestion de l'urbanisme. Il permet le suivi de
l'instruction des demandes de permis et d'autorisations. openADS comprend
la gestion de l'enchainement des tâches, des délais légaux et de la prise en
compte des contraintes du PLU, avec possibilité d'affichage cartographique
et bien plus encore.

Toutes les informations sur http://www.openmairie.org/


Documentation
-------------

Toutes les instructions pour l'installation, l'utilisation et la prise en main
du logiciel dans la documentation en ligne :
http://docs.openmairie.org/?project=openads&format=html


Licence
-------

Voir le fichier LICENSE.txt

