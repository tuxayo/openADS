<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("commission");
$tableSelect=DB_PREFIXE."commission";
$champs=array(
    "commission",
    "code",
    "commission_type",
    "libelle",
    "date_commission",
    "heure_commission",
    "lieu_adresse_ligne1",
    "lieu_adresse_ligne2",
    "lieu_salle",
    "listes_de_diffusion",
    "participants",
    "om_fichier_commission_ordre_jour",
    "om_final_commission_ordre_jour",
    "om_fichier_commission_compte_rendu",
    "om_final_commission_compte_rendu",
    "om_collectivite");
//champs select
$sql_commission_type="SELECT commission_type.commission_type, commission_type.libelle FROM ".DB_PREFIXE."commission_type WHERE ((commission_type.om_validite_debut IS NULL AND (commission_type.om_validite_fin IS NULL OR commission_type.om_validite_fin > CURRENT_DATE)) OR (commission_type.om_validite_debut <= CURRENT_DATE AND (commission_type.om_validite_fin IS NULL OR commission_type.om_validite_fin > CURRENT_DATE))) ORDER BY commission_type.libelle ASC";
$sql_commission_type_by_id = "SELECT commission_type.commission_type, commission_type.libelle FROM ".DB_PREFIXE."commission_type WHERE commission_type = <idx>";
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
?>