<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("lien_service_service_categorie");
$tableSelect=DB_PREFIXE."lien_service_service_categorie";
$champs=array(
    "lien_service_service_categorie",
    "service_categorie",
    "service");
//champs select
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = '<idx>'";
$sql_service_categorie="SELECT service_categorie.service_categorie, service_categorie.libelle FROM ".DB_PREFIXE."service_categorie ORDER BY service_categorie.libelle ASC";
$sql_service_categorie_by_id = "SELECT service_categorie.service_categorie, service_categorie.libelle FROM ".DB_PREFIXE."service_categorie WHERE service_categorie = <idx>";
?>