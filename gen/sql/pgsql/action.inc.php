<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("action");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."action";
// SELECT 
$champAffiche = array(
    'action.action as "'._("action").'"',
    'action.libelle as "'._("libelle").'"',
    'action.regle_etat as "'._("regle_etat").'"',
    'action.regle_delai as "'._("regle_delai").'"',
    'action.regle_accord_tacite as "'._("regle_accord_tacite").'"',
    'action.regle_avis as "'._("regle_avis").'"',
    'action.regle_date_limite as "'._("regle_date_limite").'"',
    'action.regle_date_notification_delai as "'._("regle_date_notification_delai").'"',
    'action.regle_date_complet as "'._("regle_date_complet").'"',
    'action.regle_date_validite as "'._("regle_date_validite").'"',
    'action.regle_date_decision as "'._("regle_date_decision").'"',
    'action.regle_date_chantier as "'._("regle_date_chantier").'"',
    'action.regle_date_achevement as "'._("regle_date_achevement").'"',
    'action.regle_date_conformite as "'._("regle_date_conformite").'"',
    'action.regle_date_rejet as "'._("regle_date_rejet").'"',
    'action.regle_date_dernier_depot as "'._("regle_date_dernier_depot").'"',
    'action.regle_date_limite_incompletude as "'._("regle_date_limite_incompletude").'"',
    'action.regle_delai_incompletude as "'._("regle_delai_incompletude").'"',
    'action.regle_autorite_competente as "'._("regle_autorite_competente").'"',
    'action.regle_date_cloture_instruction as "'._("regle_date_cloture_instruction").'"',
    'action.regle_date_premiere_visite as "'._("regle_date_premiere_visite").'"',
    'action.regle_date_derniere_visite as "'._("regle_date_derniere_visite").'"',
    'action.regle_date_contradictoire as "'._("regle_date_contradictoire").'"',
    'action.regle_date_retour_contradictoire as "'._("regle_date_retour_contradictoire").'"',
    'action.regle_date_ait as "'._("regle_date_ait").'"',
    'action.regle_date_transmission_parquet as "'._("regle_date_transmission_parquet").'"',
    'action.regle_donnees_techniques1 as "'._("regle_donnees_techniques1").'"',
    'action.regle_donnees_techniques2 as "'._("regle_donnees_techniques2").'"',
    'action.regle_donnees_techniques3 as "'._("regle_donnees_techniques3").'"',
    'action.regle_donnees_techniques4 as "'._("regle_donnees_techniques4").'"',
    'action.regle_donnees_techniques5 as "'._("regle_donnees_techniques5").'"',
    'action.cible_regle_donnees_techniques1 as "'._("cible_regle_donnees_techniques1").'"',
    'action.cible_regle_donnees_techniques2 as "'._("cible_regle_donnees_techniques2").'"',
    'action.cible_regle_donnees_techniques3 as "'._("cible_regle_donnees_techniques3").'"',
    'action.cible_regle_donnees_techniques4 as "'._("cible_regle_donnees_techniques4").'"',
    'action.cible_regle_donnees_techniques5 as "'._("cible_regle_donnees_techniques5").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'action.action as "'._("action").'"',
    'action.libelle as "'._("libelle").'"',
    'action.regle_etat as "'._("regle_etat").'"',
    'action.regle_delai as "'._("regle_delai").'"',
    'action.regle_accord_tacite as "'._("regle_accord_tacite").'"',
    'action.regle_avis as "'._("regle_avis").'"',
    'action.regle_date_limite as "'._("regle_date_limite").'"',
    'action.regle_date_notification_delai as "'._("regle_date_notification_delai").'"',
    'action.regle_date_complet as "'._("regle_date_complet").'"',
    'action.regle_date_validite as "'._("regle_date_validite").'"',
    'action.regle_date_decision as "'._("regle_date_decision").'"',
    'action.regle_date_chantier as "'._("regle_date_chantier").'"',
    'action.regle_date_achevement as "'._("regle_date_achevement").'"',
    'action.regle_date_conformite as "'._("regle_date_conformite").'"',
    'action.regle_date_rejet as "'._("regle_date_rejet").'"',
    'action.regle_date_dernier_depot as "'._("regle_date_dernier_depot").'"',
    'action.regle_date_limite_incompletude as "'._("regle_date_limite_incompletude").'"',
    'action.regle_delai_incompletude as "'._("regle_delai_incompletude").'"',
    'action.regle_autorite_competente as "'._("regle_autorite_competente").'"',
    'action.regle_date_cloture_instruction as "'._("regle_date_cloture_instruction").'"',
    'action.regle_date_premiere_visite as "'._("regle_date_premiere_visite").'"',
    'action.regle_date_derniere_visite as "'._("regle_date_derniere_visite").'"',
    'action.regle_date_contradictoire as "'._("regle_date_contradictoire").'"',
    'action.regle_date_retour_contradictoire as "'._("regle_date_retour_contradictoire").'"',
    'action.regle_date_ait as "'._("regle_date_ait").'"',
    'action.regle_date_transmission_parquet as "'._("regle_date_transmission_parquet").'"',
    'action.regle_donnees_techniques1 as "'._("regle_donnees_techniques1").'"',
    'action.regle_donnees_techniques2 as "'._("regle_donnees_techniques2").'"',
    'action.regle_donnees_techniques3 as "'._("regle_donnees_techniques3").'"',
    'action.regle_donnees_techniques4 as "'._("regle_donnees_techniques4").'"',
    'action.regle_donnees_techniques5 as "'._("regle_donnees_techniques5").'"',
    'action.cible_regle_donnees_techniques1 as "'._("cible_regle_donnees_techniques1").'"',
    'action.cible_regle_donnees_techniques2 as "'._("cible_regle_donnees_techniques2").'"',
    'action.cible_regle_donnees_techniques3 as "'._("cible_regle_donnees_techniques3").'"',
    'action.cible_regle_donnees_techniques4 as "'._("cible_regle_donnees_techniques4").'"',
    'action.cible_regle_donnees_techniques5 as "'._("cible_regle_donnees_techniques5").'"',
    );
$tri="ORDER BY action.libelle ASC NULLS LAST";
$edition="action";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'evenement',
    'instruction',
);

?>