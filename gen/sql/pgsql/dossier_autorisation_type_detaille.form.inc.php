<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("dossier_autorisation_type_detaille");
$tableSelect=DB_PREFIXE."dossier_autorisation_type_detaille";
$champs=array(
    "dossier_autorisation_type_detaille",
    "code",
    "libelle",
    "description",
    "dossier_autorisation_type",
    "cerfa",
    "cerfa_lot",
    "duree_validite_parametrage");
//champs select
$sql_cerfa="SELECT cerfa.cerfa, cerfa.libelle FROM ".DB_PREFIXE."cerfa WHERE ((cerfa.om_validite_debut IS NULL AND (cerfa.om_validite_fin IS NULL OR cerfa.om_validite_fin > CURRENT_DATE)) OR (cerfa.om_validite_debut <= CURRENT_DATE AND (cerfa.om_validite_fin IS NULL OR cerfa.om_validite_fin > CURRENT_DATE))) ORDER BY cerfa.libelle ASC";
$sql_cerfa_by_id = "SELECT cerfa.cerfa, cerfa.libelle FROM ".DB_PREFIXE."cerfa WHERE cerfa = <idx>";
$sql_cerfa_lot="SELECT cerfa.cerfa, cerfa.libelle FROM ".DB_PREFIXE."cerfa WHERE ((cerfa.om_validite_debut IS NULL AND (cerfa.om_validite_fin IS NULL OR cerfa.om_validite_fin > CURRENT_DATE)) OR (cerfa.om_validite_debut <= CURRENT_DATE AND (cerfa.om_validite_fin IS NULL OR cerfa.om_validite_fin > CURRENT_DATE))) ORDER BY cerfa.libelle ASC";
$sql_cerfa_lot_by_id = "SELECT cerfa.cerfa, cerfa.libelle FROM ".DB_PREFIXE."cerfa WHERE cerfa = <idx>";
$sql_dossier_autorisation_type="SELECT dossier_autorisation_type.dossier_autorisation_type, dossier_autorisation_type.libelle FROM ".DB_PREFIXE."dossier_autorisation_type ORDER BY dossier_autorisation_type.libelle ASC";
$sql_dossier_autorisation_type_by_id = "SELECT dossier_autorisation_type.dossier_autorisation_type, dossier_autorisation_type.libelle FROM ".DB_PREFIXE."dossier_autorisation_type WHERE dossier_autorisation_type = <idx>";
?>