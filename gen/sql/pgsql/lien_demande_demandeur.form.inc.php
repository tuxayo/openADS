<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("lien_demande_demandeur");
$tableSelect=DB_PREFIXE."lien_demande_demandeur";
$champs=array(
    "lien_demande_demandeur",
    "petitionnaire_principal",
    "demande",
    "demandeur");
//champs select
$sql_demande="SELECT demande.demande, demande.dossier_autorisation_type_detaille FROM ".DB_PREFIXE."demande ORDER BY demande.dossier_autorisation_type_detaille ASC";
$sql_demande_by_id = "SELECT demande.demande, demande.dossier_autorisation_type_detaille FROM ".DB_PREFIXE."demande WHERE demande = <idx>";
$sql_demandeur="SELECT demandeur.demandeur, demandeur.type_demandeur FROM ".DB_PREFIXE."demandeur ORDER BY demandeur.type_demandeur ASC";
$sql_demandeur_by_id = "SELECT demandeur.demandeur, demandeur.type_demandeur FROM ".DB_PREFIXE."demandeur WHERE demandeur = <idx>";
?>