<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("affectation_automatique");
$tableSelect=DB_PREFIXE."affectation_automatique";
$champs=array(
    "affectation_automatique",
    "arrondissement",
    "quartier",
    "section",
    "instructeur",
    "dossier_autorisation_type_detaille",
    "om_collectivite",
    "instructeur_2");
//champs select
$sql_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement ORDER BY arrondissement.libelle ASC";
$sql_arrondissement_by_id = "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE arrondissement = <idx>";
$sql_dossier_autorisation_type_detaille="SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille ORDER BY dossier_autorisation_type_detaille.libelle ASC";
$sql_dossier_autorisation_type_detaille_by_id = "SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille WHERE dossier_autorisation_type_detaille = <idx>";
$sql_instructeur="SELECT instructeur.instructeur, instructeur.nom FROM ".DB_PREFIXE."instructeur WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE))) ORDER BY instructeur.nom ASC";
$sql_instructeur_by_id = "SELECT instructeur.instructeur, instructeur.nom FROM ".DB_PREFIXE."instructeur WHERE instructeur = <idx>";
$sql_instructeur_2="SELECT instructeur.instructeur, instructeur.nom FROM ".DB_PREFIXE."instructeur WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE))) ORDER BY instructeur.nom ASC";
$sql_instructeur_2_by_id = "SELECT instructeur.instructeur, instructeur.nom FROM ".DB_PREFIXE."instructeur WHERE instructeur = <idx>";
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
$sql_quartier="SELECT quartier.quartier, quartier.libelle FROM ".DB_PREFIXE."quartier ORDER BY quartier.libelle ASC";
$sql_quartier_by_id = "SELECT quartier.quartier, quartier.libelle FROM ".DB_PREFIXE."quartier WHERE quartier = <idx>";
?>