<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_parcelle");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_parcelle
    LEFT JOIN ".DB_PREFIXE."dossier 
        ON dossier_parcelle.dossier=dossier.dossier ";
// SELECT 
$champAffiche = array(
    'dossier_parcelle.dossier_parcelle as "'._("dossier_parcelle").'"',
    'dossier.annee as "'._("dossier").'"',
    'dossier_parcelle.parcelle as "'._("parcelle").'"',
    'dossier_parcelle.libelle as "'._("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'dossier_parcelle.dossier_parcelle as "'._("dossier_parcelle").'"',
    'dossier.annee as "'._("dossier").'"',
    'dossier_parcelle.parcelle as "'._("parcelle").'"',
    'dossier_parcelle.libelle as "'._("libelle").'"',
    );
$tri="ORDER BY dossier_parcelle.libelle ASC NULLS LAST";
$edition="dossier_parcelle";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
);
// Filtre listing sous formulaire - dossier
if (in_array($retourformulaire, $foreign_keys_extended["dossier"])) {
    $selection = " WHERE (dossier_parcelle.dossier = '".$f->db->escapeSimple($idxformulaire)."') ";
}

?>