<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("service");
$tableSelect=DB_PREFIXE."service";
$champs=array(
    "abrege",
    "libelle",
    "adresse",
    "adresse2",
    "cp",
    "ville",
    "email",
    "delai",
    "service",
    "consultation_papier",
    "notification_email",
    "om_validite_debut",
    "om_validite_fin",
    "type_consultation",
    "edition",
    "om_collectivite",
    "delai_type");
//champs select
$sql_edition="SELECT om_etat.om_etat, om_etat.libelle FROM ".DB_PREFIXE."om_etat ORDER BY om_etat.libelle ASC";
$sql_edition_by_id = "SELECT om_etat.om_etat, om_etat.libelle FROM ".DB_PREFIXE."om_etat WHERE om_etat = <idx>";
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
?>