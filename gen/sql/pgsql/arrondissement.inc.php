<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("arrondissement");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."arrondissement";
// SELECT 
$champAffiche = array(
    'arrondissement.arrondissement as "'._("arrondissement").'"',
    'arrondissement.libelle as "'._("libelle").'"',
    'arrondissement.code_postal as "'._("code_postal").'"',
    'arrondissement.code_impots as "'._("code_impots").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'arrondissement.arrondissement as "'._("arrondissement").'"',
    'arrondissement.libelle as "'._("libelle").'"',
    'arrondissement.code_postal as "'._("code_postal").'"',
    'arrondissement.code_impots as "'._("code_impots").'"',
    );
$tri="ORDER BY arrondissement.libelle ASC NULLS LAST";
$edition="arrondissement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'affectation_automatique',
    'demande',
    'dossier_autorisation',
    'quartier',
);

?>