<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("avis_decision");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."avis_decision";
// SELECT 
$champAffiche = array(
    'avis_decision.avis_decision as "'._("avis_decision").'"',
    'avis_decision.libelle as "'._("libelle").'"',
    'avis_decision.typeavis as "'._("typeavis").'"',
    'avis_decision.sitadel as "'._("sitadel").'"',
    'avis_decision.sitadel_motif as "'._("sitadel_motif").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'avis_decision.avis_decision as "'._("avis_decision").'"',
    'avis_decision.libelle as "'._("libelle").'"',
    'avis_decision.typeavis as "'._("typeavis").'"',
    'avis_decision.sitadel as "'._("sitadel").'"',
    'avis_decision.sitadel_motif as "'._("sitadel_motif").'"',
    );
$tri="ORDER BY avis_decision.libelle ASC NULLS LAST";
$edition="avis_decision";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'dossier',
    'dossier_autorisation',
    'evenement',
    'instruction',
);

?>