<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("lien_lot_demandeur");
$tableSelect=DB_PREFIXE."lien_lot_demandeur";
$champs=array(
    "lien_lot_demandeur",
    "lot",
    "demandeur",
    "petitionnaire_principal");
//champs select
$sql_demandeur="SELECT demandeur.demandeur, demandeur.type_demandeur FROM ".DB_PREFIXE."demandeur ORDER BY demandeur.type_demandeur ASC";
$sql_demandeur_by_id = "SELECT demandeur.demandeur, demandeur.type_demandeur FROM ".DB_PREFIXE."demandeur WHERE demandeur = <idx>";
$sql_lot="SELECT lot.lot, lot.libelle FROM ".DB_PREFIXE."lot ORDER BY lot.libelle ASC";
$sql_lot_by_id = "SELECT lot.lot, lot.libelle FROM ".DB_PREFIXE."lot WHERE lot = <idx>";
?>