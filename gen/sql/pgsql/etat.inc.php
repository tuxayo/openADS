<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("etat");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."etat";
// SELECT 
$champAffiche = array(
    'etat.etat as "'._("etat").'"',
    'etat.libelle as "'._("libelle").'"',
    'etat.statut as "'._("statut").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'etat.etat as "'._("etat").'"',
    'etat.libelle as "'._("libelle").'"',
    'etat.statut as "'._("statut").'"',
    );
$tri="ORDER BY etat.libelle ASC NULLS LAST";
$edition="etat";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'dossier',
    'evenement',
    'instruction',
    'lien_demande_type_etat',
    'transition',
);

?>