<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("administration")." -> "._("om_sig_map");
$tableSelect=DB_PREFIXE."om_sig_map";
$champs=array(
    "om_sig_map",
    "om_collectivite",
    "id",
    "libelle",
    "actif",
    "zoom",
    "fond_osm",
    "fond_bing",
    "fond_sat",
    "layer_info",
    "projection_externe",
    "url",
    "om_sql",
    "retour",
    "util_idx",
    "util_reqmo",
    "util_recherche",
    "source_flux",
    "fond_default",
    "om_sig_extent",
    "restrict_extent",
    "sld_marqueur",
    "sld_data",
    "point_centrage");
//champs select
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
$sql_om_sig_extent="SELECT om_sig_extent.om_sig_extent, om_sig_extent.nom FROM ".DB_PREFIXE."om_sig_extent ORDER BY om_sig_extent.nom ASC";
$sql_om_sig_extent_by_id = "SELECT om_sig_extent.om_sig_extent, om_sig_extent.nom FROM ".DB_PREFIXE."om_sig_extent WHERE om_sig_extent = <idx>";
$sql_source_flux="SELECT om_sig_map.om_sig_map, om_sig_map.libelle FROM ".DB_PREFIXE."om_sig_map ORDER BY om_sig_map.libelle ASC";
$sql_source_flux_by_id = "SELECT om_sig_map.om_sig_map, om_sig_map.libelle FROM ".DB_PREFIXE."om_sig_map WHERE om_sig_map = <idx>";
?>