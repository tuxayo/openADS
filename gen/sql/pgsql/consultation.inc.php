<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("consultation");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."consultation
    LEFT JOIN ".DB_PREFIXE."avis_consultation 
        ON consultation.avis_consultation=avis_consultation.avis_consultation 
    LEFT JOIN ".DB_PREFIXE."dossier 
        ON consultation.dossier=dossier.dossier 
    LEFT JOIN ".DB_PREFIXE."service 
        ON consultation.service=service.service ";
// SELECT 
$champAffiche = array(
    'consultation.consultation as "'._("consultation").'"',
    'dossier.annee as "'._("dossier").'"',
    'to_char(consultation.date_envoi ,\'DD/MM/YYYY\') as "'._("date_envoi").'"',
    'to_char(consultation.date_retour ,\'DD/MM/YYYY\') as "'._("date_retour").'"',
    'to_char(consultation.date_limite ,\'DD/MM/YYYY\') as "'._("date_limite").'"',
    'service.libelle as "'._("service").'"',
    'avis_consultation.libelle as "'._("avis_consultation").'"',
    'to_char(consultation.date_reception ,\'DD/MM/YYYY\') as "'._("date_reception").'"',
    'consultation.fichier as "'._("fichier").'"',
    "case consultation.lu when 't' then 'Oui' else 'Non' end as \""._("lu")."\"",
    'consultation.code_barres as "'._("code_barres").'"',
    'consultation.om_fichier_consultation as "'._("om_fichier_consultation").'"',
    "case consultation.om_final_consultation when 't' then 'Oui' else 'Non' end as \""._("om_final_consultation")."\"",
    "case consultation.marque when 't' then 'Oui' else 'Non' end as \""._("marque")."\"",
    "case consultation.visible when 't' then 'Oui' else 'Non' end as \""._("visible")."\"",
    );
//
$champNonAffiche = array(
    'consultation.motivation as "'._("motivation").'"',
    );
//
$champRecherche = array(
    'consultation.consultation as "'._("consultation").'"',
    'dossier.annee as "'._("dossier").'"',
    'service.libelle as "'._("service").'"',
    'avis_consultation.libelle as "'._("avis_consultation").'"',
    'consultation.fichier as "'._("fichier").'"',
    'consultation.code_barres as "'._("code_barres").'"',
    'consultation.om_fichier_consultation as "'._("om_fichier_consultation").'"',
    );
$tri="ORDER BY dossier.annee ASC NULLS LAST";
$edition="consultation";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "avis_consultation" => array("avis_consultation", ),
    "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - avis_consultation
if (in_array($retourformulaire, $foreign_keys_extended["avis_consultation"])) {
    $selection = " WHERE (consultation.avis_consultation = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier
if (in_array($retourformulaire, $foreign_keys_extended["dossier"])) {
    $selection = " WHERE (consultation.dossier = '".$f->db->escapeSimple($idxformulaire)."') ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (consultation.service = ".intval($idxformulaire).") ";
}

?>