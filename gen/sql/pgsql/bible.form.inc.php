<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("bible");
$tableSelect=DB_PREFIXE."bible";
$champs=array(
    "bible",
    "libelle",
    "evenement",
    "contenu",
    "complement",
    "automatique",
    "dossier_autorisation_type",
    "om_collectivite");
//champs select
$sql_dossier_autorisation_type="SELECT dossier_autorisation_type.dossier_autorisation_type, dossier_autorisation_type.libelle FROM ".DB_PREFIXE."dossier_autorisation_type ORDER BY dossier_autorisation_type.libelle ASC";
$sql_dossier_autorisation_type_by_id = "SELECT dossier_autorisation_type.dossier_autorisation_type, dossier_autorisation_type.libelle FROM ".DB_PREFIXE."dossier_autorisation_type WHERE dossier_autorisation_type = <idx>";
$sql_evenement="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_evenement_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
?>