<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("lien_demande_type_etat");
$tableSelect=DB_PREFIXE."lien_demande_type_etat";
$champs=array(
    "lien_demande_type_etat",
    "demande_type",
    "etat");
//champs select
$sql_demande_type="SELECT demande_type.demande_type, demande_type.libelle FROM ".DB_PREFIXE."demande_type ORDER BY demande_type.libelle ASC";
$sql_demande_type_by_id = "SELECT demande_type.demande_type, demande_type.libelle FROM ".DB_PREFIXE."demande_type WHERE demande_type = <idx>";
$sql_etat="SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat ORDER BY etat.libelle ASC";
$sql_etat_by_id = "SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat WHERE etat = '<idx>'";
?>