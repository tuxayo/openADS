<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("genre");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."genre";
// SELECT 
$champAffiche = array(
    'genre.genre as "'._("genre").'"',
    'genre.code as "'._("code").'"',
    'genre.libelle as "'._("libelle").'"',
    );
//
$champNonAffiche = array(
    'genre.description as "'._("description").'"',
    );
//
$champRecherche = array(
    'genre.genre as "'._("genre").'"',
    'genre.code as "'._("code").'"',
    'genre.libelle as "'._("libelle").'"',
    );
$tri="ORDER BY genre.libelle ASC NULLS LAST";
$edition="genre";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'groupe',
);

?>