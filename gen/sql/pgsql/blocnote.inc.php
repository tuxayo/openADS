<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("blocnote");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."blocnote
    LEFT JOIN ".DB_PREFIXE."dossier 
        ON blocnote.dossier=dossier.dossier ";
// SELECT 
$champAffiche = array(
    'blocnote.blocnote as "'._("blocnote").'"',
    'blocnote.categorie as "'._("categorie").'"',
    'dossier.annee as "'._("dossier").'"',
    );
//
$champNonAffiche = array(
    'blocnote.note as "'._("note").'"',
    );
//
$champRecherche = array(
    'blocnote.blocnote as "'._("blocnote").'"',
    'blocnote.categorie as "'._("categorie").'"',
    'dossier.annee as "'._("dossier").'"',
    );
$tri="ORDER BY blocnote.categorie ASC NULLS LAST";
$edition="blocnote";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
);
// Filtre listing sous formulaire - dossier
if (in_array($retourformulaire, $foreign_keys_extended["dossier"])) {
    $selection = " WHERE (blocnote.dossier = '".$f->db->escapeSimple($idxformulaire)."') ";
}

?>