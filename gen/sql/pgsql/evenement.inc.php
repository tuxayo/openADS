<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("evenement");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."evenement
    LEFT JOIN ".DB_PREFIXE."action 
        ON evenement.action=action.action 
    LEFT JOIN ".DB_PREFIXE."autorite_competente 
        ON evenement.autorite_competente=autorite_competente.autorite_competente 
    LEFT JOIN ".DB_PREFIXE."avis_decision 
        ON evenement.avis_decision=avis_decision.avis_decision 
    LEFT JOIN ".DB_PREFIXE."etat 
        ON evenement.etat=etat.etat 
    LEFT JOIN ".DB_PREFIXE."evenement as evenement4 
        ON evenement.evenement_retour_ar=evenement4.evenement 
    LEFT JOIN ".DB_PREFIXE."evenement as evenement5 
        ON evenement.evenement_retour_signature=evenement5.evenement 
    LEFT JOIN ".DB_PREFIXE."evenement as evenement6 
        ON evenement.evenement_suivant_tacite=evenement6.evenement 
    LEFT JOIN ".DB_PREFIXE."phase 
        ON evenement.phase=phase.phase ";
// SELECT 
$champAffiche = array(
    'evenement.evenement as "'._("evenement").'"',
    'evenement.libelle as "'._("libelle").'"',
    'action.libelle as "'._("action").'"',
    'etat.libelle as "'._("etat").'"',
    'evenement.delai as "'._("delai").'"',
    'evenement.accord_tacite as "'._("accord_tacite").'"',
    'evenement.delai_notification as "'._("delai_notification").'"',
    'evenement.lettretype as "'._("lettretype").'"',
    'evenement.consultation as "'._("consultation").'"',
    'avis_decision.libelle as "'._("avis_decision").'"',
    'evenement.restriction as "'._("restriction").'"',
    'evenement.type as "'._("type").'"',
    'evenement4.libelle as "'._("evenement_retour_ar").'"',
    'evenement6.libelle as "'._("evenement_suivant_tacite").'"',
    'evenement5.libelle as "'._("evenement_retour_signature").'"',
    'autorite_competente.libelle as "'._("autorite_competente").'"',
    "case evenement.retour when 't' then 'Oui' else 'Non' end as \""._("retour")."\"",
    "case evenement.non_verrouillable when 't' then 'Oui' else 'Non' end as \""._("non_verrouillable")."\"",
    'phase.code as "'._("phase").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'evenement.evenement as "'._("evenement").'"',
    'evenement.libelle as "'._("libelle").'"',
    'action.libelle as "'._("action").'"',
    'etat.libelle as "'._("etat").'"',
    'evenement.delai as "'._("delai").'"',
    'evenement.accord_tacite as "'._("accord_tacite").'"',
    'evenement.delai_notification as "'._("delai_notification").'"',
    'evenement.lettretype as "'._("lettretype").'"',
    'evenement.consultation as "'._("consultation").'"',
    'avis_decision.libelle as "'._("avis_decision").'"',
    'evenement.restriction as "'._("restriction").'"',
    'evenement.type as "'._("type").'"',
    'evenement4.libelle as "'._("evenement_retour_ar").'"',
    'evenement6.libelle as "'._("evenement_suivant_tacite").'"',
    'evenement5.libelle as "'._("evenement_retour_signature").'"',
    'autorite_competente.libelle as "'._("autorite_competente").'"',
    'phase.code as "'._("phase").'"',
    );
$tri="ORDER BY evenement.libelle ASC NULLS LAST";
$edition="evenement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "action" => array("action", ),
    "autorite_competente" => array("autorite_competente", ),
    "avis_decision" => array("avis_decision", ),
    "etat" => array("etat", ),
    "evenement" => array("evenement", ),
    "phase" => array("phase", ),
);
// Filtre listing sous formulaire - action
if (in_array($retourformulaire, $foreign_keys_extended["action"])) {
    $selection = " WHERE (evenement.action = '".$f->db->escapeSimple($idxformulaire)."') ";
}
// Filtre listing sous formulaire - autorite_competente
if (in_array($retourformulaire, $foreign_keys_extended["autorite_competente"])) {
    $selection = " WHERE (evenement.autorite_competente = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - avis_decision
if (in_array($retourformulaire, $foreign_keys_extended["avis_decision"])) {
    $selection = " WHERE (evenement.avis_decision = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etat
if (in_array($retourformulaire, $foreign_keys_extended["etat"])) {
    $selection = " WHERE (evenement.etat = '".$f->db->escapeSimple($idxformulaire)."') ";
}
// Filtre listing sous formulaire - evenement
if (in_array($retourformulaire, $foreign_keys_extended["evenement"])) {
    $selection = " WHERE (evenement.evenement_retour_ar = ".intval($idxformulaire)." OR evenement.evenement_retour_signature = ".intval($idxformulaire)." OR evenement.evenement_suivant_tacite = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - phase
if (in_array($retourformulaire, $foreign_keys_extended["phase"])) {
    $selection = " WHERE (evenement.phase = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'bible',
    'demande_type',
    'dossier',
    'evenement',
    'instruction',
    'lien_dossier_instruction_type_evenement',
    'transition',
);

?>