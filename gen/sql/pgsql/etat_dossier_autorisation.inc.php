<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("etat_dossier_autorisation");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."etat_dossier_autorisation";
// SELECT 
$champAffiche = array(
    'etat_dossier_autorisation.etat_dossier_autorisation as "'._("etat_dossier_autorisation").'"',
    'etat_dossier_autorisation.libelle as "'._("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'etat_dossier_autorisation.etat_dossier_autorisation as "'._("etat_dossier_autorisation").'"',
    'etat_dossier_autorisation.libelle as "'._("libelle").'"',
    );
$tri="ORDER BY etat_dossier_autorisation.libelle ASC NULLS LAST";
$edition="etat_dossier_autorisation";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'dossier_autorisation',
);

?>