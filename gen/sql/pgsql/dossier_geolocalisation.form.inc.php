<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("dossier_geolocalisation");
$tableSelect=DB_PREFIXE."dossier_geolocalisation";
$champs=array(
    "dossier_geolocalisation",
    "dossier",
    "date_verif_parcelle",
    "etat_verif_parcelle",
    "message_verif_parcelle",
    "date_calcul_emprise",
    "etat_calcul_emprise",
    "message_calcul_emprise",
    "date_dessin_emprise",
    "etat_dessin_emprise",
    "message_dessin_emprise",
    "date_calcul_centroide",
    "etat_calcul_centroide",
    "message_calcul_centroide",
    "date_recup_contrainte",
    "etat_recup_contrainte",
    "message_recup_contrainte",
    "terrain_references_cadastrales_archive");
//champs select
$sql_dossier="SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier ORDER BY dossier.annee ASC";
$sql_dossier_by_id = "SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier WHERE dossier = '<idx>'";
?>