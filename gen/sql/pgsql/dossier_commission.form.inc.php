<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("dossier_commission");
$tableSelect=DB_PREFIXE."dossier_commission";
$champs=array(
    "dossier_commission",
    "dossier",
    "commission_type",
    "date_souhaitee",
    "motivation",
    "commission",
    "avis",
    "lu");
//champs select
$sql_commission="SELECT commission.commission, commission.libelle FROM ".DB_PREFIXE."commission ORDER BY commission.libelle ASC";
$sql_commission_by_id = "SELECT commission.commission, commission.libelle FROM ".DB_PREFIXE."commission WHERE commission = <idx>";
$sql_commission_type="SELECT commission_type.commission_type, commission_type.libelle FROM ".DB_PREFIXE."commission_type WHERE ((commission_type.om_validite_debut IS NULL AND (commission_type.om_validite_fin IS NULL OR commission_type.om_validite_fin > CURRENT_DATE)) OR (commission_type.om_validite_debut <= CURRENT_DATE AND (commission_type.om_validite_fin IS NULL OR commission_type.om_validite_fin > CURRENT_DATE))) ORDER BY commission_type.libelle ASC";
$sql_commission_type_by_id = "SELECT commission_type.commission_type, commission_type.libelle FROM ".DB_PREFIXE."commission_type WHERE commission_type = <idx>";
$sql_dossier="SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier ORDER BY dossier.annee ASC";
$sql_dossier_by_id = "SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier WHERE dossier = '<idx>'";
?>