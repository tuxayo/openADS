<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("signataire_arrete");
$tableSelect=DB_PREFIXE."signataire_arrete";
$champs=array(
    "signataire_arrete",
    "civilite",
    "nom",
    "prenom",
    "qualite",
    "signature",
    "defaut",
    "om_validite_debut",
    "om_validite_fin",
    "om_collectivite");
//champs select
$sql_civilite="SELECT civilite.civilite, civilite.libelle FROM ".DB_PREFIXE."civilite WHERE ((civilite.om_validite_debut IS NULL AND (civilite.om_validite_fin IS NULL OR civilite.om_validite_fin > CURRENT_DATE)) OR (civilite.om_validite_debut <= CURRENT_DATE AND (civilite.om_validite_fin IS NULL OR civilite.om_validite_fin > CURRENT_DATE))) ORDER BY civilite.libelle ASC";
$sql_civilite_by_id = "SELECT civilite.civilite, civilite.libelle FROM ".DB_PREFIXE."civilite WHERE civilite = '<idx>'";
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
?>