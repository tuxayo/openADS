<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("lien_donnees_techniques_moyen_retenu_juge");
$tableSelect=DB_PREFIXE."lien_donnees_techniques_moyen_retenu_juge";
$champs=array(
    "lien_donnees_techniques_moyen_retenu_juge",
    "donnees_techniques",
    "moyen_retenu_juge");
//champs select
$sql_donnees_techniques="SELECT donnees_techniques.donnees_techniques, donnees_techniques.dossier_instruction FROM ".DB_PREFIXE."donnees_techniques ORDER BY donnees_techniques.dossier_instruction ASC";
$sql_donnees_techniques_by_id = "SELECT donnees_techniques.donnees_techniques, donnees_techniques.dossier_instruction FROM ".DB_PREFIXE."donnees_techniques WHERE donnees_techniques = <idx>";
$sql_moyen_retenu_juge="SELECT moyen_retenu_juge.moyen_retenu_juge, moyen_retenu_juge.libelle FROM ".DB_PREFIXE."moyen_retenu_juge WHERE ((moyen_retenu_juge.om_validite_debut IS NULL AND (moyen_retenu_juge.om_validite_fin IS NULL OR moyen_retenu_juge.om_validite_fin > CURRENT_DATE)) OR (moyen_retenu_juge.om_validite_debut <= CURRENT_DATE AND (moyen_retenu_juge.om_validite_fin IS NULL OR moyen_retenu_juge.om_validite_fin > CURRENT_DATE))) ORDER BY moyen_retenu_juge.libelle ASC";
$sql_moyen_retenu_juge_by_id = "SELECT moyen_retenu_juge.moyen_retenu_juge, moyen_retenu_juge.libelle FROM ".DB_PREFIXE."moyen_retenu_juge WHERE moyen_retenu_juge = <idx>";
?>