<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("document_numerise_type");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."document_numerise_type
    LEFT JOIN ".DB_PREFIXE."document_numerise_type_categorie 
        ON document_numerise_type.document_numerise_type_categorie=document_numerise_type_categorie.document_numerise_type_categorie ";
// SELECT 
$champAffiche = array(
    'document_numerise_type.document_numerise_type as "'._("document_numerise_type").'"',
    'document_numerise_type.code as "'._("code").'"',
    'document_numerise_type.libelle as "'._("libelle").'"',
    'document_numerise_type_categorie.libelle as "'._("document_numerise_type_categorie").'"',
    );
//
$champNonAffiche = array(
    'document_numerise_type.aff_service_consulte as "'._("aff_service_consulte").'"',
    'document_numerise_type.aff_da as "'._("aff_da").'"',
    'document_numerise_type.synchro_metadonnee as "'._("synchro_metadonnee").'"',
    );
//
$champRecherche = array(
    'document_numerise_type.document_numerise_type as "'._("document_numerise_type").'"',
    'document_numerise_type.code as "'._("code").'"',
    'document_numerise_type.libelle as "'._("libelle").'"',
    'document_numerise_type_categorie.libelle as "'._("document_numerise_type_categorie").'"',
    );
$tri="ORDER BY document_numerise_type.libelle ASC NULLS LAST";
$edition="document_numerise_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "document_numerise_type_categorie" => array("document_numerise_type_categorie", ),
);
// Filtre listing sous formulaire - document_numerise_type_categorie
if (in_array($retourformulaire, $foreign_keys_extended["document_numerise_type_categorie"])) {
    $selection = " WHERE (document_numerise_type.document_numerise_type_categorie = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'document_numerise',
    'lien_document_numerise_type_instructeur_qualite',
);

?>