<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("document_numerise");
$tableSelect=DB_PREFIXE."document_numerise";
$champs=array(
    "document_numerise",
    "uid",
    "dossier",
    "nom_fichier",
    "date_creation",
    "document_numerise_type");
//champs select
$sql_document_numerise_type="SELECT document_numerise_type.document_numerise_type, document_numerise_type.libelle FROM ".DB_PREFIXE."document_numerise_type ORDER BY document_numerise_type.libelle ASC";
$sql_document_numerise_type_by_id = "SELECT document_numerise_type.document_numerise_type, document_numerise_type.libelle FROM ".DB_PREFIXE."document_numerise_type WHERE document_numerise_type = <idx>";
$sql_dossier="SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier ORDER BY dossier.annee ASC";
$sql_dossier_by_id = "SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier WHERE dossier = '<idx>'";
?>