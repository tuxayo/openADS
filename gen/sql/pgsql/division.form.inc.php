<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("division");
$tableSelect=DB_PREFIXE."division";
$champs=array(
    "division",
    "code",
    "libelle",
    "description",
    "chef",
    "direction",
    "om_validite_debut",
    "om_validite_fin");
//champs select
$sql_direction="SELECT direction.direction, direction.libelle FROM ".DB_PREFIXE."direction WHERE ((direction.om_validite_debut IS NULL AND (direction.om_validite_fin IS NULL OR direction.om_validite_fin > CURRENT_DATE)) OR (direction.om_validite_debut <= CURRENT_DATE AND (direction.om_validite_fin IS NULL OR direction.om_validite_fin > CURRENT_DATE))) ORDER BY direction.libelle ASC";
$sql_direction_by_id = "SELECT direction.direction, direction.libelle FROM ".DB_PREFIXE."direction WHERE direction = <idx>";
?>