<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("quartier");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."quartier
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON quartier.arrondissement=arrondissement.arrondissement ";
// SELECT 
$champAffiche = array(
    'quartier.quartier as "'._("quartier").'"',
    'arrondissement.libelle as "'._("arrondissement").'"',
    'quartier.code_impots as "'._("code_impots").'"',
    'quartier.libelle as "'._("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'quartier.quartier as "'._("quartier").'"',
    'arrondissement.libelle as "'._("arrondissement").'"',
    'quartier.code_impots as "'._("code_impots").'"',
    'quartier.libelle as "'._("libelle").'"',
    );
$tri="ORDER BY quartier.libelle ASC NULLS LAST";
$edition="quartier";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "arrondissement" => array("arrondissement", ),
);
// Filtre listing sous formulaire - arrondissement
if (in_array($retourformulaire, $foreign_keys_extended["arrondissement"])) {
    $selection = " WHERE (quartier.arrondissement = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'affectation_automatique',
    'dossier',
);

?>