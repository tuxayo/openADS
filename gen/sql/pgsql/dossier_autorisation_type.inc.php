<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_autorisation_type");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_autorisation_type
    LEFT JOIN ".DB_PREFIXE."groupe 
        ON dossier_autorisation_type.groupe=groupe.groupe ";
// SELECT 
$champAffiche = array(
    'dossier_autorisation_type.dossier_autorisation_type as "'._("dossier_autorisation_type").'"',
    'dossier_autorisation_type.code as "'._("code").'"',
    'dossier_autorisation_type.libelle as "'._("libelle").'"',
    "case dossier_autorisation_type.confidentiel when 't' then 'Oui' else 'Non' end as \""._("confidentiel")."\"",
    'groupe.libelle as "'._("groupe").'"',
    'dossier_autorisation_type.affichage_form as "'._("affichage_form").'"',
    "case dossier_autorisation_type.cacher_da when 't' then 'Oui' else 'Non' end as \""._("cacher_da")."\"",
    );
//
$champNonAffiche = array(
    'dossier_autorisation_type.description as "'._("description").'"',
    );
//
$champRecherche = array(
    'dossier_autorisation_type.dossier_autorisation_type as "'._("dossier_autorisation_type").'"',
    'dossier_autorisation_type.code as "'._("code").'"',
    'dossier_autorisation_type.libelle as "'._("libelle").'"',
    'groupe.libelle as "'._("groupe").'"',
    'dossier_autorisation_type.affichage_form as "'._("affichage_form").'"',
    );
$tri="ORDER BY dossier_autorisation_type.libelle ASC NULLS LAST";
$edition="dossier_autorisation_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "groupe" => array("groupe", ),
);
// Filtre listing sous formulaire - groupe
if (in_array($retourformulaire, $foreign_keys_extended["groupe"])) {
    $selection = " WHERE (dossier_autorisation_type.groupe = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'bible',
    'dossier_autorisation_type_detaille',
);

?>