<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("lien_donnees_techniques_moyen_souleve");
$tableSelect=DB_PREFIXE."lien_donnees_techniques_moyen_souleve";
$champs=array(
    "lien_donnees_techniques_moyen_souleve",
    "donnees_techniques",
    "moyen_souleve");
//champs select
$sql_donnees_techniques="SELECT donnees_techniques.donnees_techniques, donnees_techniques.dossier_instruction FROM ".DB_PREFIXE."donnees_techniques ORDER BY donnees_techniques.dossier_instruction ASC";
$sql_donnees_techniques_by_id = "SELECT donnees_techniques.donnees_techniques, donnees_techniques.dossier_instruction FROM ".DB_PREFIXE."donnees_techniques WHERE donnees_techniques = <idx>";
$sql_moyen_souleve="SELECT moyen_souleve.moyen_souleve, moyen_souleve.libelle FROM ".DB_PREFIXE."moyen_souleve WHERE ((moyen_souleve.om_validite_debut IS NULL AND (moyen_souleve.om_validite_fin IS NULL OR moyen_souleve.om_validite_fin > CURRENT_DATE)) OR (moyen_souleve.om_validite_debut <= CURRENT_DATE AND (moyen_souleve.om_validite_fin IS NULL OR moyen_souleve.om_validite_fin > CURRENT_DATE))) ORDER BY moyen_souleve.libelle ASC";
$sql_moyen_souleve_by_id = "SELECT moyen_souleve.moyen_souleve, moyen_souleve.libelle FROM ".DB_PREFIXE."moyen_souleve WHERE moyen_souleve = <idx>";
?>