<?php
//$Id$ 
//gen openMairie le 15/06/2017 19:12

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier
    LEFT JOIN ".DB_PREFIXE."dossier 
        ON dossier.autorisation_contestee=dossier.dossier 
    LEFT JOIN ".DB_PREFIXE."autorite_competente 
        ON dossier.autorite_competente=autorite_competente.autorite_competente 
    LEFT JOIN ".DB_PREFIXE."avis_decision 
        ON dossier.avis_decision=avis_decision.avis_decision 
    LEFT JOIN ".DB_PREFIXE."division 
        ON dossier.division=division.division 
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation 
        ON dossier.dossier_autorisation=dossier_autorisation.dossier_autorisation 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction_type 
        ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type 
    LEFT JOIN ".DB_PREFIXE."etat as etat6 
        ON dossier.etat=etat6.etat 
    LEFT JOIN ".DB_PREFIXE."etat as etat7 
        ON dossier.etat_pendant_incompletude=etat7.etat 
    LEFT JOIN ".DB_PREFIXE."evenement as evenement8 
        ON dossier.evenement_suivant_tacite=evenement8.evenement 
    LEFT JOIN ".DB_PREFIXE."evenement as evenement9 
        ON dossier.evenement_suivant_tacite_incompletude=evenement9.evenement 
    LEFT JOIN ".DB_PREFIXE."instructeur as instructeur10 
        ON dossier.instructeur=instructeur10.instructeur 
    LEFT JOIN ".DB_PREFIXE."instructeur as instructeur11 
        ON dossier.instructeur_2=instructeur11.instructeur 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON dossier.om_collectivite=om_collectivite.om_collectivite 
    LEFT JOIN ".DB_PREFIXE."quartier 
        ON dossier.quartier=quartier.quartier ";
// SELECT 
$champAffiche = array(
    'dossier.dossier as "'._("dossier").'"',
    'dossier.annee as "'._("annee").'"',
    'etat6.libelle as "'._("etat").'"',
    'instructeur10.nom as "'._("instructeur").'"',
    'to_char(dossier.date_demande ,\'DD/MM/YYYY\') as "'._("date_demande").'"',
    'to_char(dossier.date_depot ,\'DD/MM/YYYY\') as "'._("date_depot").'"',
    'to_char(dossier.date_complet ,\'DD/MM/YYYY\') as "'._("date_complet").'"',
    'to_char(dossier.date_rejet ,\'DD/MM/YYYY\') as "'._("date_rejet").'"',
    'to_char(dossier.date_notification_delai ,\'DD/MM/YYYY\') as "'._("date_notification_delai").'"',
    'dossier.delai as "'._("delai").'"',
    'to_char(dossier.date_limite ,\'DD/MM/YYYY\') as "'._("date_limite").'"',
    'dossier.accord_tacite as "'._("accord_tacite").'"',
    'to_char(dossier.date_decision ,\'DD/MM/YYYY\') as "'._("date_decision").'"',
    'to_char(dossier.date_validite ,\'DD/MM/YYYY\') as "'._("date_validite").'"',
    'to_char(dossier.date_chantier ,\'DD/MM/YYYY\') as "'._("date_chantier").'"',
    'to_char(dossier.date_achevement ,\'DD/MM/YYYY\') as "'._("date_achevement").'"',
    'to_char(dossier.date_conformite ,\'DD/MM/YYYY\') as "'._("date_conformite").'"',
    "case dossier.erp when 't' then 'Oui' else 'Non' end as \""._("erp")."\"",
    'avis_decision.libelle as "'._("avis_decision").'"',
    "case dossier.enjeu_erp when 't' then 'Oui' else 'Non' end as \""._("enjeu_erp")."\"",
    "case dossier.enjeu_urba when 't' then 'Oui' else 'Non' end as \""._("enjeu_urba")."\"",
    'division.libelle as "'._("division").'"',
    'autorite_competente.libelle as "'._("autorite_competente").'"',
    "case dossier.a_qualifier when 't' then 'Oui' else 'Non' end as \""._("a_qualifier")."\"",
    'dossier.terrain_adresse_voie_numero as "'._("terrain_adresse_voie_numero").'"',
    'dossier.terrain_adresse_voie as "'._("terrain_adresse_voie").'"',
    'dossier.terrain_adresse_lieu_dit as "'._("terrain_adresse_lieu_dit").'"',
    'dossier.terrain_adresse_localite as "'._("terrain_adresse_localite").'"',
    'dossier.terrain_adresse_code_postal as "'._("terrain_adresse_code_postal").'"',
    'dossier.terrain_adresse_bp as "'._("terrain_adresse_bp").'"',
    'dossier.terrain_adresse_cedex as "'._("terrain_adresse_cedex").'"',
    'dossier.terrain_superficie as "'._("terrain_superficie").'"',
    'dossier_autorisation.dossier_autorisation_type_detaille as "'._("dossier_autorisation").'"',
    'dossier_instruction_type.libelle as "'._("dossier_instruction_type").'"',
    'to_char(dossier.date_dernier_depot ,\'DD/MM/YYYY\') as "'._("date_dernier_depot").'"',
    'dossier.version as "'._("version").'"',
    "case dossier.incompletude when 't' then 'Oui' else 'Non' end as \""._("incompletude")."\"",
    'evenement8.libelle as "'._("evenement_suivant_tacite").'"',
    'evenement9.libelle as "'._("evenement_suivant_tacite_incompletude").'"',
    'etat7.libelle as "'._("etat_pendant_incompletude").'"',
    'to_char(dossier.date_limite_incompletude ,\'DD/MM/YYYY\') as "'._("date_limite_incompletude").'"',
    'dossier.delai_incompletude as "'._("delai_incompletude").'"',
    'dossier.dossier_libelle as "'._("dossier_libelle").'"',
    'dossier.numero_versement_archive as "'._("numero_versement_archive").'"',
    'dossier.duree_validite as "'._("duree_validite").'"',
    'quartier.libelle as "'._("quartier").'"',
    "case dossier.incomplet_notifie when 't' then 'Oui' else 'Non' end as \""._("incomplet_notifie")."\"",
    'dossier.tax_secteur as "'._("tax_secteur").'"',
    'dossier.tax_mtn_part_commu as "'._("tax_mtn_part_commu").'"',
    'dossier.tax_mtn_part_depart as "'._("tax_mtn_part_depart").'"',
    'dossier.tax_mtn_part_reg as "'._("tax_mtn_part_reg").'"',
    'dossier.tax_mtn_total as "'._("tax_mtn_total").'"',
    "case dossier.interface_referentiel_erp when 't' then 'Oui' else 'Non' end as \""._("interface_referentiel_erp")."\"",
    'dossier.annee as "'._("autorisation_contestee").'"',
    'to_char(dossier.date_cloture_instruction ,\'DD/MM/YYYY\') as "'._("date_cloture_instruction").'"',
    'to_char(dossier.date_premiere_visite ,\'DD/MM/YYYY\') as "'._("date_premiere_visite").'"',
    'to_char(dossier.date_derniere_visite ,\'DD/MM/YYYY\') as "'._("date_derniere_visite").'"',
    'to_char(dossier.date_contradictoire ,\'DD/MM/YYYY\') as "'._("date_contradictoire").'"',
    'to_char(dossier.date_retour_contradictoire ,\'DD/MM/YYYY\') as "'._("date_retour_contradictoire").'"',
    'to_char(dossier.date_ait ,\'DD/MM/YYYY\') as "'._("date_ait").'"',
    'to_char(dossier.date_transmission_parquet ,\'DD/MM/YYYY\') as "'._("date_transmission_parquet").'"',
    'instructeur11.nom as "'._("instructeur_2").'"',
    'dossier.tax_mtn_rap as "'._("tax_mtn_rap").'"',
    'dossier.tax_mtn_part_commu_sans_exo as "'._("tax_mtn_part_commu_sans_exo").'"',
    'dossier.tax_mtn_part_depart_sans_exo as "'._("tax_mtn_part_depart_sans_exo").'"',
    'dossier.tax_mtn_part_reg_sans_exo as "'._("tax_mtn_part_reg_sans_exo").'"',
    'dossier.tax_mtn_total_sans_exo as "'._("tax_mtn_total_sans_exo").'"',
    'dossier.tax_mtn_rap_sans_exo as "'._("tax_mtn_rap_sans_exo").'"',
    'dossier.geom as "'._("geom").'"',
    'dossier.geom1 as "'._("geom1").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
//
$champNonAffiche = array(
    'dossier.description as "'._("description").'"',
    'dossier.terrain_references_cadastrales as "'._("terrain_references_cadastrales").'"',
    'dossier.om_collectivite as "'._("om_collectivite").'"',
    'dossier.log_instructions as "'._("log_instructions").'"',
    );
//
$champRecherche = array(
    'dossier.dossier as "'._("dossier").'"',
    'dossier.annee as "'._("annee").'"',
    'etat6.libelle as "'._("etat").'"',
    'instructeur10.nom as "'._("instructeur").'"',
    'dossier.delai as "'._("delai").'"',
    'dossier.accord_tacite as "'._("accord_tacite").'"',
    'avis_decision.libelle as "'._("avis_decision").'"',
    'division.libelle as "'._("division").'"',
    'autorite_competente.libelle as "'._("autorite_competente").'"',
    'dossier.terrain_adresse_voie_numero as "'._("terrain_adresse_voie_numero").'"',
    'dossier.terrain_adresse_voie as "'._("terrain_adresse_voie").'"',
    'dossier.terrain_adresse_lieu_dit as "'._("terrain_adresse_lieu_dit").'"',
    'dossier.terrain_adresse_localite as "'._("terrain_adresse_localite").'"',
    'dossier.terrain_adresse_code_postal as "'._("terrain_adresse_code_postal").'"',
    'dossier.terrain_adresse_bp as "'._("terrain_adresse_bp").'"',
    'dossier.terrain_adresse_cedex as "'._("terrain_adresse_cedex").'"',
    'dossier.terrain_superficie as "'._("terrain_superficie").'"',
    'dossier_autorisation.dossier_autorisation_type_detaille as "'._("dossier_autorisation").'"',
    'dossier_instruction_type.libelle as "'._("dossier_instruction_type").'"',
    'dossier.version as "'._("version").'"',
    'evenement8.libelle as "'._("evenement_suivant_tacite").'"',
    'evenement9.libelle as "'._("evenement_suivant_tacite_incompletude").'"',
    'etat7.libelle as "'._("etat_pendant_incompletude").'"',
    'dossier.delai_incompletude as "'._("delai_incompletude").'"',
    'dossier.dossier_libelle as "'._("dossier_libelle").'"',
    'dossier.numero_versement_archive as "'._("numero_versement_archive").'"',
    'dossier.duree_validite as "'._("duree_validite").'"',
    'quartier.libelle as "'._("quartier").'"',
    'dossier.tax_secteur as "'._("tax_secteur").'"',
    'dossier.tax_mtn_part_commu as "'._("tax_mtn_part_commu").'"',
    'dossier.tax_mtn_part_depart as "'._("tax_mtn_part_depart").'"',
    'dossier.tax_mtn_part_reg as "'._("tax_mtn_part_reg").'"',
    'dossier.tax_mtn_total as "'._("tax_mtn_total").'"',
    'dossier.annee as "'._("autorisation_contestee").'"',
    'instructeur11.nom as "'._("instructeur_2").'"',
    'dossier.tax_mtn_rap as "'._("tax_mtn_rap").'"',
    'dossier.tax_mtn_part_commu_sans_exo as "'._("tax_mtn_part_commu_sans_exo").'"',
    'dossier.tax_mtn_part_depart_sans_exo as "'._("tax_mtn_part_depart_sans_exo").'"',
    'dossier.tax_mtn_part_reg_sans_exo as "'._("tax_mtn_part_reg_sans_exo").'"',
    'dossier.tax_mtn_total_sans_exo as "'._("tax_mtn_total_sans_exo").'"',
    'dossier.tax_mtn_rap_sans_exo as "'._("tax_mtn_rap_sans_exo").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
$tri="ORDER BY dossier.annee ASC NULLS LAST";
$edition="dossier";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
    "autorite_competente" => array("autorite_competente", ),
    "avis_decision" => array("avis_decision", ),
    "division" => array("division", ),
    "dossier_autorisation" => array("dossier_autorisation", ),
    "dossier_instruction_type" => array("dossier_instruction_type", ),
    "etat" => array("etat", ),
    "evenement" => array("evenement", ),
    "instructeur" => array("instructeur", ),
    "om_collectivite" => array("om_collectivite", ),
    "quartier" => array("quartier", ),
);
// Filtre listing sous formulaire - dossier
if (in_array($retourformulaire, $foreign_keys_extended["dossier"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.autorisation_contestee = '".$f->db->escapeSimple($idxformulaire)."') ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.autorisation_contestee = '".$f->db->escapeSimple($idxformulaire)."') ";
    }
}
// Filtre listing sous formulaire - autorite_competente
if (in_array($retourformulaire, $foreign_keys_extended["autorite_competente"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.autorite_competente = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.autorite_competente = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - avis_decision
if (in_array($retourformulaire, $foreign_keys_extended["avis_decision"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.avis_decision = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.avis_decision = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - division
if (in_array($retourformulaire, $foreign_keys_extended["division"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.division = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.division = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - dossier_autorisation
if (in_array($retourformulaire, $foreign_keys_extended["dossier_autorisation"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.dossier_autorisation = '".$f->db->escapeSimple($idxformulaire)."') ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.dossier_autorisation = '".$f->db->escapeSimple($idxformulaire)."') ";
    }
}
// Filtre listing sous formulaire - dossier_instruction_type
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction_type"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.dossier_instruction_type = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.dossier_instruction_type = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - etat
if (in_array($retourformulaire, $foreign_keys_extended["etat"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.etat = '".$f->db->escapeSimple($idxformulaire)."' OR dossier.etat_pendant_incompletude = '".$f->db->escapeSimple($idxformulaire)."') ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.etat = '".$f->db->escapeSimple($idxformulaire)."' OR dossier.etat_pendant_incompletude = '".$f->db->escapeSimple($idxformulaire)."') ";
    }
}
// Filtre listing sous formulaire - evenement
if (in_array($retourformulaire, $foreign_keys_extended["evenement"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.evenement_suivant_tacite = ".intval($idxformulaire)." OR dossier.evenement_suivant_tacite_incompletude = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.evenement_suivant_tacite = ".intval($idxformulaire)." OR dossier.evenement_suivant_tacite_incompletude = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - instructeur
if (in_array($retourformulaire, $foreign_keys_extended["instructeur"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.instructeur = ".intval($idxformulaire)." OR dossier.instructeur_2 = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.instructeur = ".intval($idxformulaire)." OR dossier.instructeur_2 = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.om_collectivite = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - quartier
if (in_array($retourformulaire, $foreign_keys_extended["quartier"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier.quartier = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier.quartier = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'blocnote',
    'consultation',
    'demande',
    'document_numerise',
    'donnees_techniques',
    'dossier',
    'dossier_commission',
    'dossier_contrainte',
    'dossier_geolocalisation',
    'dossier_message',
    'dossier_parcelle',
    'instruction',
    'lien_dossier_demandeur',
    'lien_dossier_dossier',
    'lot',
    'rapport_instruction',
);

?>