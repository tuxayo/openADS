<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("lien_dossier_instruction_type_evenement");
$tableSelect=DB_PREFIXE."lien_dossier_instruction_type_evenement";
$champs=array(
    "lien_dossier_instruction_type_evenement",
    "dossier_instruction_type",
    "evenement");
//champs select
$sql_dossier_instruction_type="SELECT dossier_instruction_type.dossier_instruction_type, dossier_instruction_type.libelle FROM ".DB_PREFIXE."dossier_instruction_type ORDER BY dossier_instruction_type.libelle ASC";
$sql_dossier_instruction_type_by_id = "SELECT dossier_instruction_type.dossier_instruction_type, dossier_instruction_type.libelle FROM ".DB_PREFIXE."dossier_instruction_type WHERE dossier_instruction_type = <idx>";
$sql_evenement="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_evenement_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
?>