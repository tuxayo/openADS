<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("instructeur");
$tableSelect=DB_PREFIXE."instructeur";
$champs=array(
    "instructeur",
    "nom",
    "telephone",
    "division",
    "om_utilisateur",
    "om_validite_debut",
    "om_validite_fin",
    "instructeur_qualite");
//champs select
$sql_division="SELECT division.division, division.libelle FROM ".DB_PREFIXE."division WHERE ((division.om_validite_debut IS NULL AND (division.om_validite_fin IS NULL OR division.om_validite_fin > CURRENT_DATE)) OR (division.om_validite_debut <= CURRENT_DATE AND (division.om_validite_fin IS NULL OR division.om_validite_fin > CURRENT_DATE))) ORDER BY division.libelle ASC";
$sql_division_by_id = "SELECT division.division, division.libelle FROM ".DB_PREFIXE."division WHERE division = <idx>";
$sql_instructeur_qualite="SELECT instructeur_qualite.instructeur_qualite, instructeur_qualite.libelle FROM ".DB_PREFIXE."instructeur_qualite ORDER BY instructeur_qualite.libelle ASC";
$sql_instructeur_qualite_by_id = "SELECT instructeur_qualite.instructeur_qualite, instructeur_qualite.libelle FROM ".DB_PREFIXE."instructeur_qualite WHERE instructeur_qualite = <idx>";
$sql_om_utilisateur="SELECT om_utilisateur.om_utilisateur, om_utilisateur.nom FROM ".DB_PREFIXE."om_utilisateur ORDER BY om_utilisateur.nom ASC";
$sql_om_utilisateur_by_id = "SELECT om_utilisateur.om_utilisateur, om_utilisateur.nom FROM ".DB_PREFIXE."om_utilisateur WHERE om_utilisateur = <idx>";
?>