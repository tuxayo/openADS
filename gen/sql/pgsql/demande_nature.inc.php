<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("demande_nature");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."demande_nature";
// SELECT 
$champAffiche = array(
    'demande_nature.demande_nature as "'._("demande_nature").'"',
    'demande_nature.code as "'._("code").'"',
    'demande_nature.libelle as "'._("libelle").'"',
    );
//
$champNonAffiche = array(
    'demande_nature.description as "'._("description").'"',
    );
//
$champRecherche = array(
    'demande_nature.demande_nature as "'._("demande_nature").'"',
    'demande_nature.code as "'._("code").'"',
    'demande_nature.libelle as "'._("libelle").'"',
    );
$tri="ORDER BY demande_nature.libelle ASC NULLS LAST";
$edition="demande_nature";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'demande_type',
);

?>