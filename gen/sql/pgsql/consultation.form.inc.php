<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("consultation");
$tableSelect=DB_PREFIXE."consultation";
$champs=array(
    "consultation",
    "dossier",
    "date_envoi",
    "date_retour",
    "date_limite",
    "service",
    "avis_consultation",
    "date_reception",
    "motivation",
    "fichier",
    "lu",
    "code_barres",
    "om_fichier_consultation",
    "om_final_consultation",
    "marque",
    "visible");
//champs select
$sql_avis_consultation="SELECT avis_consultation.avis_consultation, avis_consultation.libelle FROM ".DB_PREFIXE."avis_consultation WHERE ((avis_consultation.om_validite_debut IS NULL AND (avis_consultation.om_validite_fin IS NULL OR avis_consultation.om_validite_fin > CURRENT_DATE)) OR (avis_consultation.om_validite_debut <= CURRENT_DATE AND (avis_consultation.om_validite_fin IS NULL OR avis_consultation.om_validite_fin > CURRENT_DATE))) ORDER BY avis_consultation.libelle ASC";
$sql_avis_consultation_by_id = "SELECT avis_consultation.avis_consultation, avis_consultation.libelle FROM ".DB_PREFIXE."avis_consultation WHERE avis_consultation = '<idx>'";
$sql_dossier="SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier ORDER BY dossier.annee ASC";
$sql_dossier_by_id = "SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier WHERE dossier = '<idx>'";
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = '<idx>'";
?>