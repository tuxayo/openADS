<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("document_numerise");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."document_numerise
    LEFT JOIN ".DB_PREFIXE."document_numerise_type 
        ON document_numerise.document_numerise_type=document_numerise_type.document_numerise_type 
    LEFT JOIN ".DB_PREFIXE."dossier 
        ON document_numerise.dossier=dossier.dossier ";
// SELECT 
$champAffiche = array(
    'document_numerise.document_numerise as "'._("document_numerise").'"',
    'document_numerise.uid as "'._("uid").'"',
    'dossier.annee as "'._("dossier").'"',
    'document_numerise.nom_fichier as "'._("nom_fichier").'"',
    'to_char(document_numerise.date_creation ,\'DD/MM/YYYY\') as "'._("date_creation").'"',
    'document_numerise_type.libelle as "'._("document_numerise_type").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'document_numerise.document_numerise as "'._("document_numerise").'"',
    'document_numerise.uid as "'._("uid").'"',
    'dossier.annee as "'._("dossier").'"',
    'document_numerise.nom_fichier as "'._("nom_fichier").'"',
    'document_numerise_type.libelle as "'._("document_numerise_type").'"',
    );
$tri="ORDER BY document_numerise.uid ASC NULLS LAST";
$edition="document_numerise";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "document_numerise_type" => array("document_numerise_type", ),
    "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
);
// Filtre listing sous formulaire - document_numerise_type
if (in_array($retourformulaire, $foreign_keys_extended["document_numerise_type"])) {
    $selection = " WHERE (document_numerise.document_numerise_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier
if (in_array($retourformulaire, $foreign_keys_extended["dossier"])) {
    $selection = " WHERE (document_numerise.dossier = '".$f->db->escapeSimple($idxformulaire)."') ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'instruction',
);

?>