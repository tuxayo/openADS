<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("dossier_autorisation");
$tableSelect=DB_PREFIXE."dossier_autorisation";
$champs=array(
    "dossier_autorisation",
    "dossier_autorisation_type_detaille",
    "exercice",
    "insee",
    "terrain_references_cadastrales",
    "terrain_adresse_voie_numero",
    "terrain_adresse_voie",
    "terrain_adresse_lieu_dit",
    "terrain_adresse_localite",
    "terrain_adresse_code_postal",
    "terrain_adresse_bp",
    "terrain_adresse_cedex",
    "terrain_superficie",
    "arrondissement",
    "depot_initial",
    "erp_numero_batiment",
    "erp_ouvert",
    "erp_date_ouverture",
    "erp_arrete_decision",
    "erp_date_arrete_decision",
    "numero_version",
    "etat_dossier_autorisation",
    "date_depot",
    "date_decision",
    "date_validite",
    "date_chantier",
    "date_achevement",
    "avis_decision",
    "etat_dernier_dossier_instruction_accepte",
    "dossier_autorisation_libelle",
    "om_collectivite",
    "cle_acces_citoyen");
//champs select
$sql_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement ORDER BY arrondissement.libelle ASC";
$sql_arrondissement_by_id = "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE arrondissement = <idx>";
$sql_avis_decision="SELECT avis_decision.avis_decision, avis_decision.libelle FROM ".DB_PREFIXE."avis_decision ORDER BY avis_decision.libelle ASC";
$sql_avis_decision_by_id = "SELECT avis_decision.avis_decision, avis_decision.libelle FROM ".DB_PREFIXE."avis_decision WHERE avis_decision = '<idx>'";
$sql_dossier_autorisation_type_detaille="SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille ORDER BY dossier_autorisation_type_detaille.libelle ASC";
$sql_dossier_autorisation_type_detaille_by_id = "SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille WHERE dossier_autorisation_type_detaille = <idx>";
$sql_etat_dernier_dossier_instruction_accepte="SELECT etat_dossier_autorisation.etat_dossier_autorisation, etat_dossier_autorisation.libelle FROM ".DB_PREFIXE."etat_dossier_autorisation ORDER BY etat_dossier_autorisation.libelle ASC";
$sql_etat_dernier_dossier_instruction_accepte_by_id = "SELECT etat_dossier_autorisation.etat_dossier_autorisation, etat_dossier_autorisation.libelle FROM ".DB_PREFIXE."etat_dossier_autorisation WHERE etat_dossier_autorisation = <idx>";
$sql_etat_dossier_autorisation="SELECT etat_dossier_autorisation.etat_dossier_autorisation, etat_dossier_autorisation.libelle FROM ".DB_PREFIXE."etat_dossier_autorisation ORDER BY etat_dossier_autorisation.libelle ASC";
$sql_etat_dossier_autorisation_by_id = "SELECT etat_dossier_autorisation.etat_dossier_autorisation, etat_dossier_autorisation.libelle FROM ".DB_PREFIXE."etat_dossier_autorisation WHERE etat_dossier_autorisation = <idx>";
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
?>