<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_message");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_message
    LEFT JOIN ".DB_PREFIXE."dossier 
        ON dossier_message.dossier=dossier.dossier ";
// SELECT 
$champAffiche = array(
    'dossier_message.dossier_message as "'._("dossier_message").'"',
    'dossier.annee as "'._("dossier").'"',
    'dossier_message.type as "'._("type").'"',
    'dossier_message.emetteur as "'._("emetteur").'"',
    'dossier_message.date_emission as "'._("date_emission").'"',
    "case dossier_message.lu when 't' then 'Oui' else 'Non' end as \""._("lu")."\"",
    'dossier_message.categorie as "'._("categorie").'"',
    'dossier_message.destinataire as "'._("destinataire").'"',
    );
//
$champNonAffiche = array(
    'dossier_message.contenu as "'._("contenu").'"',
    );
//
$champRecherche = array(
    'dossier_message.dossier_message as "'._("dossier_message").'"',
    'dossier.annee as "'._("dossier").'"',
    'dossier_message.type as "'._("type").'"',
    'dossier_message.emetteur as "'._("emetteur").'"',
    'dossier_message.categorie as "'._("categorie").'"',
    'dossier_message.destinataire as "'._("destinataire").'"',
    );
$tri="ORDER BY dossier.annee ASC NULLS LAST";
$edition="dossier_message";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
);
// Filtre listing sous formulaire - dossier
if (in_array($retourformulaire, $foreign_keys_extended["dossier"])) {
    $selection = " WHERE (dossier_message.dossier = '".$f->db->escapeSimple($idxformulaire)."') ";
}

?>