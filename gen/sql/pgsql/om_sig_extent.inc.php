<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("administration")." -> "._("om_sig_extent");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."om_sig_extent";
// SELECT 
$champAffiche = array(
    'om_sig_extent.om_sig_extent as "'._("om_sig_extent").'"',
    'om_sig_extent.nom as "'._("nom").'"',
    'om_sig_extent.extent as "'._("extent").'"',
    "case om_sig_extent.valide when 't' then 'Oui' else 'Non' end as \""._("valide")."\"",
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'om_sig_extent.om_sig_extent as "'._("om_sig_extent").'"',
    'om_sig_extent.nom as "'._("nom").'"',
    'om_sig_extent.extent as "'._("extent").'"',
    );
$tri="ORDER BY om_sig_extent.nom ASC NULLS LAST";
$edition="om_sig_extent";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'om_sig_map',
);

?>