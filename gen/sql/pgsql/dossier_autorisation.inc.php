<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_autorisation");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_autorisation
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON dossier_autorisation.arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."avis_decision 
        ON dossier_autorisation.avis_decision=avis_decision.avis_decision 
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille 
        ON dossier_autorisation.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
    LEFT JOIN ".DB_PREFIXE."etat_dossier_autorisation as etat_dossier_autorisation3 
        ON dossier_autorisation.etat_dernier_dossier_instruction_accepte=etat_dossier_autorisation3.etat_dossier_autorisation 
    LEFT JOIN ".DB_PREFIXE."etat_dossier_autorisation as etat_dossier_autorisation4 
        ON dossier_autorisation.etat_dossier_autorisation=etat_dossier_autorisation4.etat_dossier_autorisation 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON dossier_autorisation.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'dossier_autorisation.dossier_autorisation as "'._("dossier_autorisation").'"',
    'dossier_autorisation_type_detaille.libelle as "'._("dossier_autorisation_type_detaille").'"',
    'dossier_autorisation.exercice as "'._("exercice").'"',
    'dossier_autorisation.insee as "'._("insee").'"',
    'dossier_autorisation.terrain_adresse_voie_numero as "'._("terrain_adresse_voie_numero").'"',
    'dossier_autorisation.terrain_adresse_voie as "'._("terrain_adresse_voie").'"',
    'dossier_autorisation.terrain_adresse_lieu_dit as "'._("terrain_adresse_lieu_dit").'"',
    'dossier_autorisation.terrain_adresse_localite as "'._("terrain_adresse_localite").'"',
    'dossier_autorisation.terrain_adresse_code_postal as "'._("terrain_adresse_code_postal").'"',
    'dossier_autorisation.terrain_adresse_bp as "'._("terrain_adresse_bp").'"',
    'dossier_autorisation.terrain_adresse_cedex as "'._("terrain_adresse_cedex").'"',
    'dossier_autorisation.terrain_superficie as "'._("terrain_superficie").'"',
    'arrondissement.libelle as "'._("arrondissement").'"',
    'to_char(dossier_autorisation.depot_initial ,\'DD/MM/YYYY\') as "'._("depot_initial").'"',
    'dossier_autorisation.erp_numero_batiment as "'._("erp_numero_batiment").'"',
    "case dossier_autorisation.erp_ouvert when 't' then 'Oui' else 'Non' end as \""._("erp_ouvert")."\"",
    'to_char(dossier_autorisation.erp_date_ouverture ,\'DD/MM/YYYY\') as "'._("erp_date_ouverture").'"',
    "case dossier_autorisation.erp_arrete_decision when 't' then 'Oui' else 'Non' end as \""._("erp_arrete_decision")."\"",
    'to_char(dossier_autorisation.erp_date_arrete_decision ,\'DD/MM/YYYY\') as "'._("erp_date_arrete_decision").'"',
    'dossier_autorisation.numero_version as "'._("numero_version").'"',
    'etat_dossier_autorisation4.libelle as "'._("etat_dossier_autorisation").'"',
    'to_char(dossier_autorisation.date_depot ,\'DD/MM/YYYY\') as "'._("date_depot").'"',
    'to_char(dossier_autorisation.date_decision ,\'DD/MM/YYYY\') as "'._("date_decision").'"',
    'to_char(dossier_autorisation.date_validite ,\'DD/MM/YYYY\') as "'._("date_validite").'"',
    'to_char(dossier_autorisation.date_chantier ,\'DD/MM/YYYY\') as "'._("date_chantier").'"',
    'to_char(dossier_autorisation.date_achevement ,\'DD/MM/YYYY\') as "'._("date_achevement").'"',
    'avis_decision.libelle as "'._("avis_decision").'"',
    'etat_dossier_autorisation3.libelle as "'._("etat_dernier_dossier_instruction_accepte").'"',
    'dossier_autorisation.dossier_autorisation_libelle as "'._("dossier_autorisation_libelle").'"',
    'dossier_autorisation.cle_acces_citoyen as "'._("cle_acces_citoyen").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
//
$champNonAffiche = array(
    'dossier_autorisation.terrain_references_cadastrales as "'._("terrain_references_cadastrales").'"',
    'dossier_autorisation.om_collectivite as "'._("om_collectivite").'"',
    );
//
$champRecherche = array(
    'dossier_autorisation.dossier_autorisation as "'._("dossier_autorisation").'"',
    'dossier_autorisation_type_detaille.libelle as "'._("dossier_autorisation_type_detaille").'"',
    'dossier_autorisation.exercice as "'._("exercice").'"',
    'dossier_autorisation.insee as "'._("insee").'"',
    'dossier_autorisation.terrain_adresse_voie_numero as "'._("terrain_adresse_voie_numero").'"',
    'dossier_autorisation.terrain_adresse_voie as "'._("terrain_adresse_voie").'"',
    'dossier_autorisation.terrain_adresse_lieu_dit as "'._("terrain_adresse_lieu_dit").'"',
    'dossier_autorisation.terrain_adresse_localite as "'._("terrain_adresse_localite").'"',
    'dossier_autorisation.terrain_adresse_code_postal as "'._("terrain_adresse_code_postal").'"',
    'dossier_autorisation.terrain_adresse_bp as "'._("terrain_adresse_bp").'"',
    'dossier_autorisation.terrain_adresse_cedex as "'._("terrain_adresse_cedex").'"',
    'dossier_autorisation.terrain_superficie as "'._("terrain_superficie").'"',
    'arrondissement.libelle as "'._("arrondissement").'"',
    'dossier_autorisation.erp_numero_batiment as "'._("erp_numero_batiment").'"',
    'dossier_autorisation.numero_version as "'._("numero_version").'"',
    'etat_dossier_autorisation4.libelle as "'._("etat_dossier_autorisation").'"',
    'avis_decision.libelle as "'._("avis_decision").'"',
    'etat_dossier_autorisation3.libelle as "'._("etat_dernier_dossier_instruction_accepte").'"',
    'dossier_autorisation.dossier_autorisation_libelle as "'._("dossier_autorisation_libelle").'"',
    'dossier_autorisation.cle_acces_citoyen as "'._("cle_acces_citoyen").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
$tri="ORDER BY dossier_autorisation_type_detaille.libelle ASC NULLS LAST";
$edition="dossier_autorisation";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (dossier_autorisation.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "arrondissement" => array("arrondissement", ),
    "avis_decision" => array("avis_decision", ),
    "dossier_autorisation_type_detaille" => array("dossier_autorisation_type_detaille", ),
    "etat_dossier_autorisation" => array("etat_dossier_autorisation", ),
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - arrondissement
if (in_array($retourformulaire, $foreign_keys_extended["arrondissement"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier_autorisation.arrondissement = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier_autorisation.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier_autorisation.arrondissement = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - avis_decision
if (in_array($retourformulaire, $foreign_keys_extended["avis_decision"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier_autorisation.avis_decision = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier_autorisation.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier_autorisation.avis_decision = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - dossier_autorisation_type_detaille
if (in_array($retourformulaire, $foreign_keys_extended["dossier_autorisation_type_detaille"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier_autorisation.dossier_autorisation_type_detaille = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier_autorisation.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier_autorisation.dossier_autorisation_type_detaille = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - etat_dossier_autorisation
if (in_array($retourformulaire, $foreign_keys_extended["etat_dossier_autorisation"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier_autorisation.etat_dernier_dossier_instruction_accepte = ".intval($idxformulaire)." OR dossier_autorisation.etat_dossier_autorisation = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier_autorisation.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier_autorisation.etat_dernier_dossier_instruction_accepte = ".intval($idxformulaire)." OR dossier_autorisation.etat_dossier_autorisation = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (dossier_autorisation.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (dossier_autorisation.om_collectivite = '".$_SESSION["collectivite"]."') AND (dossier_autorisation.om_collectivite = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'demande',
    'donnees_techniques',
    'dossier',
    'dossier_autorisation_parcelle',
    'lien_dossier_autorisation_demandeur',
    'lot',
);

?>