<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("groupe");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."groupe
    LEFT JOIN ".DB_PREFIXE."genre 
        ON groupe.genre=genre.genre ";
// SELECT 
$champAffiche = array(
    'groupe.groupe as "'._("groupe").'"',
    'groupe.code as "'._("code").'"',
    'groupe.libelle as "'._("libelle").'"',
    'genre.libelle as "'._("genre").'"',
    );
//
$champNonAffiche = array(
    'groupe.description as "'._("description").'"',
    );
//
$champRecherche = array(
    'groupe.groupe as "'._("groupe").'"',
    'groupe.code as "'._("code").'"',
    'groupe.libelle as "'._("libelle").'"',
    'genre.libelle as "'._("genre").'"',
    );
$tri="ORDER BY groupe.libelle ASC NULLS LAST";
$edition="groupe";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "genre" => array("genre", ),
);
// Filtre listing sous formulaire - genre
if (in_array($retourformulaire, $foreign_keys_extended["genre"])) {
    $selection = " WHERE (groupe.genre = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'demande_type',
    'dossier_autorisation_type',
    'lien_om_profil_groupe',
    'lien_om_utilisateur_groupe',
);

?>