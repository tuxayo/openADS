<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("moyen_retenu_juge");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."moyen_retenu_juge";
// SELECT 
$champAffiche = array(
    'moyen_retenu_juge.moyen_retenu_juge as "'._("moyen_retenu_juge").'"',
    'moyen_retenu_juge.code as "'._("code").'"',
    'moyen_retenu_juge.libelle as "'._("libelle").'"',
    'to_char(moyen_retenu_juge.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(moyen_retenu_juge.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
    );
//
$champNonAffiche = array(
    'moyen_retenu_juge.description as "'._("description").'"',
    );
//
$champRecherche = array(
    'moyen_retenu_juge.moyen_retenu_juge as "'._("moyen_retenu_juge").'"',
    'moyen_retenu_juge.code as "'._("code").'"',
    'moyen_retenu_juge.libelle as "'._("libelle").'"',
    );
$tri="ORDER BY moyen_retenu_juge.libelle ASC NULLS LAST";
$edition="moyen_retenu_juge";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((moyen_retenu_juge.om_validite_debut IS NULL AND (moyen_retenu_juge.om_validite_fin IS NULL OR moyen_retenu_juge.om_validite_fin > CURRENT_DATE)) OR (moyen_retenu_juge.om_validite_debut <= CURRENT_DATE AND (moyen_retenu_juge.om_validite_fin IS NULL OR moyen_retenu_juge.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((moyen_retenu_juge.om_validite_debut IS NULL AND (moyen_retenu_juge.om_validite_fin IS NULL OR moyen_retenu_juge.om_validite_fin > CURRENT_DATE)) OR (moyen_retenu_juge.om_validite_debut <= CURRENT_DATE AND (moyen_retenu_juge.om_validite_fin IS NULL OR moyen_retenu_juge.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'lien_donnees_techniques_moyen_retenu_juge',
);

?>