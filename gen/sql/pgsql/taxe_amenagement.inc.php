<?php
//$Id$ 
//gen openMairie le 19/05/2017 15:32

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("taxe_amenagement");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."taxe_amenagement
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON taxe_amenagement.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'taxe_amenagement.taxe_amenagement as "'._("taxe_amenagement").'"',
    "case taxe_amenagement.en_ile_de_france when 't' then 'Oui' else 'Non' end as \""._("en_ile_de_france")."\"",
    'taxe_amenagement.val_forf_surf_cstr as "'._("val_forf_surf_cstr").'"',
    'taxe_amenagement.val_forf_empl_tente_carav_rml as "'._("val_forf_empl_tente_carav_rml").'"',
    'taxe_amenagement.val_forf_empl_hll as "'._("val_forf_empl_hll").'"',
    'taxe_amenagement.val_forf_surf_piscine as "'._("val_forf_surf_piscine").'"',
    'taxe_amenagement.val_forf_nb_eolienne as "'._("val_forf_nb_eolienne").'"',
    'taxe_amenagement.val_forf_surf_pann_photo as "'._("val_forf_surf_pann_photo").'"',
    'taxe_amenagement.val_forf_nb_parking_ext as "'._("val_forf_nb_parking_ext").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
//
$champNonAffiche = array(
    'taxe_amenagement.om_collectivite as "'._("om_collectivite").'"',
    'taxe_amenagement.tx_depart as "'._("tx_depart").'"',
    'taxe_amenagement.tx_reg as "'._("tx_reg").'"',
    'taxe_amenagement.tx_comm_secteur_1 as "'._("tx_comm_secteur_1").'"',
    'taxe_amenagement.tx_comm_secteur_2 as "'._("tx_comm_secteur_2").'"',
    'taxe_amenagement.tx_comm_secteur_3 as "'._("tx_comm_secteur_3").'"',
    'taxe_amenagement.tx_comm_secteur_4 as "'._("tx_comm_secteur_4").'"',
    'taxe_amenagement.tx_comm_secteur_5 as "'._("tx_comm_secteur_5").'"',
    'taxe_amenagement.tx_comm_secteur_6 as "'._("tx_comm_secteur_6").'"',
    'taxe_amenagement.tx_comm_secteur_7 as "'._("tx_comm_secteur_7").'"',
    'taxe_amenagement.tx_comm_secteur_8 as "'._("tx_comm_secteur_8").'"',
    'taxe_amenagement.tx_comm_secteur_9 as "'._("tx_comm_secteur_9").'"',
    'taxe_amenagement.tx_comm_secteur_10 as "'._("tx_comm_secteur_10").'"',
    'taxe_amenagement.tx_comm_secteur_11 as "'._("tx_comm_secteur_11").'"',
    'taxe_amenagement.tx_comm_secteur_12 as "'._("tx_comm_secteur_12").'"',
    'taxe_amenagement.tx_comm_secteur_13 as "'._("tx_comm_secteur_13").'"',
    'taxe_amenagement.tx_comm_secteur_14 as "'._("tx_comm_secteur_14").'"',
    'taxe_amenagement.tx_comm_secteur_15 as "'._("tx_comm_secteur_15").'"',
    'taxe_amenagement.tx_comm_secteur_16 as "'._("tx_comm_secteur_16").'"',
    'taxe_amenagement.tx_comm_secteur_17 as "'._("tx_comm_secteur_17").'"',
    'taxe_amenagement.tx_comm_secteur_18 as "'._("tx_comm_secteur_18").'"',
    'taxe_amenagement.tx_comm_secteur_19 as "'._("tx_comm_secteur_19").'"',
    'taxe_amenagement.tx_comm_secteur_20 as "'._("tx_comm_secteur_20").'"',
    'taxe_amenagement.tx_exo_facul_1_reg as "'._("tx_exo_facul_1_reg").'"',
    'taxe_amenagement.tx_exo_facul_2_reg as "'._("tx_exo_facul_2_reg").'"',
    'taxe_amenagement.tx_exo_facul_3_reg as "'._("tx_exo_facul_3_reg").'"',
    'taxe_amenagement.tx_exo_facul_4_reg as "'._("tx_exo_facul_4_reg").'"',
    'taxe_amenagement.tx_exo_facul_5_reg as "'._("tx_exo_facul_5_reg").'"',
    'taxe_amenagement.tx_exo_facul_6_reg as "'._("tx_exo_facul_6_reg").'"',
    'taxe_amenagement.tx_exo_facul_7_reg as "'._("tx_exo_facul_7_reg").'"',
    'taxe_amenagement.tx_exo_facul_8_reg as "'._("tx_exo_facul_8_reg").'"',
    'taxe_amenagement.tx_exo_facul_9_reg as "'._("tx_exo_facul_9_reg").'"',
    'taxe_amenagement.tx_exo_facul_1_depart as "'._("tx_exo_facul_1_depart").'"',
    'taxe_amenagement.tx_exo_facul_2_depart as "'._("tx_exo_facul_2_depart").'"',
    'taxe_amenagement.tx_exo_facul_3_depart as "'._("tx_exo_facul_3_depart").'"',
    'taxe_amenagement.tx_exo_facul_4_depart as "'._("tx_exo_facul_4_depart").'"',
    'taxe_amenagement.tx_exo_facul_5_depart as "'._("tx_exo_facul_5_depart").'"',
    'taxe_amenagement.tx_exo_facul_6_depart as "'._("tx_exo_facul_6_depart").'"',
    'taxe_amenagement.tx_exo_facul_7_depart as "'._("tx_exo_facul_7_depart").'"',
    'taxe_amenagement.tx_exo_facul_8_depart as "'._("tx_exo_facul_8_depart").'"',
    'taxe_amenagement.tx_exo_facul_9_depart as "'._("tx_exo_facul_9_depart").'"',
    'taxe_amenagement.tx_exo_facul_1_comm as "'._("tx_exo_facul_1_comm").'"',
    'taxe_amenagement.tx_exo_facul_2_comm as "'._("tx_exo_facul_2_comm").'"',
    'taxe_amenagement.tx_exo_facul_3_comm as "'._("tx_exo_facul_3_comm").'"',
    'taxe_amenagement.tx_exo_facul_4_comm as "'._("tx_exo_facul_4_comm").'"',
    'taxe_amenagement.tx_exo_facul_5_comm as "'._("tx_exo_facul_5_comm").'"',
    'taxe_amenagement.tx_exo_facul_6_comm as "'._("tx_exo_facul_6_comm").'"',
    'taxe_amenagement.tx_exo_facul_7_comm as "'._("tx_exo_facul_7_comm").'"',
    'taxe_amenagement.tx_exo_facul_8_comm as "'._("tx_exo_facul_8_comm").'"',
    'taxe_amenagement.tx_exo_facul_9_comm as "'._("tx_exo_facul_9_comm").'"',
    'taxe_amenagement.tx_rap as "'._("tx_rap").'"',
    );
//
$champRecherche = array(
    'taxe_amenagement.taxe_amenagement as "'._("taxe_amenagement").'"',
    'taxe_amenagement.val_forf_surf_cstr as "'._("val_forf_surf_cstr").'"',
    'taxe_amenagement.val_forf_empl_tente_carav_rml as "'._("val_forf_empl_tente_carav_rml").'"',
    'taxe_amenagement.val_forf_empl_hll as "'._("val_forf_empl_hll").'"',
    'taxe_amenagement.val_forf_surf_piscine as "'._("val_forf_surf_piscine").'"',
    'taxe_amenagement.val_forf_nb_eolienne as "'._("val_forf_nb_eolienne").'"',
    'taxe_amenagement.val_forf_surf_pann_photo as "'._("val_forf_surf_pann_photo").'"',
    'taxe_amenagement.val_forf_nb_parking_ext as "'._("val_forf_nb_parking_ext").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
$tri="ORDER BY om_collectivite.libelle ASC NULLS LAST";
$edition="taxe_amenagement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (taxe_amenagement.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (taxe_amenagement.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (taxe_amenagement.om_collectivite = '".$_SESSION["collectivite"]."') AND (taxe_amenagement.om_collectivite = ".intval($idxformulaire).") ";
    }
}

?>