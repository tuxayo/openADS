<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("instruction");
$tableSelect=DB_PREFIXE."instruction";
$champs=array(
    "instruction",
    "destinataire",
    "date_evenement",
    "evenement",
    "lettretype",
    "complement_om_html",
    "complement2_om_html",
    "dossier",
    "action",
    "delai",
    "etat",
    "accord_tacite",
    "delai_notification",
    "archive_delai",
    "archive_date_complet",
    "archive_date_rejet",
    "archive_date_limite",
    "archive_date_notification_delai",
    "archive_accord_tacite",
    "archive_etat",
    "archive_date_decision",
    "archive_avis",
    "archive_date_validite",
    "archive_date_achevement",
    "archive_date_chantier",
    "archive_date_conformite",
    "complement3_om_html",
    "complement4_om_html",
    "complement5_om_html",
    "complement6_om_html",
    "complement7_om_html",
    "complement8_om_html",
    "complement9_om_html",
    "complement10_om_html",
    "complement11_om_html",
    "complement12_om_html",
    "complement13_om_html",
    "complement14_om_html",
    "complement15_om_html",
    "avis_decision",
    "date_finalisation_courrier",
    "date_envoi_signature",
    "date_retour_signature",
    "date_envoi_rar",
    "date_retour_rar",
    "date_envoi_controle_legalite",
    "date_retour_controle_legalite",
    "signataire_arrete",
    "numero_arrete",
    "archive_date_dernier_depot",
    "archive_incompletude",
    "archive_evenement_suivant_tacite",
    "archive_evenement_suivant_tacite_incompletude",
    "archive_etat_pendant_incompletude",
    "archive_date_limite_incompletude",
    "archive_delai_incompletude",
    "code_barres",
    "om_fichier_instruction",
    "om_final_instruction",
    "document_numerise",
    "archive_autorite_competente",
    "autorite_competente",
    "duree_validite_parametrage",
    "duree_validite",
    "archive_incomplet_notifie",
    "om_final_instruction_utilisateur",
    "created_by_commune",
    "date_depot",
    "archive_date_cloture_instruction",
    "archive_date_premiere_visite",
    "archive_date_derniere_visite",
    "archive_date_contradictoire",
    "archive_date_retour_contradictoire",
    "archive_date_ait",
    "archive_date_transmission_parquet");
//champs select
$sql_action="SELECT action.action, action.libelle FROM ".DB_PREFIXE."action ORDER BY action.libelle ASC";
$sql_action_by_id = "SELECT action.action, action.libelle FROM ".DB_PREFIXE."action WHERE action = '<idx>'";
$sql_archive_etat_pendant_incompletude="SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat ORDER BY etat.libelle ASC";
$sql_archive_etat_pendant_incompletude_by_id = "SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat WHERE etat = '<idx>'";
$sql_archive_evenement_suivant_tacite="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_archive_evenement_suivant_tacite_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
$sql_archive_evenement_suivant_tacite_incompletude="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_archive_evenement_suivant_tacite_incompletude_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
$sql_autorite_competente="SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente ORDER BY autorite_competente.libelle ASC";
$sql_autorite_competente_by_id = "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente WHERE autorite_competente = <idx>";
$sql_avis_decision="SELECT avis_decision.avis_decision, avis_decision.libelle FROM ".DB_PREFIXE."avis_decision ORDER BY avis_decision.libelle ASC";
$sql_avis_decision_by_id = "SELECT avis_decision.avis_decision, avis_decision.libelle FROM ".DB_PREFIXE."avis_decision WHERE avis_decision = '<idx>'";
$sql_document_numerise="SELECT document_numerise.document_numerise, document_numerise.uid FROM ".DB_PREFIXE."document_numerise ORDER BY document_numerise.uid ASC";
$sql_document_numerise_by_id = "SELECT document_numerise.document_numerise, document_numerise.uid FROM ".DB_PREFIXE."document_numerise WHERE document_numerise = <idx>";
$sql_dossier="SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier ORDER BY dossier.annee ASC";
$sql_dossier_by_id = "SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier WHERE dossier = '<idx>'";
$sql_etat="SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat ORDER BY etat.libelle ASC";
$sql_etat_by_id = "SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat WHERE etat = '<idx>'";
$sql_evenement="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_evenement_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
$sql_signataire_arrete="SELECT signataire_arrete.signataire_arrete, signataire_arrete.civilite FROM ".DB_PREFIXE."signataire_arrete WHERE ((signataire_arrete.om_validite_debut IS NULL AND (signataire_arrete.om_validite_fin IS NULL OR signataire_arrete.om_validite_fin > CURRENT_DATE)) OR (signataire_arrete.om_validite_debut <= CURRENT_DATE AND (signataire_arrete.om_validite_fin IS NULL OR signataire_arrete.om_validite_fin > CURRENT_DATE))) ORDER BY signataire_arrete.civilite ASC";
$sql_signataire_arrete_by_id = "SELECT signataire_arrete.signataire_arrete, signataire_arrete.civilite FROM ".DB_PREFIXE."signataire_arrete WHERE signataire_arrete = <idx>";
?>