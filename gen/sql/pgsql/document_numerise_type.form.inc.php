<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("document_numerise_type");
$tableSelect=DB_PREFIXE."document_numerise_type";
$champs=array(
    "document_numerise_type",
    "code",
    "libelle",
    "document_numerise_type_categorie",
    "aff_service_consulte",
    "aff_da",
    "synchro_metadonnee");
//champs select
$sql_document_numerise_type_categorie="SELECT document_numerise_type_categorie.document_numerise_type_categorie, document_numerise_type_categorie.libelle FROM ".DB_PREFIXE."document_numerise_type_categorie ORDER BY document_numerise_type_categorie.libelle ASC";
$sql_document_numerise_type_categorie_by_id = "SELECT document_numerise_type_categorie.document_numerise_type_categorie, document_numerise_type_categorie.libelle FROM ".DB_PREFIXE."document_numerise_type_categorie WHERE document_numerise_type_categorie = <idx>";
?>