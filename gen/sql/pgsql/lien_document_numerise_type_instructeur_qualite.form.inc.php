<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("lien_document_numerise_type_instructeur_qualite");
$tableSelect=DB_PREFIXE."lien_document_numerise_type_instructeur_qualite";
$champs=array(
    "lien_document_numerise_type_instructeur_qualite",
    "document_numerise_type",
    "instructeur_qualite");
//champs select
$sql_document_numerise_type="SELECT document_numerise_type.document_numerise_type, document_numerise_type.libelle FROM ".DB_PREFIXE."document_numerise_type ORDER BY document_numerise_type.libelle ASC";
$sql_document_numerise_type_by_id = "SELECT document_numerise_type.document_numerise_type, document_numerise_type.libelle FROM ".DB_PREFIXE."document_numerise_type WHERE document_numerise_type = <idx>";
$sql_instructeur_qualite="SELECT instructeur_qualite.instructeur_qualite, instructeur_qualite.libelle FROM ".DB_PREFIXE."instructeur_qualite ORDER BY instructeur_qualite.libelle ASC";
$sql_instructeur_qualite_by_id = "SELECT instructeur_qualite.instructeur_qualite, instructeur_qualite.libelle FROM ".DB_PREFIXE."instructeur_qualite WHERE instructeur_qualite = <idx>";
?>