<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("lien_om_profil_groupe");
$tableSelect=DB_PREFIXE."lien_om_profil_groupe";
$champs=array(
    "lien_om_profil_groupe",
    "om_profil",
    "groupe",
    "confidentiel",
    "enregistrement_demande");
//champs select
$sql_groupe="SELECT groupe.groupe, groupe.libelle FROM ".DB_PREFIXE."groupe ORDER BY groupe.libelle ASC";
$sql_groupe_by_id = "SELECT groupe.groupe, groupe.libelle FROM ".DB_PREFIXE."groupe WHERE groupe = <idx>";
$sql_om_profil="SELECT om_profil.om_profil, om_profil.libelle FROM ".DB_PREFIXE."om_profil WHERE ((om_profil.om_validite_debut IS NULL AND (om_profil.om_validite_fin IS NULL OR om_profil.om_validite_fin > CURRENT_DATE)) OR (om_profil.om_validite_debut <= CURRENT_DATE AND (om_profil.om_validite_fin IS NULL OR om_profil.om_validite_fin > CURRENT_DATE))) ORDER BY om_profil.libelle ASC";
$sql_om_profil_by_id = "SELECT om_profil.om_profil, om_profil.libelle FROM ".DB_PREFIXE."om_profil WHERE om_profil = <idx>";
?>