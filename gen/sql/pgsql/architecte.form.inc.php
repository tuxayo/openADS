<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("architecte");
$tableSelect=DB_PREFIXE."architecte";
$champs=array(
    "architecte",
    "nom",
    "prenom",
    "adresse1",
    "adresse2",
    "cp",
    "ville",
    "pays",
    "inscription",
    "telephone",
    "fax",
    "email",
    "note",
    "frequent",
    "nom_cabinet",
    "conseil_regional");
?>