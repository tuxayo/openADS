<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("dossier_contrainte");
$tableSelect=DB_PREFIXE."dossier_contrainte";
$champs=array(
    "dossier_contrainte",
    "dossier",
    "contrainte",
    "texte_complete",
    "reference");
//champs select
$sql_contrainte="SELECT contrainte.contrainte, contrainte.libelle FROM ".DB_PREFIXE."contrainte WHERE ((contrainte.om_validite_debut IS NULL AND (contrainte.om_validite_fin IS NULL OR contrainte.om_validite_fin > CURRENT_DATE)) OR (contrainte.om_validite_debut <= CURRENT_DATE AND (contrainte.om_validite_fin IS NULL OR contrainte.om_validite_fin > CURRENT_DATE))) ORDER BY contrainte.libelle ASC";
$sql_contrainte_by_id = "SELECT contrainte.contrainte, contrainte.libelle FROM ".DB_PREFIXE."contrainte WHERE contrainte = <idx>";
$sql_dossier="SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier ORDER BY dossier.annee ASC";
$sql_dossier_by_id = "SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier WHERE dossier = '<idx>'";
?>