<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("transition");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."transition
    LEFT JOIN ".DB_PREFIXE."etat 
        ON transition.etat=etat.etat 
    LEFT JOIN ".DB_PREFIXE."evenement 
        ON transition.evenement=evenement.evenement ";
// SELECT 
$champAffiche = array(
    'transition.transition as "'._("transition").'"',
    'etat.libelle as "'._("etat").'"',
    'evenement.libelle as "'._("evenement").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'transition.transition as "'._("transition").'"',
    'etat.libelle as "'._("etat").'"',
    'evenement.libelle as "'._("evenement").'"',
    );
$tri="ORDER BY etat.libelle ASC NULLS LAST";
$edition="transition";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etat" => array("etat", ),
    "evenement" => array("evenement", ),
);
// Filtre listing sous formulaire - etat
if (in_array($retourformulaire, $foreign_keys_extended["etat"])) {
    $selection = " WHERE (transition.etat = '".$f->db->escapeSimple($idxformulaire)."') ";
}
// Filtre listing sous formulaire - evenement
if (in_array($retourformulaire, $foreign_keys_extended["evenement"])) {
    $selection = " WHERE (transition.evenement = ".intval($idxformulaire).") ";
}

?>