<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("demandeur");
$tableSelect=DB_PREFIXE."demandeur";
$champs=array(
    "demandeur",
    "type_demandeur",
    "qualite",
    "particulier_nom",
    "particulier_prenom",
    "particulier_date_naissance",
    "particulier_commune_naissance",
    "particulier_departement_naissance",
    "personne_morale_denomination",
    "personne_morale_raison_sociale",
    "personne_morale_siret",
    "personne_morale_categorie_juridique",
    "personne_morale_nom",
    "personne_morale_prenom",
    "numero",
    "voie",
    "complement",
    "lieu_dit",
    "localite",
    "code_postal",
    "bp",
    "cedex",
    "pays",
    "division_territoriale",
    "telephone_fixe",
    "telephone_mobile",
    "indicatif",
    "courriel",
    "notification",
    "frequent",
    "particulier_civilite",
    "personne_morale_civilite",
    "fax",
    "om_collectivite");
//champs select
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
$sql_particulier_civilite="SELECT civilite.civilite, civilite.libelle FROM ".DB_PREFIXE."civilite WHERE ((civilite.om_validite_debut IS NULL AND (civilite.om_validite_fin IS NULL OR civilite.om_validite_fin > CURRENT_DATE)) OR (civilite.om_validite_debut <= CURRENT_DATE AND (civilite.om_validite_fin IS NULL OR civilite.om_validite_fin > CURRENT_DATE))) ORDER BY civilite.libelle ASC";
$sql_particulier_civilite_by_id = "SELECT civilite.civilite, civilite.libelle FROM ".DB_PREFIXE."civilite WHERE civilite = '<idx>'";
$sql_personne_morale_civilite="SELECT civilite.civilite, civilite.libelle FROM ".DB_PREFIXE."civilite WHERE ((civilite.om_validite_debut IS NULL AND (civilite.om_validite_fin IS NULL OR civilite.om_validite_fin > CURRENT_DATE)) OR (civilite.om_validite_debut <= CURRENT_DATE AND (civilite.om_validite_fin IS NULL OR civilite.om_validite_fin > CURRENT_DATE))) ORDER BY civilite.libelle ASC";
$sql_personne_morale_civilite_by_id = "SELECT civilite.civilite, civilite.libelle FROM ".DB_PREFIXE."civilite WHERE civilite = '<idx>'";
?>