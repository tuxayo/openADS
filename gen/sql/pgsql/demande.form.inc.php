<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("demande");
$tableSelect=DB_PREFIXE."demande";
$champs=array(
    "demande",
    "dossier_autorisation_type_detaille",
    "demande_type",
    "dossier_instruction",
    "dossier_autorisation",
    "date_demande",
    "terrain_references_cadastrales",
    "terrain_adresse_voie_numero",
    "terrain_adresse_voie",
    "terrain_adresse_lieu_dit",
    "terrain_adresse_localite",
    "terrain_adresse_code_postal",
    "terrain_adresse_bp",
    "terrain_adresse_cedex",
    "terrain_superficie",
    "instruction_recepisse",
    "arrondissement",
    "om_collectivite",
    "autorisation_contestee");
//champs select
$sql_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement ORDER BY arrondissement.libelle ASC";
$sql_arrondissement_by_id = "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE arrondissement = <idx>";
$sql_autorisation_contestee="SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier ORDER BY dossier.annee ASC";
$sql_autorisation_contestee_by_id = "SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier WHERE dossier = '<idx>'";
$sql_demande_type="SELECT demande_type.demande_type, demande_type.libelle FROM ".DB_PREFIXE."demande_type ORDER BY demande_type.libelle ASC";
$sql_demande_type_by_id = "SELECT demande_type.demande_type, demande_type.libelle FROM ".DB_PREFIXE."demande_type WHERE demande_type = <idx>";
$sql_dossier_autorisation="SELECT dossier_autorisation.dossier_autorisation, dossier_autorisation.dossier_autorisation_type_detaille FROM ".DB_PREFIXE."dossier_autorisation ORDER BY dossier_autorisation.dossier_autorisation_type_detaille ASC";
$sql_dossier_autorisation_by_id = "SELECT dossier_autorisation.dossier_autorisation, dossier_autorisation.dossier_autorisation_type_detaille FROM ".DB_PREFIXE."dossier_autorisation WHERE dossier_autorisation = '<idx>'";
$sql_dossier_autorisation_type_detaille="SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille ORDER BY dossier_autorisation_type_detaille.libelle ASC";
$sql_dossier_autorisation_type_detaille_by_id = "SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille WHERE dossier_autorisation_type_detaille = <idx>";
$sql_dossier_instruction="SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier ORDER BY dossier.annee ASC";
$sql_dossier_instruction_by_id = "SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier WHERE dossier = '<idx>'";
$sql_instruction_recepisse="SELECT instruction.instruction, instruction.destinataire FROM ".DB_PREFIXE."instruction ORDER BY instruction.destinataire ASC";
$sql_instruction_recepisse_by_id = "SELECT instruction.instruction, instruction.destinataire FROM ".DB_PREFIXE."instruction WHERE instruction = <idx>";
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
?>