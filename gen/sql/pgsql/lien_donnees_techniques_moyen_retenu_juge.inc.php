<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("lien_donnees_techniques_moyen_retenu_juge");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."lien_donnees_techniques_moyen_retenu_juge
    LEFT JOIN ".DB_PREFIXE."donnees_techniques 
        ON lien_donnees_techniques_moyen_retenu_juge.donnees_techniques=donnees_techniques.donnees_techniques 
    LEFT JOIN ".DB_PREFIXE."moyen_retenu_juge 
        ON lien_donnees_techniques_moyen_retenu_juge.moyen_retenu_juge=moyen_retenu_juge.moyen_retenu_juge ";
// SELECT 
$champAffiche = array(
    'lien_donnees_techniques_moyen_retenu_juge.lien_donnees_techniques_moyen_retenu_juge as "'._("lien_donnees_techniques_moyen_retenu_juge").'"',
    'donnees_techniques.dossier_instruction as "'._("donnees_techniques").'"',
    'moyen_retenu_juge.libelle as "'._("moyen_retenu_juge").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_donnees_techniques_moyen_retenu_juge.lien_donnees_techniques_moyen_retenu_juge as "'._("lien_donnees_techniques_moyen_retenu_juge").'"',
    'donnees_techniques.dossier_instruction as "'._("donnees_techniques").'"',
    'moyen_retenu_juge.libelle as "'._("moyen_retenu_juge").'"',
    );
$tri="ORDER BY donnees_techniques.dossier_instruction ASC NULLS LAST";
$edition="lien_donnees_techniques_moyen_retenu_juge";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "donnees_techniques" => array("donnees_techniques", ),
    "moyen_retenu_juge" => array("moyen_retenu_juge", ),
);
// Filtre listing sous formulaire - donnees_techniques
if (in_array($retourformulaire, $foreign_keys_extended["donnees_techniques"])) {
    $selection = " WHERE (lien_donnees_techniques_moyen_retenu_juge.donnees_techniques = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - moyen_retenu_juge
if (in_array($retourformulaire, $foreign_keys_extended["moyen_retenu_juge"])) {
    $selection = " WHERE (lien_donnees_techniques_moyen_retenu_juge.moyen_retenu_juge = ".intval($idxformulaire).") ";
}

?>