<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_instruction_type");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_instruction_type
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille 
        ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille ";
// SELECT 
$champAffiche = array(
    'dossier_instruction_type.dossier_instruction_type as "'._("dossier_instruction_type").'"',
    'dossier_instruction_type.code as "'._("code").'"',
    'dossier_instruction_type.libelle as "'._("libelle").'"',
    'dossier_autorisation_type_detaille.libelle as "'._("dossier_autorisation_type_detaille").'"',
    "case dossier_instruction_type.suffixe when 't' then 'Oui' else 'Non' end as \""._("suffixe")."\"",
    'dossier_instruction_type.mouvement_sitadel as "'._("mouvement_sitadel").'"',
    );
//
$champNonAffiche = array(
    'dossier_instruction_type.description as "'._("description").'"',
    );
//
$champRecherche = array(
    'dossier_instruction_type.dossier_instruction_type as "'._("dossier_instruction_type").'"',
    'dossier_instruction_type.code as "'._("code").'"',
    'dossier_instruction_type.libelle as "'._("libelle").'"',
    'dossier_autorisation_type_detaille.libelle as "'._("dossier_autorisation_type_detaille").'"',
    'dossier_instruction_type.mouvement_sitadel as "'._("mouvement_sitadel").'"',
    );
$tri="ORDER BY dossier_instruction_type.libelle ASC NULLS LAST";
$edition="dossier_instruction_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier_autorisation_type_detaille" => array("dossier_autorisation_type_detaille", ),
);
// Filtre listing sous formulaire - dossier_autorisation_type_detaille
if (in_array($retourformulaire, $foreign_keys_extended["dossier_autorisation_type_detaille"])) {
    $selection = " WHERE (dossier_instruction_type.dossier_autorisation_type_detaille = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'demande_type',
    'dossier',
    'lien_dossier_instruction_type_evenement',
);

?>