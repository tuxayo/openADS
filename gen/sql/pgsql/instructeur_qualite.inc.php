<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("instructeur_qualite");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."instructeur_qualite";
// SELECT 
$champAffiche = array(
    'instructeur_qualite.instructeur_qualite as "'._("instructeur_qualite").'"',
    'instructeur_qualite.code as "'._("code").'"',
    'instructeur_qualite.libelle as "'._("libelle").'"',
    );
//
$champNonAffiche = array(
    'instructeur_qualite.description as "'._("description").'"',
    );
//
$champRecherche = array(
    'instructeur_qualite.instructeur_qualite as "'._("instructeur_qualite").'"',
    'instructeur_qualite.code as "'._("code").'"',
    'instructeur_qualite.libelle as "'._("libelle").'"',
    );
$tri="ORDER BY instructeur_qualite.libelle ASC NULLS LAST";
$edition="instructeur_qualite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'instructeur',
    'lien_document_numerise_type_instructeur_qualite',
);

?>