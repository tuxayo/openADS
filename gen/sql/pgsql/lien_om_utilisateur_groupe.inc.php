<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("lien_om_utilisateur_groupe");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."lien_om_utilisateur_groupe
    LEFT JOIN ".DB_PREFIXE."groupe 
        ON lien_om_utilisateur_groupe.groupe=groupe.groupe ";
// SELECT 
$champAffiche = array(
    'lien_om_utilisateur_groupe.lien_om_utilisateur_groupe as "'._("lien_om_utilisateur_groupe").'"',
    'lien_om_utilisateur_groupe.login as "'._("login").'"',
    'groupe.libelle as "'._("groupe").'"',
    "case lien_om_utilisateur_groupe.confidentiel when 't' then 'Oui' else 'Non' end as \""._("confidentiel")."\"",
    "case lien_om_utilisateur_groupe.enregistrement_demande when 't' then 'Oui' else 'Non' end as \""._("enregistrement_demande")."\"",
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_om_utilisateur_groupe.lien_om_utilisateur_groupe as "'._("lien_om_utilisateur_groupe").'"',
    'lien_om_utilisateur_groupe.login as "'._("login").'"',
    'groupe.libelle as "'._("groupe").'"',
    );
$tri="ORDER BY lien_om_utilisateur_groupe.login ASC NULLS LAST";
$edition="lien_om_utilisateur_groupe";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "groupe" => array("groupe", ),
);
// Filtre listing sous formulaire - groupe
if (in_array($retourformulaire, $foreign_keys_extended["groupe"])) {
    $selection = " WHERE (lien_om_utilisateur_groupe.groupe = ".intval($idxformulaire).") ";
}

?>