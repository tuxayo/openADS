<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_contrainte");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_contrainte
    LEFT JOIN ".DB_PREFIXE."contrainte 
        ON dossier_contrainte.contrainte=contrainte.contrainte 
    LEFT JOIN ".DB_PREFIXE."dossier 
        ON dossier_contrainte.dossier=dossier.dossier ";
// SELECT 
$champAffiche = array(
    'dossier_contrainte.dossier_contrainte as "'._("dossier_contrainte").'"',
    'dossier.annee as "'._("dossier").'"',
    'contrainte.libelle as "'._("contrainte").'"',
    "case dossier_contrainte.reference when 't' then 'Oui' else 'Non' end as \""._("reference")."\"",
    );
//
$champNonAffiche = array(
    'dossier_contrainte.texte_complete as "'._("texte_complete").'"',
    );
//
$champRecherche = array(
    'dossier_contrainte.dossier_contrainte as "'._("dossier_contrainte").'"',
    'dossier.annee as "'._("dossier").'"',
    'contrainte.libelle as "'._("contrainte").'"',
    );
$tri="ORDER BY dossier.annee ASC NULLS LAST";
$edition="dossier_contrainte";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "contrainte" => array("contrainte", ),
    "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
);
// Filtre listing sous formulaire - contrainte
if (in_array($retourformulaire, $foreign_keys_extended["contrainte"])) {
    $selection = " WHERE (dossier_contrainte.contrainte = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier
if (in_array($retourformulaire, $foreign_keys_extended["dossier"])) {
    $selection = " WHERE (dossier_contrainte.dossier = '".$f->db->escapeSimple($idxformulaire)."') ";
}

?>