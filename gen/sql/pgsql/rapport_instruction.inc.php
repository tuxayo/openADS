<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("rapport_instruction");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."rapport_instruction
    LEFT JOIN ".DB_PREFIXE."dossier 
        ON rapport_instruction.dossier_instruction=dossier.dossier ";
// SELECT 
$champAffiche = array(
    'rapport_instruction.rapport_instruction as "'._("rapport_instruction").'"',
    'dossier.annee as "'._("dossier_instruction").'"',
    'rapport_instruction.om_fichier_rapport_instruction as "'._("om_fichier_rapport_instruction").'"',
    "case rapport_instruction.om_final_rapport_instruction when 't' then 'Oui' else 'Non' end as \""._("om_final_rapport_instruction")."\"",
    );
//
$champNonAffiche = array(
    'rapport_instruction.analyse_reglementaire_om_html as "'._("analyse_reglementaire_om_html").'"',
    'rapport_instruction.description_projet_om_html as "'._("description_projet_om_html").'"',
    'rapport_instruction.proposition_decision as "'._("proposition_decision").'"',
    'rapport_instruction.complement_om_html as "'._("complement_om_html").'"',
    );
//
$champRecherche = array(
    'rapport_instruction.rapport_instruction as "'._("rapport_instruction").'"',
    'dossier.annee as "'._("dossier_instruction").'"',
    'rapport_instruction.om_fichier_rapport_instruction as "'._("om_fichier_rapport_instruction").'"',
    );
$tri="ORDER BY dossier.annee ASC NULLS LAST";
$edition="rapport_instruction";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
);
// Filtre listing sous formulaire - dossier
if (in_array($retourformulaire, $foreign_keys_extended["dossier"])) {
    $selection = " WHERE (rapport_instruction.dossier_instruction = '".$f->db->escapeSimple($idxformulaire)."') ";
}

?>