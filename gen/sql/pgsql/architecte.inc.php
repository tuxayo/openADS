<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("architecte");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."architecte";
// SELECT 
$champAffiche = array(
    'architecte.architecte as "'._("architecte").'"',
    'architecte.nom as "'._("nom").'"',
    'architecte.prenom as "'._("prenom").'"',
    'architecte.adresse1 as "'._("adresse1").'"',
    'architecte.adresse2 as "'._("adresse2").'"',
    'architecte.cp as "'._("cp").'"',
    'architecte.ville as "'._("ville").'"',
    'architecte.pays as "'._("pays").'"',
    'architecte.inscription as "'._("inscription").'"',
    'architecte.telephone as "'._("telephone").'"',
    'architecte.fax as "'._("fax").'"',
    'architecte.email as "'._("email").'"',
    "case architecte.frequent when 't' then 'Oui' else 'Non' end as \""._("frequent")."\"",
    'architecte.nom_cabinet as "'._("nom_cabinet").'"',
    'architecte.conseil_regional as "'._("conseil_regional").'"',
    );
//
$champNonAffiche = array(
    'architecte.note as "'._("note").'"',
    );
//
$champRecherche = array(
    'architecte.architecte as "'._("architecte").'"',
    'architecte.nom as "'._("nom").'"',
    'architecte.prenom as "'._("prenom").'"',
    'architecte.adresse1 as "'._("adresse1").'"',
    'architecte.adresse2 as "'._("adresse2").'"',
    'architecte.cp as "'._("cp").'"',
    'architecte.ville as "'._("ville").'"',
    'architecte.pays as "'._("pays").'"',
    'architecte.inscription as "'._("inscription").'"',
    'architecte.telephone as "'._("telephone").'"',
    'architecte.fax as "'._("fax").'"',
    'architecte.email as "'._("email").'"',
    'architecte.nom_cabinet as "'._("nom_cabinet").'"',
    'architecte.conseil_regional as "'._("conseil_regional").'"',
    );
$tri="ORDER BY architecte.nom ASC NULLS LAST";
$edition="architecte";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'donnees_techniques',
);

?>