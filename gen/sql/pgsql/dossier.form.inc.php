<?php
//$Id$ 
//gen openMairie le 11/07/2017 17:00

$DEBUG=0;
$ent = _("application")." -> "._("dossier");
$tableSelect=DB_PREFIXE."dossier";
$champs=array(
    "dossier",
    "annee",
    "etat",
    "instructeur",
    "date_demande",
    "date_depot",
    "date_complet",
    "date_rejet",
    "date_notification_delai",
    "delai",
    "date_limite",
    "accord_tacite",
    "date_decision",
    "date_validite",
    "date_chantier",
    "date_achevement",
    "date_conformite",
    "description",
    "erp",
    "avis_decision",
    "enjeu_erp",
    "enjeu_urba",
    "division",
    "autorite_competente",
    "a_qualifier",
    "terrain_references_cadastrales",
    "terrain_adresse_voie_numero",
    "terrain_adresse_voie",
    "terrain_adresse_lieu_dit",
    "terrain_adresse_localite",
    "terrain_adresse_code_postal",
    "terrain_adresse_bp",
    "terrain_adresse_cedex",
    "terrain_superficie",
    "dossier_autorisation",
    "dossier_instruction_type",
    "date_dernier_depot",
    "version",
    "incompletude",
    "evenement_suivant_tacite",
    "evenement_suivant_tacite_incompletude",
    "etat_pendant_incompletude",
    "date_limite_incompletude",
    "delai_incompletude",
    "dossier_libelle",
    "numero_versement_archive",
    "duree_validite",
    "quartier",
    "incomplet_notifie",
    "om_collectivite",
    "tax_secteur",
    "tax_mtn_part_commu",
    "tax_mtn_part_depart",
    "tax_mtn_part_reg",
    "tax_mtn_total",
    "log_instructions",
    "interface_referentiel_erp",
    "autorisation_contestee",
    "date_cloture_instruction",
    "date_premiere_visite",
    "date_derniere_visite",
    "date_contradictoire",
    "date_retour_contradictoire",
    "date_ait",
    "date_transmission_parquet",
    "instructeur_2",
    "tax_mtn_rap",
    "tax_mtn_part_commu_sans_exo",
    "tax_mtn_part_depart_sans_exo",
    "tax_mtn_part_reg_sans_exo",
    "tax_mtn_total_sans_exo",
    "tax_mtn_rap_sans_exo",
    "geom",
    "geom1");
//champs select
$sql_autorisation_contestee="SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier ORDER BY dossier.annee ASC";
$sql_autorisation_contestee_by_id = "SELECT dossier.dossier, dossier.annee FROM ".DB_PREFIXE."dossier WHERE dossier = '<idx>'";
$sql_autorite_competente="SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente ORDER BY autorite_competente.libelle ASC";
$sql_autorite_competente_by_id = "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente WHERE autorite_competente = <idx>";
$sql_avis_decision="SELECT avis_decision.avis_decision, avis_decision.libelle FROM ".DB_PREFIXE."avis_decision ORDER BY avis_decision.libelle ASC";
$sql_avis_decision_by_id = "SELECT avis_decision.avis_decision, avis_decision.libelle FROM ".DB_PREFIXE."avis_decision WHERE avis_decision = '<idx>'";
$sql_division="SELECT division.division, division.libelle FROM ".DB_PREFIXE."division WHERE ((division.om_validite_debut IS NULL AND (division.om_validite_fin IS NULL OR division.om_validite_fin > CURRENT_DATE)) OR (division.om_validite_debut <= CURRENT_DATE AND (division.om_validite_fin IS NULL OR division.om_validite_fin > CURRENT_DATE))) ORDER BY division.libelle ASC";
$sql_division_by_id = "SELECT division.division, division.libelle FROM ".DB_PREFIXE."division WHERE division = <idx>";
$sql_dossier_autorisation="SELECT dossier_autorisation.dossier_autorisation, dossier_autorisation.dossier_autorisation_type_detaille FROM ".DB_PREFIXE."dossier_autorisation ORDER BY dossier_autorisation.dossier_autorisation_type_detaille ASC";
$sql_dossier_autorisation_by_id = "SELECT dossier_autorisation.dossier_autorisation, dossier_autorisation.dossier_autorisation_type_detaille FROM ".DB_PREFIXE."dossier_autorisation WHERE dossier_autorisation = '<idx>'";
$sql_dossier_instruction_type="SELECT dossier_instruction_type.dossier_instruction_type, dossier_instruction_type.libelle FROM ".DB_PREFIXE."dossier_instruction_type ORDER BY dossier_instruction_type.libelle ASC";
$sql_dossier_instruction_type_by_id = "SELECT dossier_instruction_type.dossier_instruction_type, dossier_instruction_type.libelle FROM ".DB_PREFIXE."dossier_instruction_type WHERE dossier_instruction_type = <idx>";
$sql_etat="SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat ORDER BY etat.libelle ASC";
$sql_etat_by_id = "SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat WHERE etat = '<idx>'";
$sql_etat_pendant_incompletude="SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat ORDER BY etat.libelle ASC";
$sql_etat_pendant_incompletude_by_id = "SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat WHERE etat = '<idx>'";
$sql_evenement_suivant_tacite="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_evenement_suivant_tacite_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
$sql_evenement_suivant_tacite_incompletude="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_evenement_suivant_tacite_incompletude_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
$sql_instructeur="SELECT instructeur.instructeur, instructeur.nom FROM ".DB_PREFIXE."instructeur WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE))) ORDER BY instructeur.nom ASC";
$sql_instructeur_by_id = "SELECT instructeur.instructeur, instructeur.nom FROM ".DB_PREFIXE."instructeur WHERE instructeur = <idx>";
$sql_instructeur_2="SELECT instructeur.instructeur, instructeur.nom FROM ".DB_PREFIXE."instructeur WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE))) ORDER BY instructeur.nom ASC";
$sql_instructeur_2_by_id = "SELECT instructeur.instructeur, instructeur.nom FROM ".DB_PREFIXE."instructeur WHERE instructeur = <idx>";
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
$sql_quartier="SELECT quartier.quartier, quartier.libelle FROM ".DB_PREFIXE."quartier ORDER BY quartier.libelle ASC";
$sql_quartier_by_id = "SELECT quartier.quartier, quartier.libelle FROM ".DB_PREFIXE."quartier WHERE quartier = <idx>";
?>