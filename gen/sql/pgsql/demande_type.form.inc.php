<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("demande_type");
$tableSelect=DB_PREFIXE."demande_type";
$champs=array(
    "demande_type",
    "code",
    "libelle",
    "description",
    "demande_nature",
    "groupe",
    "dossier_instruction_type",
    "dossier_autorisation_type_detaille",
    "contraintes",
    "qualification",
    "evenement",
    "document_obligatoire");
//champs select
$sql_demande_nature="SELECT demande_nature.demande_nature, demande_nature.libelle FROM ".DB_PREFIXE."demande_nature ORDER BY demande_nature.libelle ASC";
$sql_demande_nature_by_id = "SELECT demande_nature.demande_nature, demande_nature.libelle FROM ".DB_PREFIXE."demande_nature WHERE demande_nature = <idx>";
$sql_dossier_autorisation_type_detaille="SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille ORDER BY dossier_autorisation_type_detaille.libelle ASC";
$sql_dossier_autorisation_type_detaille_by_id = "SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille WHERE dossier_autorisation_type_detaille = <idx>";
$sql_dossier_instruction_type="SELECT dossier_instruction_type.dossier_instruction_type, dossier_instruction_type.libelle FROM ".DB_PREFIXE."dossier_instruction_type ORDER BY dossier_instruction_type.libelle ASC";
$sql_dossier_instruction_type_by_id = "SELECT dossier_instruction_type.dossier_instruction_type, dossier_instruction_type.libelle FROM ".DB_PREFIXE."dossier_instruction_type WHERE dossier_instruction_type = <idx>";
$sql_evenement="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_evenement_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
$sql_groupe="SELECT groupe.groupe, groupe.libelle FROM ".DB_PREFIXE."groupe ORDER BY groupe.libelle ASC";
$sql_groupe_by_id = "SELECT groupe.groupe, groupe.libelle FROM ".DB_PREFIXE."groupe WHERE groupe = <idx>";
?>