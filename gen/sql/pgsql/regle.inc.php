<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("regle");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."regle";
// SELECT 
$champAffiche = array(
    'regle.regle as "'._("regle").'"',
    'regle.sens as "'._("sens").'"',
    'regle.ordre as "'._("ordre").'"',
    'regle.controle as "'._("controle").'"',
    'regle.id as "'._("id").'"',
    'regle.champ as "'._("champ").'"',
    'regle.operateur as "'._("operateur").'"',
    'regle.valeur as "'._("valeur").'"',
    'regle.message as "'._("message").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'regle.regle as "'._("regle").'"',
    'regle.sens as "'._("sens").'"',
    'regle.ordre as "'._("ordre").'"',
    'regle.controle as "'._("controle").'"',
    'regle.id as "'._("id").'"',
    'regle.champ as "'._("champ").'"',
    'regle.operateur as "'._("operateur").'"',
    'regle.valeur as "'._("valeur").'"',
    'regle.message as "'._("message").'"',
    );
$tri="ORDER BY regle.sens ASC NULLS LAST";
$edition="regle";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

?>