<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("demande");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."demande
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON demande.arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."dossier as dossier1 
        ON demande.autorisation_contestee=dossier1.dossier 
    LEFT JOIN ".DB_PREFIXE."demande_type 
        ON demande.demande_type=demande_type.demande_type 
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation 
        ON demande.dossier_autorisation=dossier_autorisation.dossier_autorisation 
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille 
        ON demande.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille 
    LEFT JOIN ".DB_PREFIXE."dossier as dossier5 
        ON demande.dossier_instruction=dossier5.dossier 
    LEFT JOIN ".DB_PREFIXE."instruction 
        ON demande.instruction_recepisse=instruction.instruction 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON demande.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'demande.demande as "'._("demande").'"',
    'dossier_autorisation_type_detaille.libelle as "'._("dossier_autorisation_type_detaille").'"',
    'demande_type.libelle as "'._("demande_type").'"',
    'dossier5.annee as "'._("dossier_instruction").'"',
    'dossier_autorisation.dossier_autorisation_type_detaille as "'._("dossier_autorisation").'"',
    'to_char(demande.date_demande ,\'DD/MM/YYYY\') as "'._("date_demande").'"',
    'demande.terrain_adresse_voie_numero as "'._("terrain_adresse_voie_numero").'"',
    'demande.terrain_adresse_voie as "'._("terrain_adresse_voie").'"',
    'demande.terrain_adresse_lieu_dit as "'._("terrain_adresse_lieu_dit").'"',
    'demande.terrain_adresse_localite as "'._("terrain_adresse_localite").'"',
    'demande.terrain_adresse_code_postal as "'._("terrain_adresse_code_postal").'"',
    'demande.terrain_adresse_bp as "'._("terrain_adresse_bp").'"',
    'demande.terrain_adresse_cedex as "'._("terrain_adresse_cedex").'"',
    'demande.terrain_superficie as "'._("terrain_superficie").'"',
    'instruction.destinataire as "'._("instruction_recepisse").'"',
    'arrondissement.libelle as "'._("arrondissement").'"',
    'dossier1.annee as "'._("autorisation_contestee").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
//
$champNonAffiche = array(
    'demande.terrain_references_cadastrales as "'._("terrain_references_cadastrales").'"',
    'demande.om_collectivite as "'._("om_collectivite").'"',
    );
//
$champRecherche = array(
    'demande.demande as "'._("demande").'"',
    'dossier_autorisation_type_detaille.libelle as "'._("dossier_autorisation_type_detaille").'"',
    'demande_type.libelle as "'._("demande_type").'"',
    'dossier5.annee as "'._("dossier_instruction").'"',
    'dossier_autorisation.dossier_autorisation_type_detaille as "'._("dossier_autorisation").'"',
    'demande.terrain_adresse_voie_numero as "'._("terrain_adresse_voie_numero").'"',
    'demande.terrain_adresse_voie as "'._("terrain_adresse_voie").'"',
    'demande.terrain_adresse_lieu_dit as "'._("terrain_adresse_lieu_dit").'"',
    'demande.terrain_adresse_localite as "'._("terrain_adresse_localite").'"',
    'demande.terrain_adresse_code_postal as "'._("terrain_adresse_code_postal").'"',
    'demande.terrain_adresse_bp as "'._("terrain_adresse_bp").'"',
    'demande.terrain_adresse_cedex as "'._("terrain_adresse_cedex").'"',
    'demande.terrain_superficie as "'._("terrain_superficie").'"',
    'instruction.destinataire as "'._("instruction_recepisse").'"',
    'arrondissement.libelle as "'._("arrondissement").'"',
    'dossier1.annee as "'._("autorisation_contestee").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
$tri="ORDER BY dossier_autorisation_type_detaille.libelle ASC NULLS LAST";
$edition="demande";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (demande.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "arrondissement" => array("arrondissement", ),
    "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
    "demande_type" => array("demande_type", ),
    "dossier_autorisation" => array("dossier_autorisation", ),
    "dossier_autorisation_type_detaille" => array("dossier_autorisation_type_detaille", ),
    "instruction" => array("instruction", ),
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - arrondissement
if (in_array($retourformulaire, $foreign_keys_extended["arrondissement"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (demande.arrondissement = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (demande.om_collectivite = '".$_SESSION["collectivite"]."') AND (demande.arrondissement = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - dossier
if (in_array($retourformulaire, $foreign_keys_extended["dossier"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (demande.autorisation_contestee = '".$f->db->escapeSimple($idxformulaire)."' OR demande.dossier_instruction = '".$f->db->escapeSimple($idxformulaire)."') ";
    } else {
        // Filtre MONO
        $selection = " WHERE (demande.om_collectivite = '".$_SESSION["collectivite"]."') AND (demande.autorisation_contestee = '".$f->db->escapeSimple($idxformulaire)."' OR demande.dossier_instruction = '".$f->db->escapeSimple($idxformulaire)."') ";
    }
}
// Filtre listing sous formulaire - demande_type
if (in_array($retourformulaire, $foreign_keys_extended["demande_type"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (demande.demande_type = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (demande.om_collectivite = '".$_SESSION["collectivite"]."') AND (demande.demande_type = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - dossier_autorisation
if (in_array($retourformulaire, $foreign_keys_extended["dossier_autorisation"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (demande.dossier_autorisation = '".$f->db->escapeSimple($idxformulaire)."') ";
    } else {
        // Filtre MONO
        $selection = " WHERE (demande.om_collectivite = '".$_SESSION["collectivite"]."') AND (demande.dossier_autorisation = '".$f->db->escapeSimple($idxformulaire)."') ";
    }
}
// Filtre listing sous formulaire - dossier_autorisation_type_detaille
if (in_array($retourformulaire, $foreign_keys_extended["dossier_autorisation_type_detaille"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (demande.dossier_autorisation_type_detaille = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (demande.om_collectivite = '".$_SESSION["collectivite"]."') AND (demande.dossier_autorisation_type_detaille = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - instruction
if (in_array($retourformulaire, $foreign_keys_extended["instruction"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (demande.instruction_recepisse = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (demande.om_collectivite = '".$_SESSION["collectivite"]."') AND (demande.instruction_recepisse = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (demande.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (demande.om_collectivite = '".$_SESSION["collectivite"]."') AND (demande.om_collectivite = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'lien_demande_demandeur',
);

?>