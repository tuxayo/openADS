<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("lot");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."lot
    LEFT JOIN ".DB_PREFIXE."dossier 
        ON lot.dossier=dossier.dossier 
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation 
        ON lot.dossier_autorisation=dossier_autorisation.dossier_autorisation ";
// SELECT 
$champAffiche = array(
    'lot.lot as "'._("lot").'"',
    'lot.libelle as "'._("libelle").'"',
    'dossier_autorisation.dossier_autorisation_type_detaille as "'._("dossier_autorisation").'"',
    'dossier.annee as "'._("dossier").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lot.lot as "'._("lot").'"',
    'lot.libelle as "'._("libelle").'"',
    'dossier_autorisation.dossier_autorisation_type_detaille as "'._("dossier_autorisation").'"',
    'dossier.annee as "'._("dossier").'"',
    );
$tri="ORDER BY lot.libelle ASC NULLS LAST";
$edition="lot";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
    "dossier_autorisation" => array("dossier_autorisation", ),
);
// Filtre listing sous formulaire - dossier
if (in_array($retourformulaire, $foreign_keys_extended["dossier"])) {
    $selection = " WHERE (lot.dossier = '".$f->db->escapeSimple($idxformulaire)."') ";
}
// Filtre listing sous formulaire - dossier_autorisation
if (in_array($retourformulaire, $foreign_keys_extended["dossier_autorisation"])) {
    $selection = " WHERE (lot.dossier_autorisation = '".$f->db->escapeSimple($idxformulaire)."') ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'donnees_techniques',
    'lien_lot_demandeur',
);

?>