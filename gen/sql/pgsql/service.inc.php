<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("service");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."service
    LEFT JOIN ".DB_PREFIXE."om_etat 
        ON service.edition=om_etat.om_etat 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON service.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'service.service as "'._("service").'"',
    'service.abrege as "'._("abrege").'"',
    'service.libelle as "'._("libelle").'"',
    'service.adresse as "'._("adresse").'"',
    'service.adresse2 as "'._("adresse2").'"',
    'service.cp as "'._("cp").'"',
    'service.ville as "'._("ville").'"',
    'service.email as "'._("email").'"',
    'service.delai as "'._("delai").'"',
    "case service.consultation_papier when 't' then 'Oui' else 'Non' end as \""._("consultation_papier")."\"",
    "case service.notification_email when 't' then 'Oui' else 'Non' end as \""._("notification_email")."\"",
    'to_char(service.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(service.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
    'service.type_consultation as "'._("type_consultation").'"',
    'om_etat.libelle as "'._("edition").'"',
    'service.delai_type as "'._("delai_type").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
//
$champNonAffiche = array(
    'service.om_collectivite as "'._("om_collectivite").'"',
    );
//
$champRecherche = array(
    'service.service as "'._("service").'"',
    'service.abrege as "'._("abrege").'"',
    'service.libelle as "'._("libelle").'"',
    'service.adresse as "'._("adresse").'"',
    'service.adresse2 as "'._("adresse2").'"',
    'service.cp as "'._("cp").'"',
    'service.ville as "'._("ville").'"',
    'service.email as "'._("email").'"',
    'service.delai as "'._("delai").'"',
    'service.type_consultation as "'._("type_consultation").'"',
    'om_etat.libelle as "'._("edition").'"',
    'service.delai_type as "'._("delai_type").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
$tri="ORDER BY service.libelle ASC NULLS LAST";
$edition="service";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = " WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
} else {
    // Filtre MONO
    $selection = " WHERE (service.om_collectivite = '".$_SESSION["collectivite"]."')  AND ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_etat" => array("om_etat", ),
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - om_etat
if (in_array($retourformulaire, $foreign_keys_extended["om_etat"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (service.edition = ".intval($idxformulaire).")  AND ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
    } else {
        // Filtre MONO
        $selection = " WHERE (service.om_collectivite = '".$_SESSION["collectivite"]."') AND (service.edition = ".intval($idxformulaire).")  AND ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
    }
$where_om_validite = " AND ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (service.om_collectivite = ".intval($idxformulaire).")  AND ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
    } else {
        // Filtre MONO
        $selection = " WHERE (service.om_collectivite = '".$_SESSION["collectivite"]."') AND (service.om_collectivite = ".intval($idxformulaire).")  AND ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
    }
$where_om_validite = " AND ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'consultation',
    'lien_service_om_utilisateur',
    'lien_service_service_categorie',
);

?>