<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("commission_type");
$tableSelect=DB_PREFIXE."commission_type";
$champs=array(
    "commission_type",
    "code",
    "libelle",
    "lieu_adresse_ligne1",
    "lieu_adresse_ligne2",
    "lieu_salle",
    "listes_de_diffusion",
    "participants",
    "corps_du_courriel",
    "om_validite_debut",
    "om_validite_fin",
    "om_collectivite");
//champs select
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
?>