<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_autorisation_parcelle");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_autorisation_parcelle
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation 
        ON dossier_autorisation_parcelle.dossier_autorisation=dossier_autorisation.dossier_autorisation ";
// SELECT 
$champAffiche = array(
    'dossier_autorisation_parcelle.dossier_autorisation_parcelle as "'._("dossier_autorisation_parcelle").'"',
    'dossier_autorisation.dossier_autorisation_type_detaille as "'._("dossier_autorisation").'"',
    'dossier_autorisation_parcelle.parcelle as "'._("parcelle").'"',
    'dossier_autorisation_parcelle.libelle as "'._("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'dossier_autorisation_parcelle.dossier_autorisation_parcelle as "'._("dossier_autorisation_parcelle").'"',
    'dossier_autorisation.dossier_autorisation_type_detaille as "'._("dossier_autorisation").'"',
    'dossier_autorisation_parcelle.parcelle as "'._("parcelle").'"',
    'dossier_autorisation_parcelle.libelle as "'._("libelle").'"',
    );
$tri="ORDER BY dossier_autorisation_parcelle.libelle ASC NULLS LAST";
$edition="dossier_autorisation_parcelle";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier_autorisation" => array("dossier_autorisation", ),
);
// Filtre listing sous formulaire - dossier_autorisation
if (in_array($retourformulaire, $foreign_keys_extended["dossier_autorisation"])) {
    $selection = " WHERE (dossier_autorisation_parcelle.dossier_autorisation = '".$f->db->escapeSimple($idxformulaire)."') ";
}

?>