<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("dossier_instruction_type");
$tableSelect=DB_PREFIXE."dossier_instruction_type";
$champs=array(
    "dossier_instruction_type",
    "code",
    "libelle",
    "description",
    "dossier_autorisation_type_detaille",
    "suffixe",
    "mouvement_sitadel");
//champs select
$sql_dossier_autorisation_type_detaille="SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille ORDER BY dossier_autorisation_type_detaille.libelle ASC";
$sql_dossier_autorisation_type_detaille_by_id = "SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille WHERE dossier_autorisation_type_detaille = <idx>";
?>