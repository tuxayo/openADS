<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("autorite_competente");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."autorite_competente";
// SELECT 
$champAffiche = array(
    'autorite_competente.autorite_competente as "'._("autorite_competente").'"',
    'autorite_competente.code as "'._("code").'"',
    'autorite_competente.libelle as "'._("libelle").'"',
    'autorite_competente.autorite_competente_sitadel as "'._("autorite_competente_sitadel").'"',
    );
//
$champNonAffiche = array(
    'autorite_competente.description as "'._("description").'"',
    );
//
$champRecherche = array(
    'autorite_competente.autorite_competente as "'._("autorite_competente").'"',
    'autorite_competente.code as "'._("code").'"',
    'autorite_competente.libelle as "'._("libelle").'"',
    'autorite_competente.autorite_competente_sitadel as "'._("autorite_competente_sitadel").'"',
    );
$tri="ORDER BY autorite_competente.libelle ASC NULLS LAST";
$edition="autorite_competente";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'dossier',
    'evenement',
    'instruction',
);

?>