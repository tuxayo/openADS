<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$ent = _("application")." -> "._("evenement");
$tableSelect=DB_PREFIXE."evenement";
$champs=array(
    "evenement",
    "libelle",
    "action",
    "etat",
    "delai",
    "accord_tacite",
    "delai_notification",
    "lettretype",
    "consultation",
    "avis_decision",
    "restriction",
    "type",
    "evenement_retour_ar",
    "evenement_suivant_tacite",
    "evenement_retour_signature",
    "autorite_competente",
    "retour",
    "non_verrouillable",
    "phase");
//champs select
$sql_action="SELECT action.action, action.libelle FROM ".DB_PREFIXE."action ORDER BY action.libelle ASC";
$sql_action_by_id = "SELECT action.action, action.libelle FROM ".DB_PREFIXE."action WHERE action = '<idx>'";
$sql_autorite_competente="SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente ORDER BY autorite_competente.libelle ASC";
$sql_autorite_competente_by_id = "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente WHERE autorite_competente = <idx>";
$sql_avis_decision="SELECT avis_decision.avis_decision, avis_decision.libelle FROM ".DB_PREFIXE."avis_decision ORDER BY avis_decision.libelle ASC";
$sql_avis_decision_by_id = "SELECT avis_decision.avis_decision, avis_decision.libelle FROM ".DB_PREFIXE."avis_decision WHERE avis_decision = '<idx>'";
$sql_etat="SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat ORDER BY etat.libelle ASC";
$sql_etat_by_id = "SELECT etat.etat, etat.libelle FROM ".DB_PREFIXE."etat WHERE etat = '<idx>'";
$sql_evenement_retour_ar="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_evenement_retour_ar_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
$sql_evenement_retour_signature="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_evenement_retour_signature_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
$sql_evenement_suivant_tacite="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement ORDER BY evenement.libelle ASC";
$sql_evenement_suivant_tacite_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";
$sql_phase="SELECT phase.phase, phase.code FROM ".DB_PREFIXE."phase WHERE ((phase.om_validite_debut IS NULL AND (phase.om_validite_fin IS NULL OR phase.om_validite_fin > CURRENT_DATE)) OR (phase.om_validite_debut <= CURRENT_DATE AND (phase.om_validite_fin IS NULL OR phase.om_validite_fin > CURRENT_DATE))) ORDER BY phase.code ASC";
$sql_phase_by_id = "SELECT phase.phase, phase.code FROM ".DB_PREFIXE."phase WHERE phase = <idx>";
?>