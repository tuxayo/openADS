<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("demandeur");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."demandeur
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON demandeur.om_collectivite=om_collectivite.om_collectivite 
    LEFT JOIN ".DB_PREFIXE."civilite as civilite1 
        ON demandeur.particulier_civilite=civilite1.civilite 
    LEFT JOIN ".DB_PREFIXE."civilite as civilite2 
        ON demandeur.personne_morale_civilite=civilite2.civilite ";
// SELECT 
$champAffiche = array(
    'demandeur.demandeur as "'._("demandeur").'"',
    'demandeur.type_demandeur as "'._("type_demandeur").'"',
    'demandeur.qualite as "'._("qualite").'"',
    'demandeur.particulier_nom as "'._("particulier_nom").'"',
    'demandeur.particulier_prenom as "'._("particulier_prenom").'"',
    'to_char(demandeur.particulier_date_naissance ,\'DD/MM/YYYY\') as "'._("particulier_date_naissance").'"',
    'demandeur.particulier_commune_naissance as "'._("particulier_commune_naissance").'"',
    'demandeur.particulier_departement_naissance as "'._("particulier_departement_naissance").'"',
    'demandeur.personne_morale_denomination as "'._("personne_morale_denomination").'"',
    'demandeur.personne_morale_raison_sociale as "'._("personne_morale_raison_sociale").'"',
    'demandeur.personne_morale_siret as "'._("personne_morale_siret").'"',
    'demandeur.personne_morale_categorie_juridique as "'._("personne_morale_categorie_juridique").'"',
    'demandeur.personne_morale_nom as "'._("personne_morale_nom").'"',
    'demandeur.personne_morale_prenom as "'._("personne_morale_prenom").'"',
    'demandeur.numero as "'._("numero").'"',
    'demandeur.voie as "'._("voie").'"',
    'demandeur.complement as "'._("complement").'"',
    'demandeur.lieu_dit as "'._("lieu_dit").'"',
    'demandeur.localite as "'._("localite").'"',
    'demandeur.code_postal as "'._("code_postal").'"',
    'demandeur.bp as "'._("bp").'"',
    'demandeur.cedex as "'._("cedex").'"',
    'demandeur.pays as "'._("pays").'"',
    'demandeur.division_territoriale as "'._("division_territoriale").'"',
    'demandeur.telephone_fixe as "'._("telephone_fixe").'"',
    'demandeur.telephone_mobile as "'._("telephone_mobile").'"',
    'demandeur.indicatif as "'._("indicatif").'"',
    'demandeur.courriel as "'._("courriel").'"',
    "case demandeur.notification when 't' then 'Oui' else 'Non' end as \""._("notification")."\"",
    "case demandeur.frequent when 't' then 'Oui' else 'Non' end as \""._("frequent")."\"",
    'civilite1.libelle as "'._("particulier_civilite").'"',
    'civilite2.libelle as "'._("personne_morale_civilite").'"',
    'demandeur.fax as "'._("fax").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
//
$champNonAffiche = array(
    'demandeur.om_collectivite as "'._("om_collectivite").'"',
    );
//
$champRecherche = array(
    'demandeur.demandeur as "'._("demandeur").'"',
    'demandeur.type_demandeur as "'._("type_demandeur").'"',
    'demandeur.qualite as "'._("qualite").'"',
    'demandeur.particulier_nom as "'._("particulier_nom").'"',
    'demandeur.particulier_prenom as "'._("particulier_prenom").'"',
    'demandeur.particulier_commune_naissance as "'._("particulier_commune_naissance").'"',
    'demandeur.particulier_departement_naissance as "'._("particulier_departement_naissance").'"',
    'demandeur.personne_morale_denomination as "'._("personne_morale_denomination").'"',
    'demandeur.personne_morale_raison_sociale as "'._("personne_morale_raison_sociale").'"',
    'demandeur.personne_morale_siret as "'._("personne_morale_siret").'"',
    'demandeur.personne_morale_categorie_juridique as "'._("personne_morale_categorie_juridique").'"',
    'demandeur.personne_morale_nom as "'._("personne_morale_nom").'"',
    'demandeur.personne_morale_prenom as "'._("personne_morale_prenom").'"',
    'demandeur.numero as "'._("numero").'"',
    'demandeur.voie as "'._("voie").'"',
    'demandeur.complement as "'._("complement").'"',
    'demandeur.lieu_dit as "'._("lieu_dit").'"',
    'demandeur.localite as "'._("localite").'"',
    'demandeur.code_postal as "'._("code_postal").'"',
    'demandeur.bp as "'._("bp").'"',
    'demandeur.cedex as "'._("cedex").'"',
    'demandeur.pays as "'._("pays").'"',
    'demandeur.division_territoriale as "'._("division_territoriale").'"',
    'demandeur.telephone_fixe as "'._("telephone_fixe").'"',
    'demandeur.telephone_mobile as "'._("telephone_mobile").'"',
    'demandeur.indicatif as "'._("indicatif").'"',
    'demandeur.courriel as "'._("courriel").'"',
    'civilite1.libelle as "'._("particulier_civilite").'"',
    'civilite2.libelle as "'._("personne_morale_civilite").'"',
    'demandeur.fax as "'._("fax").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
$tri="ORDER BY demandeur.type_demandeur ASC NULLS LAST";
$edition="demandeur";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (demandeur.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_collectivite" => array("om_collectivite", ),
    "civilite" => array("civilite", ),
);
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (demandeur.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (demandeur.om_collectivite = '".$_SESSION["collectivite"]."') AND (demandeur.om_collectivite = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - civilite
if (in_array($retourformulaire, $foreign_keys_extended["civilite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (demandeur.particulier_civilite = ".intval($idxformulaire)." OR demandeur.personne_morale_civilite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (demandeur.om_collectivite = '".$_SESSION["collectivite"]."') AND (demandeur.particulier_civilite = ".intval($idxformulaire)." OR demandeur.personne_morale_civilite = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'lien_demande_demandeur',
    'lien_dossier_autorisation_demandeur',
    'lien_dossier_demandeur',
    'lien_lot_demandeur',
);

?>