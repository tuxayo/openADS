<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

$DEBUG=0;
$serie=15;
$ent = _("administration")." -> "._("om_collectivite");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."om_collectivite";
// SELECT 
$champAffiche = array(
    'om_collectivite.om_collectivite as "'._("om_collectivite").'"',
    'om_collectivite.libelle as "'._("libelle").'"',
    'om_collectivite.niveau as "'._("niveau").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'om_collectivite.om_collectivite as "'._("om_collectivite").'"',
    'om_collectivite.libelle as "'._("libelle").'"',
    'om_collectivite.niveau as "'._("niveau").'"',
    );
$tri="ORDER BY om_collectivite.libelle ASC NULLS LAST";
$edition="om_collectivite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (om_collectivite.om_collectivite = '".$_SESSION["collectivite"]."') ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'affectation_automatique',
    'bible',
    'commission',
    'commission_type',
    'contrainte',
    'demande',
    'demandeur',
    'direction',
    'dossier',
    'dossier_autorisation',
    'om_etat',
    'om_lettretype',
    'om_logo',
    'om_parametre',
    'om_sig_flux',
    'om_sig_map',
    'om_sousetat',
    'om_utilisateur',
    'service',
    'signataire_arrete',
    'taxe_amenagement',
);

?>