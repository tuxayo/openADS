<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class lien_service_service_categorie_gen extends om_dbform {

    var $table = "lien_service_service_categorie";
    var $clePrimaire = "lien_service_service_categorie";
    var $typeCle = "N";
    var $required_field = array(
        "lien_service_service_categorie"
    );
    
    var $foreign_keys_extended = array(
        "service" => array("service", ),
        "service_categorie" => array("service_categorie", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_service_service_categorie'])) {
            $this->valF['lien_service_service_categorie'] = ""; // -> requis
        } else {
            $this->valF['lien_service_service_categorie'] = $val['lien_service_service_categorie'];
        }
        if (!is_numeric($val['service_categorie'])) {
            $this->valF['service_categorie'] = NULL;
        } else {
            $this->valF['service_categorie'] = $val['service_categorie'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = NULL;
        } else {
            $this->valF['service'] = $val['service'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_service_service_categorie", "hidden");
            if ($this->is_in_context_of_foreign_key("service_categorie", $this->retourformulaire)) {
                $form->setType("service_categorie", "selecthiddenstatic");
            } else {
                $form->setType("service_categorie", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_service_service_categorie", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("service_categorie", $this->retourformulaire)) {
                $form->setType("service_categorie", "selecthiddenstatic");
            } else {
                $form->setType("service_categorie", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_service_service_categorie", "hiddenstatic");
            $form->setType("service_categorie", "selectstatic");
            $form->setType("service", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_service_service_categorie", "static");
            $form->setType("service_categorie", "selectstatic");
            $form->setType("service", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_service_service_categorie','VerifNum(this)');
        $form->setOnchange('service_categorie','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_service_service_categorie", 11);
        $form->setTaille("service_categorie", 11);
        $form->setTaille("service", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_service_service_categorie", 11);
        $form->setMax("service_categorie", 11);
        $form->setMax("service", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_service_service_categorie',_('lien_service_service_categorie'));
        $form->setLib('service_categorie',_('service_categorie'));
        $form->setLib('service',_('service'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
        // service_categorie
        $this->init_select($form, $this->f->db, $maj, null, "service_categorie", $sql_service_categorie, $sql_service_categorie_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service_categorie', $this->retourformulaire))
                $form->setVal('service_categorie', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
