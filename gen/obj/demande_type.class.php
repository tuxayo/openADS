<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class demande_type_gen extends om_dbform {

    var $table = "demande_type";
    var $clePrimaire = "demande_type";
    var $typeCle = "N";
    var $required_field = array(
        "code",
        "demande_nature",
        "demande_type",
        "evenement",
        "groupe",
        "libelle"
    );
    
    var $foreign_keys_extended = array(
        "demande_nature" => array("demande_nature", ),
        "dossier_autorisation_type_detaille" => array("dossier_autorisation_type_detaille", ),
        "dossier_instruction_type" => array("dossier_instruction_type", ),
        "evenement" => array("evenement", ),
        "groupe" => array("groupe", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['demande_type'])) {
            $this->valF['demande_type'] = ""; // -> requis
        } else {
            $this->valF['demande_type'] = $val['demande_type'];
        }
        $this->valF['code'] = $val['code'];
        $this->valF['libelle'] = $val['libelle'];
            $this->valF['description'] = $val['description'];
        if (!is_numeric($val['demande_nature'])) {
            $this->valF['demande_nature'] = ""; // -> requis
        } else {
            $this->valF['demande_nature'] = $val['demande_nature'];
        }
        if (!is_numeric($val['groupe'])) {
            $this->valF['groupe'] = ""; // -> requis
        } else {
            $this->valF['groupe'] = $val['groupe'];
        }
        if (!is_numeric($val['dossier_instruction_type'])) {
            $this->valF['dossier_instruction_type'] = NULL;
        } else {
            $this->valF['dossier_instruction_type'] = $val['dossier_instruction_type'];
        }
        if (!is_numeric($val['dossier_autorisation_type_detaille'])) {
            $this->valF['dossier_autorisation_type_detaille'] = NULL;
        } else {
            $this->valF['dossier_autorisation_type_detaille'] = $val['dossier_autorisation_type_detaille'];
        }
        if ($val['contraintes'] == "") {
            $this->valF['contraintes'] = NULL;
        } else {
            $this->valF['contraintes'] = $val['contraintes'];
        }
        if ($val['qualification'] == 1 || $val['qualification'] == "t" || $val['qualification'] == "Oui") {
            $this->valF['qualification'] = true;
        } else {
            $this->valF['qualification'] = false;
        }
        if (!is_numeric($val['evenement'])) {
            $this->valF['evenement'] = ""; // -> requis
        } else {
            $this->valF['evenement'] = $val['evenement'];
        }
            $this->valF['document_obligatoire'] = $val['document_obligatoire'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("demande_type", "hidden");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->is_in_context_of_foreign_key("demande_nature", $this->retourformulaire)) {
                $form->setType("demande_nature", "selecthiddenstatic");
            } else {
                $form->setType("demande_nature", "select");
            }
            if ($this->is_in_context_of_foreign_key("groupe", $this->retourformulaire)) {
                $form->setType("groupe", "selecthiddenstatic");
            } else {
                $form->setType("groupe", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_type", $this->retourformulaire)) {
                $form->setType("dossier_instruction_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_autorisation_type_detaille", $this->retourformulaire)) {
                $form->setType("dossier_autorisation_type_detaille", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation_type_detaille", "select");
            }
            $form->setType("contraintes", "text");
            $form->setType("qualification", "checkbox");
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement", "selecthiddenstatic");
            } else {
                $form->setType("evenement", "select");
            }
            $form->setType("document_obligatoire", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("demande_type", "hiddenstatic");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->is_in_context_of_foreign_key("demande_nature", $this->retourformulaire)) {
                $form->setType("demande_nature", "selecthiddenstatic");
            } else {
                $form->setType("demande_nature", "select");
            }
            if ($this->is_in_context_of_foreign_key("groupe", $this->retourformulaire)) {
                $form->setType("groupe", "selecthiddenstatic");
            } else {
                $form->setType("groupe", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_type", $this->retourformulaire)) {
                $form->setType("dossier_instruction_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_autorisation_type_detaille", $this->retourformulaire)) {
                $form->setType("dossier_autorisation_type_detaille", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation_type_detaille", "select");
            }
            $form->setType("contraintes", "text");
            $form->setType("qualification", "checkbox");
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement", "selecthiddenstatic");
            } else {
                $form->setType("evenement", "select");
            }
            $form->setType("document_obligatoire", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("demande_type", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("demande_nature", "selectstatic");
            $form->setType("groupe", "selectstatic");
            $form->setType("dossier_instruction_type", "selectstatic");
            $form->setType("dossier_autorisation_type_detaille", "selectstatic");
            $form->setType("contraintes", "hiddenstatic");
            $form->setType("qualification", "hiddenstatic");
            $form->setType("evenement", "selectstatic");
            $form->setType("document_obligatoire", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("demande_type", "static");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("description", "textareastatic");
            $form->setType("demande_nature", "selectstatic");
            $form->setType("groupe", "selectstatic");
            $form->setType("dossier_instruction_type", "selectstatic");
            $form->setType("dossier_autorisation_type_detaille", "selectstatic");
            $form->setType("contraintes", "static");
            $form->setType("qualification", "checkboxstatic");
            $form->setType("evenement", "selectstatic");
            $form->setType("document_obligatoire", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('demande_type','VerifNum(this)');
        $form->setOnchange('demande_nature','VerifNum(this)');
        $form->setOnchange('groupe','VerifNum(this)');
        $form->setOnchange('dossier_instruction_type','VerifNum(this)');
        $form->setOnchange('dossier_autorisation_type_detaille','VerifNum(this)');
        $form->setOnchange('evenement','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("demande_type", 11);
        $form->setTaille("code", 20);
        $form->setTaille("libelle", 30);
        $form->setTaille("description", 80);
        $form->setTaille("demande_nature", 11);
        $form->setTaille("groupe", 11);
        $form->setTaille("dossier_instruction_type", 11);
        $form->setTaille("dossier_autorisation_type_detaille", 11);
        $form->setTaille("contraintes", 20);
        $form->setTaille("qualification", 1);
        $form->setTaille("evenement", 11);
        $form->setTaille("document_obligatoire", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("demande_type", 11);
        $form->setMax("code", 20);
        $form->setMax("libelle", 100);
        $form->setMax("description", 6);
        $form->setMax("demande_nature", 11);
        $form->setMax("groupe", 11);
        $form->setMax("dossier_instruction_type", 11);
        $form->setMax("dossier_autorisation_type_detaille", 11);
        $form->setMax("contraintes", 20);
        $form->setMax("qualification", 1);
        $form->setMax("evenement", 11);
        $form->setMax("document_obligatoire", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('demande_type',_('demande_type'));
        $form->setLib('code',_('code'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('description',_('description'));
        $form->setLib('demande_nature',_('demande_nature'));
        $form->setLib('groupe',_('groupe'));
        $form->setLib('dossier_instruction_type',_('dossier_instruction_type'));
        $form->setLib('dossier_autorisation_type_detaille',_('dossier_autorisation_type_detaille'));
        $form->setLib('contraintes',_('contraintes'));
        $form->setLib('qualification',_('qualification'));
        $form->setLib('evenement',_('evenement'));
        $form->setLib('document_obligatoire',_('document_obligatoire'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // demande_nature
        $this->init_select($form, $this->f->db, $maj, null, "demande_nature", $sql_demande_nature, $sql_demande_nature_by_id, false);
        // dossier_autorisation_type_detaille
        $this->init_select($form, $this->f->db, $maj, null, "dossier_autorisation_type_detaille", $sql_dossier_autorisation_type_detaille, $sql_dossier_autorisation_type_detaille_by_id, false);
        // dossier_instruction_type
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction_type", $sql_dossier_instruction_type, $sql_dossier_instruction_type_by_id, false);
        // evenement
        $this->init_select($form, $this->f->db, $maj, null, "evenement", $sql_evenement, $sql_evenement_by_id, false);
        // groupe
        $this->init_select($form, $this->f->db, $maj, null, "groupe", $sql_groupe, $sql_groupe_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('demande_nature', $this->retourformulaire))
                $form->setVal('demande_nature', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_autorisation_type_detaille', $this->retourformulaire))
                $form->setVal('dossier_autorisation_type_detaille', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction_type', $this->retourformulaire))
                $form->setVal('dossier_instruction_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('evenement', $this->retourformulaire))
                $form->setVal('evenement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('groupe', $this->retourformulaire))
                $form->setVal('groupe', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : demande
        $this->rechercheTable($this->f->db, "demande", "demande_type", $id);
        // Verification de la cle secondaire : lien_demande_type_etat
        $this->rechercheTable($this->f->db, "lien_demande_type_etat", "demande_type", $id);
    }


}

?>
