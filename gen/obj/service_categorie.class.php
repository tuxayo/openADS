<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class service_categorie_gen extends om_dbform {

    var $table = "service_categorie";
    var $clePrimaire = "service_categorie";
    var $typeCle = "N";
    var $required_field = array(
        "service_categorie"
    );
    
    var $foreign_keys_extended = array(
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['service_categorie'])) {
            $this->valF['service_categorie'] = ""; // -> requis
        } else {
            $this->valF['service_categorie'] = $val['service_categorie'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = ""; // -> default
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("service_categorie", "hidden");
            $form->setType("libelle", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("service_categorie", "hiddenstatic");
            $form->setType("libelle", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("service_categorie", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("service_categorie", "static");
            $form->setType("libelle", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('service_categorie','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("service_categorie", 11);
        $form->setTaille("libelle", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("service_categorie", 11);
        $form->setMax("libelle", 70);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('service_categorie',_('service_categorie'));
        $form->setLib('libelle',_('libelle'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : lien_service_service_categorie
        $this->rechercheTable($this->f->db, "lien_service_service_categorie", "service_categorie", $id);
    }


}

?>
