<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class dossier_autorisation_type_detaille_gen extends om_dbform {

    var $table = "dossier_autorisation_type_detaille";
    var $clePrimaire = "dossier_autorisation_type_detaille";
    var $typeCle = "N";
    var $required_field = array(
        "dossier_autorisation_type",
        "dossier_autorisation_type_detaille"
    );
    
    var $foreign_keys_extended = array(
        "cerfa" => array("cerfa", ),
        "dossier_autorisation_type" => array("dossier_autorisation_type", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier_autorisation_type_detaille'])) {
            $this->valF['dossier_autorisation_type_detaille'] = ""; // -> requis
        } else {
            $this->valF['dossier_autorisation_type_detaille'] = $val['dossier_autorisation_type_detaille'];
        }
        if ($val['code'] == "") {
            $this->valF['code'] = NULL;
        } else {
            $this->valF['code'] = $val['code'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
            $this->valF['description'] = $val['description'];
        if (!is_numeric($val['dossier_autorisation_type'])) {
            $this->valF['dossier_autorisation_type'] = ""; // -> requis
        } else {
            $this->valF['dossier_autorisation_type'] = $val['dossier_autorisation_type'];
        }
        if (!is_numeric($val['cerfa'])) {
            $this->valF['cerfa'] = NULL;
        } else {
            $this->valF['cerfa'] = $val['cerfa'];
        }
        if (!is_numeric($val['cerfa_lot'])) {
            $this->valF['cerfa_lot'] = NULL;
        } else {
            $this->valF['cerfa_lot'] = $val['cerfa_lot'];
        }
        if (!is_numeric($val['duree_validite_parametrage'])) {
            $this->valF['duree_validite_parametrage'] = 0; // -> default
        } else {
            $this->valF['duree_validite_parametrage'] = $val['duree_validite_parametrage'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_autorisation_type_detaille", "hidden");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->is_in_context_of_foreign_key("dossier_autorisation_type", $this->retourformulaire)) {
                $form->setType("dossier_autorisation_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("cerfa", $this->retourformulaire)) {
                $form->setType("cerfa", "selecthiddenstatic");
            } else {
                $form->setType("cerfa", "select");
            }
            if ($this->is_in_context_of_foreign_key("cerfa", $this->retourformulaire)) {
                $form->setType("cerfa_lot", "selecthiddenstatic");
            } else {
                $form->setType("cerfa_lot", "select");
            }
            $form->setType("duree_validite_parametrage", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_autorisation_type_detaille", "hiddenstatic");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->is_in_context_of_foreign_key("dossier_autorisation_type", $this->retourformulaire)) {
                $form->setType("dossier_autorisation_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("cerfa", $this->retourformulaire)) {
                $form->setType("cerfa", "selecthiddenstatic");
            } else {
                $form->setType("cerfa", "select");
            }
            if ($this->is_in_context_of_foreign_key("cerfa", $this->retourformulaire)) {
                $form->setType("cerfa_lot", "selecthiddenstatic");
            } else {
                $form->setType("cerfa_lot", "select");
            }
            $form->setType("duree_validite_parametrage", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_autorisation_type_detaille", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("dossier_autorisation_type", "selectstatic");
            $form->setType("cerfa", "selectstatic");
            $form->setType("cerfa_lot", "selectstatic");
            $form->setType("duree_validite_parametrage", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_autorisation_type_detaille", "static");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("description", "textareastatic");
            $form->setType("dossier_autorisation_type", "selectstatic");
            $form->setType("cerfa", "selectstatic");
            $form->setType("cerfa_lot", "selectstatic");
            $form->setType("duree_validite_parametrage", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_autorisation_type_detaille','VerifNum(this)');
        $form->setOnchange('dossier_autorisation_type','VerifNum(this)');
        $form->setOnchange('cerfa','VerifNum(this)');
        $form->setOnchange('cerfa_lot','VerifNum(this)');
        $form->setOnchange('duree_validite_parametrage','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_autorisation_type_detaille", 11);
        $form->setTaille("code", 20);
        $form->setTaille("libelle", 30);
        $form->setTaille("description", 80);
        $form->setTaille("dossier_autorisation_type", 11);
        $form->setTaille("cerfa", 11);
        $form->setTaille("cerfa_lot", 11);
        $form->setTaille("duree_validite_parametrage", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_autorisation_type_detaille", 11);
        $form->setMax("code", 20);
        $form->setMax("libelle", 100);
        $form->setMax("description", 6);
        $form->setMax("dossier_autorisation_type", 11);
        $form->setMax("cerfa", 11);
        $form->setMax("cerfa_lot", 11);
        $form->setMax("duree_validite_parametrage", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_autorisation_type_detaille',_('dossier_autorisation_type_detaille'));
        $form->setLib('code',_('code'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('description',_('description'));
        $form->setLib('dossier_autorisation_type',_('dossier_autorisation_type'));
        $form->setLib('cerfa',_('cerfa'));
        $form->setLib('cerfa_lot',_('cerfa_lot'));
        $form->setLib('duree_validite_parametrage',_('duree_validite_parametrage'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // cerfa
        $this->init_select($form, $this->f->db, $maj, null, "cerfa", $sql_cerfa, $sql_cerfa_by_id, true);
        // cerfa_lot
        $this->init_select($form, $this->f->db, $maj, null, "cerfa_lot", $sql_cerfa_lot, $sql_cerfa_lot_by_id, true);
        // dossier_autorisation_type
        $this->init_select($form, $this->f->db, $maj, null, "dossier_autorisation_type", $sql_dossier_autorisation_type, $sql_dossier_autorisation_type_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_autorisation_type', $this->retourformulaire))
                $form->setVal('dossier_autorisation_type', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('cerfa', $this->retourformulaire))
                $form->setVal('cerfa', $idxformulaire);
            if($this->is_in_context_of_foreign_key('cerfa', $this->retourformulaire))
                $form->setVal('cerfa_lot', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : affectation_automatique
        $this->rechercheTable($this->f->db, "affectation_automatique", "dossier_autorisation_type_detaille", $id);
        // Verification de la cle secondaire : demande
        $this->rechercheTable($this->f->db, "demande", "dossier_autorisation_type_detaille", $id);
        // Verification de la cle secondaire : demande_type
        $this->rechercheTable($this->f->db, "demande_type", "dossier_autorisation_type_detaille", $id);
        // Verification de la cle secondaire : dossier_autorisation
        $this->rechercheTable($this->f->db, "dossier_autorisation", "dossier_autorisation_type_detaille", $id);
        // Verification de la cle secondaire : dossier_instruction_type
        $this->rechercheTable($this->f->db, "dossier_instruction_type", "dossier_autorisation_type_detaille", $id);
    }


}

?>
