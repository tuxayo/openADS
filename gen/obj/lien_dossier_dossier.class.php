<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

require_once "../obj/om_dbform.class.php";

class lien_dossier_dossier_gen extends om_dbform {

    var $table = "lien_dossier_dossier";
    var $clePrimaire = "lien_dossier_dossier";
    var $typeCle = "N";
    var $required_field = array(
        "dossier_cible",
        "dossier_src",
        "lien_dossier_dossier",
        "type_lien"
    );
    var $unique_key = array(
      array("dossier_cible","dossier_src"),
    );
    var $foreign_keys_extended = array(
        "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_dossier_dossier'])) {
            $this->valF['lien_dossier_dossier'] = ""; // -> requis
        } else {
            $this->valF['lien_dossier_dossier'] = $val['lien_dossier_dossier'];
        }
        $this->valF['dossier_src'] = $val['dossier_src'];
        $this->valF['dossier_cible'] = $val['dossier_cible'];
            $this->valF['type_lien'] = $val['type_lien'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_dossier_dossier", "hidden");
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("dossier_src", "selecthiddenstatic");
            } else {
                $form->setType("dossier_src", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("dossier_cible", "selecthiddenstatic");
            } else {
                $form->setType("dossier_cible", "select");
            }
            $form->setType("type_lien", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_dossier_dossier", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("dossier_src", "selecthiddenstatic");
            } else {
                $form->setType("dossier_src", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("dossier_cible", "selecthiddenstatic");
            } else {
                $form->setType("dossier_cible", "select");
            }
            $form->setType("type_lien", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_dossier_dossier", "hiddenstatic");
            $form->setType("dossier_src", "selectstatic");
            $form->setType("dossier_cible", "selectstatic");
            $form->setType("type_lien", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_dossier_dossier", "static");
            $form->setType("dossier_src", "selectstatic");
            $form->setType("dossier_cible", "selectstatic");
            $form->setType("type_lien", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_dossier_dossier','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_dossier_dossier", 11);
        $form->setTaille("dossier_src", 30);
        $form->setTaille("dossier_cible", 30);
        $form->setTaille("type_lien", 4);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_dossier_dossier", 11);
        $form->setMax("dossier_src", 30);
        $form->setMax("dossier_cible", 30);
        $form->setMax("type_lien", 4);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_dossier_dossier',_('lien_dossier_dossier'));
        $form->setLib('dossier_src',_('dossier_src'));
        $form->setLib('dossier_cible',_('dossier_cible'));
        $form->setLib('type_lien',_('type_lien'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // dossier_cible
        $this->init_select($form, $this->f->db, $maj, null, "dossier_cible", $sql_dossier_cible, $sql_dossier_cible_by_id, false);
        // dossier_src
        $this->init_select($form, $this->f->db, $maj, null, "dossier_src", $sql_dossier_src, $sql_dossier_src_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('dossier', $this->retourformulaire))
                $form->setVal('dossier_cible', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier', $this->retourformulaire))
                $form->setVal('dossier_src', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
