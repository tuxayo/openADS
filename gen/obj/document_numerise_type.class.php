<?php
//$Id$ 
//gen openMairie le 18/11/2016 15:45

require_once "../obj/om_dbform.class.php";

class document_numerise_type_gen extends om_dbform {

    var $table = "document_numerise_type";
    var $clePrimaire = "document_numerise_type";
    var $typeCle = "N";
    var $required_field = array(
        "code",
        "document_numerise_type",
        "document_numerise_type_categorie",
        "libelle"
    );
    var $unique_key = array(
      "code",
    );
    var $foreign_keys_extended = array(
        "document_numerise_type_categorie" => array("document_numerise_type_categorie", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['document_numerise_type'])) {
            $this->valF['document_numerise_type'] = ""; // -> requis
        } else {
            $this->valF['document_numerise_type'] = $val['document_numerise_type'];
        }
        $this->valF['code'] = $val['code'];
        $this->valF['libelle'] = $val['libelle'];
        if (!is_numeric($val['document_numerise_type_categorie'])) {
            $this->valF['document_numerise_type_categorie'] = ""; // -> requis
        } else {
            $this->valF['document_numerise_type_categorie'] = $val['document_numerise_type_categorie'];
        }
        if ($val['aff_service_consulte'] == 1 || $val['aff_service_consulte'] == "t" || $val['aff_service_consulte'] == "Oui") {
            $this->valF['aff_service_consulte'] = true;
        } else {
            $this->valF['aff_service_consulte'] = false;
        }
        if ($val['aff_da'] == 1 || $val['aff_da'] == "t" || $val['aff_da'] == "Oui") {
            $this->valF['aff_da'] = true;
        } else {
            $this->valF['aff_da'] = false;
        }
        if ($val['synchro_metadonnee'] == 1 || $val['synchro_metadonnee'] == "t" || $val['synchro_metadonnee'] == "Oui") {
            $this->valF['synchro_metadonnee'] = true;
        } else {
            $this->valF['synchro_metadonnee'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("document_numerise_type", "hidden");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("document_numerise_type_categorie", $this->retourformulaire)) {
                $form->setType("document_numerise_type_categorie", "selecthiddenstatic");
            } else {
                $form->setType("document_numerise_type_categorie", "select");
            }
            $form->setType("aff_service_consulte", "checkbox");
            $form->setType("aff_da", "checkbox");
            $form->setType("synchro_metadonnee", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("document_numerise_type", "hiddenstatic");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("document_numerise_type_categorie", $this->retourformulaire)) {
                $form->setType("document_numerise_type_categorie", "selecthiddenstatic");
            } else {
                $form->setType("document_numerise_type_categorie", "select");
            }
            $form->setType("aff_service_consulte", "checkbox");
            $form->setType("aff_da", "checkbox");
            $form->setType("synchro_metadonnee", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("document_numerise_type", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("document_numerise_type_categorie", "selectstatic");
            $form->setType("aff_service_consulte", "hiddenstatic");
            $form->setType("aff_da", "hiddenstatic");
            $form->setType("synchro_metadonnee", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("document_numerise_type", "static");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("document_numerise_type_categorie", "selectstatic");
            $form->setType("aff_service_consulte", "checkboxstatic");
            $form->setType("aff_da", "checkboxstatic");
            $form->setType("synchro_metadonnee", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('document_numerise_type','VerifNum(this)');
        $form->setOnchange('document_numerise_type_categorie','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("document_numerise_type", 11);
        $form->setTaille("code", 10);
        $form->setTaille("libelle", 30);
        $form->setTaille("document_numerise_type_categorie", 11);
        $form->setTaille("aff_service_consulte", 1);
        $form->setTaille("aff_da", 1);
        $form->setTaille("synchro_metadonnee", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("document_numerise_type", 11);
        $form->setMax("code", 6);
        $form->setMax("libelle", 255);
        $form->setMax("document_numerise_type_categorie", 11);
        $form->setMax("aff_service_consulte", 1);
        $form->setMax("aff_da", 1);
        $form->setMax("synchro_metadonnee", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('document_numerise_type',_('document_numerise_type'));
        $form->setLib('code',_('code'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('document_numerise_type_categorie',_('document_numerise_type_categorie'));
        $form->setLib('aff_service_consulte',_('aff_service_consulte'));
        $form->setLib('aff_da',_('aff_da'));
        $form->setLib('synchro_metadonnee',_('synchro_metadonnee'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // document_numerise_type_categorie
        $this->init_select($form, $this->f->db, $maj, null, "document_numerise_type_categorie", $sql_document_numerise_type_categorie, $sql_document_numerise_type_categorie_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('document_numerise_type_categorie', $this->retourformulaire))
                $form->setVal('document_numerise_type_categorie', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : document_numerise
        $this->rechercheTable($this->f->db, "document_numerise", "document_numerise_type", $id);
        // Verification de la cle secondaire : lien_document_numerise_type_instructeur_qualite
        $this->rechercheTable($this->f->db, "lien_document_numerise_type_instructeur_qualite", "document_numerise_type", $id);
    }


}

?>
