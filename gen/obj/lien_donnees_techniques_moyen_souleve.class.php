<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

require_once "../obj/om_dbform.class.php";

class lien_donnees_techniques_moyen_souleve_gen extends om_dbform {

    var $table = "lien_donnees_techniques_moyen_souleve";
    var $clePrimaire = "lien_donnees_techniques_moyen_souleve";
    var $typeCle = "N";
    var $required_field = array(
        "donnees_techniques",
        "lien_donnees_techniques_moyen_souleve",
        "moyen_souleve"
    );
    
    var $foreign_keys_extended = array(
        "donnees_techniques" => array("donnees_techniques", ),
        "moyen_souleve" => array("moyen_souleve", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_donnees_techniques_moyen_souleve'])) {
            $this->valF['lien_donnees_techniques_moyen_souleve'] = ""; // -> requis
        } else {
            $this->valF['lien_donnees_techniques_moyen_souleve'] = $val['lien_donnees_techniques_moyen_souleve'];
        }
        if (!is_numeric($val['donnees_techniques'])) {
            $this->valF['donnees_techniques'] = ""; // -> requis
        } else {
            $this->valF['donnees_techniques'] = $val['donnees_techniques'];
        }
        if (!is_numeric($val['moyen_souleve'])) {
            $this->valF['moyen_souleve'] = ""; // -> requis
        } else {
            $this->valF['moyen_souleve'] = $val['moyen_souleve'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_donnees_techniques_moyen_souleve", "hidden");
            if ($this->is_in_context_of_foreign_key("donnees_techniques", $this->retourformulaire)) {
                $form->setType("donnees_techniques", "selecthiddenstatic");
            } else {
                $form->setType("donnees_techniques", "select");
            }
            if ($this->is_in_context_of_foreign_key("moyen_souleve", $this->retourformulaire)) {
                $form->setType("moyen_souleve", "selecthiddenstatic");
            } else {
                $form->setType("moyen_souleve", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_donnees_techniques_moyen_souleve", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("donnees_techniques", $this->retourformulaire)) {
                $form->setType("donnees_techniques", "selecthiddenstatic");
            } else {
                $form->setType("donnees_techniques", "select");
            }
            if ($this->is_in_context_of_foreign_key("moyen_souleve", $this->retourformulaire)) {
                $form->setType("moyen_souleve", "selecthiddenstatic");
            } else {
                $form->setType("moyen_souleve", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_donnees_techniques_moyen_souleve", "hiddenstatic");
            $form->setType("donnees_techniques", "selectstatic");
            $form->setType("moyen_souleve", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_donnees_techniques_moyen_souleve", "static");
            $form->setType("donnees_techniques", "selectstatic");
            $form->setType("moyen_souleve", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_donnees_techniques_moyen_souleve','VerifNum(this)');
        $form->setOnchange('donnees_techniques','VerifNum(this)');
        $form->setOnchange('moyen_souleve','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_donnees_techniques_moyen_souleve", 11);
        $form->setTaille("donnees_techniques", 11);
        $form->setTaille("moyen_souleve", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_donnees_techniques_moyen_souleve", 11);
        $form->setMax("donnees_techniques", 11);
        $form->setMax("moyen_souleve", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_donnees_techniques_moyen_souleve',_('lien_donnees_techniques_moyen_souleve'));
        $form->setLib('donnees_techniques',_('donnees_techniques'));
        $form->setLib('moyen_souleve',_('moyen_souleve'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // donnees_techniques
        $this->init_select($form, $this->f->db, $maj, null, "donnees_techniques", $sql_donnees_techniques, $sql_donnees_techniques_by_id, false);
        // moyen_souleve
        $this->init_select($form, $this->f->db, $maj, null, "moyen_souleve", $sql_moyen_souleve, $sql_moyen_souleve_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('donnees_techniques', $this->retourformulaire))
                $form->setVal('donnees_techniques', $idxformulaire);
            if($this->is_in_context_of_foreign_key('moyen_souleve', $this->retourformulaire))
                $form->setVal('moyen_souleve', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
