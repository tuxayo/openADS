<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class lien_service_om_utilisateur_gen extends om_dbform {

    var $table = "lien_service_om_utilisateur";
    var $clePrimaire = "lien_service_om_utilisateur";
    var $typeCle = "N";
    var $required_field = array(
        "lien_service_om_utilisateur"
    );
    
    var $foreign_keys_extended = array(
        "om_utilisateur" => array("om_utilisateur", ),
        "service" => array("service", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_service_om_utilisateur'])) {
            $this->valF['lien_service_om_utilisateur'] = ""; // -> requis
        } else {
            $this->valF['lien_service_om_utilisateur'] = $val['lien_service_om_utilisateur'];
        }
        if (!is_numeric($val['om_utilisateur'])) {
            $this->valF['om_utilisateur'] = NULL;
        } else {
            $this->valF['om_utilisateur'] = $val['om_utilisateur'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = NULL;
        } else {
            $this->valF['service'] = $val['service'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_service_om_utilisateur", "hidden");
            if ($this->is_in_context_of_foreign_key("om_utilisateur", $this->retourformulaire)) {
                $form->setType("om_utilisateur", "selecthiddenstatic");
            } else {
                $form->setType("om_utilisateur", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_service_om_utilisateur", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("om_utilisateur", $this->retourformulaire)) {
                $form->setType("om_utilisateur", "selecthiddenstatic");
            } else {
                $form->setType("om_utilisateur", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_service_om_utilisateur", "hiddenstatic");
            $form->setType("om_utilisateur", "selectstatic");
            $form->setType("service", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_service_om_utilisateur", "static");
            $form->setType("om_utilisateur", "selectstatic");
            $form->setType("service", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_service_om_utilisateur','VerifNum(this)');
        $form->setOnchange('om_utilisateur','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_service_om_utilisateur", 11);
        $form->setTaille("om_utilisateur", 20);
        $form->setTaille("service", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_service_om_utilisateur", 11);
        $form->setMax("om_utilisateur", 20);
        $form->setMax("service", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_service_om_utilisateur',_('lien_service_om_utilisateur'));
        $form->setLib('om_utilisateur',_('om_utilisateur'));
        $form->setLib('service',_('service'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // om_utilisateur
        $this->init_select($form, $this->f->db, $maj, null, "om_utilisateur", $sql_om_utilisateur, $sql_om_utilisateur_by_id, false);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('om_utilisateur', $this->retourformulaire))
                $form->setVal('om_utilisateur', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
