<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class lien_demande_type_etat_gen extends om_dbform {

    var $table = "lien_demande_type_etat";
    var $clePrimaire = "lien_demande_type_etat";
    var $typeCle = "N";
    var $required_field = array(
        "demande_type",
        "etat",
        "lien_demande_type_etat"
    );
    
    var $foreign_keys_extended = array(
        "demande_type" => array("demande_type", ),
        "etat" => array("etat", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_demande_type_etat'])) {
            $this->valF['lien_demande_type_etat'] = ""; // -> requis
        } else {
            $this->valF['lien_demande_type_etat'] = $val['lien_demande_type_etat'];
        }
        if (!is_numeric($val['demande_type'])) {
            $this->valF['demande_type'] = ""; // -> requis
        } else {
            $this->valF['demande_type'] = $val['demande_type'];
        }
        $this->valF['etat'] = $val['etat'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_demande_type_etat", "hidden");
            if ($this->is_in_context_of_foreign_key("demande_type", $this->retourformulaire)) {
                $form->setType("demande_type", "selecthiddenstatic");
            } else {
                $form->setType("demande_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("etat", $this->retourformulaire)) {
                $form->setType("etat", "selecthiddenstatic");
            } else {
                $form->setType("etat", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_demande_type_etat", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("demande_type", $this->retourformulaire)) {
                $form->setType("demande_type", "selecthiddenstatic");
            } else {
                $form->setType("demande_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("etat", $this->retourformulaire)) {
                $form->setType("etat", "selecthiddenstatic");
            } else {
                $form->setType("etat", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_demande_type_etat", "hiddenstatic");
            $form->setType("demande_type", "selectstatic");
            $form->setType("etat", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_demande_type_etat", "static");
            $form->setType("demande_type", "selectstatic");
            $form->setType("etat", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_demande_type_etat','VerifNum(this)');
        $form->setOnchange('demande_type','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_demande_type_etat", 11);
        $form->setTaille("demande_type", 11);
        $form->setTaille("etat", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_demande_type_etat", 11);
        $form->setMax("demande_type", 11);
        $form->setMax("etat", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_demande_type_etat',_('lien_demande_type_etat'));
        $form->setLib('demande_type',_('demande_type'));
        $form->setLib('etat',_('etat'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // demande_type
        $this->init_select($form, $this->f->db, $maj, null, "demande_type", $sql_demande_type, $sql_demande_type_by_id, false);
        // etat
        $this->init_select($form, $this->f->db, $maj, null, "etat", $sql_etat, $sql_etat_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('demande_type', $this->retourformulaire))
                $form->setVal('demande_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etat', $this->retourformulaire))
                $form->setVal('etat', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
