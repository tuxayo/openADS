<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class evenement_gen extends om_dbform {

    var $table = "evenement";
    var $clePrimaire = "evenement";
    var $typeCle = "N";
    var $required_field = array(
        "evenement",
        "libelle"
    );
    
    var $foreign_keys_extended = array(
        "action" => array("action", ),
        "autorite_competente" => array("autorite_competente", ),
        "avis_decision" => array("avis_decision", ),
        "etat" => array("etat", ),
        "evenement" => array("evenement", ),
        "phase" => array("phase", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['evenement'])) {
            $this->valF['evenement'] = ""; // -> requis
        } else {
            $this->valF['evenement'] = $val['evenement'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if ($val['action'] == "") {
            $this->valF['action'] = NULL;
        } else {
            $this->valF['action'] = $val['action'];
        }
        if ($val['etat'] == "") {
            $this->valF['etat'] = NULL;
        } else {
            $this->valF['etat'] = $val['etat'];
        }
        if (!is_numeric($val['delai'])) {
            $this->valF['delai'] = NULL;
        } else {
            $this->valF['delai'] = $val['delai'];
        }
        if ($val['accord_tacite'] == "") {
            $this->valF['accord_tacite'] = ""; // -> default
        } else {
            $this->valF['accord_tacite'] = $val['accord_tacite'];
        }
        if (!is_numeric($val['delai_notification'])) {
            $this->valF['delai_notification'] = NULL;
        } else {
            $this->valF['delai_notification'] = $val['delai_notification'];
        }
        if ($val['lettretype'] == "") {
            $this->valF['lettretype'] = ""; // -> default
        } else {
            $this->valF['lettretype'] = $val['lettretype'];
        }
        if ($val['consultation'] == "") {
            $this->valF['consultation'] = ""; // -> default
        } else {
            $this->valF['consultation'] = $val['consultation'];
        }
        if (!is_numeric($val['avis_decision'])) {
            $this->valF['avis_decision'] = NULL;
        } else {
            $this->valF['avis_decision'] = $val['avis_decision'];
        }
        if ($val['restriction'] == "") {
            $this->valF['restriction'] = NULL;
        } else {
            $this->valF['restriction'] = $val['restriction'];
        }
        if ($val['type'] == "") {
            $this->valF['type'] = NULL;
        } else {
            $this->valF['type'] = $val['type'];
        }
        if (!is_numeric($val['evenement_retour_ar'])) {
            $this->valF['evenement_retour_ar'] = NULL;
        } else {
            $this->valF['evenement_retour_ar'] = $val['evenement_retour_ar'];
        }
        if (!is_numeric($val['evenement_suivant_tacite'])) {
            $this->valF['evenement_suivant_tacite'] = NULL;
        } else {
            $this->valF['evenement_suivant_tacite'] = $val['evenement_suivant_tacite'];
        }
        if (!is_numeric($val['evenement_retour_signature'])) {
            $this->valF['evenement_retour_signature'] = NULL;
        } else {
            $this->valF['evenement_retour_signature'] = $val['evenement_retour_signature'];
        }
        if (!is_numeric($val['autorite_competente'])) {
            $this->valF['autorite_competente'] = NULL;
        } else {
            $this->valF['autorite_competente'] = $val['autorite_competente'];
        }
        if ($val['retour'] == 1 || $val['retour'] == "t" || $val['retour'] == "Oui") {
            $this->valF['retour'] = true;
        } else {
            $this->valF['retour'] = false;
        }
        if ($val['non_verrouillable'] == 1 || $val['non_verrouillable'] == "t" || $val['non_verrouillable'] == "Oui") {
            $this->valF['non_verrouillable'] = true;
        } else {
            $this->valF['non_verrouillable'] = false;
        }
        if (!is_numeric($val['phase'])) {
            $this->valF['phase'] = NULL;
        } else {
            $this->valF['phase'] = $val['phase'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("evenement", "hidden");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("action", $this->retourformulaire)) {
                $form->setType("action", "selecthiddenstatic");
            } else {
                $form->setType("action", "select");
            }
            if ($this->is_in_context_of_foreign_key("etat", $this->retourformulaire)) {
                $form->setType("etat", "selecthiddenstatic");
            } else {
                $form->setType("etat", "select");
            }
            $form->setType("delai", "text");
            $form->setType("accord_tacite", "text");
            $form->setType("delai_notification", "text");
            $form->setType("lettretype", "text");
            $form->setType("consultation", "text");
            if ($this->is_in_context_of_foreign_key("avis_decision", $this->retourformulaire)) {
                $form->setType("avis_decision", "selecthiddenstatic");
            } else {
                $form->setType("avis_decision", "select");
            }
            $form->setType("restriction", "text");
            $form->setType("type", "text");
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement_retour_ar", "selecthiddenstatic");
            } else {
                $form->setType("evenement_retour_ar", "select");
            }
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement_suivant_tacite", "selecthiddenstatic");
            } else {
                $form->setType("evenement_suivant_tacite", "select");
            }
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement_retour_signature", "selecthiddenstatic");
            } else {
                $form->setType("evenement_retour_signature", "select");
            }
            if ($this->is_in_context_of_foreign_key("autorite_competente", $this->retourformulaire)) {
                $form->setType("autorite_competente", "selecthiddenstatic");
            } else {
                $form->setType("autorite_competente", "select");
            }
            $form->setType("retour", "checkbox");
            $form->setType("non_verrouillable", "checkbox");
            if ($this->is_in_context_of_foreign_key("phase", $this->retourformulaire)) {
                $form->setType("phase", "selecthiddenstatic");
            } else {
                $form->setType("phase", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("evenement", "hiddenstatic");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("action", $this->retourformulaire)) {
                $form->setType("action", "selecthiddenstatic");
            } else {
                $form->setType("action", "select");
            }
            if ($this->is_in_context_of_foreign_key("etat", $this->retourformulaire)) {
                $form->setType("etat", "selecthiddenstatic");
            } else {
                $form->setType("etat", "select");
            }
            $form->setType("delai", "text");
            $form->setType("accord_tacite", "text");
            $form->setType("delai_notification", "text");
            $form->setType("lettretype", "text");
            $form->setType("consultation", "text");
            if ($this->is_in_context_of_foreign_key("avis_decision", $this->retourformulaire)) {
                $form->setType("avis_decision", "selecthiddenstatic");
            } else {
                $form->setType("avis_decision", "select");
            }
            $form->setType("restriction", "text");
            $form->setType("type", "text");
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement_retour_ar", "selecthiddenstatic");
            } else {
                $form->setType("evenement_retour_ar", "select");
            }
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement_suivant_tacite", "selecthiddenstatic");
            } else {
                $form->setType("evenement_suivant_tacite", "select");
            }
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement_retour_signature", "selecthiddenstatic");
            } else {
                $form->setType("evenement_retour_signature", "select");
            }
            if ($this->is_in_context_of_foreign_key("autorite_competente", $this->retourformulaire)) {
                $form->setType("autorite_competente", "selecthiddenstatic");
            } else {
                $form->setType("autorite_competente", "select");
            }
            $form->setType("retour", "checkbox");
            $form->setType("non_verrouillable", "checkbox");
            if ($this->is_in_context_of_foreign_key("phase", $this->retourformulaire)) {
                $form->setType("phase", "selecthiddenstatic");
            } else {
                $form->setType("phase", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("evenement", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("action", "selectstatic");
            $form->setType("etat", "selectstatic");
            $form->setType("delai", "hiddenstatic");
            $form->setType("accord_tacite", "hiddenstatic");
            $form->setType("delai_notification", "hiddenstatic");
            $form->setType("lettretype", "hiddenstatic");
            $form->setType("consultation", "hiddenstatic");
            $form->setType("avis_decision", "selectstatic");
            $form->setType("restriction", "hiddenstatic");
            $form->setType("type", "hiddenstatic");
            $form->setType("evenement_retour_ar", "selectstatic");
            $form->setType("evenement_suivant_tacite", "selectstatic");
            $form->setType("evenement_retour_signature", "selectstatic");
            $form->setType("autorite_competente", "selectstatic");
            $form->setType("retour", "hiddenstatic");
            $form->setType("non_verrouillable", "hiddenstatic");
            $form->setType("phase", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("evenement", "static");
            $form->setType("libelle", "static");
            $form->setType("action", "selectstatic");
            $form->setType("etat", "selectstatic");
            $form->setType("delai", "static");
            $form->setType("accord_tacite", "static");
            $form->setType("delai_notification", "static");
            $form->setType("lettretype", "static");
            $form->setType("consultation", "static");
            $form->setType("avis_decision", "selectstatic");
            $form->setType("restriction", "static");
            $form->setType("type", "static");
            $form->setType("evenement_retour_ar", "selectstatic");
            $form->setType("evenement_suivant_tacite", "selectstatic");
            $form->setType("evenement_retour_signature", "selectstatic");
            $form->setType("autorite_competente", "selectstatic");
            $form->setType("retour", "checkboxstatic");
            $form->setType("non_verrouillable", "checkboxstatic");
            $form->setType("phase", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('evenement','VerifNum(this)');
        $form->setOnchange('delai','VerifNum(this)');
        $form->setOnchange('delai_notification','VerifNum(this)');
        $form->setOnchange('avis_decision','VerifNum(this)');
        $form->setOnchange('evenement_retour_ar','VerifNum(this)');
        $form->setOnchange('evenement_suivant_tacite','VerifNum(this)');
        $form->setOnchange('evenement_retour_signature','VerifNum(this)');
        $form->setOnchange('autorite_competente','VerifNum(this)');
        $form->setOnchange('phase','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("evenement", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("action", 20);
        $form->setTaille("etat", 20);
        $form->setTaille("delai", 11);
        $form->setTaille("accord_tacite", 10);
        $form->setTaille("delai_notification", 11);
        $form->setTaille("lettretype", 30);
        $form->setTaille("consultation", 10);
        $form->setTaille("avis_decision", 11);
        $form->setTaille("restriction", 30);
        $form->setTaille("type", 30);
        $form->setTaille("evenement_retour_ar", 11);
        $form->setTaille("evenement_suivant_tacite", 11);
        $form->setTaille("evenement_retour_signature", 11);
        $form->setTaille("autorite_competente", 11);
        $form->setTaille("retour", 1);
        $form->setTaille("non_verrouillable", 1);
        $form->setTaille("phase", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("evenement", 11);
        $form->setMax("libelle", 70);
        $form->setMax("action", 20);
        $form->setMax("etat", 20);
        $form->setMax("delai", 11);
        $form->setMax("accord_tacite", 3);
        $form->setMax("delai_notification", 11);
        $form->setMax("lettretype", 60);
        $form->setMax("consultation", 3);
        $form->setMax("avis_decision", 11);
        $form->setMax("restriction", 255);
        $form->setMax("type", 100);
        $form->setMax("evenement_retour_ar", 11);
        $form->setMax("evenement_suivant_tacite", 11);
        $form->setMax("evenement_retour_signature", 11);
        $form->setMax("autorite_competente", 11);
        $form->setMax("retour", 1);
        $form->setMax("non_verrouillable", 1);
        $form->setMax("phase", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('evenement',_('evenement'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('action',_('action'));
        $form->setLib('etat',_('etat'));
        $form->setLib('delai',_('delai'));
        $form->setLib('accord_tacite',_('accord_tacite'));
        $form->setLib('delai_notification',_('delai_notification'));
        $form->setLib('lettretype',_('lettretype'));
        $form->setLib('consultation',_('consultation'));
        $form->setLib('avis_decision',_('avis_decision'));
        $form->setLib('restriction',_('restriction'));
        $form->setLib('type',_('type'));
        $form->setLib('evenement_retour_ar',_('evenement_retour_ar'));
        $form->setLib('evenement_suivant_tacite',_('evenement_suivant_tacite'));
        $form->setLib('evenement_retour_signature',_('evenement_retour_signature'));
        $form->setLib('autorite_competente',_('autorite_competente'));
        $form->setLib('retour',_('retour'));
        $form->setLib('non_verrouillable',_('non_verrouillable'));
        $form->setLib('phase',_('phase'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // action
        $this->init_select($form, $this->f->db, $maj, null, "action", $sql_action, $sql_action_by_id, false);
        // autorite_competente
        $this->init_select($form, $this->f->db, $maj, null, "autorite_competente", $sql_autorite_competente, $sql_autorite_competente_by_id, false);
        // avis_decision
        $this->init_select($form, $this->f->db, $maj, null, "avis_decision", $sql_avis_decision, $sql_avis_decision_by_id, false);
        // etat
        $this->init_select($form, $this->f->db, $maj, null, "etat", $sql_etat, $sql_etat_by_id, false);
        // evenement_retour_ar
        $this->init_select($form, $this->f->db, $maj, null, "evenement_retour_ar", $sql_evenement_retour_ar, $sql_evenement_retour_ar_by_id, false);
        // evenement_retour_signature
        $this->init_select($form, $this->f->db, $maj, null, "evenement_retour_signature", $sql_evenement_retour_signature, $sql_evenement_retour_signature_by_id, false);
        // evenement_suivant_tacite
        $this->init_select($form, $this->f->db, $maj, null, "evenement_suivant_tacite", $sql_evenement_suivant_tacite, $sql_evenement_suivant_tacite_by_id, false);
        // phase
        $this->init_select($form, $this->f->db, $maj, null, "phase", $sql_phase, $sql_phase_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('action', $this->retourformulaire))
                $form->setVal('action', $idxformulaire);
            if($this->is_in_context_of_foreign_key('autorite_competente', $this->retourformulaire))
                $form->setVal('autorite_competente', $idxformulaire);
            if($this->is_in_context_of_foreign_key('avis_decision', $this->retourformulaire))
                $form->setVal('avis_decision', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etat', $this->retourformulaire))
                $form->setVal('etat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('phase', $this->retourformulaire))
                $form->setVal('phase', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('evenement', $this->retourformulaire))
                $form->setVal('evenement_retour_ar', $idxformulaire);
            if($this->is_in_context_of_foreign_key('evenement', $this->retourformulaire))
                $form->setVal('evenement_retour_signature', $idxformulaire);
            if($this->is_in_context_of_foreign_key('evenement', $this->retourformulaire))
                $form->setVal('evenement_suivant_tacite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : bible
        $this->rechercheTable($this->f->db, "bible", "evenement", $id);
        // Verification de la cle secondaire : demande_type
        $this->rechercheTable($this->f->db, "demande_type", "evenement", $id);
        // Verification de la cle secondaire : dossier
        $this->rechercheTable($this->f->db, "dossier", "evenement_suivant_tacite", $id);
        // Verification de la cle secondaire : dossier
        $this->rechercheTable($this->f->db, "dossier", "evenement_suivant_tacite_incompletude", $id);
        // Verification de la cle secondaire : evenement
        $this->rechercheTable($this->f->db, "evenement", "evenement_retour_ar", $id);
        // Verification de la cle secondaire : evenement
        $this->rechercheTable($this->f->db, "evenement", "evenement_retour_signature", $id);
        // Verification de la cle secondaire : evenement
        $this->rechercheTable($this->f->db, "evenement", "evenement_suivant_tacite", $id);
        // Verification de la cle secondaire : instruction
        $this->rechercheTable($this->f->db, "instruction", "archive_evenement_suivant_tacite", $id);
        // Verification de la cle secondaire : instruction
        $this->rechercheTable($this->f->db, "instruction", "archive_evenement_suivant_tacite_incompletude", $id);
        // Verification de la cle secondaire : instruction
        $this->rechercheTable($this->f->db, "instruction", "evenement", $id);
        // Verification de la cle secondaire : lien_dossier_instruction_type_evenement
        $this->rechercheTable($this->f->db, "lien_dossier_instruction_type_evenement", "evenement", $id);
        // Verification de la cle secondaire : transition
        $this->rechercheTable($this->f->db, "transition", "evenement", $id);
    }


}

?>
