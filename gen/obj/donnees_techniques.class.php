<?php
//$Id$ 
//gen openMairie le 05/07/2017 16:27

require_once "../obj/om_dbform.class.php";

class donnees_techniques_gen extends om_dbform {

    var $table = "donnees_techniques";
    var $clePrimaire = "donnees_techniques";
    var $typeCle = "N";
    var $required_field = array(
        "cerfa",
        "donnees_techniques"
    );
    var $unique_key = array(
      "dossier_autorisation",
      "dossier_instruction",
      "lot",
    );
    var $foreign_keys_extended = array(
        "architecte" => array("architecte", ),
        "objet_recours" => array("objet_recours", ),
        "dossier_autorisation" => array("dossier_autorisation", ),
        "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
        "lot" => array("lot", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['donnees_techniques'])) {
            $this->valF['donnees_techniques'] = ""; // -> requis
        } else {
            $this->valF['donnees_techniques'] = $val['donnees_techniques'];
        }
        if ($val['dossier_instruction'] == "") {
            $this->valF['dossier_instruction'] = NULL;
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
        if (!is_numeric($val['lot'])) {
            $this->valF['lot'] = NULL;
        } else {
            $this->valF['lot'] = $val['lot'];
        }
        if ($val['am_lotiss'] == 1 || $val['am_lotiss'] == "t" || $val['am_lotiss'] == "Oui") {
            $this->valF['am_lotiss'] = true;
        } else {
            $this->valF['am_lotiss'] = false;
        }
        if ($val['am_autre_div'] == 1 || $val['am_autre_div'] == "t" || $val['am_autre_div'] == "Oui") {
            $this->valF['am_autre_div'] = true;
        } else {
            $this->valF['am_autre_div'] = false;
        }
        if ($val['am_camping'] == 1 || $val['am_camping'] == "t" || $val['am_camping'] == "Oui") {
            $this->valF['am_camping'] = true;
        } else {
            $this->valF['am_camping'] = false;
        }
        if ($val['am_caravane'] == 1 || $val['am_caravane'] == "t" || $val['am_caravane'] == "Oui") {
            $this->valF['am_caravane'] = true;
        } else {
            $this->valF['am_caravane'] = false;
        }
        if (!is_numeric($val['am_carav_duree'])) {
            $this->valF['am_carav_duree'] = NULL;
        } else {
            $this->valF['am_carav_duree'] = $val['am_carav_duree'];
        }
        if ($val['am_statio'] == 1 || $val['am_statio'] == "t" || $val['am_statio'] == "Oui") {
            $this->valF['am_statio'] = true;
        } else {
            $this->valF['am_statio'] = false;
        }
        if (!is_numeric($val['am_statio_cont'])) {
            $this->valF['am_statio_cont'] = NULL;
        } else {
            $this->valF['am_statio_cont'] = $val['am_statio_cont'];
        }
        if ($val['am_affou_exhau'] == 1 || $val['am_affou_exhau'] == "t" || $val['am_affou_exhau'] == "Oui") {
            $this->valF['am_affou_exhau'] = true;
        } else {
            $this->valF['am_affou_exhau'] = false;
        }
        if (!is_numeric($val['am_affou_exhau_sup'])) {
            $this->valF['am_affou_exhau_sup'] = NULL;
        } else {
            $this->valF['am_affou_exhau_sup'] = $val['am_affou_exhau_sup'];
        }
        if (!is_numeric($val['am_affou_prof'])) {
            $this->valF['am_affou_prof'] = NULL;
        } else {
            $this->valF['am_affou_prof'] = $val['am_affou_prof'];
        }
        if (!is_numeric($val['am_exhau_haut'])) {
            $this->valF['am_exhau_haut'] = NULL;
        } else {
            $this->valF['am_exhau_haut'] = $val['am_exhau_haut'];
        }
        if ($val['am_coupe_abat'] == 1 || $val['am_coupe_abat'] == "t" || $val['am_coupe_abat'] == "Oui") {
            $this->valF['am_coupe_abat'] = true;
        } else {
            $this->valF['am_coupe_abat'] = false;
        }
        if ($val['am_prot_plu'] == 1 || $val['am_prot_plu'] == "t" || $val['am_prot_plu'] == "Oui") {
            $this->valF['am_prot_plu'] = true;
        } else {
            $this->valF['am_prot_plu'] = false;
        }
        if ($val['am_prot_muni'] == 1 || $val['am_prot_muni'] == "t" || $val['am_prot_muni'] == "Oui") {
            $this->valF['am_prot_muni'] = true;
        } else {
            $this->valF['am_prot_muni'] = false;
        }
        if ($val['am_mobil_voyage'] == 1 || $val['am_mobil_voyage'] == "t" || $val['am_mobil_voyage'] == "Oui") {
            $this->valF['am_mobil_voyage'] = true;
        } else {
            $this->valF['am_mobil_voyage'] = false;
        }
        if ($val['am_aire_voyage'] == 1 || $val['am_aire_voyage'] == "t" || $val['am_aire_voyage'] == "Oui") {
            $this->valF['am_aire_voyage'] = true;
        } else {
            $this->valF['am_aire_voyage'] = false;
        }
        if ($val['am_rememb_afu'] == 1 || $val['am_rememb_afu'] == "t" || $val['am_rememb_afu'] == "Oui") {
            $this->valF['am_rememb_afu'] = true;
        } else {
            $this->valF['am_rememb_afu'] = false;
        }
        if ($val['am_parc_resid_loi'] == 1 || $val['am_parc_resid_loi'] == "t" || $val['am_parc_resid_loi'] == "Oui") {
            $this->valF['am_parc_resid_loi'] = true;
        } else {
            $this->valF['am_parc_resid_loi'] = false;
        }
        if ($val['am_sport_moto'] == 1 || $val['am_sport_moto'] == "t" || $val['am_sport_moto'] == "Oui") {
            $this->valF['am_sport_moto'] = true;
        } else {
            $this->valF['am_sport_moto'] = false;
        }
        if ($val['am_sport_attrac'] == 1 || $val['am_sport_attrac'] == "t" || $val['am_sport_attrac'] == "Oui") {
            $this->valF['am_sport_attrac'] = true;
        } else {
            $this->valF['am_sport_attrac'] = false;
        }
        if ($val['am_sport_golf'] == 1 || $val['am_sport_golf'] == "t" || $val['am_sport_golf'] == "Oui") {
            $this->valF['am_sport_golf'] = true;
        } else {
            $this->valF['am_sport_golf'] = false;
        }
        if ($val['am_mob_art'] == 1 || $val['am_mob_art'] == "t" || $val['am_mob_art'] == "Oui") {
            $this->valF['am_mob_art'] = true;
        } else {
            $this->valF['am_mob_art'] = false;
        }
        if ($val['am_modif_voie_esp'] == 1 || $val['am_modif_voie_esp'] == "t" || $val['am_modif_voie_esp'] == "Oui") {
            $this->valF['am_modif_voie_esp'] = true;
        } else {
            $this->valF['am_modif_voie_esp'] = false;
        }
        if ($val['am_plant_voie_esp'] == 1 || $val['am_plant_voie_esp'] == "t" || $val['am_plant_voie_esp'] == "Oui") {
            $this->valF['am_plant_voie_esp'] = true;
        } else {
            $this->valF['am_plant_voie_esp'] = false;
        }
        if ($val['am_chem_ouv_esp'] == 1 || $val['am_chem_ouv_esp'] == "t" || $val['am_chem_ouv_esp'] == "Oui") {
            $this->valF['am_chem_ouv_esp'] = true;
        } else {
            $this->valF['am_chem_ouv_esp'] = false;
        }
        if ($val['am_agri_peche'] == 1 || $val['am_agri_peche'] == "t" || $val['am_agri_peche'] == "Oui") {
            $this->valF['am_agri_peche'] = true;
        } else {
            $this->valF['am_agri_peche'] = false;
        }
        if ($val['am_crea_voie'] == 1 || $val['am_crea_voie'] == "t" || $val['am_crea_voie'] == "Oui") {
            $this->valF['am_crea_voie'] = true;
        } else {
            $this->valF['am_crea_voie'] = false;
        }
        if ($val['am_modif_voie_exist'] == 1 || $val['am_modif_voie_exist'] == "t" || $val['am_modif_voie_exist'] == "Oui") {
            $this->valF['am_modif_voie_exist'] = true;
        } else {
            $this->valF['am_modif_voie_exist'] = false;
        }
        if ($val['am_crea_esp_sauv'] == 1 || $val['am_crea_esp_sauv'] == "t" || $val['am_crea_esp_sauv'] == "Oui") {
            $this->valF['am_crea_esp_sauv'] = true;
        } else {
            $this->valF['am_crea_esp_sauv'] = false;
        }
        if ($val['am_crea_esp_class'] == 1 || $val['am_crea_esp_class'] == "t" || $val['am_crea_esp_class'] == "Oui") {
            $this->valF['am_crea_esp_class'] = true;
        } else {
            $this->valF['am_crea_esp_class'] = false;
        }
            $this->valF['am_projet_desc'] = $val['am_projet_desc'];
        if (!is_numeric($val['am_terr_surf'])) {
            $this->valF['am_terr_surf'] = NULL;
        } else {
            $this->valF['am_terr_surf'] = $val['am_terr_surf'];
        }
            $this->valF['am_tranche_desc'] = $val['am_tranche_desc'];
        if (!is_numeric($val['am_lot_max_nb'])) {
            $this->valF['am_lot_max_nb'] = NULL;
        } else {
            $this->valF['am_lot_max_nb'] = $val['am_lot_max_nb'];
        }
        if (!is_numeric($val['am_lot_max_shon'])) {
            $this->valF['am_lot_max_shon'] = NULL;
        } else {
            $this->valF['am_lot_max_shon'] = $val['am_lot_max_shon'];
        }
        if ($val['am_lot_cstr_cos'] == 1 || $val['am_lot_cstr_cos'] == "t" || $val['am_lot_cstr_cos'] == "Oui") {
            $this->valF['am_lot_cstr_cos'] = true;
        } else {
            $this->valF['am_lot_cstr_cos'] = false;
        }
        if ($val['am_lot_cstr_plan'] == 1 || $val['am_lot_cstr_plan'] == "t" || $val['am_lot_cstr_plan'] == "Oui") {
            $this->valF['am_lot_cstr_plan'] = true;
        } else {
            $this->valF['am_lot_cstr_plan'] = false;
        }
        if ($val['am_lot_cstr_vente'] == 1 || $val['am_lot_cstr_vente'] == "t" || $val['am_lot_cstr_vente'] == "Oui") {
            $this->valF['am_lot_cstr_vente'] = true;
        } else {
            $this->valF['am_lot_cstr_vente'] = false;
        }
        if ($val['am_lot_fin_diff'] == 1 || $val['am_lot_fin_diff'] == "t" || $val['am_lot_fin_diff'] == "Oui") {
            $this->valF['am_lot_fin_diff'] = true;
        } else {
            $this->valF['am_lot_fin_diff'] = false;
        }
        if ($val['am_lot_consign'] == 1 || $val['am_lot_consign'] == "t" || $val['am_lot_consign'] == "Oui") {
            $this->valF['am_lot_consign'] = true;
        } else {
            $this->valF['am_lot_consign'] = false;
        }
        if ($val['am_lot_gar_achev'] == 1 || $val['am_lot_gar_achev'] == "t" || $val['am_lot_gar_achev'] == "Oui") {
            $this->valF['am_lot_gar_achev'] = true;
        } else {
            $this->valF['am_lot_gar_achev'] = false;
        }
        if ($val['am_lot_vente_ant'] == 1 || $val['am_lot_vente_ant'] == "t" || $val['am_lot_vente_ant'] == "Oui") {
            $this->valF['am_lot_vente_ant'] = true;
        } else {
            $this->valF['am_lot_vente_ant'] = false;
        }
        if (!is_numeric($val['am_empl_nb'])) {
            $this->valF['am_empl_nb'] = NULL;
        } else {
            $this->valF['am_empl_nb'] = $val['am_empl_nb'];
        }
        if (!is_numeric($val['am_tente_nb'])) {
            $this->valF['am_tente_nb'] = NULL;
        } else {
            $this->valF['am_tente_nb'] = $val['am_tente_nb'];
        }
        if (!is_numeric($val['am_carav_nb'])) {
            $this->valF['am_carav_nb'] = NULL;
        } else {
            $this->valF['am_carav_nb'] = $val['am_carav_nb'];
        }
        if (!is_numeric($val['am_mobil_nb'])) {
            $this->valF['am_mobil_nb'] = NULL;
        } else {
            $this->valF['am_mobil_nb'] = $val['am_mobil_nb'];
        }
        if (!is_numeric($val['am_pers_nb'])) {
            $this->valF['am_pers_nb'] = NULL;
        } else {
            $this->valF['am_pers_nb'] = $val['am_pers_nb'];
        }
        if (!is_numeric($val['am_empl_hll_nb'])) {
            $this->valF['am_empl_hll_nb'] = NULL;
        } else {
            $this->valF['am_empl_hll_nb'] = $val['am_empl_hll_nb'];
        }
        if (!is_numeric($val['am_hll_shon'])) {
            $this->valF['am_hll_shon'] = NULL;
        } else {
            $this->valF['am_hll_shon'] = $val['am_hll_shon'];
        }
            $this->valF['am_periode_exploit'] = $val['am_periode_exploit'];
        if ($val['am_exist_agrand'] == 1 || $val['am_exist_agrand'] == "t" || $val['am_exist_agrand'] == "Oui") {
            $this->valF['am_exist_agrand'] = true;
        } else {
            $this->valF['am_exist_agrand'] = false;
        }
        if ($val['am_exist_date'] != "") {
            $this->valF['am_exist_date'] = $this->dateDB($val['am_exist_date']);
        } else {
            $this->valF['am_exist_date'] = NULL;
        }
        if ($val['am_exist_num'] == "") {
            $this->valF['am_exist_num'] = NULL;
        } else {
            $this->valF['am_exist_num'] = $val['am_exist_num'];
        }
        if (!is_numeric($val['am_exist_nb_avant'])) {
            $this->valF['am_exist_nb_avant'] = NULL;
        } else {
            $this->valF['am_exist_nb_avant'] = $val['am_exist_nb_avant'];
        }
        if (!is_numeric($val['am_exist_nb_apres'])) {
            $this->valF['am_exist_nb_apres'] = NULL;
        } else {
            $this->valF['am_exist_nb_apres'] = $val['am_exist_nb_apres'];
        }
        if ($val['am_coupe_bois'] == 1 || $val['am_coupe_bois'] == "t" || $val['am_coupe_bois'] == "Oui") {
            $this->valF['am_coupe_bois'] = true;
        } else {
            $this->valF['am_coupe_bois'] = false;
        }
        if ($val['am_coupe_parc'] == 1 || $val['am_coupe_parc'] == "t" || $val['am_coupe_parc'] == "Oui") {
            $this->valF['am_coupe_parc'] = true;
        } else {
            $this->valF['am_coupe_parc'] = false;
        }
        if ($val['am_coupe_align'] == 1 || $val['am_coupe_align'] == "t" || $val['am_coupe_align'] == "Oui") {
            $this->valF['am_coupe_align'] = true;
        } else {
            $this->valF['am_coupe_align'] = false;
        }
        if ($val['am_coupe_ess'] == "") {
            $this->valF['am_coupe_ess'] = NULL;
        } else {
            $this->valF['am_coupe_ess'] = $val['am_coupe_ess'];
        }
        if ($val['am_coupe_age'] == "") {
            $this->valF['am_coupe_age'] = NULL;
        } else {
            $this->valF['am_coupe_age'] = $val['am_coupe_age'];
        }
        if ($val['am_coupe_dens'] == "") {
            $this->valF['am_coupe_dens'] = NULL;
        } else {
            $this->valF['am_coupe_dens'] = $val['am_coupe_dens'];
        }
        if ($val['am_coupe_qual'] == "") {
            $this->valF['am_coupe_qual'] = NULL;
        } else {
            $this->valF['am_coupe_qual'] = $val['am_coupe_qual'];
        }
        if ($val['am_coupe_trait'] == "") {
            $this->valF['am_coupe_trait'] = NULL;
        } else {
            $this->valF['am_coupe_trait'] = $val['am_coupe_trait'];
        }
        if ($val['am_coupe_autr'] == "") {
            $this->valF['am_coupe_autr'] = NULL;
        } else {
            $this->valF['am_coupe_autr'] = $val['am_coupe_autr'];
        }
        if ($val['co_archi_recours'] == 1 || $val['co_archi_recours'] == "t" || $val['co_archi_recours'] == "Oui") {
            $this->valF['co_archi_recours'] = true;
        } else {
            $this->valF['co_archi_recours'] = false;
        }
        if ($val['co_cstr_nouv'] == 1 || $val['co_cstr_nouv'] == "t" || $val['co_cstr_nouv'] == "Oui") {
            $this->valF['co_cstr_nouv'] = true;
        } else {
            $this->valF['co_cstr_nouv'] = false;
        }
        if ($val['co_cstr_exist'] == 1 || $val['co_cstr_exist'] == "t" || $val['co_cstr_exist'] == "Oui") {
            $this->valF['co_cstr_exist'] = true;
        } else {
            $this->valF['co_cstr_exist'] = false;
        }
        if ($val['co_cloture'] == 1 || $val['co_cloture'] == "t" || $val['co_cloture'] == "Oui") {
            $this->valF['co_cloture'] = true;
        } else {
            $this->valF['co_cloture'] = false;
        }
        if (!is_numeric($val['co_elec_tension'])) {
            $this->valF['co_elec_tension'] = NULL;
        } else {
            $this->valF['co_elec_tension'] = $val['co_elec_tension'];
        }
        if ($val['co_div_terr'] == 1 || $val['co_div_terr'] == "t" || $val['co_div_terr'] == "Oui") {
            $this->valF['co_div_terr'] = true;
        } else {
            $this->valF['co_div_terr'] = false;
        }
            $this->valF['co_projet_desc'] = $val['co_projet_desc'];
        if ($val['co_anx_pisc'] == 1 || $val['co_anx_pisc'] == "t" || $val['co_anx_pisc'] == "Oui") {
            $this->valF['co_anx_pisc'] = true;
        } else {
            $this->valF['co_anx_pisc'] = false;
        }
        if ($val['co_anx_gara'] == 1 || $val['co_anx_gara'] == "t" || $val['co_anx_gara'] == "Oui") {
            $this->valF['co_anx_gara'] = true;
        } else {
            $this->valF['co_anx_gara'] = false;
        }
        if ($val['co_anx_veran'] == 1 || $val['co_anx_veran'] == "t" || $val['co_anx_veran'] == "Oui") {
            $this->valF['co_anx_veran'] = true;
        } else {
            $this->valF['co_anx_veran'] = false;
        }
        if ($val['co_anx_abri'] == 1 || $val['co_anx_abri'] == "t" || $val['co_anx_abri'] == "Oui") {
            $this->valF['co_anx_abri'] = true;
        } else {
            $this->valF['co_anx_abri'] = false;
        }
        if ($val['co_anx_autr'] == 1 || $val['co_anx_autr'] == "t" || $val['co_anx_autr'] == "Oui") {
            $this->valF['co_anx_autr'] = true;
        } else {
            $this->valF['co_anx_autr'] = false;
        }
            $this->valF['co_anx_autr_desc'] = $val['co_anx_autr_desc'];
        if (!is_numeric($val['co_tot_log_nb'])) {
            $this->valF['co_tot_log_nb'] = NULL;
        } else {
            $this->valF['co_tot_log_nb'] = $val['co_tot_log_nb'];
        }
        if (!is_numeric($val['co_tot_ind_nb'])) {
            $this->valF['co_tot_ind_nb'] = NULL;
        } else {
            $this->valF['co_tot_ind_nb'] = $val['co_tot_ind_nb'];
        }
        if (!is_numeric($val['co_tot_coll_nb'])) {
            $this->valF['co_tot_coll_nb'] = NULL;
        } else {
            $this->valF['co_tot_coll_nb'] = $val['co_tot_coll_nb'];
        }
        if (!is_numeric($val['co_mais_piece_nb'])) {
            $this->valF['co_mais_piece_nb'] = NULL;
        } else {
            $this->valF['co_mais_piece_nb'] = $val['co_mais_piece_nb'];
        }
        if (!is_numeric($val['co_mais_niv_nb'])) {
            $this->valF['co_mais_niv_nb'] = NULL;
        } else {
            $this->valF['co_mais_niv_nb'] = $val['co_mais_niv_nb'];
        }
        if (!is_numeric($val['co_fin_lls_nb'])) {
            $this->valF['co_fin_lls_nb'] = NULL;
        } else {
            $this->valF['co_fin_lls_nb'] = $val['co_fin_lls_nb'];
        }
        if (!is_numeric($val['co_fin_aa_nb'])) {
            $this->valF['co_fin_aa_nb'] = NULL;
        } else {
            $this->valF['co_fin_aa_nb'] = $val['co_fin_aa_nb'];
        }
        if (!is_numeric($val['co_fin_ptz_nb'])) {
            $this->valF['co_fin_ptz_nb'] = NULL;
        } else {
            $this->valF['co_fin_ptz_nb'] = $val['co_fin_ptz_nb'];
        }
        if (!is_numeric($val['co_fin_autr_nb'])) {
            $this->valF['co_fin_autr_nb'] = NULL;
        } else {
            $this->valF['co_fin_autr_nb'] = $val['co_fin_autr_nb'];
        }
            $this->valF['co_fin_autr_desc'] = $val['co_fin_autr_desc'];
        if ($val['co_mais_contrat_ind'] == 1 || $val['co_mais_contrat_ind'] == "t" || $val['co_mais_contrat_ind'] == "Oui") {
            $this->valF['co_mais_contrat_ind'] = true;
        } else {
            $this->valF['co_mais_contrat_ind'] = false;
        }
        if ($val['co_uti_pers'] == 1 || $val['co_uti_pers'] == "t" || $val['co_uti_pers'] == "Oui") {
            $this->valF['co_uti_pers'] = true;
        } else {
            $this->valF['co_uti_pers'] = false;
        }
        if ($val['co_uti_vente'] == 1 || $val['co_uti_vente'] == "t" || $val['co_uti_vente'] == "Oui") {
            $this->valF['co_uti_vente'] = true;
        } else {
            $this->valF['co_uti_vente'] = false;
        }
        if ($val['co_uti_loc'] == 1 || $val['co_uti_loc'] == "t" || $val['co_uti_loc'] == "Oui") {
            $this->valF['co_uti_loc'] = true;
        } else {
            $this->valF['co_uti_loc'] = false;
        }
        if ($val['co_uti_princ'] == 1 || $val['co_uti_princ'] == "t" || $val['co_uti_princ'] == "Oui") {
            $this->valF['co_uti_princ'] = true;
        } else {
            $this->valF['co_uti_princ'] = false;
        }
        if ($val['co_uti_secon'] == 1 || $val['co_uti_secon'] == "t" || $val['co_uti_secon'] == "Oui") {
            $this->valF['co_uti_secon'] = true;
        } else {
            $this->valF['co_uti_secon'] = false;
        }
        if ($val['co_resid_agees'] == 1 || $val['co_resid_agees'] == "t" || $val['co_resid_agees'] == "Oui") {
            $this->valF['co_resid_agees'] = true;
        } else {
            $this->valF['co_resid_agees'] = false;
        }
        if ($val['co_resid_etud'] == 1 || $val['co_resid_etud'] == "t" || $val['co_resid_etud'] == "Oui") {
            $this->valF['co_resid_etud'] = true;
        } else {
            $this->valF['co_resid_etud'] = false;
        }
        if ($val['co_resid_tourism'] == 1 || $val['co_resid_tourism'] == "t" || $val['co_resid_tourism'] == "Oui") {
            $this->valF['co_resid_tourism'] = true;
        } else {
            $this->valF['co_resid_tourism'] = false;
        }
        if ($val['co_resid_hot_soc'] == 1 || $val['co_resid_hot_soc'] == "t" || $val['co_resid_hot_soc'] == "Oui") {
            $this->valF['co_resid_hot_soc'] = true;
        } else {
            $this->valF['co_resid_hot_soc'] = false;
        }
        if ($val['co_resid_soc'] == 1 || $val['co_resid_soc'] == "t" || $val['co_resid_soc'] == "Oui") {
            $this->valF['co_resid_soc'] = true;
        } else {
            $this->valF['co_resid_soc'] = false;
        }
        if ($val['co_resid_hand'] == 1 || $val['co_resid_hand'] == "t" || $val['co_resid_hand'] == "Oui") {
            $this->valF['co_resid_hand'] = true;
        } else {
            $this->valF['co_resid_hand'] = false;
        }
        if ($val['co_resid_autr'] == 1 || $val['co_resid_autr'] == "t" || $val['co_resid_autr'] == "Oui") {
            $this->valF['co_resid_autr'] = true;
        } else {
            $this->valF['co_resid_autr'] = false;
        }
            $this->valF['co_resid_autr_desc'] = $val['co_resid_autr_desc'];
        if (!is_numeric($val['co_foyer_chamb_nb'])) {
            $this->valF['co_foyer_chamb_nb'] = NULL;
        } else {
            $this->valF['co_foyer_chamb_nb'] = $val['co_foyer_chamb_nb'];
        }
        if (!is_numeric($val['co_log_1p_nb'])) {
            $this->valF['co_log_1p_nb'] = NULL;
        } else {
            $this->valF['co_log_1p_nb'] = $val['co_log_1p_nb'];
        }
        if (!is_numeric($val['co_log_2p_nb'])) {
            $this->valF['co_log_2p_nb'] = NULL;
        } else {
            $this->valF['co_log_2p_nb'] = $val['co_log_2p_nb'];
        }
        if (!is_numeric($val['co_log_3p_nb'])) {
            $this->valF['co_log_3p_nb'] = NULL;
        } else {
            $this->valF['co_log_3p_nb'] = $val['co_log_3p_nb'];
        }
        if (!is_numeric($val['co_log_4p_nb'])) {
            $this->valF['co_log_4p_nb'] = NULL;
        } else {
            $this->valF['co_log_4p_nb'] = $val['co_log_4p_nb'];
        }
        if (!is_numeric($val['co_log_5p_nb'])) {
            $this->valF['co_log_5p_nb'] = NULL;
        } else {
            $this->valF['co_log_5p_nb'] = $val['co_log_5p_nb'];
        }
        if (!is_numeric($val['co_log_6p_nb'])) {
            $this->valF['co_log_6p_nb'] = NULL;
        } else {
            $this->valF['co_log_6p_nb'] = $val['co_log_6p_nb'];
        }
        if (!is_numeric($val['co_bat_niv_nb'])) {
            $this->valF['co_bat_niv_nb'] = NULL;
        } else {
            $this->valF['co_bat_niv_nb'] = $val['co_bat_niv_nb'];
        }
        if ($val['co_trx_exten'] == 1 || $val['co_trx_exten'] == "t" || $val['co_trx_exten'] == "Oui") {
            $this->valF['co_trx_exten'] = true;
        } else {
            $this->valF['co_trx_exten'] = false;
        }
        if ($val['co_trx_surelev'] == 1 || $val['co_trx_surelev'] == "t" || $val['co_trx_surelev'] == "Oui") {
            $this->valF['co_trx_surelev'] = true;
        } else {
            $this->valF['co_trx_surelev'] = false;
        }
        if ($val['co_trx_nivsup'] == 1 || $val['co_trx_nivsup'] == "t" || $val['co_trx_nivsup'] == "Oui") {
            $this->valF['co_trx_nivsup'] = true;
        } else {
            $this->valF['co_trx_nivsup'] = false;
        }
            $this->valF['co_demont_periode'] = $val['co_demont_periode'];
        if ($val['co_sp_transport'] == 1 || $val['co_sp_transport'] == "t" || $val['co_sp_transport'] == "Oui") {
            $this->valF['co_sp_transport'] = true;
        } else {
            $this->valF['co_sp_transport'] = false;
        }
        if ($val['co_sp_enseign'] == 1 || $val['co_sp_enseign'] == "t" || $val['co_sp_enseign'] == "Oui") {
            $this->valF['co_sp_enseign'] = true;
        } else {
            $this->valF['co_sp_enseign'] = false;
        }
        if ($val['co_sp_act_soc'] == 1 || $val['co_sp_act_soc'] == "t" || $val['co_sp_act_soc'] == "Oui") {
            $this->valF['co_sp_act_soc'] = true;
        } else {
            $this->valF['co_sp_act_soc'] = false;
        }
        if ($val['co_sp_ouvr_spe'] == 1 || $val['co_sp_ouvr_spe'] == "t" || $val['co_sp_ouvr_spe'] == "Oui") {
            $this->valF['co_sp_ouvr_spe'] = true;
        } else {
            $this->valF['co_sp_ouvr_spe'] = false;
        }
        if ($val['co_sp_sante'] == 1 || $val['co_sp_sante'] == "t" || $val['co_sp_sante'] == "Oui") {
            $this->valF['co_sp_sante'] = true;
        } else {
            $this->valF['co_sp_sante'] = false;
        }
        if ($val['co_sp_culture'] == 1 || $val['co_sp_culture'] == "t" || $val['co_sp_culture'] == "Oui") {
            $this->valF['co_sp_culture'] = true;
        } else {
            $this->valF['co_sp_culture'] = false;
        }
        if (!is_numeric($val['co_statio_avt_nb'])) {
            $this->valF['co_statio_avt_nb'] = NULL;
        } else {
            $this->valF['co_statio_avt_nb'] = $val['co_statio_avt_nb'];
        }
        if (!is_numeric($val['co_statio_apr_nb'])) {
            $this->valF['co_statio_apr_nb'] = NULL;
        } else {
            $this->valF['co_statio_apr_nb'] = $val['co_statio_apr_nb'];
        }
            $this->valF['co_statio_adr'] = $val['co_statio_adr'];
        if (!is_numeric($val['co_statio_place_nb'])) {
            $this->valF['co_statio_place_nb'] = NULL;
        } else {
            $this->valF['co_statio_place_nb'] = $val['co_statio_place_nb'];
        }
        if (!is_numeric($val['co_statio_tot_surf'])) {
            $this->valF['co_statio_tot_surf'] = NULL;
        } else {
            $this->valF['co_statio_tot_surf'] = $val['co_statio_tot_surf'];
        }
        if (!is_numeric($val['co_statio_tot_shob'])) {
            $this->valF['co_statio_tot_shob'] = NULL;
        } else {
            $this->valF['co_statio_tot_shob'] = $val['co_statio_tot_shob'];
        }
        if (!is_numeric($val['co_statio_comm_cin_surf'])) {
            $this->valF['co_statio_comm_cin_surf'] = NULL;
        } else {
            $this->valF['co_statio_comm_cin_surf'] = $val['co_statio_comm_cin_surf'];
        }
        if (!is_numeric($val['su_avt_shon1'])) {
            $this->valF['su_avt_shon1'] = NULL;
        } else {
            $this->valF['su_avt_shon1'] = $val['su_avt_shon1'];
        }
        if (!is_numeric($val['su_avt_shon2'])) {
            $this->valF['su_avt_shon2'] = NULL;
        } else {
            $this->valF['su_avt_shon2'] = $val['su_avt_shon2'];
        }
        if (!is_numeric($val['su_avt_shon3'])) {
            $this->valF['su_avt_shon3'] = NULL;
        } else {
            $this->valF['su_avt_shon3'] = $val['su_avt_shon3'];
        }
        if (!is_numeric($val['su_avt_shon4'])) {
            $this->valF['su_avt_shon4'] = NULL;
        } else {
            $this->valF['su_avt_shon4'] = $val['su_avt_shon4'];
        }
        if (!is_numeric($val['su_avt_shon5'])) {
            $this->valF['su_avt_shon5'] = NULL;
        } else {
            $this->valF['su_avt_shon5'] = $val['su_avt_shon5'];
        }
        if (!is_numeric($val['su_avt_shon6'])) {
            $this->valF['su_avt_shon6'] = NULL;
        } else {
            $this->valF['su_avt_shon6'] = $val['su_avt_shon6'];
        }
        if (!is_numeric($val['su_avt_shon7'])) {
            $this->valF['su_avt_shon7'] = NULL;
        } else {
            $this->valF['su_avt_shon7'] = $val['su_avt_shon7'];
        }
        if (!is_numeric($val['su_avt_shon8'])) {
            $this->valF['su_avt_shon8'] = NULL;
        } else {
            $this->valF['su_avt_shon8'] = $val['su_avt_shon8'];
        }
        if (!is_numeric($val['su_avt_shon9'])) {
            $this->valF['su_avt_shon9'] = NULL;
        } else {
            $this->valF['su_avt_shon9'] = $val['su_avt_shon9'];
        }
        if (!is_numeric($val['su_cstr_shon1'])) {
            $this->valF['su_cstr_shon1'] = NULL;
        } else {
            $this->valF['su_cstr_shon1'] = $val['su_cstr_shon1'];
        }
        if (!is_numeric($val['su_cstr_shon2'])) {
            $this->valF['su_cstr_shon2'] = NULL;
        } else {
            $this->valF['su_cstr_shon2'] = $val['su_cstr_shon2'];
        }
        if (!is_numeric($val['su_cstr_shon3'])) {
            $this->valF['su_cstr_shon3'] = NULL;
        } else {
            $this->valF['su_cstr_shon3'] = $val['su_cstr_shon3'];
        }
        if (!is_numeric($val['su_cstr_shon4'])) {
            $this->valF['su_cstr_shon4'] = NULL;
        } else {
            $this->valF['su_cstr_shon4'] = $val['su_cstr_shon4'];
        }
        if (!is_numeric($val['su_cstr_shon5'])) {
            $this->valF['su_cstr_shon5'] = NULL;
        } else {
            $this->valF['su_cstr_shon5'] = $val['su_cstr_shon5'];
        }
        if (!is_numeric($val['su_cstr_shon6'])) {
            $this->valF['su_cstr_shon6'] = NULL;
        } else {
            $this->valF['su_cstr_shon6'] = $val['su_cstr_shon6'];
        }
        if (!is_numeric($val['su_cstr_shon7'])) {
            $this->valF['su_cstr_shon7'] = NULL;
        } else {
            $this->valF['su_cstr_shon7'] = $val['su_cstr_shon7'];
        }
        if (!is_numeric($val['su_cstr_shon8'])) {
            $this->valF['su_cstr_shon8'] = NULL;
        } else {
            $this->valF['su_cstr_shon8'] = $val['su_cstr_shon8'];
        }
        if (!is_numeric($val['su_cstr_shon9'])) {
            $this->valF['su_cstr_shon9'] = NULL;
        } else {
            $this->valF['su_cstr_shon9'] = $val['su_cstr_shon9'];
        }
        if (!is_numeric($val['su_trsf_shon1'])) {
            $this->valF['su_trsf_shon1'] = NULL;
        } else {
            $this->valF['su_trsf_shon1'] = $val['su_trsf_shon1'];
        }
        if (!is_numeric($val['su_trsf_shon2'])) {
            $this->valF['su_trsf_shon2'] = NULL;
        } else {
            $this->valF['su_trsf_shon2'] = $val['su_trsf_shon2'];
        }
        if (!is_numeric($val['su_trsf_shon3'])) {
            $this->valF['su_trsf_shon3'] = NULL;
        } else {
            $this->valF['su_trsf_shon3'] = $val['su_trsf_shon3'];
        }
        if (!is_numeric($val['su_trsf_shon4'])) {
            $this->valF['su_trsf_shon4'] = NULL;
        } else {
            $this->valF['su_trsf_shon4'] = $val['su_trsf_shon4'];
        }
        if (!is_numeric($val['su_trsf_shon5'])) {
            $this->valF['su_trsf_shon5'] = NULL;
        } else {
            $this->valF['su_trsf_shon5'] = $val['su_trsf_shon5'];
        }
        if (!is_numeric($val['su_trsf_shon6'])) {
            $this->valF['su_trsf_shon6'] = NULL;
        } else {
            $this->valF['su_trsf_shon6'] = $val['su_trsf_shon6'];
        }
        if (!is_numeric($val['su_trsf_shon7'])) {
            $this->valF['su_trsf_shon7'] = NULL;
        } else {
            $this->valF['su_trsf_shon7'] = $val['su_trsf_shon7'];
        }
        if (!is_numeric($val['su_trsf_shon8'])) {
            $this->valF['su_trsf_shon8'] = NULL;
        } else {
            $this->valF['su_trsf_shon8'] = $val['su_trsf_shon8'];
        }
        if (!is_numeric($val['su_trsf_shon9'])) {
            $this->valF['su_trsf_shon9'] = NULL;
        } else {
            $this->valF['su_trsf_shon9'] = $val['su_trsf_shon9'];
        }
        if (!is_numeric($val['su_chge_shon1'])) {
            $this->valF['su_chge_shon1'] = NULL;
        } else {
            $this->valF['su_chge_shon1'] = $val['su_chge_shon1'];
        }
        if (!is_numeric($val['su_chge_shon2'])) {
            $this->valF['su_chge_shon2'] = NULL;
        } else {
            $this->valF['su_chge_shon2'] = $val['su_chge_shon2'];
        }
        if (!is_numeric($val['su_chge_shon3'])) {
            $this->valF['su_chge_shon3'] = NULL;
        } else {
            $this->valF['su_chge_shon3'] = $val['su_chge_shon3'];
        }
        if (!is_numeric($val['su_chge_shon4'])) {
            $this->valF['su_chge_shon4'] = NULL;
        } else {
            $this->valF['su_chge_shon4'] = $val['su_chge_shon4'];
        }
        if (!is_numeric($val['su_chge_shon5'])) {
            $this->valF['su_chge_shon5'] = NULL;
        } else {
            $this->valF['su_chge_shon5'] = $val['su_chge_shon5'];
        }
        if (!is_numeric($val['su_chge_shon6'])) {
            $this->valF['su_chge_shon6'] = NULL;
        } else {
            $this->valF['su_chge_shon6'] = $val['su_chge_shon6'];
        }
        if (!is_numeric($val['su_chge_shon7'])) {
            $this->valF['su_chge_shon7'] = NULL;
        } else {
            $this->valF['su_chge_shon7'] = $val['su_chge_shon7'];
        }
        if (!is_numeric($val['su_chge_shon8'])) {
            $this->valF['su_chge_shon8'] = NULL;
        } else {
            $this->valF['su_chge_shon8'] = $val['su_chge_shon8'];
        }
        if (!is_numeric($val['su_chge_shon9'])) {
            $this->valF['su_chge_shon9'] = NULL;
        } else {
            $this->valF['su_chge_shon9'] = $val['su_chge_shon9'];
        }
        if (!is_numeric($val['su_demo_shon1'])) {
            $this->valF['su_demo_shon1'] = NULL;
        } else {
            $this->valF['su_demo_shon1'] = $val['su_demo_shon1'];
        }
        if (!is_numeric($val['su_demo_shon2'])) {
            $this->valF['su_demo_shon2'] = NULL;
        } else {
            $this->valF['su_demo_shon2'] = $val['su_demo_shon2'];
        }
        if (!is_numeric($val['su_demo_shon3'])) {
            $this->valF['su_demo_shon3'] = NULL;
        } else {
            $this->valF['su_demo_shon3'] = $val['su_demo_shon3'];
        }
        if (!is_numeric($val['su_demo_shon4'])) {
            $this->valF['su_demo_shon4'] = NULL;
        } else {
            $this->valF['su_demo_shon4'] = $val['su_demo_shon4'];
        }
        if (!is_numeric($val['su_demo_shon5'])) {
            $this->valF['su_demo_shon5'] = NULL;
        } else {
            $this->valF['su_demo_shon5'] = $val['su_demo_shon5'];
        }
        if (!is_numeric($val['su_demo_shon6'])) {
            $this->valF['su_demo_shon6'] = NULL;
        } else {
            $this->valF['su_demo_shon6'] = $val['su_demo_shon6'];
        }
        if (!is_numeric($val['su_demo_shon7'])) {
            $this->valF['su_demo_shon7'] = NULL;
        } else {
            $this->valF['su_demo_shon7'] = $val['su_demo_shon7'];
        }
        if (!is_numeric($val['su_demo_shon8'])) {
            $this->valF['su_demo_shon8'] = NULL;
        } else {
            $this->valF['su_demo_shon8'] = $val['su_demo_shon8'];
        }
        if (!is_numeric($val['su_demo_shon9'])) {
            $this->valF['su_demo_shon9'] = NULL;
        } else {
            $this->valF['su_demo_shon9'] = $val['su_demo_shon9'];
        }
        if (!is_numeric($val['su_sup_shon1'])) {
            $this->valF['su_sup_shon1'] = NULL;
        } else {
            $this->valF['su_sup_shon1'] = $val['su_sup_shon1'];
        }
        if (!is_numeric($val['su_sup_shon2'])) {
            $this->valF['su_sup_shon2'] = NULL;
        } else {
            $this->valF['su_sup_shon2'] = $val['su_sup_shon2'];
        }
        if (!is_numeric($val['su_sup_shon3'])) {
            $this->valF['su_sup_shon3'] = NULL;
        } else {
            $this->valF['su_sup_shon3'] = $val['su_sup_shon3'];
        }
        if (!is_numeric($val['su_sup_shon4'])) {
            $this->valF['su_sup_shon4'] = NULL;
        } else {
            $this->valF['su_sup_shon4'] = $val['su_sup_shon4'];
        }
        if (!is_numeric($val['su_sup_shon5'])) {
            $this->valF['su_sup_shon5'] = NULL;
        } else {
            $this->valF['su_sup_shon5'] = $val['su_sup_shon5'];
        }
        if (!is_numeric($val['su_sup_shon6'])) {
            $this->valF['su_sup_shon6'] = NULL;
        } else {
            $this->valF['su_sup_shon6'] = $val['su_sup_shon6'];
        }
        if (!is_numeric($val['su_sup_shon7'])) {
            $this->valF['su_sup_shon7'] = NULL;
        } else {
            $this->valF['su_sup_shon7'] = $val['su_sup_shon7'];
        }
        if (!is_numeric($val['su_sup_shon8'])) {
            $this->valF['su_sup_shon8'] = NULL;
        } else {
            $this->valF['su_sup_shon8'] = $val['su_sup_shon8'];
        }
        if (!is_numeric($val['su_sup_shon9'])) {
            $this->valF['su_sup_shon9'] = NULL;
        } else {
            $this->valF['su_sup_shon9'] = $val['su_sup_shon9'];
        }
        if (!is_numeric($val['su_tot_shon1'])) {
            $this->valF['su_tot_shon1'] = NULL;
        } else {
            $this->valF['su_tot_shon1'] = $val['su_tot_shon1'];
        }
        if (!is_numeric($val['su_tot_shon2'])) {
            $this->valF['su_tot_shon2'] = NULL;
        } else {
            $this->valF['su_tot_shon2'] = $val['su_tot_shon2'];
        }
        if (!is_numeric($val['su_tot_shon3'])) {
            $this->valF['su_tot_shon3'] = NULL;
        } else {
            $this->valF['su_tot_shon3'] = $val['su_tot_shon3'];
        }
        if (!is_numeric($val['su_tot_shon4'])) {
            $this->valF['su_tot_shon4'] = NULL;
        } else {
            $this->valF['su_tot_shon4'] = $val['su_tot_shon4'];
        }
        if (!is_numeric($val['su_tot_shon5'])) {
            $this->valF['su_tot_shon5'] = NULL;
        } else {
            $this->valF['su_tot_shon5'] = $val['su_tot_shon5'];
        }
        if (!is_numeric($val['su_tot_shon6'])) {
            $this->valF['su_tot_shon6'] = NULL;
        } else {
            $this->valF['su_tot_shon6'] = $val['su_tot_shon6'];
        }
        if (!is_numeric($val['su_tot_shon7'])) {
            $this->valF['su_tot_shon7'] = NULL;
        } else {
            $this->valF['su_tot_shon7'] = $val['su_tot_shon7'];
        }
        if (!is_numeric($val['su_tot_shon8'])) {
            $this->valF['su_tot_shon8'] = NULL;
        } else {
            $this->valF['su_tot_shon8'] = $val['su_tot_shon8'];
        }
        if (!is_numeric($val['su_tot_shon9'])) {
            $this->valF['su_tot_shon9'] = NULL;
        } else {
            $this->valF['su_tot_shon9'] = $val['su_tot_shon9'];
        }
        if (!is_numeric($val['su_avt_shon_tot'])) {
            $this->valF['su_avt_shon_tot'] = NULL;
        } else {
            $this->valF['su_avt_shon_tot'] = $val['su_avt_shon_tot'];
        }
        if (!is_numeric($val['su_cstr_shon_tot'])) {
            $this->valF['su_cstr_shon_tot'] = NULL;
        } else {
            $this->valF['su_cstr_shon_tot'] = $val['su_cstr_shon_tot'];
        }
        if (!is_numeric($val['su_trsf_shon_tot'])) {
            $this->valF['su_trsf_shon_tot'] = NULL;
        } else {
            $this->valF['su_trsf_shon_tot'] = $val['su_trsf_shon_tot'];
        }
        if (!is_numeric($val['su_chge_shon_tot'])) {
            $this->valF['su_chge_shon_tot'] = NULL;
        } else {
            $this->valF['su_chge_shon_tot'] = $val['su_chge_shon_tot'];
        }
        if (!is_numeric($val['su_demo_shon_tot'])) {
            $this->valF['su_demo_shon_tot'] = NULL;
        } else {
            $this->valF['su_demo_shon_tot'] = $val['su_demo_shon_tot'];
        }
        if (!is_numeric($val['su_sup_shon_tot'])) {
            $this->valF['su_sup_shon_tot'] = NULL;
        } else {
            $this->valF['su_sup_shon_tot'] = $val['su_sup_shon_tot'];
        }
        if (!is_numeric($val['su_tot_shon_tot'])) {
            $this->valF['su_tot_shon_tot'] = NULL;
        } else {
            $this->valF['su_tot_shon_tot'] = $val['su_tot_shon_tot'];
        }
            $this->valF['dm_constr_dates'] = $val['dm_constr_dates'];
        if ($val['dm_total'] == 1 || $val['dm_total'] == "t" || $val['dm_total'] == "Oui") {
            $this->valF['dm_total'] = true;
        } else {
            $this->valF['dm_total'] = false;
        }
        if ($val['dm_partiel'] == 1 || $val['dm_partiel'] == "t" || $val['dm_partiel'] == "Oui") {
            $this->valF['dm_partiel'] = true;
        } else {
            $this->valF['dm_partiel'] = false;
        }
            $this->valF['dm_projet_desc'] = $val['dm_projet_desc'];
        if (!is_numeric($val['dm_tot_log_nb'])) {
            $this->valF['dm_tot_log_nb'] = NULL;
        } else {
            $this->valF['dm_tot_log_nb'] = $val['dm_tot_log_nb'];
        }
        if (!is_numeric($val['tax_surf_tot'])) {
            $this->valF['tax_surf_tot'] = NULL;
        } else {
            $this->valF['tax_surf_tot'] = $val['tax_surf_tot'];
        }
        if (!is_numeric($val['tax_surf'])) {
            $this->valF['tax_surf'] = NULL;
        } else {
            $this->valF['tax_surf'] = $val['tax_surf'];
        }
        if (!is_numeric($val['tax_surf_suppr_mod'])) {
            $this->valF['tax_surf_suppr_mod'] = NULL;
        } else {
            $this->valF['tax_surf_suppr_mod'] = $val['tax_surf_suppr_mod'];
        }
        if (!is_numeric($val['tax_su_princ_log_nb1'])) {
            $this->valF['tax_su_princ_log_nb1'] = NULL;
        } else {
            $this->valF['tax_su_princ_log_nb1'] = $val['tax_su_princ_log_nb1'];
        }
        if (!is_numeric($val['tax_su_princ_log_nb2'])) {
            $this->valF['tax_su_princ_log_nb2'] = NULL;
        } else {
            $this->valF['tax_su_princ_log_nb2'] = $val['tax_su_princ_log_nb2'];
        }
        if (!is_numeric($val['tax_su_princ_log_nb3'])) {
            $this->valF['tax_su_princ_log_nb3'] = NULL;
        } else {
            $this->valF['tax_su_princ_log_nb3'] = $val['tax_su_princ_log_nb3'];
        }
        if (!is_numeric($val['tax_su_princ_log_nb4'])) {
            $this->valF['tax_su_princ_log_nb4'] = NULL;
        } else {
            $this->valF['tax_su_princ_log_nb4'] = $val['tax_su_princ_log_nb4'];
        }
        if (!is_numeric($val['tax_su_princ_log_nb_tot1'])) {
            $this->valF['tax_su_princ_log_nb_tot1'] = NULL;
        } else {
            $this->valF['tax_su_princ_log_nb_tot1'] = $val['tax_su_princ_log_nb_tot1'];
        }
        if (!is_numeric($val['tax_su_princ_log_nb_tot2'])) {
            $this->valF['tax_su_princ_log_nb_tot2'] = NULL;
        } else {
            $this->valF['tax_su_princ_log_nb_tot2'] = $val['tax_su_princ_log_nb_tot2'];
        }
        if (!is_numeric($val['tax_su_princ_log_nb_tot3'])) {
            $this->valF['tax_su_princ_log_nb_tot3'] = NULL;
        } else {
            $this->valF['tax_su_princ_log_nb_tot3'] = $val['tax_su_princ_log_nb_tot3'];
        }
        if (!is_numeric($val['tax_su_princ_log_nb_tot4'])) {
            $this->valF['tax_su_princ_log_nb_tot4'] = NULL;
        } else {
            $this->valF['tax_su_princ_log_nb_tot4'] = $val['tax_su_princ_log_nb_tot4'];
        }
        if (!is_numeric($val['tax_su_princ_surf1'])) {
            $this->valF['tax_su_princ_surf1'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf1'] = $val['tax_su_princ_surf1'];
        }
        if (!is_numeric($val['tax_su_princ_surf2'])) {
            $this->valF['tax_su_princ_surf2'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf2'] = $val['tax_su_princ_surf2'];
        }
        if (!is_numeric($val['tax_su_princ_surf3'])) {
            $this->valF['tax_su_princ_surf3'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf3'] = $val['tax_su_princ_surf3'];
        }
        if (!is_numeric($val['tax_su_princ_surf4'])) {
            $this->valF['tax_su_princ_surf4'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf4'] = $val['tax_su_princ_surf4'];
        }
        if (!is_numeric($val['tax_su_princ_surf_sup1'])) {
            $this->valF['tax_su_princ_surf_sup1'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf_sup1'] = $val['tax_su_princ_surf_sup1'];
        }
        if (!is_numeric($val['tax_su_princ_surf_sup2'])) {
            $this->valF['tax_su_princ_surf_sup2'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf_sup2'] = $val['tax_su_princ_surf_sup2'];
        }
        if (!is_numeric($val['tax_su_princ_surf_sup3'])) {
            $this->valF['tax_su_princ_surf_sup3'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf_sup3'] = $val['tax_su_princ_surf_sup3'];
        }
        if (!is_numeric($val['tax_su_princ_surf_sup4'])) {
            $this->valF['tax_su_princ_surf_sup4'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf_sup4'] = $val['tax_su_princ_surf_sup4'];
        }
        if (!is_numeric($val['tax_su_heber_log_nb1'])) {
            $this->valF['tax_su_heber_log_nb1'] = NULL;
        } else {
            $this->valF['tax_su_heber_log_nb1'] = $val['tax_su_heber_log_nb1'];
        }
        if (!is_numeric($val['tax_su_heber_log_nb2'])) {
            $this->valF['tax_su_heber_log_nb2'] = NULL;
        } else {
            $this->valF['tax_su_heber_log_nb2'] = $val['tax_su_heber_log_nb2'];
        }
        if (!is_numeric($val['tax_su_heber_log_nb3'])) {
            $this->valF['tax_su_heber_log_nb3'] = NULL;
        } else {
            $this->valF['tax_su_heber_log_nb3'] = $val['tax_su_heber_log_nb3'];
        }
        if (!is_numeric($val['tax_su_heber_log_nb_tot1'])) {
            $this->valF['tax_su_heber_log_nb_tot1'] = NULL;
        } else {
            $this->valF['tax_su_heber_log_nb_tot1'] = $val['tax_su_heber_log_nb_tot1'];
        }
        if (!is_numeric($val['tax_su_heber_log_nb_tot2'])) {
            $this->valF['tax_su_heber_log_nb_tot2'] = NULL;
        } else {
            $this->valF['tax_su_heber_log_nb_tot2'] = $val['tax_su_heber_log_nb_tot2'];
        }
        if (!is_numeric($val['tax_su_heber_log_nb_tot3'])) {
            $this->valF['tax_su_heber_log_nb_tot3'] = NULL;
        } else {
            $this->valF['tax_su_heber_log_nb_tot3'] = $val['tax_su_heber_log_nb_tot3'];
        }
        if (!is_numeric($val['tax_su_heber_surf1'])) {
            $this->valF['tax_su_heber_surf1'] = NULL;
        } else {
            $this->valF['tax_su_heber_surf1'] = $val['tax_su_heber_surf1'];
        }
        if (!is_numeric($val['tax_su_heber_surf2'])) {
            $this->valF['tax_su_heber_surf2'] = NULL;
        } else {
            $this->valF['tax_su_heber_surf2'] = $val['tax_su_heber_surf2'];
        }
        if (!is_numeric($val['tax_su_heber_surf3'])) {
            $this->valF['tax_su_heber_surf3'] = NULL;
        } else {
            $this->valF['tax_su_heber_surf3'] = $val['tax_su_heber_surf3'];
        }
        if (!is_numeric($val['tax_su_heber_surf_sup1'])) {
            $this->valF['tax_su_heber_surf_sup1'] = NULL;
        } else {
            $this->valF['tax_su_heber_surf_sup1'] = $val['tax_su_heber_surf_sup1'];
        }
        if (!is_numeric($val['tax_su_heber_surf_sup2'])) {
            $this->valF['tax_su_heber_surf_sup2'] = NULL;
        } else {
            $this->valF['tax_su_heber_surf_sup2'] = $val['tax_su_heber_surf_sup2'];
        }
        if (!is_numeric($val['tax_su_heber_surf_sup3'])) {
            $this->valF['tax_su_heber_surf_sup3'] = NULL;
        } else {
            $this->valF['tax_su_heber_surf_sup3'] = $val['tax_su_heber_surf_sup3'];
        }
        if (!is_numeric($val['tax_su_secon_log_nb'])) {
            $this->valF['tax_su_secon_log_nb'] = NULL;
        } else {
            $this->valF['tax_su_secon_log_nb'] = $val['tax_su_secon_log_nb'];
        }
        if (!is_numeric($val['tax_su_tot_log_nb'])) {
            $this->valF['tax_su_tot_log_nb'] = NULL;
        } else {
            $this->valF['tax_su_tot_log_nb'] = $val['tax_su_tot_log_nb'];
        }
        if (!is_numeric($val['tax_su_secon_log_nb_tot'])) {
            $this->valF['tax_su_secon_log_nb_tot'] = NULL;
        } else {
            $this->valF['tax_su_secon_log_nb_tot'] = $val['tax_su_secon_log_nb_tot'];
        }
        if (!is_numeric($val['tax_su_tot_log_nb_tot'])) {
            $this->valF['tax_su_tot_log_nb_tot'] = NULL;
        } else {
            $this->valF['tax_su_tot_log_nb_tot'] = $val['tax_su_tot_log_nb_tot'];
        }
        if (!is_numeric($val['tax_su_secon_surf'])) {
            $this->valF['tax_su_secon_surf'] = NULL;
        } else {
            $this->valF['tax_su_secon_surf'] = $val['tax_su_secon_surf'];
        }
        if (!is_numeric($val['tax_su_tot_surf'])) {
            $this->valF['tax_su_tot_surf'] = NULL;
        } else {
            $this->valF['tax_su_tot_surf'] = $val['tax_su_tot_surf'];
        }
        if (!is_numeric($val['tax_su_secon_surf_sup'])) {
            $this->valF['tax_su_secon_surf_sup'] = NULL;
        } else {
            $this->valF['tax_su_secon_surf_sup'] = $val['tax_su_secon_surf_sup'];
        }
        if (!is_numeric($val['tax_su_tot_surf_sup'])) {
            $this->valF['tax_su_tot_surf_sup'] = NULL;
        } else {
            $this->valF['tax_su_tot_surf_sup'] = $val['tax_su_tot_surf_sup'];
        }
        if ($val['tax_ext_pret'] == 1 || $val['tax_ext_pret'] == "t" || $val['tax_ext_pret'] == "Oui") {
            $this->valF['tax_ext_pret'] = true;
        } else {
            $this->valF['tax_ext_pret'] = false;
        }
            $this->valF['tax_ext_desc'] = $val['tax_ext_desc'];
        if (!is_numeric($val['tax_surf_tax_exist_cons'])) {
            $this->valF['tax_surf_tax_exist_cons'] = NULL;
        } else {
            $this->valF['tax_surf_tax_exist_cons'] = $val['tax_surf_tax_exist_cons'];
        }
        if (!is_numeric($val['tax_log_exist_nb'])) {
            $this->valF['tax_log_exist_nb'] = NULL;
        } else {
            $this->valF['tax_log_exist_nb'] = $val['tax_log_exist_nb'];
        }
        if (!is_numeric($val['tax_am_statio_ext'])) {
            $this->valF['tax_am_statio_ext'] = NULL;
        } else {
            $this->valF['tax_am_statio_ext'] = $val['tax_am_statio_ext'];
        }
        if (!is_numeric($val['tax_sup_bass_pisc'])) {
            $this->valF['tax_sup_bass_pisc'] = NULL;
        } else {
            $this->valF['tax_sup_bass_pisc'] = $val['tax_sup_bass_pisc'];
        }
        if (!is_numeric($val['tax_empl_ten_carav_mobil_nb'])) {
            $this->valF['tax_empl_ten_carav_mobil_nb'] = NULL;
        } else {
            $this->valF['tax_empl_ten_carav_mobil_nb'] = $val['tax_empl_ten_carav_mobil_nb'];
        }
        if (!is_numeric($val['tax_empl_hll_nb'])) {
            $this->valF['tax_empl_hll_nb'] = NULL;
        } else {
            $this->valF['tax_empl_hll_nb'] = $val['tax_empl_hll_nb'];
        }
        if (!is_numeric($val['tax_eol_haut_nb'])) {
            $this->valF['tax_eol_haut_nb'] = NULL;
        } else {
            $this->valF['tax_eol_haut_nb'] = $val['tax_eol_haut_nb'];
        }
        if (!is_numeric($val['tax_pann_volt_sup'])) {
            $this->valF['tax_pann_volt_sup'] = NULL;
        } else {
            $this->valF['tax_pann_volt_sup'] = $val['tax_pann_volt_sup'];
        }
        if (!is_numeric($val['tax_am_statio_ext_sup'])) {
            $this->valF['tax_am_statio_ext_sup'] = NULL;
        } else {
            $this->valF['tax_am_statio_ext_sup'] = $val['tax_am_statio_ext_sup'];
        }
        if (!is_numeric($val['tax_sup_bass_pisc_sup'])) {
            $this->valF['tax_sup_bass_pisc_sup'] = NULL;
        } else {
            $this->valF['tax_sup_bass_pisc_sup'] = $val['tax_sup_bass_pisc_sup'];
        }
        if (!is_numeric($val['tax_empl_ten_carav_mobil_nb_sup'])) {
            $this->valF['tax_empl_ten_carav_mobil_nb_sup'] = NULL;
        } else {
            $this->valF['tax_empl_ten_carav_mobil_nb_sup'] = $val['tax_empl_ten_carav_mobil_nb_sup'];
        }
        if (!is_numeric($val['tax_empl_hll_nb_sup'])) {
            $this->valF['tax_empl_hll_nb_sup'] = NULL;
        } else {
            $this->valF['tax_empl_hll_nb_sup'] = $val['tax_empl_hll_nb_sup'];
        }
        if (!is_numeric($val['tax_eol_haut_nb_sup'])) {
            $this->valF['tax_eol_haut_nb_sup'] = NULL;
        } else {
            $this->valF['tax_eol_haut_nb_sup'] = $val['tax_eol_haut_nb_sup'];
        }
        if (!is_numeric($val['tax_pann_volt_sup_sup'])) {
            $this->valF['tax_pann_volt_sup_sup'] = NULL;
        } else {
            $this->valF['tax_pann_volt_sup_sup'] = $val['tax_pann_volt_sup_sup'];
        }
        if ($val['tax_trx_presc_ppr'] == 1 || $val['tax_trx_presc_ppr'] == "t" || $val['tax_trx_presc_ppr'] == "Oui") {
            $this->valF['tax_trx_presc_ppr'] = true;
        } else {
            $this->valF['tax_trx_presc_ppr'] = false;
        }
        if ($val['tax_monu_hist'] == 1 || $val['tax_monu_hist'] == "t" || $val['tax_monu_hist'] == "Oui") {
            $this->valF['tax_monu_hist'] = true;
        } else {
            $this->valF['tax_monu_hist'] = false;
        }
        if (!is_numeric($val['tax_comm_nb'])) {
            $this->valF['tax_comm_nb'] = NULL;
        } else {
            $this->valF['tax_comm_nb'] = $val['tax_comm_nb'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf1'])) {
            $this->valF['tax_su_non_habit_surf1'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf1'] = $val['tax_su_non_habit_surf1'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf2'])) {
            $this->valF['tax_su_non_habit_surf2'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf2'] = $val['tax_su_non_habit_surf2'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf3'])) {
            $this->valF['tax_su_non_habit_surf3'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf3'] = $val['tax_su_non_habit_surf3'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf4'])) {
            $this->valF['tax_su_non_habit_surf4'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf4'] = $val['tax_su_non_habit_surf4'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf5'])) {
            $this->valF['tax_su_non_habit_surf5'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf5'] = $val['tax_su_non_habit_surf5'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf6'])) {
            $this->valF['tax_su_non_habit_surf6'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf6'] = $val['tax_su_non_habit_surf6'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf7'])) {
            $this->valF['tax_su_non_habit_surf7'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf7'] = $val['tax_su_non_habit_surf7'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_sup1'])) {
            $this->valF['tax_su_non_habit_surf_sup1'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_sup1'] = $val['tax_su_non_habit_surf_sup1'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_sup2'])) {
            $this->valF['tax_su_non_habit_surf_sup2'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_sup2'] = $val['tax_su_non_habit_surf_sup2'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_sup3'])) {
            $this->valF['tax_su_non_habit_surf_sup3'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_sup3'] = $val['tax_su_non_habit_surf_sup3'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_sup4'])) {
            $this->valF['tax_su_non_habit_surf_sup4'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_sup4'] = $val['tax_su_non_habit_surf_sup4'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_sup5'])) {
            $this->valF['tax_su_non_habit_surf_sup5'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_sup5'] = $val['tax_su_non_habit_surf_sup5'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_sup6'])) {
            $this->valF['tax_su_non_habit_surf_sup6'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_sup6'] = $val['tax_su_non_habit_surf_sup6'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_sup7'])) {
            $this->valF['tax_su_non_habit_surf_sup7'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_sup7'] = $val['tax_su_non_habit_surf_sup7'];
        }
        if ($val['vsd_surf_planch_smd'] == 1 || $val['vsd_surf_planch_smd'] == "t" || $val['vsd_surf_planch_smd'] == "Oui") {
            $this->valF['vsd_surf_planch_smd'] = true;
        } else {
            $this->valF['vsd_surf_planch_smd'] = false;
        }
        if (!is_numeric($val['vsd_unit_fonc_sup'])) {
            $this->valF['vsd_unit_fonc_sup'] = NULL;
        } else {
            $this->valF['vsd_unit_fonc_sup'] = $val['vsd_unit_fonc_sup'];
        }
        if (!is_numeric($val['vsd_unit_fonc_constr_sup'])) {
            $this->valF['vsd_unit_fonc_constr_sup'] = NULL;
        } else {
            $this->valF['vsd_unit_fonc_constr_sup'] = $val['vsd_unit_fonc_constr_sup'];
        }
        if (!is_numeric($val['vsd_val_terr'])) {
            $this->valF['vsd_val_terr'] = NULL;
        } else {
            $this->valF['vsd_val_terr'] = $val['vsd_val_terr'];
        }
        if (!is_numeric($val['vsd_const_sxist_non_dem_surf'])) {
            $this->valF['vsd_const_sxist_non_dem_surf'] = NULL;
        } else {
            $this->valF['vsd_const_sxist_non_dem_surf'] = $val['vsd_const_sxist_non_dem_surf'];
        }
        if ($val['vsd_rescr_fisc'] != "") {
            $this->valF['vsd_rescr_fisc'] = $this->dateDB($val['vsd_rescr_fisc']);
        } else {
            $this->valF['vsd_rescr_fisc'] = NULL;
        }
        if (!is_numeric($val['pld_val_terr'])) {
            $this->valF['pld_val_terr'] = NULL;
        } else {
            $this->valF['pld_val_terr'] = $val['pld_val_terr'];
        }
        if ($val['pld_const_exist_dem'] == 1 || $val['pld_const_exist_dem'] == "t" || $val['pld_const_exist_dem'] == "Oui") {
            $this->valF['pld_const_exist_dem'] = true;
        } else {
            $this->valF['pld_const_exist_dem'] = false;
        }
        if (!is_numeric($val['pld_const_exist_dem_surf'])) {
            $this->valF['pld_const_exist_dem_surf'] = NULL;
        } else {
            $this->valF['pld_const_exist_dem_surf'] = $val['pld_const_exist_dem_surf'];
        }
        if ($val['code_cnil'] == 1 || $val['code_cnil'] == "t" || $val['code_cnil'] == "Oui") {
            $this->valF['code_cnil'] = true;
        } else {
            $this->valF['code_cnil'] = false;
        }
        if ($val['terr_juri_titul'] == "") {
            $this->valF['terr_juri_titul'] = NULL;
        } else {
            $this->valF['terr_juri_titul'] = $val['terr_juri_titul'];
        }
        if ($val['terr_juri_lot'] == "") {
            $this->valF['terr_juri_lot'] = NULL;
        } else {
            $this->valF['terr_juri_lot'] = $val['terr_juri_lot'];
        }
        if ($val['terr_juri_zac'] == "") {
            $this->valF['terr_juri_zac'] = NULL;
        } else {
            $this->valF['terr_juri_zac'] = $val['terr_juri_zac'];
        }
        if ($val['terr_juri_afu'] == "") {
            $this->valF['terr_juri_afu'] = NULL;
        } else {
            $this->valF['terr_juri_afu'] = $val['terr_juri_afu'];
        }
        if ($val['terr_juri_pup'] == "") {
            $this->valF['terr_juri_pup'] = NULL;
        } else {
            $this->valF['terr_juri_pup'] = $val['terr_juri_pup'];
        }
        if ($val['terr_juri_oin'] == "") {
            $this->valF['terr_juri_oin'] = NULL;
        } else {
            $this->valF['terr_juri_oin'] = $val['terr_juri_oin'];
        }
            $this->valF['terr_juri_desc'] = $val['terr_juri_desc'];
        if (!is_numeric($val['terr_div_surf_etab'])) {
            $this->valF['terr_div_surf_etab'] = NULL;
        } else {
            $this->valF['terr_div_surf_etab'] = $val['terr_div_surf_etab'];
        }
        if (!is_numeric($val['terr_div_surf_av_div'])) {
            $this->valF['terr_div_surf_av_div'] = NULL;
        } else {
            $this->valF['terr_div_surf_av_div'] = $val['terr_div_surf_av_div'];
        }
        if ($val['doc_date'] != "") {
            $this->valF['doc_date'] = $this->dateDB($val['doc_date']);
        } else {
            $this->valF['doc_date'] = NULL;
        }
        if ($val['doc_tot_trav'] == 1 || $val['doc_tot_trav'] == "t" || $val['doc_tot_trav'] == "Oui") {
            $this->valF['doc_tot_trav'] = true;
        } else {
            $this->valF['doc_tot_trav'] = false;
        }
        if ($val['doc_tranche_trav'] == 1 || $val['doc_tranche_trav'] == "t" || $val['doc_tranche_trav'] == "Oui") {
            $this->valF['doc_tranche_trav'] = true;
        } else {
            $this->valF['doc_tranche_trav'] = false;
        }
            $this->valF['doc_tranche_trav_desc'] = $val['doc_tranche_trav_desc'];
        if (!is_numeric($val['doc_surf'])) {
            $this->valF['doc_surf'] = NULL;
        } else {
            $this->valF['doc_surf'] = $val['doc_surf'];
        }
        if (!is_numeric($val['doc_nb_log'])) {
            $this->valF['doc_nb_log'] = NULL;
        } else {
            $this->valF['doc_nb_log'] = $val['doc_nb_log'];
        }
        if (!is_numeric($val['doc_nb_log_indiv'])) {
            $this->valF['doc_nb_log_indiv'] = NULL;
        } else {
            $this->valF['doc_nb_log_indiv'] = $val['doc_nb_log_indiv'];
        }
        if (!is_numeric($val['doc_nb_log_coll'])) {
            $this->valF['doc_nb_log_coll'] = NULL;
        } else {
            $this->valF['doc_nb_log_coll'] = $val['doc_nb_log_coll'];
        }
        if (!is_numeric($val['doc_nb_log_lls'])) {
            $this->valF['doc_nb_log_lls'] = NULL;
        } else {
            $this->valF['doc_nb_log_lls'] = $val['doc_nb_log_lls'];
        }
        if (!is_numeric($val['doc_nb_log_aa'])) {
            $this->valF['doc_nb_log_aa'] = NULL;
        } else {
            $this->valF['doc_nb_log_aa'] = $val['doc_nb_log_aa'];
        }
        if (!is_numeric($val['doc_nb_log_ptz'])) {
            $this->valF['doc_nb_log_ptz'] = NULL;
        } else {
            $this->valF['doc_nb_log_ptz'] = $val['doc_nb_log_ptz'];
        }
        if (!is_numeric($val['doc_nb_log_autre'])) {
            $this->valF['doc_nb_log_autre'] = NULL;
        } else {
            $this->valF['doc_nb_log_autre'] = $val['doc_nb_log_autre'];
        }
        if ($val['daact_date'] != "") {
            $this->valF['daact_date'] = $this->dateDB($val['daact_date']);
        } else {
            $this->valF['daact_date'] = NULL;
        }
        if ($val['daact_date_chgmt_dest'] != "") {
            $this->valF['daact_date_chgmt_dest'] = $this->dateDB($val['daact_date_chgmt_dest']);
        } else {
            $this->valF['daact_date_chgmt_dest'] = NULL;
        }
        if ($val['daact_tot_trav'] == 1 || $val['daact_tot_trav'] == "t" || $val['daact_tot_trav'] == "Oui") {
            $this->valF['daact_tot_trav'] = true;
        } else {
            $this->valF['daact_tot_trav'] = false;
        }
        if ($val['daact_tranche_trav'] == 1 || $val['daact_tranche_trav'] == "t" || $val['daact_tranche_trav'] == "Oui") {
            $this->valF['daact_tranche_trav'] = true;
        } else {
            $this->valF['daact_tranche_trav'] = false;
        }
            $this->valF['daact_tranche_trav_desc'] = $val['daact_tranche_trav_desc'];
        if (!is_numeric($val['daact_surf'])) {
            $this->valF['daact_surf'] = NULL;
        } else {
            $this->valF['daact_surf'] = $val['daact_surf'];
        }
        if (!is_numeric($val['daact_nb_log'])) {
            $this->valF['daact_nb_log'] = NULL;
        } else {
            $this->valF['daact_nb_log'] = $val['daact_nb_log'];
        }
        if (!is_numeric($val['daact_nb_log_indiv'])) {
            $this->valF['daact_nb_log_indiv'] = NULL;
        } else {
            $this->valF['daact_nb_log_indiv'] = $val['daact_nb_log_indiv'];
        }
        if (!is_numeric($val['daact_nb_log_coll'])) {
            $this->valF['daact_nb_log_coll'] = NULL;
        } else {
            $this->valF['daact_nb_log_coll'] = $val['daact_nb_log_coll'];
        }
        if (!is_numeric($val['daact_nb_log_lls'])) {
            $this->valF['daact_nb_log_lls'] = NULL;
        } else {
            $this->valF['daact_nb_log_lls'] = $val['daact_nb_log_lls'];
        }
        if (!is_numeric($val['daact_nb_log_aa'])) {
            $this->valF['daact_nb_log_aa'] = NULL;
        } else {
            $this->valF['daact_nb_log_aa'] = $val['daact_nb_log_aa'];
        }
        if (!is_numeric($val['daact_nb_log_ptz'])) {
            $this->valF['daact_nb_log_ptz'] = NULL;
        } else {
            $this->valF['daact_nb_log_ptz'] = $val['daact_nb_log_ptz'];
        }
        if (!is_numeric($val['daact_nb_log_autre'])) {
            $this->valF['daact_nb_log_autre'] = NULL;
        } else {
            $this->valF['daact_nb_log_autre'] = $val['daact_nb_log_autre'];
        }
        if ($val['dossier_autorisation'] == "") {
            $this->valF['dossier_autorisation'] = NULL;
        } else {
            $this->valF['dossier_autorisation'] = $val['dossier_autorisation'];
        }
        if ($val['am_div_mun'] == 1 || $val['am_div_mun'] == "t" || $val['am_div_mun'] == "Oui") {
            $this->valF['am_div_mun'] = true;
        } else {
            $this->valF['am_div_mun'] = false;
        }
        if ($val['co_perf_energ'] == "") {
            $this->valF['co_perf_energ'] = NULL;
        } else {
            $this->valF['co_perf_energ'] = $val['co_perf_energ'];
        }
        if (!is_numeric($val['architecte'])) {
            $this->valF['architecte'] = NULL;
        } else {
            $this->valF['architecte'] = $val['architecte'];
        }
        if ($val['co_statio_avt_shob'] == "") {
            $this->valF['co_statio_avt_shob'] = NULL;
        } else {
            $this->valF['co_statio_avt_shob'] = $val['co_statio_avt_shob'];
        }
        if ($val['co_statio_apr_shob'] == "") {
            $this->valF['co_statio_apr_shob'] = NULL;
        } else {
            $this->valF['co_statio_apr_shob'] = $val['co_statio_apr_shob'];
        }
        if ($val['co_statio_avt_surf'] == "") {
            $this->valF['co_statio_avt_surf'] = NULL;
        } else {
            $this->valF['co_statio_avt_surf'] = $val['co_statio_avt_surf'];
        }
        if ($val['co_statio_apr_surf'] == "") {
            $this->valF['co_statio_apr_surf'] = NULL;
        } else {
            $this->valF['co_statio_apr_surf'] = $val['co_statio_apr_surf'];
        }
        if ($val['co_trx_amgt'] == "") {
            $this->valF['co_trx_amgt'] = NULL;
        } else {
            $this->valF['co_trx_amgt'] = $val['co_trx_amgt'];
        }
        if ($val['co_modif_aspect'] == "") {
            $this->valF['co_modif_aspect'] = NULL;
        } else {
            $this->valF['co_modif_aspect'] = $val['co_modif_aspect'];
        }
        if ($val['co_modif_struct'] == "") {
            $this->valF['co_modif_struct'] = NULL;
        } else {
            $this->valF['co_modif_struct'] = $val['co_modif_struct'];
        }
        if ($val['co_ouvr_elec'] == 1 || $val['co_ouvr_elec'] == "t" || $val['co_ouvr_elec'] == "Oui") {
            $this->valF['co_ouvr_elec'] = true;
        } else {
            $this->valF['co_ouvr_elec'] = false;
        }
        if ($val['co_ouvr_infra'] == 1 || $val['co_ouvr_infra'] == "t" || $val['co_ouvr_infra'] == "Oui") {
            $this->valF['co_ouvr_infra'] = true;
        } else {
            $this->valF['co_ouvr_infra'] = false;
        }
        if ($val['co_trx_imm'] == "") {
            $this->valF['co_trx_imm'] = NULL;
        } else {
            $this->valF['co_trx_imm'] = $val['co_trx_imm'];
        }
        if ($val['co_cstr_shob'] == "") {
            $this->valF['co_cstr_shob'] = NULL;
        } else {
            $this->valF['co_cstr_shob'] = $val['co_cstr_shob'];
        }
        if ($val['am_voyage_deb'] == "") {
            $this->valF['am_voyage_deb'] = NULL;
        } else {
            $this->valF['am_voyage_deb'] = $val['am_voyage_deb'];
        }
        if ($val['am_voyage_fin'] == "") {
            $this->valF['am_voyage_fin'] = NULL;
        } else {
            $this->valF['am_voyage_fin'] = $val['am_voyage_fin'];
        }
        if ($val['am_modif_amgt'] == "") {
            $this->valF['am_modif_amgt'] = NULL;
        } else {
            $this->valF['am_modif_amgt'] = $val['am_modif_amgt'];
        }
        if ($val['am_lot_max_shob'] == "") {
            $this->valF['am_lot_max_shob'] = NULL;
        } else {
            $this->valF['am_lot_max_shob'] = $val['am_lot_max_shob'];
        }
        if ($val['mod_desc'] == "") {
            $this->valF['mod_desc'] = NULL;
        } else {
            $this->valF['mod_desc'] = $val['mod_desc'];
        }
        if ($val['tr_total'] == "") {
            $this->valF['tr_total'] = NULL;
        } else {
            $this->valF['tr_total'] = $val['tr_total'];
        }
        if ($val['tr_partiel'] == "") {
            $this->valF['tr_partiel'] = NULL;
        } else {
            $this->valF['tr_partiel'] = $val['tr_partiel'];
        }
        if ($val['tr_desc'] == "") {
            $this->valF['tr_desc'] = NULL;
        } else {
            $this->valF['tr_desc'] = $val['tr_desc'];
        }
        if ($val['avap_co_elt_pro'] == 1 || $val['avap_co_elt_pro'] == "t" || $val['avap_co_elt_pro'] == "Oui") {
            $this->valF['avap_co_elt_pro'] = true;
        } else {
            $this->valF['avap_co_elt_pro'] = false;
        }
        if ($val['avap_nouv_haut_surf'] == 1 || $val['avap_nouv_haut_surf'] == "t" || $val['avap_nouv_haut_surf'] == "Oui") {
            $this->valF['avap_nouv_haut_surf'] = true;
        } else {
            $this->valF['avap_nouv_haut_surf'] = false;
        }
        if ($val['avap_co_clot'] == "") {
            $this->valF['avap_co_clot'] = NULL;
        } else {
            $this->valF['avap_co_clot'] = $val['avap_co_clot'];
        }
        if ($val['avap_aut_coup_aba_arb'] == "") {
            $this->valF['avap_aut_coup_aba_arb'] = NULL;
        } else {
            $this->valF['avap_aut_coup_aba_arb'] = $val['avap_aut_coup_aba_arb'];
        }
        if ($val['avap_ouv_infra'] == "") {
            $this->valF['avap_ouv_infra'] = NULL;
        } else {
            $this->valF['avap_ouv_infra'] = $val['avap_ouv_infra'];
        }
        if ($val['avap_aut_inst_mob'] == "") {
            $this->valF['avap_aut_inst_mob'] = NULL;
        } else {
            $this->valF['avap_aut_inst_mob'] = $val['avap_aut_inst_mob'];
        }
        if ($val['avap_aut_plant'] == "") {
            $this->valF['avap_aut_plant'] = NULL;
        } else {
            $this->valF['avap_aut_plant'] = $val['avap_aut_plant'];
        }
        if ($val['avap_aut_auv_elec'] == "") {
            $this->valF['avap_aut_auv_elec'] = NULL;
        } else {
            $this->valF['avap_aut_auv_elec'] = $val['avap_aut_auv_elec'];
        }
        if ($val['tax_dest_loc_tr'] == "") {
            $this->valF['tax_dest_loc_tr'] = NULL;
        } else {
            $this->valF['tax_dest_loc_tr'] = $val['tax_dest_loc_tr'];
        }
            $this->valF['ope_proj_desc'] = $val['ope_proj_desc'];
        if (!is_numeric($val['tax_surf_tot_cstr'])) {
            $this->valF['tax_surf_tot_cstr'] = NULL;
        } else {
            $this->valF['tax_surf_tot_cstr'] = $val['tax_surf_tot_cstr'];
        }
        if (!is_numeric($val['cerfa'])) {
            $this->valF['cerfa'] = ""; // -> requis
        } else {
            $this->valF['cerfa'] = $val['cerfa'];
        }
        if (!is_numeric($val['tax_surf_loc_stat'])) {
            $this->valF['tax_surf_loc_stat'] = NULL;
        } else {
            $this->valF['tax_surf_loc_stat'] = $val['tax_surf_loc_stat'];
        }
        if (!is_numeric($val['tax_su_princ_surf_stat1'])) {
            $this->valF['tax_su_princ_surf_stat1'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf_stat1'] = $val['tax_su_princ_surf_stat1'];
        }
        if (!is_numeric($val['tax_su_princ_surf_stat2'])) {
            $this->valF['tax_su_princ_surf_stat2'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf_stat2'] = $val['tax_su_princ_surf_stat2'];
        }
        if (!is_numeric($val['tax_su_princ_surf_stat3'])) {
            $this->valF['tax_su_princ_surf_stat3'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf_stat3'] = $val['tax_su_princ_surf_stat3'];
        }
        if (!is_numeric($val['tax_su_princ_surf_stat4'])) {
            $this->valF['tax_su_princ_surf_stat4'] = NULL;
        } else {
            $this->valF['tax_su_princ_surf_stat4'] = $val['tax_su_princ_surf_stat4'];
        }
        if (!is_numeric($val['tax_su_secon_surf_stat'])) {
            $this->valF['tax_su_secon_surf_stat'] = NULL;
        } else {
            $this->valF['tax_su_secon_surf_stat'] = $val['tax_su_secon_surf_stat'];
        }
        if (!is_numeric($val['tax_su_heber_surf_stat1'])) {
            $this->valF['tax_su_heber_surf_stat1'] = NULL;
        } else {
            $this->valF['tax_su_heber_surf_stat1'] = $val['tax_su_heber_surf_stat1'];
        }
        if (!is_numeric($val['tax_su_heber_surf_stat2'])) {
            $this->valF['tax_su_heber_surf_stat2'] = NULL;
        } else {
            $this->valF['tax_su_heber_surf_stat2'] = $val['tax_su_heber_surf_stat2'];
        }
        if (!is_numeric($val['tax_su_heber_surf_stat3'])) {
            $this->valF['tax_su_heber_surf_stat3'] = NULL;
        } else {
            $this->valF['tax_su_heber_surf_stat3'] = $val['tax_su_heber_surf_stat3'];
        }
        if (!is_numeric($val['tax_su_tot_surf_stat'])) {
            $this->valF['tax_su_tot_surf_stat'] = NULL;
        } else {
            $this->valF['tax_su_tot_surf_stat'] = $val['tax_su_tot_surf_stat'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_stat1'])) {
            $this->valF['tax_su_non_habit_surf_stat1'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_stat1'] = $val['tax_su_non_habit_surf_stat1'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_stat2'])) {
            $this->valF['tax_su_non_habit_surf_stat2'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_stat2'] = $val['tax_su_non_habit_surf_stat2'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_stat3'])) {
            $this->valF['tax_su_non_habit_surf_stat3'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_stat3'] = $val['tax_su_non_habit_surf_stat3'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_stat4'])) {
            $this->valF['tax_su_non_habit_surf_stat4'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_stat4'] = $val['tax_su_non_habit_surf_stat4'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_stat5'])) {
            $this->valF['tax_su_non_habit_surf_stat5'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_stat5'] = $val['tax_su_non_habit_surf_stat5'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_stat6'])) {
            $this->valF['tax_su_non_habit_surf_stat6'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_stat6'] = $val['tax_su_non_habit_surf_stat6'];
        }
        if (!is_numeric($val['tax_su_non_habit_surf_stat7'])) {
            $this->valF['tax_su_non_habit_surf_stat7'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_surf_stat7'] = $val['tax_su_non_habit_surf_stat7'];
        }
        if (!is_numeric($val['tax_su_parc_statio_expl_comm_surf'])) {
            $this->valF['tax_su_parc_statio_expl_comm_surf'] = NULL;
        } else {
            $this->valF['tax_su_parc_statio_expl_comm_surf'] = $val['tax_su_parc_statio_expl_comm_surf'];
        }
        if (!is_numeric($val['tax_log_ap_trvx_nb'])) {
            $this->valF['tax_log_ap_trvx_nb'] = NULL;
        } else {
            $this->valF['tax_log_ap_trvx_nb'] = $val['tax_log_ap_trvx_nb'];
        }
        if (!is_numeric($val['tax_am_statio_ext_cr'])) {
            $this->valF['tax_am_statio_ext_cr'] = NULL;
        } else {
            $this->valF['tax_am_statio_ext_cr'] = $val['tax_am_statio_ext_cr'];
        }
        if (!is_numeric($val['tax_sup_bass_pisc_cr'])) {
            $this->valF['tax_sup_bass_pisc_cr'] = NULL;
        } else {
            $this->valF['tax_sup_bass_pisc_cr'] = $val['tax_sup_bass_pisc_cr'];
        }
        if (!is_numeric($val['tax_empl_ten_carav_mobil_nb_cr'])) {
            $this->valF['tax_empl_ten_carav_mobil_nb_cr'] = NULL;
        } else {
            $this->valF['tax_empl_ten_carav_mobil_nb_cr'] = $val['tax_empl_ten_carav_mobil_nb_cr'];
        }
        if (!is_numeric($val['tax_empl_hll_nb_cr'])) {
            $this->valF['tax_empl_hll_nb_cr'] = NULL;
        } else {
            $this->valF['tax_empl_hll_nb_cr'] = $val['tax_empl_hll_nb_cr'];
        }
        if (!is_numeric($val['tax_eol_haut_nb_cr'])) {
            $this->valF['tax_eol_haut_nb_cr'] = NULL;
        } else {
            $this->valF['tax_eol_haut_nb_cr'] = $val['tax_eol_haut_nb_cr'];
        }
        if (!is_numeric($val['tax_pann_volt_sup_cr'])) {
            $this->valF['tax_pann_volt_sup_cr'] = NULL;
        } else {
            $this->valF['tax_pann_volt_sup_cr'] = $val['tax_pann_volt_sup_cr'];
        }
        if (!is_numeric($val['tax_surf_loc_arch'])) {
            $this->valF['tax_surf_loc_arch'] = NULL;
        } else {
            $this->valF['tax_surf_loc_arch'] = $val['tax_surf_loc_arch'];
        }
        if (!is_numeric($val['tax_surf_pisc_arch'])) {
            $this->valF['tax_surf_pisc_arch'] = NULL;
        } else {
            $this->valF['tax_surf_pisc_arch'] = $val['tax_surf_pisc_arch'];
        }
        if (!is_numeric($val['tax_am_statio_ext_arch'])) {
            $this->valF['tax_am_statio_ext_arch'] = NULL;
        } else {
            $this->valF['tax_am_statio_ext_arch'] = $val['tax_am_statio_ext_arch'];
        }
        if (!is_numeric($val['tax_empl_ten_carav_mobil_nb_arch'])) {
            $this->valF['tax_empl_ten_carav_mobil_nb_arch'] = NULL;
        } else {
            $this->valF['tax_empl_ten_carav_mobil_nb_arch'] = $val['tax_empl_ten_carav_mobil_nb_arch'];
        }
        if (!is_numeric($val['tax_empl_hll_nb_arch'])) {
            $this->valF['tax_empl_hll_nb_arch'] = NULL;
        } else {
            $this->valF['tax_empl_hll_nb_arch'] = $val['tax_empl_hll_nb_arch'];
        }
        if (!is_numeric($val['tax_eol_haut_nb_arch'])) {
            $this->valF['tax_eol_haut_nb_arch'] = NULL;
        } else {
            $this->valF['tax_eol_haut_nb_arch'] = $val['tax_eol_haut_nb_arch'];
        }
        if ($val['ope_proj_div_co'] == 1 || $val['ope_proj_div_co'] == "t" || $val['ope_proj_div_co'] == "Oui") {
            $this->valF['ope_proj_div_co'] = true;
        } else {
            $this->valF['ope_proj_div_co'] = false;
        }
        if ($val['ope_proj_div_contr'] == 1 || $val['ope_proj_div_contr'] == "t" || $val['ope_proj_div_contr'] == "Oui") {
            $this->valF['ope_proj_div_contr'] = true;
        } else {
            $this->valF['ope_proj_div_contr'] = false;
        }
            $this->valF['tax_desc'] = $val['tax_desc'];
        if ($val['erp_cstr_neuve'] == 1 || $val['erp_cstr_neuve'] == "t" || $val['erp_cstr_neuve'] == "Oui") {
            $this->valF['erp_cstr_neuve'] = true;
        } else {
            $this->valF['erp_cstr_neuve'] = false;
        }
        if ($val['erp_trvx_acc'] == 1 || $val['erp_trvx_acc'] == "t" || $val['erp_trvx_acc'] == "Oui") {
            $this->valF['erp_trvx_acc'] = true;
        } else {
            $this->valF['erp_trvx_acc'] = false;
        }
        if ($val['erp_extension'] == 1 || $val['erp_extension'] == "t" || $val['erp_extension'] == "Oui") {
            $this->valF['erp_extension'] = true;
        } else {
            $this->valF['erp_extension'] = false;
        }
        if ($val['erp_rehab'] == 1 || $val['erp_rehab'] == "t" || $val['erp_rehab'] == "Oui") {
            $this->valF['erp_rehab'] = true;
        } else {
            $this->valF['erp_rehab'] = false;
        }
        if ($val['erp_trvx_am'] == 1 || $val['erp_trvx_am'] == "t" || $val['erp_trvx_am'] == "Oui") {
            $this->valF['erp_trvx_am'] = true;
        } else {
            $this->valF['erp_trvx_am'] = false;
        }
        if ($val['erp_vol_nouv_exist'] == 1 || $val['erp_vol_nouv_exist'] == "t" || $val['erp_vol_nouv_exist'] == "Oui") {
            $this->valF['erp_vol_nouv_exist'] = true;
        } else {
            $this->valF['erp_vol_nouv_exist'] = false;
        }
        if (!is_numeric($val['erp_loc_eff1'])) {
            $this->valF['erp_loc_eff1'] = NULL;
        } else {
            $this->valF['erp_loc_eff1'] = $val['erp_loc_eff1'];
        }
        if (!is_numeric($val['erp_loc_eff2'])) {
            $this->valF['erp_loc_eff2'] = NULL;
        } else {
            $this->valF['erp_loc_eff2'] = $val['erp_loc_eff2'];
        }
        if (!is_numeric($val['erp_loc_eff3'])) {
            $this->valF['erp_loc_eff3'] = NULL;
        } else {
            $this->valF['erp_loc_eff3'] = $val['erp_loc_eff3'];
        }
        if (!is_numeric($val['erp_loc_eff4'])) {
            $this->valF['erp_loc_eff4'] = NULL;
        } else {
            $this->valF['erp_loc_eff4'] = $val['erp_loc_eff4'];
        }
        if (!is_numeric($val['erp_loc_eff5'])) {
            $this->valF['erp_loc_eff5'] = NULL;
        } else {
            $this->valF['erp_loc_eff5'] = $val['erp_loc_eff5'];
        }
        if (!is_numeric($val['erp_loc_eff_tot'])) {
            $this->valF['erp_loc_eff_tot'] = NULL;
        } else {
            $this->valF['erp_loc_eff_tot'] = $val['erp_loc_eff_tot'];
        }
        if (!is_numeric($val['erp_public_eff1'])) {
            $this->valF['erp_public_eff1'] = NULL;
        } else {
            $this->valF['erp_public_eff1'] = $val['erp_public_eff1'];
        }
        if (!is_numeric($val['erp_public_eff2'])) {
            $this->valF['erp_public_eff2'] = NULL;
        } else {
            $this->valF['erp_public_eff2'] = $val['erp_public_eff2'];
        }
        if (!is_numeric($val['erp_public_eff3'])) {
            $this->valF['erp_public_eff3'] = NULL;
        } else {
            $this->valF['erp_public_eff3'] = $val['erp_public_eff3'];
        }
        if (!is_numeric($val['erp_public_eff4'])) {
            $this->valF['erp_public_eff4'] = NULL;
        } else {
            $this->valF['erp_public_eff4'] = $val['erp_public_eff4'];
        }
        if (!is_numeric($val['erp_public_eff5'])) {
            $this->valF['erp_public_eff5'] = NULL;
        } else {
            $this->valF['erp_public_eff5'] = $val['erp_public_eff5'];
        }
        if (!is_numeric($val['erp_public_eff_tot'])) {
            $this->valF['erp_public_eff_tot'] = NULL;
        } else {
            $this->valF['erp_public_eff_tot'] = $val['erp_public_eff_tot'];
        }
        if (!is_numeric($val['erp_perso_eff1'])) {
            $this->valF['erp_perso_eff1'] = NULL;
        } else {
            $this->valF['erp_perso_eff1'] = $val['erp_perso_eff1'];
        }
        if (!is_numeric($val['erp_perso_eff2'])) {
            $this->valF['erp_perso_eff2'] = NULL;
        } else {
            $this->valF['erp_perso_eff2'] = $val['erp_perso_eff2'];
        }
        if (!is_numeric($val['erp_perso_eff3'])) {
            $this->valF['erp_perso_eff3'] = NULL;
        } else {
            $this->valF['erp_perso_eff3'] = $val['erp_perso_eff3'];
        }
        if (!is_numeric($val['erp_perso_eff4'])) {
            $this->valF['erp_perso_eff4'] = NULL;
        } else {
            $this->valF['erp_perso_eff4'] = $val['erp_perso_eff4'];
        }
        if (!is_numeric($val['erp_perso_eff5'])) {
            $this->valF['erp_perso_eff5'] = NULL;
        } else {
            $this->valF['erp_perso_eff5'] = $val['erp_perso_eff5'];
        }
        if (!is_numeric($val['erp_perso_eff_tot'])) {
            $this->valF['erp_perso_eff_tot'] = NULL;
        } else {
            $this->valF['erp_perso_eff_tot'] = $val['erp_perso_eff_tot'];
        }
        if (!is_numeric($val['erp_tot_eff1'])) {
            $this->valF['erp_tot_eff1'] = NULL;
        } else {
            $this->valF['erp_tot_eff1'] = $val['erp_tot_eff1'];
        }
        if (!is_numeric($val['erp_tot_eff2'])) {
            $this->valF['erp_tot_eff2'] = NULL;
        } else {
            $this->valF['erp_tot_eff2'] = $val['erp_tot_eff2'];
        }
        if (!is_numeric($val['erp_tot_eff3'])) {
            $this->valF['erp_tot_eff3'] = NULL;
        } else {
            $this->valF['erp_tot_eff3'] = $val['erp_tot_eff3'];
        }
        if (!is_numeric($val['erp_tot_eff4'])) {
            $this->valF['erp_tot_eff4'] = NULL;
        } else {
            $this->valF['erp_tot_eff4'] = $val['erp_tot_eff4'];
        }
        if (!is_numeric($val['erp_tot_eff5'])) {
            $this->valF['erp_tot_eff5'] = NULL;
        } else {
            $this->valF['erp_tot_eff5'] = $val['erp_tot_eff5'];
        }
        if (!is_numeric($val['erp_tot_eff_tot'])) {
            $this->valF['erp_tot_eff_tot'] = NULL;
        } else {
            $this->valF['erp_tot_eff_tot'] = $val['erp_tot_eff_tot'];
        }
        if (!is_numeric($val['erp_class_cat'])) {
            $this->valF['erp_class_cat'] = NULL;
        } else {
            $this->valF['erp_class_cat'] = $val['erp_class_cat'];
        }
        if (!is_numeric($val['erp_class_type'])) {
            $this->valF['erp_class_type'] = NULL;
        } else {
            $this->valF['erp_class_type'] = $val['erp_class_type'];
        }
        if (!is_numeric($val['tax_surf_abr_jard_pig_colom'])) {
            $this->valF['tax_surf_abr_jard_pig_colom'] = NULL;
        } else {
            $this->valF['tax_surf_abr_jard_pig_colom'] = $val['tax_surf_abr_jard_pig_colom'];
        }
        if (!is_numeric($val['tax_su_non_habit_abr_jard_pig_colom'])) {
            $this->valF['tax_su_non_habit_abr_jard_pig_colom'] = NULL;
        } else {
            $this->valF['tax_su_non_habit_abr_jard_pig_colom'] = $val['tax_su_non_habit_abr_jard_pig_colom'];
        }
        if ($val['dia_imm_non_bati'] == 1 || $val['dia_imm_non_bati'] == "t" || $val['dia_imm_non_bati'] == "Oui") {
            $this->valF['dia_imm_non_bati'] = true;
        } else {
            $this->valF['dia_imm_non_bati'] = false;
        }
        if ($val['dia_imm_bati_terr_propr'] == 1 || $val['dia_imm_bati_terr_propr'] == "t" || $val['dia_imm_bati_terr_propr'] == "Oui") {
            $this->valF['dia_imm_bati_terr_propr'] = true;
        } else {
            $this->valF['dia_imm_bati_terr_propr'] = false;
        }
        if ($val['dia_imm_bati_terr_autr'] == 1 || $val['dia_imm_bati_terr_autr'] == "t" || $val['dia_imm_bati_terr_autr'] == "Oui") {
            $this->valF['dia_imm_bati_terr_autr'] = true;
        } else {
            $this->valF['dia_imm_bati_terr_autr'] = false;
        }
            $this->valF['dia_imm_bati_terr_autr_desc'] = $val['dia_imm_bati_terr_autr_desc'];
        if ($val['dia_bat_copro'] == 1 || $val['dia_bat_copro'] == "t" || $val['dia_bat_copro'] == "Oui") {
            $this->valF['dia_bat_copro'] = true;
        } else {
            $this->valF['dia_bat_copro'] = false;
        }
            $this->valF['dia_bat_copro_desc'] = $val['dia_bat_copro_desc'];
            $this->valF['dia_lot_numero'] = $val['dia_lot_numero'];
            $this->valF['dia_lot_bat'] = $val['dia_lot_bat'];
            $this->valF['dia_lot_etage'] = $val['dia_lot_etage'];
            $this->valF['dia_lot_quote_part'] = $val['dia_lot_quote_part'];
        if ($val['dia_us_hab'] == 1 || $val['dia_us_hab'] == "t" || $val['dia_us_hab'] == "Oui") {
            $this->valF['dia_us_hab'] = true;
        } else {
            $this->valF['dia_us_hab'] = false;
        }
        if ($val['dia_us_pro'] == 1 || $val['dia_us_pro'] == "t" || $val['dia_us_pro'] == "Oui") {
            $this->valF['dia_us_pro'] = true;
        } else {
            $this->valF['dia_us_pro'] = false;
        }
        if ($val['dia_us_mixte'] == 1 || $val['dia_us_mixte'] == "t" || $val['dia_us_mixte'] == "Oui") {
            $this->valF['dia_us_mixte'] = true;
        } else {
            $this->valF['dia_us_mixte'] = false;
        }
        if ($val['dia_us_comm'] == 1 || $val['dia_us_comm'] == "t" || $val['dia_us_comm'] == "Oui") {
            $this->valF['dia_us_comm'] = true;
        } else {
            $this->valF['dia_us_comm'] = false;
        }
        if ($val['dia_us_agr'] == 1 || $val['dia_us_agr'] == "t" || $val['dia_us_agr'] == "Oui") {
            $this->valF['dia_us_agr'] = true;
        } else {
            $this->valF['dia_us_agr'] = false;
        }
        if ($val['dia_us_autre'] == 1 || $val['dia_us_autre'] == "t" || $val['dia_us_autre'] == "Oui") {
            $this->valF['dia_us_autre'] = true;
        } else {
            $this->valF['dia_us_autre'] = false;
        }
            $this->valF['dia_us_autre_prec'] = $val['dia_us_autre_prec'];
        if ($val['dia_occ_prop'] == 1 || $val['dia_occ_prop'] == "t" || $val['dia_occ_prop'] == "Oui") {
            $this->valF['dia_occ_prop'] = true;
        } else {
            $this->valF['dia_occ_prop'] = false;
        }
        if ($val['dia_occ_loc'] == 1 || $val['dia_occ_loc'] == "t" || $val['dia_occ_loc'] == "Oui") {
            $this->valF['dia_occ_loc'] = true;
        } else {
            $this->valF['dia_occ_loc'] = false;
        }
        if ($val['dia_occ_sans_occ'] == 1 || $val['dia_occ_sans_occ'] == "t" || $val['dia_occ_sans_occ'] == "Oui") {
            $this->valF['dia_occ_sans_occ'] = true;
        } else {
            $this->valF['dia_occ_sans_occ'] = false;
        }
        if ($val['dia_occ_autre'] == 1 || $val['dia_occ_autre'] == "t" || $val['dia_occ_autre'] == "Oui") {
            $this->valF['dia_occ_autre'] = true;
        } else {
            $this->valF['dia_occ_autre'] = false;
        }
            $this->valF['dia_occ_autre_prec'] = $val['dia_occ_autre_prec'];
        if ($val['dia_mod_cess_prix_vente'] == "") {
            $this->valF['dia_mod_cess_prix_vente'] = NULL;
        } else {
            $this->valF['dia_mod_cess_prix_vente'] = $val['dia_mod_cess_prix_vente'];
        }
        if ($val['dia_mod_cess_prix_vente_mob'] == "") {
            $this->valF['dia_mod_cess_prix_vente_mob'] = NULL;
        } else {
            $this->valF['dia_mod_cess_prix_vente_mob'] = $val['dia_mod_cess_prix_vente_mob'];
        }
        if ($val['dia_mod_cess_prix_vente_cheptel'] == "") {
            $this->valF['dia_mod_cess_prix_vente_cheptel'] = NULL;
        } else {
            $this->valF['dia_mod_cess_prix_vente_cheptel'] = $val['dia_mod_cess_prix_vente_cheptel'];
        }
        if ($val['dia_mod_cess_prix_vente_recol'] == "") {
            $this->valF['dia_mod_cess_prix_vente_recol'] = NULL;
        } else {
            $this->valF['dia_mod_cess_prix_vente_recol'] = $val['dia_mod_cess_prix_vente_recol'];
        }
        if ($val['dia_mod_cess_prix_vente_autre'] == "") {
            $this->valF['dia_mod_cess_prix_vente_autre'] = NULL;
        } else {
            $this->valF['dia_mod_cess_prix_vente_autre'] = $val['dia_mod_cess_prix_vente_autre'];
        }
        if ($val['dia_mod_cess_commi'] == 1 || $val['dia_mod_cess_commi'] == "t" || $val['dia_mod_cess_commi'] == "Oui") {
            $this->valF['dia_mod_cess_commi'] = true;
        } else {
            $this->valF['dia_mod_cess_commi'] = false;
        }
        if ($val['dia_mod_cess_commi_ttc'] == "") {
            $this->valF['dia_mod_cess_commi_ttc'] = NULL;
        } else {
            $this->valF['dia_mod_cess_commi_ttc'] = $val['dia_mod_cess_commi_ttc'];
        }
        if ($val['dia_mod_cess_commi_ht'] == "") {
            $this->valF['dia_mod_cess_commi_ht'] = NULL;
        } else {
            $this->valF['dia_mod_cess_commi_ht'] = $val['dia_mod_cess_commi_ht'];
        }
        if ($val['dia_acquereur_nom_prenom'] == "") {
            $this->valF['dia_acquereur_nom_prenom'] = NULL;
        } else {
            $this->valF['dia_acquereur_nom_prenom'] = $val['dia_acquereur_nom_prenom'];
        }
        if ($val['dia_acquereur_adr_num_voie'] == "") {
            $this->valF['dia_acquereur_adr_num_voie'] = NULL;
        } else {
            $this->valF['dia_acquereur_adr_num_voie'] = $val['dia_acquereur_adr_num_voie'];
        }
        if ($val['dia_acquereur_adr_ext'] == "") {
            $this->valF['dia_acquereur_adr_ext'] = NULL;
        } else {
            $this->valF['dia_acquereur_adr_ext'] = $val['dia_acquereur_adr_ext'];
        }
        if ($val['dia_acquereur_adr_type_voie'] == "") {
            $this->valF['dia_acquereur_adr_type_voie'] = NULL;
        } else {
            $this->valF['dia_acquereur_adr_type_voie'] = $val['dia_acquereur_adr_type_voie'];
        }
        if ($val['dia_acquereur_adr_nom_voie'] == "") {
            $this->valF['dia_acquereur_adr_nom_voie'] = NULL;
        } else {
            $this->valF['dia_acquereur_adr_nom_voie'] = $val['dia_acquereur_adr_nom_voie'];
        }
        if ($val['dia_acquereur_adr_lieu_dit_bp'] == "") {
            $this->valF['dia_acquereur_adr_lieu_dit_bp'] = NULL;
        } else {
            $this->valF['dia_acquereur_adr_lieu_dit_bp'] = $val['dia_acquereur_adr_lieu_dit_bp'];
        }
        if ($val['dia_acquereur_adr_cp'] == "") {
            $this->valF['dia_acquereur_adr_cp'] = NULL;
        } else {
            $this->valF['dia_acquereur_adr_cp'] = $val['dia_acquereur_adr_cp'];
        }
        if ($val['dia_acquereur_adr_localite'] == "") {
            $this->valF['dia_acquereur_adr_localite'] = NULL;
        } else {
            $this->valF['dia_acquereur_adr_localite'] = $val['dia_acquereur_adr_localite'];
        }
            $this->valF['dia_observation'] = $val['dia_observation'];
        if (!is_numeric($val['su2_avt_shon1'])) {
            $this->valF['su2_avt_shon1'] = NULL;
        } else {
            $this->valF['su2_avt_shon1'] = $val['su2_avt_shon1'];
        }
        if (!is_numeric($val['su2_avt_shon2'])) {
            $this->valF['su2_avt_shon2'] = NULL;
        } else {
            $this->valF['su2_avt_shon2'] = $val['su2_avt_shon2'];
        }
        if (!is_numeric($val['su2_avt_shon3'])) {
            $this->valF['su2_avt_shon3'] = NULL;
        } else {
            $this->valF['su2_avt_shon3'] = $val['su2_avt_shon3'];
        }
        if (!is_numeric($val['su2_avt_shon4'])) {
            $this->valF['su2_avt_shon4'] = NULL;
        } else {
            $this->valF['su2_avt_shon4'] = $val['su2_avt_shon4'];
        }
        if (!is_numeric($val['su2_avt_shon5'])) {
            $this->valF['su2_avt_shon5'] = NULL;
        } else {
            $this->valF['su2_avt_shon5'] = $val['su2_avt_shon5'];
        }
        if (!is_numeric($val['su2_avt_shon6'])) {
            $this->valF['su2_avt_shon6'] = NULL;
        } else {
            $this->valF['su2_avt_shon6'] = $val['su2_avt_shon6'];
        }
        if (!is_numeric($val['su2_avt_shon7'])) {
            $this->valF['su2_avt_shon7'] = NULL;
        } else {
            $this->valF['su2_avt_shon7'] = $val['su2_avt_shon7'];
        }
        if (!is_numeric($val['su2_avt_shon8'])) {
            $this->valF['su2_avt_shon8'] = NULL;
        } else {
            $this->valF['su2_avt_shon8'] = $val['su2_avt_shon8'];
        }
        if (!is_numeric($val['su2_avt_shon9'])) {
            $this->valF['su2_avt_shon9'] = NULL;
        } else {
            $this->valF['su2_avt_shon9'] = $val['su2_avt_shon9'];
        }
        if (!is_numeric($val['su2_avt_shon10'])) {
            $this->valF['su2_avt_shon10'] = NULL;
        } else {
            $this->valF['su2_avt_shon10'] = $val['su2_avt_shon10'];
        }
        if (!is_numeric($val['su2_avt_shon11'])) {
            $this->valF['su2_avt_shon11'] = NULL;
        } else {
            $this->valF['su2_avt_shon11'] = $val['su2_avt_shon11'];
        }
        if (!is_numeric($val['su2_avt_shon12'])) {
            $this->valF['su2_avt_shon12'] = NULL;
        } else {
            $this->valF['su2_avt_shon12'] = $val['su2_avt_shon12'];
        }
        if (!is_numeric($val['su2_avt_shon13'])) {
            $this->valF['su2_avt_shon13'] = NULL;
        } else {
            $this->valF['su2_avt_shon13'] = $val['su2_avt_shon13'];
        }
        if (!is_numeric($val['su2_avt_shon14'])) {
            $this->valF['su2_avt_shon14'] = NULL;
        } else {
            $this->valF['su2_avt_shon14'] = $val['su2_avt_shon14'];
        }
        if (!is_numeric($val['su2_avt_shon15'])) {
            $this->valF['su2_avt_shon15'] = NULL;
        } else {
            $this->valF['su2_avt_shon15'] = $val['su2_avt_shon15'];
        }
        if (!is_numeric($val['su2_avt_shon16'])) {
            $this->valF['su2_avt_shon16'] = NULL;
        } else {
            $this->valF['su2_avt_shon16'] = $val['su2_avt_shon16'];
        }
        if (!is_numeric($val['su2_avt_shon17'])) {
            $this->valF['su2_avt_shon17'] = NULL;
        } else {
            $this->valF['su2_avt_shon17'] = $val['su2_avt_shon17'];
        }
        if (!is_numeric($val['su2_avt_shon18'])) {
            $this->valF['su2_avt_shon18'] = NULL;
        } else {
            $this->valF['su2_avt_shon18'] = $val['su2_avt_shon18'];
        }
        if (!is_numeric($val['su2_avt_shon19'])) {
            $this->valF['su2_avt_shon19'] = NULL;
        } else {
            $this->valF['su2_avt_shon19'] = $val['su2_avt_shon19'];
        }
        if (!is_numeric($val['su2_avt_shon20'])) {
            $this->valF['su2_avt_shon20'] = NULL;
        } else {
            $this->valF['su2_avt_shon20'] = $val['su2_avt_shon20'];
        }
        if (!is_numeric($val['su2_avt_shon_tot'])) {
            $this->valF['su2_avt_shon_tot'] = NULL;
        } else {
            $this->valF['su2_avt_shon_tot'] = $val['su2_avt_shon_tot'];
        }
        if (!is_numeric($val['su2_cstr_shon1'])) {
            $this->valF['su2_cstr_shon1'] = NULL;
        } else {
            $this->valF['su2_cstr_shon1'] = $val['su2_cstr_shon1'];
        }
        if (!is_numeric($val['su2_cstr_shon2'])) {
            $this->valF['su2_cstr_shon2'] = NULL;
        } else {
            $this->valF['su2_cstr_shon2'] = $val['su2_cstr_shon2'];
        }
        if (!is_numeric($val['su2_cstr_shon3'])) {
            $this->valF['su2_cstr_shon3'] = NULL;
        } else {
            $this->valF['su2_cstr_shon3'] = $val['su2_cstr_shon3'];
        }
        if (!is_numeric($val['su2_cstr_shon4'])) {
            $this->valF['su2_cstr_shon4'] = NULL;
        } else {
            $this->valF['su2_cstr_shon4'] = $val['su2_cstr_shon4'];
        }
        if (!is_numeric($val['su2_cstr_shon5'])) {
            $this->valF['su2_cstr_shon5'] = NULL;
        } else {
            $this->valF['su2_cstr_shon5'] = $val['su2_cstr_shon5'];
        }
        if (!is_numeric($val['su2_cstr_shon6'])) {
            $this->valF['su2_cstr_shon6'] = NULL;
        } else {
            $this->valF['su2_cstr_shon6'] = $val['su2_cstr_shon6'];
        }
        if (!is_numeric($val['su2_cstr_shon7'])) {
            $this->valF['su2_cstr_shon7'] = NULL;
        } else {
            $this->valF['su2_cstr_shon7'] = $val['su2_cstr_shon7'];
        }
        if (!is_numeric($val['su2_cstr_shon8'])) {
            $this->valF['su2_cstr_shon8'] = NULL;
        } else {
            $this->valF['su2_cstr_shon8'] = $val['su2_cstr_shon8'];
        }
        if (!is_numeric($val['su2_cstr_shon9'])) {
            $this->valF['su2_cstr_shon9'] = NULL;
        } else {
            $this->valF['su2_cstr_shon9'] = $val['su2_cstr_shon9'];
        }
        if (!is_numeric($val['su2_cstr_shon10'])) {
            $this->valF['su2_cstr_shon10'] = NULL;
        } else {
            $this->valF['su2_cstr_shon10'] = $val['su2_cstr_shon10'];
        }
        if (!is_numeric($val['su2_cstr_shon11'])) {
            $this->valF['su2_cstr_shon11'] = NULL;
        } else {
            $this->valF['su2_cstr_shon11'] = $val['su2_cstr_shon11'];
        }
        if (!is_numeric($val['su2_cstr_shon12'])) {
            $this->valF['su2_cstr_shon12'] = NULL;
        } else {
            $this->valF['su2_cstr_shon12'] = $val['su2_cstr_shon12'];
        }
        if (!is_numeric($val['su2_cstr_shon13'])) {
            $this->valF['su2_cstr_shon13'] = NULL;
        } else {
            $this->valF['su2_cstr_shon13'] = $val['su2_cstr_shon13'];
        }
        if (!is_numeric($val['su2_cstr_shon14'])) {
            $this->valF['su2_cstr_shon14'] = NULL;
        } else {
            $this->valF['su2_cstr_shon14'] = $val['su2_cstr_shon14'];
        }
        if (!is_numeric($val['su2_cstr_shon15'])) {
            $this->valF['su2_cstr_shon15'] = NULL;
        } else {
            $this->valF['su2_cstr_shon15'] = $val['su2_cstr_shon15'];
        }
        if (!is_numeric($val['su2_cstr_shon16'])) {
            $this->valF['su2_cstr_shon16'] = NULL;
        } else {
            $this->valF['su2_cstr_shon16'] = $val['su2_cstr_shon16'];
        }
        if (!is_numeric($val['su2_cstr_shon17'])) {
            $this->valF['su2_cstr_shon17'] = NULL;
        } else {
            $this->valF['su2_cstr_shon17'] = $val['su2_cstr_shon17'];
        }
        if (!is_numeric($val['su2_cstr_shon18'])) {
            $this->valF['su2_cstr_shon18'] = NULL;
        } else {
            $this->valF['su2_cstr_shon18'] = $val['su2_cstr_shon18'];
        }
        if (!is_numeric($val['su2_cstr_shon19'])) {
            $this->valF['su2_cstr_shon19'] = NULL;
        } else {
            $this->valF['su2_cstr_shon19'] = $val['su2_cstr_shon19'];
        }
        if (!is_numeric($val['su2_cstr_shon20'])) {
            $this->valF['su2_cstr_shon20'] = NULL;
        } else {
            $this->valF['su2_cstr_shon20'] = $val['su2_cstr_shon20'];
        }
        if (!is_numeric($val['su2_cstr_shon_tot'])) {
            $this->valF['su2_cstr_shon_tot'] = NULL;
        } else {
            $this->valF['su2_cstr_shon_tot'] = $val['su2_cstr_shon_tot'];
        }
        if (!is_numeric($val['su2_chge_shon1'])) {
            $this->valF['su2_chge_shon1'] = NULL;
        } else {
            $this->valF['su2_chge_shon1'] = $val['su2_chge_shon1'];
        }
        if (!is_numeric($val['su2_chge_shon2'])) {
            $this->valF['su2_chge_shon2'] = NULL;
        } else {
            $this->valF['su2_chge_shon2'] = $val['su2_chge_shon2'];
        }
        if (!is_numeric($val['su2_chge_shon3'])) {
            $this->valF['su2_chge_shon3'] = NULL;
        } else {
            $this->valF['su2_chge_shon3'] = $val['su2_chge_shon3'];
        }
        if (!is_numeric($val['su2_chge_shon4'])) {
            $this->valF['su2_chge_shon4'] = NULL;
        } else {
            $this->valF['su2_chge_shon4'] = $val['su2_chge_shon4'];
        }
        if (!is_numeric($val['su2_chge_shon5'])) {
            $this->valF['su2_chge_shon5'] = NULL;
        } else {
            $this->valF['su2_chge_shon5'] = $val['su2_chge_shon5'];
        }
        if (!is_numeric($val['su2_chge_shon6'])) {
            $this->valF['su2_chge_shon6'] = NULL;
        } else {
            $this->valF['su2_chge_shon6'] = $val['su2_chge_shon6'];
        }
        if (!is_numeric($val['su2_chge_shon7'])) {
            $this->valF['su2_chge_shon7'] = NULL;
        } else {
            $this->valF['su2_chge_shon7'] = $val['su2_chge_shon7'];
        }
        if (!is_numeric($val['su2_chge_shon8'])) {
            $this->valF['su2_chge_shon8'] = NULL;
        } else {
            $this->valF['su2_chge_shon8'] = $val['su2_chge_shon8'];
        }
        if (!is_numeric($val['su2_chge_shon9'])) {
            $this->valF['su2_chge_shon9'] = NULL;
        } else {
            $this->valF['su2_chge_shon9'] = $val['su2_chge_shon9'];
        }
        if (!is_numeric($val['su2_chge_shon10'])) {
            $this->valF['su2_chge_shon10'] = NULL;
        } else {
            $this->valF['su2_chge_shon10'] = $val['su2_chge_shon10'];
        }
        if (!is_numeric($val['su2_chge_shon11'])) {
            $this->valF['su2_chge_shon11'] = NULL;
        } else {
            $this->valF['su2_chge_shon11'] = $val['su2_chge_shon11'];
        }
        if (!is_numeric($val['su2_chge_shon12'])) {
            $this->valF['su2_chge_shon12'] = NULL;
        } else {
            $this->valF['su2_chge_shon12'] = $val['su2_chge_shon12'];
        }
        if (!is_numeric($val['su2_chge_shon13'])) {
            $this->valF['su2_chge_shon13'] = NULL;
        } else {
            $this->valF['su2_chge_shon13'] = $val['su2_chge_shon13'];
        }
        if (!is_numeric($val['su2_chge_shon14'])) {
            $this->valF['su2_chge_shon14'] = NULL;
        } else {
            $this->valF['su2_chge_shon14'] = $val['su2_chge_shon14'];
        }
        if (!is_numeric($val['su2_chge_shon15'])) {
            $this->valF['su2_chge_shon15'] = NULL;
        } else {
            $this->valF['su2_chge_shon15'] = $val['su2_chge_shon15'];
        }
        if (!is_numeric($val['su2_chge_shon16'])) {
            $this->valF['su2_chge_shon16'] = NULL;
        } else {
            $this->valF['su2_chge_shon16'] = $val['su2_chge_shon16'];
        }
        if (!is_numeric($val['su2_chge_shon17'])) {
            $this->valF['su2_chge_shon17'] = NULL;
        } else {
            $this->valF['su2_chge_shon17'] = $val['su2_chge_shon17'];
        }
        if (!is_numeric($val['su2_chge_shon18'])) {
            $this->valF['su2_chge_shon18'] = NULL;
        } else {
            $this->valF['su2_chge_shon18'] = $val['su2_chge_shon18'];
        }
        if (!is_numeric($val['su2_chge_shon19'])) {
            $this->valF['su2_chge_shon19'] = NULL;
        } else {
            $this->valF['su2_chge_shon19'] = $val['su2_chge_shon19'];
        }
        if (!is_numeric($val['su2_chge_shon20'])) {
            $this->valF['su2_chge_shon20'] = NULL;
        } else {
            $this->valF['su2_chge_shon20'] = $val['su2_chge_shon20'];
        }
        if (!is_numeric($val['su2_chge_shon_tot'])) {
            $this->valF['su2_chge_shon_tot'] = NULL;
        } else {
            $this->valF['su2_chge_shon_tot'] = $val['su2_chge_shon_tot'];
        }
        if (!is_numeric($val['su2_demo_shon1'])) {
            $this->valF['su2_demo_shon1'] = NULL;
        } else {
            $this->valF['su2_demo_shon1'] = $val['su2_demo_shon1'];
        }
        if (!is_numeric($val['su2_demo_shon2'])) {
            $this->valF['su2_demo_shon2'] = NULL;
        } else {
            $this->valF['su2_demo_shon2'] = $val['su2_demo_shon2'];
        }
        if (!is_numeric($val['su2_demo_shon3'])) {
            $this->valF['su2_demo_shon3'] = NULL;
        } else {
            $this->valF['su2_demo_shon3'] = $val['su2_demo_shon3'];
        }
        if (!is_numeric($val['su2_demo_shon4'])) {
            $this->valF['su2_demo_shon4'] = NULL;
        } else {
            $this->valF['su2_demo_shon4'] = $val['su2_demo_shon4'];
        }
        if (!is_numeric($val['su2_demo_shon5'])) {
            $this->valF['su2_demo_shon5'] = NULL;
        } else {
            $this->valF['su2_demo_shon5'] = $val['su2_demo_shon5'];
        }
        if (!is_numeric($val['su2_demo_shon6'])) {
            $this->valF['su2_demo_shon6'] = NULL;
        } else {
            $this->valF['su2_demo_shon6'] = $val['su2_demo_shon6'];
        }
        if (!is_numeric($val['su2_demo_shon7'])) {
            $this->valF['su2_demo_shon7'] = NULL;
        } else {
            $this->valF['su2_demo_shon7'] = $val['su2_demo_shon7'];
        }
        if (!is_numeric($val['su2_demo_shon8'])) {
            $this->valF['su2_demo_shon8'] = NULL;
        } else {
            $this->valF['su2_demo_shon8'] = $val['su2_demo_shon8'];
        }
        if (!is_numeric($val['su2_demo_shon9'])) {
            $this->valF['su2_demo_shon9'] = NULL;
        } else {
            $this->valF['su2_demo_shon9'] = $val['su2_demo_shon9'];
        }
        if (!is_numeric($val['su2_demo_shon10'])) {
            $this->valF['su2_demo_shon10'] = NULL;
        } else {
            $this->valF['su2_demo_shon10'] = $val['su2_demo_shon10'];
        }
        if (!is_numeric($val['su2_demo_shon11'])) {
            $this->valF['su2_demo_shon11'] = NULL;
        } else {
            $this->valF['su2_demo_shon11'] = $val['su2_demo_shon11'];
        }
        if (!is_numeric($val['su2_demo_shon12'])) {
            $this->valF['su2_demo_shon12'] = NULL;
        } else {
            $this->valF['su2_demo_shon12'] = $val['su2_demo_shon12'];
        }
        if (!is_numeric($val['su2_demo_shon13'])) {
            $this->valF['su2_demo_shon13'] = NULL;
        } else {
            $this->valF['su2_demo_shon13'] = $val['su2_demo_shon13'];
        }
        if (!is_numeric($val['su2_demo_shon14'])) {
            $this->valF['su2_demo_shon14'] = NULL;
        } else {
            $this->valF['su2_demo_shon14'] = $val['su2_demo_shon14'];
        }
        if (!is_numeric($val['su2_demo_shon15'])) {
            $this->valF['su2_demo_shon15'] = NULL;
        } else {
            $this->valF['su2_demo_shon15'] = $val['su2_demo_shon15'];
        }
        if (!is_numeric($val['su2_demo_shon16'])) {
            $this->valF['su2_demo_shon16'] = NULL;
        } else {
            $this->valF['su2_demo_shon16'] = $val['su2_demo_shon16'];
        }
        if (!is_numeric($val['su2_demo_shon17'])) {
            $this->valF['su2_demo_shon17'] = NULL;
        } else {
            $this->valF['su2_demo_shon17'] = $val['su2_demo_shon17'];
        }
        if (!is_numeric($val['su2_demo_shon18'])) {
            $this->valF['su2_demo_shon18'] = NULL;
        } else {
            $this->valF['su2_demo_shon18'] = $val['su2_demo_shon18'];
        }
        if (!is_numeric($val['su2_demo_shon19'])) {
            $this->valF['su2_demo_shon19'] = NULL;
        } else {
            $this->valF['su2_demo_shon19'] = $val['su2_demo_shon19'];
        }
        if (!is_numeric($val['su2_demo_shon20'])) {
            $this->valF['su2_demo_shon20'] = NULL;
        } else {
            $this->valF['su2_demo_shon20'] = $val['su2_demo_shon20'];
        }
        if (!is_numeric($val['su2_demo_shon_tot'])) {
            $this->valF['su2_demo_shon_tot'] = NULL;
        } else {
            $this->valF['su2_demo_shon_tot'] = $val['su2_demo_shon_tot'];
        }
        if (!is_numeric($val['su2_sup_shon1'])) {
            $this->valF['su2_sup_shon1'] = NULL;
        } else {
            $this->valF['su2_sup_shon1'] = $val['su2_sup_shon1'];
        }
        if (!is_numeric($val['su2_sup_shon2'])) {
            $this->valF['su2_sup_shon2'] = NULL;
        } else {
            $this->valF['su2_sup_shon2'] = $val['su2_sup_shon2'];
        }
        if (!is_numeric($val['su2_sup_shon3'])) {
            $this->valF['su2_sup_shon3'] = NULL;
        } else {
            $this->valF['su2_sup_shon3'] = $val['su2_sup_shon3'];
        }
        if (!is_numeric($val['su2_sup_shon4'])) {
            $this->valF['su2_sup_shon4'] = NULL;
        } else {
            $this->valF['su2_sup_shon4'] = $val['su2_sup_shon4'];
        }
        if (!is_numeric($val['su2_sup_shon5'])) {
            $this->valF['su2_sup_shon5'] = NULL;
        } else {
            $this->valF['su2_sup_shon5'] = $val['su2_sup_shon5'];
        }
        if (!is_numeric($val['su2_sup_shon6'])) {
            $this->valF['su2_sup_shon6'] = NULL;
        } else {
            $this->valF['su2_sup_shon6'] = $val['su2_sup_shon6'];
        }
        if (!is_numeric($val['su2_sup_shon7'])) {
            $this->valF['su2_sup_shon7'] = NULL;
        } else {
            $this->valF['su2_sup_shon7'] = $val['su2_sup_shon7'];
        }
        if (!is_numeric($val['su2_sup_shon8'])) {
            $this->valF['su2_sup_shon8'] = NULL;
        } else {
            $this->valF['su2_sup_shon8'] = $val['su2_sup_shon8'];
        }
        if (!is_numeric($val['su2_sup_shon9'])) {
            $this->valF['su2_sup_shon9'] = NULL;
        } else {
            $this->valF['su2_sup_shon9'] = $val['su2_sup_shon9'];
        }
        if (!is_numeric($val['su2_sup_shon10'])) {
            $this->valF['su2_sup_shon10'] = NULL;
        } else {
            $this->valF['su2_sup_shon10'] = $val['su2_sup_shon10'];
        }
        if (!is_numeric($val['su2_sup_shon11'])) {
            $this->valF['su2_sup_shon11'] = NULL;
        } else {
            $this->valF['su2_sup_shon11'] = $val['su2_sup_shon11'];
        }
        if (!is_numeric($val['su2_sup_shon12'])) {
            $this->valF['su2_sup_shon12'] = NULL;
        } else {
            $this->valF['su2_sup_shon12'] = $val['su2_sup_shon12'];
        }
        if (!is_numeric($val['su2_sup_shon13'])) {
            $this->valF['su2_sup_shon13'] = NULL;
        } else {
            $this->valF['su2_sup_shon13'] = $val['su2_sup_shon13'];
        }
        if (!is_numeric($val['su2_sup_shon14'])) {
            $this->valF['su2_sup_shon14'] = NULL;
        } else {
            $this->valF['su2_sup_shon14'] = $val['su2_sup_shon14'];
        }
        if (!is_numeric($val['su2_sup_shon15'])) {
            $this->valF['su2_sup_shon15'] = NULL;
        } else {
            $this->valF['su2_sup_shon15'] = $val['su2_sup_shon15'];
        }
        if (!is_numeric($val['su2_sup_shon16'])) {
            $this->valF['su2_sup_shon16'] = NULL;
        } else {
            $this->valF['su2_sup_shon16'] = $val['su2_sup_shon16'];
        }
        if (!is_numeric($val['su2_sup_shon17'])) {
            $this->valF['su2_sup_shon17'] = NULL;
        } else {
            $this->valF['su2_sup_shon17'] = $val['su2_sup_shon17'];
        }
        if (!is_numeric($val['su2_sup_shon18'])) {
            $this->valF['su2_sup_shon18'] = NULL;
        } else {
            $this->valF['su2_sup_shon18'] = $val['su2_sup_shon18'];
        }
        if (!is_numeric($val['su2_sup_shon19'])) {
            $this->valF['su2_sup_shon19'] = NULL;
        } else {
            $this->valF['su2_sup_shon19'] = $val['su2_sup_shon19'];
        }
        if (!is_numeric($val['su2_sup_shon20'])) {
            $this->valF['su2_sup_shon20'] = NULL;
        } else {
            $this->valF['su2_sup_shon20'] = $val['su2_sup_shon20'];
        }
        if (!is_numeric($val['su2_sup_shon_tot'])) {
            $this->valF['su2_sup_shon_tot'] = NULL;
        } else {
            $this->valF['su2_sup_shon_tot'] = $val['su2_sup_shon_tot'];
        }
        if (!is_numeric($val['su2_tot_shon1'])) {
            $this->valF['su2_tot_shon1'] = NULL;
        } else {
            $this->valF['su2_tot_shon1'] = $val['su2_tot_shon1'];
        }
        if (!is_numeric($val['su2_tot_shon2'])) {
            $this->valF['su2_tot_shon2'] = NULL;
        } else {
            $this->valF['su2_tot_shon2'] = $val['su2_tot_shon2'];
        }
        if (!is_numeric($val['su2_tot_shon3'])) {
            $this->valF['su2_tot_shon3'] = NULL;
        } else {
            $this->valF['su2_tot_shon3'] = $val['su2_tot_shon3'];
        }
        if (!is_numeric($val['su2_tot_shon4'])) {
            $this->valF['su2_tot_shon4'] = NULL;
        } else {
            $this->valF['su2_tot_shon4'] = $val['su2_tot_shon4'];
        }
        if (!is_numeric($val['su2_tot_shon5'])) {
            $this->valF['su2_tot_shon5'] = NULL;
        } else {
            $this->valF['su2_tot_shon5'] = $val['su2_tot_shon5'];
        }
        if (!is_numeric($val['su2_tot_shon6'])) {
            $this->valF['su2_tot_shon6'] = NULL;
        } else {
            $this->valF['su2_tot_shon6'] = $val['su2_tot_shon6'];
        }
        if (!is_numeric($val['su2_tot_shon7'])) {
            $this->valF['su2_tot_shon7'] = NULL;
        } else {
            $this->valF['su2_tot_shon7'] = $val['su2_tot_shon7'];
        }
        if (!is_numeric($val['su2_tot_shon8'])) {
            $this->valF['su2_tot_shon8'] = NULL;
        } else {
            $this->valF['su2_tot_shon8'] = $val['su2_tot_shon8'];
        }
        if (!is_numeric($val['su2_tot_shon9'])) {
            $this->valF['su2_tot_shon9'] = NULL;
        } else {
            $this->valF['su2_tot_shon9'] = $val['su2_tot_shon9'];
        }
        if (!is_numeric($val['su2_tot_shon10'])) {
            $this->valF['su2_tot_shon10'] = NULL;
        } else {
            $this->valF['su2_tot_shon10'] = $val['su2_tot_shon10'];
        }
        if (!is_numeric($val['su2_tot_shon11'])) {
            $this->valF['su2_tot_shon11'] = NULL;
        } else {
            $this->valF['su2_tot_shon11'] = $val['su2_tot_shon11'];
        }
        if (!is_numeric($val['su2_tot_shon12'])) {
            $this->valF['su2_tot_shon12'] = NULL;
        } else {
            $this->valF['su2_tot_shon12'] = $val['su2_tot_shon12'];
        }
        if (!is_numeric($val['su2_tot_shon13'])) {
            $this->valF['su2_tot_shon13'] = NULL;
        } else {
            $this->valF['su2_tot_shon13'] = $val['su2_tot_shon13'];
        }
        if (!is_numeric($val['su2_tot_shon14'])) {
            $this->valF['su2_tot_shon14'] = NULL;
        } else {
            $this->valF['su2_tot_shon14'] = $val['su2_tot_shon14'];
        }
        if (!is_numeric($val['su2_tot_shon15'])) {
            $this->valF['su2_tot_shon15'] = NULL;
        } else {
            $this->valF['su2_tot_shon15'] = $val['su2_tot_shon15'];
        }
        if (!is_numeric($val['su2_tot_shon16'])) {
            $this->valF['su2_tot_shon16'] = NULL;
        } else {
            $this->valF['su2_tot_shon16'] = $val['su2_tot_shon16'];
        }
        if (!is_numeric($val['su2_tot_shon17'])) {
            $this->valF['su2_tot_shon17'] = NULL;
        } else {
            $this->valF['su2_tot_shon17'] = $val['su2_tot_shon17'];
        }
        if (!is_numeric($val['su2_tot_shon18'])) {
            $this->valF['su2_tot_shon18'] = NULL;
        } else {
            $this->valF['su2_tot_shon18'] = $val['su2_tot_shon18'];
        }
        if (!is_numeric($val['su2_tot_shon19'])) {
            $this->valF['su2_tot_shon19'] = NULL;
        } else {
            $this->valF['su2_tot_shon19'] = $val['su2_tot_shon19'];
        }
        if (!is_numeric($val['su2_tot_shon20'])) {
            $this->valF['su2_tot_shon20'] = NULL;
        } else {
            $this->valF['su2_tot_shon20'] = $val['su2_tot_shon20'];
        }
        if (!is_numeric($val['su2_tot_shon_tot'])) {
            $this->valF['su2_tot_shon_tot'] = NULL;
        } else {
            $this->valF['su2_tot_shon_tot'] = $val['su2_tot_shon_tot'];
        }
            $this->valF['dia_occ_sol_su_terre'] = $val['dia_occ_sol_su_terre'];
            $this->valF['dia_occ_sol_su_pres'] = $val['dia_occ_sol_su_pres'];
            $this->valF['dia_occ_sol_su_verger'] = $val['dia_occ_sol_su_verger'];
            $this->valF['dia_occ_sol_su_vigne'] = $val['dia_occ_sol_su_vigne'];
            $this->valF['dia_occ_sol_su_bois'] = $val['dia_occ_sol_su_bois'];
            $this->valF['dia_occ_sol_su_lande'] = $val['dia_occ_sol_su_lande'];
            $this->valF['dia_occ_sol_su_carriere'] = $val['dia_occ_sol_su_carriere'];
            $this->valF['dia_occ_sol_su_eau_cadastree'] = $val['dia_occ_sol_su_eau_cadastree'];
            $this->valF['dia_occ_sol_su_jardin'] = $val['dia_occ_sol_su_jardin'];
            $this->valF['dia_occ_sol_su_terr_batir'] = $val['dia_occ_sol_su_terr_batir'];
            $this->valF['dia_occ_sol_su_terr_agr'] = $val['dia_occ_sol_su_terr_agr'];
            $this->valF['dia_occ_sol_su_sol'] = $val['dia_occ_sol_su_sol'];
        if ($val['dia_bati_vend_tot'] == 1 || $val['dia_bati_vend_tot'] == "t" || $val['dia_bati_vend_tot'] == "Oui") {
            $this->valF['dia_bati_vend_tot'] = true;
        } else {
            $this->valF['dia_bati_vend_tot'] = false;
        }
            $this->valF['dia_bati_vend_tot_txt'] = $val['dia_bati_vend_tot_txt'];
            $this->valF['dia_su_co_sol'] = $val['dia_su_co_sol'];
            $this->valF['dia_su_util_hab'] = $val['dia_su_util_hab'];
            $this->valF['dia_nb_niv'] = $val['dia_nb_niv'];
            $this->valF['dia_nb_appart'] = $val['dia_nb_appart'];
            $this->valF['dia_nb_autre_loc'] = $val['dia_nb_autre_loc'];
        if ($val['dia_vente_lot_volume'] == 1 || $val['dia_vente_lot_volume'] == "t" || $val['dia_vente_lot_volume'] == "Oui") {
            $this->valF['dia_vente_lot_volume'] = true;
        } else {
            $this->valF['dia_vente_lot_volume'] = false;
        }
            $this->valF['dia_vente_lot_volume_txt'] = $val['dia_vente_lot_volume_txt'];
            $this->valF['dia_lot_nat_su'] = $val['dia_lot_nat_su'];
        if ($val['dia_lot_bat_achv_plus_10'] == 1 || $val['dia_lot_bat_achv_plus_10'] == "t" || $val['dia_lot_bat_achv_plus_10'] == "Oui") {
            $this->valF['dia_lot_bat_achv_plus_10'] = true;
        } else {
            $this->valF['dia_lot_bat_achv_plus_10'] = false;
        }
        if ($val['dia_lot_bat_achv_moins_10'] == 1 || $val['dia_lot_bat_achv_moins_10'] == "t" || $val['dia_lot_bat_achv_moins_10'] == "Oui") {
            $this->valF['dia_lot_bat_achv_moins_10'] = true;
        } else {
            $this->valF['dia_lot_bat_achv_moins_10'] = false;
        }
        if ($val['dia_lot_regl_copro_publ_hypo_plus_10'] == 1 || $val['dia_lot_regl_copro_publ_hypo_plus_10'] == "t" || $val['dia_lot_regl_copro_publ_hypo_plus_10'] == "Oui") {
            $this->valF['dia_lot_regl_copro_publ_hypo_plus_10'] = true;
        } else {
            $this->valF['dia_lot_regl_copro_publ_hypo_plus_10'] = false;
        }
        if ($val['dia_lot_regl_copro_publ_hypo_moins_10'] == 1 || $val['dia_lot_regl_copro_publ_hypo_moins_10'] == "t" || $val['dia_lot_regl_copro_publ_hypo_moins_10'] == "Oui") {
            $this->valF['dia_lot_regl_copro_publ_hypo_moins_10'] = true;
        } else {
            $this->valF['dia_lot_regl_copro_publ_hypo_moins_10'] = false;
        }
            $this->valF['dia_indivi_quote_part'] = $val['dia_indivi_quote_part'];
            $this->valF['dia_design_societe'] = $val['dia_design_societe'];
            $this->valF['dia_design_droit'] = $val['dia_design_droit'];
            $this->valF['dia_droit_soc_nat'] = $val['dia_droit_soc_nat'];
            $this->valF['dia_droit_soc_nb'] = $val['dia_droit_soc_nb'];
            $this->valF['dia_droit_soc_num_part'] = $val['dia_droit_soc_num_part'];
        if ($val['dia_droit_reel_perso_grevant_bien_oui'] == 1 || $val['dia_droit_reel_perso_grevant_bien_oui'] == "t" || $val['dia_droit_reel_perso_grevant_bien_oui'] == "Oui") {
            $this->valF['dia_droit_reel_perso_grevant_bien_oui'] = true;
        } else {
            $this->valF['dia_droit_reel_perso_grevant_bien_oui'] = false;
        }
        if ($val['dia_droit_reel_perso_grevant_bien_non'] == 1 || $val['dia_droit_reel_perso_grevant_bien_non'] == "t" || $val['dia_droit_reel_perso_grevant_bien_non'] == "Oui") {
            $this->valF['dia_droit_reel_perso_grevant_bien_non'] = true;
        } else {
            $this->valF['dia_droit_reel_perso_grevant_bien_non'] = false;
        }
            $this->valF['dia_droit_reel_perso_nat'] = $val['dia_droit_reel_perso_nat'];
            $this->valF['dia_droit_reel_perso_viag'] = $val['dia_droit_reel_perso_viag'];
            $this->valF['dia_mod_cess_adr'] = $val['dia_mod_cess_adr'];
        if ($val['dia_mod_cess_sign_act_auth'] == 1 || $val['dia_mod_cess_sign_act_auth'] == "t" || $val['dia_mod_cess_sign_act_auth'] == "Oui") {
            $this->valF['dia_mod_cess_sign_act_auth'] = true;
        } else {
            $this->valF['dia_mod_cess_sign_act_auth'] = false;
        }
        if ($val['dia_mod_cess_terme'] == 1 || $val['dia_mod_cess_terme'] == "t" || $val['dia_mod_cess_terme'] == "Oui") {
            $this->valF['dia_mod_cess_terme'] = true;
        } else {
            $this->valF['dia_mod_cess_terme'] = false;
        }
            $this->valF['dia_mod_cess_terme_prec'] = $val['dia_mod_cess_terme_prec'];
        if ($val['dia_mod_cess_bene_acquereur'] == 1 || $val['dia_mod_cess_bene_acquereur'] == "t" || $val['dia_mod_cess_bene_acquereur'] == "Oui") {
            $this->valF['dia_mod_cess_bene_acquereur'] = true;
        } else {
            $this->valF['dia_mod_cess_bene_acquereur'] = false;
        }
        if ($val['dia_mod_cess_bene_vendeur'] == 1 || $val['dia_mod_cess_bene_vendeur'] == "t" || $val['dia_mod_cess_bene_vendeur'] == "Oui") {
            $this->valF['dia_mod_cess_bene_vendeur'] = true;
        } else {
            $this->valF['dia_mod_cess_bene_vendeur'] = false;
        }
        if ($val['dia_mod_cess_paie_nat'] == 1 || $val['dia_mod_cess_paie_nat'] == "t" || $val['dia_mod_cess_paie_nat'] == "Oui") {
            $this->valF['dia_mod_cess_paie_nat'] = true;
        } else {
            $this->valF['dia_mod_cess_paie_nat'] = false;
        }
            $this->valF['dia_mod_cess_design_contr_alien'] = $val['dia_mod_cess_design_contr_alien'];
            $this->valF['dia_mod_cess_eval_contr'] = $val['dia_mod_cess_eval_contr'];
        if ($val['dia_mod_cess_rente_viag'] == 1 || $val['dia_mod_cess_rente_viag'] == "t" || $val['dia_mod_cess_rente_viag'] == "Oui") {
            $this->valF['dia_mod_cess_rente_viag'] = true;
        } else {
            $this->valF['dia_mod_cess_rente_viag'] = false;
        }
            $this->valF['dia_mod_cess_mnt_an'] = $val['dia_mod_cess_mnt_an'];
            $this->valF['dia_mod_cess_mnt_compt'] = $val['dia_mod_cess_mnt_compt'];
            $this->valF['dia_mod_cess_bene_rente'] = $val['dia_mod_cess_bene_rente'];
        if ($val['dia_mod_cess_droit_usa_hab'] == 1 || $val['dia_mod_cess_droit_usa_hab'] == "t" || $val['dia_mod_cess_droit_usa_hab'] == "Oui") {
            $this->valF['dia_mod_cess_droit_usa_hab'] = true;
        } else {
            $this->valF['dia_mod_cess_droit_usa_hab'] = false;
        }
            $this->valF['dia_mod_cess_droit_usa_hab_prec'] = $val['dia_mod_cess_droit_usa_hab_prec'];
            $this->valF['dia_mod_cess_eval_usa_usufruit'] = $val['dia_mod_cess_eval_usa_usufruit'];
        if ($val['dia_mod_cess_vente_nue_prop'] == 1 || $val['dia_mod_cess_vente_nue_prop'] == "t" || $val['dia_mod_cess_vente_nue_prop'] == "Oui") {
            $this->valF['dia_mod_cess_vente_nue_prop'] = true;
        } else {
            $this->valF['dia_mod_cess_vente_nue_prop'] = false;
        }
            $this->valF['dia_mod_cess_vente_nue_prop_prec'] = $val['dia_mod_cess_vente_nue_prop_prec'];
        if ($val['dia_mod_cess_echange'] == 1 || $val['dia_mod_cess_echange'] == "t" || $val['dia_mod_cess_echange'] == "Oui") {
            $this->valF['dia_mod_cess_echange'] = true;
        } else {
            $this->valF['dia_mod_cess_echange'] = false;
        }
            $this->valF['dia_mod_cess_design_bien_recus_ech'] = $val['dia_mod_cess_design_bien_recus_ech'];
            $this->valF['dia_mod_cess_mnt_soulte'] = $val['dia_mod_cess_mnt_soulte'];
            $this->valF['dia_mod_cess_prop_contre_echan'] = $val['dia_mod_cess_prop_contre_echan'];
            $this->valF['dia_mod_cess_apport_societe'] = $val['dia_mod_cess_apport_societe'];
            $this->valF['dia_mod_cess_bene'] = $val['dia_mod_cess_bene'];
            $this->valF['dia_mod_cess_esti_bien'] = $val['dia_mod_cess_esti_bien'];
        if ($val['dia_mod_cess_cess_terr_loc_co'] == 1 || $val['dia_mod_cess_cess_terr_loc_co'] == "t" || $val['dia_mod_cess_cess_terr_loc_co'] == "Oui") {
            $this->valF['dia_mod_cess_cess_terr_loc_co'] = true;
        } else {
            $this->valF['dia_mod_cess_cess_terr_loc_co'] = false;
        }
            $this->valF['dia_mod_cess_esti_terr'] = $val['dia_mod_cess_esti_terr'];
            $this->valF['dia_mod_cess_esti_loc'] = $val['dia_mod_cess_esti_loc'];
        if ($val['dia_mod_cess_esti_imm_loca'] == 1 || $val['dia_mod_cess_esti_imm_loca'] == "t" || $val['dia_mod_cess_esti_imm_loca'] == "Oui") {
            $this->valF['dia_mod_cess_esti_imm_loca'] = true;
        } else {
            $this->valF['dia_mod_cess_esti_imm_loca'] = false;
        }
        if ($val['dia_mod_cess_adju_vol'] == 1 || $val['dia_mod_cess_adju_vol'] == "t" || $val['dia_mod_cess_adju_vol'] == "Oui") {
            $this->valF['dia_mod_cess_adju_vol'] = true;
        } else {
            $this->valF['dia_mod_cess_adju_vol'] = false;
        }
        if ($val['dia_mod_cess_adju_obl'] == 1 || $val['dia_mod_cess_adju_obl'] == "t" || $val['dia_mod_cess_adju_obl'] == "Oui") {
            $this->valF['dia_mod_cess_adju_obl'] = true;
        } else {
            $this->valF['dia_mod_cess_adju_obl'] = false;
        }
        if ($val['dia_mod_cess_adju_fin_indivi'] == 1 || $val['dia_mod_cess_adju_fin_indivi'] == "t" || $val['dia_mod_cess_adju_fin_indivi'] == "Oui") {
            $this->valF['dia_mod_cess_adju_fin_indivi'] = true;
        } else {
            $this->valF['dia_mod_cess_adju_fin_indivi'] = false;
        }
            $this->valF['dia_mod_cess_adju_date_lieu'] = $val['dia_mod_cess_adju_date_lieu'];
            $this->valF['dia_mod_cess_mnt_mise_prix'] = $val['dia_mod_cess_mnt_mise_prix'];
        if ($val['dia_prop_titu_prix_indique'] == 1 || $val['dia_prop_titu_prix_indique'] == "t" || $val['dia_prop_titu_prix_indique'] == "Oui") {
            $this->valF['dia_prop_titu_prix_indique'] = true;
        } else {
            $this->valF['dia_prop_titu_prix_indique'] = false;
        }
        if ($val['dia_prop_recherche_acqu_prix_indique'] == 1 || $val['dia_prop_recherche_acqu_prix_indique'] == "t" || $val['dia_prop_recherche_acqu_prix_indique'] == "Oui") {
            $this->valF['dia_prop_recherche_acqu_prix_indique'] = true;
        } else {
            $this->valF['dia_prop_recherche_acqu_prix_indique'] = false;
        }
            $this->valF['dia_acquereur_prof'] = $val['dia_acquereur_prof'];
            $this->valF['dia_indic_compl_ope'] = $val['dia_indic_compl_ope'];
        if ($val['dia_vente_adju'] == 1 || $val['dia_vente_adju'] == "t" || $val['dia_vente_adju'] == "Oui") {
            $this->valF['dia_vente_adju'] = true;
        } else {
            $this->valF['dia_vente_adju'] = false;
        }
        if ($val['am_terr_res_demon'] == 1 || $val['am_terr_res_demon'] == "t" || $val['am_terr_res_demon'] == "Oui") {
            $this->valF['am_terr_res_demon'] = true;
        } else {
            $this->valF['am_terr_res_demon'] = false;
        }
        if ($val['am_air_terr_res_mob'] == 1 || $val['am_air_terr_res_mob'] == "t" || $val['am_air_terr_res_mob'] == "Oui") {
            $this->valF['am_air_terr_res_mob'] = true;
        } else {
            $this->valF['am_air_terr_res_mob'] = false;
        }
        if (!is_numeric($val['ctx_objet_recours'])) {
            $this->valF['ctx_objet_recours'] = NULL;
        } else {
            $this->valF['ctx_objet_recours'] = $val['ctx_objet_recours'];
        }
        if ($val['ctx_reference_sagace'] == "") {
            $this->valF['ctx_reference_sagace'] = NULL;
        } else {
            $this->valF['ctx_reference_sagace'] = $val['ctx_reference_sagace'];
        }
            $this->valF['ctx_nature_travaux_infra_om_html'] = $val['ctx_nature_travaux_infra_om_html'];
            $this->valF['ctx_synthese_nti'] = $val['ctx_synthese_nti'];
            $this->valF['ctx_article_non_resp_om_html'] = $val['ctx_article_non_resp_om_html'];
            $this->valF['ctx_synthese_anr'] = $val['ctx_synthese_anr'];
        if ($val['ctx_reference_parquet'] == "") {
            $this->valF['ctx_reference_parquet'] = NULL;
        } else {
            $this->valF['ctx_reference_parquet'] = $val['ctx_reference_parquet'];
        }
        if ($val['ctx_element_taxation'] == "") {
            $this->valF['ctx_element_taxation'] = NULL;
        } else {
            $this->valF['ctx_element_taxation'] = $val['ctx_element_taxation'];
        }
        if ($val['ctx_infraction'] == 1 || $val['ctx_infraction'] == "t" || $val['ctx_infraction'] == "Oui") {
            $this->valF['ctx_infraction'] = true;
        } else {
            $this->valF['ctx_infraction'] = false;
        }
        if ($val['ctx_regularisable'] == 1 || $val['ctx_regularisable'] == "t" || $val['ctx_regularisable'] == "Oui") {
            $this->valF['ctx_regularisable'] = true;
        } else {
            $this->valF['ctx_regularisable'] = false;
        }
        if ($val['ctx_reference_courrier'] == "") {
            $this->valF['ctx_reference_courrier'] = NULL;
        } else {
            $this->valF['ctx_reference_courrier'] = $val['ctx_reference_courrier'];
        }
        if ($val['ctx_date_audience'] != "") {
            $this->valF['ctx_date_audience'] = $this->dateDB($val['ctx_date_audience']);
        } else {
            $this->valF['ctx_date_audience'] = NULL;
        }
        if ($val['ctx_date_ajournement'] != "") {
            $this->valF['ctx_date_ajournement'] = $this->dateDB($val['ctx_date_ajournement']);
        } else {
            $this->valF['ctx_date_ajournement'] = NULL;
        }
        if ($val['exo_facul_1'] == 1 || $val['exo_facul_1'] == "t" || $val['exo_facul_1'] == "Oui") {
            $this->valF['exo_facul_1'] = true;
        } else {
            $this->valF['exo_facul_1'] = false;
        }
        if ($val['exo_facul_2'] == 1 || $val['exo_facul_2'] == "t" || $val['exo_facul_2'] == "Oui") {
            $this->valF['exo_facul_2'] = true;
        } else {
            $this->valF['exo_facul_2'] = false;
        }
        if ($val['exo_facul_3'] == 1 || $val['exo_facul_3'] == "t" || $val['exo_facul_3'] == "Oui") {
            $this->valF['exo_facul_3'] = true;
        } else {
            $this->valF['exo_facul_3'] = false;
        }
        if ($val['exo_facul_4'] == 1 || $val['exo_facul_4'] == "t" || $val['exo_facul_4'] == "Oui") {
            $this->valF['exo_facul_4'] = true;
        } else {
            $this->valF['exo_facul_4'] = false;
        }
        if ($val['exo_facul_5'] == 1 || $val['exo_facul_5'] == "t" || $val['exo_facul_5'] == "Oui") {
            $this->valF['exo_facul_5'] = true;
        } else {
            $this->valF['exo_facul_5'] = false;
        }
        if ($val['exo_facul_6'] == 1 || $val['exo_facul_6'] == "t" || $val['exo_facul_6'] == "Oui") {
            $this->valF['exo_facul_6'] = true;
        } else {
            $this->valF['exo_facul_6'] = false;
        }
        if ($val['exo_facul_7'] == 1 || $val['exo_facul_7'] == "t" || $val['exo_facul_7'] == "Oui") {
            $this->valF['exo_facul_7'] = true;
        } else {
            $this->valF['exo_facul_7'] = false;
        }
        if ($val['exo_facul_8'] == 1 || $val['exo_facul_8'] == "t" || $val['exo_facul_8'] == "Oui") {
            $this->valF['exo_facul_8'] = true;
        } else {
            $this->valF['exo_facul_8'] = false;
        }
        if ($val['exo_facul_9'] == 1 || $val['exo_facul_9'] == "t" || $val['exo_facul_9'] == "Oui") {
            $this->valF['exo_facul_9'] = true;
        } else {
            $this->valF['exo_facul_9'] = false;
        }
        if ($val['exo_ta_1'] == 1 || $val['exo_ta_1'] == "t" || $val['exo_ta_1'] == "Oui") {
            $this->valF['exo_ta_1'] = true;
        } else {
            $this->valF['exo_ta_1'] = false;
        }
        if ($val['exo_ta_2'] == 1 || $val['exo_ta_2'] == "t" || $val['exo_ta_2'] == "Oui") {
            $this->valF['exo_ta_2'] = true;
        } else {
            $this->valF['exo_ta_2'] = false;
        }
        if ($val['exo_ta_3'] == 1 || $val['exo_ta_3'] == "t" || $val['exo_ta_3'] == "Oui") {
            $this->valF['exo_ta_3'] = true;
        } else {
            $this->valF['exo_ta_3'] = false;
        }
        if ($val['exo_ta_4'] == 1 || $val['exo_ta_4'] == "t" || $val['exo_ta_4'] == "Oui") {
            $this->valF['exo_ta_4'] = true;
        } else {
            $this->valF['exo_ta_4'] = false;
        }
        if ($val['exo_ta_5'] == 1 || $val['exo_ta_5'] == "t" || $val['exo_ta_5'] == "Oui") {
            $this->valF['exo_ta_5'] = true;
        } else {
            $this->valF['exo_ta_5'] = false;
        }
        if ($val['exo_ta_6'] == 1 || $val['exo_ta_6'] == "t" || $val['exo_ta_6'] == "Oui") {
            $this->valF['exo_ta_6'] = true;
        } else {
            $this->valF['exo_ta_6'] = false;
        }
        if ($val['exo_ta_7'] == 1 || $val['exo_ta_7'] == "t" || $val['exo_ta_7'] == "Oui") {
            $this->valF['exo_ta_7'] = true;
        } else {
            $this->valF['exo_ta_7'] = false;
        }
        if ($val['exo_ta_8'] == 1 || $val['exo_ta_8'] == "t" || $val['exo_ta_8'] == "Oui") {
            $this->valF['exo_ta_8'] = true;
        } else {
            $this->valF['exo_ta_8'] = false;
        }
        if ($val['exo_ta_9'] == 1 || $val['exo_ta_9'] == "t" || $val['exo_ta_9'] == "Oui") {
            $this->valF['exo_ta_9'] = true;
        } else {
            $this->valF['exo_ta_9'] = false;
        }
        if ($val['exo_rap_1'] == 1 || $val['exo_rap_1'] == "t" || $val['exo_rap_1'] == "Oui") {
            $this->valF['exo_rap_1'] = true;
        } else {
            $this->valF['exo_rap_1'] = false;
        }
        if ($val['exo_rap_2'] == 1 || $val['exo_rap_2'] == "t" || $val['exo_rap_2'] == "Oui") {
            $this->valF['exo_rap_2'] = true;
        } else {
            $this->valF['exo_rap_2'] = false;
        }
        if ($val['exo_rap_3'] == 1 || $val['exo_rap_3'] == "t" || $val['exo_rap_3'] == "Oui") {
            $this->valF['exo_rap_3'] = true;
        } else {
            $this->valF['exo_rap_3'] = false;
        }
        if ($val['exo_rap_4'] == 1 || $val['exo_rap_4'] == "t" || $val['exo_rap_4'] == "Oui") {
            $this->valF['exo_rap_4'] = true;
        } else {
            $this->valF['exo_rap_4'] = false;
        }
        if ($val['exo_rap_5'] == 1 || $val['exo_rap_5'] == "t" || $val['exo_rap_5'] == "Oui") {
            $this->valF['exo_rap_5'] = true;
        } else {
            $this->valF['exo_rap_5'] = false;
        }
        if ($val['exo_rap_6'] == 1 || $val['exo_rap_6'] == "t" || $val['exo_rap_6'] == "Oui") {
            $this->valF['exo_rap_6'] = true;
        } else {
            $this->valF['exo_rap_6'] = false;
        }
        if ($val['exo_rap_7'] == 1 || $val['exo_rap_7'] == "t" || $val['exo_rap_7'] == "Oui") {
            $this->valF['exo_rap_7'] = true;
        } else {
            $this->valF['exo_rap_7'] = false;
        }
        if ($val['exo_rap_8'] == 1 || $val['exo_rap_8'] == "t" || $val['exo_rap_8'] == "Oui") {
            $this->valF['exo_rap_8'] = true;
        } else {
            $this->valF['exo_rap_8'] = false;
        }
        if (!is_numeric($val['mtn_exo_ta_part_commu'])) {
            $this->valF['mtn_exo_ta_part_commu'] = NULL;
        } else {
            $this->valF['mtn_exo_ta_part_commu'] = $val['mtn_exo_ta_part_commu'];
        }
        if (!is_numeric($val['mtn_exo_ta_part_depart'])) {
            $this->valF['mtn_exo_ta_part_depart'] = NULL;
        } else {
            $this->valF['mtn_exo_ta_part_depart'] = $val['mtn_exo_ta_part_depart'];
        }
        if (!is_numeric($val['mtn_exo_ta_part_reg'])) {
            $this->valF['mtn_exo_ta_part_reg'] = NULL;
        } else {
            $this->valF['mtn_exo_ta_part_reg'] = $val['mtn_exo_ta_part_reg'];
        }
        if (!is_numeric($val['mtn_exo_rap'])) {
            $this->valF['mtn_exo_rap'] = NULL;
        } else {
            $this->valF['mtn_exo_rap'] = $val['mtn_exo_rap'];
        }
        if ($val['dpc_type'] == "") {
            $this->valF['dpc_type'] = NULL;
        } else {
            $this->valF['dpc_type'] = $val['dpc_type'];
        }
            $this->valF['dpc_desc_actv_ex'] = $val['dpc_desc_actv_ex'];
            $this->valF['dpc_desc_ca'] = $val['dpc_desc_ca'];
            $this->valF['dpc_desc_aut_prec'] = $val['dpc_desc_aut_prec'];
        if ($val['dpc_desig_comm_arti'] == 1 || $val['dpc_desig_comm_arti'] == "t" || $val['dpc_desig_comm_arti'] == "Oui") {
            $this->valF['dpc_desig_comm_arti'] = true;
        } else {
            $this->valF['dpc_desig_comm_arti'] = false;
        }
        if ($val['dpc_desig_loc_hab'] == 1 || $val['dpc_desig_loc_hab'] == "t" || $val['dpc_desig_loc_hab'] == "Oui") {
            $this->valF['dpc_desig_loc_hab'] = true;
        } else {
            $this->valF['dpc_desig_loc_hab'] = false;
        }
        if ($val['dpc_desig_loc_ann'] == 1 || $val['dpc_desig_loc_ann'] == "t" || $val['dpc_desig_loc_ann'] == "Oui") {
            $this->valF['dpc_desig_loc_ann'] = true;
        } else {
            $this->valF['dpc_desig_loc_ann'] = false;
        }
            $this->valF['dpc_desig_loc_ann_prec'] = $val['dpc_desig_loc_ann_prec'];
        if ($val['dpc_bail_comm_date'] != "") {
            $this->valF['dpc_bail_comm_date'] = $this->dateDB($val['dpc_bail_comm_date']);
        } else {
            $this->valF['dpc_bail_comm_date'] = NULL;
        }
            $this->valF['dpc_bail_comm_loyer'] = $val['dpc_bail_comm_loyer'];
            $this->valF['dpc_actv_acqu'] = $val['dpc_actv_acqu'];
            $this->valF['dpc_nb_sala_di'] = $val['dpc_nb_sala_di'];
            $this->valF['dpc_nb_sala_dd'] = $val['dpc_nb_sala_dd'];
            $this->valF['dpc_nb_sala_tc'] = $val['dpc_nb_sala_tc'];
            $this->valF['dpc_nb_sala_tp'] = $val['dpc_nb_sala_tp'];
        if ($val['dpc_moda_cess_vente_am'] == 1 || $val['dpc_moda_cess_vente_am'] == "t" || $val['dpc_moda_cess_vente_am'] == "Oui") {
            $this->valF['dpc_moda_cess_vente_am'] = true;
        } else {
            $this->valF['dpc_moda_cess_vente_am'] = false;
        }
        if ($val['dpc_moda_cess_adj'] == 1 || $val['dpc_moda_cess_adj'] == "t" || $val['dpc_moda_cess_adj'] == "Oui") {
            $this->valF['dpc_moda_cess_adj'] = true;
        } else {
            $this->valF['dpc_moda_cess_adj'] = false;
        }
            $this->valF['dpc_moda_cess_prix'] = $val['dpc_moda_cess_prix'];
        if ($val['dpc_moda_cess_adj_date'] != "") {
            $this->valF['dpc_moda_cess_adj_date'] = $this->dateDB($val['dpc_moda_cess_adj_date']);
        } else {
            $this->valF['dpc_moda_cess_adj_date'] = NULL;
        }
            $this->valF['dpc_moda_cess_adj_prec'] = $val['dpc_moda_cess_adj_prec'];
        if ($val['dpc_moda_cess_paie_comp'] == 1 || $val['dpc_moda_cess_paie_comp'] == "t" || $val['dpc_moda_cess_paie_comp'] == "Oui") {
            $this->valF['dpc_moda_cess_paie_comp'] = true;
        } else {
            $this->valF['dpc_moda_cess_paie_comp'] = false;
        }
        if ($val['dpc_moda_cess_paie_terme'] == 1 || $val['dpc_moda_cess_paie_terme'] == "t" || $val['dpc_moda_cess_paie_terme'] == "Oui") {
            $this->valF['dpc_moda_cess_paie_terme'] = true;
        } else {
            $this->valF['dpc_moda_cess_paie_terme'] = false;
        }
            $this->valF['dpc_moda_cess_paie_terme_prec'] = $val['dpc_moda_cess_paie_terme_prec'];
        if ($val['dpc_moda_cess_paie_nat'] == 1 || $val['dpc_moda_cess_paie_nat'] == "t" || $val['dpc_moda_cess_paie_nat'] == "Oui") {
            $this->valF['dpc_moda_cess_paie_nat'] = true;
        } else {
            $this->valF['dpc_moda_cess_paie_nat'] = false;
        }
        if ($val['dpc_moda_cess_paie_nat_desig_alien'] == 1 || $val['dpc_moda_cess_paie_nat_desig_alien'] == "t" || $val['dpc_moda_cess_paie_nat_desig_alien'] == "Oui") {
            $this->valF['dpc_moda_cess_paie_nat_desig_alien'] = true;
        } else {
            $this->valF['dpc_moda_cess_paie_nat_desig_alien'] = false;
        }
            $this->valF['dpc_moda_cess_paie_nat_desig_alien_prec'] = $val['dpc_moda_cess_paie_nat_desig_alien_prec'];
        if ($val['dpc_moda_cess_paie_nat_eval'] == 1 || $val['dpc_moda_cess_paie_nat_eval'] == "t" || $val['dpc_moda_cess_paie_nat_eval'] == "Oui") {
            $this->valF['dpc_moda_cess_paie_nat_eval'] = true;
        } else {
            $this->valF['dpc_moda_cess_paie_nat_eval'] = false;
        }
            $this->valF['dpc_moda_cess_paie_nat_eval_prec'] = $val['dpc_moda_cess_paie_nat_eval_prec'];
        if ($val['dpc_moda_cess_paie_aut'] == 1 || $val['dpc_moda_cess_paie_aut'] == "t" || $val['dpc_moda_cess_paie_aut'] == "Oui") {
            $this->valF['dpc_moda_cess_paie_aut'] = true;
        } else {
            $this->valF['dpc_moda_cess_paie_aut'] = false;
        }
            $this->valF['dpc_moda_cess_paie_aut_prec'] = $val['dpc_moda_cess_paie_aut_prec'];
        if ($val['dpc_ss_signe_demande_acqu'] == 1 || $val['dpc_ss_signe_demande_acqu'] == "t" || $val['dpc_ss_signe_demande_acqu'] == "Oui") {
            $this->valF['dpc_ss_signe_demande_acqu'] = true;
        } else {
            $this->valF['dpc_ss_signe_demande_acqu'] = false;
        }
        if ($val['dpc_ss_signe_recher_trouv_acqu'] == 1 || $val['dpc_ss_signe_recher_trouv_acqu'] == "t" || $val['dpc_ss_signe_recher_trouv_acqu'] == "Oui") {
            $this->valF['dpc_ss_signe_recher_trouv_acqu'] = true;
        } else {
            $this->valF['dpc_ss_signe_recher_trouv_acqu'] = false;
        }
        if ($val['dpc_notif_adr_prop'] == 1 || $val['dpc_notif_adr_prop'] == "t" || $val['dpc_notif_adr_prop'] == "Oui") {
            $this->valF['dpc_notif_adr_prop'] = true;
        } else {
            $this->valF['dpc_notif_adr_prop'] = false;
        }
        if ($val['dpc_notif_adr_manda'] == 1 || $val['dpc_notif_adr_manda'] == "t" || $val['dpc_notif_adr_manda'] == "Oui") {
            $this->valF['dpc_notif_adr_manda'] = true;
        } else {
            $this->valF['dpc_notif_adr_manda'] = false;
        }
            $this->valF['dpc_obs'] = $val['dpc_obs'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("donnees_techniques", "hidden");
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("lot", $this->retourformulaire)) {
                $form->setType("lot", "selecthiddenstatic");
            } else {
                $form->setType("lot", "select");
            }
            $form->setType("am_lotiss", "checkbox");
            $form->setType("am_autre_div", "checkbox");
            $form->setType("am_camping", "checkbox");
            $form->setType("am_caravane", "checkbox");
            $form->setType("am_carav_duree", "text");
            $form->setType("am_statio", "checkbox");
            $form->setType("am_statio_cont", "text");
            $form->setType("am_affou_exhau", "checkbox");
            $form->setType("am_affou_exhau_sup", "text");
            $form->setType("am_affou_prof", "text");
            $form->setType("am_exhau_haut", "text");
            $form->setType("am_coupe_abat", "checkbox");
            $form->setType("am_prot_plu", "checkbox");
            $form->setType("am_prot_muni", "checkbox");
            $form->setType("am_mobil_voyage", "checkbox");
            $form->setType("am_aire_voyage", "checkbox");
            $form->setType("am_rememb_afu", "checkbox");
            $form->setType("am_parc_resid_loi", "checkbox");
            $form->setType("am_sport_moto", "checkbox");
            $form->setType("am_sport_attrac", "checkbox");
            $form->setType("am_sport_golf", "checkbox");
            $form->setType("am_mob_art", "checkbox");
            $form->setType("am_modif_voie_esp", "checkbox");
            $form->setType("am_plant_voie_esp", "checkbox");
            $form->setType("am_chem_ouv_esp", "checkbox");
            $form->setType("am_agri_peche", "checkbox");
            $form->setType("am_crea_voie", "checkbox");
            $form->setType("am_modif_voie_exist", "checkbox");
            $form->setType("am_crea_esp_sauv", "checkbox");
            $form->setType("am_crea_esp_class", "checkbox");
            $form->setType("am_projet_desc", "textarea");
            $form->setType("am_terr_surf", "text");
            $form->setType("am_tranche_desc", "textarea");
            $form->setType("am_lot_max_nb", "text");
            $form->setType("am_lot_max_shon", "text");
            $form->setType("am_lot_cstr_cos", "checkbox");
            $form->setType("am_lot_cstr_plan", "checkbox");
            $form->setType("am_lot_cstr_vente", "checkbox");
            $form->setType("am_lot_fin_diff", "checkbox");
            $form->setType("am_lot_consign", "checkbox");
            $form->setType("am_lot_gar_achev", "checkbox");
            $form->setType("am_lot_vente_ant", "checkbox");
            $form->setType("am_empl_nb", "text");
            $form->setType("am_tente_nb", "text");
            $form->setType("am_carav_nb", "text");
            $form->setType("am_mobil_nb", "text");
            $form->setType("am_pers_nb", "text");
            $form->setType("am_empl_hll_nb", "text");
            $form->setType("am_hll_shon", "text");
            $form->setType("am_periode_exploit", "textarea");
            $form->setType("am_exist_agrand", "checkbox");
            $form->setType("am_exist_date", "date");
            $form->setType("am_exist_num", "text");
            $form->setType("am_exist_nb_avant", "text");
            $form->setType("am_exist_nb_apres", "text");
            $form->setType("am_coupe_bois", "checkbox");
            $form->setType("am_coupe_parc", "checkbox");
            $form->setType("am_coupe_align", "checkbox");
            $form->setType("am_coupe_ess", "text");
            $form->setType("am_coupe_age", "text");
            $form->setType("am_coupe_dens", "text");
            $form->setType("am_coupe_qual", "text");
            $form->setType("am_coupe_trait", "text");
            $form->setType("am_coupe_autr", "text");
            $form->setType("co_archi_recours", "checkbox");
            $form->setType("co_cstr_nouv", "checkbox");
            $form->setType("co_cstr_exist", "checkbox");
            $form->setType("co_cloture", "checkbox");
            $form->setType("co_elec_tension", "text");
            $form->setType("co_div_terr", "checkbox");
            $form->setType("co_projet_desc", "textarea");
            $form->setType("co_anx_pisc", "checkbox");
            $form->setType("co_anx_gara", "checkbox");
            $form->setType("co_anx_veran", "checkbox");
            $form->setType("co_anx_abri", "checkbox");
            $form->setType("co_anx_autr", "checkbox");
            $form->setType("co_anx_autr_desc", "textarea");
            $form->setType("co_tot_log_nb", "text");
            $form->setType("co_tot_ind_nb", "text");
            $form->setType("co_tot_coll_nb", "text");
            $form->setType("co_mais_piece_nb", "text");
            $form->setType("co_mais_niv_nb", "text");
            $form->setType("co_fin_lls_nb", "text");
            $form->setType("co_fin_aa_nb", "text");
            $form->setType("co_fin_ptz_nb", "text");
            $form->setType("co_fin_autr_nb", "text");
            $form->setType("co_fin_autr_desc", "textarea");
            $form->setType("co_mais_contrat_ind", "checkbox");
            $form->setType("co_uti_pers", "checkbox");
            $form->setType("co_uti_vente", "checkbox");
            $form->setType("co_uti_loc", "checkbox");
            $form->setType("co_uti_princ", "checkbox");
            $form->setType("co_uti_secon", "checkbox");
            $form->setType("co_resid_agees", "checkbox");
            $form->setType("co_resid_etud", "checkbox");
            $form->setType("co_resid_tourism", "checkbox");
            $form->setType("co_resid_hot_soc", "checkbox");
            $form->setType("co_resid_soc", "checkbox");
            $form->setType("co_resid_hand", "checkbox");
            $form->setType("co_resid_autr", "checkbox");
            $form->setType("co_resid_autr_desc", "textarea");
            $form->setType("co_foyer_chamb_nb", "text");
            $form->setType("co_log_1p_nb", "text");
            $form->setType("co_log_2p_nb", "text");
            $form->setType("co_log_3p_nb", "text");
            $form->setType("co_log_4p_nb", "text");
            $form->setType("co_log_5p_nb", "text");
            $form->setType("co_log_6p_nb", "text");
            $form->setType("co_bat_niv_nb", "text");
            $form->setType("co_trx_exten", "checkbox");
            $form->setType("co_trx_surelev", "checkbox");
            $form->setType("co_trx_nivsup", "checkbox");
            $form->setType("co_demont_periode", "textarea");
            $form->setType("co_sp_transport", "checkbox");
            $form->setType("co_sp_enseign", "checkbox");
            $form->setType("co_sp_act_soc", "checkbox");
            $form->setType("co_sp_ouvr_spe", "checkbox");
            $form->setType("co_sp_sante", "checkbox");
            $form->setType("co_sp_culture", "checkbox");
            $form->setType("co_statio_avt_nb", "text");
            $form->setType("co_statio_apr_nb", "text");
            $form->setType("co_statio_adr", "textarea");
            $form->setType("co_statio_place_nb", "text");
            $form->setType("co_statio_tot_surf", "text");
            $form->setType("co_statio_tot_shob", "text");
            $form->setType("co_statio_comm_cin_surf", "text");
            $form->setType("su_avt_shon1", "text");
            $form->setType("su_avt_shon2", "text");
            $form->setType("su_avt_shon3", "text");
            $form->setType("su_avt_shon4", "text");
            $form->setType("su_avt_shon5", "text");
            $form->setType("su_avt_shon6", "text");
            $form->setType("su_avt_shon7", "text");
            $form->setType("su_avt_shon8", "text");
            $form->setType("su_avt_shon9", "text");
            $form->setType("su_cstr_shon1", "text");
            $form->setType("su_cstr_shon2", "text");
            $form->setType("su_cstr_shon3", "text");
            $form->setType("su_cstr_shon4", "text");
            $form->setType("su_cstr_shon5", "text");
            $form->setType("su_cstr_shon6", "text");
            $form->setType("su_cstr_shon7", "text");
            $form->setType("su_cstr_shon8", "text");
            $form->setType("su_cstr_shon9", "text");
            $form->setType("su_trsf_shon1", "text");
            $form->setType("su_trsf_shon2", "text");
            $form->setType("su_trsf_shon3", "text");
            $form->setType("su_trsf_shon4", "text");
            $form->setType("su_trsf_shon5", "text");
            $form->setType("su_trsf_shon6", "text");
            $form->setType("su_trsf_shon7", "text");
            $form->setType("su_trsf_shon8", "text");
            $form->setType("su_trsf_shon9", "text");
            $form->setType("su_chge_shon1", "text");
            $form->setType("su_chge_shon2", "text");
            $form->setType("su_chge_shon3", "text");
            $form->setType("su_chge_shon4", "text");
            $form->setType("su_chge_shon5", "text");
            $form->setType("su_chge_shon6", "text");
            $form->setType("su_chge_shon7", "text");
            $form->setType("su_chge_shon8", "text");
            $form->setType("su_chge_shon9", "text");
            $form->setType("su_demo_shon1", "text");
            $form->setType("su_demo_shon2", "text");
            $form->setType("su_demo_shon3", "text");
            $form->setType("su_demo_shon4", "text");
            $form->setType("su_demo_shon5", "text");
            $form->setType("su_demo_shon6", "text");
            $form->setType("su_demo_shon7", "text");
            $form->setType("su_demo_shon8", "text");
            $form->setType("su_demo_shon9", "text");
            $form->setType("su_sup_shon1", "text");
            $form->setType("su_sup_shon2", "text");
            $form->setType("su_sup_shon3", "text");
            $form->setType("su_sup_shon4", "text");
            $form->setType("su_sup_shon5", "text");
            $form->setType("su_sup_shon6", "text");
            $form->setType("su_sup_shon7", "text");
            $form->setType("su_sup_shon8", "text");
            $form->setType("su_sup_shon9", "text");
            $form->setType("su_tot_shon1", "text");
            $form->setType("su_tot_shon2", "text");
            $form->setType("su_tot_shon3", "text");
            $form->setType("su_tot_shon4", "text");
            $form->setType("su_tot_shon5", "text");
            $form->setType("su_tot_shon6", "text");
            $form->setType("su_tot_shon7", "text");
            $form->setType("su_tot_shon8", "text");
            $form->setType("su_tot_shon9", "text");
            $form->setType("su_avt_shon_tot", "text");
            $form->setType("su_cstr_shon_tot", "text");
            $form->setType("su_trsf_shon_tot", "text");
            $form->setType("su_chge_shon_tot", "text");
            $form->setType("su_demo_shon_tot", "text");
            $form->setType("su_sup_shon_tot", "text");
            $form->setType("su_tot_shon_tot", "text");
            $form->setType("dm_constr_dates", "textarea");
            $form->setType("dm_total", "checkbox");
            $form->setType("dm_partiel", "checkbox");
            $form->setType("dm_projet_desc", "textarea");
            $form->setType("dm_tot_log_nb", "text");
            $form->setType("tax_surf_tot", "text");
            $form->setType("tax_surf", "text");
            $form->setType("tax_surf_suppr_mod", "text");
            $form->setType("tax_su_princ_log_nb1", "text");
            $form->setType("tax_su_princ_log_nb2", "text");
            $form->setType("tax_su_princ_log_nb3", "text");
            $form->setType("tax_su_princ_log_nb4", "text");
            $form->setType("tax_su_princ_log_nb_tot1", "text");
            $form->setType("tax_su_princ_log_nb_tot2", "text");
            $form->setType("tax_su_princ_log_nb_tot3", "text");
            $form->setType("tax_su_princ_log_nb_tot4", "text");
            $form->setType("tax_su_princ_surf1", "text");
            $form->setType("tax_su_princ_surf2", "text");
            $form->setType("tax_su_princ_surf3", "text");
            $form->setType("tax_su_princ_surf4", "text");
            $form->setType("tax_su_princ_surf_sup1", "text");
            $form->setType("tax_su_princ_surf_sup2", "text");
            $form->setType("tax_su_princ_surf_sup3", "text");
            $form->setType("tax_su_princ_surf_sup4", "text");
            $form->setType("tax_su_heber_log_nb1", "text");
            $form->setType("tax_su_heber_log_nb2", "text");
            $form->setType("tax_su_heber_log_nb3", "text");
            $form->setType("tax_su_heber_log_nb_tot1", "text");
            $form->setType("tax_su_heber_log_nb_tot2", "text");
            $form->setType("tax_su_heber_log_nb_tot3", "text");
            $form->setType("tax_su_heber_surf1", "text");
            $form->setType("tax_su_heber_surf2", "text");
            $form->setType("tax_su_heber_surf3", "text");
            $form->setType("tax_su_heber_surf_sup1", "text");
            $form->setType("tax_su_heber_surf_sup2", "text");
            $form->setType("tax_su_heber_surf_sup3", "text");
            $form->setType("tax_su_secon_log_nb", "text");
            $form->setType("tax_su_tot_log_nb", "text");
            $form->setType("tax_su_secon_log_nb_tot", "text");
            $form->setType("tax_su_tot_log_nb_tot", "text");
            $form->setType("tax_su_secon_surf", "text");
            $form->setType("tax_su_tot_surf", "text");
            $form->setType("tax_su_secon_surf_sup", "text");
            $form->setType("tax_su_tot_surf_sup", "text");
            $form->setType("tax_ext_pret", "checkbox");
            $form->setType("tax_ext_desc", "textarea");
            $form->setType("tax_surf_tax_exist_cons", "text");
            $form->setType("tax_log_exist_nb", "text");
            $form->setType("tax_am_statio_ext", "text");
            $form->setType("tax_sup_bass_pisc", "text");
            $form->setType("tax_empl_ten_carav_mobil_nb", "text");
            $form->setType("tax_empl_hll_nb", "text");
            $form->setType("tax_eol_haut_nb", "text");
            $form->setType("tax_pann_volt_sup", "text");
            $form->setType("tax_am_statio_ext_sup", "text");
            $form->setType("tax_sup_bass_pisc_sup", "text");
            $form->setType("tax_empl_ten_carav_mobil_nb_sup", "text");
            $form->setType("tax_empl_hll_nb_sup", "text");
            $form->setType("tax_eol_haut_nb_sup", "text");
            $form->setType("tax_pann_volt_sup_sup", "text");
            $form->setType("tax_trx_presc_ppr", "checkbox");
            $form->setType("tax_monu_hist", "checkbox");
            $form->setType("tax_comm_nb", "text");
            $form->setType("tax_su_non_habit_surf1", "text");
            $form->setType("tax_su_non_habit_surf2", "text");
            $form->setType("tax_su_non_habit_surf3", "text");
            $form->setType("tax_su_non_habit_surf4", "text");
            $form->setType("tax_su_non_habit_surf5", "text");
            $form->setType("tax_su_non_habit_surf6", "text");
            $form->setType("tax_su_non_habit_surf7", "text");
            $form->setType("tax_su_non_habit_surf_sup1", "text");
            $form->setType("tax_su_non_habit_surf_sup2", "text");
            $form->setType("tax_su_non_habit_surf_sup3", "text");
            $form->setType("tax_su_non_habit_surf_sup4", "text");
            $form->setType("tax_su_non_habit_surf_sup5", "text");
            $form->setType("tax_su_non_habit_surf_sup6", "text");
            $form->setType("tax_su_non_habit_surf_sup7", "text");
            $form->setType("vsd_surf_planch_smd", "checkbox");
            $form->setType("vsd_unit_fonc_sup", "text");
            $form->setType("vsd_unit_fonc_constr_sup", "text");
            $form->setType("vsd_val_terr", "text");
            $form->setType("vsd_const_sxist_non_dem_surf", "text");
            $form->setType("vsd_rescr_fisc", "date");
            $form->setType("pld_val_terr", "text");
            $form->setType("pld_const_exist_dem", "checkbox");
            $form->setType("pld_const_exist_dem_surf", "text");
            $form->setType("code_cnil", "checkbox");
            $form->setType("terr_juri_titul", "text");
            $form->setType("terr_juri_lot", "text");
            $form->setType("terr_juri_zac", "text");
            $form->setType("terr_juri_afu", "text");
            $form->setType("terr_juri_pup", "text");
            $form->setType("terr_juri_oin", "text");
            $form->setType("terr_juri_desc", "textarea");
            $form->setType("terr_div_surf_etab", "text");
            $form->setType("terr_div_surf_av_div", "text");
            $form->setType("doc_date", "date");
            $form->setType("doc_tot_trav", "checkbox");
            $form->setType("doc_tranche_trav", "checkbox");
            $form->setType("doc_tranche_trav_desc", "textarea");
            $form->setType("doc_surf", "text");
            $form->setType("doc_nb_log", "text");
            $form->setType("doc_nb_log_indiv", "text");
            $form->setType("doc_nb_log_coll", "text");
            $form->setType("doc_nb_log_lls", "text");
            $form->setType("doc_nb_log_aa", "text");
            $form->setType("doc_nb_log_ptz", "text");
            $form->setType("doc_nb_log_autre", "text");
            $form->setType("daact_date", "date");
            $form->setType("daact_date_chgmt_dest", "date");
            $form->setType("daact_tot_trav", "checkbox");
            $form->setType("daact_tranche_trav", "checkbox");
            $form->setType("daact_tranche_trav_desc", "textarea");
            $form->setType("daact_surf", "text");
            $form->setType("daact_nb_log", "text");
            $form->setType("daact_nb_log_indiv", "text");
            $form->setType("daact_nb_log_coll", "text");
            $form->setType("daact_nb_log_lls", "text");
            $form->setType("daact_nb_log_aa", "text");
            $form->setType("daact_nb_log_ptz", "text");
            $form->setType("daact_nb_log_autre", "text");
            if ($this->is_in_context_of_foreign_key("dossier_autorisation", $this->retourformulaire)) {
                $form->setType("dossier_autorisation", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation", "select");
            }
            $form->setType("am_div_mun", "checkbox");
            $form->setType("co_perf_energ", "text");
            if ($this->is_in_context_of_foreign_key("architecte", $this->retourformulaire)) {
                $form->setType("architecte", "selecthiddenstatic");
            } else {
                $form->setType("architecte", "select");
            }
            $form->setType("co_statio_avt_shob", "text");
            $form->setType("co_statio_apr_shob", "text");
            $form->setType("co_statio_avt_surf", "text");
            $form->setType("co_statio_apr_surf", "text");
            $form->setType("co_trx_amgt", "text");
            $form->setType("co_modif_aspect", "text");
            $form->setType("co_modif_struct", "text");
            $form->setType("co_ouvr_elec", "checkbox");
            $form->setType("co_ouvr_infra", "checkbox");
            $form->setType("co_trx_imm", "text");
            $form->setType("co_cstr_shob", "text");
            $form->setType("am_voyage_deb", "text");
            $form->setType("am_voyage_fin", "text");
            $form->setType("am_modif_amgt", "text");
            $form->setType("am_lot_max_shob", "text");
            $form->setType("mod_desc", "text");
            $form->setType("tr_total", "text");
            $form->setType("tr_partiel", "text");
            $form->setType("tr_desc", "text");
            $form->setType("avap_co_elt_pro", "checkbox");
            $form->setType("avap_nouv_haut_surf", "checkbox");
            $form->setType("avap_co_clot", "text");
            $form->setType("avap_aut_coup_aba_arb", "text");
            $form->setType("avap_ouv_infra", "text");
            $form->setType("avap_aut_inst_mob", "text");
            $form->setType("avap_aut_plant", "text");
            $form->setType("avap_aut_auv_elec", "text");
            $form->setType("tax_dest_loc_tr", "text");
            $form->setType("ope_proj_desc", "textarea");
            $form->setType("tax_surf_tot_cstr", "text");
            $form->setType("cerfa", "text");
            $form->setType("tax_surf_loc_stat", "text");
            $form->setType("tax_su_princ_surf_stat1", "text");
            $form->setType("tax_su_princ_surf_stat2", "text");
            $form->setType("tax_su_princ_surf_stat3", "text");
            $form->setType("tax_su_princ_surf_stat4", "text");
            $form->setType("tax_su_secon_surf_stat", "text");
            $form->setType("tax_su_heber_surf_stat1", "text");
            $form->setType("tax_su_heber_surf_stat2", "text");
            $form->setType("tax_su_heber_surf_stat3", "text");
            $form->setType("tax_su_tot_surf_stat", "text");
            $form->setType("tax_su_non_habit_surf_stat1", "text");
            $form->setType("tax_su_non_habit_surf_stat2", "text");
            $form->setType("tax_su_non_habit_surf_stat3", "text");
            $form->setType("tax_su_non_habit_surf_stat4", "text");
            $form->setType("tax_su_non_habit_surf_stat5", "text");
            $form->setType("tax_su_non_habit_surf_stat6", "text");
            $form->setType("tax_su_non_habit_surf_stat7", "text");
            $form->setType("tax_su_parc_statio_expl_comm_surf", "text");
            $form->setType("tax_log_ap_trvx_nb", "text");
            $form->setType("tax_am_statio_ext_cr", "text");
            $form->setType("tax_sup_bass_pisc_cr", "text");
            $form->setType("tax_empl_ten_carav_mobil_nb_cr", "text");
            $form->setType("tax_empl_hll_nb_cr", "text");
            $form->setType("tax_eol_haut_nb_cr", "text");
            $form->setType("tax_pann_volt_sup_cr", "text");
            $form->setType("tax_surf_loc_arch", "text");
            $form->setType("tax_surf_pisc_arch", "text");
            $form->setType("tax_am_statio_ext_arch", "text");
            $form->setType("tax_empl_ten_carav_mobil_nb_arch", "text");
            $form->setType("tax_empl_hll_nb_arch", "text");
            $form->setType("tax_eol_haut_nb_arch", "text");
            $form->setType("ope_proj_div_co", "checkbox");
            $form->setType("ope_proj_div_contr", "checkbox");
            $form->setType("tax_desc", "textarea");
            $form->setType("erp_cstr_neuve", "checkbox");
            $form->setType("erp_trvx_acc", "checkbox");
            $form->setType("erp_extension", "checkbox");
            $form->setType("erp_rehab", "checkbox");
            $form->setType("erp_trvx_am", "checkbox");
            $form->setType("erp_vol_nouv_exist", "checkbox");
            $form->setType("erp_loc_eff1", "text");
            $form->setType("erp_loc_eff2", "text");
            $form->setType("erp_loc_eff3", "text");
            $form->setType("erp_loc_eff4", "text");
            $form->setType("erp_loc_eff5", "text");
            $form->setType("erp_loc_eff_tot", "text");
            $form->setType("erp_public_eff1", "text");
            $form->setType("erp_public_eff2", "text");
            $form->setType("erp_public_eff3", "text");
            $form->setType("erp_public_eff4", "text");
            $form->setType("erp_public_eff5", "text");
            $form->setType("erp_public_eff_tot", "text");
            $form->setType("erp_perso_eff1", "text");
            $form->setType("erp_perso_eff2", "text");
            $form->setType("erp_perso_eff3", "text");
            $form->setType("erp_perso_eff4", "text");
            $form->setType("erp_perso_eff5", "text");
            $form->setType("erp_perso_eff_tot", "text");
            $form->setType("erp_tot_eff1", "text");
            $form->setType("erp_tot_eff2", "text");
            $form->setType("erp_tot_eff3", "text");
            $form->setType("erp_tot_eff4", "text");
            $form->setType("erp_tot_eff5", "text");
            $form->setType("erp_tot_eff_tot", "text");
            $form->setType("erp_class_cat", "text");
            $form->setType("erp_class_type", "text");
            $form->setType("tax_surf_abr_jard_pig_colom", "text");
            $form->setType("tax_su_non_habit_abr_jard_pig_colom", "text");
            $form->setType("dia_imm_non_bati", "checkbox");
            $form->setType("dia_imm_bati_terr_propr", "checkbox");
            $form->setType("dia_imm_bati_terr_autr", "checkbox");
            $form->setType("dia_imm_bati_terr_autr_desc", "textarea");
            $form->setType("dia_bat_copro", "checkbox");
            $form->setType("dia_bat_copro_desc", "textarea");
            $form->setType("dia_lot_numero", "textarea");
            $form->setType("dia_lot_bat", "textarea");
            $form->setType("dia_lot_etage", "textarea");
            $form->setType("dia_lot_quote_part", "textarea");
            $form->setType("dia_us_hab", "checkbox");
            $form->setType("dia_us_pro", "checkbox");
            $form->setType("dia_us_mixte", "checkbox");
            $form->setType("dia_us_comm", "checkbox");
            $form->setType("dia_us_agr", "checkbox");
            $form->setType("dia_us_autre", "checkbox");
            $form->setType("dia_us_autre_prec", "textarea");
            $form->setType("dia_occ_prop", "checkbox");
            $form->setType("dia_occ_loc", "checkbox");
            $form->setType("dia_occ_sans_occ", "checkbox");
            $form->setType("dia_occ_autre", "checkbox");
            $form->setType("dia_occ_autre_prec", "textarea");
            $form->setType("dia_mod_cess_prix_vente", "text");
            $form->setType("dia_mod_cess_prix_vente_mob", "text");
            $form->setType("dia_mod_cess_prix_vente_cheptel", "text");
            $form->setType("dia_mod_cess_prix_vente_recol", "text");
            $form->setType("dia_mod_cess_prix_vente_autre", "text");
            $form->setType("dia_mod_cess_commi", "checkbox");
            $form->setType("dia_mod_cess_commi_ttc", "text");
            $form->setType("dia_mod_cess_commi_ht", "text");
            $form->setType("dia_acquereur_nom_prenom", "text");
            $form->setType("dia_acquereur_adr_num_voie", "text");
            $form->setType("dia_acquereur_adr_ext", "text");
            $form->setType("dia_acquereur_adr_type_voie", "text");
            $form->setType("dia_acquereur_adr_nom_voie", "text");
            $form->setType("dia_acquereur_adr_lieu_dit_bp", "text");
            $form->setType("dia_acquereur_adr_cp", "text");
            $form->setType("dia_acquereur_adr_localite", "text");
            $form->setType("dia_observation", "textarea");
            $form->setType("su2_avt_shon1", "text");
            $form->setType("su2_avt_shon2", "text");
            $form->setType("su2_avt_shon3", "text");
            $form->setType("su2_avt_shon4", "text");
            $form->setType("su2_avt_shon5", "text");
            $form->setType("su2_avt_shon6", "text");
            $form->setType("su2_avt_shon7", "text");
            $form->setType("su2_avt_shon8", "text");
            $form->setType("su2_avt_shon9", "text");
            $form->setType("su2_avt_shon10", "text");
            $form->setType("su2_avt_shon11", "text");
            $form->setType("su2_avt_shon12", "text");
            $form->setType("su2_avt_shon13", "text");
            $form->setType("su2_avt_shon14", "text");
            $form->setType("su2_avt_shon15", "text");
            $form->setType("su2_avt_shon16", "text");
            $form->setType("su2_avt_shon17", "text");
            $form->setType("su2_avt_shon18", "text");
            $form->setType("su2_avt_shon19", "text");
            $form->setType("su2_avt_shon20", "text");
            $form->setType("su2_avt_shon_tot", "text");
            $form->setType("su2_cstr_shon1", "text");
            $form->setType("su2_cstr_shon2", "text");
            $form->setType("su2_cstr_shon3", "text");
            $form->setType("su2_cstr_shon4", "text");
            $form->setType("su2_cstr_shon5", "text");
            $form->setType("su2_cstr_shon6", "text");
            $form->setType("su2_cstr_shon7", "text");
            $form->setType("su2_cstr_shon8", "text");
            $form->setType("su2_cstr_shon9", "text");
            $form->setType("su2_cstr_shon10", "text");
            $form->setType("su2_cstr_shon11", "text");
            $form->setType("su2_cstr_shon12", "text");
            $form->setType("su2_cstr_shon13", "text");
            $form->setType("su2_cstr_shon14", "text");
            $form->setType("su2_cstr_shon15", "text");
            $form->setType("su2_cstr_shon16", "text");
            $form->setType("su2_cstr_shon17", "text");
            $form->setType("su2_cstr_shon18", "text");
            $form->setType("su2_cstr_shon19", "text");
            $form->setType("su2_cstr_shon20", "text");
            $form->setType("su2_cstr_shon_tot", "text");
            $form->setType("su2_chge_shon1", "text");
            $form->setType("su2_chge_shon2", "text");
            $form->setType("su2_chge_shon3", "text");
            $form->setType("su2_chge_shon4", "text");
            $form->setType("su2_chge_shon5", "text");
            $form->setType("su2_chge_shon6", "text");
            $form->setType("su2_chge_shon7", "text");
            $form->setType("su2_chge_shon8", "text");
            $form->setType("su2_chge_shon9", "text");
            $form->setType("su2_chge_shon10", "text");
            $form->setType("su2_chge_shon11", "text");
            $form->setType("su2_chge_shon12", "text");
            $form->setType("su2_chge_shon13", "text");
            $form->setType("su2_chge_shon14", "text");
            $form->setType("su2_chge_shon15", "text");
            $form->setType("su2_chge_shon16", "text");
            $form->setType("su2_chge_shon17", "text");
            $form->setType("su2_chge_shon18", "text");
            $form->setType("su2_chge_shon19", "text");
            $form->setType("su2_chge_shon20", "text");
            $form->setType("su2_chge_shon_tot", "text");
            $form->setType("su2_demo_shon1", "text");
            $form->setType("su2_demo_shon2", "text");
            $form->setType("su2_demo_shon3", "text");
            $form->setType("su2_demo_shon4", "text");
            $form->setType("su2_demo_shon5", "text");
            $form->setType("su2_demo_shon6", "text");
            $form->setType("su2_demo_shon7", "text");
            $form->setType("su2_demo_shon8", "text");
            $form->setType("su2_demo_shon9", "text");
            $form->setType("su2_demo_shon10", "text");
            $form->setType("su2_demo_shon11", "text");
            $form->setType("su2_demo_shon12", "text");
            $form->setType("su2_demo_shon13", "text");
            $form->setType("su2_demo_shon14", "text");
            $form->setType("su2_demo_shon15", "text");
            $form->setType("su2_demo_shon16", "text");
            $form->setType("su2_demo_shon17", "text");
            $form->setType("su2_demo_shon18", "text");
            $form->setType("su2_demo_shon19", "text");
            $form->setType("su2_demo_shon20", "text");
            $form->setType("su2_demo_shon_tot", "text");
            $form->setType("su2_sup_shon1", "text");
            $form->setType("su2_sup_shon2", "text");
            $form->setType("su2_sup_shon3", "text");
            $form->setType("su2_sup_shon4", "text");
            $form->setType("su2_sup_shon5", "text");
            $form->setType("su2_sup_shon6", "text");
            $form->setType("su2_sup_shon7", "text");
            $form->setType("su2_sup_shon8", "text");
            $form->setType("su2_sup_shon9", "text");
            $form->setType("su2_sup_shon10", "text");
            $form->setType("su2_sup_shon11", "text");
            $form->setType("su2_sup_shon12", "text");
            $form->setType("su2_sup_shon13", "text");
            $form->setType("su2_sup_shon14", "text");
            $form->setType("su2_sup_shon15", "text");
            $form->setType("su2_sup_shon16", "text");
            $form->setType("su2_sup_shon17", "text");
            $form->setType("su2_sup_shon18", "text");
            $form->setType("su2_sup_shon19", "text");
            $form->setType("su2_sup_shon20", "text");
            $form->setType("su2_sup_shon_tot", "text");
            $form->setType("su2_tot_shon1", "text");
            $form->setType("su2_tot_shon2", "text");
            $form->setType("su2_tot_shon3", "text");
            $form->setType("su2_tot_shon4", "text");
            $form->setType("su2_tot_shon5", "text");
            $form->setType("su2_tot_shon6", "text");
            $form->setType("su2_tot_shon7", "text");
            $form->setType("su2_tot_shon8", "text");
            $form->setType("su2_tot_shon9", "text");
            $form->setType("su2_tot_shon10", "text");
            $form->setType("su2_tot_shon11", "text");
            $form->setType("su2_tot_shon12", "text");
            $form->setType("su2_tot_shon13", "text");
            $form->setType("su2_tot_shon14", "text");
            $form->setType("su2_tot_shon15", "text");
            $form->setType("su2_tot_shon16", "text");
            $form->setType("su2_tot_shon17", "text");
            $form->setType("su2_tot_shon18", "text");
            $form->setType("su2_tot_shon19", "text");
            $form->setType("su2_tot_shon20", "text");
            $form->setType("su2_tot_shon_tot", "text");
            $form->setType("dia_occ_sol_su_terre", "textarea");
            $form->setType("dia_occ_sol_su_pres", "textarea");
            $form->setType("dia_occ_sol_su_verger", "textarea");
            $form->setType("dia_occ_sol_su_vigne", "textarea");
            $form->setType("dia_occ_sol_su_bois", "textarea");
            $form->setType("dia_occ_sol_su_lande", "textarea");
            $form->setType("dia_occ_sol_su_carriere", "textarea");
            $form->setType("dia_occ_sol_su_eau_cadastree", "textarea");
            $form->setType("dia_occ_sol_su_jardin", "textarea");
            $form->setType("dia_occ_sol_su_terr_batir", "textarea");
            $form->setType("dia_occ_sol_su_terr_agr", "textarea");
            $form->setType("dia_occ_sol_su_sol", "textarea");
            $form->setType("dia_bati_vend_tot", "checkbox");
            $form->setType("dia_bati_vend_tot_txt", "textarea");
            $form->setType("dia_su_co_sol", "textarea");
            $form->setType("dia_su_util_hab", "textarea");
            $form->setType("dia_nb_niv", "textarea");
            $form->setType("dia_nb_appart", "textarea");
            $form->setType("dia_nb_autre_loc", "textarea");
            $form->setType("dia_vente_lot_volume", "checkbox");
            $form->setType("dia_vente_lot_volume_txt", "textarea");
            $form->setType("dia_lot_nat_su", "textarea");
            $form->setType("dia_lot_bat_achv_plus_10", "checkbox");
            $form->setType("dia_lot_bat_achv_moins_10", "checkbox");
            $form->setType("dia_lot_regl_copro_publ_hypo_plus_10", "checkbox");
            $form->setType("dia_lot_regl_copro_publ_hypo_moins_10", "checkbox");
            $form->setType("dia_indivi_quote_part", "textarea");
            $form->setType("dia_design_societe", "textarea");
            $form->setType("dia_design_droit", "textarea");
            $form->setType("dia_droit_soc_nat", "textarea");
            $form->setType("dia_droit_soc_nb", "textarea");
            $form->setType("dia_droit_soc_num_part", "textarea");
            $form->setType("dia_droit_reel_perso_grevant_bien_oui", "checkbox");
            $form->setType("dia_droit_reel_perso_grevant_bien_non", "checkbox");
            $form->setType("dia_droit_reel_perso_nat", "textarea");
            $form->setType("dia_droit_reel_perso_viag", "textarea");
            $form->setType("dia_mod_cess_adr", "textarea");
            $form->setType("dia_mod_cess_sign_act_auth", "checkbox");
            $form->setType("dia_mod_cess_terme", "checkbox");
            $form->setType("dia_mod_cess_terme_prec", "textarea");
            $form->setType("dia_mod_cess_bene_acquereur", "checkbox");
            $form->setType("dia_mod_cess_bene_vendeur", "checkbox");
            $form->setType("dia_mod_cess_paie_nat", "checkbox");
            $form->setType("dia_mod_cess_design_contr_alien", "textarea");
            $form->setType("dia_mod_cess_eval_contr", "textarea");
            $form->setType("dia_mod_cess_rente_viag", "checkbox");
            $form->setType("dia_mod_cess_mnt_an", "textarea");
            $form->setType("dia_mod_cess_mnt_compt", "textarea");
            $form->setType("dia_mod_cess_bene_rente", "textarea");
            $form->setType("dia_mod_cess_droit_usa_hab", "checkbox");
            $form->setType("dia_mod_cess_droit_usa_hab_prec", "textarea");
            $form->setType("dia_mod_cess_eval_usa_usufruit", "textarea");
            $form->setType("dia_mod_cess_vente_nue_prop", "checkbox");
            $form->setType("dia_mod_cess_vente_nue_prop_prec", "textarea");
            $form->setType("dia_mod_cess_echange", "checkbox");
            $form->setType("dia_mod_cess_design_bien_recus_ech", "textarea");
            $form->setType("dia_mod_cess_mnt_soulte", "textarea");
            $form->setType("dia_mod_cess_prop_contre_echan", "textarea");
            $form->setType("dia_mod_cess_apport_societe", "textarea");
            $form->setType("dia_mod_cess_bene", "textarea");
            $form->setType("dia_mod_cess_esti_bien", "textarea");
            $form->setType("dia_mod_cess_cess_terr_loc_co", "checkbox");
            $form->setType("dia_mod_cess_esti_terr", "textarea");
            $form->setType("dia_mod_cess_esti_loc", "textarea");
            $form->setType("dia_mod_cess_esti_imm_loca", "checkbox");
            $form->setType("dia_mod_cess_adju_vol", "checkbox");
            $form->setType("dia_mod_cess_adju_obl", "checkbox");
            $form->setType("dia_mod_cess_adju_fin_indivi", "checkbox");
            $form->setType("dia_mod_cess_adju_date_lieu", "textarea");
            $form->setType("dia_mod_cess_mnt_mise_prix", "textarea");
            $form->setType("dia_prop_titu_prix_indique", "checkbox");
            $form->setType("dia_prop_recherche_acqu_prix_indique", "checkbox");
            $form->setType("dia_acquereur_prof", "textarea");
            $form->setType("dia_indic_compl_ope", "textarea");
            $form->setType("dia_vente_adju", "checkbox");
            $form->setType("am_terr_res_demon", "checkbox");
            $form->setType("am_air_terr_res_mob", "checkbox");
            if ($this->is_in_context_of_foreign_key("objet_recours", $this->retourformulaire)) {
                $form->setType("ctx_objet_recours", "selecthiddenstatic");
            } else {
                $form->setType("ctx_objet_recours", "select");
            }
            $form->setType("ctx_reference_sagace", "text");
            $form->setType("ctx_nature_travaux_infra_om_html", "html");
            $form->setType("ctx_synthese_nti", "textarea");
            $form->setType("ctx_article_non_resp_om_html", "html");
            $form->setType("ctx_synthese_anr", "textarea");
            $form->setType("ctx_reference_parquet", "text");
            $form->setType("ctx_element_taxation", "text");
            $form->setType("ctx_infraction", "checkbox");
            $form->setType("ctx_regularisable", "checkbox");
            $form->setType("ctx_reference_courrier", "text");
            $form->setType("ctx_date_audience", "date");
            $form->setType("ctx_date_ajournement", "date");
            $form->setType("exo_facul_1", "checkbox");
            $form->setType("exo_facul_2", "checkbox");
            $form->setType("exo_facul_3", "checkbox");
            $form->setType("exo_facul_4", "checkbox");
            $form->setType("exo_facul_5", "checkbox");
            $form->setType("exo_facul_6", "checkbox");
            $form->setType("exo_facul_7", "checkbox");
            $form->setType("exo_facul_8", "checkbox");
            $form->setType("exo_facul_9", "checkbox");
            $form->setType("exo_ta_1", "checkbox");
            $form->setType("exo_ta_2", "checkbox");
            $form->setType("exo_ta_3", "checkbox");
            $form->setType("exo_ta_4", "checkbox");
            $form->setType("exo_ta_5", "checkbox");
            $form->setType("exo_ta_6", "checkbox");
            $form->setType("exo_ta_7", "checkbox");
            $form->setType("exo_ta_8", "checkbox");
            $form->setType("exo_ta_9", "checkbox");
            $form->setType("exo_rap_1", "checkbox");
            $form->setType("exo_rap_2", "checkbox");
            $form->setType("exo_rap_3", "checkbox");
            $form->setType("exo_rap_4", "checkbox");
            $form->setType("exo_rap_5", "checkbox");
            $form->setType("exo_rap_6", "checkbox");
            $form->setType("exo_rap_7", "checkbox");
            $form->setType("exo_rap_8", "checkbox");
            $form->setType("mtn_exo_ta_part_commu", "text");
            $form->setType("mtn_exo_ta_part_depart", "text");
            $form->setType("mtn_exo_ta_part_reg", "text");
            $form->setType("mtn_exo_rap", "text");
            $form->setType("dpc_type", "text");
            $form->setType("dpc_desc_actv_ex", "textarea");
            $form->setType("dpc_desc_ca", "textarea");
            $form->setType("dpc_desc_aut_prec", "textarea");
            $form->setType("dpc_desig_comm_arti", "checkbox");
            $form->setType("dpc_desig_loc_hab", "checkbox");
            $form->setType("dpc_desig_loc_ann", "checkbox");
            $form->setType("dpc_desig_loc_ann_prec", "textarea");
            $form->setType("dpc_bail_comm_date", "date");
            $form->setType("dpc_bail_comm_loyer", "textarea");
            $form->setType("dpc_actv_acqu", "textarea");
            $form->setType("dpc_nb_sala_di", "textarea");
            $form->setType("dpc_nb_sala_dd", "textarea");
            $form->setType("dpc_nb_sala_tc", "textarea");
            $form->setType("dpc_nb_sala_tp", "textarea");
            $form->setType("dpc_moda_cess_vente_am", "checkbox");
            $form->setType("dpc_moda_cess_adj", "checkbox");
            $form->setType("dpc_moda_cess_prix", "textarea");
            $form->setType("dpc_moda_cess_adj_date", "date");
            $form->setType("dpc_moda_cess_adj_prec", "textarea");
            $form->setType("dpc_moda_cess_paie_comp", "checkbox");
            $form->setType("dpc_moda_cess_paie_terme", "checkbox");
            $form->setType("dpc_moda_cess_paie_terme_prec", "textarea");
            $form->setType("dpc_moda_cess_paie_nat", "checkbox");
            $form->setType("dpc_moda_cess_paie_nat_desig_alien", "checkbox");
            $form->setType("dpc_moda_cess_paie_nat_desig_alien_prec", "textarea");
            $form->setType("dpc_moda_cess_paie_nat_eval", "checkbox");
            $form->setType("dpc_moda_cess_paie_nat_eval_prec", "textarea");
            $form->setType("dpc_moda_cess_paie_aut", "checkbox");
            $form->setType("dpc_moda_cess_paie_aut_prec", "textarea");
            $form->setType("dpc_ss_signe_demande_acqu", "checkbox");
            $form->setType("dpc_ss_signe_recher_trouv_acqu", "checkbox");
            $form->setType("dpc_notif_adr_prop", "checkbox");
            $form->setType("dpc_notif_adr_manda", "checkbox");
            $form->setType("dpc_obs", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("donnees_techniques", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("lot", $this->retourformulaire)) {
                $form->setType("lot", "selecthiddenstatic");
            } else {
                $form->setType("lot", "select");
            }
            $form->setType("am_lotiss", "checkbox");
            $form->setType("am_autre_div", "checkbox");
            $form->setType("am_camping", "checkbox");
            $form->setType("am_caravane", "checkbox");
            $form->setType("am_carav_duree", "text");
            $form->setType("am_statio", "checkbox");
            $form->setType("am_statio_cont", "text");
            $form->setType("am_affou_exhau", "checkbox");
            $form->setType("am_affou_exhau_sup", "text");
            $form->setType("am_affou_prof", "text");
            $form->setType("am_exhau_haut", "text");
            $form->setType("am_coupe_abat", "checkbox");
            $form->setType("am_prot_plu", "checkbox");
            $form->setType("am_prot_muni", "checkbox");
            $form->setType("am_mobil_voyage", "checkbox");
            $form->setType("am_aire_voyage", "checkbox");
            $form->setType("am_rememb_afu", "checkbox");
            $form->setType("am_parc_resid_loi", "checkbox");
            $form->setType("am_sport_moto", "checkbox");
            $form->setType("am_sport_attrac", "checkbox");
            $form->setType("am_sport_golf", "checkbox");
            $form->setType("am_mob_art", "checkbox");
            $form->setType("am_modif_voie_esp", "checkbox");
            $form->setType("am_plant_voie_esp", "checkbox");
            $form->setType("am_chem_ouv_esp", "checkbox");
            $form->setType("am_agri_peche", "checkbox");
            $form->setType("am_crea_voie", "checkbox");
            $form->setType("am_modif_voie_exist", "checkbox");
            $form->setType("am_crea_esp_sauv", "checkbox");
            $form->setType("am_crea_esp_class", "checkbox");
            $form->setType("am_projet_desc", "textarea");
            $form->setType("am_terr_surf", "text");
            $form->setType("am_tranche_desc", "textarea");
            $form->setType("am_lot_max_nb", "text");
            $form->setType("am_lot_max_shon", "text");
            $form->setType("am_lot_cstr_cos", "checkbox");
            $form->setType("am_lot_cstr_plan", "checkbox");
            $form->setType("am_lot_cstr_vente", "checkbox");
            $form->setType("am_lot_fin_diff", "checkbox");
            $form->setType("am_lot_consign", "checkbox");
            $form->setType("am_lot_gar_achev", "checkbox");
            $form->setType("am_lot_vente_ant", "checkbox");
            $form->setType("am_empl_nb", "text");
            $form->setType("am_tente_nb", "text");
            $form->setType("am_carav_nb", "text");
            $form->setType("am_mobil_nb", "text");
            $form->setType("am_pers_nb", "text");
            $form->setType("am_empl_hll_nb", "text");
            $form->setType("am_hll_shon", "text");
            $form->setType("am_periode_exploit", "textarea");
            $form->setType("am_exist_agrand", "checkbox");
            $form->setType("am_exist_date", "date");
            $form->setType("am_exist_num", "text");
            $form->setType("am_exist_nb_avant", "text");
            $form->setType("am_exist_nb_apres", "text");
            $form->setType("am_coupe_bois", "checkbox");
            $form->setType("am_coupe_parc", "checkbox");
            $form->setType("am_coupe_align", "checkbox");
            $form->setType("am_coupe_ess", "text");
            $form->setType("am_coupe_age", "text");
            $form->setType("am_coupe_dens", "text");
            $form->setType("am_coupe_qual", "text");
            $form->setType("am_coupe_trait", "text");
            $form->setType("am_coupe_autr", "text");
            $form->setType("co_archi_recours", "checkbox");
            $form->setType("co_cstr_nouv", "checkbox");
            $form->setType("co_cstr_exist", "checkbox");
            $form->setType("co_cloture", "checkbox");
            $form->setType("co_elec_tension", "text");
            $form->setType("co_div_terr", "checkbox");
            $form->setType("co_projet_desc", "textarea");
            $form->setType("co_anx_pisc", "checkbox");
            $form->setType("co_anx_gara", "checkbox");
            $form->setType("co_anx_veran", "checkbox");
            $form->setType("co_anx_abri", "checkbox");
            $form->setType("co_anx_autr", "checkbox");
            $form->setType("co_anx_autr_desc", "textarea");
            $form->setType("co_tot_log_nb", "text");
            $form->setType("co_tot_ind_nb", "text");
            $form->setType("co_tot_coll_nb", "text");
            $form->setType("co_mais_piece_nb", "text");
            $form->setType("co_mais_niv_nb", "text");
            $form->setType("co_fin_lls_nb", "text");
            $form->setType("co_fin_aa_nb", "text");
            $form->setType("co_fin_ptz_nb", "text");
            $form->setType("co_fin_autr_nb", "text");
            $form->setType("co_fin_autr_desc", "textarea");
            $form->setType("co_mais_contrat_ind", "checkbox");
            $form->setType("co_uti_pers", "checkbox");
            $form->setType("co_uti_vente", "checkbox");
            $form->setType("co_uti_loc", "checkbox");
            $form->setType("co_uti_princ", "checkbox");
            $form->setType("co_uti_secon", "checkbox");
            $form->setType("co_resid_agees", "checkbox");
            $form->setType("co_resid_etud", "checkbox");
            $form->setType("co_resid_tourism", "checkbox");
            $form->setType("co_resid_hot_soc", "checkbox");
            $form->setType("co_resid_soc", "checkbox");
            $form->setType("co_resid_hand", "checkbox");
            $form->setType("co_resid_autr", "checkbox");
            $form->setType("co_resid_autr_desc", "textarea");
            $form->setType("co_foyer_chamb_nb", "text");
            $form->setType("co_log_1p_nb", "text");
            $form->setType("co_log_2p_nb", "text");
            $form->setType("co_log_3p_nb", "text");
            $form->setType("co_log_4p_nb", "text");
            $form->setType("co_log_5p_nb", "text");
            $form->setType("co_log_6p_nb", "text");
            $form->setType("co_bat_niv_nb", "text");
            $form->setType("co_trx_exten", "checkbox");
            $form->setType("co_trx_surelev", "checkbox");
            $form->setType("co_trx_nivsup", "checkbox");
            $form->setType("co_demont_periode", "textarea");
            $form->setType("co_sp_transport", "checkbox");
            $form->setType("co_sp_enseign", "checkbox");
            $form->setType("co_sp_act_soc", "checkbox");
            $form->setType("co_sp_ouvr_spe", "checkbox");
            $form->setType("co_sp_sante", "checkbox");
            $form->setType("co_sp_culture", "checkbox");
            $form->setType("co_statio_avt_nb", "text");
            $form->setType("co_statio_apr_nb", "text");
            $form->setType("co_statio_adr", "textarea");
            $form->setType("co_statio_place_nb", "text");
            $form->setType("co_statio_tot_surf", "text");
            $form->setType("co_statio_tot_shob", "text");
            $form->setType("co_statio_comm_cin_surf", "text");
            $form->setType("su_avt_shon1", "text");
            $form->setType("su_avt_shon2", "text");
            $form->setType("su_avt_shon3", "text");
            $form->setType("su_avt_shon4", "text");
            $form->setType("su_avt_shon5", "text");
            $form->setType("su_avt_shon6", "text");
            $form->setType("su_avt_shon7", "text");
            $form->setType("su_avt_shon8", "text");
            $form->setType("su_avt_shon9", "text");
            $form->setType("su_cstr_shon1", "text");
            $form->setType("su_cstr_shon2", "text");
            $form->setType("su_cstr_shon3", "text");
            $form->setType("su_cstr_shon4", "text");
            $form->setType("su_cstr_shon5", "text");
            $form->setType("su_cstr_shon6", "text");
            $form->setType("su_cstr_shon7", "text");
            $form->setType("su_cstr_shon8", "text");
            $form->setType("su_cstr_shon9", "text");
            $form->setType("su_trsf_shon1", "text");
            $form->setType("su_trsf_shon2", "text");
            $form->setType("su_trsf_shon3", "text");
            $form->setType("su_trsf_shon4", "text");
            $form->setType("su_trsf_shon5", "text");
            $form->setType("su_trsf_shon6", "text");
            $form->setType("su_trsf_shon7", "text");
            $form->setType("su_trsf_shon8", "text");
            $form->setType("su_trsf_shon9", "text");
            $form->setType("su_chge_shon1", "text");
            $form->setType("su_chge_shon2", "text");
            $form->setType("su_chge_shon3", "text");
            $form->setType("su_chge_shon4", "text");
            $form->setType("su_chge_shon5", "text");
            $form->setType("su_chge_shon6", "text");
            $form->setType("su_chge_shon7", "text");
            $form->setType("su_chge_shon8", "text");
            $form->setType("su_chge_shon9", "text");
            $form->setType("su_demo_shon1", "text");
            $form->setType("su_demo_shon2", "text");
            $form->setType("su_demo_shon3", "text");
            $form->setType("su_demo_shon4", "text");
            $form->setType("su_demo_shon5", "text");
            $form->setType("su_demo_shon6", "text");
            $form->setType("su_demo_shon7", "text");
            $form->setType("su_demo_shon8", "text");
            $form->setType("su_demo_shon9", "text");
            $form->setType("su_sup_shon1", "text");
            $form->setType("su_sup_shon2", "text");
            $form->setType("su_sup_shon3", "text");
            $form->setType("su_sup_shon4", "text");
            $form->setType("su_sup_shon5", "text");
            $form->setType("su_sup_shon6", "text");
            $form->setType("su_sup_shon7", "text");
            $form->setType("su_sup_shon8", "text");
            $form->setType("su_sup_shon9", "text");
            $form->setType("su_tot_shon1", "text");
            $form->setType("su_tot_shon2", "text");
            $form->setType("su_tot_shon3", "text");
            $form->setType("su_tot_shon4", "text");
            $form->setType("su_tot_shon5", "text");
            $form->setType("su_tot_shon6", "text");
            $form->setType("su_tot_shon7", "text");
            $form->setType("su_tot_shon8", "text");
            $form->setType("su_tot_shon9", "text");
            $form->setType("su_avt_shon_tot", "text");
            $form->setType("su_cstr_shon_tot", "text");
            $form->setType("su_trsf_shon_tot", "text");
            $form->setType("su_chge_shon_tot", "text");
            $form->setType("su_demo_shon_tot", "text");
            $form->setType("su_sup_shon_tot", "text");
            $form->setType("su_tot_shon_tot", "text");
            $form->setType("dm_constr_dates", "textarea");
            $form->setType("dm_total", "checkbox");
            $form->setType("dm_partiel", "checkbox");
            $form->setType("dm_projet_desc", "textarea");
            $form->setType("dm_tot_log_nb", "text");
            $form->setType("tax_surf_tot", "text");
            $form->setType("tax_surf", "text");
            $form->setType("tax_surf_suppr_mod", "text");
            $form->setType("tax_su_princ_log_nb1", "text");
            $form->setType("tax_su_princ_log_nb2", "text");
            $form->setType("tax_su_princ_log_nb3", "text");
            $form->setType("tax_su_princ_log_nb4", "text");
            $form->setType("tax_su_princ_log_nb_tot1", "text");
            $form->setType("tax_su_princ_log_nb_tot2", "text");
            $form->setType("tax_su_princ_log_nb_tot3", "text");
            $form->setType("tax_su_princ_log_nb_tot4", "text");
            $form->setType("tax_su_princ_surf1", "text");
            $form->setType("tax_su_princ_surf2", "text");
            $form->setType("tax_su_princ_surf3", "text");
            $form->setType("tax_su_princ_surf4", "text");
            $form->setType("tax_su_princ_surf_sup1", "text");
            $form->setType("tax_su_princ_surf_sup2", "text");
            $form->setType("tax_su_princ_surf_sup3", "text");
            $form->setType("tax_su_princ_surf_sup4", "text");
            $form->setType("tax_su_heber_log_nb1", "text");
            $form->setType("tax_su_heber_log_nb2", "text");
            $form->setType("tax_su_heber_log_nb3", "text");
            $form->setType("tax_su_heber_log_nb_tot1", "text");
            $form->setType("tax_su_heber_log_nb_tot2", "text");
            $form->setType("tax_su_heber_log_nb_tot3", "text");
            $form->setType("tax_su_heber_surf1", "text");
            $form->setType("tax_su_heber_surf2", "text");
            $form->setType("tax_su_heber_surf3", "text");
            $form->setType("tax_su_heber_surf_sup1", "text");
            $form->setType("tax_su_heber_surf_sup2", "text");
            $form->setType("tax_su_heber_surf_sup3", "text");
            $form->setType("tax_su_secon_log_nb", "text");
            $form->setType("tax_su_tot_log_nb", "text");
            $form->setType("tax_su_secon_log_nb_tot", "text");
            $form->setType("tax_su_tot_log_nb_tot", "text");
            $form->setType("tax_su_secon_surf", "text");
            $form->setType("tax_su_tot_surf", "text");
            $form->setType("tax_su_secon_surf_sup", "text");
            $form->setType("tax_su_tot_surf_sup", "text");
            $form->setType("tax_ext_pret", "checkbox");
            $form->setType("tax_ext_desc", "textarea");
            $form->setType("tax_surf_tax_exist_cons", "text");
            $form->setType("tax_log_exist_nb", "text");
            $form->setType("tax_am_statio_ext", "text");
            $form->setType("tax_sup_bass_pisc", "text");
            $form->setType("tax_empl_ten_carav_mobil_nb", "text");
            $form->setType("tax_empl_hll_nb", "text");
            $form->setType("tax_eol_haut_nb", "text");
            $form->setType("tax_pann_volt_sup", "text");
            $form->setType("tax_am_statio_ext_sup", "text");
            $form->setType("tax_sup_bass_pisc_sup", "text");
            $form->setType("tax_empl_ten_carav_mobil_nb_sup", "text");
            $form->setType("tax_empl_hll_nb_sup", "text");
            $form->setType("tax_eol_haut_nb_sup", "text");
            $form->setType("tax_pann_volt_sup_sup", "text");
            $form->setType("tax_trx_presc_ppr", "checkbox");
            $form->setType("tax_monu_hist", "checkbox");
            $form->setType("tax_comm_nb", "text");
            $form->setType("tax_su_non_habit_surf1", "text");
            $form->setType("tax_su_non_habit_surf2", "text");
            $form->setType("tax_su_non_habit_surf3", "text");
            $form->setType("tax_su_non_habit_surf4", "text");
            $form->setType("tax_su_non_habit_surf5", "text");
            $form->setType("tax_su_non_habit_surf6", "text");
            $form->setType("tax_su_non_habit_surf7", "text");
            $form->setType("tax_su_non_habit_surf_sup1", "text");
            $form->setType("tax_su_non_habit_surf_sup2", "text");
            $form->setType("tax_su_non_habit_surf_sup3", "text");
            $form->setType("tax_su_non_habit_surf_sup4", "text");
            $form->setType("tax_su_non_habit_surf_sup5", "text");
            $form->setType("tax_su_non_habit_surf_sup6", "text");
            $form->setType("tax_su_non_habit_surf_sup7", "text");
            $form->setType("vsd_surf_planch_smd", "checkbox");
            $form->setType("vsd_unit_fonc_sup", "text");
            $form->setType("vsd_unit_fonc_constr_sup", "text");
            $form->setType("vsd_val_terr", "text");
            $form->setType("vsd_const_sxist_non_dem_surf", "text");
            $form->setType("vsd_rescr_fisc", "date");
            $form->setType("pld_val_terr", "text");
            $form->setType("pld_const_exist_dem", "checkbox");
            $form->setType("pld_const_exist_dem_surf", "text");
            $form->setType("code_cnil", "checkbox");
            $form->setType("terr_juri_titul", "text");
            $form->setType("terr_juri_lot", "text");
            $form->setType("terr_juri_zac", "text");
            $form->setType("terr_juri_afu", "text");
            $form->setType("terr_juri_pup", "text");
            $form->setType("terr_juri_oin", "text");
            $form->setType("terr_juri_desc", "textarea");
            $form->setType("terr_div_surf_etab", "text");
            $form->setType("terr_div_surf_av_div", "text");
            $form->setType("doc_date", "date");
            $form->setType("doc_tot_trav", "checkbox");
            $form->setType("doc_tranche_trav", "checkbox");
            $form->setType("doc_tranche_trav_desc", "textarea");
            $form->setType("doc_surf", "text");
            $form->setType("doc_nb_log", "text");
            $form->setType("doc_nb_log_indiv", "text");
            $form->setType("doc_nb_log_coll", "text");
            $form->setType("doc_nb_log_lls", "text");
            $form->setType("doc_nb_log_aa", "text");
            $form->setType("doc_nb_log_ptz", "text");
            $form->setType("doc_nb_log_autre", "text");
            $form->setType("daact_date", "date");
            $form->setType("daact_date_chgmt_dest", "date");
            $form->setType("daact_tot_trav", "checkbox");
            $form->setType("daact_tranche_trav", "checkbox");
            $form->setType("daact_tranche_trav_desc", "textarea");
            $form->setType("daact_surf", "text");
            $form->setType("daact_nb_log", "text");
            $form->setType("daact_nb_log_indiv", "text");
            $form->setType("daact_nb_log_coll", "text");
            $form->setType("daact_nb_log_lls", "text");
            $form->setType("daact_nb_log_aa", "text");
            $form->setType("daact_nb_log_ptz", "text");
            $form->setType("daact_nb_log_autre", "text");
            if ($this->is_in_context_of_foreign_key("dossier_autorisation", $this->retourformulaire)) {
                $form->setType("dossier_autorisation", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation", "select");
            }
            $form->setType("am_div_mun", "checkbox");
            $form->setType("co_perf_energ", "text");
            if ($this->is_in_context_of_foreign_key("architecte", $this->retourformulaire)) {
                $form->setType("architecte", "selecthiddenstatic");
            } else {
                $form->setType("architecte", "select");
            }
            $form->setType("co_statio_avt_shob", "text");
            $form->setType("co_statio_apr_shob", "text");
            $form->setType("co_statio_avt_surf", "text");
            $form->setType("co_statio_apr_surf", "text");
            $form->setType("co_trx_amgt", "text");
            $form->setType("co_modif_aspect", "text");
            $form->setType("co_modif_struct", "text");
            $form->setType("co_ouvr_elec", "checkbox");
            $form->setType("co_ouvr_infra", "checkbox");
            $form->setType("co_trx_imm", "text");
            $form->setType("co_cstr_shob", "text");
            $form->setType("am_voyage_deb", "text");
            $form->setType("am_voyage_fin", "text");
            $form->setType("am_modif_amgt", "text");
            $form->setType("am_lot_max_shob", "text");
            $form->setType("mod_desc", "text");
            $form->setType("tr_total", "text");
            $form->setType("tr_partiel", "text");
            $form->setType("tr_desc", "text");
            $form->setType("avap_co_elt_pro", "checkbox");
            $form->setType("avap_nouv_haut_surf", "checkbox");
            $form->setType("avap_co_clot", "text");
            $form->setType("avap_aut_coup_aba_arb", "text");
            $form->setType("avap_ouv_infra", "text");
            $form->setType("avap_aut_inst_mob", "text");
            $form->setType("avap_aut_plant", "text");
            $form->setType("avap_aut_auv_elec", "text");
            $form->setType("tax_dest_loc_tr", "text");
            $form->setType("ope_proj_desc", "textarea");
            $form->setType("tax_surf_tot_cstr", "text");
            $form->setType("cerfa", "text");
            $form->setType("tax_surf_loc_stat", "text");
            $form->setType("tax_su_princ_surf_stat1", "text");
            $form->setType("tax_su_princ_surf_stat2", "text");
            $form->setType("tax_su_princ_surf_stat3", "text");
            $form->setType("tax_su_princ_surf_stat4", "text");
            $form->setType("tax_su_secon_surf_stat", "text");
            $form->setType("tax_su_heber_surf_stat1", "text");
            $form->setType("tax_su_heber_surf_stat2", "text");
            $form->setType("tax_su_heber_surf_stat3", "text");
            $form->setType("tax_su_tot_surf_stat", "text");
            $form->setType("tax_su_non_habit_surf_stat1", "text");
            $form->setType("tax_su_non_habit_surf_stat2", "text");
            $form->setType("tax_su_non_habit_surf_stat3", "text");
            $form->setType("tax_su_non_habit_surf_stat4", "text");
            $form->setType("tax_su_non_habit_surf_stat5", "text");
            $form->setType("tax_su_non_habit_surf_stat6", "text");
            $form->setType("tax_su_non_habit_surf_stat7", "text");
            $form->setType("tax_su_parc_statio_expl_comm_surf", "text");
            $form->setType("tax_log_ap_trvx_nb", "text");
            $form->setType("tax_am_statio_ext_cr", "text");
            $form->setType("tax_sup_bass_pisc_cr", "text");
            $form->setType("tax_empl_ten_carav_mobil_nb_cr", "text");
            $form->setType("tax_empl_hll_nb_cr", "text");
            $form->setType("tax_eol_haut_nb_cr", "text");
            $form->setType("tax_pann_volt_sup_cr", "text");
            $form->setType("tax_surf_loc_arch", "text");
            $form->setType("tax_surf_pisc_arch", "text");
            $form->setType("tax_am_statio_ext_arch", "text");
            $form->setType("tax_empl_ten_carav_mobil_nb_arch", "text");
            $form->setType("tax_empl_hll_nb_arch", "text");
            $form->setType("tax_eol_haut_nb_arch", "text");
            $form->setType("ope_proj_div_co", "checkbox");
            $form->setType("ope_proj_div_contr", "checkbox");
            $form->setType("tax_desc", "textarea");
            $form->setType("erp_cstr_neuve", "checkbox");
            $form->setType("erp_trvx_acc", "checkbox");
            $form->setType("erp_extension", "checkbox");
            $form->setType("erp_rehab", "checkbox");
            $form->setType("erp_trvx_am", "checkbox");
            $form->setType("erp_vol_nouv_exist", "checkbox");
            $form->setType("erp_loc_eff1", "text");
            $form->setType("erp_loc_eff2", "text");
            $form->setType("erp_loc_eff3", "text");
            $form->setType("erp_loc_eff4", "text");
            $form->setType("erp_loc_eff5", "text");
            $form->setType("erp_loc_eff_tot", "text");
            $form->setType("erp_public_eff1", "text");
            $form->setType("erp_public_eff2", "text");
            $form->setType("erp_public_eff3", "text");
            $form->setType("erp_public_eff4", "text");
            $form->setType("erp_public_eff5", "text");
            $form->setType("erp_public_eff_tot", "text");
            $form->setType("erp_perso_eff1", "text");
            $form->setType("erp_perso_eff2", "text");
            $form->setType("erp_perso_eff3", "text");
            $form->setType("erp_perso_eff4", "text");
            $form->setType("erp_perso_eff5", "text");
            $form->setType("erp_perso_eff_tot", "text");
            $form->setType("erp_tot_eff1", "text");
            $form->setType("erp_tot_eff2", "text");
            $form->setType("erp_tot_eff3", "text");
            $form->setType("erp_tot_eff4", "text");
            $form->setType("erp_tot_eff5", "text");
            $form->setType("erp_tot_eff_tot", "text");
            $form->setType("erp_class_cat", "text");
            $form->setType("erp_class_type", "text");
            $form->setType("tax_surf_abr_jard_pig_colom", "text");
            $form->setType("tax_su_non_habit_abr_jard_pig_colom", "text");
            $form->setType("dia_imm_non_bati", "checkbox");
            $form->setType("dia_imm_bati_terr_propr", "checkbox");
            $form->setType("dia_imm_bati_terr_autr", "checkbox");
            $form->setType("dia_imm_bati_terr_autr_desc", "textarea");
            $form->setType("dia_bat_copro", "checkbox");
            $form->setType("dia_bat_copro_desc", "textarea");
            $form->setType("dia_lot_numero", "textarea");
            $form->setType("dia_lot_bat", "textarea");
            $form->setType("dia_lot_etage", "textarea");
            $form->setType("dia_lot_quote_part", "textarea");
            $form->setType("dia_us_hab", "checkbox");
            $form->setType("dia_us_pro", "checkbox");
            $form->setType("dia_us_mixte", "checkbox");
            $form->setType("dia_us_comm", "checkbox");
            $form->setType("dia_us_agr", "checkbox");
            $form->setType("dia_us_autre", "checkbox");
            $form->setType("dia_us_autre_prec", "textarea");
            $form->setType("dia_occ_prop", "checkbox");
            $form->setType("dia_occ_loc", "checkbox");
            $form->setType("dia_occ_sans_occ", "checkbox");
            $form->setType("dia_occ_autre", "checkbox");
            $form->setType("dia_occ_autre_prec", "textarea");
            $form->setType("dia_mod_cess_prix_vente", "text");
            $form->setType("dia_mod_cess_prix_vente_mob", "text");
            $form->setType("dia_mod_cess_prix_vente_cheptel", "text");
            $form->setType("dia_mod_cess_prix_vente_recol", "text");
            $form->setType("dia_mod_cess_prix_vente_autre", "text");
            $form->setType("dia_mod_cess_commi", "checkbox");
            $form->setType("dia_mod_cess_commi_ttc", "text");
            $form->setType("dia_mod_cess_commi_ht", "text");
            $form->setType("dia_acquereur_nom_prenom", "text");
            $form->setType("dia_acquereur_adr_num_voie", "text");
            $form->setType("dia_acquereur_adr_ext", "text");
            $form->setType("dia_acquereur_adr_type_voie", "text");
            $form->setType("dia_acquereur_adr_nom_voie", "text");
            $form->setType("dia_acquereur_adr_lieu_dit_bp", "text");
            $form->setType("dia_acquereur_adr_cp", "text");
            $form->setType("dia_acquereur_adr_localite", "text");
            $form->setType("dia_observation", "textarea");
            $form->setType("su2_avt_shon1", "text");
            $form->setType("su2_avt_shon2", "text");
            $form->setType("su2_avt_shon3", "text");
            $form->setType("su2_avt_shon4", "text");
            $form->setType("su2_avt_shon5", "text");
            $form->setType("su2_avt_shon6", "text");
            $form->setType("su2_avt_shon7", "text");
            $form->setType("su2_avt_shon8", "text");
            $form->setType("su2_avt_shon9", "text");
            $form->setType("su2_avt_shon10", "text");
            $form->setType("su2_avt_shon11", "text");
            $form->setType("su2_avt_shon12", "text");
            $form->setType("su2_avt_shon13", "text");
            $form->setType("su2_avt_shon14", "text");
            $form->setType("su2_avt_shon15", "text");
            $form->setType("su2_avt_shon16", "text");
            $form->setType("su2_avt_shon17", "text");
            $form->setType("su2_avt_shon18", "text");
            $form->setType("su2_avt_shon19", "text");
            $form->setType("su2_avt_shon20", "text");
            $form->setType("su2_avt_shon_tot", "text");
            $form->setType("su2_cstr_shon1", "text");
            $form->setType("su2_cstr_shon2", "text");
            $form->setType("su2_cstr_shon3", "text");
            $form->setType("su2_cstr_shon4", "text");
            $form->setType("su2_cstr_shon5", "text");
            $form->setType("su2_cstr_shon6", "text");
            $form->setType("su2_cstr_shon7", "text");
            $form->setType("su2_cstr_shon8", "text");
            $form->setType("su2_cstr_shon9", "text");
            $form->setType("su2_cstr_shon10", "text");
            $form->setType("su2_cstr_shon11", "text");
            $form->setType("su2_cstr_shon12", "text");
            $form->setType("su2_cstr_shon13", "text");
            $form->setType("su2_cstr_shon14", "text");
            $form->setType("su2_cstr_shon15", "text");
            $form->setType("su2_cstr_shon16", "text");
            $form->setType("su2_cstr_shon17", "text");
            $form->setType("su2_cstr_shon18", "text");
            $form->setType("su2_cstr_shon19", "text");
            $form->setType("su2_cstr_shon20", "text");
            $form->setType("su2_cstr_shon_tot", "text");
            $form->setType("su2_chge_shon1", "text");
            $form->setType("su2_chge_shon2", "text");
            $form->setType("su2_chge_shon3", "text");
            $form->setType("su2_chge_shon4", "text");
            $form->setType("su2_chge_shon5", "text");
            $form->setType("su2_chge_shon6", "text");
            $form->setType("su2_chge_shon7", "text");
            $form->setType("su2_chge_shon8", "text");
            $form->setType("su2_chge_shon9", "text");
            $form->setType("su2_chge_shon10", "text");
            $form->setType("su2_chge_shon11", "text");
            $form->setType("su2_chge_shon12", "text");
            $form->setType("su2_chge_shon13", "text");
            $form->setType("su2_chge_shon14", "text");
            $form->setType("su2_chge_shon15", "text");
            $form->setType("su2_chge_shon16", "text");
            $form->setType("su2_chge_shon17", "text");
            $form->setType("su2_chge_shon18", "text");
            $form->setType("su2_chge_shon19", "text");
            $form->setType("su2_chge_shon20", "text");
            $form->setType("su2_chge_shon_tot", "text");
            $form->setType("su2_demo_shon1", "text");
            $form->setType("su2_demo_shon2", "text");
            $form->setType("su2_demo_shon3", "text");
            $form->setType("su2_demo_shon4", "text");
            $form->setType("su2_demo_shon5", "text");
            $form->setType("su2_demo_shon6", "text");
            $form->setType("su2_demo_shon7", "text");
            $form->setType("su2_demo_shon8", "text");
            $form->setType("su2_demo_shon9", "text");
            $form->setType("su2_demo_shon10", "text");
            $form->setType("su2_demo_shon11", "text");
            $form->setType("su2_demo_shon12", "text");
            $form->setType("su2_demo_shon13", "text");
            $form->setType("su2_demo_shon14", "text");
            $form->setType("su2_demo_shon15", "text");
            $form->setType("su2_demo_shon16", "text");
            $form->setType("su2_demo_shon17", "text");
            $form->setType("su2_demo_shon18", "text");
            $form->setType("su2_demo_shon19", "text");
            $form->setType("su2_demo_shon20", "text");
            $form->setType("su2_demo_shon_tot", "text");
            $form->setType("su2_sup_shon1", "text");
            $form->setType("su2_sup_shon2", "text");
            $form->setType("su2_sup_shon3", "text");
            $form->setType("su2_sup_shon4", "text");
            $form->setType("su2_sup_shon5", "text");
            $form->setType("su2_sup_shon6", "text");
            $form->setType("su2_sup_shon7", "text");
            $form->setType("su2_sup_shon8", "text");
            $form->setType("su2_sup_shon9", "text");
            $form->setType("su2_sup_shon10", "text");
            $form->setType("su2_sup_shon11", "text");
            $form->setType("su2_sup_shon12", "text");
            $form->setType("su2_sup_shon13", "text");
            $form->setType("su2_sup_shon14", "text");
            $form->setType("su2_sup_shon15", "text");
            $form->setType("su2_sup_shon16", "text");
            $form->setType("su2_sup_shon17", "text");
            $form->setType("su2_sup_shon18", "text");
            $form->setType("su2_sup_shon19", "text");
            $form->setType("su2_sup_shon20", "text");
            $form->setType("su2_sup_shon_tot", "text");
            $form->setType("su2_tot_shon1", "text");
            $form->setType("su2_tot_shon2", "text");
            $form->setType("su2_tot_shon3", "text");
            $form->setType("su2_tot_shon4", "text");
            $form->setType("su2_tot_shon5", "text");
            $form->setType("su2_tot_shon6", "text");
            $form->setType("su2_tot_shon7", "text");
            $form->setType("su2_tot_shon8", "text");
            $form->setType("su2_tot_shon9", "text");
            $form->setType("su2_tot_shon10", "text");
            $form->setType("su2_tot_shon11", "text");
            $form->setType("su2_tot_shon12", "text");
            $form->setType("su2_tot_shon13", "text");
            $form->setType("su2_tot_shon14", "text");
            $form->setType("su2_tot_shon15", "text");
            $form->setType("su2_tot_shon16", "text");
            $form->setType("su2_tot_shon17", "text");
            $form->setType("su2_tot_shon18", "text");
            $form->setType("su2_tot_shon19", "text");
            $form->setType("su2_tot_shon20", "text");
            $form->setType("su2_tot_shon_tot", "text");
            $form->setType("dia_occ_sol_su_terre", "textarea");
            $form->setType("dia_occ_sol_su_pres", "textarea");
            $form->setType("dia_occ_sol_su_verger", "textarea");
            $form->setType("dia_occ_sol_su_vigne", "textarea");
            $form->setType("dia_occ_sol_su_bois", "textarea");
            $form->setType("dia_occ_sol_su_lande", "textarea");
            $form->setType("dia_occ_sol_su_carriere", "textarea");
            $form->setType("dia_occ_sol_su_eau_cadastree", "textarea");
            $form->setType("dia_occ_sol_su_jardin", "textarea");
            $form->setType("dia_occ_sol_su_terr_batir", "textarea");
            $form->setType("dia_occ_sol_su_terr_agr", "textarea");
            $form->setType("dia_occ_sol_su_sol", "textarea");
            $form->setType("dia_bati_vend_tot", "checkbox");
            $form->setType("dia_bati_vend_tot_txt", "textarea");
            $form->setType("dia_su_co_sol", "textarea");
            $form->setType("dia_su_util_hab", "textarea");
            $form->setType("dia_nb_niv", "textarea");
            $form->setType("dia_nb_appart", "textarea");
            $form->setType("dia_nb_autre_loc", "textarea");
            $form->setType("dia_vente_lot_volume", "checkbox");
            $form->setType("dia_vente_lot_volume_txt", "textarea");
            $form->setType("dia_lot_nat_su", "textarea");
            $form->setType("dia_lot_bat_achv_plus_10", "checkbox");
            $form->setType("dia_lot_bat_achv_moins_10", "checkbox");
            $form->setType("dia_lot_regl_copro_publ_hypo_plus_10", "checkbox");
            $form->setType("dia_lot_regl_copro_publ_hypo_moins_10", "checkbox");
            $form->setType("dia_indivi_quote_part", "textarea");
            $form->setType("dia_design_societe", "textarea");
            $form->setType("dia_design_droit", "textarea");
            $form->setType("dia_droit_soc_nat", "textarea");
            $form->setType("dia_droit_soc_nb", "textarea");
            $form->setType("dia_droit_soc_num_part", "textarea");
            $form->setType("dia_droit_reel_perso_grevant_bien_oui", "checkbox");
            $form->setType("dia_droit_reel_perso_grevant_bien_non", "checkbox");
            $form->setType("dia_droit_reel_perso_nat", "textarea");
            $form->setType("dia_droit_reel_perso_viag", "textarea");
            $form->setType("dia_mod_cess_adr", "textarea");
            $form->setType("dia_mod_cess_sign_act_auth", "checkbox");
            $form->setType("dia_mod_cess_terme", "checkbox");
            $form->setType("dia_mod_cess_terme_prec", "textarea");
            $form->setType("dia_mod_cess_bene_acquereur", "checkbox");
            $form->setType("dia_mod_cess_bene_vendeur", "checkbox");
            $form->setType("dia_mod_cess_paie_nat", "checkbox");
            $form->setType("dia_mod_cess_design_contr_alien", "textarea");
            $form->setType("dia_mod_cess_eval_contr", "textarea");
            $form->setType("dia_mod_cess_rente_viag", "checkbox");
            $form->setType("dia_mod_cess_mnt_an", "textarea");
            $form->setType("dia_mod_cess_mnt_compt", "textarea");
            $form->setType("dia_mod_cess_bene_rente", "textarea");
            $form->setType("dia_mod_cess_droit_usa_hab", "checkbox");
            $form->setType("dia_mod_cess_droit_usa_hab_prec", "textarea");
            $form->setType("dia_mod_cess_eval_usa_usufruit", "textarea");
            $form->setType("dia_mod_cess_vente_nue_prop", "checkbox");
            $form->setType("dia_mod_cess_vente_nue_prop_prec", "textarea");
            $form->setType("dia_mod_cess_echange", "checkbox");
            $form->setType("dia_mod_cess_design_bien_recus_ech", "textarea");
            $form->setType("dia_mod_cess_mnt_soulte", "textarea");
            $form->setType("dia_mod_cess_prop_contre_echan", "textarea");
            $form->setType("dia_mod_cess_apport_societe", "textarea");
            $form->setType("dia_mod_cess_bene", "textarea");
            $form->setType("dia_mod_cess_esti_bien", "textarea");
            $form->setType("dia_mod_cess_cess_terr_loc_co", "checkbox");
            $form->setType("dia_mod_cess_esti_terr", "textarea");
            $form->setType("dia_mod_cess_esti_loc", "textarea");
            $form->setType("dia_mod_cess_esti_imm_loca", "checkbox");
            $form->setType("dia_mod_cess_adju_vol", "checkbox");
            $form->setType("dia_mod_cess_adju_obl", "checkbox");
            $form->setType("dia_mod_cess_adju_fin_indivi", "checkbox");
            $form->setType("dia_mod_cess_adju_date_lieu", "textarea");
            $form->setType("dia_mod_cess_mnt_mise_prix", "textarea");
            $form->setType("dia_prop_titu_prix_indique", "checkbox");
            $form->setType("dia_prop_recherche_acqu_prix_indique", "checkbox");
            $form->setType("dia_acquereur_prof", "textarea");
            $form->setType("dia_indic_compl_ope", "textarea");
            $form->setType("dia_vente_adju", "checkbox");
            $form->setType("am_terr_res_demon", "checkbox");
            $form->setType("am_air_terr_res_mob", "checkbox");
            if ($this->is_in_context_of_foreign_key("objet_recours", $this->retourformulaire)) {
                $form->setType("ctx_objet_recours", "selecthiddenstatic");
            } else {
                $form->setType("ctx_objet_recours", "select");
            }
            $form->setType("ctx_reference_sagace", "text");
            $form->setType("ctx_nature_travaux_infra_om_html", "html");
            $form->setType("ctx_synthese_nti", "textarea");
            $form->setType("ctx_article_non_resp_om_html", "html");
            $form->setType("ctx_synthese_anr", "textarea");
            $form->setType("ctx_reference_parquet", "text");
            $form->setType("ctx_element_taxation", "text");
            $form->setType("ctx_infraction", "checkbox");
            $form->setType("ctx_regularisable", "checkbox");
            $form->setType("ctx_reference_courrier", "text");
            $form->setType("ctx_date_audience", "date");
            $form->setType("ctx_date_ajournement", "date");
            $form->setType("exo_facul_1", "checkbox");
            $form->setType("exo_facul_2", "checkbox");
            $form->setType("exo_facul_3", "checkbox");
            $form->setType("exo_facul_4", "checkbox");
            $form->setType("exo_facul_5", "checkbox");
            $form->setType("exo_facul_6", "checkbox");
            $form->setType("exo_facul_7", "checkbox");
            $form->setType("exo_facul_8", "checkbox");
            $form->setType("exo_facul_9", "checkbox");
            $form->setType("exo_ta_1", "checkbox");
            $form->setType("exo_ta_2", "checkbox");
            $form->setType("exo_ta_3", "checkbox");
            $form->setType("exo_ta_4", "checkbox");
            $form->setType("exo_ta_5", "checkbox");
            $form->setType("exo_ta_6", "checkbox");
            $form->setType("exo_ta_7", "checkbox");
            $form->setType("exo_ta_8", "checkbox");
            $form->setType("exo_ta_9", "checkbox");
            $form->setType("exo_rap_1", "checkbox");
            $form->setType("exo_rap_2", "checkbox");
            $form->setType("exo_rap_3", "checkbox");
            $form->setType("exo_rap_4", "checkbox");
            $form->setType("exo_rap_5", "checkbox");
            $form->setType("exo_rap_6", "checkbox");
            $form->setType("exo_rap_7", "checkbox");
            $form->setType("exo_rap_8", "checkbox");
            $form->setType("mtn_exo_ta_part_commu", "text");
            $form->setType("mtn_exo_ta_part_depart", "text");
            $form->setType("mtn_exo_ta_part_reg", "text");
            $form->setType("mtn_exo_rap", "text");
            $form->setType("dpc_type", "text");
            $form->setType("dpc_desc_actv_ex", "textarea");
            $form->setType("dpc_desc_ca", "textarea");
            $form->setType("dpc_desc_aut_prec", "textarea");
            $form->setType("dpc_desig_comm_arti", "checkbox");
            $form->setType("dpc_desig_loc_hab", "checkbox");
            $form->setType("dpc_desig_loc_ann", "checkbox");
            $form->setType("dpc_desig_loc_ann_prec", "textarea");
            $form->setType("dpc_bail_comm_date", "date");
            $form->setType("dpc_bail_comm_loyer", "textarea");
            $form->setType("dpc_actv_acqu", "textarea");
            $form->setType("dpc_nb_sala_di", "textarea");
            $form->setType("dpc_nb_sala_dd", "textarea");
            $form->setType("dpc_nb_sala_tc", "textarea");
            $form->setType("dpc_nb_sala_tp", "textarea");
            $form->setType("dpc_moda_cess_vente_am", "checkbox");
            $form->setType("dpc_moda_cess_adj", "checkbox");
            $form->setType("dpc_moda_cess_prix", "textarea");
            $form->setType("dpc_moda_cess_adj_date", "date");
            $form->setType("dpc_moda_cess_adj_prec", "textarea");
            $form->setType("dpc_moda_cess_paie_comp", "checkbox");
            $form->setType("dpc_moda_cess_paie_terme", "checkbox");
            $form->setType("dpc_moda_cess_paie_terme_prec", "textarea");
            $form->setType("dpc_moda_cess_paie_nat", "checkbox");
            $form->setType("dpc_moda_cess_paie_nat_desig_alien", "checkbox");
            $form->setType("dpc_moda_cess_paie_nat_desig_alien_prec", "textarea");
            $form->setType("dpc_moda_cess_paie_nat_eval", "checkbox");
            $form->setType("dpc_moda_cess_paie_nat_eval_prec", "textarea");
            $form->setType("dpc_moda_cess_paie_aut", "checkbox");
            $form->setType("dpc_moda_cess_paie_aut_prec", "textarea");
            $form->setType("dpc_ss_signe_demande_acqu", "checkbox");
            $form->setType("dpc_ss_signe_recher_trouv_acqu", "checkbox");
            $form->setType("dpc_notif_adr_prop", "checkbox");
            $form->setType("dpc_notif_adr_manda", "checkbox");
            $form->setType("dpc_obs", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("donnees_techniques", "hiddenstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("lot", "selectstatic");
            $form->setType("am_lotiss", "hiddenstatic");
            $form->setType("am_autre_div", "hiddenstatic");
            $form->setType("am_camping", "hiddenstatic");
            $form->setType("am_caravane", "hiddenstatic");
            $form->setType("am_carav_duree", "hiddenstatic");
            $form->setType("am_statio", "hiddenstatic");
            $form->setType("am_statio_cont", "hiddenstatic");
            $form->setType("am_affou_exhau", "hiddenstatic");
            $form->setType("am_affou_exhau_sup", "hiddenstatic");
            $form->setType("am_affou_prof", "hiddenstatic");
            $form->setType("am_exhau_haut", "hiddenstatic");
            $form->setType("am_coupe_abat", "hiddenstatic");
            $form->setType("am_prot_plu", "hiddenstatic");
            $form->setType("am_prot_muni", "hiddenstatic");
            $form->setType("am_mobil_voyage", "hiddenstatic");
            $form->setType("am_aire_voyage", "hiddenstatic");
            $form->setType("am_rememb_afu", "hiddenstatic");
            $form->setType("am_parc_resid_loi", "hiddenstatic");
            $form->setType("am_sport_moto", "hiddenstatic");
            $form->setType("am_sport_attrac", "hiddenstatic");
            $form->setType("am_sport_golf", "hiddenstatic");
            $form->setType("am_mob_art", "hiddenstatic");
            $form->setType("am_modif_voie_esp", "hiddenstatic");
            $form->setType("am_plant_voie_esp", "hiddenstatic");
            $form->setType("am_chem_ouv_esp", "hiddenstatic");
            $form->setType("am_agri_peche", "hiddenstatic");
            $form->setType("am_crea_voie", "hiddenstatic");
            $form->setType("am_modif_voie_exist", "hiddenstatic");
            $form->setType("am_crea_esp_sauv", "hiddenstatic");
            $form->setType("am_crea_esp_class", "hiddenstatic");
            $form->setType("am_projet_desc", "hiddenstatic");
            $form->setType("am_terr_surf", "hiddenstatic");
            $form->setType("am_tranche_desc", "hiddenstatic");
            $form->setType("am_lot_max_nb", "hiddenstatic");
            $form->setType("am_lot_max_shon", "hiddenstatic");
            $form->setType("am_lot_cstr_cos", "hiddenstatic");
            $form->setType("am_lot_cstr_plan", "hiddenstatic");
            $form->setType("am_lot_cstr_vente", "hiddenstatic");
            $form->setType("am_lot_fin_diff", "hiddenstatic");
            $form->setType("am_lot_consign", "hiddenstatic");
            $form->setType("am_lot_gar_achev", "hiddenstatic");
            $form->setType("am_lot_vente_ant", "hiddenstatic");
            $form->setType("am_empl_nb", "hiddenstatic");
            $form->setType("am_tente_nb", "hiddenstatic");
            $form->setType("am_carav_nb", "hiddenstatic");
            $form->setType("am_mobil_nb", "hiddenstatic");
            $form->setType("am_pers_nb", "hiddenstatic");
            $form->setType("am_empl_hll_nb", "hiddenstatic");
            $form->setType("am_hll_shon", "hiddenstatic");
            $form->setType("am_periode_exploit", "hiddenstatic");
            $form->setType("am_exist_agrand", "hiddenstatic");
            $form->setType("am_exist_date", "hiddenstatic");
            $form->setType("am_exist_num", "hiddenstatic");
            $form->setType("am_exist_nb_avant", "hiddenstatic");
            $form->setType("am_exist_nb_apres", "hiddenstatic");
            $form->setType("am_coupe_bois", "hiddenstatic");
            $form->setType("am_coupe_parc", "hiddenstatic");
            $form->setType("am_coupe_align", "hiddenstatic");
            $form->setType("am_coupe_ess", "hiddenstatic");
            $form->setType("am_coupe_age", "hiddenstatic");
            $form->setType("am_coupe_dens", "hiddenstatic");
            $form->setType("am_coupe_qual", "hiddenstatic");
            $form->setType("am_coupe_trait", "hiddenstatic");
            $form->setType("am_coupe_autr", "hiddenstatic");
            $form->setType("co_archi_recours", "hiddenstatic");
            $form->setType("co_cstr_nouv", "hiddenstatic");
            $form->setType("co_cstr_exist", "hiddenstatic");
            $form->setType("co_cloture", "hiddenstatic");
            $form->setType("co_elec_tension", "hiddenstatic");
            $form->setType("co_div_terr", "hiddenstatic");
            $form->setType("co_projet_desc", "hiddenstatic");
            $form->setType("co_anx_pisc", "hiddenstatic");
            $form->setType("co_anx_gara", "hiddenstatic");
            $form->setType("co_anx_veran", "hiddenstatic");
            $form->setType("co_anx_abri", "hiddenstatic");
            $form->setType("co_anx_autr", "hiddenstatic");
            $form->setType("co_anx_autr_desc", "hiddenstatic");
            $form->setType("co_tot_log_nb", "hiddenstatic");
            $form->setType("co_tot_ind_nb", "hiddenstatic");
            $form->setType("co_tot_coll_nb", "hiddenstatic");
            $form->setType("co_mais_piece_nb", "hiddenstatic");
            $form->setType("co_mais_niv_nb", "hiddenstatic");
            $form->setType("co_fin_lls_nb", "hiddenstatic");
            $form->setType("co_fin_aa_nb", "hiddenstatic");
            $form->setType("co_fin_ptz_nb", "hiddenstatic");
            $form->setType("co_fin_autr_nb", "hiddenstatic");
            $form->setType("co_fin_autr_desc", "hiddenstatic");
            $form->setType("co_mais_contrat_ind", "hiddenstatic");
            $form->setType("co_uti_pers", "hiddenstatic");
            $form->setType("co_uti_vente", "hiddenstatic");
            $form->setType("co_uti_loc", "hiddenstatic");
            $form->setType("co_uti_princ", "hiddenstatic");
            $form->setType("co_uti_secon", "hiddenstatic");
            $form->setType("co_resid_agees", "hiddenstatic");
            $form->setType("co_resid_etud", "hiddenstatic");
            $form->setType("co_resid_tourism", "hiddenstatic");
            $form->setType("co_resid_hot_soc", "hiddenstatic");
            $form->setType("co_resid_soc", "hiddenstatic");
            $form->setType("co_resid_hand", "hiddenstatic");
            $form->setType("co_resid_autr", "hiddenstatic");
            $form->setType("co_resid_autr_desc", "hiddenstatic");
            $form->setType("co_foyer_chamb_nb", "hiddenstatic");
            $form->setType("co_log_1p_nb", "hiddenstatic");
            $form->setType("co_log_2p_nb", "hiddenstatic");
            $form->setType("co_log_3p_nb", "hiddenstatic");
            $form->setType("co_log_4p_nb", "hiddenstatic");
            $form->setType("co_log_5p_nb", "hiddenstatic");
            $form->setType("co_log_6p_nb", "hiddenstatic");
            $form->setType("co_bat_niv_nb", "hiddenstatic");
            $form->setType("co_trx_exten", "hiddenstatic");
            $form->setType("co_trx_surelev", "hiddenstatic");
            $form->setType("co_trx_nivsup", "hiddenstatic");
            $form->setType("co_demont_periode", "hiddenstatic");
            $form->setType("co_sp_transport", "hiddenstatic");
            $form->setType("co_sp_enseign", "hiddenstatic");
            $form->setType("co_sp_act_soc", "hiddenstatic");
            $form->setType("co_sp_ouvr_spe", "hiddenstatic");
            $form->setType("co_sp_sante", "hiddenstatic");
            $form->setType("co_sp_culture", "hiddenstatic");
            $form->setType("co_statio_avt_nb", "hiddenstatic");
            $form->setType("co_statio_apr_nb", "hiddenstatic");
            $form->setType("co_statio_adr", "hiddenstatic");
            $form->setType("co_statio_place_nb", "hiddenstatic");
            $form->setType("co_statio_tot_surf", "hiddenstatic");
            $form->setType("co_statio_tot_shob", "hiddenstatic");
            $form->setType("co_statio_comm_cin_surf", "hiddenstatic");
            $form->setType("su_avt_shon1", "hiddenstatic");
            $form->setType("su_avt_shon2", "hiddenstatic");
            $form->setType("su_avt_shon3", "hiddenstatic");
            $form->setType("su_avt_shon4", "hiddenstatic");
            $form->setType("su_avt_shon5", "hiddenstatic");
            $form->setType("su_avt_shon6", "hiddenstatic");
            $form->setType("su_avt_shon7", "hiddenstatic");
            $form->setType("su_avt_shon8", "hiddenstatic");
            $form->setType("su_avt_shon9", "hiddenstatic");
            $form->setType("su_cstr_shon1", "hiddenstatic");
            $form->setType("su_cstr_shon2", "hiddenstatic");
            $form->setType("su_cstr_shon3", "hiddenstatic");
            $form->setType("su_cstr_shon4", "hiddenstatic");
            $form->setType("su_cstr_shon5", "hiddenstatic");
            $form->setType("su_cstr_shon6", "hiddenstatic");
            $form->setType("su_cstr_shon7", "hiddenstatic");
            $form->setType("su_cstr_shon8", "hiddenstatic");
            $form->setType("su_cstr_shon9", "hiddenstatic");
            $form->setType("su_trsf_shon1", "hiddenstatic");
            $form->setType("su_trsf_shon2", "hiddenstatic");
            $form->setType("su_trsf_shon3", "hiddenstatic");
            $form->setType("su_trsf_shon4", "hiddenstatic");
            $form->setType("su_trsf_shon5", "hiddenstatic");
            $form->setType("su_trsf_shon6", "hiddenstatic");
            $form->setType("su_trsf_shon7", "hiddenstatic");
            $form->setType("su_trsf_shon8", "hiddenstatic");
            $form->setType("su_trsf_shon9", "hiddenstatic");
            $form->setType("su_chge_shon1", "hiddenstatic");
            $form->setType("su_chge_shon2", "hiddenstatic");
            $form->setType("su_chge_shon3", "hiddenstatic");
            $form->setType("su_chge_shon4", "hiddenstatic");
            $form->setType("su_chge_shon5", "hiddenstatic");
            $form->setType("su_chge_shon6", "hiddenstatic");
            $form->setType("su_chge_shon7", "hiddenstatic");
            $form->setType("su_chge_shon8", "hiddenstatic");
            $form->setType("su_chge_shon9", "hiddenstatic");
            $form->setType("su_demo_shon1", "hiddenstatic");
            $form->setType("su_demo_shon2", "hiddenstatic");
            $form->setType("su_demo_shon3", "hiddenstatic");
            $form->setType("su_demo_shon4", "hiddenstatic");
            $form->setType("su_demo_shon5", "hiddenstatic");
            $form->setType("su_demo_shon6", "hiddenstatic");
            $form->setType("su_demo_shon7", "hiddenstatic");
            $form->setType("su_demo_shon8", "hiddenstatic");
            $form->setType("su_demo_shon9", "hiddenstatic");
            $form->setType("su_sup_shon1", "hiddenstatic");
            $form->setType("su_sup_shon2", "hiddenstatic");
            $form->setType("su_sup_shon3", "hiddenstatic");
            $form->setType("su_sup_shon4", "hiddenstatic");
            $form->setType("su_sup_shon5", "hiddenstatic");
            $form->setType("su_sup_shon6", "hiddenstatic");
            $form->setType("su_sup_shon7", "hiddenstatic");
            $form->setType("su_sup_shon8", "hiddenstatic");
            $form->setType("su_sup_shon9", "hiddenstatic");
            $form->setType("su_tot_shon1", "hiddenstatic");
            $form->setType("su_tot_shon2", "hiddenstatic");
            $form->setType("su_tot_shon3", "hiddenstatic");
            $form->setType("su_tot_shon4", "hiddenstatic");
            $form->setType("su_tot_shon5", "hiddenstatic");
            $form->setType("su_tot_shon6", "hiddenstatic");
            $form->setType("su_tot_shon7", "hiddenstatic");
            $form->setType("su_tot_shon8", "hiddenstatic");
            $form->setType("su_tot_shon9", "hiddenstatic");
            $form->setType("su_avt_shon_tot", "hiddenstatic");
            $form->setType("su_cstr_shon_tot", "hiddenstatic");
            $form->setType("su_trsf_shon_tot", "hiddenstatic");
            $form->setType("su_chge_shon_tot", "hiddenstatic");
            $form->setType("su_demo_shon_tot", "hiddenstatic");
            $form->setType("su_sup_shon_tot", "hiddenstatic");
            $form->setType("su_tot_shon_tot", "hiddenstatic");
            $form->setType("dm_constr_dates", "hiddenstatic");
            $form->setType("dm_total", "hiddenstatic");
            $form->setType("dm_partiel", "hiddenstatic");
            $form->setType("dm_projet_desc", "hiddenstatic");
            $form->setType("dm_tot_log_nb", "hiddenstatic");
            $form->setType("tax_surf_tot", "hiddenstatic");
            $form->setType("tax_surf", "hiddenstatic");
            $form->setType("tax_surf_suppr_mod", "hiddenstatic");
            $form->setType("tax_su_princ_log_nb1", "hiddenstatic");
            $form->setType("tax_su_princ_log_nb2", "hiddenstatic");
            $form->setType("tax_su_princ_log_nb3", "hiddenstatic");
            $form->setType("tax_su_princ_log_nb4", "hiddenstatic");
            $form->setType("tax_su_princ_log_nb_tot1", "hiddenstatic");
            $form->setType("tax_su_princ_log_nb_tot2", "hiddenstatic");
            $form->setType("tax_su_princ_log_nb_tot3", "hiddenstatic");
            $form->setType("tax_su_princ_log_nb_tot4", "hiddenstatic");
            $form->setType("tax_su_princ_surf1", "hiddenstatic");
            $form->setType("tax_su_princ_surf2", "hiddenstatic");
            $form->setType("tax_su_princ_surf3", "hiddenstatic");
            $form->setType("tax_su_princ_surf4", "hiddenstatic");
            $form->setType("tax_su_princ_surf_sup1", "hiddenstatic");
            $form->setType("tax_su_princ_surf_sup2", "hiddenstatic");
            $form->setType("tax_su_princ_surf_sup3", "hiddenstatic");
            $form->setType("tax_su_princ_surf_sup4", "hiddenstatic");
            $form->setType("tax_su_heber_log_nb1", "hiddenstatic");
            $form->setType("tax_su_heber_log_nb2", "hiddenstatic");
            $form->setType("tax_su_heber_log_nb3", "hiddenstatic");
            $form->setType("tax_su_heber_log_nb_tot1", "hiddenstatic");
            $form->setType("tax_su_heber_log_nb_tot2", "hiddenstatic");
            $form->setType("tax_su_heber_log_nb_tot3", "hiddenstatic");
            $form->setType("tax_su_heber_surf1", "hiddenstatic");
            $form->setType("tax_su_heber_surf2", "hiddenstatic");
            $form->setType("tax_su_heber_surf3", "hiddenstatic");
            $form->setType("tax_su_heber_surf_sup1", "hiddenstatic");
            $form->setType("tax_su_heber_surf_sup2", "hiddenstatic");
            $form->setType("tax_su_heber_surf_sup3", "hiddenstatic");
            $form->setType("tax_su_secon_log_nb", "hiddenstatic");
            $form->setType("tax_su_tot_log_nb", "hiddenstatic");
            $form->setType("tax_su_secon_log_nb_tot", "hiddenstatic");
            $form->setType("tax_su_tot_log_nb_tot", "hiddenstatic");
            $form->setType("tax_su_secon_surf", "hiddenstatic");
            $form->setType("tax_su_tot_surf", "hiddenstatic");
            $form->setType("tax_su_secon_surf_sup", "hiddenstatic");
            $form->setType("tax_su_tot_surf_sup", "hiddenstatic");
            $form->setType("tax_ext_pret", "hiddenstatic");
            $form->setType("tax_ext_desc", "hiddenstatic");
            $form->setType("tax_surf_tax_exist_cons", "hiddenstatic");
            $form->setType("tax_log_exist_nb", "hiddenstatic");
            $form->setType("tax_am_statio_ext", "hiddenstatic");
            $form->setType("tax_sup_bass_pisc", "hiddenstatic");
            $form->setType("tax_empl_ten_carav_mobil_nb", "hiddenstatic");
            $form->setType("tax_empl_hll_nb", "hiddenstatic");
            $form->setType("tax_eol_haut_nb", "hiddenstatic");
            $form->setType("tax_pann_volt_sup", "hiddenstatic");
            $form->setType("tax_am_statio_ext_sup", "hiddenstatic");
            $form->setType("tax_sup_bass_pisc_sup", "hiddenstatic");
            $form->setType("tax_empl_ten_carav_mobil_nb_sup", "hiddenstatic");
            $form->setType("tax_empl_hll_nb_sup", "hiddenstatic");
            $form->setType("tax_eol_haut_nb_sup", "hiddenstatic");
            $form->setType("tax_pann_volt_sup_sup", "hiddenstatic");
            $form->setType("tax_trx_presc_ppr", "hiddenstatic");
            $form->setType("tax_monu_hist", "hiddenstatic");
            $form->setType("tax_comm_nb", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf1", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf2", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf3", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf4", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf5", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf6", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf7", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_sup1", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_sup2", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_sup3", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_sup4", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_sup5", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_sup6", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_sup7", "hiddenstatic");
            $form->setType("vsd_surf_planch_smd", "hiddenstatic");
            $form->setType("vsd_unit_fonc_sup", "hiddenstatic");
            $form->setType("vsd_unit_fonc_constr_sup", "hiddenstatic");
            $form->setType("vsd_val_terr", "hiddenstatic");
            $form->setType("vsd_const_sxist_non_dem_surf", "hiddenstatic");
            $form->setType("vsd_rescr_fisc", "hiddenstatic");
            $form->setType("pld_val_terr", "hiddenstatic");
            $form->setType("pld_const_exist_dem", "hiddenstatic");
            $form->setType("pld_const_exist_dem_surf", "hiddenstatic");
            $form->setType("code_cnil", "hiddenstatic");
            $form->setType("terr_juri_titul", "hiddenstatic");
            $form->setType("terr_juri_lot", "hiddenstatic");
            $form->setType("terr_juri_zac", "hiddenstatic");
            $form->setType("terr_juri_afu", "hiddenstatic");
            $form->setType("terr_juri_pup", "hiddenstatic");
            $form->setType("terr_juri_oin", "hiddenstatic");
            $form->setType("terr_juri_desc", "hiddenstatic");
            $form->setType("terr_div_surf_etab", "hiddenstatic");
            $form->setType("terr_div_surf_av_div", "hiddenstatic");
            $form->setType("doc_date", "hiddenstatic");
            $form->setType("doc_tot_trav", "hiddenstatic");
            $form->setType("doc_tranche_trav", "hiddenstatic");
            $form->setType("doc_tranche_trav_desc", "hiddenstatic");
            $form->setType("doc_surf", "hiddenstatic");
            $form->setType("doc_nb_log", "hiddenstatic");
            $form->setType("doc_nb_log_indiv", "hiddenstatic");
            $form->setType("doc_nb_log_coll", "hiddenstatic");
            $form->setType("doc_nb_log_lls", "hiddenstatic");
            $form->setType("doc_nb_log_aa", "hiddenstatic");
            $form->setType("doc_nb_log_ptz", "hiddenstatic");
            $form->setType("doc_nb_log_autre", "hiddenstatic");
            $form->setType("daact_date", "hiddenstatic");
            $form->setType("daact_date_chgmt_dest", "hiddenstatic");
            $form->setType("daact_tot_trav", "hiddenstatic");
            $form->setType("daact_tranche_trav", "hiddenstatic");
            $form->setType("daact_tranche_trav_desc", "hiddenstatic");
            $form->setType("daact_surf", "hiddenstatic");
            $form->setType("daact_nb_log", "hiddenstatic");
            $form->setType("daact_nb_log_indiv", "hiddenstatic");
            $form->setType("daact_nb_log_coll", "hiddenstatic");
            $form->setType("daact_nb_log_lls", "hiddenstatic");
            $form->setType("daact_nb_log_aa", "hiddenstatic");
            $form->setType("daact_nb_log_ptz", "hiddenstatic");
            $form->setType("daact_nb_log_autre", "hiddenstatic");
            $form->setType("dossier_autorisation", "selectstatic");
            $form->setType("am_div_mun", "hiddenstatic");
            $form->setType("co_perf_energ", "hiddenstatic");
            $form->setType("architecte", "selectstatic");
            $form->setType("co_statio_avt_shob", "hiddenstatic");
            $form->setType("co_statio_apr_shob", "hiddenstatic");
            $form->setType("co_statio_avt_surf", "hiddenstatic");
            $form->setType("co_statio_apr_surf", "hiddenstatic");
            $form->setType("co_trx_amgt", "hiddenstatic");
            $form->setType("co_modif_aspect", "hiddenstatic");
            $form->setType("co_modif_struct", "hiddenstatic");
            $form->setType("co_ouvr_elec", "hiddenstatic");
            $form->setType("co_ouvr_infra", "hiddenstatic");
            $form->setType("co_trx_imm", "hiddenstatic");
            $form->setType("co_cstr_shob", "hiddenstatic");
            $form->setType("am_voyage_deb", "hiddenstatic");
            $form->setType("am_voyage_fin", "hiddenstatic");
            $form->setType("am_modif_amgt", "hiddenstatic");
            $form->setType("am_lot_max_shob", "hiddenstatic");
            $form->setType("mod_desc", "hiddenstatic");
            $form->setType("tr_total", "hiddenstatic");
            $form->setType("tr_partiel", "hiddenstatic");
            $form->setType("tr_desc", "hiddenstatic");
            $form->setType("avap_co_elt_pro", "hiddenstatic");
            $form->setType("avap_nouv_haut_surf", "hiddenstatic");
            $form->setType("avap_co_clot", "hiddenstatic");
            $form->setType("avap_aut_coup_aba_arb", "hiddenstatic");
            $form->setType("avap_ouv_infra", "hiddenstatic");
            $form->setType("avap_aut_inst_mob", "hiddenstatic");
            $form->setType("avap_aut_plant", "hiddenstatic");
            $form->setType("avap_aut_auv_elec", "hiddenstatic");
            $form->setType("tax_dest_loc_tr", "hiddenstatic");
            $form->setType("ope_proj_desc", "hiddenstatic");
            $form->setType("tax_surf_tot_cstr", "hiddenstatic");
            $form->setType("cerfa", "hiddenstatic");
            $form->setType("tax_surf_loc_stat", "hiddenstatic");
            $form->setType("tax_su_princ_surf_stat1", "hiddenstatic");
            $form->setType("tax_su_princ_surf_stat2", "hiddenstatic");
            $form->setType("tax_su_princ_surf_stat3", "hiddenstatic");
            $form->setType("tax_su_princ_surf_stat4", "hiddenstatic");
            $form->setType("tax_su_secon_surf_stat", "hiddenstatic");
            $form->setType("tax_su_heber_surf_stat1", "hiddenstatic");
            $form->setType("tax_su_heber_surf_stat2", "hiddenstatic");
            $form->setType("tax_su_heber_surf_stat3", "hiddenstatic");
            $form->setType("tax_su_tot_surf_stat", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_stat1", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_stat2", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_stat3", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_stat4", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_stat5", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_stat6", "hiddenstatic");
            $form->setType("tax_su_non_habit_surf_stat7", "hiddenstatic");
            $form->setType("tax_su_parc_statio_expl_comm_surf", "hiddenstatic");
            $form->setType("tax_log_ap_trvx_nb", "hiddenstatic");
            $form->setType("tax_am_statio_ext_cr", "hiddenstatic");
            $form->setType("tax_sup_bass_pisc_cr", "hiddenstatic");
            $form->setType("tax_empl_ten_carav_mobil_nb_cr", "hiddenstatic");
            $form->setType("tax_empl_hll_nb_cr", "hiddenstatic");
            $form->setType("tax_eol_haut_nb_cr", "hiddenstatic");
            $form->setType("tax_pann_volt_sup_cr", "hiddenstatic");
            $form->setType("tax_surf_loc_arch", "hiddenstatic");
            $form->setType("tax_surf_pisc_arch", "hiddenstatic");
            $form->setType("tax_am_statio_ext_arch", "hiddenstatic");
            $form->setType("tax_empl_ten_carav_mobil_nb_arch", "hiddenstatic");
            $form->setType("tax_empl_hll_nb_arch", "hiddenstatic");
            $form->setType("tax_eol_haut_nb_arch", "hiddenstatic");
            $form->setType("ope_proj_div_co", "hiddenstatic");
            $form->setType("ope_proj_div_contr", "hiddenstatic");
            $form->setType("tax_desc", "hiddenstatic");
            $form->setType("erp_cstr_neuve", "hiddenstatic");
            $form->setType("erp_trvx_acc", "hiddenstatic");
            $form->setType("erp_extension", "hiddenstatic");
            $form->setType("erp_rehab", "hiddenstatic");
            $form->setType("erp_trvx_am", "hiddenstatic");
            $form->setType("erp_vol_nouv_exist", "hiddenstatic");
            $form->setType("erp_loc_eff1", "hiddenstatic");
            $form->setType("erp_loc_eff2", "hiddenstatic");
            $form->setType("erp_loc_eff3", "hiddenstatic");
            $form->setType("erp_loc_eff4", "hiddenstatic");
            $form->setType("erp_loc_eff5", "hiddenstatic");
            $form->setType("erp_loc_eff_tot", "hiddenstatic");
            $form->setType("erp_public_eff1", "hiddenstatic");
            $form->setType("erp_public_eff2", "hiddenstatic");
            $form->setType("erp_public_eff3", "hiddenstatic");
            $form->setType("erp_public_eff4", "hiddenstatic");
            $form->setType("erp_public_eff5", "hiddenstatic");
            $form->setType("erp_public_eff_tot", "hiddenstatic");
            $form->setType("erp_perso_eff1", "hiddenstatic");
            $form->setType("erp_perso_eff2", "hiddenstatic");
            $form->setType("erp_perso_eff3", "hiddenstatic");
            $form->setType("erp_perso_eff4", "hiddenstatic");
            $form->setType("erp_perso_eff5", "hiddenstatic");
            $form->setType("erp_perso_eff_tot", "hiddenstatic");
            $form->setType("erp_tot_eff1", "hiddenstatic");
            $form->setType("erp_tot_eff2", "hiddenstatic");
            $form->setType("erp_tot_eff3", "hiddenstatic");
            $form->setType("erp_tot_eff4", "hiddenstatic");
            $form->setType("erp_tot_eff5", "hiddenstatic");
            $form->setType("erp_tot_eff_tot", "hiddenstatic");
            $form->setType("erp_class_cat", "hiddenstatic");
            $form->setType("erp_class_type", "hiddenstatic");
            $form->setType("tax_surf_abr_jard_pig_colom", "hiddenstatic");
            $form->setType("tax_su_non_habit_abr_jard_pig_colom", "hiddenstatic");
            $form->setType("dia_imm_non_bati", "hiddenstatic");
            $form->setType("dia_imm_bati_terr_propr", "hiddenstatic");
            $form->setType("dia_imm_bati_terr_autr", "hiddenstatic");
            $form->setType("dia_imm_bati_terr_autr_desc", "hiddenstatic");
            $form->setType("dia_bat_copro", "hiddenstatic");
            $form->setType("dia_bat_copro_desc", "hiddenstatic");
            $form->setType("dia_lot_numero", "hiddenstatic");
            $form->setType("dia_lot_bat", "hiddenstatic");
            $form->setType("dia_lot_etage", "hiddenstatic");
            $form->setType("dia_lot_quote_part", "hiddenstatic");
            $form->setType("dia_us_hab", "hiddenstatic");
            $form->setType("dia_us_pro", "hiddenstatic");
            $form->setType("dia_us_mixte", "hiddenstatic");
            $form->setType("dia_us_comm", "hiddenstatic");
            $form->setType("dia_us_agr", "hiddenstatic");
            $form->setType("dia_us_autre", "hiddenstatic");
            $form->setType("dia_us_autre_prec", "hiddenstatic");
            $form->setType("dia_occ_prop", "hiddenstatic");
            $form->setType("dia_occ_loc", "hiddenstatic");
            $form->setType("dia_occ_sans_occ", "hiddenstatic");
            $form->setType("dia_occ_autre", "hiddenstatic");
            $form->setType("dia_occ_autre_prec", "hiddenstatic");
            $form->setType("dia_mod_cess_prix_vente", "hiddenstatic");
            $form->setType("dia_mod_cess_prix_vente_mob", "hiddenstatic");
            $form->setType("dia_mod_cess_prix_vente_cheptel", "hiddenstatic");
            $form->setType("dia_mod_cess_prix_vente_recol", "hiddenstatic");
            $form->setType("dia_mod_cess_prix_vente_autre", "hiddenstatic");
            $form->setType("dia_mod_cess_commi", "hiddenstatic");
            $form->setType("dia_mod_cess_commi_ttc", "hiddenstatic");
            $form->setType("dia_mod_cess_commi_ht", "hiddenstatic");
            $form->setType("dia_acquereur_nom_prenom", "hiddenstatic");
            $form->setType("dia_acquereur_adr_num_voie", "hiddenstatic");
            $form->setType("dia_acquereur_adr_ext", "hiddenstatic");
            $form->setType("dia_acquereur_adr_type_voie", "hiddenstatic");
            $form->setType("dia_acquereur_adr_nom_voie", "hiddenstatic");
            $form->setType("dia_acquereur_adr_lieu_dit_bp", "hiddenstatic");
            $form->setType("dia_acquereur_adr_cp", "hiddenstatic");
            $form->setType("dia_acquereur_adr_localite", "hiddenstatic");
            $form->setType("dia_observation", "hiddenstatic");
            $form->setType("su2_avt_shon1", "hiddenstatic");
            $form->setType("su2_avt_shon2", "hiddenstatic");
            $form->setType("su2_avt_shon3", "hiddenstatic");
            $form->setType("su2_avt_shon4", "hiddenstatic");
            $form->setType("su2_avt_shon5", "hiddenstatic");
            $form->setType("su2_avt_shon6", "hiddenstatic");
            $form->setType("su2_avt_shon7", "hiddenstatic");
            $form->setType("su2_avt_shon8", "hiddenstatic");
            $form->setType("su2_avt_shon9", "hiddenstatic");
            $form->setType("su2_avt_shon10", "hiddenstatic");
            $form->setType("su2_avt_shon11", "hiddenstatic");
            $form->setType("su2_avt_shon12", "hiddenstatic");
            $form->setType("su2_avt_shon13", "hiddenstatic");
            $form->setType("su2_avt_shon14", "hiddenstatic");
            $form->setType("su2_avt_shon15", "hiddenstatic");
            $form->setType("su2_avt_shon16", "hiddenstatic");
            $form->setType("su2_avt_shon17", "hiddenstatic");
            $form->setType("su2_avt_shon18", "hiddenstatic");
            $form->setType("su2_avt_shon19", "hiddenstatic");
            $form->setType("su2_avt_shon20", "hiddenstatic");
            $form->setType("su2_avt_shon_tot", "hiddenstatic");
            $form->setType("su2_cstr_shon1", "hiddenstatic");
            $form->setType("su2_cstr_shon2", "hiddenstatic");
            $form->setType("su2_cstr_shon3", "hiddenstatic");
            $form->setType("su2_cstr_shon4", "hiddenstatic");
            $form->setType("su2_cstr_shon5", "hiddenstatic");
            $form->setType("su2_cstr_shon6", "hiddenstatic");
            $form->setType("su2_cstr_shon7", "hiddenstatic");
            $form->setType("su2_cstr_shon8", "hiddenstatic");
            $form->setType("su2_cstr_shon9", "hiddenstatic");
            $form->setType("su2_cstr_shon10", "hiddenstatic");
            $form->setType("su2_cstr_shon11", "hiddenstatic");
            $form->setType("su2_cstr_shon12", "hiddenstatic");
            $form->setType("su2_cstr_shon13", "hiddenstatic");
            $form->setType("su2_cstr_shon14", "hiddenstatic");
            $form->setType("su2_cstr_shon15", "hiddenstatic");
            $form->setType("su2_cstr_shon16", "hiddenstatic");
            $form->setType("su2_cstr_shon17", "hiddenstatic");
            $form->setType("su2_cstr_shon18", "hiddenstatic");
            $form->setType("su2_cstr_shon19", "hiddenstatic");
            $form->setType("su2_cstr_shon20", "hiddenstatic");
            $form->setType("su2_cstr_shon_tot", "hiddenstatic");
            $form->setType("su2_chge_shon1", "hiddenstatic");
            $form->setType("su2_chge_shon2", "hiddenstatic");
            $form->setType("su2_chge_shon3", "hiddenstatic");
            $form->setType("su2_chge_shon4", "hiddenstatic");
            $form->setType("su2_chge_shon5", "hiddenstatic");
            $form->setType("su2_chge_shon6", "hiddenstatic");
            $form->setType("su2_chge_shon7", "hiddenstatic");
            $form->setType("su2_chge_shon8", "hiddenstatic");
            $form->setType("su2_chge_shon9", "hiddenstatic");
            $form->setType("su2_chge_shon10", "hiddenstatic");
            $form->setType("su2_chge_shon11", "hiddenstatic");
            $form->setType("su2_chge_shon12", "hiddenstatic");
            $form->setType("su2_chge_shon13", "hiddenstatic");
            $form->setType("su2_chge_shon14", "hiddenstatic");
            $form->setType("su2_chge_shon15", "hiddenstatic");
            $form->setType("su2_chge_shon16", "hiddenstatic");
            $form->setType("su2_chge_shon17", "hiddenstatic");
            $form->setType("su2_chge_shon18", "hiddenstatic");
            $form->setType("su2_chge_shon19", "hiddenstatic");
            $form->setType("su2_chge_shon20", "hiddenstatic");
            $form->setType("su2_chge_shon_tot", "hiddenstatic");
            $form->setType("su2_demo_shon1", "hiddenstatic");
            $form->setType("su2_demo_shon2", "hiddenstatic");
            $form->setType("su2_demo_shon3", "hiddenstatic");
            $form->setType("su2_demo_shon4", "hiddenstatic");
            $form->setType("su2_demo_shon5", "hiddenstatic");
            $form->setType("su2_demo_shon6", "hiddenstatic");
            $form->setType("su2_demo_shon7", "hiddenstatic");
            $form->setType("su2_demo_shon8", "hiddenstatic");
            $form->setType("su2_demo_shon9", "hiddenstatic");
            $form->setType("su2_demo_shon10", "hiddenstatic");
            $form->setType("su2_demo_shon11", "hiddenstatic");
            $form->setType("su2_demo_shon12", "hiddenstatic");
            $form->setType("su2_demo_shon13", "hiddenstatic");
            $form->setType("su2_demo_shon14", "hiddenstatic");
            $form->setType("su2_demo_shon15", "hiddenstatic");
            $form->setType("su2_demo_shon16", "hiddenstatic");
            $form->setType("su2_demo_shon17", "hiddenstatic");
            $form->setType("su2_demo_shon18", "hiddenstatic");
            $form->setType("su2_demo_shon19", "hiddenstatic");
            $form->setType("su2_demo_shon20", "hiddenstatic");
            $form->setType("su2_demo_shon_tot", "hiddenstatic");
            $form->setType("su2_sup_shon1", "hiddenstatic");
            $form->setType("su2_sup_shon2", "hiddenstatic");
            $form->setType("su2_sup_shon3", "hiddenstatic");
            $form->setType("su2_sup_shon4", "hiddenstatic");
            $form->setType("su2_sup_shon5", "hiddenstatic");
            $form->setType("su2_sup_shon6", "hiddenstatic");
            $form->setType("su2_sup_shon7", "hiddenstatic");
            $form->setType("su2_sup_shon8", "hiddenstatic");
            $form->setType("su2_sup_shon9", "hiddenstatic");
            $form->setType("su2_sup_shon10", "hiddenstatic");
            $form->setType("su2_sup_shon11", "hiddenstatic");
            $form->setType("su2_sup_shon12", "hiddenstatic");
            $form->setType("su2_sup_shon13", "hiddenstatic");
            $form->setType("su2_sup_shon14", "hiddenstatic");
            $form->setType("su2_sup_shon15", "hiddenstatic");
            $form->setType("su2_sup_shon16", "hiddenstatic");
            $form->setType("su2_sup_shon17", "hiddenstatic");
            $form->setType("su2_sup_shon18", "hiddenstatic");
            $form->setType("su2_sup_shon19", "hiddenstatic");
            $form->setType("su2_sup_shon20", "hiddenstatic");
            $form->setType("su2_sup_shon_tot", "hiddenstatic");
            $form->setType("su2_tot_shon1", "hiddenstatic");
            $form->setType("su2_tot_shon2", "hiddenstatic");
            $form->setType("su2_tot_shon3", "hiddenstatic");
            $form->setType("su2_tot_shon4", "hiddenstatic");
            $form->setType("su2_tot_shon5", "hiddenstatic");
            $form->setType("su2_tot_shon6", "hiddenstatic");
            $form->setType("su2_tot_shon7", "hiddenstatic");
            $form->setType("su2_tot_shon8", "hiddenstatic");
            $form->setType("su2_tot_shon9", "hiddenstatic");
            $form->setType("su2_tot_shon10", "hiddenstatic");
            $form->setType("su2_tot_shon11", "hiddenstatic");
            $form->setType("su2_tot_shon12", "hiddenstatic");
            $form->setType("su2_tot_shon13", "hiddenstatic");
            $form->setType("su2_tot_shon14", "hiddenstatic");
            $form->setType("su2_tot_shon15", "hiddenstatic");
            $form->setType("su2_tot_shon16", "hiddenstatic");
            $form->setType("su2_tot_shon17", "hiddenstatic");
            $form->setType("su2_tot_shon18", "hiddenstatic");
            $form->setType("su2_tot_shon19", "hiddenstatic");
            $form->setType("su2_tot_shon20", "hiddenstatic");
            $form->setType("su2_tot_shon_tot", "hiddenstatic");
            $form->setType("dia_occ_sol_su_terre", "hiddenstatic");
            $form->setType("dia_occ_sol_su_pres", "hiddenstatic");
            $form->setType("dia_occ_sol_su_verger", "hiddenstatic");
            $form->setType("dia_occ_sol_su_vigne", "hiddenstatic");
            $form->setType("dia_occ_sol_su_bois", "hiddenstatic");
            $form->setType("dia_occ_sol_su_lande", "hiddenstatic");
            $form->setType("dia_occ_sol_su_carriere", "hiddenstatic");
            $form->setType("dia_occ_sol_su_eau_cadastree", "hiddenstatic");
            $form->setType("dia_occ_sol_su_jardin", "hiddenstatic");
            $form->setType("dia_occ_sol_su_terr_batir", "hiddenstatic");
            $form->setType("dia_occ_sol_su_terr_agr", "hiddenstatic");
            $form->setType("dia_occ_sol_su_sol", "hiddenstatic");
            $form->setType("dia_bati_vend_tot", "hiddenstatic");
            $form->setType("dia_bati_vend_tot_txt", "hiddenstatic");
            $form->setType("dia_su_co_sol", "hiddenstatic");
            $form->setType("dia_su_util_hab", "hiddenstatic");
            $form->setType("dia_nb_niv", "hiddenstatic");
            $form->setType("dia_nb_appart", "hiddenstatic");
            $form->setType("dia_nb_autre_loc", "hiddenstatic");
            $form->setType("dia_vente_lot_volume", "hiddenstatic");
            $form->setType("dia_vente_lot_volume_txt", "hiddenstatic");
            $form->setType("dia_lot_nat_su", "hiddenstatic");
            $form->setType("dia_lot_bat_achv_plus_10", "hiddenstatic");
            $form->setType("dia_lot_bat_achv_moins_10", "hiddenstatic");
            $form->setType("dia_lot_regl_copro_publ_hypo_plus_10", "hiddenstatic");
            $form->setType("dia_lot_regl_copro_publ_hypo_moins_10", "hiddenstatic");
            $form->setType("dia_indivi_quote_part", "hiddenstatic");
            $form->setType("dia_design_societe", "hiddenstatic");
            $form->setType("dia_design_droit", "hiddenstatic");
            $form->setType("dia_droit_soc_nat", "hiddenstatic");
            $form->setType("dia_droit_soc_nb", "hiddenstatic");
            $form->setType("dia_droit_soc_num_part", "hiddenstatic");
            $form->setType("dia_droit_reel_perso_grevant_bien_oui", "hiddenstatic");
            $form->setType("dia_droit_reel_perso_grevant_bien_non", "hiddenstatic");
            $form->setType("dia_droit_reel_perso_nat", "hiddenstatic");
            $form->setType("dia_droit_reel_perso_viag", "hiddenstatic");
            $form->setType("dia_mod_cess_adr", "hiddenstatic");
            $form->setType("dia_mod_cess_sign_act_auth", "hiddenstatic");
            $form->setType("dia_mod_cess_terme", "hiddenstatic");
            $form->setType("dia_mod_cess_terme_prec", "hiddenstatic");
            $form->setType("dia_mod_cess_bene_acquereur", "hiddenstatic");
            $form->setType("dia_mod_cess_bene_vendeur", "hiddenstatic");
            $form->setType("dia_mod_cess_paie_nat", "hiddenstatic");
            $form->setType("dia_mod_cess_design_contr_alien", "hiddenstatic");
            $form->setType("dia_mod_cess_eval_contr", "hiddenstatic");
            $form->setType("dia_mod_cess_rente_viag", "hiddenstatic");
            $form->setType("dia_mod_cess_mnt_an", "hiddenstatic");
            $form->setType("dia_mod_cess_mnt_compt", "hiddenstatic");
            $form->setType("dia_mod_cess_bene_rente", "hiddenstatic");
            $form->setType("dia_mod_cess_droit_usa_hab", "hiddenstatic");
            $form->setType("dia_mod_cess_droit_usa_hab_prec", "hiddenstatic");
            $form->setType("dia_mod_cess_eval_usa_usufruit", "hiddenstatic");
            $form->setType("dia_mod_cess_vente_nue_prop", "hiddenstatic");
            $form->setType("dia_mod_cess_vente_nue_prop_prec", "hiddenstatic");
            $form->setType("dia_mod_cess_echange", "hiddenstatic");
            $form->setType("dia_mod_cess_design_bien_recus_ech", "hiddenstatic");
            $form->setType("dia_mod_cess_mnt_soulte", "hiddenstatic");
            $form->setType("dia_mod_cess_prop_contre_echan", "hiddenstatic");
            $form->setType("dia_mod_cess_apport_societe", "hiddenstatic");
            $form->setType("dia_mod_cess_bene", "hiddenstatic");
            $form->setType("dia_mod_cess_esti_bien", "hiddenstatic");
            $form->setType("dia_mod_cess_cess_terr_loc_co", "hiddenstatic");
            $form->setType("dia_mod_cess_esti_terr", "hiddenstatic");
            $form->setType("dia_mod_cess_esti_loc", "hiddenstatic");
            $form->setType("dia_mod_cess_esti_imm_loca", "hiddenstatic");
            $form->setType("dia_mod_cess_adju_vol", "hiddenstatic");
            $form->setType("dia_mod_cess_adju_obl", "hiddenstatic");
            $form->setType("dia_mod_cess_adju_fin_indivi", "hiddenstatic");
            $form->setType("dia_mod_cess_adju_date_lieu", "hiddenstatic");
            $form->setType("dia_mod_cess_mnt_mise_prix", "hiddenstatic");
            $form->setType("dia_prop_titu_prix_indique", "hiddenstatic");
            $form->setType("dia_prop_recherche_acqu_prix_indique", "hiddenstatic");
            $form->setType("dia_acquereur_prof", "hiddenstatic");
            $form->setType("dia_indic_compl_ope", "hiddenstatic");
            $form->setType("dia_vente_adju", "hiddenstatic");
            $form->setType("am_terr_res_demon", "hiddenstatic");
            $form->setType("am_air_terr_res_mob", "hiddenstatic");
            $form->setType("ctx_objet_recours", "selectstatic");
            $form->setType("ctx_reference_sagace", "hiddenstatic");
            $form->setType("ctx_nature_travaux_infra_om_html", "hiddenstatic");
            $form->setType("ctx_synthese_nti", "hiddenstatic");
            $form->setType("ctx_article_non_resp_om_html", "hiddenstatic");
            $form->setType("ctx_synthese_anr", "hiddenstatic");
            $form->setType("ctx_reference_parquet", "hiddenstatic");
            $form->setType("ctx_element_taxation", "hiddenstatic");
            $form->setType("ctx_infraction", "hiddenstatic");
            $form->setType("ctx_regularisable", "hiddenstatic");
            $form->setType("ctx_reference_courrier", "hiddenstatic");
            $form->setType("ctx_date_audience", "hiddenstatic");
            $form->setType("ctx_date_ajournement", "hiddenstatic");
            $form->setType("exo_facul_1", "hiddenstatic");
            $form->setType("exo_facul_2", "hiddenstatic");
            $form->setType("exo_facul_3", "hiddenstatic");
            $form->setType("exo_facul_4", "hiddenstatic");
            $form->setType("exo_facul_5", "hiddenstatic");
            $form->setType("exo_facul_6", "hiddenstatic");
            $form->setType("exo_facul_7", "hiddenstatic");
            $form->setType("exo_facul_8", "hiddenstatic");
            $form->setType("exo_facul_9", "hiddenstatic");
            $form->setType("exo_ta_1", "hiddenstatic");
            $form->setType("exo_ta_2", "hiddenstatic");
            $form->setType("exo_ta_3", "hiddenstatic");
            $form->setType("exo_ta_4", "hiddenstatic");
            $form->setType("exo_ta_5", "hiddenstatic");
            $form->setType("exo_ta_6", "hiddenstatic");
            $form->setType("exo_ta_7", "hiddenstatic");
            $form->setType("exo_ta_8", "hiddenstatic");
            $form->setType("exo_ta_9", "hiddenstatic");
            $form->setType("exo_rap_1", "hiddenstatic");
            $form->setType("exo_rap_2", "hiddenstatic");
            $form->setType("exo_rap_3", "hiddenstatic");
            $form->setType("exo_rap_4", "hiddenstatic");
            $form->setType("exo_rap_5", "hiddenstatic");
            $form->setType("exo_rap_6", "hiddenstatic");
            $form->setType("exo_rap_7", "hiddenstatic");
            $form->setType("exo_rap_8", "hiddenstatic");
            $form->setType("mtn_exo_ta_part_commu", "hiddenstatic");
            $form->setType("mtn_exo_ta_part_depart", "hiddenstatic");
            $form->setType("mtn_exo_ta_part_reg", "hiddenstatic");
            $form->setType("mtn_exo_rap", "hiddenstatic");
            $form->setType("dpc_type", "hiddenstatic");
            $form->setType("dpc_desc_actv_ex", "hiddenstatic");
            $form->setType("dpc_desc_ca", "hiddenstatic");
            $form->setType("dpc_desc_aut_prec", "hiddenstatic");
            $form->setType("dpc_desig_comm_arti", "hiddenstatic");
            $form->setType("dpc_desig_loc_hab", "hiddenstatic");
            $form->setType("dpc_desig_loc_ann", "hiddenstatic");
            $form->setType("dpc_desig_loc_ann_prec", "hiddenstatic");
            $form->setType("dpc_bail_comm_date", "hiddenstatic");
            $form->setType("dpc_bail_comm_loyer", "hiddenstatic");
            $form->setType("dpc_actv_acqu", "hiddenstatic");
            $form->setType("dpc_nb_sala_di", "hiddenstatic");
            $form->setType("dpc_nb_sala_dd", "hiddenstatic");
            $form->setType("dpc_nb_sala_tc", "hiddenstatic");
            $form->setType("dpc_nb_sala_tp", "hiddenstatic");
            $form->setType("dpc_moda_cess_vente_am", "hiddenstatic");
            $form->setType("dpc_moda_cess_adj", "hiddenstatic");
            $form->setType("dpc_moda_cess_prix", "hiddenstatic");
            $form->setType("dpc_moda_cess_adj_date", "hiddenstatic");
            $form->setType("dpc_moda_cess_adj_prec", "hiddenstatic");
            $form->setType("dpc_moda_cess_paie_comp", "hiddenstatic");
            $form->setType("dpc_moda_cess_paie_terme", "hiddenstatic");
            $form->setType("dpc_moda_cess_paie_terme_prec", "hiddenstatic");
            $form->setType("dpc_moda_cess_paie_nat", "hiddenstatic");
            $form->setType("dpc_moda_cess_paie_nat_desig_alien", "hiddenstatic");
            $form->setType("dpc_moda_cess_paie_nat_desig_alien_prec", "hiddenstatic");
            $form->setType("dpc_moda_cess_paie_nat_eval", "hiddenstatic");
            $form->setType("dpc_moda_cess_paie_nat_eval_prec", "hiddenstatic");
            $form->setType("dpc_moda_cess_paie_aut", "hiddenstatic");
            $form->setType("dpc_moda_cess_paie_aut_prec", "hiddenstatic");
            $form->setType("dpc_ss_signe_demande_acqu", "hiddenstatic");
            $form->setType("dpc_ss_signe_recher_trouv_acqu", "hiddenstatic");
            $form->setType("dpc_notif_adr_prop", "hiddenstatic");
            $form->setType("dpc_notif_adr_manda", "hiddenstatic");
            $form->setType("dpc_obs", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("donnees_techniques", "static");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("lot", "selectstatic");
            $form->setType("am_lotiss", "checkboxstatic");
            $form->setType("am_autre_div", "checkboxstatic");
            $form->setType("am_camping", "checkboxstatic");
            $form->setType("am_caravane", "checkboxstatic");
            $form->setType("am_carav_duree", "static");
            $form->setType("am_statio", "checkboxstatic");
            $form->setType("am_statio_cont", "static");
            $form->setType("am_affou_exhau", "checkboxstatic");
            $form->setType("am_affou_exhau_sup", "static");
            $form->setType("am_affou_prof", "static");
            $form->setType("am_exhau_haut", "static");
            $form->setType("am_coupe_abat", "checkboxstatic");
            $form->setType("am_prot_plu", "checkboxstatic");
            $form->setType("am_prot_muni", "checkboxstatic");
            $form->setType("am_mobil_voyage", "checkboxstatic");
            $form->setType("am_aire_voyage", "checkboxstatic");
            $form->setType("am_rememb_afu", "checkboxstatic");
            $form->setType("am_parc_resid_loi", "checkboxstatic");
            $form->setType("am_sport_moto", "checkboxstatic");
            $form->setType("am_sport_attrac", "checkboxstatic");
            $form->setType("am_sport_golf", "checkboxstatic");
            $form->setType("am_mob_art", "checkboxstatic");
            $form->setType("am_modif_voie_esp", "checkboxstatic");
            $form->setType("am_plant_voie_esp", "checkboxstatic");
            $form->setType("am_chem_ouv_esp", "checkboxstatic");
            $form->setType("am_agri_peche", "checkboxstatic");
            $form->setType("am_crea_voie", "checkboxstatic");
            $form->setType("am_modif_voie_exist", "checkboxstatic");
            $form->setType("am_crea_esp_sauv", "checkboxstatic");
            $form->setType("am_crea_esp_class", "checkboxstatic");
            $form->setType("am_projet_desc", "textareastatic");
            $form->setType("am_terr_surf", "static");
            $form->setType("am_tranche_desc", "textareastatic");
            $form->setType("am_lot_max_nb", "static");
            $form->setType("am_lot_max_shon", "static");
            $form->setType("am_lot_cstr_cos", "checkboxstatic");
            $form->setType("am_lot_cstr_plan", "checkboxstatic");
            $form->setType("am_lot_cstr_vente", "checkboxstatic");
            $form->setType("am_lot_fin_diff", "checkboxstatic");
            $form->setType("am_lot_consign", "checkboxstatic");
            $form->setType("am_lot_gar_achev", "checkboxstatic");
            $form->setType("am_lot_vente_ant", "checkboxstatic");
            $form->setType("am_empl_nb", "static");
            $form->setType("am_tente_nb", "static");
            $form->setType("am_carav_nb", "static");
            $form->setType("am_mobil_nb", "static");
            $form->setType("am_pers_nb", "static");
            $form->setType("am_empl_hll_nb", "static");
            $form->setType("am_hll_shon", "static");
            $form->setType("am_periode_exploit", "textareastatic");
            $form->setType("am_exist_agrand", "checkboxstatic");
            $form->setType("am_exist_date", "datestatic");
            $form->setType("am_exist_num", "static");
            $form->setType("am_exist_nb_avant", "static");
            $form->setType("am_exist_nb_apres", "static");
            $form->setType("am_coupe_bois", "checkboxstatic");
            $form->setType("am_coupe_parc", "checkboxstatic");
            $form->setType("am_coupe_align", "checkboxstatic");
            $form->setType("am_coupe_ess", "static");
            $form->setType("am_coupe_age", "static");
            $form->setType("am_coupe_dens", "static");
            $form->setType("am_coupe_qual", "static");
            $form->setType("am_coupe_trait", "static");
            $form->setType("am_coupe_autr", "static");
            $form->setType("co_archi_recours", "checkboxstatic");
            $form->setType("co_cstr_nouv", "checkboxstatic");
            $form->setType("co_cstr_exist", "checkboxstatic");
            $form->setType("co_cloture", "checkboxstatic");
            $form->setType("co_elec_tension", "static");
            $form->setType("co_div_terr", "checkboxstatic");
            $form->setType("co_projet_desc", "textareastatic");
            $form->setType("co_anx_pisc", "checkboxstatic");
            $form->setType("co_anx_gara", "checkboxstatic");
            $form->setType("co_anx_veran", "checkboxstatic");
            $form->setType("co_anx_abri", "checkboxstatic");
            $form->setType("co_anx_autr", "checkboxstatic");
            $form->setType("co_anx_autr_desc", "textareastatic");
            $form->setType("co_tot_log_nb", "static");
            $form->setType("co_tot_ind_nb", "static");
            $form->setType("co_tot_coll_nb", "static");
            $form->setType("co_mais_piece_nb", "static");
            $form->setType("co_mais_niv_nb", "static");
            $form->setType("co_fin_lls_nb", "static");
            $form->setType("co_fin_aa_nb", "static");
            $form->setType("co_fin_ptz_nb", "static");
            $form->setType("co_fin_autr_nb", "static");
            $form->setType("co_fin_autr_desc", "textareastatic");
            $form->setType("co_mais_contrat_ind", "checkboxstatic");
            $form->setType("co_uti_pers", "checkboxstatic");
            $form->setType("co_uti_vente", "checkboxstatic");
            $form->setType("co_uti_loc", "checkboxstatic");
            $form->setType("co_uti_princ", "checkboxstatic");
            $form->setType("co_uti_secon", "checkboxstatic");
            $form->setType("co_resid_agees", "checkboxstatic");
            $form->setType("co_resid_etud", "checkboxstatic");
            $form->setType("co_resid_tourism", "checkboxstatic");
            $form->setType("co_resid_hot_soc", "checkboxstatic");
            $form->setType("co_resid_soc", "checkboxstatic");
            $form->setType("co_resid_hand", "checkboxstatic");
            $form->setType("co_resid_autr", "checkboxstatic");
            $form->setType("co_resid_autr_desc", "textareastatic");
            $form->setType("co_foyer_chamb_nb", "static");
            $form->setType("co_log_1p_nb", "static");
            $form->setType("co_log_2p_nb", "static");
            $form->setType("co_log_3p_nb", "static");
            $form->setType("co_log_4p_nb", "static");
            $form->setType("co_log_5p_nb", "static");
            $form->setType("co_log_6p_nb", "static");
            $form->setType("co_bat_niv_nb", "static");
            $form->setType("co_trx_exten", "checkboxstatic");
            $form->setType("co_trx_surelev", "checkboxstatic");
            $form->setType("co_trx_nivsup", "checkboxstatic");
            $form->setType("co_demont_periode", "textareastatic");
            $form->setType("co_sp_transport", "checkboxstatic");
            $form->setType("co_sp_enseign", "checkboxstatic");
            $form->setType("co_sp_act_soc", "checkboxstatic");
            $form->setType("co_sp_ouvr_spe", "checkboxstatic");
            $form->setType("co_sp_sante", "checkboxstatic");
            $form->setType("co_sp_culture", "checkboxstatic");
            $form->setType("co_statio_avt_nb", "static");
            $form->setType("co_statio_apr_nb", "static");
            $form->setType("co_statio_adr", "textareastatic");
            $form->setType("co_statio_place_nb", "static");
            $form->setType("co_statio_tot_surf", "static");
            $form->setType("co_statio_tot_shob", "static");
            $form->setType("co_statio_comm_cin_surf", "static");
            $form->setType("su_avt_shon1", "static");
            $form->setType("su_avt_shon2", "static");
            $form->setType("su_avt_shon3", "static");
            $form->setType("su_avt_shon4", "static");
            $form->setType("su_avt_shon5", "static");
            $form->setType("su_avt_shon6", "static");
            $form->setType("su_avt_shon7", "static");
            $form->setType("su_avt_shon8", "static");
            $form->setType("su_avt_shon9", "static");
            $form->setType("su_cstr_shon1", "static");
            $form->setType("su_cstr_shon2", "static");
            $form->setType("su_cstr_shon3", "static");
            $form->setType("su_cstr_shon4", "static");
            $form->setType("su_cstr_shon5", "static");
            $form->setType("su_cstr_shon6", "static");
            $form->setType("su_cstr_shon7", "static");
            $form->setType("su_cstr_shon8", "static");
            $form->setType("su_cstr_shon9", "static");
            $form->setType("su_trsf_shon1", "static");
            $form->setType("su_trsf_shon2", "static");
            $form->setType("su_trsf_shon3", "static");
            $form->setType("su_trsf_shon4", "static");
            $form->setType("su_trsf_shon5", "static");
            $form->setType("su_trsf_shon6", "static");
            $form->setType("su_trsf_shon7", "static");
            $form->setType("su_trsf_shon8", "static");
            $form->setType("su_trsf_shon9", "static");
            $form->setType("su_chge_shon1", "static");
            $form->setType("su_chge_shon2", "static");
            $form->setType("su_chge_shon3", "static");
            $form->setType("su_chge_shon4", "static");
            $form->setType("su_chge_shon5", "static");
            $form->setType("su_chge_shon6", "static");
            $form->setType("su_chge_shon7", "static");
            $form->setType("su_chge_shon8", "static");
            $form->setType("su_chge_shon9", "static");
            $form->setType("su_demo_shon1", "static");
            $form->setType("su_demo_shon2", "static");
            $form->setType("su_demo_shon3", "static");
            $form->setType("su_demo_shon4", "static");
            $form->setType("su_demo_shon5", "static");
            $form->setType("su_demo_shon6", "static");
            $form->setType("su_demo_shon7", "static");
            $form->setType("su_demo_shon8", "static");
            $form->setType("su_demo_shon9", "static");
            $form->setType("su_sup_shon1", "static");
            $form->setType("su_sup_shon2", "static");
            $form->setType("su_sup_shon3", "static");
            $form->setType("su_sup_shon4", "static");
            $form->setType("su_sup_shon5", "static");
            $form->setType("su_sup_shon6", "static");
            $form->setType("su_sup_shon7", "static");
            $form->setType("su_sup_shon8", "static");
            $form->setType("su_sup_shon9", "static");
            $form->setType("su_tot_shon1", "static");
            $form->setType("su_tot_shon2", "static");
            $form->setType("su_tot_shon3", "static");
            $form->setType("su_tot_shon4", "static");
            $form->setType("su_tot_shon5", "static");
            $form->setType("su_tot_shon6", "static");
            $form->setType("su_tot_shon7", "static");
            $form->setType("su_tot_shon8", "static");
            $form->setType("su_tot_shon9", "static");
            $form->setType("su_avt_shon_tot", "static");
            $form->setType("su_cstr_shon_tot", "static");
            $form->setType("su_trsf_shon_tot", "static");
            $form->setType("su_chge_shon_tot", "static");
            $form->setType("su_demo_shon_tot", "static");
            $form->setType("su_sup_shon_tot", "static");
            $form->setType("su_tot_shon_tot", "static");
            $form->setType("dm_constr_dates", "textareastatic");
            $form->setType("dm_total", "checkboxstatic");
            $form->setType("dm_partiel", "checkboxstatic");
            $form->setType("dm_projet_desc", "textareastatic");
            $form->setType("dm_tot_log_nb", "static");
            $form->setType("tax_surf_tot", "static");
            $form->setType("tax_surf", "static");
            $form->setType("tax_surf_suppr_mod", "static");
            $form->setType("tax_su_princ_log_nb1", "static");
            $form->setType("tax_su_princ_log_nb2", "static");
            $form->setType("tax_su_princ_log_nb3", "static");
            $form->setType("tax_su_princ_log_nb4", "static");
            $form->setType("tax_su_princ_log_nb_tot1", "static");
            $form->setType("tax_su_princ_log_nb_tot2", "static");
            $form->setType("tax_su_princ_log_nb_tot3", "static");
            $form->setType("tax_su_princ_log_nb_tot4", "static");
            $form->setType("tax_su_princ_surf1", "static");
            $form->setType("tax_su_princ_surf2", "static");
            $form->setType("tax_su_princ_surf3", "static");
            $form->setType("tax_su_princ_surf4", "static");
            $form->setType("tax_su_princ_surf_sup1", "static");
            $form->setType("tax_su_princ_surf_sup2", "static");
            $form->setType("tax_su_princ_surf_sup3", "static");
            $form->setType("tax_su_princ_surf_sup4", "static");
            $form->setType("tax_su_heber_log_nb1", "static");
            $form->setType("tax_su_heber_log_nb2", "static");
            $form->setType("tax_su_heber_log_nb3", "static");
            $form->setType("tax_su_heber_log_nb_tot1", "static");
            $form->setType("tax_su_heber_log_nb_tot2", "static");
            $form->setType("tax_su_heber_log_nb_tot3", "static");
            $form->setType("tax_su_heber_surf1", "static");
            $form->setType("tax_su_heber_surf2", "static");
            $form->setType("tax_su_heber_surf3", "static");
            $form->setType("tax_su_heber_surf_sup1", "static");
            $form->setType("tax_su_heber_surf_sup2", "static");
            $form->setType("tax_su_heber_surf_sup3", "static");
            $form->setType("tax_su_secon_log_nb", "static");
            $form->setType("tax_su_tot_log_nb", "static");
            $form->setType("tax_su_secon_log_nb_tot", "static");
            $form->setType("tax_su_tot_log_nb_tot", "static");
            $form->setType("tax_su_secon_surf", "static");
            $form->setType("tax_su_tot_surf", "static");
            $form->setType("tax_su_secon_surf_sup", "static");
            $form->setType("tax_su_tot_surf_sup", "static");
            $form->setType("tax_ext_pret", "checkboxstatic");
            $form->setType("tax_ext_desc", "textareastatic");
            $form->setType("tax_surf_tax_exist_cons", "static");
            $form->setType("tax_log_exist_nb", "static");
            $form->setType("tax_am_statio_ext", "static");
            $form->setType("tax_sup_bass_pisc", "static");
            $form->setType("tax_empl_ten_carav_mobil_nb", "static");
            $form->setType("tax_empl_hll_nb", "static");
            $form->setType("tax_eol_haut_nb", "static");
            $form->setType("tax_pann_volt_sup", "static");
            $form->setType("tax_am_statio_ext_sup", "static");
            $form->setType("tax_sup_bass_pisc_sup", "static");
            $form->setType("tax_empl_ten_carav_mobil_nb_sup", "static");
            $form->setType("tax_empl_hll_nb_sup", "static");
            $form->setType("tax_eol_haut_nb_sup", "static");
            $form->setType("tax_pann_volt_sup_sup", "static");
            $form->setType("tax_trx_presc_ppr", "checkboxstatic");
            $form->setType("tax_monu_hist", "checkboxstatic");
            $form->setType("tax_comm_nb", "static");
            $form->setType("tax_su_non_habit_surf1", "static");
            $form->setType("tax_su_non_habit_surf2", "static");
            $form->setType("tax_su_non_habit_surf3", "static");
            $form->setType("tax_su_non_habit_surf4", "static");
            $form->setType("tax_su_non_habit_surf5", "static");
            $form->setType("tax_su_non_habit_surf6", "static");
            $form->setType("tax_su_non_habit_surf7", "static");
            $form->setType("tax_su_non_habit_surf_sup1", "static");
            $form->setType("tax_su_non_habit_surf_sup2", "static");
            $form->setType("tax_su_non_habit_surf_sup3", "static");
            $form->setType("tax_su_non_habit_surf_sup4", "static");
            $form->setType("tax_su_non_habit_surf_sup5", "static");
            $form->setType("tax_su_non_habit_surf_sup6", "static");
            $form->setType("tax_su_non_habit_surf_sup7", "static");
            $form->setType("vsd_surf_planch_smd", "checkboxstatic");
            $form->setType("vsd_unit_fonc_sup", "static");
            $form->setType("vsd_unit_fonc_constr_sup", "static");
            $form->setType("vsd_val_terr", "static");
            $form->setType("vsd_const_sxist_non_dem_surf", "static");
            $form->setType("vsd_rescr_fisc", "datestatic");
            $form->setType("pld_val_terr", "static");
            $form->setType("pld_const_exist_dem", "checkboxstatic");
            $form->setType("pld_const_exist_dem_surf", "static");
            $form->setType("code_cnil", "checkboxstatic");
            $form->setType("terr_juri_titul", "static");
            $form->setType("terr_juri_lot", "static");
            $form->setType("terr_juri_zac", "static");
            $form->setType("terr_juri_afu", "static");
            $form->setType("terr_juri_pup", "static");
            $form->setType("terr_juri_oin", "static");
            $form->setType("terr_juri_desc", "textareastatic");
            $form->setType("terr_div_surf_etab", "static");
            $form->setType("terr_div_surf_av_div", "static");
            $form->setType("doc_date", "datestatic");
            $form->setType("doc_tot_trav", "checkboxstatic");
            $form->setType("doc_tranche_trav", "checkboxstatic");
            $form->setType("doc_tranche_trav_desc", "textareastatic");
            $form->setType("doc_surf", "static");
            $form->setType("doc_nb_log", "static");
            $form->setType("doc_nb_log_indiv", "static");
            $form->setType("doc_nb_log_coll", "static");
            $form->setType("doc_nb_log_lls", "static");
            $form->setType("doc_nb_log_aa", "static");
            $form->setType("doc_nb_log_ptz", "static");
            $form->setType("doc_nb_log_autre", "static");
            $form->setType("daact_date", "datestatic");
            $form->setType("daact_date_chgmt_dest", "datestatic");
            $form->setType("daact_tot_trav", "checkboxstatic");
            $form->setType("daact_tranche_trav", "checkboxstatic");
            $form->setType("daact_tranche_trav_desc", "textareastatic");
            $form->setType("daact_surf", "static");
            $form->setType("daact_nb_log", "static");
            $form->setType("daact_nb_log_indiv", "static");
            $form->setType("daact_nb_log_coll", "static");
            $form->setType("daact_nb_log_lls", "static");
            $form->setType("daact_nb_log_aa", "static");
            $form->setType("daact_nb_log_ptz", "static");
            $form->setType("daact_nb_log_autre", "static");
            $form->setType("dossier_autorisation", "selectstatic");
            $form->setType("am_div_mun", "checkboxstatic");
            $form->setType("co_perf_energ", "static");
            $form->setType("architecte", "selectstatic");
            $form->setType("co_statio_avt_shob", "static");
            $form->setType("co_statio_apr_shob", "static");
            $form->setType("co_statio_avt_surf", "static");
            $form->setType("co_statio_apr_surf", "static");
            $form->setType("co_trx_amgt", "static");
            $form->setType("co_modif_aspect", "static");
            $form->setType("co_modif_struct", "static");
            $form->setType("co_ouvr_elec", "checkboxstatic");
            $form->setType("co_ouvr_infra", "checkboxstatic");
            $form->setType("co_trx_imm", "static");
            $form->setType("co_cstr_shob", "static");
            $form->setType("am_voyage_deb", "static");
            $form->setType("am_voyage_fin", "static");
            $form->setType("am_modif_amgt", "static");
            $form->setType("am_lot_max_shob", "static");
            $form->setType("mod_desc", "static");
            $form->setType("tr_total", "static");
            $form->setType("tr_partiel", "static");
            $form->setType("tr_desc", "static");
            $form->setType("avap_co_elt_pro", "checkboxstatic");
            $form->setType("avap_nouv_haut_surf", "checkboxstatic");
            $form->setType("avap_co_clot", "static");
            $form->setType("avap_aut_coup_aba_arb", "static");
            $form->setType("avap_ouv_infra", "static");
            $form->setType("avap_aut_inst_mob", "static");
            $form->setType("avap_aut_plant", "static");
            $form->setType("avap_aut_auv_elec", "static");
            $form->setType("tax_dest_loc_tr", "static");
            $form->setType("ope_proj_desc", "textareastatic");
            $form->setType("tax_surf_tot_cstr", "static");
            $form->setType("cerfa", "static");
            $form->setType("tax_surf_loc_stat", "static");
            $form->setType("tax_su_princ_surf_stat1", "static");
            $form->setType("tax_su_princ_surf_stat2", "static");
            $form->setType("tax_su_princ_surf_stat3", "static");
            $form->setType("tax_su_princ_surf_stat4", "static");
            $form->setType("tax_su_secon_surf_stat", "static");
            $form->setType("tax_su_heber_surf_stat1", "static");
            $form->setType("tax_su_heber_surf_stat2", "static");
            $form->setType("tax_su_heber_surf_stat3", "static");
            $form->setType("tax_su_tot_surf_stat", "static");
            $form->setType("tax_su_non_habit_surf_stat1", "static");
            $form->setType("tax_su_non_habit_surf_stat2", "static");
            $form->setType("tax_su_non_habit_surf_stat3", "static");
            $form->setType("tax_su_non_habit_surf_stat4", "static");
            $form->setType("tax_su_non_habit_surf_stat5", "static");
            $form->setType("tax_su_non_habit_surf_stat6", "static");
            $form->setType("tax_su_non_habit_surf_stat7", "static");
            $form->setType("tax_su_parc_statio_expl_comm_surf", "static");
            $form->setType("tax_log_ap_trvx_nb", "static");
            $form->setType("tax_am_statio_ext_cr", "static");
            $form->setType("tax_sup_bass_pisc_cr", "static");
            $form->setType("tax_empl_ten_carav_mobil_nb_cr", "static");
            $form->setType("tax_empl_hll_nb_cr", "static");
            $form->setType("tax_eol_haut_nb_cr", "static");
            $form->setType("tax_pann_volt_sup_cr", "static");
            $form->setType("tax_surf_loc_arch", "static");
            $form->setType("tax_surf_pisc_arch", "static");
            $form->setType("tax_am_statio_ext_arch", "static");
            $form->setType("tax_empl_ten_carav_mobil_nb_arch", "static");
            $form->setType("tax_empl_hll_nb_arch", "static");
            $form->setType("tax_eol_haut_nb_arch", "static");
            $form->setType("ope_proj_div_co", "checkboxstatic");
            $form->setType("ope_proj_div_contr", "checkboxstatic");
            $form->setType("tax_desc", "textareastatic");
            $form->setType("erp_cstr_neuve", "checkboxstatic");
            $form->setType("erp_trvx_acc", "checkboxstatic");
            $form->setType("erp_extension", "checkboxstatic");
            $form->setType("erp_rehab", "checkboxstatic");
            $form->setType("erp_trvx_am", "checkboxstatic");
            $form->setType("erp_vol_nouv_exist", "checkboxstatic");
            $form->setType("erp_loc_eff1", "static");
            $form->setType("erp_loc_eff2", "static");
            $form->setType("erp_loc_eff3", "static");
            $form->setType("erp_loc_eff4", "static");
            $form->setType("erp_loc_eff5", "static");
            $form->setType("erp_loc_eff_tot", "static");
            $form->setType("erp_public_eff1", "static");
            $form->setType("erp_public_eff2", "static");
            $form->setType("erp_public_eff3", "static");
            $form->setType("erp_public_eff4", "static");
            $form->setType("erp_public_eff5", "static");
            $form->setType("erp_public_eff_tot", "static");
            $form->setType("erp_perso_eff1", "static");
            $form->setType("erp_perso_eff2", "static");
            $form->setType("erp_perso_eff3", "static");
            $form->setType("erp_perso_eff4", "static");
            $form->setType("erp_perso_eff5", "static");
            $form->setType("erp_perso_eff_tot", "static");
            $form->setType("erp_tot_eff1", "static");
            $form->setType("erp_tot_eff2", "static");
            $form->setType("erp_tot_eff3", "static");
            $form->setType("erp_tot_eff4", "static");
            $form->setType("erp_tot_eff5", "static");
            $form->setType("erp_tot_eff_tot", "static");
            $form->setType("erp_class_cat", "static");
            $form->setType("erp_class_type", "static");
            $form->setType("tax_surf_abr_jard_pig_colom", "static");
            $form->setType("tax_su_non_habit_abr_jard_pig_colom", "static");
            $form->setType("dia_imm_non_bati", "checkboxstatic");
            $form->setType("dia_imm_bati_terr_propr", "checkboxstatic");
            $form->setType("dia_imm_bati_terr_autr", "checkboxstatic");
            $form->setType("dia_imm_bati_terr_autr_desc", "textareastatic");
            $form->setType("dia_bat_copro", "checkboxstatic");
            $form->setType("dia_bat_copro_desc", "textareastatic");
            $form->setType("dia_lot_numero", "textareastatic");
            $form->setType("dia_lot_bat", "textareastatic");
            $form->setType("dia_lot_etage", "textareastatic");
            $form->setType("dia_lot_quote_part", "textareastatic");
            $form->setType("dia_us_hab", "checkboxstatic");
            $form->setType("dia_us_pro", "checkboxstatic");
            $form->setType("dia_us_mixte", "checkboxstatic");
            $form->setType("dia_us_comm", "checkboxstatic");
            $form->setType("dia_us_agr", "checkboxstatic");
            $form->setType("dia_us_autre", "checkboxstatic");
            $form->setType("dia_us_autre_prec", "textareastatic");
            $form->setType("dia_occ_prop", "checkboxstatic");
            $form->setType("dia_occ_loc", "checkboxstatic");
            $form->setType("dia_occ_sans_occ", "checkboxstatic");
            $form->setType("dia_occ_autre", "checkboxstatic");
            $form->setType("dia_occ_autre_prec", "textareastatic");
            $form->setType("dia_mod_cess_prix_vente", "static");
            $form->setType("dia_mod_cess_prix_vente_mob", "static");
            $form->setType("dia_mod_cess_prix_vente_cheptel", "static");
            $form->setType("dia_mod_cess_prix_vente_recol", "static");
            $form->setType("dia_mod_cess_prix_vente_autre", "static");
            $form->setType("dia_mod_cess_commi", "checkboxstatic");
            $form->setType("dia_mod_cess_commi_ttc", "static");
            $form->setType("dia_mod_cess_commi_ht", "static");
            $form->setType("dia_acquereur_nom_prenom", "static");
            $form->setType("dia_acquereur_adr_num_voie", "static");
            $form->setType("dia_acquereur_adr_ext", "static");
            $form->setType("dia_acquereur_adr_type_voie", "static");
            $form->setType("dia_acquereur_adr_nom_voie", "static");
            $form->setType("dia_acquereur_adr_lieu_dit_bp", "static");
            $form->setType("dia_acquereur_adr_cp", "static");
            $form->setType("dia_acquereur_adr_localite", "static");
            $form->setType("dia_observation", "textareastatic");
            $form->setType("su2_avt_shon1", "static");
            $form->setType("su2_avt_shon2", "static");
            $form->setType("su2_avt_shon3", "static");
            $form->setType("su2_avt_shon4", "static");
            $form->setType("su2_avt_shon5", "static");
            $form->setType("su2_avt_shon6", "static");
            $form->setType("su2_avt_shon7", "static");
            $form->setType("su2_avt_shon8", "static");
            $form->setType("su2_avt_shon9", "static");
            $form->setType("su2_avt_shon10", "static");
            $form->setType("su2_avt_shon11", "static");
            $form->setType("su2_avt_shon12", "static");
            $form->setType("su2_avt_shon13", "static");
            $form->setType("su2_avt_shon14", "static");
            $form->setType("su2_avt_shon15", "static");
            $form->setType("su2_avt_shon16", "static");
            $form->setType("su2_avt_shon17", "static");
            $form->setType("su2_avt_shon18", "static");
            $form->setType("su2_avt_shon19", "static");
            $form->setType("su2_avt_shon20", "static");
            $form->setType("su2_avt_shon_tot", "static");
            $form->setType("su2_cstr_shon1", "static");
            $form->setType("su2_cstr_shon2", "static");
            $form->setType("su2_cstr_shon3", "static");
            $form->setType("su2_cstr_shon4", "static");
            $form->setType("su2_cstr_shon5", "static");
            $form->setType("su2_cstr_shon6", "static");
            $form->setType("su2_cstr_shon7", "static");
            $form->setType("su2_cstr_shon8", "static");
            $form->setType("su2_cstr_shon9", "static");
            $form->setType("su2_cstr_shon10", "static");
            $form->setType("su2_cstr_shon11", "static");
            $form->setType("su2_cstr_shon12", "static");
            $form->setType("su2_cstr_shon13", "static");
            $form->setType("su2_cstr_shon14", "static");
            $form->setType("su2_cstr_shon15", "static");
            $form->setType("su2_cstr_shon16", "static");
            $form->setType("su2_cstr_shon17", "static");
            $form->setType("su2_cstr_shon18", "static");
            $form->setType("su2_cstr_shon19", "static");
            $form->setType("su2_cstr_shon20", "static");
            $form->setType("su2_cstr_shon_tot", "static");
            $form->setType("su2_chge_shon1", "static");
            $form->setType("su2_chge_shon2", "static");
            $form->setType("su2_chge_shon3", "static");
            $form->setType("su2_chge_shon4", "static");
            $form->setType("su2_chge_shon5", "static");
            $form->setType("su2_chge_shon6", "static");
            $form->setType("su2_chge_shon7", "static");
            $form->setType("su2_chge_shon8", "static");
            $form->setType("su2_chge_shon9", "static");
            $form->setType("su2_chge_shon10", "static");
            $form->setType("su2_chge_shon11", "static");
            $form->setType("su2_chge_shon12", "static");
            $form->setType("su2_chge_shon13", "static");
            $form->setType("su2_chge_shon14", "static");
            $form->setType("su2_chge_shon15", "static");
            $form->setType("su2_chge_shon16", "static");
            $form->setType("su2_chge_shon17", "static");
            $form->setType("su2_chge_shon18", "static");
            $form->setType("su2_chge_shon19", "static");
            $form->setType("su2_chge_shon20", "static");
            $form->setType("su2_chge_shon_tot", "static");
            $form->setType("su2_demo_shon1", "static");
            $form->setType("su2_demo_shon2", "static");
            $form->setType("su2_demo_shon3", "static");
            $form->setType("su2_demo_shon4", "static");
            $form->setType("su2_demo_shon5", "static");
            $form->setType("su2_demo_shon6", "static");
            $form->setType("su2_demo_shon7", "static");
            $form->setType("su2_demo_shon8", "static");
            $form->setType("su2_demo_shon9", "static");
            $form->setType("su2_demo_shon10", "static");
            $form->setType("su2_demo_shon11", "static");
            $form->setType("su2_demo_shon12", "static");
            $form->setType("su2_demo_shon13", "static");
            $form->setType("su2_demo_shon14", "static");
            $form->setType("su2_demo_shon15", "static");
            $form->setType("su2_demo_shon16", "static");
            $form->setType("su2_demo_shon17", "static");
            $form->setType("su2_demo_shon18", "static");
            $form->setType("su2_demo_shon19", "static");
            $form->setType("su2_demo_shon20", "static");
            $form->setType("su2_demo_shon_tot", "static");
            $form->setType("su2_sup_shon1", "static");
            $form->setType("su2_sup_shon2", "static");
            $form->setType("su2_sup_shon3", "static");
            $form->setType("su2_sup_shon4", "static");
            $form->setType("su2_sup_shon5", "static");
            $form->setType("su2_sup_shon6", "static");
            $form->setType("su2_sup_shon7", "static");
            $form->setType("su2_sup_shon8", "static");
            $form->setType("su2_sup_shon9", "static");
            $form->setType("su2_sup_shon10", "static");
            $form->setType("su2_sup_shon11", "static");
            $form->setType("su2_sup_shon12", "static");
            $form->setType("su2_sup_shon13", "static");
            $form->setType("su2_sup_shon14", "static");
            $form->setType("su2_sup_shon15", "static");
            $form->setType("su2_sup_shon16", "static");
            $form->setType("su2_sup_shon17", "static");
            $form->setType("su2_sup_shon18", "static");
            $form->setType("su2_sup_shon19", "static");
            $form->setType("su2_sup_shon20", "static");
            $form->setType("su2_sup_shon_tot", "static");
            $form->setType("su2_tot_shon1", "static");
            $form->setType("su2_tot_shon2", "static");
            $form->setType("su2_tot_shon3", "static");
            $form->setType("su2_tot_shon4", "static");
            $form->setType("su2_tot_shon5", "static");
            $form->setType("su2_tot_shon6", "static");
            $form->setType("su2_tot_shon7", "static");
            $form->setType("su2_tot_shon8", "static");
            $form->setType("su2_tot_shon9", "static");
            $form->setType("su2_tot_shon10", "static");
            $form->setType("su2_tot_shon11", "static");
            $form->setType("su2_tot_shon12", "static");
            $form->setType("su2_tot_shon13", "static");
            $form->setType("su2_tot_shon14", "static");
            $form->setType("su2_tot_shon15", "static");
            $form->setType("su2_tot_shon16", "static");
            $form->setType("su2_tot_shon17", "static");
            $form->setType("su2_tot_shon18", "static");
            $form->setType("su2_tot_shon19", "static");
            $form->setType("su2_tot_shon20", "static");
            $form->setType("su2_tot_shon_tot", "static");
            $form->setType("dia_occ_sol_su_terre", "textareastatic");
            $form->setType("dia_occ_sol_su_pres", "textareastatic");
            $form->setType("dia_occ_sol_su_verger", "textareastatic");
            $form->setType("dia_occ_sol_su_vigne", "textareastatic");
            $form->setType("dia_occ_sol_su_bois", "textareastatic");
            $form->setType("dia_occ_sol_su_lande", "textareastatic");
            $form->setType("dia_occ_sol_su_carriere", "textareastatic");
            $form->setType("dia_occ_sol_su_eau_cadastree", "textareastatic");
            $form->setType("dia_occ_sol_su_jardin", "textareastatic");
            $form->setType("dia_occ_sol_su_terr_batir", "textareastatic");
            $form->setType("dia_occ_sol_su_terr_agr", "textareastatic");
            $form->setType("dia_occ_sol_su_sol", "textareastatic");
            $form->setType("dia_bati_vend_tot", "checkboxstatic");
            $form->setType("dia_bati_vend_tot_txt", "textareastatic");
            $form->setType("dia_su_co_sol", "textareastatic");
            $form->setType("dia_su_util_hab", "textareastatic");
            $form->setType("dia_nb_niv", "textareastatic");
            $form->setType("dia_nb_appart", "textareastatic");
            $form->setType("dia_nb_autre_loc", "textareastatic");
            $form->setType("dia_vente_lot_volume", "checkboxstatic");
            $form->setType("dia_vente_lot_volume_txt", "textareastatic");
            $form->setType("dia_lot_nat_su", "textareastatic");
            $form->setType("dia_lot_bat_achv_plus_10", "checkboxstatic");
            $form->setType("dia_lot_bat_achv_moins_10", "checkboxstatic");
            $form->setType("dia_lot_regl_copro_publ_hypo_plus_10", "checkboxstatic");
            $form->setType("dia_lot_regl_copro_publ_hypo_moins_10", "checkboxstatic");
            $form->setType("dia_indivi_quote_part", "textareastatic");
            $form->setType("dia_design_societe", "textareastatic");
            $form->setType("dia_design_droit", "textareastatic");
            $form->setType("dia_droit_soc_nat", "textareastatic");
            $form->setType("dia_droit_soc_nb", "textareastatic");
            $form->setType("dia_droit_soc_num_part", "textareastatic");
            $form->setType("dia_droit_reel_perso_grevant_bien_oui", "checkboxstatic");
            $form->setType("dia_droit_reel_perso_grevant_bien_non", "checkboxstatic");
            $form->setType("dia_droit_reel_perso_nat", "textareastatic");
            $form->setType("dia_droit_reel_perso_viag", "textareastatic");
            $form->setType("dia_mod_cess_adr", "textareastatic");
            $form->setType("dia_mod_cess_sign_act_auth", "checkboxstatic");
            $form->setType("dia_mod_cess_terme", "checkboxstatic");
            $form->setType("dia_mod_cess_terme_prec", "textareastatic");
            $form->setType("dia_mod_cess_bene_acquereur", "checkboxstatic");
            $form->setType("dia_mod_cess_bene_vendeur", "checkboxstatic");
            $form->setType("dia_mod_cess_paie_nat", "checkboxstatic");
            $form->setType("dia_mod_cess_design_contr_alien", "textareastatic");
            $form->setType("dia_mod_cess_eval_contr", "textareastatic");
            $form->setType("dia_mod_cess_rente_viag", "checkboxstatic");
            $form->setType("dia_mod_cess_mnt_an", "textareastatic");
            $form->setType("dia_mod_cess_mnt_compt", "textareastatic");
            $form->setType("dia_mod_cess_bene_rente", "textareastatic");
            $form->setType("dia_mod_cess_droit_usa_hab", "checkboxstatic");
            $form->setType("dia_mod_cess_droit_usa_hab_prec", "textareastatic");
            $form->setType("dia_mod_cess_eval_usa_usufruit", "textareastatic");
            $form->setType("dia_mod_cess_vente_nue_prop", "checkboxstatic");
            $form->setType("dia_mod_cess_vente_nue_prop_prec", "textareastatic");
            $form->setType("dia_mod_cess_echange", "checkboxstatic");
            $form->setType("dia_mod_cess_design_bien_recus_ech", "textareastatic");
            $form->setType("dia_mod_cess_mnt_soulte", "textareastatic");
            $form->setType("dia_mod_cess_prop_contre_echan", "textareastatic");
            $form->setType("dia_mod_cess_apport_societe", "textareastatic");
            $form->setType("dia_mod_cess_bene", "textareastatic");
            $form->setType("dia_mod_cess_esti_bien", "textareastatic");
            $form->setType("dia_mod_cess_cess_terr_loc_co", "checkboxstatic");
            $form->setType("dia_mod_cess_esti_terr", "textareastatic");
            $form->setType("dia_mod_cess_esti_loc", "textareastatic");
            $form->setType("dia_mod_cess_esti_imm_loca", "checkboxstatic");
            $form->setType("dia_mod_cess_adju_vol", "checkboxstatic");
            $form->setType("dia_mod_cess_adju_obl", "checkboxstatic");
            $form->setType("dia_mod_cess_adju_fin_indivi", "checkboxstatic");
            $form->setType("dia_mod_cess_adju_date_lieu", "textareastatic");
            $form->setType("dia_mod_cess_mnt_mise_prix", "textareastatic");
            $form->setType("dia_prop_titu_prix_indique", "checkboxstatic");
            $form->setType("dia_prop_recherche_acqu_prix_indique", "checkboxstatic");
            $form->setType("dia_acquereur_prof", "textareastatic");
            $form->setType("dia_indic_compl_ope", "textareastatic");
            $form->setType("dia_vente_adju", "checkboxstatic");
            $form->setType("am_terr_res_demon", "checkboxstatic");
            $form->setType("am_air_terr_res_mob", "checkboxstatic");
            $form->setType("ctx_objet_recours", "selectstatic");
            $form->setType("ctx_reference_sagace", "static");
            $form->setType("ctx_nature_travaux_infra_om_html", "htmlstatic");
            $form->setType("ctx_synthese_nti", "textareastatic");
            $form->setType("ctx_article_non_resp_om_html", "htmlstatic");
            $form->setType("ctx_synthese_anr", "textareastatic");
            $form->setType("ctx_reference_parquet", "static");
            $form->setType("ctx_element_taxation", "static");
            $form->setType("ctx_infraction", "checkboxstatic");
            $form->setType("ctx_regularisable", "checkboxstatic");
            $form->setType("ctx_reference_courrier", "static");
            $form->setType("ctx_date_audience", "datestatic");
            $form->setType("ctx_date_ajournement", "datestatic");
            $form->setType("exo_facul_1", "checkboxstatic");
            $form->setType("exo_facul_2", "checkboxstatic");
            $form->setType("exo_facul_3", "checkboxstatic");
            $form->setType("exo_facul_4", "checkboxstatic");
            $form->setType("exo_facul_5", "checkboxstatic");
            $form->setType("exo_facul_6", "checkboxstatic");
            $form->setType("exo_facul_7", "checkboxstatic");
            $form->setType("exo_facul_8", "checkboxstatic");
            $form->setType("exo_facul_9", "checkboxstatic");
            $form->setType("exo_ta_1", "checkboxstatic");
            $form->setType("exo_ta_2", "checkboxstatic");
            $form->setType("exo_ta_3", "checkboxstatic");
            $form->setType("exo_ta_4", "checkboxstatic");
            $form->setType("exo_ta_5", "checkboxstatic");
            $form->setType("exo_ta_6", "checkboxstatic");
            $form->setType("exo_ta_7", "checkboxstatic");
            $form->setType("exo_ta_8", "checkboxstatic");
            $form->setType("exo_ta_9", "checkboxstatic");
            $form->setType("exo_rap_1", "checkboxstatic");
            $form->setType("exo_rap_2", "checkboxstatic");
            $form->setType("exo_rap_3", "checkboxstatic");
            $form->setType("exo_rap_4", "checkboxstatic");
            $form->setType("exo_rap_5", "checkboxstatic");
            $form->setType("exo_rap_6", "checkboxstatic");
            $form->setType("exo_rap_7", "checkboxstatic");
            $form->setType("exo_rap_8", "checkboxstatic");
            $form->setType("mtn_exo_ta_part_commu", "static");
            $form->setType("mtn_exo_ta_part_depart", "static");
            $form->setType("mtn_exo_ta_part_reg", "static");
            $form->setType("mtn_exo_rap", "static");
            $form->setType("dpc_type", "static");
            $form->setType("dpc_desc_actv_ex", "textareastatic");
            $form->setType("dpc_desc_ca", "textareastatic");
            $form->setType("dpc_desc_aut_prec", "textareastatic");
            $form->setType("dpc_desig_comm_arti", "checkboxstatic");
            $form->setType("dpc_desig_loc_hab", "checkboxstatic");
            $form->setType("dpc_desig_loc_ann", "checkboxstatic");
            $form->setType("dpc_desig_loc_ann_prec", "textareastatic");
            $form->setType("dpc_bail_comm_date", "datestatic");
            $form->setType("dpc_bail_comm_loyer", "textareastatic");
            $form->setType("dpc_actv_acqu", "textareastatic");
            $form->setType("dpc_nb_sala_di", "textareastatic");
            $form->setType("dpc_nb_sala_dd", "textareastatic");
            $form->setType("dpc_nb_sala_tc", "textareastatic");
            $form->setType("dpc_nb_sala_tp", "textareastatic");
            $form->setType("dpc_moda_cess_vente_am", "checkboxstatic");
            $form->setType("dpc_moda_cess_adj", "checkboxstatic");
            $form->setType("dpc_moda_cess_prix", "textareastatic");
            $form->setType("dpc_moda_cess_adj_date", "datestatic");
            $form->setType("dpc_moda_cess_adj_prec", "textareastatic");
            $form->setType("dpc_moda_cess_paie_comp", "checkboxstatic");
            $form->setType("dpc_moda_cess_paie_terme", "checkboxstatic");
            $form->setType("dpc_moda_cess_paie_terme_prec", "textareastatic");
            $form->setType("dpc_moda_cess_paie_nat", "checkboxstatic");
            $form->setType("dpc_moda_cess_paie_nat_desig_alien", "checkboxstatic");
            $form->setType("dpc_moda_cess_paie_nat_desig_alien_prec", "textareastatic");
            $form->setType("dpc_moda_cess_paie_nat_eval", "checkboxstatic");
            $form->setType("dpc_moda_cess_paie_nat_eval_prec", "textareastatic");
            $form->setType("dpc_moda_cess_paie_aut", "checkboxstatic");
            $form->setType("dpc_moda_cess_paie_aut_prec", "textareastatic");
            $form->setType("dpc_ss_signe_demande_acqu", "checkboxstatic");
            $form->setType("dpc_ss_signe_recher_trouv_acqu", "checkboxstatic");
            $form->setType("dpc_notif_adr_prop", "checkboxstatic");
            $form->setType("dpc_notif_adr_manda", "checkboxstatic");
            $form->setType("dpc_obs", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('donnees_techniques','VerifNum(this)');
        $form->setOnchange('lot','VerifNum(this)');
        $form->setOnchange('am_carav_duree','VerifNum(this)');
        $form->setOnchange('am_statio_cont','VerifNum(this)');
        $form->setOnchange('am_affou_exhau_sup','VerifFloat(this)');
        $form->setOnchange('am_affou_prof','VerifFloat(this)');
        $form->setOnchange('am_exhau_haut','VerifFloat(this)');
        $form->setOnchange('am_terr_surf','VerifFloat(this)');
        $form->setOnchange('am_lot_max_nb','VerifNum(this)');
        $form->setOnchange('am_lot_max_shon','VerifFloat(this)');
        $form->setOnchange('am_empl_nb','VerifNum(this)');
        $form->setOnchange('am_tente_nb','VerifNum(this)');
        $form->setOnchange('am_carav_nb','VerifNum(this)');
        $form->setOnchange('am_mobil_nb','VerifNum(this)');
        $form->setOnchange('am_pers_nb','VerifNum(this)');
        $form->setOnchange('am_empl_hll_nb','VerifNum(this)');
        $form->setOnchange('am_hll_shon','VerifFloat(this)');
        $form->setOnchange('am_exist_date','fdate(this)');
        $form->setOnchange('am_exist_nb_avant','VerifNum(this)');
        $form->setOnchange('am_exist_nb_apres','VerifNum(this)');
        $form->setOnchange('co_elec_tension','VerifFloat(this)');
        $form->setOnchange('co_tot_log_nb','VerifNum(this)');
        $form->setOnchange('co_tot_ind_nb','VerifNum(this)');
        $form->setOnchange('co_tot_coll_nb','VerifNum(this)');
        $form->setOnchange('co_mais_piece_nb','VerifNum(this)');
        $form->setOnchange('co_mais_niv_nb','VerifNum(this)');
        $form->setOnchange('co_fin_lls_nb','VerifNum(this)');
        $form->setOnchange('co_fin_aa_nb','VerifNum(this)');
        $form->setOnchange('co_fin_ptz_nb','VerifNum(this)');
        $form->setOnchange('co_fin_autr_nb','VerifNum(this)');
        $form->setOnchange('co_foyer_chamb_nb','VerifNum(this)');
        $form->setOnchange('co_log_1p_nb','VerifNum(this)');
        $form->setOnchange('co_log_2p_nb','VerifNum(this)');
        $form->setOnchange('co_log_3p_nb','VerifNum(this)');
        $form->setOnchange('co_log_4p_nb','VerifNum(this)');
        $form->setOnchange('co_log_5p_nb','VerifNum(this)');
        $form->setOnchange('co_log_6p_nb','VerifNum(this)');
        $form->setOnchange('co_bat_niv_nb','VerifNum(this)');
        $form->setOnchange('co_statio_avt_nb','VerifNum(this)');
        $form->setOnchange('co_statio_apr_nb','VerifNum(this)');
        $form->setOnchange('co_statio_place_nb','VerifNum(this)');
        $form->setOnchange('co_statio_tot_surf','VerifFloat(this)');
        $form->setOnchange('co_statio_tot_shob','VerifFloat(this)');
        $form->setOnchange('co_statio_comm_cin_surf','VerifFloat(this)');
        $form->setOnchange('su_avt_shon1','VerifFloat(this)');
        $form->setOnchange('su_avt_shon2','VerifFloat(this)');
        $form->setOnchange('su_avt_shon3','VerifFloat(this)');
        $form->setOnchange('su_avt_shon4','VerifFloat(this)');
        $form->setOnchange('su_avt_shon5','VerifFloat(this)');
        $form->setOnchange('su_avt_shon6','VerifFloat(this)');
        $form->setOnchange('su_avt_shon7','VerifFloat(this)');
        $form->setOnchange('su_avt_shon8','VerifFloat(this)');
        $form->setOnchange('su_avt_shon9','VerifFloat(this)');
        $form->setOnchange('su_cstr_shon1','VerifFloat(this)');
        $form->setOnchange('su_cstr_shon2','VerifFloat(this)');
        $form->setOnchange('su_cstr_shon3','VerifFloat(this)');
        $form->setOnchange('su_cstr_shon4','VerifFloat(this)');
        $form->setOnchange('su_cstr_shon5','VerifFloat(this)');
        $form->setOnchange('su_cstr_shon6','VerifFloat(this)');
        $form->setOnchange('su_cstr_shon7','VerifFloat(this)');
        $form->setOnchange('su_cstr_shon8','VerifFloat(this)');
        $form->setOnchange('su_cstr_shon9','VerifFloat(this)');
        $form->setOnchange('su_trsf_shon1','VerifFloat(this)');
        $form->setOnchange('su_trsf_shon2','VerifFloat(this)');
        $form->setOnchange('su_trsf_shon3','VerifFloat(this)');
        $form->setOnchange('su_trsf_shon4','VerifFloat(this)');
        $form->setOnchange('su_trsf_shon5','VerifFloat(this)');
        $form->setOnchange('su_trsf_shon6','VerifFloat(this)');
        $form->setOnchange('su_trsf_shon7','VerifFloat(this)');
        $form->setOnchange('su_trsf_shon8','VerifFloat(this)');
        $form->setOnchange('su_trsf_shon9','VerifFloat(this)');
        $form->setOnchange('su_chge_shon1','VerifFloat(this)');
        $form->setOnchange('su_chge_shon2','VerifFloat(this)');
        $form->setOnchange('su_chge_shon3','VerifFloat(this)');
        $form->setOnchange('su_chge_shon4','VerifFloat(this)');
        $form->setOnchange('su_chge_shon5','VerifFloat(this)');
        $form->setOnchange('su_chge_shon6','VerifFloat(this)');
        $form->setOnchange('su_chge_shon7','VerifFloat(this)');
        $form->setOnchange('su_chge_shon8','VerifFloat(this)');
        $form->setOnchange('su_chge_shon9','VerifFloat(this)');
        $form->setOnchange('su_demo_shon1','VerifFloat(this)');
        $form->setOnchange('su_demo_shon2','VerifFloat(this)');
        $form->setOnchange('su_demo_shon3','VerifFloat(this)');
        $form->setOnchange('su_demo_shon4','VerifFloat(this)');
        $form->setOnchange('su_demo_shon5','VerifFloat(this)');
        $form->setOnchange('su_demo_shon6','VerifFloat(this)');
        $form->setOnchange('su_demo_shon7','VerifFloat(this)');
        $form->setOnchange('su_demo_shon8','VerifFloat(this)');
        $form->setOnchange('su_demo_shon9','VerifFloat(this)');
        $form->setOnchange('su_sup_shon1','VerifFloat(this)');
        $form->setOnchange('su_sup_shon2','VerifFloat(this)');
        $form->setOnchange('su_sup_shon3','VerifFloat(this)');
        $form->setOnchange('su_sup_shon4','VerifFloat(this)');
        $form->setOnchange('su_sup_shon5','VerifFloat(this)');
        $form->setOnchange('su_sup_shon6','VerifFloat(this)');
        $form->setOnchange('su_sup_shon7','VerifFloat(this)');
        $form->setOnchange('su_sup_shon8','VerifFloat(this)');
        $form->setOnchange('su_sup_shon9','VerifFloat(this)');
        $form->setOnchange('su_tot_shon1','VerifFloat(this)');
        $form->setOnchange('su_tot_shon2','VerifFloat(this)');
        $form->setOnchange('su_tot_shon3','VerifFloat(this)');
        $form->setOnchange('su_tot_shon4','VerifFloat(this)');
        $form->setOnchange('su_tot_shon5','VerifFloat(this)');
        $form->setOnchange('su_tot_shon6','VerifFloat(this)');
        $form->setOnchange('su_tot_shon7','VerifFloat(this)');
        $form->setOnchange('su_tot_shon8','VerifFloat(this)');
        $form->setOnchange('su_tot_shon9','VerifFloat(this)');
        $form->setOnchange('su_avt_shon_tot','VerifFloat(this)');
        $form->setOnchange('su_cstr_shon_tot','VerifFloat(this)');
        $form->setOnchange('su_trsf_shon_tot','VerifFloat(this)');
        $form->setOnchange('su_chge_shon_tot','VerifFloat(this)');
        $form->setOnchange('su_demo_shon_tot','VerifFloat(this)');
        $form->setOnchange('su_sup_shon_tot','VerifFloat(this)');
        $form->setOnchange('su_tot_shon_tot','VerifFloat(this)');
        $form->setOnchange('dm_tot_log_nb','VerifNum(this)');
        $form->setOnchange('tax_surf_tot','VerifFloat(this)');
        $form->setOnchange('tax_surf','VerifFloat(this)');
        $form->setOnchange('tax_surf_suppr_mod','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_log_nb1','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_log_nb2','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_log_nb3','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_log_nb4','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_log_nb_tot1','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_log_nb_tot2','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_log_nb_tot3','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_log_nb_tot4','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf1','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf2','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf3','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf4','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf_sup1','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf_sup2','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf_sup3','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf_sup4','VerifFloat(this)');
        $form->setOnchange('tax_su_heber_log_nb1','VerifNum(this)');
        $form->setOnchange('tax_su_heber_log_nb2','VerifNum(this)');
        $form->setOnchange('tax_su_heber_log_nb3','VerifNum(this)');
        $form->setOnchange('tax_su_heber_log_nb_tot1','VerifNum(this)');
        $form->setOnchange('tax_su_heber_log_nb_tot2','VerifNum(this)');
        $form->setOnchange('tax_su_heber_log_nb_tot3','VerifNum(this)');
        $form->setOnchange('tax_su_heber_surf1','VerifFloat(this)');
        $form->setOnchange('tax_su_heber_surf2','VerifFloat(this)');
        $form->setOnchange('tax_su_heber_surf3','VerifFloat(this)');
        $form->setOnchange('tax_su_heber_surf_sup1','VerifFloat(this)');
        $form->setOnchange('tax_su_heber_surf_sup2','VerifFloat(this)');
        $form->setOnchange('tax_su_heber_surf_sup3','VerifFloat(this)');
        $form->setOnchange('tax_su_secon_log_nb','VerifNum(this)');
        $form->setOnchange('tax_su_tot_log_nb','VerifNum(this)');
        $form->setOnchange('tax_su_secon_log_nb_tot','VerifNum(this)');
        $form->setOnchange('tax_su_tot_log_nb_tot','VerifNum(this)');
        $form->setOnchange('tax_su_secon_surf','VerifFloat(this)');
        $form->setOnchange('tax_su_tot_surf','VerifFloat(this)');
        $form->setOnchange('tax_su_secon_surf_sup','VerifFloat(this)');
        $form->setOnchange('tax_su_tot_surf_sup','VerifFloat(this)');
        $form->setOnchange('tax_surf_tax_exist_cons','VerifFloat(this)');
        $form->setOnchange('tax_log_exist_nb','VerifNum(this)');
        $form->setOnchange('tax_am_statio_ext','VerifNum(this)');
        $form->setOnchange('tax_sup_bass_pisc','VerifFloat(this)');
        $form->setOnchange('tax_empl_ten_carav_mobil_nb','VerifNum(this)');
        $form->setOnchange('tax_empl_hll_nb','VerifNum(this)');
        $form->setOnchange('tax_eol_haut_nb','VerifNum(this)');
        $form->setOnchange('tax_pann_volt_sup','VerifFloat(this)');
        $form->setOnchange('tax_am_statio_ext_sup','VerifNum(this)');
        $form->setOnchange('tax_sup_bass_pisc_sup','VerifFloat(this)');
        $form->setOnchange('tax_empl_ten_carav_mobil_nb_sup','VerifNum(this)');
        $form->setOnchange('tax_empl_hll_nb_sup','VerifNum(this)');
        $form->setOnchange('tax_eol_haut_nb_sup','VerifNum(this)');
        $form->setOnchange('tax_pann_volt_sup_sup','VerifFloat(this)');
        $form->setOnchange('tax_comm_nb','VerifNum(this)');
        $form->setOnchange('tax_su_non_habit_surf1','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf2','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf3','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf4','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf5','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf6','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf7','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_sup1','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_sup2','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_sup3','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_sup4','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_sup5','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_sup6','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_sup7','VerifFloat(this)');
        $form->setOnchange('vsd_unit_fonc_sup','VerifFloat(this)');
        $form->setOnchange('vsd_unit_fonc_constr_sup','VerifFloat(this)');
        $form->setOnchange('vsd_val_terr','VerifFloat(this)');
        $form->setOnchange('vsd_const_sxist_non_dem_surf','VerifFloat(this)');
        $form->setOnchange('vsd_rescr_fisc','fdate(this)');
        $form->setOnchange('pld_val_terr','VerifFloat(this)');
        $form->setOnchange('pld_const_exist_dem_surf','VerifFloat(this)');
        $form->setOnchange('terr_div_surf_etab','VerifFloat(this)');
        $form->setOnchange('terr_div_surf_av_div','VerifFloat(this)');
        $form->setOnchange('doc_date','fdate(this)');
        $form->setOnchange('doc_surf','VerifFloat(this)');
        $form->setOnchange('doc_nb_log','VerifNum(this)');
        $form->setOnchange('doc_nb_log_indiv','VerifNum(this)');
        $form->setOnchange('doc_nb_log_coll','VerifNum(this)');
        $form->setOnchange('doc_nb_log_lls','VerifNum(this)');
        $form->setOnchange('doc_nb_log_aa','VerifNum(this)');
        $form->setOnchange('doc_nb_log_ptz','VerifNum(this)');
        $form->setOnchange('doc_nb_log_autre','VerifNum(this)');
        $form->setOnchange('daact_date','fdate(this)');
        $form->setOnchange('daact_date_chgmt_dest','fdate(this)');
        $form->setOnchange('daact_surf','VerifFloat(this)');
        $form->setOnchange('daact_nb_log','VerifNum(this)');
        $form->setOnchange('daact_nb_log_indiv','VerifNum(this)');
        $form->setOnchange('daact_nb_log_coll','VerifNum(this)');
        $form->setOnchange('daact_nb_log_lls','VerifNum(this)');
        $form->setOnchange('daact_nb_log_aa','VerifNum(this)');
        $form->setOnchange('daact_nb_log_ptz','VerifNum(this)');
        $form->setOnchange('daact_nb_log_autre','VerifNum(this)');
        $form->setOnchange('architecte','VerifNum(this)');
        $form->setOnchange('tax_surf_tot_cstr','VerifNum(this)');
        $form->setOnchange('cerfa','VerifNum(this)');
        $form->setOnchange('tax_surf_loc_stat','VerifNum(this)');
        $form->setOnchange('tax_su_princ_surf_stat1','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf_stat2','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf_stat3','VerifFloat(this)');
        $form->setOnchange('tax_su_princ_surf_stat4','VerifFloat(this)');
        $form->setOnchange('tax_su_secon_surf_stat','VerifFloat(this)');
        $form->setOnchange('tax_su_heber_surf_stat1','VerifFloat(this)');
        $form->setOnchange('tax_su_heber_surf_stat2','VerifFloat(this)');
        $form->setOnchange('tax_su_heber_surf_stat3','VerifFloat(this)');
        $form->setOnchange('tax_su_tot_surf_stat','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_stat1','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_stat2','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_stat3','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_stat4','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_stat5','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_stat6','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_surf_stat7','VerifFloat(this)');
        $form->setOnchange('tax_su_parc_statio_expl_comm_surf','VerifFloat(this)');
        $form->setOnchange('tax_log_ap_trvx_nb','VerifNum(this)');
        $form->setOnchange('tax_am_statio_ext_cr','VerifNum(this)');
        $form->setOnchange('tax_sup_bass_pisc_cr','VerifFloat(this)');
        $form->setOnchange('tax_empl_ten_carav_mobil_nb_cr','VerifNum(this)');
        $form->setOnchange('tax_empl_hll_nb_cr','VerifNum(this)');
        $form->setOnchange('tax_eol_haut_nb_cr','VerifNum(this)');
        $form->setOnchange('tax_pann_volt_sup_cr','VerifFloat(this)');
        $form->setOnchange('tax_surf_loc_arch','VerifFloat(this)');
        $form->setOnchange('tax_surf_pisc_arch','VerifFloat(this)');
        $form->setOnchange('tax_am_statio_ext_arch','VerifFloat(this)');
        $form->setOnchange('tax_empl_ten_carav_mobil_nb_arch','VerifFloat(this)');
        $form->setOnchange('tax_empl_hll_nb_arch','VerifFloat(this)');
        $form->setOnchange('tax_eol_haut_nb_arch','VerifNum(this)');
        $form->setOnchange('erp_loc_eff1','VerifFloat(this)');
        $form->setOnchange('erp_loc_eff2','VerifFloat(this)');
        $form->setOnchange('erp_loc_eff3','VerifFloat(this)');
        $form->setOnchange('erp_loc_eff4','VerifFloat(this)');
        $form->setOnchange('erp_loc_eff5','VerifFloat(this)');
        $form->setOnchange('erp_loc_eff_tot','VerifFloat(this)');
        $form->setOnchange('erp_public_eff1','VerifFloat(this)');
        $form->setOnchange('erp_public_eff2','VerifFloat(this)');
        $form->setOnchange('erp_public_eff3','VerifFloat(this)');
        $form->setOnchange('erp_public_eff4','VerifFloat(this)');
        $form->setOnchange('erp_public_eff5','VerifFloat(this)');
        $form->setOnchange('erp_public_eff_tot','VerifFloat(this)');
        $form->setOnchange('erp_perso_eff1','VerifFloat(this)');
        $form->setOnchange('erp_perso_eff2','VerifFloat(this)');
        $form->setOnchange('erp_perso_eff3','VerifFloat(this)');
        $form->setOnchange('erp_perso_eff4','VerifFloat(this)');
        $form->setOnchange('erp_perso_eff5','VerifFloat(this)');
        $form->setOnchange('erp_perso_eff_tot','VerifFloat(this)');
        $form->setOnchange('erp_tot_eff1','VerifFloat(this)');
        $form->setOnchange('erp_tot_eff2','VerifFloat(this)');
        $form->setOnchange('erp_tot_eff3','VerifFloat(this)');
        $form->setOnchange('erp_tot_eff4','VerifFloat(this)');
        $form->setOnchange('erp_tot_eff5','VerifFloat(this)');
        $form->setOnchange('erp_tot_eff_tot','VerifFloat(this)');
        $form->setOnchange('erp_class_cat','VerifNum(this)');
        $form->setOnchange('erp_class_type','VerifNum(this)');
        $form->setOnchange('tax_surf_abr_jard_pig_colom','VerifFloat(this)');
        $form->setOnchange('tax_su_non_habit_abr_jard_pig_colom','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon1','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon2','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon3','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon4','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon5','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon6','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon7','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon8','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon9','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon10','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon11','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon12','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon13','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon14','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon15','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon16','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon17','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon18','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon19','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon20','VerifFloat(this)');
        $form->setOnchange('su2_avt_shon_tot','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon1','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon2','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon3','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon4','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon5','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon6','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon7','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon8','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon9','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon10','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon11','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon12','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon13','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon14','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon15','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon16','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon17','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon18','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon19','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon20','VerifFloat(this)');
        $form->setOnchange('su2_cstr_shon_tot','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon1','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon2','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon3','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon4','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon5','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon6','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon7','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon8','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon9','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon10','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon11','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon12','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon13','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon14','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon15','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon16','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon17','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon18','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon19','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon20','VerifFloat(this)');
        $form->setOnchange('su2_chge_shon_tot','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon1','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon2','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon3','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon4','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon5','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon6','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon7','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon8','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon9','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon10','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon11','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon12','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon13','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon14','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon15','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon16','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon17','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon18','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon19','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon20','VerifFloat(this)');
        $form->setOnchange('su2_demo_shon_tot','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon1','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon2','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon3','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon4','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon5','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon6','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon7','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon8','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon9','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon10','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon11','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon12','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon13','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon14','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon15','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon16','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon17','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon18','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon19','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon20','VerifFloat(this)');
        $form->setOnchange('su2_sup_shon_tot','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon1','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon2','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon3','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon4','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon5','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon6','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon7','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon8','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon9','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon10','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon11','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon12','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon13','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon14','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon15','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon16','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon17','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon18','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon19','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon20','VerifFloat(this)');
        $form->setOnchange('su2_tot_shon_tot','VerifFloat(this)');
        $form->setOnchange('ctx_objet_recours','VerifNum(this)');
        $form->setOnchange('ctx_date_audience','fdate(this)');
        $form->setOnchange('ctx_date_ajournement','fdate(this)');
        $form->setOnchange('mtn_exo_ta_part_commu','VerifFloat(this)');
        $form->setOnchange('mtn_exo_ta_part_depart','VerifFloat(this)');
        $form->setOnchange('mtn_exo_ta_part_reg','VerifFloat(this)');
        $form->setOnchange('mtn_exo_rap','VerifFloat(this)');
        $form->setOnchange('dpc_bail_comm_date','fdate(this)');
        $form->setOnchange('dpc_moda_cess_adj_date','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("donnees_techniques", 11);
        $form->setTaille("dossier_instruction", 30);
        $form->setTaille("lot", 11);
        $form->setTaille("am_lotiss", 1);
        $form->setTaille("am_autre_div", 1);
        $form->setTaille("am_camping", 1);
        $form->setTaille("am_caravane", 1);
        $form->setTaille("am_carav_duree", 11);
        $form->setTaille("am_statio", 1);
        $form->setTaille("am_statio_cont", 11);
        $form->setTaille("am_affou_exhau", 1);
        $form->setTaille("am_affou_exhau_sup", 10);
        $form->setTaille("am_affou_prof", 10);
        $form->setTaille("am_exhau_haut", 10);
        $form->setTaille("am_coupe_abat", 1);
        $form->setTaille("am_prot_plu", 1);
        $form->setTaille("am_prot_muni", 1);
        $form->setTaille("am_mobil_voyage", 1);
        $form->setTaille("am_aire_voyage", 1);
        $form->setTaille("am_rememb_afu", 1);
        $form->setTaille("am_parc_resid_loi", 1);
        $form->setTaille("am_sport_moto", 1);
        $form->setTaille("am_sport_attrac", 1);
        $form->setTaille("am_sport_golf", 1);
        $form->setTaille("am_mob_art", 1);
        $form->setTaille("am_modif_voie_esp", 1);
        $form->setTaille("am_plant_voie_esp", 1);
        $form->setTaille("am_chem_ouv_esp", 1);
        $form->setTaille("am_agri_peche", 1);
        $form->setTaille("am_crea_voie", 1);
        $form->setTaille("am_modif_voie_exist", 1);
        $form->setTaille("am_crea_esp_sauv", 1);
        $form->setTaille("am_crea_esp_class", 1);
        $form->setTaille("am_projet_desc", 80);
        $form->setTaille("am_terr_surf", 10);
        $form->setTaille("am_tranche_desc", 80);
        $form->setTaille("am_lot_max_nb", 11);
        $form->setTaille("am_lot_max_shon", 10);
        $form->setTaille("am_lot_cstr_cos", 1);
        $form->setTaille("am_lot_cstr_plan", 1);
        $form->setTaille("am_lot_cstr_vente", 1);
        $form->setTaille("am_lot_fin_diff", 1);
        $form->setTaille("am_lot_consign", 1);
        $form->setTaille("am_lot_gar_achev", 1);
        $form->setTaille("am_lot_vente_ant", 1);
        $form->setTaille("am_empl_nb", 11);
        $form->setTaille("am_tente_nb", 11);
        $form->setTaille("am_carav_nb", 11);
        $form->setTaille("am_mobil_nb", 11);
        $form->setTaille("am_pers_nb", 11);
        $form->setTaille("am_empl_hll_nb", 11);
        $form->setTaille("am_hll_shon", 10);
        $form->setTaille("am_periode_exploit", 80);
        $form->setTaille("am_exist_agrand", 1);
        $form->setTaille("am_exist_date", 12);
        $form->setTaille("am_exist_num", 30);
        $form->setTaille("am_exist_nb_avant", 11);
        $form->setTaille("am_exist_nb_apres", 11);
        $form->setTaille("am_coupe_bois", 1);
        $form->setTaille("am_coupe_parc", 1);
        $form->setTaille("am_coupe_align", 1);
        $form->setTaille("am_coupe_ess", 30);
        $form->setTaille("am_coupe_age", 15);
        $form->setTaille("am_coupe_dens", 30);
        $form->setTaille("am_coupe_qual", 30);
        $form->setTaille("am_coupe_trait", 30);
        $form->setTaille("am_coupe_autr", 30);
        $form->setTaille("co_archi_recours", 1);
        $form->setTaille("co_cstr_nouv", 1);
        $form->setTaille("co_cstr_exist", 1);
        $form->setTaille("co_cloture", 1);
        $form->setTaille("co_elec_tension", 10);
        $form->setTaille("co_div_terr", 1);
        $form->setTaille("co_projet_desc", 80);
        $form->setTaille("co_anx_pisc", 1);
        $form->setTaille("co_anx_gara", 1);
        $form->setTaille("co_anx_veran", 1);
        $form->setTaille("co_anx_abri", 1);
        $form->setTaille("co_anx_autr", 1);
        $form->setTaille("co_anx_autr_desc", 80);
        $form->setTaille("co_tot_log_nb", 11);
        $form->setTaille("co_tot_ind_nb", 11);
        $form->setTaille("co_tot_coll_nb", 11);
        $form->setTaille("co_mais_piece_nb", 11);
        $form->setTaille("co_mais_niv_nb", 11);
        $form->setTaille("co_fin_lls_nb", 11);
        $form->setTaille("co_fin_aa_nb", 11);
        $form->setTaille("co_fin_ptz_nb", 11);
        $form->setTaille("co_fin_autr_nb", 11);
        $form->setTaille("co_fin_autr_desc", 80);
        $form->setTaille("co_mais_contrat_ind", 1);
        $form->setTaille("co_uti_pers", 1);
        $form->setTaille("co_uti_vente", 1);
        $form->setTaille("co_uti_loc", 1);
        $form->setTaille("co_uti_princ", 1);
        $form->setTaille("co_uti_secon", 1);
        $form->setTaille("co_resid_agees", 1);
        $form->setTaille("co_resid_etud", 1);
        $form->setTaille("co_resid_tourism", 1);
        $form->setTaille("co_resid_hot_soc", 1);
        $form->setTaille("co_resid_soc", 1);
        $form->setTaille("co_resid_hand", 1);
        $form->setTaille("co_resid_autr", 1);
        $form->setTaille("co_resid_autr_desc", 80);
        $form->setTaille("co_foyer_chamb_nb", 11);
        $form->setTaille("co_log_1p_nb", 11);
        $form->setTaille("co_log_2p_nb", 11);
        $form->setTaille("co_log_3p_nb", 11);
        $form->setTaille("co_log_4p_nb", 11);
        $form->setTaille("co_log_5p_nb", 11);
        $form->setTaille("co_log_6p_nb", 11);
        $form->setTaille("co_bat_niv_nb", 11);
        $form->setTaille("co_trx_exten", 1);
        $form->setTaille("co_trx_surelev", 1);
        $form->setTaille("co_trx_nivsup", 1);
        $form->setTaille("co_demont_periode", 80);
        $form->setTaille("co_sp_transport", 1);
        $form->setTaille("co_sp_enseign", 1);
        $form->setTaille("co_sp_act_soc", 1);
        $form->setTaille("co_sp_ouvr_spe", 1);
        $form->setTaille("co_sp_sante", 1);
        $form->setTaille("co_sp_culture", 1);
        $form->setTaille("co_statio_avt_nb", 11);
        $form->setTaille("co_statio_apr_nb", 11);
        $form->setTaille("co_statio_adr", 80);
        $form->setTaille("co_statio_place_nb", 11);
        $form->setTaille("co_statio_tot_surf", 10);
        $form->setTaille("co_statio_tot_shob", 10);
        $form->setTaille("co_statio_comm_cin_surf", 10);
        $form->setTaille("su_avt_shon1", 10);
        $form->setTaille("su_avt_shon2", 10);
        $form->setTaille("su_avt_shon3", 10);
        $form->setTaille("su_avt_shon4", 10);
        $form->setTaille("su_avt_shon5", 10);
        $form->setTaille("su_avt_shon6", 10);
        $form->setTaille("su_avt_shon7", 10);
        $form->setTaille("su_avt_shon8", 10);
        $form->setTaille("su_avt_shon9", 10);
        $form->setTaille("su_cstr_shon1", 10);
        $form->setTaille("su_cstr_shon2", 10);
        $form->setTaille("su_cstr_shon3", 10);
        $form->setTaille("su_cstr_shon4", 10);
        $form->setTaille("su_cstr_shon5", 10);
        $form->setTaille("su_cstr_shon6", 10);
        $form->setTaille("su_cstr_shon7", 10);
        $form->setTaille("su_cstr_shon8", 10);
        $form->setTaille("su_cstr_shon9", 10);
        $form->setTaille("su_trsf_shon1", 10);
        $form->setTaille("su_trsf_shon2", 10);
        $form->setTaille("su_trsf_shon3", 10);
        $form->setTaille("su_trsf_shon4", 10);
        $form->setTaille("su_trsf_shon5", 10);
        $form->setTaille("su_trsf_shon6", 10);
        $form->setTaille("su_trsf_shon7", 10);
        $form->setTaille("su_trsf_shon8", 10);
        $form->setTaille("su_trsf_shon9", 10);
        $form->setTaille("su_chge_shon1", 10);
        $form->setTaille("su_chge_shon2", 10);
        $form->setTaille("su_chge_shon3", 10);
        $form->setTaille("su_chge_shon4", 10);
        $form->setTaille("su_chge_shon5", 10);
        $form->setTaille("su_chge_shon6", 10);
        $form->setTaille("su_chge_shon7", 10);
        $form->setTaille("su_chge_shon8", 10);
        $form->setTaille("su_chge_shon9", 10);
        $form->setTaille("su_demo_shon1", 10);
        $form->setTaille("su_demo_shon2", 10);
        $form->setTaille("su_demo_shon3", 10);
        $form->setTaille("su_demo_shon4", 10);
        $form->setTaille("su_demo_shon5", 10);
        $form->setTaille("su_demo_shon6", 10);
        $form->setTaille("su_demo_shon7", 10);
        $form->setTaille("su_demo_shon8", 10);
        $form->setTaille("su_demo_shon9", 10);
        $form->setTaille("su_sup_shon1", 10);
        $form->setTaille("su_sup_shon2", 10);
        $form->setTaille("su_sup_shon3", 10);
        $form->setTaille("su_sup_shon4", 10);
        $form->setTaille("su_sup_shon5", 10);
        $form->setTaille("su_sup_shon6", 10);
        $form->setTaille("su_sup_shon7", 10);
        $form->setTaille("su_sup_shon8", 10);
        $form->setTaille("su_sup_shon9", 10);
        $form->setTaille("su_tot_shon1", 10);
        $form->setTaille("su_tot_shon2", 10);
        $form->setTaille("su_tot_shon3", 10);
        $form->setTaille("su_tot_shon4", 10);
        $form->setTaille("su_tot_shon5", 10);
        $form->setTaille("su_tot_shon6", 10);
        $form->setTaille("su_tot_shon7", 10);
        $form->setTaille("su_tot_shon8", 10);
        $form->setTaille("su_tot_shon9", 10);
        $form->setTaille("su_avt_shon_tot", 10);
        $form->setTaille("su_cstr_shon_tot", 10);
        $form->setTaille("su_trsf_shon_tot", 10);
        $form->setTaille("su_chge_shon_tot", 10);
        $form->setTaille("su_demo_shon_tot", 10);
        $form->setTaille("su_sup_shon_tot", 10);
        $form->setTaille("su_tot_shon_tot", 10);
        $form->setTaille("dm_constr_dates", 80);
        $form->setTaille("dm_total", 1);
        $form->setTaille("dm_partiel", 1);
        $form->setTaille("dm_projet_desc", 80);
        $form->setTaille("dm_tot_log_nb", 11);
        $form->setTaille("tax_surf_tot", 10);
        $form->setTaille("tax_surf", 10);
        $form->setTaille("tax_surf_suppr_mod", 10);
        $form->setTaille("tax_su_princ_log_nb1", 10);
        $form->setTaille("tax_su_princ_log_nb2", 10);
        $form->setTaille("tax_su_princ_log_nb3", 10);
        $form->setTaille("tax_su_princ_log_nb4", 10);
        $form->setTaille("tax_su_princ_log_nb_tot1", 10);
        $form->setTaille("tax_su_princ_log_nb_tot2", 10);
        $form->setTaille("tax_su_princ_log_nb_tot3", 10);
        $form->setTaille("tax_su_princ_log_nb_tot4", 10);
        $form->setTaille("tax_su_princ_surf1", 10);
        $form->setTaille("tax_su_princ_surf2", 10);
        $form->setTaille("tax_su_princ_surf3", 10);
        $form->setTaille("tax_su_princ_surf4", 10);
        $form->setTaille("tax_su_princ_surf_sup1", 10);
        $form->setTaille("tax_su_princ_surf_sup2", 10);
        $form->setTaille("tax_su_princ_surf_sup3", 10);
        $form->setTaille("tax_su_princ_surf_sup4", 10);
        $form->setTaille("tax_su_heber_log_nb1", 11);
        $form->setTaille("tax_su_heber_log_nb2", 11);
        $form->setTaille("tax_su_heber_log_nb3", 11);
        $form->setTaille("tax_su_heber_log_nb_tot1", 11);
        $form->setTaille("tax_su_heber_log_nb_tot2", 11);
        $form->setTaille("tax_su_heber_log_nb_tot3", 11);
        $form->setTaille("tax_su_heber_surf1", 10);
        $form->setTaille("tax_su_heber_surf2", 10);
        $form->setTaille("tax_su_heber_surf3", 10);
        $form->setTaille("tax_su_heber_surf_sup1", 10);
        $form->setTaille("tax_su_heber_surf_sup2", 10);
        $form->setTaille("tax_su_heber_surf_sup3", 10);
        $form->setTaille("tax_su_secon_log_nb", 11);
        $form->setTaille("tax_su_tot_log_nb", 11);
        $form->setTaille("tax_su_secon_log_nb_tot", 11);
        $form->setTaille("tax_su_tot_log_nb_tot", 11);
        $form->setTaille("tax_su_secon_surf", 10);
        $form->setTaille("tax_su_tot_surf", 10);
        $form->setTaille("tax_su_secon_surf_sup", 10);
        $form->setTaille("tax_su_tot_surf_sup", 10);
        $form->setTaille("tax_ext_pret", 1);
        $form->setTaille("tax_ext_desc", 80);
        $form->setTaille("tax_surf_tax_exist_cons", 10);
        $form->setTaille("tax_log_exist_nb", 11);
        $form->setTaille("tax_am_statio_ext", 11);
        $form->setTaille("tax_sup_bass_pisc", 10);
        $form->setTaille("tax_empl_ten_carav_mobil_nb", 11);
        $form->setTaille("tax_empl_hll_nb", 11);
        $form->setTaille("tax_eol_haut_nb", 11);
        $form->setTaille("tax_pann_volt_sup", 10);
        $form->setTaille("tax_am_statio_ext_sup", 11);
        $form->setTaille("tax_sup_bass_pisc_sup", 10);
        $form->setTaille("tax_empl_ten_carav_mobil_nb_sup", 11);
        $form->setTaille("tax_empl_hll_nb_sup", 11);
        $form->setTaille("tax_eol_haut_nb_sup", 11);
        $form->setTaille("tax_pann_volt_sup_sup", 10);
        $form->setTaille("tax_trx_presc_ppr", 1);
        $form->setTaille("tax_monu_hist", 1);
        $form->setTaille("tax_comm_nb", 11);
        $form->setTaille("tax_su_non_habit_surf1", 10);
        $form->setTaille("tax_su_non_habit_surf2", 10);
        $form->setTaille("tax_su_non_habit_surf3", 10);
        $form->setTaille("tax_su_non_habit_surf4", 10);
        $form->setTaille("tax_su_non_habit_surf5", 10);
        $form->setTaille("tax_su_non_habit_surf6", 10);
        $form->setTaille("tax_su_non_habit_surf7", 10);
        $form->setTaille("tax_su_non_habit_surf_sup1", 10);
        $form->setTaille("tax_su_non_habit_surf_sup2", 10);
        $form->setTaille("tax_su_non_habit_surf_sup3", 10);
        $form->setTaille("tax_su_non_habit_surf_sup4", 10);
        $form->setTaille("tax_su_non_habit_surf_sup5", 10);
        $form->setTaille("tax_su_non_habit_surf_sup6", 10);
        $form->setTaille("tax_su_non_habit_surf_sup7", 10);
        $form->setTaille("vsd_surf_planch_smd", 1);
        $form->setTaille("vsd_unit_fonc_sup", 10);
        $form->setTaille("vsd_unit_fonc_constr_sup", 10);
        $form->setTaille("vsd_val_terr", 10);
        $form->setTaille("vsd_const_sxist_non_dem_surf", 10);
        $form->setTaille("vsd_rescr_fisc", 12);
        $form->setTaille("pld_val_terr", 10);
        $form->setTaille("pld_const_exist_dem", 1);
        $form->setTaille("pld_const_exist_dem_surf", 10);
        $form->setTaille("code_cnil", 1);
        $form->setTaille("terr_juri_titul", 20);
        $form->setTaille("terr_juri_lot", 20);
        $form->setTaille("terr_juri_zac", 20);
        $form->setTaille("terr_juri_afu", 20);
        $form->setTaille("terr_juri_pup", 20);
        $form->setTaille("terr_juri_oin", 20);
        $form->setTaille("terr_juri_desc", 80);
        $form->setTaille("terr_div_surf_etab", 10);
        $form->setTaille("terr_div_surf_av_div", 10);
        $form->setTaille("doc_date", 12);
        $form->setTaille("doc_tot_trav", 1);
        $form->setTaille("doc_tranche_trav", 1);
        $form->setTaille("doc_tranche_trav_desc", 80);
        $form->setTaille("doc_surf", 10);
        $form->setTaille("doc_nb_log", 11);
        $form->setTaille("doc_nb_log_indiv", 11);
        $form->setTaille("doc_nb_log_coll", 11);
        $form->setTaille("doc_nb_log_lls", 11);
        $form->setTaille("doc_nb_log_aa", 11);
        $form->setTaille("doc_nb_log_ptz", 11);
        $form->setTaille("doc_nb_log_autre", 11);
        $form->setTaille("daact_date", 12);
        $form->setTaille("daact_date_chgmt_dest", 12);
        $form->setTaille("daact_tot_trav", 1);
        $form->setTaille("daact_tranche_trav", 1);
        $form->setTaille("daact_tranche_trav_desc", 80);
        $form->setTaille("daact_surf", 10);
        $form->setTaille("daact_nb_log", 11);
        $form->setTaille("daact_nb_log_indiv", 11);
        $form->setTaille("daact_nb_log_coll", 11);
        $form->setTaille("daact_nb_log_lls", 11);
        $form->setTaille("daact_nb_log_aa", 11);
        $form->setTaille("daact_nb_log_ptz", 11);
        $form->setTaille("daact_nb_log_autre", 11);
        $form->setTaille("dossier_autorisation", 20);
        $form->setTaille("am_div_mun", 1);
        $form->setTaille("co_perf_energ", 30);
        $form->setTaille("architecte", 11);
        $form->setTaille("co_statio_avt_shob", 30);
        $form->setTaille("co_statio_apr_shob", 30);
        $form->setTaille("co_statio_avt_surf", 30);
        $form->setTaille("co_statio_apr_surf", 30);
        $form->setTaille("co_trx_amgt", 30);
        $form->setTaille("co_modif_aspect", 30);
        $form->setTaille("co_modif_struct", 30);
        $form->setTaille("co_ouvr_elec", 1);
        $form->setTaille("co_ouvr_infra", 1);
        $form->setTaille("co_trx_imm", 30);
        $form->setTaille("co_cstr_shob", 30);
        $form->setTaille("am_voyage_deb", 30);
        $form->setTaille("am_voyage_fin", 30);
        $form->setTaille("am_modif_amgt", 30);
        $form->setTaille("am_lot_max_shob", 30);
        $form->setTaille("mod_desc", 30);
        $form->setTaille("tr_total", 30);
        $form->setTaille("tr_partiel", 30);
        $form->setTaille("tr_desc", 30);
        $form->setTaille("avap_co_elt_pro", 1);
        $form->setTaille("avap_nouv_haut_surf", 1);
        $form->setTaille("avap_co_clot", 30);
        $form->setTaille("avap_aut_coup_aba_arb", 30);
        $form->setTaille("avap_ouv_infra", 30);
        $form->setTaille("avap_aut_inst_mob", 30);
        $form->setTaille("avap_aut_plant", 30);
        $form->setTaille("avap_aut_auv_elec", 30);
        $form->setTaille("tax_dest_loc_tr", 30);
        $form->setTaille("ope_proj_desc", 80);
        $form->setTaille("tax_surf_tot_cstr", 11);
        $form->setTaille("cerfa", 11);
        $form->setTaille("tax_surf_loc_stat", 11);
        $form->setTaille("tax_su_princ_surf_stat1", 10);
        $form->setTaille("tax_su_princ_surf_stat2", 10);
        $form->setTaille("tax_su_princ_surf_stat3", 10);
        $form->setTaille("tax_su_princ_surf_stat4", 10);
        $form->setTaille("tax_su_secon_surf_stat", 10);
        $form->setTaille("tax_su_heber_surf_stat1", 10);
        $form->setTaille("tax_su_heber_surf_stat2", 10);
        $form->setTaille("tax_su_heber_surf_stat3", 10);
        $form->setTaille("tax_su_tot_surf_stat", 10);
        $form->setTaille("tax_su_non_habit_surf_stat1", 10);
        $form->setTaille("tax_su_non_habit_surf_stat2", 10);
        $form->setTaille("tax_su_non_habit_surf_stat3", 10);
        $form->setTaille("tax_su_non_habit_surf_stat4", 10);
        $form->setTaille("tax_su_non_habit_surf_stat5", 10);
        $form->setTaille("tax_su_non_habit_surf_stat6", 10);
        $form->setTaille("tax_su_non_habit_surf_stat7", 10);
        $form->setTaille("tax_su_parc_statio_expl_comm_surf", 10);
        $form->setTaille("tax_log_ap_trvx_nb", 11);
        $form->setTaille("tax_am_statio_ext_cr", 11);
        $form->setTaille("tax_sup_bass_pisc_cr", 10);
        $form->setTaille("tax_empl_ten_carav_mobil_nb_cr", 11);
        $form->setTaille("tax_empl_hll_nb_cr", 11);
        $form->setTaille("tax_eol_haut_nb_cr", 11);
        $form->setTaille("tax_pann_volt_sup_cr", 10);
        $form->setTaille("tax_surf_loc_arch", 10);
        $form->setTaille("tax_surf_pisc_arch", 10);
        $form->setTaille("tax_am_statio_ext_arch", 10);
        $form->setTaille("tax_empl_ten_carav_mobil_nb_arch", 10);
        $form->setTaille("tax_empl_hll_nb_arch", 10);
        $form->setTaille("tax_eol_haut_nb_arch", 11);
        $form->setTaille("ope_proj_div_co", 1);
        $form->setTaille("ope_proj_div_contr", 1);
        $form->setTaille("tax_desc", 80);
        $form->setTaille("erp_cstr_neuve", 1);
        $form->setTaille("erp_trvx_acc", 1);
        $form->setTaille("erp_extension", 1);
        $form->setTaille("erp_rehab", 1);
        $form->setTaille("erp_trvx_am", 1);
        $form->setTaille("erp_vol_nouv_exist", 1);
        $form->setTaille("erp_loc_eff1", 10);
        $form->setTaille("erp_loc_eff2", 10);
        $form->setTaille("erp_loc_eff3", 10);
        $form->setTaille("erp_loc_eff4", 10);
        $form->setTaille("erp_loc_eff5", 10);
        $form->setTaille("erp_loc_eff_tot", 10);
        $form->setTaille("erp_public_eff1", 10);
        $form->setTaille("erp_public_eff2", 10);
        $form->setTaille("erp_public_eff3", 10);
        $form->setTaille("erp_public_eff4", 10);
        $form->setTaille("erp_public_eff5", 10);
        $form->setTaille("erp_public_eff_tot", 10);
        $form->setTaille("erp_perso_eff1", 10);
        $form->setTaille("erp_perso_eff2", 10);
        $form->setTaille("erp_perso_eff3", 10);
        $form->setTaille("erp_perso_eff4", 10);
        $form->setTaille("erp_perso_eff5", 10);
        $form->setTaille("erp_perso_eff_tot", 10);
        $form->setTaille("erp_tot_eff1", 10);
        $form->setTaille("erp_tot_eff2", 10);
        $form->setTaille("erp_tot_eff3", 10);
        $form->setTaille("erp_tot_eff4", 10);
        $form->setTaille("erp_tot_eff5", 10);
        $form->setTaille("erp_tot_eff_tot", 10);
        $form->setTaille("erp_class_cat", 11);
        $form->setTaille("erp_class_type", 11);
        $form->setTaille("tax_surf_abr_jard_pig_colom", 10);
        $form->setTaille("tax_su_non_habit_abr_jard_pig_colom", 10);
        $form->setTaille("dia_imm_non_bati", 1);
        $form->setTaille("dia_imm_bati_terr_propr", 1);
        $form->setTaille("dia_imm_bati_terr_autr", 1);
        $form->setTaille("dia_imm_bati_terr_autr_desc", 80);
        $form->setTaille("dia_bat_copro", 1);
        $form->setTaille("dia_bat_copro_desc", 80);
        $form->setTaille("dia_lot_numero", 80);
        $form->setTaille("dia_lot_bat", 80);
        $form->setTaille("dia_lot_etage", 80);
        $form->setTaille("dia_lot_quote_part", 80);
        $form->setTaille("dia_us_hab", 1);
        $form->setTaille("dia_us_pro", 1);
        $form->setTaille("dia_us_mixte", 1);
        $form->setTaille("dia_us_comm", 1);
        $form->setTaille("dia_us_agr", 1);
        $form->setTaille("dia_us_autre", 1);
        $form->setTaille("dia_us_autre_prec", 80);
        $form->setTaille("dia_occ_prop", 1);
        $form->setTaille("dia_occ_loc", 1);
        $form->setTaille("dia_occ_sans_occ", 1);
        $form->setTaille("dia_occ_autre", 1);
        $form->setTaille("dia_occ_autre_prec", 80);
        $form->setTaille("dia_mod_cess_prix_vente", 30);
        $form->setTaille("dia_mod_cess_prix_vente_mob", 30);
        $form->setTaille("dia_mod_cess_prix_vente_cheptel", 30);
        $form->setTaille("dia_mod_cess_prix_vente_recol", 30);
        $form->setTaille("dia_mod_cess_prix_vente_autre", 30);
        $form->setTaille("dia_mod_cess_commi", 1);
        $form->setTaille("dia_mod_cess_commi_ttc", 30);
        $form->setTaille("dia_mod_cess_commi_ht", 30);
        $form->setTaille("dia_acquereur_nom_prenom", 30);
        $form->setTaille("dia_acquereur_adr_num_voie", 10);
        $form->setTaille("dia_acquereur_adr_ext", 30);
        $form->setTaille("dia_acquereur_adr_type_voie", 20);
        $form->setTaille("dia_acquereur_adr_nom_voie", 30);
        $form->setTaille("dia_acquereur_adr_lieu_dit_bp", 30);
        $form->setTaille("dia_acquereur_adr_cp", 10);
        $form->setTaille("dia_acquereur_adr_localite", 30);
        $form->setTaille("dia_observation", 80);
        $form->setTaille("su2_avt_shon1", 10);
        $form->setTaille("su2_avt_shon2", 10);
        $form->setTaille("su2_avt_shon3", 10);
        $form->setTaille("su2_avt_shon4", 10);
        $form->setTaille("su2_avt_shon5", 10);
        $form->setTaille("su2_avt_shon6", 10);
        $form->setTaille("su2_avt_shon7", 10);
        $form->setTaille("su2_avt_shon8", 10);
        $form->setTaille("su2_avt_shon9", 10);
        $form->setTaille("su2_avt_shon10", 10);
        $form->setTaille("su2_avt_shon11", 10);
        $form->setTaille("su2_avt_shon12", 10);
        $form->setTaille("su2_avt_shon13", 10);
        $form->setTaille("su2_avt_shon14", 10);
        $form->setTaille("su2_avt_shon15", 10);
        $form->setTaille("su2_avt_shon16", 10);
        $form->setTaille("su2_avt_shon17", 10);
        $form->setTaille("su2_avt_shon18", 10);
        $form->setTaille("su2_avt_shon19", 10);
        $form->setTaille("su2_avt_shon20", 10);
        $form->setTaille("su2_avt_shon_tot", 10);
        $form->setTaille("su2_cstr_shon1", 10);
        $form->setTaille("su2_cstr_shon2", 10);
        $form->setTaille("su2_cstr_shon3", 10);
        $form->setTaille("su2_cstr_shon4", 10);
        $form->setTaille("su2_cstr_shon5", 10);
        $form->setTaille("su2_cstr_shon6", 10);
        $form->setTaille("su2_cstr_shon7", 10);
        $form->setTaille("su2_cstr_shon8", 10);
        $form->setTaille("su2_cstr_shon9", 10);
        $form->setTaille("su2_cstr_shon10", 10);
        $form->setTaille("su2_cstr_shon11", 10);
        $form->setTaille("su2_cstr_shon12", 10);
        $form->setTaille("su2_cstr_shon13", 10);
        $form->setTaille("su2_cstr_shon14", 10);
        $form->setTaille("su2_cstr_shon15", 10);
        $form->setTaille("su2_cstr_shon16", 10);
        $form->setTaille("su2_cstr_shon17", 10);
        $form->setTaille("su2_cstr_shon18", 10);
        $form->setTaille("su2_cstr_shon19", 10);
        $form->setTaille("su2_cstr_shon20", 10);
        $form->setTaille("su2_cstr_shon_tot", 10);
        $form->setTaille("su2_chge_shon1", 10);
        $form->setTaille("su2_chge_shon2", 10);
        $form->setTaille("su2_chge_shon3", 10);
        $form->setTaille("su2_chge_shon4", 10);
        $form->setTaille("su2_chge_shon5", 10);
        $form->setTaille("su2_chge_shon6", 10);
        $form->setTaille("su2_chge_shon7", 10);
        $form->setTaille("su2_chge_shon8", 10);
        $form->setTaille("su2_chge_shon9", 10);
        $form->setTaille("su2_chge_shon10", 10);
        $form->setTaille("su2_chge_shon11", 10);
        $form->setTaille("su2_chge_shon12", 10);
        $form->setTaille("su2_chge_shon13", 10);
        $form->setTaille("su2_chge_shon14", 10);
        $form->setTaille("su2_chge_shon15", 10);
        $form->setTaille("su2_chge_shon16", 10);
        $form->setTaille("su2_chge_shon17", 10);
        $form->setTaille("su2_chge_shon18", 10);
        $form->setTaille("su2_chge_shon19", 10);
        $form->setTaille("su2_chge_shon20", 10);
        $form->setTaille("su2_chge_shon_tot", 10);
        $form->setTaille("su2_demo_shon1", 10);
        $form->setTaille("su2_demo_shon2", 10);
        $form->setTaille("su2_demo_shon3", 10);
        $form->setTaille("su2_demo_shon4", 10);
        $form->setTaille("su2_demo_shon5", 10);
        $form->setTaille("su2_demo_shon6", 10);
        $form->setTaille("su2_demo_shon7", 10);
        $form->setTaille("su2_demo_shon8", 10);
        $form->setTaille("su2_demo_shon9", 10);
        $form->setTaille("su2_demo_shon10", 10);
        $form->setTaille("su2_demo_shon11", 10);
        $form->setTaille("su2_demo_shon12", 10);
        $form->setTaille("su2_demo_shon13", 10);
        $form->setTaille("su2_demo_shon14", 10);
        $form->setTaille("su2_demo_shon15", 10);
        $form->setTaille("su2_demo_shon16", 10);
        $form->setTaille("su2_demo_shon17", 10);
        $form->setTaille("su2_demo_shon18", 10);
        $form->setTaille("su2_demo_shon19", 10);
        $form->setTaille("su2_demo_shon20", 10);
        $form->setTaille("su2_demo_shon_tot", 10);
        $form->setTaille("su2_sup_shon1", 10);
        $form->setTaille("su2_sup_shon2", 10);
        $form->setTaille("su2_sup_shon3", 10);
        $form->setTaille("su2_sup_shon4", 10);
        $form->setTaille("su2_sup_shon5", 10);
        $form->setTaille("su2_sup_shon6", 10);
        $form->setTaille("su2_sup_shon7", 10);
        $form->setTaille("su2_sup_shon8", 10);
        $form->setTaille("su2_sup_shon9", 10);
        $form->setTaille("su2_sup_shon10", 10);
        $form->setTaille("su2_sup_shon11", 10);
        $form->setTaille("su2_sup_shon12", 10);
        $form->setTaille("su2_sup_shon13", 10);
        $form->setTaille("su2_sup_shon14", 10);
        $form->setTaille("su2_sup_shon15", 10);
        $form->setTaille("su2_sup_shon16", 10);
        $form->setTaille("su2_sup_shon17", 10);
        $form->setTaille("su2_sup_shon18", 10);
        $form->setTaille("su2_sup_shon19", 10);
        $form->setTaille("su2_sup_shon20", 10);
        $form->setTaille("su2_sup_shon_tot", 10);
        $form->setTaille("su2_tot_shon1", 10);
        $form->setTaille("su2_tot_shon2", 10);
        $form->setTaille("su2_tot_shon3", 10);
        $form->setTaille("su2_tot_shon4", 10);
        $form->setTaille("su2_tot_shon5", 10);
        $form->setTaille("su2_tot_shon6", 10);
        $form->setTaille("su2_tot_shon7", 10);
        $form->setTaille("su2_tot_shon8", 10);
        $form->setTaille("su2_tot_shon9", 10);
        $form->setTaille("su2_tot_shon10", 10);
        $form->setTaille("su2_tot_shon11", 10);
        $form->setTaille("su2_tot_shon12", 10);
        $form->setTaille("su2_tot_shon13", 10);
        $form->setTaille("su2_tot_shon14", 10);
        $form->setTaille("su2_tot_shon15", 10);
        $form->setTaille("su2_tot_shon16", 10);
        $form->setTaille("su2_tot_shon17", 10);
        $form->setTaille("su2_tot_shon18", 10);
        $form->setTaille("su2_tot_shon19", 10);
        $form->setTaille("su2_tot_shon20", 10);
        $form->setTaille("su2_tot_shon_tot", 10);
        $form->setTaille("dia_occ_sol_su_terre", 80);
        $form->setTaille("dia_occ_sol_su_pres", 80);
        $form->setTaille("dia_occ_sol_su_verger", 80);
        $form->setTaille("dia_occ_sol_su_vigne", 80);
        $form->setTaille("dia_occ_sol_su_bois", 80);
        $form->setTaille("dia_occ_sol_su_lande", 80);
        $form->setTaille("dia_occ_sol_su_carriere", 80);
        $form->setTaille("dia_occ_sol_su_eau_cadastree", 80);
        $form->setTaille("dia_occ_sol_su_jardin", 80);
        $form->setTaille("dia_occ_sol_su_terr_batir", 80);
        $form->setTaille("dia_occ_sol_su_terr_agr", 80);
        $form->setTaille("dia_occ_sol_su_sol", 80);
        $form->setTaille("dia_bati_vend_tot", 1);
        $form->setTaille("dia_bati_vend_tot_txt", 80);
        $form->setTaille("dia_su_co_sol", 80);
        $form->setTaille("dia_su_util_hab", 80);
        $form->setTaille("dia_nb_niv", 80);
        $form->setTaille("dia_nb_appart", 80);
        $form->setTaille("dia_nb_autre_loc", 80);
        $form->setTaille("dia_vente_lot_volume", 1);
        $form->setTaille("dia_vente_lot_volume_txt", 80);
        $form->setTaille("dia_lot_nat_su", 80);
        $form->setTaille("dia_lot_bat_achv_plus_10", 1);
        $form->setTaille("dia_lot_bat_achv_moins_10", 1);
        $form->setTaille("dia_lot_regl_copro_publ_hypo_plus_10", 1);
        $form->setTaille("dia_lot_regl_copro_publ_hypo_moins_10", 1);
        $form->setTaille("dia_indivi_quote_part", 80);
        $form->setTaille("dia_design_societe", 80);
        $form->setTaille("dia_design_droit", 80);
        $form->setTaille("dia_droit_soc_nat", 80);
        $form->setTaille("dia_droit_soc_nb", 80);
        $form->setTaille("dia_droit_soc_num_part", 80);
        $form->setTaille("dia_droit_reel_perso_grevant_bien_oui", 1);
        $form->setTaille("dia_droit_reel_perso_grevant_bien_non", 1);
        $form->setTaille("dia_droit_reel_perso_nat", 80);
        $form->setTaille("dia_droit_reel_perso_viag", 80);
        $form->setTaille("dia_mod_cess_adr", 80);
        $form->setTaille("dia_mod_cess_sign_act_auth", 1);
        $form->setTaille("dia_mod_cess_terme", 1);
        $form->setTaille("dia_mod_cess_terme_prec", 80);
        $form->setTaille("dia_mod_cess_bene_acquereur", 1);
        $form->setTaille("dia_mod_cess_bene_vendeur", 1);
        $form->setTaille("dia_mod_cess_paie_nat", 1);
        $form->setTaille("dia_mod_cess_design_contr_alien", 80);
        $form->setTaille("dia_mod_cess_eval_contr", 80);
        $form->setTaille("dia_mod_cess_rente_viag", 1);
        $form->setTaille("dia_mod_cess_mnt_an", 80);
        $form->setTaille("dia_mod_cess_mnt_compt", 80);
        $form->setTaille("dia_mod_cess_bene_rente", 80);
        $form->setTaille("dia_mod_cess_droit_usa_hab", 1);
        $form->setTaille("dia_mod_cess_droit_usa_hab_prec", 80);
        $form->setTaille("dia_mod_cess_eval_usa_usufruit", 80);
        $form->setTaille("dia_mod_cess_vente_nue_prop", 1);
        $form->setTaille("dia_mod_cess_vente_nue_prop_prec", 80);
        $form->setTaille("dia_mod_cess_echange", 1);
        $form->setTaille("dia_mod_cess_design_bien_recus_ech", 80);
        $form->setTaille("dia_mod_cess_mnt_soulte", 80);
        $form->setTaille("dia_mod_cess_prop_contre_echan", 80);
        $form->setTaille("dia_mod_cess_apport_societe", 80);
        $form->setTaille("dia_mod_cess_bene", 80);
        $form->setTaille("dia_mod_cess_esti_bien", 80);
        $form->setTaille("dia_mod_cess_cess_terr_loc_co", 1);
        $form->setTaille("dia_mod_cess_esti_terr", 80);
        $form->setTaille("dia_mod_cess_esti_loc", 80);
        $form->setTaille("dia_mod_cess_esti_imm_loca", 1);
        $form->setTaille("dia_mod_cess_adju_vol", 1);
        $form->setTaille("dia_mod_cess_adju_obl", 1);
        $form->setTaille("dia_mod_cess_adju_fin_indivi", 1);
        $form->setTaille("dia_mod_cess_adju_date_lieu", 80);
        $form->setTaille("dia_mod_cess_mnt_mise_prix", 80);
        $form->setTaille("dia_prop_titu_prix_indique", 1);
        $form->setTaille("dia_prop_recherche_acqu_prix_indique", 1);
        $form->setTaille("dia_acquereur_prof", 80);
        $form->setTaille("dia_indic_compl_ope", 80);
        $form->setTaille("dia_vente_adju", 1);
        $form->setTaille("am_terr_res_demon", 1);
        $form->setTaille("am_air_terr_res_mob", 1);
        $form->setTaille("ctx_objet_recours", 11);
        $form->setTaille("ctx_reference_sagace", 30);
        $form->setTaille("ctx_nature_travaux_infra_om_html", 80);
        $form->setTaille("ctx_synthese_nti", 80);
        $form->setTaille("ctx_article_non_resp_om_html", 80);
        $form->setTaille("ctx_synthese_anr", 80);
        $form->setTaille("ctx_reference_parquet", 30);
        $form->setTaille("ctx_element_taxation", 30);
        $form->setTaille("ctx_infraction", 1);
        $form->setTaille("ctx_regularisable", 1);
        $form->setTaille("ctx_reference_courrier", 30);
        $form->setTaille("ctx_date_audience", 12);
        $form->setTaille("ctx_date_ajournement", 12);
        $form->setTaille("exo_facul_1", 1);
        $form->setTaille("exo_facul_2", 1);
        $form->setTaille("exo_facul_3", 1);
        $form->setTaille("exo_facul_4", 1);
        $form->setTaille("exo_facul_5", 1);
        $form->setTaille("exo_facul_6", 1);
        $form->setTaille("exo_facul_7", 1);
        $form->setTaille("exo_facul_8", 1);
        $form->setTaille("exo_facul_9", 1);
        $form->setTaille("exo_ta_1", 1);
        $form->setTaille("exo_ta_2", 1);
        $form->setTaille("exo_ta_3", 1);
        $form->setTaille("exo_ta_4", 1);
        $form->setTaille("exo_ta_5", 1);
        $form->setTaille("exo_ta_6", 1);
        $form->setTaille("exo_ta_7", 1);
        $form->setTaille("exo_ta_8", 1);
        $form->setTaille("exo_ta_9", 1);
        $form->setTaille("exo_rap_1", 1);
        $form->setTaille("exo_rap_2", 1);
        $form->setTaille("exo_rap_3", 1);
        $form->setTaille("exo_rap_4", 1);
        $form->setTaille("exo_rap_5", 1);
        $form->setTaille("exo_rap_6", 1);
        $form->setTaille("exo_rap_7", 1);
        $form->setTaille("exo_rap_8", 1);
        $form->setTaille("mtn_exo_ta_part_commu", 10);
        $form->setTaille("mtn_exo_ta_part_depart", 10);
        $form->setTaille("mtn_exo_ta_part_reg", 10);
        $form->setTaille("mtn_exo_rap", 10);
        $form->setTaille("dpc_type", 30);
        $form->setTaille("dpc_desc_actv_ex", 80);
        $form->setTaille("dpc_desc_ca", 80);
        $form->setTaille("dpc_desc_aut_prec", 80);
        $form->setTaille("dpc_desig_comm_arti", 1);
        $form->setTaille("dpc_desig_loc_hab", 1);
        $form->setTaille("dpc_desig_loc_ann", 1);
        $form->setTaille("dpc_desig_loc_ann_prec", 80);
        $form->setTaille("dpc_bail_comm_date", 12);
        $form->setTaille("dpc_bail_comm_loyer", 80);
        $form->setTaille("dpc_actv_acqu", 80);
        $form->setTaille("dpc_nb_sala_di", 80);
        $form->setTaille("dpc_nb_sala_dd", 80);
        $form->setTaille("dpc_nb_sala_tc", 80);
        $form->setTaille("dpc_nb_sala_tp", 80);
        $form->setTaille("dpc_moda_cess_vente_am", 1);
        $form->setTaille("dpc_moda_cess_adj", 1);
        $form->setTaille("dpc_moda_cess_prix", 80);
        $form->setTaille("dpc_moda_cess_adj_date", 12);
        $form->setTaille("dpc_moda_cess_adj_prec", 80);
        $form->setTaille("dpc_moda_cess_paie_comp", 1);
        $form->setTaille("dpc_moda_cess_paie_terme", 1);
        $form->setTaille("dpc_moda_cess_paie_terme_prec", 80);
        $form->setTaille("dpc_moda_cess_paie_nat", 1);
        $form->setTaille("dpc_moda_cess_paie_nat_desig_alien", 1);
        $form->setTaille("dpc_moda_cess_paie_nat_desig_alien_prec", 80);
        $form->setTaille("dpc_moda_cess_paie_nat_eval", 1);
        $form->setTaille("dpc_moda_cess_paie_nat_eval_prec", 80);
        $form->setTaille("dpc_moda_cess_paie_aut", 1);
        $form->setTaille("dpc_moda_cess_paie_aut_prec", 80);
        $form->setTaille("dpc_ss_signe_demande_acqu", 1);
        $form->setTaille("dpc_ss_signe_recher_trouv_acqu", 1);
        $form->setTaille("dpc_notif_adr_prop", 1);
        $form->setTaille("dpc_notif_adr_manda", 1);
        $form->setTaille("dpc_obs", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("donnees_techniques", 11);
        $form->setMax("dossier_instruction", 30);
        $form->setMax("lot", 11);
        $form->setMax("am_lotiss", 1);
        $form->setMax("am_autre_div", 1);
        $form->setMax("am_camping", 1);
        $form->setMax("am_caravane", 1);
        $form->setMax("am_carav_duree", 11);
        $form->setMax("am_statio", 1);
        $form->setMax("am_statio_cont", 11);
        $form->setMax("am_affou_exhau", 1);
        $form->setMax("am_affou_exhau_sup", -5);
        $form->setMax("am_affou_prof", -5);
        $form->setMax("am_exhau_haut", -5);
        $form->setMax("am_coupe_abat", 1);
        $form->setMax("am_prot_plu", 1);
        $form->setMax("am_prot_muni", 1);
        $form->setMax("am_mobil_voyage", 1);
        $form->setMax("am_aire_voyage", 1);
        $form->setMax("am_rememb_afu", 1);
        $form->setMax("am_parc_resid_loi", 1);
        $form->setMax("am_sport_moto", 1);
        $form->setMax("am_sport_attrac", 1);
        $form->setMax("am_sport_golf", 1);
        $form->setMax("am_mob_art", 1);
        $form->setMax("am_modif_voie_esp", 1);
        $form->setMax("am_plant_voie_esp", 1);
        $form->setMax("am_chem_ouv_esp", 1);
        $form->setMax("am_agri_peche", 1);
        $form->setMax("am_crea_voie", 1);
        $form->setMax("am_modif_voie_exist", 1);
        $form->setMax("am_crea_esp_sauv", 1);
        $form->setMax("am_crea_esp_class", 1);
        $form->setMax("am_projet_desc", 6);
        $form->setMax("am_terr_surf", -5);
        $form->setMax("am_tranche_desc", 6);
        $form->setMax("am_lot_max_nb", 11);
        $form->setMax("am_lot_max_shon", -5);
        $form->setMax("am_lot_cstr_cos", 1);
        $form->setMax("am_lot_cstr_plan", 1);
        $form->setMax("am_lot_cstr_vente", 1);
        $form->setMax("am_lot_fin_diff", 1);
        $form->setMax("am_lot_consign", 1);
        $form->setMax("am_lot_gar_achev", 1);
        $form->setMax("am_lot_vente_ant", 1);
        $form->setMax("am_empl_nb", 11);
        $form->setMax("am_tente_nb", 11);
        $form->setMax("am_carav_nb", 11);
        $form->setMax("am_mobil_nb", 11);
        $form->setMax("am_pers_nb", 11);
        $form->setMax("am_empl_hll_nb", 11);
        $form->setMax("am_hll_shon", -5);
        $form->setMax("am_periode_exploit", 6);
        $form->setMax("am_exist_agrand", 1);
        $form->setMax("am_exist_date", 12);
        $form->setMax("am_exist_num", 100);
        $form->setMax("am_exist_nb_avant", 11);
        $form->setMax("am_exist_nb_apres", 11);
        $form->setMax("am_coupe_bois", 1);
        $form->setMax("am_coupe_parc", 1);
        $form->setMax("am_coupe_align", 1);
        $form->setMax("am_coupe_ess", 100);
        $form->setMax("am_coupe_age", 15);
        $form->setMax("am_coupe_dens", 100);
        $form->setMax("am_coupe_qual", 100);
        $form->setMax("am_coupe_trait", 100);
        $form->setMax("am_coupe_autr", 100);
        $form->setMax("co_archi_recours", 1);
        $form->setMax("co_cstr_nouv", 1);
        $form->setMax("co_cstr_exist", 1);
        $form->setMax("co_cloture", 1);
        $form->setMax("co_elec_tension", -5);
        $form->setMax("co_div_terr", 1);
        $form->setMax("co_projet_desc", 6);
        $form->setMax("co_anx_pisc", 1);
        $form->setMax("co_anx_gara", 1);
        $form->setMax("co_anx_veran", 1);
        $form->setMax("co_anx_abri", 1);
        $form->setMax("co_anx_autr", 1);
        $form->setMax("co_anx_autr_desc", 6);
        $form->setMax("co_tot_log_nb", 11);
        $form->setMax("co_tot_ind_nb", 11);
        $form->setMax("co_tot_coll_nb", 11);
        $form->setMax("co_mais_piece_nb", 11);
        $form->setMax("co_mais_niv_nb", 11);
        $form->setMax("co_fin_lls_nb", 11);
        $form->setMax("co_fin_aa_nb", 11);
        $form->setMax("co_fin_ptz_nb", 11);
        $form->setMax("co_fin_autr_nb", 11);
        $form->setMax("co_fin_autr_desc", 6);
        $form->setMax("co_mais_contrat_ind", 1);
        $form->setMax("co_uti_pers", 1);
        $form->setMax("co_uti_vente", 1);
        $form->setMax("co_uti_loc", 1);
        $form->setMax("co_uti_princ", 1);
        $form->setMax("co_uti_secon", 1);
        $form->setMax("co_resid_agees", 1);
        $form->setMax("co_resid_etud", 1);
        $form->setMax("co_resid_tourism", 1);
        $form->setMax("co_resid_hot_soc", 1);
        $form->setMax("co_resid_soc", 1);
        $form->setMax("co_resid_hand", 1);
        $form->setMax("co_resid_autr", 1);
        $form->setMax("co_resid_autr_desc", 6);
        $form->setMax("co_foyer_chamb_nb", 11);
        $form->setMax("co_log_1p_nb", 11);
        $form->setMax("co_log_2p_nb", 11);
        $form->setMax("co_log_3p_nb", 11);
        $form->setMax("co_log_4p_nb", 11);
        $form->setMax("co_log_5p_nb", 11);
        $form->setMax("co_log_6p_nb", 11);
        $form->setMax("co_bat_niv_nb", 11);
        $form->setMax("co_trx_exten", 1);
        $form->setMax("co_trx_surelev", 1);
        $form->setMax("co_trx_nivsup", 1);
        $form->setMax("co_demont_periode", 6);
        $form->setMax("co_sp_transport", 1);
        $form->setMax("co_sp_enseign", 1);
        $form->setMax("co_sp_act_soc", 1);
        $form->setMax("co_sp_ouvr_spe", 1);
        $form->setMax("co_sp_sante", 1);
        $form->setMax("co_sp_culture", 1);
        $form->setMax("co_statio_avt_nb", 11);
        $form->setMax("co_statio_apr_nb", 11);
        $form->setMax("co_statio_adr", 6);
        $form->setMax("co_statio_place_nb", 11);
        $form->setMax("co_statio_tot_surf", -5);
        $form->setMax("co_statio_tot_shob", -5);
        $form->setMax("co_statio_comm_cin_surf", -5);
        $form->setMax("su_avt_shon1", -5);
        $form->setMax("su_avt_shon2", -5);
        $form->setMax("su_avt_shon3", -5);
        $form->setMax("su_avt_shon4", -5);
        $form->setMax("su_avt_shon5", -5);
        $form->setMax("su_avt_shon6", -5);
        $form->setMax("su_avt_shon7", -5);
        $form->setMax("su_avt_shon8", -5);
        $form->setMax("su_avt_shon9", -5);
        $form->setMax("su_cstr_shon1", -5);
        $form->setMax("su_cstr_shon2", -5);
        $form->setMax("su_cstr_shon3", -5);
        $form->setMax("su_cstr_shon4", -5);
        $form->setMax("su_cstr_shon5", -5);
        $form->setMax("su_cstr_shon6", -5);
        $form->setMax("su_cstr_shon7", -5);
        $form->setMax("su_cstr_shon8", -5);
        $form->setMax("su_cstr_shon9", -5);
        $form->setMax("su_trsf_shon1", -5);
        $form->setMax("su_trsf_shon2", -5);
        $form->setMax("su_trsf_shon3", -5);
        $form->setMax("su_trsf_shon4", -5);
        $form->setMax("su_trsf_shon5", -5);
        $form->setMax("su_trsf_shon6", -5);
        $form->setMax("su_trsf_shon7", -5);
        $form->setMax("su_trsf_shon8", -5);
        $form->setMax("su_trsf_shon9", -5);
        $form->setMax("su_chge_shon1", -5);
        $form->setMax("su_chge_shon2", -5);
        $form->setMax("su_chge_shon3", -5);
        $form->setMax("su_chge_shon4", -5);
        $form->setMax("su_chge_shon5", -5);
        $form->setMax("su_chge_shon6", -5);
        $form->setMax("su_chge_shon7", -5);
        $form->setMax("su_chge_shon8", -5);
        $form->setMax("su_chge_shon9", -5);
        $form->setMax("su_demo_shon1", -5);
        $form->setMax("su_demo_shon2", -5);
        $form->setMax("su_demo_shon3", -5);
        $form->setMax("su_demo_shon4", -5);
        $form->setMax("su_demo_shon5", -5);
        $form->setMax("su_demo_shon6", -5);
        $form->setMax("su_demo_shon7", -5);
        $form->setMax("su_demo_shon8", -5);
        $form->setMax("su_demo_shon9", -5);
        $form->setMax("su_sup_shon1", -5);
        $form->setMax("su_sup_shon2", -5);
        $form->setMax("su_sup_shon3", -5);
        $form->setMax("su_sup_shon4", -5);
        $form->setMax("su_sup_shon5", -5);
        $form->setMax("su_sup_shon6", -5);
        $form->setMax("su_sup_shon7", -5);
        $form->setMax("su_sup_shon8", -5);
        $form->setMax("su_sup_shon9", -5);
        $form->setMax("su_tot_shon1", -5);
        $form->setMax("su_tot_shon2", -5);
        $form->setMax("su_tot_shon3", -5);
        $form->setMax("su_tot_shon4", -5);
        $form->setMax("su_tot_shon5", -5);
        $form->setMax("su_tot_shon6", -5);
        $form->setMax("su_tot_shon7", -5);
        $form->setMax("su_tot_shon8", -5);
        $form->setMax("su_tot_shon9", -5);
        $form->setMax("su_avt_shon_tot", -5);
        $form->setMax("su_cstr_shon_tot", -5);
        $form->setMax("su_trsf_shon_tot", -5);
        $form->setMax("su_chge_shon_tot", -5);
        $form->setMax("su_demo_shon_tot", -5);
        $form->setMax("su_sup_shon_tot", -5);
        $form->setMax("su_tot_shon_tot", -5);
        $form->setMax("dm_constr_dates", 6);
        $form->setMax("dm_total", 1);
        $form->setMax("dm_partiel", 1);
        $form->setMax("dm_projet_desc", 6);
        $form->setMax("dm_tot_log_nb", 11);
        $form->setMax("tax_surf_tot", -5);
        $form->setMax("tax_surf", -5);
        $form->setMax("tax_surf_suppr_mod", -5);
        $form->setMax("tax_su_princ_log_nb1", -5);
        $form->setMax("tax_su_princ_log_nb2", -5);
        $form->setMax("tax_su_princ_log_nb3", -5);
        $form->setMax("tax_su_princ_log_nb4", -5);
        $form->setMax("tax_su_princ_log_nb_tot1", -5);
        $form->setMax("tax_su_princ_log_nb_tot2", -5);
        $form->setMax("tax_su_princ_log_nb_tot3", -5);
        $form->setMax("tax_su_princ_log_nb_tot4", -5);
        $form->setMax("tax_su_princ_surf1", -5);
        $form->setMax("tax_su_princ_surf2", -5);
        $form->setMax("tax_su_princ_surf3", -5);
        $form->setMax("tax_su_princ_surf4", -5);
        $form->setMax("tax_su_princ_surf_sup1", -5);
        $form->setMax("tax_su_princ_surf_sup2", -5);
        $form->setMax("tax_su_princ_surf_sup3", -5);
        $form->setMax("tax_su_princ_surf_sup4", -5);
        $form->setMax("tax_su_heber_log_nb1", 11);
        $form->setMax("tax_su_heber_log_nb2", 11);
        $form->setMax("tax_su_heber_log_nb3", 11);
        $form->setMax("tax_su_heber_log_nb_tot1", 11);
        $form->setMax("tax_su_heber_log_nb_tot2", 11);
        $form->setMax("tax_su_heber_log_nb_tot3", 11);
        $form->setMax("tax_su_heber_surf1", -5);
        $form->setMax("tax_su_heber_surf2", -5);
        $form->setMax("tax_su_heber_surf3", -5);
        $form->setMax("tax_su_heber_surf_sup1", -5);
        $form->setMax("tax_su_heber_surf_sup2", -5);
        $form->setMax("tax_su_heber_surf_sup3", -5);
        $form->setMax("tax_su_secon_log_nb", 11);
        $form->setMax("tax_su_tot_log_nb", 11);
        $form->setMax("tax_su_secon_log_nb_tot", 11);
        $form->setMax("tax_su_tot_log_nb_tot", 11);
        $form->setMax("tax_su_secon_surf", -5);
        $form->setMax("tax_su_tot_surf", -5);
        $form->setMax("tax_su_secon_surf_sup", -5);
        $form->setMax("tax_su_tot_surf_sup", -5);
        $form->setMax("tax_ext_pret", 1);
        $form->setMax("tax_ext_desc", 6);
        $form->setMax("tax_surf_tax_exist_cons", -5);
        $form->setMax("tax_log_exist_nb", 11);
        $form->setMax("tax_am_statio_ext", 11);
        $form->setMax("tax_sup_bass_pisc", -5);
        $form->setMax("tax_empl_ten_carav_mobil_nb", 11);
        $form->setMax("tax_empl_hll_nb", 11);
        $form->setMax("tax_eol_haut_nb", 11);
        $form->setMax("tax_pann_volt_sup", -5);
        $form->setMax("tax_am_statio_ext_sup", 11);
        $form->setMax("tax_sup_bass_pisc_sup", -5);
        $form->setMax("tax_empl_ten_carav_mobil_nb_sup", 11);
        $form->setMax("tax_empl_hll_nb_sup", 11);
        $form->setMax("tax_eol_haut_nb_sup", 11);
        $form->setMax("tax_pann_volt_sup_sup", -5);
        $form->setMax("tax_trx_presc_ppr", 1);
        $form->setMax("tax_monu_hist", 1);
        $form->setMax("tax_comm_nb", 11);
        $form->setMax("tax_su_non_habit_surf1", -5);
        $form->setMax("tax_su_non_habit_surf2", -5);
        $form->setMax("tax_su_non_habit_surf3", -5);
        $form->setMax("tax_su_non_habit_surf4", -5);
        $form->setMax("tax_su_non_habit_surf5", -5);
        $form->setMax("tax_su_non_habit_surf6", -5);
        $form->setMax("tax_su_non_habit_surf7", -5);
        $form->setMax("tax_su_non_habit_surf_sup1", -5);
        $form->setMax("tax_su_non_habit_surf_sup2", -5);
        $form->setMax("tax_su_non_habit_surf_sup3", -5);
        $form->setMax("tax_su_non_habit_surf_sup4", -5);
        $form->setMax("tax_su_non_habit_surf_sup5", -5);
        $form->setMax("tax_su_non_habit_surf_sup6", -5);
        $form->setMax("tax_su_non_habit_surf_sup7", -5);
        $form->setMax("vsd_surf_planch_smd", 1);
        $form->setMax("vsd_unit_fonc_sup", -5);
        $form->setMax("vsd_unit_fonc_constr_sup", -5);
        $form->setMax("vsd_val_terr", -5);
        $form->setMax("vsd_const_sxist_non_dem_surf", -5);
        $form->setMax("vsd_rescr_fisc", 12);
        $form->setMax("pld_val_terr", -5);
        $form->setMax("pld_const_exist_dem", 1);
        $form->setMax("pld_const_exist_dem_surf", -5);
        $form->setMax("code_cnil", 1);
        $form->setMax("terr_juri_titul", 20);
        $form->setMax("terr_juri_lot", 20);
        $form->setMax("terr_juri_zac", 20);
        $form->setMax("terr_juri_afu", 20);
        $form->setMax("terr_juri_pup", 20);
        $form->setMax("terr_juri_oin", 20);
        $form->setMax("terr_juri_desc", 6);
        $form->setMax("terr_div_surf_etab", -5);
        $form->setMax("terr_div_surf_av_div", -5);
        $form->setMax("doc_date", 12);
        $form->setMax("doc_tot_trav", 1);
        $form->setMax("doc_tranche_trav", 1);
        $form->setMax("doc_tranche_trav_desc", 6);
        $form->setMax("doc_surf", -5);
        $form->setMax("doc_nb_log", 11);
        $form->setMax("doc_nb_log_indiv", 11);
        $form->setMax("doc_nb_log_coll", 11);
        $form->setMax("doc_nb_log_lls", 11);
        $form->setMax("doc_nb_log_aa", 11);
        $form->setMax("doc_nb_log_ptz", 11);
        $form->setMax("doc_nb_log_autre", 11);
        $form->setMax("daact_date", 12);
        $form->setMax("daact_date_chgmt_dest", 12);
        $form->setMax("daact_tot_trav", 1);
        $form->setMax("daact_tranche_trav", 1);
        $form->setMax("daact_tranche_trav_desc", 6);
        $form->setMax("daact_surf", -5);
        $form->setMax("daact_nb_log", 11);
        $form->setMax("daact_nb_log_indiv", 11);
        $form->setMax("daact_nb_log_coll", 11);
        $form->setMax("daact_nb_log_lls", 11);
        $form->setMax("daact_nb_log_aa", 11);
        $form->setMax("daact_nb_log_ptz", 11);
        $form->setMax("daact_nb_log_autre", 11);
        $form->setMax("dossier_autorisation", 20);
        $form->setMax("am_div_mun", 1);
        $form->setMax("co_perf_energ", 40);
        $form->setMax("architecte", 11);
        $form->setMax("co_statio_avt_shob", 250);
        $form->setMax("co_statio_apr_shob", 250);
        $form->setMax("co_statio_avt_surf", 250);
        $form->setMax("co_statio_apr_surf", 250);
        $form->setMax("co_trx_amgt", 250);
        $form->setMax("co_modif_aspect", 250);
        $form->setMax("co_modif_struct", 250);
        $form->setMax("co_ouvr_elec", 1);
        $form->setMax("co_ouvr_infra", 1);
        $form->setMax("co_trx_imm", 250);
        $form->setMax("co_cstr_shob", 250);
        $form->setMax("am_voyage_deb", 250);
        $form->setMax("am_voyage_fin", 250);
        $form->setMax("am_modif_amgt", 250);
        $form->setMax("am_lot_max_shob", 250);
        $form->setMax("mod_desc", 250);
        $form->setMax("tr_total", 250);
        $form->setMax("tr_partiel", 250);
        $form->setMax("tr_desc", 250);
        $form->setMax("avap_co_elt_pro", 1);
        $form->setMax("avap_nouv_haut_surf", 1);
        $form->setMax("avap_co_clot", 250);
        $form->setMax("avap_aut_coup_aba_arb", 250);
        $form->setMax("avap_ouv_infra", 250);
        $form->setMax("avap_aut_inst_mob", 250);
        $form->setMax("avap_aut_plant", 250);
        $form->setMax("avap_aut_auv_elec", 250);
        $form->setMax("tax_dest_loc_tr", 250);
        $form->setMax("ope_proj_desc", 6);
        $form->setMax("tax_surf_tot_cstr", 11);
        $form->setMax("cerfa", 11);
        $form->setMax("tax_surf_loc_stat", 11);
        $form->setMax("tax_su_princ_surf_stat1", -5);
        $form->setMax("tax_su_princ_surf_stat2", -5);
        $form->setMax("tax_su_princ_surf_stat3", -5);
        $form->setMax("tax_su_princ_surf_stat4", -5);
        $form->setMax("tax_su_secon_surf_stat", -5);
        $form->setMax("tax_su_heber_surf_stat1", -5);
        $form->setMax("tax_su_heber_surf_stat2", -5);
        $form->setMax("tax_su_heber_surf_stat3", -5);
        $form->setMax("tax_su_tot_surf_stat", -5);
        $form->setMax("tax_su_non_habit_surf_stat1", -5);
        $form->setMax("tax_su_non_habit_surf_stat2", -5);
        $form->setMax("tax_su_non_habit_surf_stat3", -5);
        $form->setMax("tax_su_non_habit_surf_stat4", -5);
        $form->setMax("tax_su_non_habit_surf_stat5", -5);
        $form->setMax("tax_su_non_habit_surf_stat6", -5);
        $form->setMax("tax_su_non_habit_surf_stat7", -5);
        $form->setMax("tax_su_parc_statio_expl_comm_surf", -5);
        $form->setMax("tax_log_ap_trvx_nb", 11);
        $form->setMax("tax_am_statio_ext_cr", 11);
        $form->setMax("tax_sup_bass_pisc_cr", -5);
        $form->setMax("tax_empl_ten_carav_mobil_nb_cr", 11);
        $form->setMax("tax_empl_hll_nb_cr", 11);
        $form->setMax("tax_eol_haut_nb_cr", 11);
        $form->setMax("tax_pann_volt_sup_cr", -5);
        $form->setMax("tax_surf_loc_arch", -5);
        $form->setMax("tax_surf_pisc_arch", -5);
        $form->setMax("tax_am_statio_ext_arch", -5);
        $form->setMax("tax_empl_ten_carav_mobil_nb_arch", -5);
        $form->setMax("tax_empl_hll_nb_arch", -5);
        $form->setMax("tax_eol_haut_nb_arch", 11);
        $form->setMax("ope_proj_div_co", 1);
        $form->setMax("ope_proj_div_contr", 1);
        $form->setMax("tax_desc", 6);
        $form->setMax("erp_cstr_neuve", 1);
        $form->setMax("erp_trvx_acc", 1);
        $form->setMax("erp_extension", 1);
        $form->setMax("erp_rehab", 1);
        $form->setMax("erp_trvx_am", 1);
        $form->setMax("erp_vol_nouv_exist", 1);
        $form->setMax("erp_loc_eff1", -5);
        $form->setMax("erp_loc_eff2", -5);
        $form->setMax("erp_loc_eff3", -5);
        $form->setMax("erp_loc_eff4", -5);
        $form->setMax("erp_loc_eff5", -5);
        $form->setMax("erp_loc_eff_tot", -5);
        $form->setMax("erp_public_eff1", -5);
        $form->setMax("erp_public_eff2", -5);
        $form->setMax("erp_public_eff3", -5);
        $form->setMax("erp_public_eff4", -5);
        $form->setMax("erp_public_eff5", -5);
        $form->setMax("erp_public_eff_tot", -5);
        $form->setMax("erp_perso_eff1", -5);
        $form->setMax("erp_perso_eff2", -5);
        $form->setMax("erp_perso_eff3", -5);
        $form->setMax("erp_perso_eff4", -5);
        $form->setMax("erp_perso_eff5", -5);
        $form->setMax("erp_perso_eff_tot", -5);
        $form->setMax("erp_tot_eff1", -5);
        $form->setMax("erp_tot_eff2", -5);
        $form->setMax("erp_tot_eff3", -5);
        $form->setMax("erp_tot_eff4", -5);
        $form->setMax("erp_tot_eff5", -5);
        $form->setMax("erp_tot_eff_tot", -5);
        $form->setMax("erp_class_cat", 11);
        $form->setMax("erp_class_type", 11);
        $form->setMax("tax_surf_abr_jard_pig_colom", -5);
        $form->setMax("tax_su_non_habit_abr_jard_pig_colom", -5);
        $form->setMax("dia_imm_non_bati", 1);
        $form->setMax("dia_imm_bati_terr_propr", 1);
        $form->setMax("dia_imm_bati_terr_autr", 1);
        $form->setMax("dia_imm_bati_terr_autr_desc", 6);
        $form->setMax("dia_bat_copro", 1);
        $form->setMax("dia_bat_copro_desc", 6);
        $form->setMax("dia_lot_numero", 6);
        $form->setMax("dia_lot_bat", 6);
        $form->setMax("dia_lot_etage", 6);
        $form->setMax("dia_lot_quote_part", 6);
        $form->setMax("dia_us_hab", 1);
        $form->setMax("dia_us_pro", 1);
        $form->setMax("dia_us_mixte", 1);
        $form->setMax("dia_us_comm", 1);
        $form->setMax("dia_us_agr", 1);
        $form->setMax("dia_us_autre", 1);
        $form->setMax("dia_us_autre_prec", 6);
        $form->setMax("dia_occ_prop", 1);
        $form->setMax("dia_occ_loc", 1);
        $form->setMax("dia_occ_sans_occ", 1);
        $form->setMax("dia_occ_autre", 1);
        $form->setMax("dia_occ_autre_prec", 6);
        $form->setMax("dia_mod_cess_prix_vente", 250);
        $form->setMax("dia_mod_cess_prix_vente_mob", 250);
        $form->setMax("dia_mod_cess_prix_vente_cheptel", 250);
        $form->setMax("dia_mod_cess_prix_vente_recol", 250);
        $form->setMax("dia_mod_cess_prix_vente_autre", 250);
        $form->setMax("dia_mod_cess_commi", 1);
        $form->setMax("dia_mod_cess_commi_ttc", 250);
        $form->setMax("dia_mod_cess_commi_ht", 250);
        $form->setMax("dia_acquereur_nom_prenom", 150);
        $form->setMax("dia_acquereur_adr_num_voie", 10);
        $form->setMax("dia_acquereur_adr_ext", 55);
        $form->setMax("dia_acquereur_adr_type_voie", 20);
        $form->setMax("dia_acquereur_adr_nom_voie", 55);
        $form->setMax("dia_acquereur_adr_lieu_dit_bp", 39);
        $form->setMax("dia_acquereur_adr_cp", 5);
        $form->setMax("dia_acquereur_adr_localite", 250);
        $form->setMax("dia_observation", 6);
        $form->setMax("su2_avt_shon1", -5);
        $form->setMax("su2_avt_shon2", -5);
        $form->setMax("su2_avt_shon3", -5);
        $form->setMax("su2_avt_shon4", -5);
        $form->setMax("su2_avt_shon5", -5);
        $form->setMax("su2_avt_shon6", -5);
        $form->setMax("su2_avt_shon7", -5);
        $form->setMax("su2_avt_shon8", -5);
        $form->setMax("su2_avt_shon9", -5);
        $form->setMax("su2_avt_shon10", -5);
        $form->setMax("su2_avt_shon11", -5);
        $form->setMax("su2_avt_shon12", -5);
        $form->setMax("su2_avt_shon13", -5);
        $form->setMax("su2_avt_shon14", -5);
        $form->setMax("su2_avt_shon15", -5);
        $form->setMax("su2_avt_shon16", -5);
        $form->setMax("su2_avt_shon17", -5);
        $form->setMax("su2_avt_shon18", -5);
        $form->setMax("su2_avt_shon19", -5);
        $form->setMax("su2_avt_shon20", -5);
        $form->setMax("su2_avt_shon_tot", -5);
        $form->setMax("su2_cstr_shon1", -5);
        $form->setMax("su2_cstr_shon2", -5);
        $form->setMax("su2_cstr_shon3", -5);
        $form->setMax("su2_cstr_shon4", -5);
        $form->setMax("su2_cstr_shon5", -5);
        $form->setMax("su2_cstr_shon6", -5);
        $form->setMax("su2_cstr_shon7", -5);
        $form->setMax("su2_cstr_shon8", -5);
        $form->setMax("su2_cstr_shon9", -5);
        $form->setMax("su2_cstr_shon10", -5);
        $form->setMax("su2_cstr_shon11", -5);
        $form->setMax("su2_cstr_shon12", -5);
        $form->setMax("su2_cstr_shon13", -5);
        $form->setMax("su2_cstr_shon14", -5);
        $form->setMax("su2_cstr_shon15", -5);
        $form->setMax("su2_cstr_shon16", -5);
        $form->setMax("su2_cstr_shon17", -5);
        $form->setMax("su2_cstr_shon18", -5);
        $form->setMax("su2_cstr_shon19", -5);
        $form->setMax("su2_cstr_shon20", -5);
        $form->setMax("su2_cstr_shon_tot", -5);
        $form->setMax("su2_chge_shon1", -5);
        $form->setMax("su2_chge_shon2", -5);
        $form->setMax("su2_chge_shon3", -5);
        $form->setMax("su2_chge_shon4", -5);
        $form->setMax("su2_chge_shon5", -5);
        $form->setMax("su2_chge_shon6", -5);
        $form->setMax("su2_chge_shon7", -5);
        $form->setMax("su2_chge_shon8", -5);
        $form->setMax("su2_chge_shon9", -5);
        $form->setMax("su2_chge_shon10", -5);
        $form->setMax("su2_chge_shon11", -5);
        $form->setMax("su2_chge_shon12", -5);
        $form->setMax("su2_chge_shon13", -5);
        $form->setMax("su2_chge_shon14", -5);
        $form->setMax("su2_chge_shon15", -5);
        $form->setMax("su2_chge_shon16", -5);
        $form->setMax("su2_chge_shon17", -5);
        $form->setMax("su2_chge_shon18", -5);
        $form->setMax("su2_chge_shon19", -5);
        $form->setMax("su2_chge_shon20", -5);
        $form->setMax("su2_chge_shon_tot", -5);
        $form->setMax("su2_demo_shon1", -5);
        $form->setMax("su2_demo_shon2", -5);
        $form->setMax("su2_demo_shon3", -5);
        $form->setMax("su2_demo_shon4", -5);
        $form->setMax("su2_demo_shon5", -5);
        $form->setMax("su2_demo_shon6", -5);
        $form->setMax("su2_demo_shon7", -5);
        $form->setMax("su2_demo_shon8", -5);
        $form->setMax("su2_demo_shon9", -5);
        $form->setMax("su2_demo_shon10", -5);
        $form->setMax("su2_demo_shon11", -5);
        $form->setMax("su2_demo_shon12", -5);
        $form->setMax("su2_demo_shon13", -5);
        $form->setMax("su2_demo_shon14", -5);
        $form->setMax("su2_demo_shon15", -5);
        $form->setMax("su2_demo_shon16", -5);
        $form->setMax("su2_demo_shon17", -5);
        $form->setMax("su2_demo_shon18", -5);
        $form->setMax("su2_demo_shon19", -5);
        $form->setMax("su2_demo_shon20", -5);
        $form->setMax("su2_demo_shon_tot", -5);
        $form->setMax("su2_sup_shon1", -5);
        $form->setMax("su2_sup_shon2", -5);
        $form->setMax("su2_sup_shon3", -5);
        $form->setMax("su2_sup_shon4", -5);
        $form->setMax("su2_sup_shon5", -5);
        $form->setMax("su2_sup_shon6", -5);
        $form->setMax("su2_sup_shon7", -5);
        $form->setMax("su2_sup_shon8", -5);
        $form->setMax("su2_sup_shon9", -5);
        $form->setMax("su2_sup_shon10", -5);
        $form->setMax("su2_sup_shon11", -5);
        $form->setMax("su2_sup_shon12", -5);
        $form->setMax("su2_sup_shon13", -5);
        $form->setMax("su2_sup_shon14", -5);
        $form->setMax("su2_sup_shon15", -5);
        $form->setMax("su2_sup_shon16", -5);
        $form->setMax("su2_sup_shon17", -5);
        $form->setMax("su2_sup_shon18", -5);
        $form->setMax("su2_sup_shon19", -5);
        $form->setMax("su2_sup_shon20", -5);
        $form->setMax("su2_sup_shon_tot", -5);
        $form->setMax("su2_tot_shon1", -5);
        $form->setMax("su2_tot_shon2", -5);
        $form->setMax("su2_tot_shon3", -5);
        $form->setMax("su2_tot_shon4", -5);
        $form->setMax("su2_tot_shon5", -5);
        $form->setMax("su2_tot_shon6", -5);
        $form->setMax("su2_tot_shon7", -5);
        $form->setMax("su2_tot_shon8", -5);
        $form->setMax("su2_tot_shon9", -5);
        $form->setMax("su2_tot_shon10", -5);
        $form->setMax("su2_tot_shon11", -5);
        $form->setMax("su2_tot_shon12", -5);
        $form->setMax("su2_tot_shon13", -5);
        $form->setMax("su2_tot_shon14", -5);
        $form->setMax("su2_tot_shon15", -5);
        $form->setMax("su2_tot_shon16", -5);
        $form->setMax("su2_tot_shon17", -5);
        $form->setMax("su2_tot_shon18", -5);
        $form->setMax("su2_tot_shon19", -5);
        $form->setMax("su2_tot_shon20", -5);
        $form->setMax("su2_tot_shon_tot", -5);
        $form->setMax("dia_occ_sol_su_terre", 6);
        $form->setMax("dia_occ_sol_su_pres", 6);
        $form->setMax("dia_occ_sol_su_verger", 6);
        $form->setMax("dia_occ_sol_su_vigne", 6);
        $form->setMax("dia_occ_sol_su_bois", 6);
        $form->setMax("dia_occ_sol_su_lande", 6);
        $form->setMax("dia_occ_sol_su_carriere", 6);
        $form->setMax("dia_occ_sol_su_eau_cadastree", 6);
        $form->setMax("dia_occ_sol_su_jardin", 6);
        $form->setMax("dia_occ_sol_su_terr_batir", 6);
        $form->setMax("dia_occ_sol_su_terr_agr", 6);
        $form->setMax("dia_occ_sol_su_sol", 6);
        $form->setMax("dia_bati_vend_tot", 1);
        $form->setMax("dia_bati_vend_tot_txt", 6);
        $form->setMax("dia_su_co_sol", 6);
        $form->setMax("dia_su_util_hab", 6);
        $form->setMax("dia_nb_niv", 6);
        $form->setMax("dia_nb_appart", 6);
        $form->setMax("dia_nb_autre_loc", 6);
        $form->setMax("dia_vente_lot_volume", 1);
        $form->setMax("dia_vente_lot_volume_txt", 6);
        $form->setMax("dia_lot_nat_su", 6);
        $form->setMax("dia_lot_bat_achv_plus_10", 1);
        $form->setMax("dia_lot_bat_achv_moins_10", 1);
        $form->setMax("dia_lot_regl_copro_publ_hypo_plus_10", 1);
        $form->setMax("dia_lot_regl_copro_publ_hypo_moins_10", 1);
        $form->setMax("dia_indivi_quote_part", 6);
        $form->setMax("dia_design_societe", 6);
        $form->setMax("dia_design_droit", 6);
        $form->setMax("dia_droit_soc_nat", 6);
        $form->setMax("dia_droit_soc_nb", 6);
        $form->setMax("dia_droit_soc_num_part", 6);
        $form->setMax("dia_droit_reel_perso_grevant_bien_oui", 1);
        $form->setMax("dia_droit_reel_perso_grevant_bien_non", 1);
        $form->setMax("dia_droit_reel_perso_nat", 6);
        $form->setMax("dia_droit_reel_perso_viag", 6);
        $form->setMax("dia_mod_cess_adr", 6);
        $form->setMax("dia_mod_cess_sign_act_auth", 1);
        $form->setMax("dia_mod_cess_terme", 1);
        $form->setMax("dia_mod_cess_terme_prec", 6);
        $form->setMax("dia_mod_cess_bene_acquereur", 1);
        $form->setMax("dia_mod_cess_bene_vendeur", 1);
        $form->setMax("dia_mod_cess_paie_nat", 1);
        $form->setMax("dia_mod_cess_design_contr_alien", 6);
        $form->setMax("dia_mod_cess_eval_contr", 6);
        $form->setMax("dia_mod_cess_rente_viag", 1);
        $form->setMax("dia_mod_cess_mnt_an", 6);
        $form->setMax("dia_mod_cess_mnt_compt", 6);
        $form->setMax("dia_mod_cess_bene_rente", 6);
        $form->setMax("dia_mod_cess_droit_usa_hab", 1);
        $form->setMax("dia_mod_cess_droit_usa_hab_prec", 6);
        $form->setMax("dia_mod_cess_eval_usa_usufruit", 6);
        $form->setMax("dia_mod_cess_vente_nue_prop", 1);
        $form->setMax("dia_mod_cess_vente_nue_prop_prec", 6);
        $form->setMax("dia_mod_cess_echange", 1);
        $form->setMax("dia_mod_cess_design_bien_recus_ech", 6);
        $form->setMax("dia_mod_cess_mnt_soulte", 6);
        $form->setMax("dia_mod_cess_prop_contre_echan", 6);
        $form->setMax("dia_mod_cess_apport_societe", 6);
        $form->setMax("dia_mod_cess_bene", 6);
        $form->setMax("dia_mod_cess_esti_bien", 6);
        $form->setMax("dia_mod_cess_cess_terr_loc_co", 1);
        $form->setMax("dia_mod_cess_esti_terr", 6);
        $form->setMax("dia_mod_cess_esti_loc", 6);
        $form->setMax("dia_mod_cess_esti_imm_loca", 1);
        $form->setMax("dia_mod_cess_adju_vol", 1);
        $form->setMax("dia_mod_cess_adju_obl", 1);
        $form->setMax("dia_mod_cess_adju_fin_indivi", 1);
        $form->setMax("dia_mod_cess_adju_date_lieu", 6);
        $form->setMax("dia_mod_cess_mnt_mise_prix", 6);
        $form->setMax("dia_prop_titu_prix_indique", 1);
        $form->setMax("dia_prop_recherche_acqu_prix_indique", 1);
        $form->setMax("dia_acquereur_prof", 6);
        $form->setMax("dia_indic_compl_ope", 6);
        $form->setMax("dia_vente_adju", 1);
        $form->setMax("am_terr_res_demon", 1);
        $form->setMax("am_air_terr_res_mob", 1);
        $form->setMax("ctx_objet_recours", 11);
        $form->setMax("ctx_reference_sagace", 255);
        $form->setMax("ctx_nature_travaux_infra_om_html", 6);
        $form->setMax("ctx_synthese_nti", 6);
        $form->setMax("ctx_article_non_resp_om_html", 6);
        $form->setMax("ctx_synthese_anr", 6);
        $form->setMax("ctx_reference_parquet", 255);
        $form->setMax("ctx_element_taxation", 255);
        $form->setMax("ctx_infraction", 1);
        $form->setMax("ctx_regularisable", 1);
        $form->setMax("ctx_reference_courrier", 255);
        $form->setMax("ctx_date_audience", 12);
        $form->setMax("ctx_date_ajournement", 12);
        $form->setMax("exo_facul_1", 1);
        $form->setMax("exo_facul_2", 1);
        $form->setMax("exo_facul_3", 1);
        $form->setMax("exo_facul_4", 1);
        $form->setMax("exo_facul_5", 1);
        $form->setMax("exo_facul_6", 1);
        $form->setMax("exo_facul_7", 1);
        $form->setMax("exo_facul_8", 1);
        $form->setMax("exo_facul_9", 1);
        $form->setMax("exo_ta_1", 1);
        $form->setMax("exo_ta_2", 1);
        $form->setMax("exo_ta_3", 1);
        $form->setMax("exo_ta_4", 1);
        $form->setMax("exo_ta_5", 1);
        $form->setMax("exo_ta_6", 1);
        $form->setMax("exo_ta_7", 1);
        $form->setMax("exo_ta_8", 1);
        $form->setMax("exo_ta_9", 1);
        $form->setMax("exo_rap_1", 1);
        $form->setMax("exo_rap_2", 1);
        $form->setMax("exo_rap_3", 1);
        $form->setMax("exo_rap_4", 1);
        $form->setMax("exo_rap_5", 1);
        $form->setMax("exo_rap_6", 1);
        $form->setMax("exo_rap_7", 1);
        $form->setMax("exo_rap_8", 1);
        $form->setMax("mtn_exo_ta_part_commu", -5);
        $form->setMax("mtn_exo_ta_part_depart", -5);
        $form->setMax("mtn_exo_ta_part_reg", -5);
        $form->setMax("mtn_exo_rap", -5);
        $form->setMax("dpc_type", 100);
        $form->setMax("dpc_desc_actv_ex", 6);
        $form->setMax("dpc_desc_ca", 6);
        $form->setMax("dpc_desc_aut_prec", 6);
        $form->setMax("dpc_desig_comm_arti", 1);
        $form->setMax("dpc_desig_loc_hab", 1);
        $form->setMax("dpc_desig_loc_ann", 1);
        $form->setMax("dpc_desig_loc_ann_prec", 6);
        $form->setMax("dpc_bail_comm_date", 12);
        $form->setMax("dpc_bail_comm_loyer", 6);
        $form->setMax("dpc_actv_acqu", 6);
        $form->setMax("dpc_nb_sala_di", 6);
        $form->setMax("dpc_nb_sala_dd", 6);
        $form->setMax("dpc_nb_sala_tc", 6);
        $form->setMax("dpc_nb_sala_tp", 6);
        $form->setMax("dpc_moda_cess_vente_am", 1);
        $form->setMax("dpc_moda_cess_adj", 1);
        $form->setMax("dpc_moda_cess_prix", 6);
        $form->setMax("dpc_moda_cess_adj_date", 12);
        $form->setMax("dpc_moda_cess_adj_prec", 6);
        $form->setMax("dpc_moda_cess_paie_comp", 1);
        $form->setMax("dpc_moda_cess_paie_terme", 1);
        $form->setMax("dpc_moda_cess_paie_terme_prec", 6);
        $form->setMax("dpc_moda_cess_paie_nat", 1);
        $form->setMax("dpc_moda_cess_paie_nat_desig_alien", 1);
        $form->setMax("dpc_moda_cess_paie_nat_desig_alien_prec", 6);
        $form->setMax("dpc_moda_cess_paie_nat_eval", 1);
        $form->setMax("dpc_moda_cess_paie_nat_eval_prec", 6);
        $form->setMax("dpc_moda_cess_paie_aut", 1);
        $form->setMax("dpc_moda_cess_paie_aut_prec", 6);
        $form->setMax("dpc_ss_signe_demande_acqu", 1);
        $form->setMax("dpc_ss_signe_recher_trouv_acqu", 1);
        $form->setMax("dpc_notif_adr_prop", 1);
        $form->setMax("dpc_notif_adr_manda", 1);
        $form->setMax("dpc_obs", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('donnees_techniques',_('donnees_techniques'));
        $form->setLib('dossier_instruction',_('dossier_instruction'));
        $form->setLib('lot',_('lot'));
        $form->setLib('am_lotiss',_('am_lotiss'));
        $form->setLib('am_autre_div',_('am_autre_div'));
        $form->setLib('am_camping',_('am_camping'));
        $form->setLib('am_caravane',_('am_caravane'));
        $form->setLib('am_carav_duree',_('am_carav_duree'));
        $form->setLib('am_statio',_('am_statio'));
        $form->setLib('am_statio_cont',_('am_statio_cont'));
        $form->setLib('am_affou_exhau',_('am_affou_exhau'));
        $form->setLib('am_affou_exhau_sup',_('am_affou_exhau_sup'));
        $form->setLib('am_affou_prof',_('am_affou_prof'));
        $form->setLib('am_exhau_haut',_('am_exhau_haut'));
        $form->setLib('am_coupe_abat',_('am_coupe_abat'));
        $form->setLib('am_prot_plu',_('am_prot_plu'));
        $form->setLib('am_prot_muni',_('am_prot_muni'));
        $form->setLib('am_mobil_voyage',_('am_mobil_voyage'));
        $form->setLib('am_aire_voyage',_('am_aire_voyage'));
        $form->setLib('am_rememb_afu',_('am_rememb_afu'));
        $form->setLib('am_parc_resid_loi',_('am_parc_resid_loi'));
        $form->setLib('am_sport_moto',_('am_sport_moto'));
        $form->setLib('am_sport_attrac',_('am_sport_attrac'));
        $form->setLib('am_sport_golf',_('am_sport_golf'));
        $form->setLib('am_mob_art',_('am_mob_art'));
        $form->setLib('am_modif_voie_esp',_('am_modif_voie_esp'));
        $form->setLib('am_plant_voie_esp',_('am_plant_voie_esp'));
        $form->setLib('am_chem_ouv_esp',_('am_chem_ouv_esp'));
        $form->setLib('am_agri_peche',_('am_agri_peche'));
        $form->setLib('am_crea_voie',_('am_crea_voie'));
        $form->setLib('am_modif_voie_exist',_('am_modif_voie_exist'));
        $form->setLib('am_crea_esp_sauv',_('am_crea_esp_sauv'));
        $form->setLib('am_crea_esp_class',_('am_crea_esp_class'));
        $form->setLib('am_projet_desc',_('am_projet_desc'));
        $form->setLib('am_terr_surf',_('am_terr_surf'));
        $form->setLib('am_tranche_desc',_('am_tranche_desc'));
        $form->setLib('am_lot_max_nb',_('am_lot_max_nb'));
        $form->setLib('am_lot_max_shon',_('am_lot_max_shon'));
        $form->setLib('am_lot_cstr_cos',_('am_lot_cstr_cos'));
        $form->setLib('am_lot_cstr_plan',_('am_lot_cstr_plan'));
        $form->setLib('am_lot_cstr_vente',_('am_lot_cstr_vente'));
        $form->setLib('am_lot_fin_diff',_('am_lot_fin_diff'));
        $form->setLib('am_lot_consign',_('am_lot_consign'));
        $form->setLib('am_lot_gar_achev',_('am_lot_gar_achev'));
        $form->setLib('am_lot_vente_ant',_('am_lot_vente_ant'));
        $form->setLib('am_empl_nb',_('am_empl_nb'));
        $form->setLib('am_tente_nb',_('am_tente_nb'));
        $form->setLib('am_carav_nb',_('am_carav_nb'));
        $form->setLib('am_mobil_nb',_('am_mobil_nb'));
        $form->setLib('am_pers_nb',_('am_pers_nb'));
        $form->setLib('am_empl_hll_nb',_('am_empl_hll_nb'));
        $form->setLib('am_hll_shon',_('am_hll_shon'));
        $form->setLib('am_periode_exploit',_('am_periode_exploit'));
        $form->setLib('am_exist_agrand',_('am_exist_agrand'));
        $form->setLib('am_exist_date',_('am_exist_date'));
        $form->setLib('am_exist_num',_('am_exist_num'));
        $form->setLib('am_exist_nb_avant',_('am_exist_nb_avant'));
        $form->setLib('am_exist_nb_apres',_('am_exist_nb_apres'));
        $form->setLib('am_coupe_bois',_('am_coupe_bois'));
        $form->setLib('am_coupe_parc',_('am_coupe_parc'));
        $form->setLib('am_coupe_align',_('am_coupe_align'));
        $form->setLib('am_coupe_ess',_('am_coupe_ess'));
        $form->setLib('am_coupe_age',_('am_coupe_age'));
        $form->setLib('am_coupe_dens',_('am_coupe_dens'));
        $form->setLib('am_coupe_qual',_('am_coupe_qual'));
        $form->setLib('am_coupe_trait',_('am_coupe_trait'));
        $form->setLib('am_coupe_autr',_('am_coupe_autr'));
        $form->setLib('co_archi_recours',_('co_archi_recours'));
        $form->setLib('co_cstr_nouv',_('co_cstr_nouv'));
        $form->setLib('co_cstr_exist',_('co_cstr_exist'));
        $form->setLib('co_cloture',_('co_cloture'));
        $form->setLib('co_elec_tension',_('co_elec_tension'));
        $form->setLib('co_div_terr',_('co_div_terr'));
        $form->setLib('co_projet_desc',_('co_projet_desc'));
        $form->setLib('co_anx_pisc',_('co_anx_pisc'));
        $form->setLib('co_anx_gara',_('co_anx_gara'));
        $form->setLib('co_anx_veran',_('co_anx_veran'));
        $form->setLib('co_anx_abri',_('co_anx_abri'));
        $form->setLib('co_anx_autr',_('co_anx_autr'));
        $form->setLib('co_anx_autr_desc',_('co_anx_autr_desc'));
        $form->setLib('co_tot_log_nb',_('co_tot_log_nb'));
        $form->setLib('co_tot_ind_nb',_('co_tot_ind_nb'));
        $form->setLib('co_tot_coll_nb',_('co_tot_coll_nb'));
        $form->setLib('co_mais_piece_nb',_('co_mais_piece_nb'));
        $form->setLib('co_mais_niv_nb',_('co_mais_niv_nb'));
        $form->setLib('co_fin_lls_nb',_('co_fin_lls_nb'));
        $form->setLib('co_fin_aa_nb',_('co_fin_aa_nb'));
        $form->setLib('co_fin_ptz_nb',_('co_fin_ptz_nb'));
        $form->setLib('co_fin_autr_nb',_('co_fin_autr_nb'));
        $form->setLib('co_fin_autr_desc',_('co_fin_autr_desc'));
        $form->setLib('co_mais_contrat_ind',_('co_mais_contrat_ind'));
        $form->setLib('co_uti_pers',_('co_uti_pers'));
        $form->setLib('co_uti_vente',_('co_uti_vente'));
        $form->setLib('co_uti_loc',_('co_uti_loc'));
        $form->setLib('co_uti_princ',_('co_uti_princ'));
        $form->setLib('co_uti_secon',_('co_uti_secon'));
        $form->setLib('co_resid_agees',_('co_resid_agees'));
        $form->setLib('co_resid_etud',_('co_resid_etud'));
        $form->setLib('co_resid_tourism',_('co_resid_tourism'));
        $form->setLib('co_resid_hot_soc',_('co_resid_hot_soc'));
        $form->setLib('co_resid_soc',_('co_resid_soc'));
        $form->setLib('co_resid_hand',_('co_resid_hand'));
        $form->setLib('co_resid_autr',_('co_resid_autr'));
        $form->setLib('co_resid_autr_desc',_('co_resid_autr_desc'));
        $form->setLib('co_foyer_chamb_nb',_('co_foyer_chamb_nb'));
        $form->setLib('co_log_1p_nb',_('co_log_1p_nb'));
        $form->setLib('co_log_2p_nb',_('co_log_2p_nb'));
        $form->setLib('co_log_3p_nb',_('co_log_3p_nb'));
        $form->setLib('co_log_4p_nb',_('co_log_4p_nb'));
        $form->setLib('co_log_5p_nb',_('co_log_5p_nb'));
        $form->setLib('co_log_6p_nb',_('co_log_6p_nb'));
        $form->setLib('co_bat_niv_nb',_('co_bat_niv_nb'));
        $form->setLib('co_trx_exten',_('co_trx_exten'));
        $form->setLib('co_trx_surelev',_('co_trx_surelev'));
        $form->setLib('co_trx_nivsup',_('co_trx_nivsup'));
        $form->setLib('co_demont_periode',_('co_demont_periode'));
        $form->setLib('co_sp_transport',_('co_sp_transport'));
        $form->setLib('co_sp_enseign',_('co_sp_enseign'));
        $form->setLib('co_sp_act_soc',_('co_sp_act_soc'));
        $form->setLib('co_sp_ouvr_spe',_('co_sp_ouvr_spe'));
        $form->setLib('co_sp_sante',_('co_sp_sante'));
        $form->setLib('co_sp_culture',_('co_sp_culture'));
        $form->setLib('co_statio_avt_nb',_('co_statio_avt_nb'));
        $form->setLib('co_statio_apr_nb',_('co_statio_apr_nb'));
        $form->setLib('co_statio_adr',_('co_statio_adr'));
        $form->setLib('co_statio_place_nb',_('co_statio_place_nb'));
        $form->setLib('co_statio_tot_surf',_('co_statio_tot_surf'));
        $form->setLib('co_statio_tot_shob',_('co_statio_tot_shob'));
        $form->setLib('co_statio_comm_cin_surf',_('co_statio_comm_cin_surf'));
        $form->setLib('su_avt_shon1',_('su_avt_shon1'));
        $form->setLib('su_avt_shon2',_('su_avt_shon2'));
        $form->setLib('su_avt_shon3',_('su_avt_shon3'));
        $form->setLib('su_avt_shon4',_('su_avt_shon4'));
        $form->setLib('su_avt_shon5',_('su_avt_shon5'));
        $form->setLib('su_avt_shon6',_('su_avt_shon6'));
        $form->setLib('su_avt_shon7',_('su_avt_shon7'));
        $form->setLib('su_avt_shon8',_('su_avt_shon8'));
        $form->setLib('su_avt_shon9',_('su_avt_shon9'));
        $form->setLib('su_cstr_shon1',_('su_cstr_shon1'));
        $form->setLib('su_cstr_shon2',_('su_cstr_shon2'));
        $form->setLib('su_cstr_shon3',_('su_cstr_shon3'));
        $form->setLib('su_cstr_shon4',_('su_cstr_shon4'));
        $form->setLib('su_cstr_shon5',_('su_cstr_shon5'));
        $form->setLib('su_cstr_shon6',_('su_cstr_shon6'));
        $form->setLib('su_cstr_shon7',_('su_cstr_shon7'));
        $form->setLib('su_cstr_shon8',_('su_cstr_shon8'));
        $form->setLib('su_cstr_shon9',_('su_cstr_shon9'));
        $form->setLib('su_trsf_shon1',_('su_trsf_shon1'));
        $form->setLib('su_trsf_shon2',_('su_trsf_shon2'));
        $form->setLib('su_trsf_shon3',_('su_trsf_shon3'));
        $form->setLib('su_trsf_shon4',_('su_trsf_shon4'));
        $form->setLib('su_trsf_shon5',_('su_trsf_shon5'));
        $form->setLib('su_trsf_shon6',_('su_trsf_shon6'));
        $form->setLib('su_trsf_shon7',_('su_trsf_shon7'));
        $form->setLib('su_trsf_shon8',_('su_trsf_shon8'));
        $form->setLib('su_trsf_shon9',_('su_trsf_shon9'));
        $form->setLib('su_chge_shon1',_('su_chge_shon1'));
        $form->setLib('su_chge_shon2',_('su_chge_shon2'));
        $form->setLib('su_chge_shon3',_('su_chge_shon3'));
        $form->setLib('su_chge_shon4',_('su_chge_shon4'));
        $form->setLib('su_chge_shon5',_('su_chge_shon5'));
        $form->setLib('su_chge_shon6',_('su_chge_shon6'));
        $form->setLib('su_chge_shon7',_('su_chge_shon7'));
        $form->setLib('su_chge_shon8',_('su_chge_shon8'));
        $form->setLib('su_chge_shon9',_('su_chge_shon9'));
        $form->setLib('su_demo_shon1',_('su_demo_shon1'));
        $form->setLib('su_demo_shon2',_('su_demo_shon2'));
        $form->setLib('su_demo_shon3',_('su_demo_shon3'));
        $form->setLib('su_demo_shon4',_('su_demo_shon4'));
        $form->setLib('su_demo_shon5',_('su_demo_shon5'));
        $form->setLib('su_demo_shon6',_('su_demo_shon6'));
        $form->setLib('su_demo_shon7',_('su_demo_shon7'));
        $form->setLib('su_demo_shon8',_('su_demo_shon8'));
        $form->setLib('su_demo_shon9',_('su_demo_shon9'));
        $form->setLib('su_sup_shon1',_('su_sup_shon1'));
        $form->setLib('su_sup_shon2',_('su_sup_shon2'));
        $form->setLib('su_sup_shon3',_('su_sup_shon3'));
        $form->setLib('su_sup_shon4',_('su_sup_shon4'));
        $form->setLib('su_sup_shon5',_('su_sup_shon5'));
        $form->setLib('su_sup_shon6',_('su_sup_shon6'));
        $form->setLib('su_sup_shon7',_('su_sup_shon7'));
        $form->setLib('su_sup_shon8',_('su_sup_shon8'));
        $form->setLib('su_sup_shon9',_('su_sup_shon9'));
        $form->setLib('su_tot_shon1',_('su_tot_shon1'));
        $form->setLib('su_tot_shon2',_('su_tot_shon2'));
        $form->setLib('su_tot_shon3',_('su_tot_shon3'));
        $form->setLib('su_tot_shon4',_('su_tot_shon4'));
        $form->setLib('su_tot_shon5',_('su_tot_shon5'));
        $form->setLib('su_tot_shon6',_('su_tot_shon6'));
        $form->setLib('su_tot_shon7',_('su_tot_shon7'));
        $form->setLib('su_tot_shon8',_('su_tot_shon8'));
        $form->setLib('su_tot_shon9',_('su_tot_shon9'));
        $form->setLib('su_avt_shon_tot',_('su_avt_shon_tot'));
        $form->setLib('su_cstr_shon_tot',_('su_cstr_shon_tot'));
        $form->setLib('su_trsf_shon_tot',_('su_trsf_shon_tot'));
        $form->setLib('su_chge_shon_tot',_('su_chge_shon_tot'));
        $form->setLib('su_demo_shon_tot',_('su_demo_shon_tot'));
        $form->setLib('su_sup_shon_tot',_('su_sup_shon_tot'));
        $form->setLib('su_tot_shon_tot',_('su_tot_shon_tot'));
        $form->setLib('dm_constr_dates',_('dm_constr_dates'));
        $form->setLib('dm_total',_('dm_total'));
        $form->setLib('dm_partiel',_('dm_partiel'));
        $form->setLib('dm_projet_desc',_('dm_projet_desc'));
        $form->setLib('dm_tot_log_nb',_('dm_tot_log_nb'));
        $form->setLib('tax_surf_tot',_('tax_surf_tot'));
        $form->setLib('tax_surf',_('tax_surf'));
        $form->setLib('tax_surf_suppr_mod',_('tax_surf_suppr_mod'));
        $form->setLib('tax_su_princ_log_nb1',_('tax_su_princ_log_nb1'));
        $form->setLib('tax_su_princ_log_nb2',_('tax_su_princ_log_nb2'));
        $form->setLib('tax_su_princ_log_nb3',_('tax_su_princ_log_nb3'));
        $form->setLib('tax_su_princ_log_nb4',_('tax_su_princ_log_nb4'));
        $form->setLib('tax_su_princ_log_nb_tot1',_('tax_su_princ_log_nb_tot1'));
        $form->setLib('tax_su_princ_log_nb_tot2',_('tax_su_princ_log_nb_tot2'));
        $form->setLib('tax_su_princ_log_nb_tot3',_('tax_su_princ_log_nb_tot3'));
        $form->setLib('tax_su_princ_log_nb_tot4',_('tax_su_princ_log_nb_tot4'));
        $form->setLib('tax_su_princ_surf1',_('tax_su_princ_surf1'));
        $form->setLib('tax_su_princ_surf2',_('tax_su_princ_surf2'));
        $form->setLib('tax_su_princ_surf3',_('tax_su_princ_surf3'));
        $form->setLib('tax_su_princ_surf4',_('tax_su_princ_surf4'));
        $form->setLib('tax_su_princ_surf_sup1',_('tax_su_princ_surf_sup1'));
        $form->setLib('tax_su_princ_surf_sup2',_('tax_su_princ_surf_sup2'));
        $form->setLib('tax_su_princ_surf_sup3',_('tax_su_princ_surf_sup3'));
        $form->setLib('tax_su_princ_surf_sup4',_('tax_su_princ_surf_sup4'));
        $form->setLib('tax_su_heber_log_nb1',_('tax_su_heber_log_nb1'));
        $form->setLib('tax_su_heber_log_nb2',_('tax_su_heber_log_nb2'));
        $form->setLib('tax_su_heber_log_nb3',_('tax_su_heber_log_nb3'));
        $form->setLib('tax_su_heber_log_nb_tot1',_('tax_su_heber_log_nb_tot1'));
        $form->setLib('tax_su_heber_log_nb_tot2',_('tax_su_heber_log_nb_tot2'));
        $form->setLib('tax_su_heber_log_nb_tot3',_('tax_su_heber_log_nb_tot3'));
        $form->setLib('tax_su_heber_surf1',_('tax_su_heber_surf1'));
        $form->setLib('tax_su_heber_surf2',_('tax_su_heber_surf2'));
        $form->setLib('tax_su_heber_surf3',_('tax_su_heber_surf3'));
        $form->setLib('tax_su_heber_surf_sup1',_('tax_su_heber_surf_sup1'));
        $form->setLib('tax_su_heber_surf_sup2',_('tax_su_heber_surf_sup2'));
        $form->setLib('tax_su_heber_surf_sup3',_('tax_su_heber_surf_sup3'));
        $form->setLib('tax_su_secon_log_nb',_('tax_su_secon_log_nb'));
        $form->setLib('tax_su_tot_log_nb',_('tax_su_tot_log_nb'));
        $form->setLib('tax_su_secon_log_nb_tot',_('tax_su_secon_log_nb_tot'));
        $form->setLib('tax_su_tot_log_nb_tot',_('tax_su_tot_log_nb_tot'));
        $form->setLib('tax_su_secon_surf',_('tax_su_secon_surf'));
        $form->setLib('tax_su_tot_surf',_('tax_su_tot_surf'));
        $form->setLib('tax_su_secon_surf_sup',_('tax_su_secon_surf_sup'));
        $form->setLib('tax_su_tot_surf_sup',_('tax_su_tot_surf_sup'));
        $form->setLib('tax_ext_pret',_('tax_ext_pret'));
        $form->setLib('tax_ext_desc',_('tax_ext_desc'));
        $form->setLib('tax_surf_tax_exist_cons',_('tax_surf_tax_exist_cons'));
        $form->setLib('tax_log_exist_nb',_('tax_log_exist_nb'));
        $form->setLib('tax_am_statio_ext',_('tax_am_statio_ext'));
        $form->setLib('tax_sup_bass_pisc',_('tax_sup_bass_pisc'));
        $form->setLib('tax_empl_ten_carav_mobil_nb',_('tax_empl_ten_carav_mobil_nb'));
        $form->setLib('tax_empl_hll_nb',_('tax_empl_hll_nb'));
        $form->setLib('tax_eol_haut_nb',_('tax_eol_haut_nb'));
        $form->setLib('tax_pann_volt_sup',_('tax_pann_volt_sup'));
        $form->setLib('tax_am_statio_ext_sup',_('tax_am_statio_ext_sup'));
        $form->setLib('tax_sup_bass_pisc_sup',_('tax_sup_bass_pisc_sup'));
        $form->setLib('tax_empl_ten_carav_mobil_nb_sup',_('tax_empl_ten_carav_mobil_nb_sup'));
        $form->setLib('tax_empl_hll_nb_sup',_('tax_empl_hll_nb_sup'));
        $form->setLib('tax_eol_haut_nb_sup',_('tax_eol_haut_nb_sup'));
        $form->setLib('tax_pann_volt_sup_sup',_('tax_pann_volt_sup_sup'));
        $form->setLib('tax_trx_presc_ppr',_('tax_trx_presc_ppr'));
        $form->setLib('tax_monu_hist',_('tax_monu_hist'));
        $form->setLib('tax_comm_nb',_('tax_comm_nb'));
        $form->setLib('tax_su_non_habit_surf1',_('tax_su_non_habit_surf1'));
        $form->setLib('tax_su_non_habit_surf2',_('tax_su_non_habit_surf2'));
        $form->setLib('tax_su_non_habit_surf3',_('tax_su_non_habit_surf3'));
        $form->setLib('tax_su_non_habit_surf4',_('tax_su_non_habit_surf4'));
        $form->setLib('tax_su_non_habit_surf5',_('tax_su_non_habit_surf5'));
        $form->setLib('tax_su_non_habit_surf6',_('tax_su_non_habit_surf6'));
        $form->setLib('tax_su_non_habit_surf7',_('tax_su_non_habit_surf7'));
        $form->setLib('tax_su_non_habit_surf_sup1',_('tax_su_non_habit_surf_sup1'));
        $form->setLib('tax_su_non_habit_surf_sup2',_('tax_su_non_habit_surf_sup2'));
        $form->setLib('tax_su_non_habit_surf_sup3',_('tax_su_non_habit_surf_sup3'));
        $form->setLib('tax_su_non_habit_surf_sup4',_('tax_su_non_habit_surf_sup4'));
        $form->setLib('tax_su_non_habit_surf_sup5',_('tax_su_non_habit_surf_sup5'));
        $form->setLib('tax_su_non_habit_surf_sup6',_('tax_su_non_habit_surf_sup6'));
        $form->setLib('tax_su_non_habit_surf_sup7',_('tax_su_non_habit_surf_sup7'));
        $form->setLib('vsd_surf_planch_smd',_('vsd_surf_planch_smd'));
        $form->setLib('vsd_unit_fonc_sup',_('vsd_unit_fonc_sup'));
        $form->setLib('vsd_unit_fonc_constr_sup',_('vsd_unit_fonc_constr_sup'));
        $form->setLib('vsd_val_terr',_('vsd_val_terr'));
        $form->setLib('vsd_const_sxist_non_dem_surf',_('vsd_const_sxist_non_dem_surf'));
        $form->setLib('vsd_rescr_fisc',_('vsd_rescr_fisc'));
        $form->setLib('pld_val_terr',_('pld_val_terr'));
        $form->setLib('pld_const_exist_dem',_('pld_const_exist_dem'));
        $form->setLib('pld_const_exist_dem_surf',_('pld_const_exist_dem_surf'));
        $form->setLib('code_cnil',_('code_cnil'));
        $form->setLib('terr_juri_titul',_('terr_juri_titul'));
        $form->setLib('terr_juri_lot',_('terr_juri_lot'));
        $form->setLib('terr_juri_zac',_('terr_juri_zac'));
        $form->setLib('terr_juri_afu',_('terr_juri_afu'));
        $form->setLib('terr_juri_pup',_('terr_juri_pup'));
        $form->setLib('terr_juri_oin',_('terr_juri_oin'));
        $form->setLib('terr_juri_desc',_('terr_juri_desc'));
        $form->setLib('terr_div_surf_etab',_('terr_div_surf_etab'));
        $form->setLib('terr_div_surf_av_div',_('terr_div_surf_av_div'));
        $form->setLib('doc_date',_('doc_date'));
        $form->setLib('doc_tot_trav',_('doc_tot_trav'));
        $form->setLib('doc_tranche_trav',_('doc_tranche_trav'));
        $form->setLib('doc_tranche_trav_desc',_('doc_tranche_trav_desc'));
        $form->setLib('doc_surf',_('doc_surf'));
        $form->setLib('doc_nb_log',_('doc_nb_log'));
        $form->setLib('doc_nb_log_indiv',_('doc_nb_log_indiv'));
        $form->setLib('doc_nb_log_coll',_('doc_nb_log_coll'));
        $form->setLib('doc_nb_log_lls',_('doc_nb_log_lls'));
        $form->setLib('doc_nb_log_aa',_('doc_nb_log_aa'));
        $form->setLib('doc_nb_log_ptz',_('doc_nb_log_ptz'));
        $form->setLib('doc_nb_log_autre',_('doc_nb_log_autre'));
        $form->setLib('daact_date',_('daact_date'));
        $form->setLib('daact_date_chgmt_dest',_('daact_date_chgmt_dest'));
        $form->setLib('daact_tot_trav',_('daact_tot_trav'));
        $form->setLib('daact_tranche_trav',_('daact_tranche_trav'));
        $form->setLib('daact_tranche_trav_desc',_('daact_tranche_trav_desc'));
        $form->setLib('daact_surf',_('daact_surf'));
        $form->setLib('daact_nb_log',_('daact_nb_log'));
        $form->setLib('daact_nb_log_indiv',_('daact_nb_log_indiv'));
        $form->setLib('daact_nb_log_coll',_('daact_nb_log_coll'));
        $form->setLib('daact_nb_log_lls',_('daact_nb_log_lls'));
        $form->setLib('daact_nb_log_aa',_('daact_nb_log_aa'));
        $form->setLib('daact_nb_log_ptz',_('daact_nb_log_ptz'));
        $form->setLib('daact_nb_log_autre',_('daact_nb_log_autre'));
        $form->setLib('dossier_autorisation',_('dossier_autorisation'));
        $form->setLib('am_div_mun',_('am_div_mun'));
        $form->setLib('co_perf_energ',_('co_perf_energ'));
        $form->setLib('architecte',_('architecte'));
        $form->setLib('co_statio_avt_shob',_('co_statio_avt_shob'));
        $form->setLib('co_statio_apr_shob',_('co_statio_apr_shob'));
        $form->setLib('co_statio_avt_surf',_('co_statio_avt_surf'));
        $form->setLib('co_statio_apr_surf',_('co_statio_apr_surf'));
        $form->setLib('co_trx_amgt',_('co_trx_amgt'));
        $form->setLib('co_modif_aspect',_('co_modif_aspect'));
        $form->setLib('co_modif_struct',_('co_modif_struct'));
        $form->setLib('co_ouvr_elec',_('co_ouvr_elec'));
        $form->setLib('co_ouvr_infra',_('co_ouvr_infra'));
        $form->setLib('co_trx_imm',_('co_trx_imm'));
        $form->setLib('co_cstr_shob',_('co_cstr_shob'));
        $form->setLib('am_voyage_deb',_('am_voyage_deb'));
        $form->setLib('am_voyage_fin',_('am_voyage_fin'));
        $form->setLib('am_modif_amgt',_('am_modif_amgt'));
        $form->setLib('am_lot_max_shob',_('am_lot_max_shob'));
        $form->setLib('mod_desc',_('mod_desc'));
        $form->setLib('tr_total',_('tr_total'));
        $form->setLib('tr_partiel',_('tr_partiel'));
        $form->setLib('tr_desc',_('tr_desc'));
        $form->setLib('avap_co_elt_pro',_('avap_co_elt_pro'));
        $form->setLib('avap_nouv_haut_surf',_('avap_nouv_haut_surf'));
        $form->setLib('avap_co_clot',_('avap_co_clot'));
        $form->setLib('avap_aut_coup_aba_arb',_('avap_aut_coup_aba_arb'));
        $form->setLib('avap_ouv_infra',_('avap_ouv_infra'));
        $form->setLib('avap_aut_inst_mob',_('avap_aut_inst_mob'));
        $form->setLib('avap_aut_plant',_('avap_aut_plant'));
        $form->setLib('avap_aut_auv_elec',_('avap_aut_auv_elec'));
        $form->setLib('tax_dest_loc_tr',_('tax_dest_loc_tr'));
        $form->setLib('ope_proj_desc',_('ope_proj_desc'));
        $form->setLib('tax_surf_tot_cstr',_('tax_surf_tot_cstr'));
        $form->setLib('cerfa',_('cerfa'));
        $form->setLib('tax_surf_loc_stat',_('tax_surf_loc_stat'));
        $form->setLib('tax_su_princ_surf_stat1',_('tax_su_princ_surf_stat1'));
        $form->setLib('tax_su_princ_surf_stat2',_('tax_su_princ_surf_stat2'));
        $form->setLib('tax_su_princ_surf_stat3',_('tax_su_princ_surf_stat3'));
        $form->setLib('tax_su_princ_surf_stat4',_('tax_su_princ_surf_stat4'));
        $form->setLib('tax_su_secon_surf_stat',_('tax_su_secon_surf_stat'));
        $form->setLib('tax_su_heber_surf_stat1',_('tax_su_heber_surf_stat1'));
        $form->setLib('tax_su_heber_surf_stat2',_('tax_su_heber_surf_stat2'));
        $form->setLib('tax_su_heber_surf_stat3',_('tax_su_heber_surf_stat3'));
        $form->setLib('tax_su_tot_surf_stat',_('tax_su_tot_surf_stat'));
        $form->setLib('tax_su_non_habit_surf_stat1',_('tax_su_non_habit_surf_stat1'));
        $form->setLib('tax_su_non_habit_surf_stat2',_('tax_su_non_habit_surf_stat2'));
        $form->setLib('tax_su_non_habit_surf_stat3',_('tax_su_non_habit_surf_stat3'));
        $form->setLib('tax_su_non_habit_surf_stat4',_('tax_su_non_habit_surf_stat4'));
        $form->setLib('tax_su_non_habit_surf_stat5',_('tax_su_non_habit_surf_stat5'));
        $form->setLib('tax_su_non_habit_surf_stat6',_('tax_su_non_habit_surf_stat6'));
        $form->setLib('tax_su_non_habit_surf_stat7',_('tax_su_non_habit_surf_stat7'));
        $form->setLib('tax_su_parc_statio_expl_comm_surf',_('tax_su_parc_statio_expl_comm_surf'));
        $form->setLib('tax_log_ap_trvx_nb',_('tax_log_ap_trvx_nb'));
        $form->setLib('tax_am_statio_ext_cr',_('tax_am_statio_ext_cr'));
        $form->setLib('tax_sup_bass_pisc_cr',_('tax_sup_bass_pisc_cr'));
        $form->setLib('tax_empl_ten_carav_mobil_nb_cr',_('tax_empl_ten_carav_mobil_nb_cr'));
        $form->setLib('tax_empl_hll_nb_cr',_('tax_empl_hll_nb_cr'));
        $form->setLib('tax_eol_haut_nb_cr',_('tax_eol_haut_nb_cr'));
        $form->setLib('tax_pann_volt_sup_cr',_('tax_pann_volt_sup_cr'));
        $form->setLib('tax_surf_loc_arch',_('tax_surf_loc_arch'));
        $form->setLib('tax_surf_pisc_arch',_('tax_surf_pisc_arch'));
        $form->setLib('tax_am_statio_ext_arch',_('tax_am_statio_ext_arch'));
        $form->setLib('tax_empl_ten_carav_mobil_nb_arch',_('tax_empl_ten_carav_mobil_nb_arch'));
        $form->setLib('tax_empl_hll_nb_arch',_('tax_empl_hll_nb_arch'));
        $form->setLib('tax_eol_haut_nb_arch',_('tax_eol_haut_nb_arch'));
        $form->setLib('ope_proj_div_co',_('ope_proj_div_co'));
        $form->setLib('ope_proj_div_contr',_('ope_proj_div_contr'));
        $form->setLib('tax_desc',_('tax_desc'));
        $form->setLib('erp_cstr_neuve',_('erp_cstr_neuve'));
        $form->setLib('erp_trvx_acc',_('erp_trvx_acc'));
        $form->setLib('erp_extension',_('erp_extension'));
        $form->setLib('erp_rehab',_('erp_rehab'));
        $form->setLib('erp_trvx_am',_('erp_trvx_am'));
        $form->setLib('erp_vol_nouv_exist',_('erp_vol_nouv_exist'));
        $form->setLib('erp_loc_eff1',_('erp_loc_eff1'));
        $form->setLib('erp_loc_eff2',_('erp_loc_eff2'));
        $form->setLib('erp_loc_eff3',_('erp_loc_eff3'));
        $form->setLib('erp_loc_eff4',_('erp_loc_eff4'));
        $form->setLib('erp_loc_eff5',_('erp_loc_eff5'));
        $form->setLib('erp_loc_eff_tot',_('erp_loc_eff_tot'));
        $form->setLib('erp_public_eff1',_('erp_public_eff1'));
        $form->setLib('erp_public_eff2',_('erp_public_eff2'));
        $form->setLib('erp_public_eff3',_('erp_public_eff3'));
        $form->setLib('erp_public_eff4',_('erp_public_eff4'));
        $form->setLib('erp_public_eff5',_('erp_public_eff5'));
        $form->setLib('erp_public_eff_tot',_('erp_public_eff_tot'));
        $form->setLib('erp_perso_eff1',_('erp_perso_eff1'));
        $form->setLib('erp_perso_eff2',_('erp_perso_eff2'));
        $form->setLib('erp_perso_eff3',_('erp_perso_eff3'));
        $form->setLib('erp_perso_eff4',_('erp_perso_eff4'));
        $form->setLib('erp_perso_eff5',_('erp_perso_eff5'));
        $form->setLib('erp_perso_eff_tot',_('erp_perso_eff_tot'));
        $form->setLib('erp_tot_eff1',_('erp_tot_eff1'));
        $form->setLib('erp_tot_eff2',_('erp_tot_eff2'));
        $form->setLib('erp_tot_eff3',_('erp_tot_eff3'));
        $form->setLib('erp_tot_eff4',_('erp_tot_eff4'));
        $form->setLib('erp_tot_eff5',_('erp_tot_eff5'));
        $form->setLib('erp_tot_eff_tot',_('erp_tot_eff_tot'));
        $form->setLib('erp_class_cat',_('erp_class_cat'));
        $form->setLib('erp_class_type',_('erp_class_type'));
        $form->setLib('tax_surf_abr_jard_pig_colom',_('tax_surf_abr_jard_pig_colom'));
        $form->setLib('tax_su_non_habit_abr_jard_pig_colom',_('tax_su_non_habit_abr_jard_pig_colom'));
        $form->setLib('dia_imm_non_bati',_('dia_imm_non_bati'));
        $form->setLib('dia_imm_bati_terr_propr',_('dia_imm_bati_terr_propr'));
        $form->setLib('dia_imm_bati_terr_autr',_('dia_imm_bati_terr_autr'));
        $form->setLib('dia_imm_bati_terr_autr_desc',_('dia_imm_bati_terr_autr_desc'));
        $form->setLib('dia_bat_copro',_('dia_bat_copro'));
        $form->setLib('dia_bat_copro_desc',_('dia_bat_copro_desc'));
        $form->setLib('dia_lot_numero',_('dia_lot_numero'));
        $form->setLib('dia_lot_bat',_('dia_lot_bat'));
        $form->setLib('dia_lot_etage',_('dia_lot_etage'));
        $form->setLib('dia_lot_quote_part',_('dia_lot_quote_part'));
        $form->setLib('dia_us_hab',_('dia_us_hab'));
        $form->setLib('dia_us_pro',_('dia_us_pro'));
        $form->setLib('dia_us_mixte',_('dia_us_mixte'));
        $form->setLib('dia_us_comm',_('dia_us_comm'));
        $form->setLib('dia_us_agr',_('dia_us_agr'));
        $form->setLib('dia_us_autre',_('dia_us_autre'));
        $form->setLib('dia_us_autre_prec',_('dia_us_autre_prec'));
        $form->setLib('dia_occ_prop',_('dia_occ_prop'));
        $form->setLib('dia_occ_loc',_('dia_occ_loc'));
        $form->setLib('dia_occ_sans_occ',_('dia_occ_sans_occ'));
        $form->setLib('dia_occ_autre',_('dia_occ_autre'));
        $form->setLib('dia_occ_autre_prec',_('dia_occ_autre_prec'));
        $form->setLib('dia_mod_cess_prix_vente',_('dia_mod_cess_prix_vente'));
        $form->setLib('dia_mod_cess_prix_vente_mob',_('dia_mod_cess_prix_vente_mob'));
        $form->setLib('dia_mod_cess_prix_vente_cheptel',_('dia_mod_cess_prix_vente_cheptel'));
        $form->setLib('dia_mod_cess_prix_vente_recol',_('dia_mod_cess_prix_vente_recol'));
        $form->setLib('dia_mod_cess_prix_vente_autre',_('dia_mod_cess_prix_vente_autre'));
        $form->setLib('dia_mod_cess_commi',_('dia_mod_cess_commi'));
        $form->setLib('dia_mod_cess_commi_ttc',_('dia_mod_cess_commi_ttc'));
        $form->setLib('dia_mod_cess_commi_ht',_('dia_mod_cess_commi_ht'));
        $form->setLib('dia_acquereur_nom_prenom',_('dia_acquereur_nom_prenom'));
        $form->setLib('dia_acquereur_adr_num_voie',_('dia_acquereur_adr_num_voie'));
        $form->setLib('dia_acquereur_adr_ext',_('dia_acquereur_adr_ext'));
        $form->setLib('dia_acquereur_adr_type_voie',_('dia_acquereur_adr_type_voie'));
        $form->setLib('dia_acquereur_adr_nom_voie',_('dia_acquereur_adr_nom_voie'));
        $form->setLib('dia_acquereur_adr_lieu_dit_bp',_('dia_acquereur_adr_lieu_dit_bp'));
        $form->setLib('dia_acquereur_adr_cp',_('dia_acquereur_adr_cp'));
        $form->setLib('dia_acquereur_adr_localite',_('dia_acquereur_adr_localite'));
        $form->setLib('dia_observation',_('dia_observation'));
        $form->setLib('su2_avt_shon1',_('su2_avt_shon1'));
        $form->setLib('su2_avt_shon2',_('su2_avt_shon2'));
        $form->setLib('su2_avt_shon3',_('su2_avt_shon3'));
        $form->setLib('su2_avt_shon4',_('su2_avt_shon4'));
        $form->setLib('su2_avt_shon5',_('su2_avt_shon5'));
        $form->setLib('su2_avt_shon6',_('su2_avt_shon6'));
        $form->setLib('su2_avt_shon7',_('su2_avt_shon7'));
        $form->setLib('su2_avt_shon8',_('su2_avt_shon8'));
        $form->setLib('su2_avt_shon9',_('su2_avt_shon9'));
        $form->setLib('su2_avt_shon10',_('su2_avt_shon10'));
        $form->setLib('su2_avt_shon11',_('su2_avt_shon11'));
        $form->setLib('su2_avt_shon12',_('su2_avt_shon12'));
        $form->setLib('su2_avt_shon13',_('su2_avt_shon13'));
        $form->setLib('su2_avt_shon14',_('su2_avt_shon14'));
        $form->setLib('su2_avt_shon15',_('su2_avt_shon15'));
        $form->setLib('su2_avt_shon16',_('su2_avt_shon16'));
        $form->setLib('su2_avt_shon17',_('su2_avt_shon17'));
        $form->setLib('su2_avt_shon18',_('su2_avt_shon18'));
        $form->setLib('su2_avt_shon19',_('su2_avt_shon19'));
        $form->setLib('su2_avt_shon20',_('su2_avt_shon20'));
        $form->setLib('su2_avt_shon_tot',_('su2_avt_shon_tot'));
        $form->setLib('su2_cstr_shon1',_('su2_cstr_shon1'));
        $form->setLib('su2_cstr_shon2',_('su2_cstr_shon2'));
        $form->setLib('su2_cstr_shon3',_('su2_cstr_shon3'));
        $form->setLib('su2_cstr_shon4',_('su2_cstr_shon4'));
        $form->setLib('su2_cstr_shon5',_('su2_cstr_shon5'));
        $form->setLib('su2_cstr_shon6',_('su2_cstr_shon6'));
        $form->setLib('su2_cstr_shon7',_('su2_cstr_shon7'));
        $form->setLib('su2_cstr_shon8',_('su2_cstr_shon8'));
        $form->setLib('su2_cstr_shon9',_('su2_cstr_shon9'));
        $form->setLib('su2_cstr_shon10',_('su2_cstr_shon10'));
        $form->setLib('su2_cstr_shon11',_('su2_cstr_shon11'));
        $form->setLib('su2_cstr_shon12',_('su2_cstr_shon12'));
        $form->setLib('su2_cstr_shon13',_('su2_cstr_shon13'));
        $form->setLib('su2_cstr_shon14',_('su2_cstr_shon14'));
        $form->setLib('su2_cstr_shon15',_('su2_cstr_shon15'));
        $form->setLib('su2_cstr_shon16',_('su2_cstr_shon16'));
        $form->setLib('su2_cstr_shon17',_('su2_cstr_shon17'));
        $form->setLib('su2_cstr_shon18',_('su2_cstr_shon18'));
        $form->setLib('su2_cstr_shon19',_('su2_cstr_shon19'));
        $form->setLib('su2_cstr_shon20',_('su2_cstr_shon20'));
        $form->setLib('su2_cstr_shon_tot',_('su2_cstr_shon_tot'));
        $form->setLib('su2_chge_shon1',_('su2_chge_shon1'));
        $form->setLib('su2_chge_shon2',_('su2_chge_shon2'));
        $form->setLib('su2_chge_shon3',_('su2_chge_shon3'));
        $form->setLib('su2_chge_shon4',_('su2_chge_shon4'));
        $form->setLib('su2_chge_shon5',_('su2_chge_shon5'));
        $form->setLib('su2_chge_shon6',_('su2_chge_shon6'));
        $form->setLib('su2_chge_shon7',_('su2_chge_shon7'));
        $form->setLib('su2_chge_shon8',_('su2_chge_shon8'));
        $form->setLib('su2_chge_shon9',_('su2_chge_shon9'));
        $form->setLib('su2_chge_shon10',_('su2_chge_shon10'));
        $form->setLib('su2_chge_shon11',_('su2_chge_shon11'));
        $form->setLib('su2_chge_shon12',_('su2_chge_shon12'));
        $form->setLib('su2_chge_shon13',_('su2_chge_shon13'));
        $form->setLib('su2_chge_shon14',_('su2_chge_shon14'));
        $form->setLib('su2_chge_shon15',_('su2_chge_shon15'));
        $form->setLib('su2_chge_shon16',_('su2_chge_shon16'));
        $form->setLib('su2_chge_shon17',_('su2_chge_shon17'));
        $form->setLib('su2_chge_shon18',_('su2_chge_shon18'));
        $form->setLib('su2_chge_shon19',_('su2_chge_shon19'));
        $form->setLib('su2_chge_shon20',_('su2_chge_shon20'));
        $form->setLib('su2_chge_shon_tot',_('su2_chge_shon_tot'));
        $form->setLib('su2_demo_shon1',_('su2_demo_shon1'));
        $form->setLib('su2_demo_shon2',_('su2_demo_shon2'));
        $form->setLib('su2_demo_shon3',_('su2_demo_shon3'));
        $form->setLib('su2_demo_shon4',_('su2_demo_shon4'));
        $form->setLib('su2_demo_shon5',_('su2_demo_shon5'));
        $form->setLib('su2_demo_shon6',_('su2_demo_shon6'));
        $form->setLib('su2_demo_shon7',_('su2_demo_shon7'));
        $form->setLib('su2_demo_shon8',_('su2_demo_shon8'));
        $form->setLib('su2_demo_shon9',_('su2_demo_shon9'));
        $form->setLib('su2_demo_shon10',_('su2_demo_shon10'));
        $form->setLib('su2_demo_shon11',_('su2_demo_shon11'));
        $form->setLib('su2_demo_shon12',_('su2_demo_shon12'));
        $form->setLib('su2_demo_shon13',_('su2_demo_shon13'));
        $form->setLib('su2_demo_shon14',_('su2_demo_shon14'));
        $form->setLib('su2_demo_shon15',_('su2_demo_shon15'));
        $form->setLib('su2_demo_shon16',_('su2_demo_shon16'));
        $form->setLib('su2_demo_shon17',_('su2_demo_shon17'));
        $form->setLib('su2_demo_shon18',_('su2_demo_shon18'));
        $form->setLib('su2_demo_shon19',_('su2_demo_shon19'));
        $form->setLib('su2_demo_shon20',_('su2_demo_shon20'));
        $form->setLib('su2_demo_shon_tot',_('su2_demo_shon_tot'));
        $form->setLib('su2_sup_shon1',_('su2_sup_shon1'));
        $form->setLib('su2_sup_shon2',_('su2_sup_shon2'));
        $form->setLib('su2_sup_shon3',_('su2_sup_shon3'));
        $form->setLib('su2_sup_shon4',_('su2_sup_shon4'));
        $form->setLib('su2_sup_shon5',_('su2_sup_shon5'));
        $form->setLib('su2_sup_shon6',_('su2_sup_shon6'));
        $form->setLib('su2_sup_shon7',_('su2_sup_shon7'));
        $form->setLib('su2_sup_shon8',_('su2_sup_shon8'));
        $form->setLib('su2_sup_shon9',_('su2_sup_shon9'));
        $form->setLib('su2_sup_shon10',_('su2_sup_shon10'));
        $form->setLib('su2_sup_shon11',_('su2_sup_shon11'));
        $form->setLib('su2_sup_shon12',_('su2_sup_shon12'));
        $form->setLib('su2_sup_shon13',_('su2_sup_shon13'));
        $form->setLib('su2_sup_shon14',_('su2_sup_shon14'));
        $form->setLib('su2_sup_shon15',_('su2_sup_shon15'));
        $form->setLib('su2_sup_shon16',_('su2_sup_shon16'));
        $form->setLib('su2_sup_shon17',_('su2_sup_shon17'));
        $form->setLib('su2_sup_shon18',_('su2_sup_shon18'));
        $form->setLib('su2_sup_shon19',_('su2_sup_shon19'));
        $form->setLib('su2_sup_shon20',_('su2_sup_shon20'));
        $form->setLib('su2_sup_shon_tot',_('su2_sup_shon_tot'));
        $form->setLib('su2_tot_shon1',_('su2_tot_shon1'));
        $form->setLib('su2_tot_shon2',_('su2_tot_shon2'));
        $form->setLib('su2_tot_shon3',_('su2_tot_shon3'));
        $form->setLib('su2_tot_shon4',_('su2_tot_shon4'));
        $form->setLib('su2_tot_shon5',_('su2_tot_shon5'));
        $form->setLib('su2_tot_shon6',_('su2_tot_shon6'));
        $form->setLib('su2_tot_shon7',_('su2_tot_shon7'));
        $form->setLib('su2_tot_shon8',_('su2_tot_shon8'));
        $form->setLib('su2_tot_shon9',_('su2_tot_shon9'));
        $form->setLib('su2_tot_shon10',_('su2_tot_shon10'));
        $form->setLib('su2_tot_shon11',_('su2_tot_shon11'));
        $form->setLib('su2_tot_shon12',_('su2_tot_shon12'));
        $form->setLib('su2_tot_shon13',_('su2_tot_shon13'));
        $form->setLib('su2_tot_shon14',_('su2_tot_shon14'));
        $form->setLib('su2_tot_shon15',_('su2_tot_shon15'));
        $form->setLib('su2_tot_shon16',_('su2_tot_shon16'));
        $form->setLib('su2_tot_shon17',_('su2_tot_shon17'));
        $form->setLib('su2_tot_shon18',_('su2_tot_shon18'));
        $form->setLib('su2_tot_shon19',_('su2_tot_shon19'));
        $form->setLib('su2_tot_shon20',_('su2_tot_shon20'));
        $form->setLib('su2_tot_shon_tot',_('su2_tot_shon_tot'));
        $form->setLib('dia_occ_sol_su_terre',_('dia_occ_sol_su_terre'));
        $form->setLib('dia_occ_sol_su_pres',_('dia_occ_sol_su_pres'));
        $form->setLib('dia_occ_sol_su_verger',_('dia_occ_sol_su_verger'));
        $form->setLib('dia_occ_sol_su_vigne',_('dia_occ_sol_su_vigne'));
        $form->setLib('dia_occ_sol_su_bois',_('dia_occ_sol_su_bois'));
        $form->setLib('dia_occ_sol_su_lande',_('dia_occ_sol_su_lande'));
        $form->setLib('dia_occ_sol_su_carriere',_('dia_occ_sol_su_carriere'));
        $form->setLib('dia_occ_sol_su_eau_cadastree',_('dia_occ_sol_su_eau_cadastree'));
        $form->setLib('dia_occ_sol_su_jardin',_('dia_occ_sol_su_jardin'));
        $form->setLib('dia_occ_sol_su_terr_batir',_('dia_occ_sol_su_terr_batir'));
        $form->setLib('dia_occ_sol_su_terr_agr',_('dia_occ_sol_su_terr_agr'));
        $form->setLib('dia_occ_sol_su_sol',_('dia_occ_sol_su_sol'));
        $form->setLib('dia_bati_vend_tot',_('dia_bati_vend_tot'));
        $form->setLib('dia_bati_vend_tot_txt',_('dia_bati_vend_tot_txt'));
        $form->setLib('dia_su_co_sol',_('dia_su_co_sol'));
        $form->setLib('dia_su_util_hab',_('dia_su_util_hab'));
        $form->setLib('dia_nb_niv',_('dia_nb_niv'));
        $form->setLib('dia_nb_appart',_('dia_nb_appart'));
        $form->setLib('dia_nb_autre_loc',_('dia_nb_autre_loc'));
        $form->setLib('dia_vente_lot_volume',_('dia_vente_lot_volume'));
        $form->setLib('dia_vente_lot_volume_txt',_('dia_vente_lot_volume_txt'));
        $form->setLib('dia_lot_nat_su',_('dia_lot_nat_su'));
        $form->setLib('dia_lot_bat_achv_plus_10',_('dia_lot_bat_achv_plus_10'));
        $form->setLib('dia_lot_bat_achv_moins_10',_('dia_lot_bat_achv_moins_10'));
        $form->setLib('dia_lot_regl_copro_publ_hypo_plus_10',_('dia_lot_regl_copro_publ_hypo_plus_10'));
        $form->setLib('dia_lot_regl_copro_publ_hypo_moins_10',_('dia_lot_regl_copro_publ_hypo_moins_10'));
        $form->setLib('dia_indivi_quote_part',_('dia_indivi_quote_part'));
        $form->setLib('dia_design_societe',_('dia_design_societe'));
        $form->setLib('dia_design_droit',_('dia_design_droit'));
        $form->setLib('dia_droit_soc_nat',_('dia_droit_soc_nat'));
        $form->setLib('dia_droit_soc_nb',_('dia_droit_soc_nb'));
        $form->setLib('dia_droit_soc_num_part',_('dia_droit_soc_num_part'));
        $form->setLib('dia_droit_reel_perso_grevant_bien_oui',_('dia_droit_reel_perso_grevant_bien_oui'));
        $form->setLib('dia_droit_reel_perso_grevant_bien_non',_('dia_droit_reel_perso_grevant_bien_non'));
        $form->setLib('dia_droit_reel_perso_nat',_('dia_droit_reel_perso_nat'));
        $form->setLib('dia_droit_reel_perso_viag',_('dia_droit_reel_perso_viag'));
        $form->setLib('dia_mod_cess_adr',_('dia_mod_cess_adr'));
        $form->setLib('dia_mod_cess_sign_act_auth',_('dia_mod_cess_sign_act_auth'));
        $form->setLib('dia_mod_cess_terme',_('dia_mod_cess_terme'));
        $form->setLib('dia_mod_cess_terme_prec',_('dia_mod_cess_terme_prec'));
        $form->setLib('dia_mod_cess_bene_acquereur',_('dia_mod_cess_bene_acquereur'));
        $form->setLib('dia_mod_cess_bene_vendeur',_('dia_mod_cess_bene_vendeur'));
        $form->setLib('dia_mod_cess_paie_nat',_('dia_mod_cess_paie_nat'));
        $form->setLib('dia_mod_cess_design_contr_alien',_('dia_mod_cess_design_contr_alien'));
        $form->setLib('dia_mod_cess_eval_contr',_('dia_mod_cess_eval_contr'));
        $form->setLib('dia_mod_cess_rente_viag',_('dia_mod_cess_rente_viag'));
        $form->setLib('dia_mod_cess_mnt_an',_('dia_mod_cess_mnt_an'));
        $form->setLib('dia_mod_cess_mnt_compt',_('dia_mod_cess_mnt_compt'));
        $form->setLib('dia_mod_cess_bene_rente',_('dia_mod_cess_bene_rente'));
        $form->setLib('dia_mod_cess_droit_usa_hab',_('dia_mod_cess_droit_usa_hab'));
        $form->setLib('dia_mod_cess_droit_usa_hab_prec',_('dia_mod_cess_droit_usa_hab_prec'));
        $form->setLib('dia_mod_cess_eval_usa_usufruit',_('dia_mod_cess_eval_usa_usufruit'));
        $form->setLib('dia_mod_cess_vente_nue_prop',_('dia_mod_cess_vente_nue_prop'));
        $form->setLib('dia_mod_cess_vente_nue_prop_prec',_('dia_mod_cess_vente_nue_prop_prec'));
        $form->setLib('dia_mod_cess_echange',_('dia_mod_cess_echange'));
        $form->setLib('dia_mod_cess_design_bien_recus_ech',_('dia_mod_cess_design_bien_recus_ech'));
        $form->setLib('dia_mod_cess_mnt_soulte',_('dia_mod_cess_mnt_soulte'));
        $form->setLib('dia_mod_cess_prop_contre_echan',_('dia_mod_cess_prop_contre_echan'));
        $form->setLib('dia_mod_cess_apport_societe',_('dia_mod_cess_apport_societe'));
        $form->setLib('dia_mod_cess_bene',_('dia_mod_cess_bene'));
        $form->setLib('dia_mod_cess_esti_bien',_('dia_mod_cess_esti_bien'));
        $form->setLib('dia_mod_cess_cess_terr_loc_co',_('dia_mod_cess_cess_terr_loc_co'));
        $form->setLib('dia_mod_cess_esti_terr',_('dia_mod_cess_esti_terr'));
        $form->setLib('dia_mod_cess_esti_loc',_('dia_mod_cess_esti_loc'));
        $form->setLib('dia_mod_cess_esti_imm_loca',_('dia_mod_cess_esti_imm_loca'));
        $form->setLib('dia_mod_cess_adju_vol',_('dia_mod_cess_adju_vol'));
        $form->setLib('dia_mod_cess_adju_obl',_('dia_mod_cess_adju_obl'));
        $form->setLib('dia_mod_cess_adju_fin_indivi',_('dia_mod_cess_adju_fin_indivi'));
        $form->setLib('dia_mod_cess_adju_date_lieu',_('dia_mod_cess_adju_date_lieu'));
        $form->setLib('dia_mod_cess_mnt_mise_prix',_('dia_mod_cess_mnt_mise_prix'));
        $form->setLib('dia_prop_titu_prix_indique',_('dia_prop_titu_prix_indique'));
        $form->setLib('dia_prop_recherche_acqu_prix_indique',_('dia_prop_recherche_acqu_prix_indique'));
        $form->setLib('dia_acquereur_prof',_('dia_acquereur_prof'));
        $form->setLib('dia_indic_compl_ope',_('dia_indic_compl_ope'));
        $form->setLib('dia_vente_adju',_('dia_vente_adju'));
        $form->setLib('am_terr_res_demon',_('am_terr_res_demon'));
        $form->setLib('am_air_terr_res_mob',_('am_air_terr_res_mob'));
        $form->setLib('ctx_objet_recours',_('ctx_objet_recours'));
        $form->setLib('ctx_reference_sagace',_('ctx_reference_sagace'));
        $form->setLib('ctx_nature_travaux_infra_om_html',_('ctx_nature_travaux_infra_om_html'));
        $form->setLib('ctx_synthese_nti',_('ctx_synthese_nti'));
        $form->setLib('ctx_article_non_resp_om_html',_('ctx_article_non_resp_om_html'));
        $form->setLib('ctx_synthese_anr',_('ctx_synthese_anr'));
        $form->setLib('ctx_reference_parquet',_('ctx_reference_parquet'));
        $form->setLib('ctx_element_taxation',_('ctx_element_taxation'));
        $form->setLib('ctx_infraction',_('ctx_infraction'));
        $form->setLib('ctx_regularisable',_('ctx_regularisable'));
        $form->setLib('ctx_reference_courrier',_('ctx_reference_courrier'));
        $form->setLib('ctx_date_audience',_('ctx_date_audience'));
        $form->setLib('ctx_date_ajournement',_('ctx_date_ajournement'));
        $form->setLib('exo_facul_1',_('exo_facul_1'));
        $form->setLib('exo_facul_2',_('exo_facul_2'));
        $form->setLib('exo_facul_3',_('exo_facul_3'));
        $form->setLib('exo_facul_4',_('exo_facul_4'));
        $form->setLib('exo_facul_5',_('exo_facul_5'));
        $form->setLib('exo_facul_6',_('exo_facul_6'));
        $form->setLib('exo_facul_7',_('exo_facul_7'));
        $form->setLib('exo_facul_8',_('exo_facul_8'));
        $form->setLib('exo_facul_9',_('exo_facul_9'));
        $form->setLib('exo_ta_1',_('exo_ta_1'));
        $form->setLib('exo_ta_2',_('exo_ta_2'));
        $form->setLib('exo_ta_3',_('exo_ta_3'));
        $form->setLib('exo_ta_4',_('exo_ta_4'));
        $form->setLib('exo_ta_5',_('exo_ta_5'));
        $form->setLib('exo_ta_6',_('exo_ta_6'));
        $form->setLib('exo_ta_7',_('exo_ta_7'));
        $form->setLib('exo_ta_8',_('exo_ta_8'));
        $form->setLib('exo_ta_9',_('exo_ta_9'));
        $form->setLib('exo_rap_1',_('exo_rap_1'));
        $form->setLib('exo_rap_2',_('exo_rap_2'));
        $form->setLib('exo_rap_3',_('exo_rap_3'));
        $form->setLib('exo_rap_4',_('exo_rap_4'));
        $form->setLib('exo_rap_5',_('exo_rap_5'));
        $form->setLib('exo_rap_6',_('exo_rap_6'));
        $form->setLib('exo_rap_7',_('exo_rap_7'));
        $form->setLib('exo_rap_8',_('exo_rap_8'));
        $form->setLib('mtn_exo_ta_part_commu',_('mtn_exo_ta_part_commu'));
        $form->setLib('mtn_exo_ta_part_depart',_('mtn_exo_ta_part_depart'));
        $form->setLib('mtn_exo_ta_part_reg',_('mtn_exo_ta_part_reg'));
        $form->setLib('mtn_exo_rap',_('mtn_exo_rap'));
        $form->setLib('dpc_type',_('dpc_type'));
        $form->setLib('dpc_desc_actv_ex',_('dpc_desc_actv_ex'));
        $form->setLib('dpc_desc_ca',_('dpc_desc_ca'));
        $form->setLib('dpc_desc_aut_prec',_('dpc_desc_aut_prec'));
        $form->setLib('dpc_desig_comm_arti',_('dpc_desig_comm_arti'));
        $form->setLib('dpc_desig_loc_hab',_('dpc_desig_loc_hab'));
        $form->setLib('dpc_desig_loc_ann',_('dpc_desig_loc_ann'));
        $form->setLib('dpc_desig_loc_ann_prec',_('dpc_desig_loc_ann_prec'));
        $form->setLib('dpc_bail_comm_date',_('dpc_bail_comm_date'));
        $form->setLib('dpc_bail_comm_loyer',_('dpc_bail_comm_loyer'));
        $form->setLib('dpc_actv_acqu',_('dpc_actv_acqu'));
        $form->setLib('dpc_nb_sala_di',_('dpc_nb_sala_di'));
        $form->setLib('dpc_nb_sala_dd',_('dpc_nb_sala_dd'));
        $form->setLib('dpc_nb_sala_tc',_('dpc_nb_sala_tc'));
        $form->setLib('dpc_nb_sala_tp',_('dpc_nb_sala_tp'));
        $form->setLib('dpc_moda_cess_vente_am',_('dpc_moda_cess_vente_am'));
        $form->setLib('dpc_moda_cess_adj',_('dpc_moda_cess_adj'));
        $form->setLib('dpc_moda_cess_prix',_('dpc_moda_cess_prix'));
        $form->setLib('dpc_moda_cess_adj_date',_('dpc_moda_cess_adj_date'));
        $form->setLib('dpc_moda_cess_adj_prec',_('dpc_moda_cess_adj_prec'));
        $form->setLib('dpc_moda_cess_paie_comp',_('dpc_moda_cess_paie_comp'));
        $form->setLib('dpc_moda_cess_paie_terme',_('dpc_moda_cess_paie_terme'));
        $form->setLib('dpc_moda_cess_paie_terme_prec',_('dpc_moda_cess_paie_terme_prec'));
        $form->setLib('dpc_moda_cess_paie_nat',_('dpc_moda_cess_paie_nat'));
        $form->setLib('dpc_moda_cess_paie_nat_desig_alien',_('dpc_moda_cess_paie_nat_desig_alien'));
        $form->setLib('dpc_moda_cess_paie_nat_desig_alien_prec',_('dpc_moda_cess_paie_nat_desig_alien_prec'));
        $form->setLib('dpc_moda_cess_paie_nat_eval',_('dpc_moda_cess_paie_nat_eval'));
        $form->setLib('dpc_moda_cess_paie_nat_eval_prec',_('dpc_moda_cess_paie_nat_eval_prec'));
        $form->setLib('dpc_moda_cess_paie_aut',_('dpc_moda_cess_paie_aut'));
        $form->setLib('dpc_moda_cess_paie_aut_prec',_('dpc_moda_cess_paie_aut_prec'));
        $form->setLib('dpc_ss_signe_demande_acqu',_('dpc_ss_signe_demande_acqu'));
        $form->setLib('dpc_ss_signe_recher_trouv_acqu',_('dpc_ss_signe_recher_trouv_acqu'));
        $form->setLib('dpc_notif_adr_prop',_('dpc_notif_adr_prop'));
        $form->setLib('dpc_notif_adr_manda',_('dpc_notif_adr_manda'));
        $form->setLib('dpc_obs',_('dpc_obs'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // architecte
        $this->init_select($form, $this->f->db, $maj, null, "architecte", $sql_architecte, $sql_architecte_by_id, false);
        // ctx_objet_recours
        $this->init_select($form, $this->f->db, $maj, null, "ctx_objet_recours", $sql_ctx_objet_recours, $sql_ctx_objet_recours_by_id, true);
        // dossier_autorisation
        $this->init_select($form, $this->f->db, $maj, null, "dossier_autorisation", $sql_dossier_autorisation, $sql_dossier_autorisation_by_id, false);
        // dossier_instruction
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction", $sql_dossier_instruction, $sql_dossier_instruction_by_id, false);
        // lot
        $this->init_select($form, $this->f->db, $maj, null, "lot", $sql_lot, $sql_lot_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('architecte', $this->retourformulaire))
                $form->setVal('architecte', $idxformulaire);
            if($this->is_in_context_of_foreign_key('objet_recours', $this->retourformulaire))
                $form->setVal('ctx_objet_recours', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_autorisation', $this->retourformulaire))
                $form->setVal('dossier_autorisation', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier', $this->retourformulaire))
                $form->setVal('dossier_instruction', $idxformulaire);
            if($this->is_in_context_of_foreign_key('lot', $this->retourformulaire))
                $form->setVal('lot', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : lien_donnees_techniques_moyen_retenu_juge
        $this->rechercheTable($this->f->db, "lien_donnees_techniques_moyen_retenu_juge", "donnees_techniques", $id);
        // Verification de la cle secondaire : lien_donnees_techniques_moyen_souleve
        $this->rechercheTable($this->f->db, "lien_donnees_techniques_moyen_souleve", "donnees_techniques", $id);
    }


}

?>
