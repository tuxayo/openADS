<?php
//$Id$ 
//gen openMairie le 15/06/2017 19:12

require_once "../obj/om_dbform.class.php";

class dossier_gen extends om_dbform {

    var $table = "dossier";
    var $clePrimaire = "dossier";
    var $typeCle = "A";
    var $required_field = array(
        "date_depot",
        "date_dernier_depot",
        "dossier",
        "dossier_autorisation",
        "dossier_instruction_type",
        "om_collectivite"
    );
    
    var $foreign_keys_extended = array(
        "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
        "autorite_competente" => array("autorite_competente", ),
        "avis_decision" => array("avis_decision", ),
        "division" => array("division", ),
        "dossier_autorisation" => array("dossier_autorisation", ),
        "dossier_instruction_type" => array("dossier_instruction_type", ),
        "etat" => array("etat", ),
        "evenement" => array("evenement", ),
        "instructeur" => array("instructeur", ),
        "om_collectivite" => array("om_collectivite", ),
        "quartier" => array("quartier", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        $this->valF['dossier'] = $val['dossier'];
        if ($val['annee'] == "") {
            $this->valF['annee'] = ""; // -> default
        } else {
            $this->valF['annee'] = $val['annee'];
        }
        if ($val['etat'] == "") {
            $this->valF['etat'] = NULL;
        } else {
            $this->valF['etat'] = $val['etat'];
        }
        if (!is_numeric($val['instructeur'])) {
            $this->valF['instructeur'] = NULL;
        } else {
            $this->valF['instructeur'] = $val['instructeur'];
        }
        if ($val['date_demande'] != "") {
            $this->valF['date_demande'] = $this->dateDB($val['date_demande']);
        } else {
            $this->valF['date_demande'] = NULL;
        }
        if ($val['date_depot'] != "") {
            $this->valF['date_depot'] = $this->dateDB($val['date_depot']);
        }
        if ($val['date_complet'] != "") {
            $this->valF['date_complet'] = $this->dateDB($val['date_complet']);
        } else {
            $this->valF['date_complet'] = NULL;
        }
        if ($val['date_rejet'] != "") {
            $this->valF['date_rejet'] = $this->dateDB($val['date_rejet']);
        } else {
            $this->valF['date_rejet'] = NULL;
        }
        if ($val['date_notification_delai'] != "") {
            $this->valF['date_notification_delai'] = $this->dateDB($val['date_notification_delai']);
        } else {
            $this->valF['date_notification_delai'] = NULL;
        }
        if (!is_numeric($val['delai'])) {
            $this->valF['delai'] = 0; // -> default
        } else {
            $this->valF['delai'] = $val['delai'];
        }
        if ($val['date_limite'] != "") {
            $this->valF['date_limite'] = $this->dateDB($val['date_limite']);
        } else {
            $this->valF['date_limite'] = NULL;
        }
        if ($val['accord_tacite'] == "") {
            $this->valF['accord_tacite'] = ""; // -> default
        } else {
            $this->valF['accord_tacite'] = $val['accord_tacite'];
        }
        if ($val['date_decision'] != "") {
            $this->valF['date_decision'] = $this->dateDB($val['date_decision']);
        } else {
            $this->valF['date_decision'] = NULL;
        }
        if ($val['date_validite'] != "") {
            $this->valF['date_validite'] = $this->dateDB($val['date_validite']);
        } else {
            $this->valF['date_validite'] = NULL;
        }
        if ($val['date_chantier'] != "") {
            $this->valF['date_chantier'] = $this->dateDB($val['date_chantier']);
        } else {
            $this->valF['date_chantier'] = NULL;
        }
        if ($val['date_achevement'] != "") {
            $this->valF['date_achevement'] = $this->dateDB($val['date_achevement']);
        } else {
            $this->valF['date_achevement'] = NULL;
        }
        if ($val['date_conformite'] != "") {
            $this->valF['date_conformite'] = $this->dateDB($val['date_conformite']);
        } else {
            $this->valF['date_conformite'] = NULL;
        }
            $this->valF['description'] = $val['description'];
        if ($val['erp'] == 1 || $val['erp'] == "t" || $val['erp'] == "Oui") {
            $this->valF['erp'] = true;
        } else {
            $this->valF['erp'] = false;
        }
        if (!is_numeric($val['avis_decision'])) {
            $this->valF['avis_decision'] = NULL;
        } else {
            $this->valF['avis_decision'] = $val['avis_decision'];
        }
        if ($val['enjeu_erp'] == 1 || $val['enjeu_erp'] == "t" || $val['enjeu_erp'] == "Oui") {
            $this->valF['enjeu_erp'] = true;
        } else {
            $this->valF['enjeu_erp'] = false;
        }
        if ($val['enjeu_urba'] == 1 || $val['enjeu_urba'] == "t" || $val['enjeu_urba'] == "Oui") {
            $this->valF['enjeu_urba'] = true;
        } else {
            $this->valF['enjeu_urba'] = false;
        }
        if (!is_numeric($val['division'])) {
            $this->valF['division'] = NULL;
        } else {
            $this->valF['division'] = $val['division'];
        }
        if (!is_numeric($val['autorite_competente'])) {
            $this->valF['autorite_competente'] = NULL;
        } else {
            $this->valF['autorite_competente'] = $val['autorite_competente'];
        }
        if ($val['a_qualifier'] == 1 || $val['a_qualifier'] == "t" || $val['a_qualifier'] == "Oui") {
            $this->valF['a_qualifier'] = true;
        } else {
            $this->valF['a_qualifier'] = false;
        }
            $this->valF['terrain_references_cadastrales'] = $val['terrain_references_cadastrales'];
        if ($val['terrain_adresse_voie_numero'] == "") {
            $this->valF['terrain_adresse_voie_numero'] = NULL;
        } else {
            $this->valF['terrain_adresse_voie_numero'] = $val['terrain_adresse_voie_numero'];
        }
        if ($val['terrain_adresse_voie'] == "") {
            $this->valF['terrain_adresse_voie'] = NULL;
        } else {
            $this->valF['terrain_adresse_voie'] = $val['terrain_adresse_voie'];
        }
        if ($val['terrain_adresse_lieu_dit'] == "") {
            $this->valF['terrain_adresse_lieu_dit'] = NULL;
        } else {
            $this->valF['terrain_adresse_lieu_dit'] = $val['terrain_adresse_lieu_dit'];
        }
        if ($val['terrain_adresse_localite'] == "") {
            $this->valF['terrain_adresse_localite'] = NULL;
        } else {
            $this->valF['terrain_adresse_localite'] = $val['terrain_adresse_localite'];
        }
        if ($val['terrain_adresse_code_postal'] == "") {
            $this->valF['terrain_adresse_code_postal'] = NULL;
        } else {
            $this->valF['terrain_adresse_code_postal'] = $val['terrain_adresse_code_postal'];
        }
        if ($val['terrain_adresse_bp'] == "") {
            $this->valF['terrain_adresse_bp'] = NULL;
        } else {
            $this->valF['terrain_adresse_bp'] = $val['terrain_adresse_bp'];
        }
        if ($val['terrain_adresse_cedex'] == "") {
            $this->valF['terrain_adresse_cedex'] = NULL;
        } else {
            $this->valF['terrain_adresse_cedex'] = $val['terrain_adresse_cedex'];
        }
        if (!is_numeric($val['terrain_superficie'])) {
            $this->valF['terrain_superficie'] = NULL;
        } else {
            $this->valF['terrain_superficie'] = $val['terrain_superficie'];
        }
        $this->valF['dossier_autorisation'] = $val['dossier_autorisation'];
        if (!is_numeric($val['dossier_instruction_type'])) {
            $this->valF['dossier_instruction_type'] = ""; // -> requis
        } else {
            $this->valF['dossier_instruction_type'] = $val['dossier_instruction_type'];
        }
        if ($val['date_dernier_depot'] != "") {
            $this->valF['date_dernier_depot'] = $this->dateDB($val['date_dernier_depot']);
        }
        if (!is_numeric($val['version'])) {
            $this->valF['version'] = NULL;
        } else {
            $this->valF['version'] = $val['version'];
        }
        if ($val['incompletude'] == 1 || $val['incompletude'] == "t" || $val['incompletude'] == "Oui") {
            $this->valF['incompletude'] = true;
        } else {
            $this->valF['incompletude'] = false;
        }
        if (!is_numeric($val['evenement_suivant_tacite'])) {
            $this->valF['evenement_suivant_tacite'] = NULL;
        } else {
            $this->valF['evenement_suivant_tacite'] = $val['evenement_suivant_tacite'];
        }
        if (!is_numeric($val['evenement_suivant_tacite_incompletude'])) {
            $this->valF['evenement_suivant_tacite_incompletude'] = NULL;
        } else {
            $this->valF['evenement_suivant_tacite_incompletude'] = $val['evenement_suivant_tacite_incompletude'];
        }
        if ($val['etat_pendant_incompletude'] == "") {
            $this->valF['etat_pendant_incompletude'] = NULL;
        } else {
            $this->valF['etat_pendant_incompletude'] = $val['etat_pendant_incompletude'];
        }
        if ($val['date_limite_incompletude'] != "") {
            $this->valF['date_limite_incompletude'] = $this->dateDB($val['date_limite_incompletude']);
        } else {
            $this->valF['date_limite_incompletude'] = NULL;
        }
        if (!is_numeric($val['delai_incompletude'])) {
            $this->valF['delai_incompletude'] = NULL;
        } else {
            $this->valF['delai_incompletude'] = $val['delai_incompletude'];
        }
        if ($val['dossier_libelle'] == "") {
            $this->valF['dossier_libelle'] = NULL;
        } else {
            $this->valF['dossier_libelle'] = $val['dossier_libelle'];
        }
        if ($val['numero_versement_archive'] == "") {
            $this->valF['numero_versement_archive'] = NULL;
        } else {
            $this->valF['numero_versement_archive'] = $val['numero_versement_archive'];
        }
        if (!is_numeric($val['duree_validite'])) {
            $this->valF['duree_validite'] = 0; // -> default
        } else {
            $this->valF['duree_validite'] = $val['duree_validite'];
        }
        if (!is_numeric($val['quartier'])) {
            $this->valF['quartier'] = NULL;
        } else {
            $this->valF['quartier'] = $val['quartier'];
        }
        if ($val['incomplet_notifie'] == 1 || $val['incomplet_notifie'] == "t" || $val['incomplet_notifie'] == "Oui") {
            $this->valF['incomplet_notifie'] = true;
        } else {
            $this->valF['incomplet_notifie'] = false;
        }
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
        if (!is_numeric($val['tax_secteur'])) {
            $this->valF['tax_secteur'] = NULL;
        } else {
            $this->valF['tax_secteur'] = $val['tax_secteur'];
        }
        if (!is_numeric($val['tax_mtn_part_commu'])) {
            $this->valF['tax_mtn_part_commu'] = NULL;
        } else {
            $this->valF['tax_mtn_part_commu'] = $val['tax_mtn_part_commu'];
        }
        if (!is_numeric($val['tax_mtn_part_depart'])) {
            $this->valF['tax_mtn_part_depart'] = NULL;
        } else {
            $this->valF['tax_mtn_part_depart'] = $val['tax_mtn_part_depart'];
        }
        if (!is_numeric($val['tax_mtn_part_reg'])) {
            $this->valF['tax_mtn_part_reg'] = NULL;
        } else {
            $this->valF['tax_mtn_part_reg'] = $val['tax_mtn_part_reg'];
        }
        if (!is_numeric($val['tax_mtn_total'])) {
            $this->valF['tax_mtn_total'] = NULL;
        } else {
            $this->valF['tax_mtn_total'] = $val['tax_mtn_total'];
        }
            $this->valF['log_instructions'] = $val['log_instructions'];
        if ($val['interface_referentiel_erp'] == 1 || $val['interface_referentiel_erp'] == "t" || $val['interface_referentiel_erp'] == "Oui") {
            $this->valF['interface_referentiel_erp'] = true;
        } else {
            $this->valF['interface_referentiel_erp'] = false;
        }
        if ($val['autorisation_contestee'] == "") {
            $this->valF['autorisation_contestee'] = NULL;
        } else {
            $this->valF['autorisation_contestee'] = $val['autorisation_contestee'];
        }
        if ($val['date_cloture_instruction'] != "") {
            $this->valF['date_cloture_instruction'] = $this->dateDB($val['date_cloture_instruction']);
        } else {
            $this->valF['date_cloture_instruction'] = NULL;
        }
        if ($val['date_premiere_visite'] != "") {
            $this->valF['date_premiere_visite'] = $this->dateDB($val['date_premiere_visite']);
        } else {
            $this->valF['date_premiere_visite'] = NULL;
        }
        if ($val['date_derniere_visite'] != "") {
            $this->valF['date_derniere_visite'] = $this->dateDB($val['date_derniere_visite']);
        } else {
            $this->valF['date_derniere_visite'] = NULL;
        }
        if ($val['date_contradictoire'] != "") {
            $this->valF['date_contradictoire'] = $this->dateDB($val['date_contradictoire']);
        } else {
            $this->valF['date_contradictoire'] = NULL;
        }
        if ($val['date_retour_contradictoire'] != "") {
            $this->valF['date_retour_contradictoire'] = $this->dateDB($val['date_retour_contradictoire']);
        } else {
            $this->valF['date_retour_contradictoire'] = NULL;
        }
        if ($val['date_ait'] != "") {
            $this->valF['date_ait'] = $this->dateDB($val['date_ait']);
        } else {
            $this->valF['date_ait'] = NULL;
        }
        if ($val['date_transmission_parquet'] != "") {
            $this->valF['date_transmission_parquet'] = $this->dateDB($val['date_transmission_parquet']);
        } else {
            $this->valF['date_transmission_parquet'] = NULL;
        }
        if (!is_numeric($val['instructeur_2'])) {
            $this->valF['instructeur_2'] = NULL;
        } else {
            $this->valF['instructeur_2'] = $val['instructeur_2'];
        }
        if (!is_numeric($val['tax_mtn_rap'])) {
            $this->valF['tax_mtn_rap'] = NULL;
        } else {
            $this->valF['tax_mtn_rap'] = $val['tax_mtn_rap'];
        }
        if (!is_numeric($val['tax_mtn_part_commu_sans_exo'])) {
            $this->valF['tax_mtn_part_commu_sans_exo'] = NULL;
        } else {
            $this->valF['tax_mtn_part_commu_sans_exo'] = $val['tax_mtn_part_commu_sans_exo'];
        }
        if (!is_numeric($val['tax_mtn_part_depart_sans_exo'])) {
            $this->valF['tax_mtn_part_depart_sans_exo'] = NULL;
        } else {
            $this->valF['tax_mtn_part_depart_sans_exo'] = $val['tax_mtn_part_depart_sans_exo'];
        }
        if (!is_numeric($val['tax_mtn_part_reg_sans_exo'])) {
            $this->valF['tax_mtn_part_reg_sans_exo'] = NULL;
        } else {
            $this->valF['tax_mtn_part_reg_sans_exo'] = $val['tax_mtn_part_reg_sans_exo'];
        }
        if (!is_numeric($val['tax_mtn_total_sans_exo'])) {
            $this->valF['tax_mtn_total_sans_exo'] = NULL;
        } else {
            $this->valF['tax_mtn_total_sans_exo'] = $val['tax_mtn_total_sans_exo'];
        }
        if (!is_numeric($val['tax_mtn_rap_sans_exo'])) {
            $this->valF['tax_mtn_rap_sans_exo'] = NULL;
        } else {
            $this->valF['tax_mtn_rap_sans_exo'] = $val['tax_mtn_rap_sans_exo'];
        }
        if ($val['geom'] == "") {
            unset($this->valF['geom']);
        } else {
            $this->valF['geom'] = $val['geom'];
        }
        if ($val['geom1'] == "") {
            unset($this->valF['geom1']);
        } else {
            $this->valF['geom1'] = $val['geom1'];
        }
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier", "text");
            $form->setType("annee", "text");
            if ($this->is_in_context_of_foreign_key("etat", $this->retourformulaire)) {
                $form->setType("etat", "selecthiddenstatic");
            } else {
                $form->setType("etat", "select");
            }
            if ($this->is_in_context_of_foreign_key("instructeur", $this->retourformulaire)) {
                $form->setType("instructeur", "selecthiddenstatic");
            } else {
                $form->setType("instructeur", "select");
            }
            $form->setType("date_demande", "date");
            $form->setType("date_depot", "date");
            $form->setType("date_complet", "date");
            $form->setType("date_rejet", "date");
            $form->setType("date_notification_delai", "date");
            $form->setType("delai", "text");
            $form->setType("date_limite", "date");
            $form->setType("accord_tacite", "text");
            $form->setType("date_decision", "date");
            $form->setType("date_validite", "date");
            $form->setType("date_chantier", "date");
            $form->setType("date_achevement", "date");
            $form->setType("date_conformite", "date");
            $form->setType("description", "textarea");
            $form->setType("erp", "checkbox");
            if ($this->is_in_context_of_foreign_key("avis_decision", $this->retourformulaire)) {
                $form->setType("avis_decision", "selecthiddenstatic");
            } else {
                $form->setType("avis_decision", "select");
            }
            $form->setType("enjeu_erp", "checkbox");
            $form->setType("enjeu_urba", "checkbox");
            if ($this->is_in_context_of_foreign_key("division", $this->retourformulaire)) {
                $form->setType("division", "selecthiddenstatic");
            } else {
                $form->setType("division", "select");
            }
            if ($this->is_in_context_of_foreign_key("autorite_competente", $this->retourformulaire)) {
                $form->setType("autorite_competente", "selecthiddenstatic");
            } else {
                $form->setType("autorite_competente", "select");
            }
            $form->setType("a_qualifier", "checkbox");
            $form->setType("terrain_references_cadastrales", "textarea");
            $form->setType("terrain_adresse_voie_numero", "text");
            $form->setType("terrain_adresse_voie", "text");
            $form->setType("terrain_adresse_lieu_dit", "text");
            $form->setType("terrain_adresse_localite", "text");
            $form->setType("terrain_adresse_code_postal", "text");
            $form->setType("terrain_adresse_bp", "text");
            $form->setType("terrain_adresse_cedex", "text");
            $form->setType("terrain_superficie", "text");
            if ($this->is_in_context_of_foreign_key("dossier_autorisation", $this->retourformulaire)) {
                $form->setType("dossier_autorisation", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_type", $this->retourformulaire)) {
                $form->setType("dossier_instruction_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_type", "select");
            }
            $form->setType("date_dernier_depot", "date");
            $form->setType("version", "text");
            $form->setType("incompletude", "checkbox");
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement_suivant_tacite", "selecthiddenstatic");
            } else {
                $form->setType("evenement_suivant_tacite", "select");
            }
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement_suivant_tacite_incompletude", "selecthiddenstatic");
            } else {
                $form->setType("evenement_suivant_tacite_incompletude", "select");
            }
            if ($this->is_in_context_of_foreign_key("etat", $this->retourformulaire)) {
                $form->setType("etat_pendant_incompletude", "selecthiddenstatic");
            } else {
                $form->setType("etat_pendant_incompletude", "select");
            }
            $form->setType("date_limite_incompletude", "date");
            $form->setType("delai_incompletude", "text");
            $form->setType("dossier_libelle", "text");
            $form->setType("numero_versement_archive", "text");
            $form->setType("duree_validite", "text");
            if ($this->is_in_context_of_foreign_key("quartier", $this->retourformulaire)) {
                $form->setType("quartier", "selecthiddenstatic");
            } else {
                $form->setType("quartier", "select");
            }
            $form->setType("incomplet_notifie", "checkbox");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("tax_secteur", "text");
            $form->setType("tax_mtn_part_commu", "text");
            $form->setType("tax_mtn_part_depart", "text");
            $form->setType("tax_mtn_part_reg", "text");
            $form->setType("tax_mtn_total", "text");
            $form->setType("log_instructions", "textarea");
            $form->setType("interface_referentiel_erp", "checkbox");
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("autorisation_contestee", "selecthiddenstatic");
            } else {
                $form->setType("autorisation_contestee", "select");
            }
            $form->setType("date_cloture_instruction", "date");
            $form->setType("date_premiere_visite", "date");
            $form->setType("date_derniere_visite", "date");
            $form->setType("date_contradictoire", "date");
            $form->setType("date_retour_contradictoire", "date");
            $form->setType("date_ait", "date");
            $form->setType("date_transmission_parquet", "date");
            if ($this->is_in_context_of_foreign_key("instructeur", $this->retourformulaire)) {
                $form->setType("instructeur_2", "selecthiddenstatic");
            } else {
                $form->setType("instructeur_2", "select");
            }
            $form->setType("tax_mtn_rap", "text");
            $form->setType("tax_mtn_part_commu_sans_exo", "text");
            $form->setType("tax_mtn_part_depart_sans_exo", "text");
            $form->setType("tax_mtn_part_reg_sans_exo", "text");
            $form->setType("tax_mtn_total_sans_exo", "text");
            $form->setType("tax_mtn_rap_sans_exo", "text");
            $form->setType("geom", "geom");
            $form->setType("geom1", "geom");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier", "hiddenstatic");
            $form->setType("annee", "text");
            if ($this->is_in_context_of_foreign_key("etat", $this->retourformulaire)) {
                $form->setType("etat", "selecthiddenstatic");
            } else {
                $form->setType("etat", "select");
            }
            if ($this->is_in_context_of_foreign_key("instructeur", $this->retourformulaire)) {
                $form->setType("instructeur", "selecthiddenstatic");
            } else {
                $form->setType("instructeur", "select");
            }
            $form->setType("date_demande", "date");
            $form->setType("date_depot", "date");
            $form->setType("date_complet", "date");
            $form->setType("date_rejet", "date");
            $form->setType("date_notification_delai", "date");
            $form->setType("delai", "text");
            $form->setType("date_limite", "date");
            $form->setType("accord_tacite", "text");
            $form->setType("date_decision", "date");
            $form->setType("date_validite", "date");
            $form->setType("date_chantier", "date");
            $form->setType("date_achevement", "date");
            $form->setType("date_conformite", "date");
            $form->setType("description", "textarea");
            $form->setType("erp", "checkbox");
            if ($this->is_in_context_of_foreign_key("avis_decision", $this->retourformulaire)) {
                $form->setType("avis_decision", "selecthiddenstatic");
            } else {
                $form->setType("avis_decision", "select");
            }
            $form->setType("enjeu_erp", "checkbox");
            $form->setType("enjeu_urba", "checkbox");
            if ($this->is_in_context_of_foreign_key("division", $this->retourformulaire)) {
                $form->setType("division", "selecthiddenstatic");
            } else {
                $form->setType("division", "select");
            }
            if ($this->is_in_context_of_foreign_key("autorite_competente", $this->retourformulaire)) {
                $form->setType("autorite_competente", "selecthiddenstatic");
            } else {
                $form->setType("autorite_competente", "select");
            }
            $form->setType("a_qualifier", "checkbox");
            $form->setType("terrain_references_cadastrales", "textarea");
            $form->setType("terrain_adresse_voie_numero", "text");
            $form->setType("terrain_adresse_voie", "text");
            $form->setType("terrain_adresse_lieu_dit", "text");
            $form->setType("terrain_adresse_localite", "text");
            $form->setType("terrain_adresse_code_postal", "text");
            $form->setType("terrain_adresse_bp", "text");
            $form->setType("terrain_adresse_cedex", "text");
            $form->setType("terrain_superficie", "text");
            if ($this->is_in_context_of_foreign_key("dossier_autorisation", $this->retourformulaire)) {
                $form->setType("dossier_autorisation", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_type", $this->retourformulaire)) {
                $form->setType("dossier_instruction_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_type", "select");
            }
            $form->setType("date_dernier_depot", "date");
            $form->setType("version", "text");
            $form->setType("incompletude", "checkbox");
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement_suivant_tacite", "selecthiddenstatic");
            } else {
                $form->setType("evenement_suivant_tacite", "select");
            }
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement_suivant_tacite_incompletude", "selecthiddenstatic");
            } else {
                $form->setType("evenement_suivant_tacite_incompletude", "select");
            }
            if ($this->is_in_context_of_foreign_key("etat", $this->retourformulaire)) {
                $form->setType("etat_pendant_incompletude", "selecthiddenstatic");
            } else {
                $form->setType("etat_pendant_incompletude", "select");
            }
            $form->setType("date_limite_incompletude", "date");
            $form->setType("delai_incompletude", "text");
            $form->setType("dossier_libelle", "text");
            $form->setType("numero_versement_archive", "text");
            $form->setType("duree_validite", "text");
            if ($this->is_in_context_of_foreign_key("quartier", $this->retourformulaire)) {
                $form->setType("quartier", "selecthiddenstatic");
            } else {
                $form->setType("quartier", "select");
            }
            $form->setType("incomplet_notifie", "checkbox");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("tax_secteur", "text");
            $form->setType("tax_mtn_part_commu", "text");
            $form->setType("tax_mtn_part_depart", "text");
            $form->setType("tax_mtn_part_reg", "text");
            $form->setType("tax_mtn_total", "text");
            $form->setType("log_instructions", "textarea");
            $form->setType("interface_referentiel_erp", "checkbox");
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("autorisation_contestee", "selecthiddenstatic");
            } else {
                $form->setType("autorisation_contestee", "select");
            }
            $form->setType("date_cloture_instruction", "date");
            $form->setType("date_premiere_visite", "date");
            $form->setType("date_derniere_visite", "date");
            $form->setType("date_contradictoire", "date");
            $form->setType("date_retour_contradictoire", "date");
            $form->setType("date_ait", "date");
            $form->setType("date_transmission_parquet", "date");
            if ($this->is_in_context_of_foreign_key("instructeur", $this->retourformulaire)) {
                $form->setType("instructeur_2", "selecthiddenstatic");
            } else {
                $form->setType("instructeur_2", "select");
            }
            $form->setType("tax_mtn_rap", "text");
            $form->setType("tax_mtn_part_commu_sans_exo", "text");
            $form->setType("tax_mtn_part_depart_sans_exo", "text");
            $form->setType("tax_mtn_part_reg_sans_exo", "text");
            $form->setType("tax_mtn_total_sans_exo", "text");
            $form->setType("tax_mtn_rap_sans_exo", "text");
            $form->setType("geom", "geom");
            $form->setType("geom1", "geom");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier", "hiddenstatic");
            $form->setType("annee", "hiddenstatic");
            $form->setType("etat", "selectstatic");
            $form->setType("instructeur", "selectstatic");
            $form->setType("date_demande", "hiddenstatic");
            $form->setType("date_depot", "hiddenstatic");
            $form->setType("date_complet", "hiddenstatic");
            $form->setType("date_rejet", "hiddenstatic");
            $form->setType("date_notification_delai", "hiddenstatic");
            $form->setType("delai", "hiddenstatic");
            $form->setType("date_limite", "hiddenstatic");
            $form->setType("accord_tacite", "hiddenstatic");
            $form->setType("date_decision", "hiddenstatic");
            $form->setType("date_validite", "hiddenstatic");
            $form->setType("date_chantier", "hiddenstatic");
            $form->setType("date_achevement", "hiddenstatic");
            $form->setType("date_conformite", "hiddenstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("erp", "hiddenstatic");
            $form->setType("avis_decision", "selectstatic");
            $form->setType("enjeu_erp", "hiddenstatic");
            $form->setType("enjeu_urba", "hiddenstatic");
            $form->setType("division", "selectstatic");
            $form->setType("autorite_competente", "selectstatic");
            $form->setType("a_qualifier", "hiddenstatic");
            $form->setType("terrain_references_cadastrales", "hiddenstatic");
            $form->setType("terrain_adresse_voie_numero", "hiddenstatic");
            $form->setType("terrain_adresse_voie", "hiddenstatic");
            $form->setType("terrain_adresse_lieu_dit", "hiddenstatic");
            $form->setType("terrain_adresse_localite", "hiddenstatic");
            $form->setType("terrain_adresse_code_postal", "hiddenstatic");
            $form->setType("terrain_adresse_bp", "hiddenstatic");
            $form->setType("terrain_adresse_cedex", "hiddenstatic");
            $form->setType("terrain_superficie", "hiddenstatic");
            $form->setType("dossier_autorisation", "selectstatic");
            $form->setType("dossier_instruction_type", "selectstatic");
            $form->setType("date_dernier_depot", "hiddenstatic");
            $form->setType("version", "hiddenstatic");
            $form->setType("incompletude", "hiddenstatic");
            $form->setType("evenement_suivant_tacite", "selectstatic");
            $form->setType("evenement_suivant_tacite_incompletude", "selectstatic");
            $form->setType("etat_pendant_incompletude", "selectstatic");
            $form->setType("date_limite_incompletude", "hiddenstatic");
            $form->setType("delai_incompletude", "hiddenstatic");
            $form->setType("dossier_libelle", "hiddenstatic");
            $form->setType("numero_versement_archive", "hiddenstatic");
            $form->setType("duree_validite", "hiddenstatic");
            $form->setType("quartier", "selectstatic");
            $form->setType("incomplet_notifie", "hiddenstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
            $form->setType("tax_secteur", "hiddenstatic");
            $form->setType("tax_mtn_part_commu", "hiddenstatic");
            $form->setType("tax_mtn_part_depart", "hiddenstatic");
            $form->setType("tax_mtn_part_reg", "hiddenstatic");
            $form->setType("tax_mtn_total", "hiddenstatic");
            $form->setType("log_instructions", "hiddenstatic");
            $form->setType("interface_referentiel_erp", "hiddenstatic");
            $form->setType("autorisation_contestee", "selectstatic");
            $form->setType("date_cloture_instruction", "hiddenstatic");
            $form->setType("date_premiere_visite", "hiddenstatic");
            $form->setType("date_derniere_visite", "hiddenstatic");
            $form->setType("date_contradictoire", "hiddenstatic");
            $form->setType("date_retour_contradictoire", "hiddenstatic");
            $form->setType("date_ait", "hiddenstatic");
            $form->setType("date_transmission_parquet", "hiddenstatic");
            $form->setType("instructeur_2", "selectstatic");
            $form->setType("tax_mtn_rap", "hiddenstatic");
            $form->setType("tax_mtn_part_commu_sans_exo", "hiddenstatic");
            $form->setType("tax_mtn_part_depart_sans_exo", "hiddenstatic");
            $form->setType("tax_mtn_part_reg_sans_exo", "hiddenstatic");
            $form->setType("tax_mtn_total_sans_exo", "hiddenstatic");
            $form->setType("tax_mtn_rap_sans_exo", "hiddenstatic");
            $form->setType("geom", "geom");
            $form->setType("geom1", "geom");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier", "static");
            $form->setType("annee", "static");
            $form->setType("etat", "selectstatic");
            $form->setType("instructeur", "selectstatic");
            $form->setType("date_demande", "datestatic");
            $form->setType("date_depot", "datestatic");
            $form->setType("date_complet", "datestatic");
            $form->setType("date_rejet", "datestatic");
            $form->setType("date_notification_delai", "datestatic");
            $form->setType("delai", "static");
            $form->setType("date_limite", "datestatic");
            $form->setType("accord_tacite", "static");
            $form->setType("date_decision", "datestatic");
            $form->setType("date_validite", "datestatic");
            $form->setType("date_chantier", "datestatic");
            $form->setType("date_achevement", "datestatic");
            $form->setType("date_conformite", "datestatic");
            $form->setType("description", "textareastatic");
            $form->setType("erp", "checkboxstatic");
            $form->setType("avis_decision", "selectstatic");
            $form->setType("enjeu_erp", "checkboxstatic");
            $form->setType("enjeu_urba", "checkboxstatic");
            $form->setType("division", "selectstatic");
            $form->setType("autorite_competente", "selectstatic");
            $form->setType("a_qualifier", "checkboxstatic");
            $form->setType("terrain_references_cadastrales", "textareastatic");
            $form->setType("terrain_adresse_voie_numero", "static");
            $form->setType("terrain_adresse_voie", "static");
            $form->setType("terrain_adresse_lieu_dit", "static");
            $form->setType("terrain_adresse_localite", "static");
            $form->setType("terrain_adresse_code_postal", "static");
            $form->setType("terrain_adresse_bp", "static");
            $form->setType("terrain_adresse_cedex", "static");
            $form->setType("terrain_superficie", "static");
            $form->setType("dossier_autorisation", "selectstatic");
            $form->setType("dossier_instruction_type", "selectstatic");
            $form->setType("date_dernier_depot", "datestatic");
            $form->setType("version", "static");
            $form->setType("incompletude", "checkboxstatic");
            $form->setType("evenement_suivant_tacite", "selectstatic");
            $form->setType("evenement_suivant_tacite_incompletude", "selectstatic");
            $form->setType("etat_pendant_incompletude", "selectstatic");
            $form->setType("date_limite_incompletude", "datestatic");
            $form->setType("delai_incompletude", "static");
            $form->setType("dossier_libelle", "static");
            $form->setType("numero_versement_archive", "static");
            $form->setType("duree_validite", "static");
            $form->setType("quartier", "selectstatic");
            $form->setType("incomplet_notifie", "checkboxstatic");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("tax_secteur", "static");
            $form->setType("tax_mtn_part_commu", "static");
            $form->setType("tax_mtn_part_depart", "static");
            $form->setType("tax_mtn_part_reg", "static");
            $form->setType("tax_mtn_total", "static");
            $form->setType("log_instructions", "textareastatic");
            $form->setType("interface_referentiel_erp", "checkboxstatic");
            $form->setType("autorisation_contestee", "selectstatic");
            $form->setType("date_cloture_instruction", "datestatic");
            $form->setType("date_premiere_visite", "datestatic");
            $form->setType("date_derniere_visite", "datestatic");
            $form->setType("date_contradictoire", "datestatic");
            $form->setType("date_retour_contradictoire", "datestatic");
            $form->setType("date_ait", "datestatic");
            $form->setType("date_transmission_parquet", "datestatic");
            $form->setType("instructeur_2", "selectstatic");
            $form->setType("tax_mtn_rap", "static");
            $form->setType("tax_mtn_part_commu_sans_exo", "static");
            $form->setType("tax_mtn_part_depart_sans_exo", "static");
            $form->setType("tax_mtn_part_reg_sans_exo", "static");
            $form->setType("tax_mtn_total_sans_exo", "static");
            $form->setType("tax_mtn_rap_sans_exo", "static");
            $form->setType("geom", "geom");
            $form->setType("geom1", "geom");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('instructeur','VerifNum(this)');
        $form->setOnchange('date_demande','fdate(this)');
        $form->setOnchange('date_depot','fdate(this)');
        $form->setOnchange('date_complet','fdate(this)');
        $form->setOnchange('date_rejet','fdate(this)');
        $form->setOnchange('date_notification_delai','fdate(this)');
        $form->setOnchange('delai','VerifNum(this)');
        $form->setOnchange('date_limite','fdate(this)');
        $form->setOnchange('date_decision','fdate(this)');
        $form->setOnchange('date_validite','fdate(this)');
        $form->setOnchange('date_chantier','fdate(this)');
        $form->setOnchange('date_achevement','fdate(this)');
        $form->setOnchange('date_conformite','fdate(this)');
        $form->setOnchange('avis_decision','VerifNum(this)');
        $form->setOnchange('division','VerifNum(this)');
        $form->setOnchange('autorite_competente','VerifNum(this)');
        $form->setOnchange('terrain_superficie','VerifFloat(this)');
        $form->setOnchange('dossier_instruction_type','VerifNum(this)');
        $form->setOnchange('date_dernier_depot','fdate(this)');
        $form->setOnchange('version','VerifNum(this)');
        $form->setOnchange('evenement_suivant_tacite','VerifNum(this)');
        $form->setOnchange('evenement_suivant_tacite_incompletude','VerifNum(this)');
        $form->setOnchange('date_limite_incompletude','fdate(this)');
        $form->setOnchange('delai_incompletude','VerifNum(this)');
        $form->setOnchange('duree_validite','VerifNum(this)');
        $form->setOnchange('quartier','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
        $form->setOnchange('tax_secteur','VerifNum(this)');
        $form->setOnchange('tax_mtn_part_commu','VerifFloat(this)');
        $form->setOnchange('tax_mtn_part_depart','VerifFloat(this)');
        $form->setOnchange('tax_mtn_part_reg','VerifFloat(this)');
        $form->setOnchange('tax_mtn_total','VerifFloat(this)');
        $form->setOnchange('date_cloture_instruction','fdate(this)');
        $form->setOnchange('date_premiere_visite','fdate(this)');
        $form->setOnchange('date_derniere_visite','fdate(this)');
        $form->setOnchange('date_contradictoire','fdate(this)');
        $form->setOnchange('date_retour_contradictoire','fdate(this)');
        $form->setOnchange('date_ait','fdate(this)');
        $form->setOnchange('date_transmission_parquet','fdate(this)');
        $form->setOnchange('instructeur_2','VerifNum(this)');
        $form->setOnchange('tax_mtn_rap','VerifFloat(this)');
        $form->setOnchange('tax_mtn_part_commu_sans_exo','VerifFloat(this)');
        $form->setOnchange('tax_mtn_part_depart_sans_exo','VerifFloat(this)');
        $form->setOnchange('tax_mtn_part_reg_sans_exo','VerifFloat(this)');
        $form->setOnchange('tax_mtn_total_sans_exo','VerifFloat(this)');
        $form->setOnchange('tax_mtn_rap_sans_exo','VerifFloat(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier", 30);
        $form->setTaille("annee", 10);
        $form->setTaille("etat", 20);
        $form->setTaille("instructeur", 11);
        $form->setTaille("date_demande", 12);
        $form->setTaille("date_depot", 12);
        $form->setTaille("date_complet", 12);
        $form->setTaille("date_rejet", 12);
        $form->setTaille("date_notification_delai", 12);
        $form->setTaille("delai", 11);
        $form->setTaille("date_limite", 12);
        $form->setTaille("accord_tacite", 10);
        $form->setTaille("date_decision", 12);
        $form->setTaille("date_validite", 12);
        $form->setTaille("date_chantier", 12);
        $form->setTaille("date_achevement", 12);
        $form->setTaille("date_conformite", 12);
        $form->setTaille("description", 80);
        $form->setTaille("erp", 1);
        $form->setTaille("avis_decision", 11);
        $form->setTaille("enjeu_erp", 1);
        $form->setTaille("enjeu_urba", 1);
        $form->setTaille("division", 11);
        $form->setTaille("autorite_competente", 11);
        $form->setTaille("a_qualifier", 1);
        $form->setTaille("terrain_references_cadastrales", 80);
        $form->setTaille("terrain_adresse_voie_numero", 20);
        $form->setTaille("terrain_adresse_voie", 30);
        $form->setTaille("terrain_adresse_lieu_dit", 30);
        $form->setTaille("terrain_adresse_localite", 30);
        $form->setTaille("terrain_adresse_code_postal", 10);
        $form->setTaille("terrain_adresse_bp", 15);
        $form->setTaille("terrain_adresse_cedex", 15);
        $form->setTaille("terrain_superficie", 20);
        $form->setTaille("dossier_autorisation", 20);
        $form->setTaille("dossier_instruction_type", 11);
        $form->setTaille("date_dernier_depot", 12);
        $form->setTaille("version", 11);
        $form->setTaille("incompletude", 1);
        $form->setTaille("evenement_suivant_tacite", 11);
        $form->setTaille("evenement_suivant_tacite_incompletude", 11);
        $form->setTaille("etat_pendant_incompletude", 20);
        $form->setTaille("date_limite_incompletude", 12);
        $form->setTaille("delai_incompletude", 11);
        $form->setTaille("dossier_libelle", 25);
        $form->setTaille("numero_versement_archive", 30);
        $form->setTaille("duree_validite", 11);
        $form->setTaille("quartier", 11);
        $form->setTaille("incomplet_notifie", 1);
        $form->setTaille("om_collectivite", 11);
        $form->setTaille("tax_secteur", 11);
        $form->setTaille("tax_mtn_part_commu", 10);
        $form->setTaille("tax_mtn_part_depart", 10);
        $form->setTaille("tax_mtn_part_reg", 10);
        $form->setTaille("tax_mtn_total", 10);
        $form->setTaille("log_instructions", 80);
        $form->setTaille("interface_referentiel_erp", 1);
        $form->setTaille("autorisation_contestee", 30);
        $form->setTaille("date_cloture_instruction", 12);
        $form->setTaille("date_premiere_visite", 12);
        $form->setTaille("date_derniere_visite", 12);
        $form->setTaille("date_contradictoire", 12);
        $form->setTaille("date_retour_contradictoire", 12);
        $form->setTaille("date_ait", 12);
        $form->setTaille("date_transmission_parquet", 12);
        $form->setTaille("instructeur_2", 11);
        $form->setTaille("tax_mtn_rap", 10);
        $form->setTaille("tax_mtn_part_commu_sans_exo", 10);
        $form->setTaille("tax_mtn_part_depart_sans_exo", 10);
        $form->setTaille("tax_mtn_part_reg_sans_exo", 10);
        $form->setTaille("tax_mtn_total_sans_exo", 10);
        $form->setTaille("tax_mtn_rap_sans_exo", 10);
        $form->setTaille("geom", 30);
        $form->setTaille("geom1", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier", 30);
        $form->setMax("annee", 2);
        $form->setMax("etat", 20);
        $form->setMax("instructeur", 11);
        $form->setMax("date_demande", 12);
        $form->setMax("date_depot", 12);
        $form->setMax("date_complet", 12);
        $form->setMax("date_rejet", 12);
        $form->setMax("date_notification_delai", 12);
        $form->setMax("delai", 11);
        $form->setMax("date_limite", 12);
        $form->setMax("accord_tacite", 3);
        $form->setMax("date_decision", 12);
        $form->setMax("date_validite", 12);
        $form->setMax("date_chantier", 12);
        $form->setMax("date_achevement", 12);
        $form->setMax("date_conformite", 12);
        $form->setMax("description", 6);
        $form->setMax("erp", 1);
        $form->setMax("avis_decision", 11);
        $form->setMax("enjeu_erp", 1);
        $form->setMax("enjeu_urba", 1);
        $form->setMax("division", 11);
        $form->setMax("autorite_competente", 11);
        $form->setMax("a_qualifier", 1);
        $form->setMax("terrain_references_cadastrales", 6);
        $form->setMax("terrain_adresse_voie_numero", 20);
        $form->setMax("terrain_adresse_voie", 30);
        $form->setMax("terrain_adresse_lieu_dit", 30);
        $form->setMax("terrain_adresse_localite", 30);
        $form->setMax("terrain_adresse_code_postal", 5);
        $form->setMax("terrain_adresse_bp", 15);
        $form->setMax("terrain_adresse_cedex", 15);
        $form->setMax("terrain_superficie", 20);
        $form->setMax("dossier_autorisation", 20);
        $form->setMax("dossier_instruction_type", 11);
        $form->setMax("date_dernier_depot", 12);
        $form->setMax("version", 11);
        $form->setMax("incompletude", 1);
        $form->setMax("evenement_suivant_tacite", 11);
        $form->setMax("evenement_suivant_tacite_incompletude", 11);
        $form->setMax("etat_pendant_incompletude", 20);
        $form->setMax("date_limite_incompletude", 12);
        $form->setMax("delai_incompletude", 11);
        $form->setMax("dossier_libelle", 25);
        $form->setMax("numero_versement_archive", 100);
        $form->setMax("duree_validite", 11);
        $form->setMax("quartier", 11);
        $form->setMax("incomplet_notifie", 1);
        $form->setMax("om_collectivite", 11);
        $form->setMax("tax_secteur", 11);
        $form->setMax("tax_mtn_part_commu", -5);
        $form->setMax("tax_mtn_part_depart", -5);
        $form->setMax("tax_mtn_part_reg", -5);
        $form->setMax("tax_mtn_total", -5);
        $form->setMax("log_instructions", 6);
        $form->setMax("interface_referentiel_erp", 1);
        $form->setMax("autorisation_contestee", 30);
        $form->setMax("date_cloture_instruction", 12);
        $form->setMax("date_premiere_visite", 12);
        $form->setMax("date_derniere_visite", 12);
        $form->setMax("date_contradictoire", 12);
        $form->setMax("date_retour_contradictoire", 12);
        $form->setMax("date_ait", 12);
        $form->setMax("date_transmission_parquet", 12);
        $form->setMax("instructeur_2", 11);
        $form->setMax("tax_mtn_rap", -5);
        $form->setMax("tax_mtn_part_commu_sans_exo", -5);
        $form->setMax("tax_mtn_part_depart_sans_exo", -5);
        $form->setMax("tax_mtn_part_reg_sans_exo", -5);
        $form->setMax("tax_mtn_total_sans_exo", -5);
        $form->setMax("tax_mtn_rap_sans_exo", -5);
        $form->setMax("geom", 551424);
        $form->setMax("geom1", 551444);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier',_('dossier'));
        $form->setLib('annee',_('annee'));
        $form->setLib('etat',_('etat'));
        $form->setLib('instructeur',_('instructeur'));
        $form->setLib('date_demande',_('date_demande'));
        $form->setLib('date_depot',_('date_depot'));
        $form->setLib('date_complet',_('date_complet'));
        $form->setLib('date_rejet',_('date_rejet'));
        $form->setLib('date_notification_delai',_('date_notification_delai'));
        $form->setLib('delai',_('delai'));
        $form->setLib('date_limite',_('date_limite'));
        $form->setLib('accord_tacite',_('accord_tacite'));
        $form->setLib('date_decision',_('date_decision'));
        $form->setLib('date_validite',_('date_validite'));
        $form->setLib('date_chantier',_('date_chantier'));
        $form->setLib('date_achevement',_('date_achevement'));
        $form->setLib('date_conformite',_('date_conformite'));
        $form->setLib('description',_('description'));
        $form->setLib('erp',_('erp'));
        $form->setLib('avis_decision',_('avis_decision'));
        $form->setLib('enjeu_erp',_('enjeu_erp'));
        $form->setLib('enjeu_urba',_('enjeu_urba'));
        $form->setLib('division',_('division'));
        $form->setLib('autorite_competente',_('autorite_competente'));
        $form->setLib('a_qualifier',_('a_qualifier'));
        $form->setLib('terrain_references_cadastrales',_('terrain_references_cadastrales'));
        $form->setLib('terrain_adresse_voie_numero',_('terrain_adresse_voie_numero'));
        $form->setLib('terrain_adresse_voie',_('terrain_adresse_voie'));
        $form->setLib('terrain_adresse_lieu_dit',_('terrain_adresse_lieu_dit'));
        $form->setLib('terrain_adresse_localite',_('terrain_adresse_localite'));
        $form->setLib('terrain_adresse_code_postal',_('terrain_adresse_code_postal'));
        $form->setLib('terrain_adresse_bp',_('terrain_adresse_bp'));
        $form->setLib('terrain_adresse_cedex',_('terrain_adresse_cedex'));
        $form->setLib('terrain_superficie',_('terrain_superficie'));
        $form->setLib('dossier_autorisation',_('dossier_autorisation'));
        $form->setLib('dossier_instruction_type',_('dossier_instruction_type'));
        $form->setLib('date_dernier_depot',_('date_dernier_depot'));
        $form->setLib('version',_('version'));
        $form->setLib('incompletude',_('incompletude'));
        $form->setLib('evenement_suivant_tacite',_('evenement_suivant_tacite'));
        $form->setLib('evenement_suivant_tacite_incompletude',_('evenement_suivant_tacite_incompletude'));
        $form->setLib('etat_pendant_incompletude',_('etat_pendant_incompletude'));
        $form->setLib('date_limite_incompletude',_('date_limite_incompletude'));
        $form->setLib('delai_incompletude',_('delai_incompletude'));
        $form->setLib('dossier_libelle',_('dossier_libelle'));
        $form->setLib('numero_versement_archive',_('numero_versement_archive'));
        $form->setLib('duree_validite',_('duree_validite'));
        $form->setLib('quartier',_('quartier'));
        $form->setLib('incomplet_notifie',_('incomplet_notifie'));
        $form->setLib('om_collectivite',_('om_collectivite'));
        $form->setLib('tax_secteur',_('tax_secteur'));
        $form->setLib('tax_mtn_part_commu',_('tax_mtn_part_commu'));
        $form->setLib('tax_mtn_part_depart',_('tax_mtn_part_depart'));
        $form->setLib('tax_mtn_part_reg',_('tax_mtn_part_reg'));
        $form->setLib('tax_mtn_total',_('tax_mtn_total'));
        $form->setLib('log_instructions',_('log_instructions'));
        $form->setLib('interface_referentiel_erp',_('interface_referentiel_erp'));
        $form->setLib('autorisation_contestee',_('autorisation_contestee'));
        $form->setLib('date_cloture_instruction',_('date_cloture_instruction'));
        $form->setLib('date_premiere_visite',_('date_premiere_visite'));
        $form->setLib('date_derniere_visite',_('date_derniere_visite'));
        $form->setLib('date_contradictoire',_('date_contradictoire'));
        $form->setLib('date_retour_contradictoire',_('date_retour_contradictoire'));
        $form->setLib('date_ait',_('date_ait'));
        $form->setLib('date_transmission_parquet',_('date_transmission_parquet'));
        $form->setLib('instructeur_2',_('instructeur_2'));
        $form->setLib('tax_mtn_rap',_('tax_mtn_rap'));
        $form->setLib('tax_mtn_part_commu_sans_exo',_('tax_mtn_part_commu_sans_exo'));
        $form->setLib('tax_mtn_part_depart_sans_exo',_('tax_mtn_part_depart_sans_exo'));
        $form->setLib('tax_mtn_part_reg_sans_exo',_('tax_mtn_part_reg_sans_exo'));
        $form->setLib('tax_mtn_total_sans_exo',_('tax_mtn_total_sans_exo'));
        $form->setLib('tax_mtn_rap_sans_exo',_('tax_mtn_rap_sans_exo'));
        $form->setLib('geom',_('geom'));
        $form->setLib('geom1',_('geom1'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // autorisation_contestee
        $this->init_select($form, $this->f->db, $maj, null, "autorisation_contestee", $sql_autorisation_contestee, $sql_autorisation_contestee_by_id, false);
        // autorite_competente
        $this->init_select($form, $this->f->db, $maj, null, "autorite_competente", $sql_autorite_competente, $sql_autorite_competente_by_id, false);
        // avis_decision
        $this->init_select($form, $this->f->db, $maj, null, "avis_decision", $sql_avis_decision, $sql_avis_decision_by_id, false);
        // division
        $this->init_select($form, $this->f->db, $maj, null, "division", $sql_division, $sql_division_by_id, true);
        // dossier_autorisation
        $this->init_select($form, $this->f->db, $maj, null, "dossier_autorisation", $sql_dossier_autorisation, $sql_dossier_autorisation_by_id, false);
        // dossier_instruction_type
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction_type", $sql_dossier_instruction_type, $sql_dossier_instruction_type_by_id, false);
        // etat
        $this->init_select($form, $this->f->db, $maj, null, "etat", $sql_etat, $sql_etat_by_id, false);
        // etat_pendant_incompletude
        $this->init_select($form, $this->f->db, $maj, null, "etat_pendant_incompletude", $sql_etat_pendant_incompletude, $sql_etat_pendant_incompletude_by_id, false);
        // evenement_suivant_tacite
        $this->init_select($form, $this->f->db, $maj, null, "evenement_suivant_tacite", $sql_evenement_suivant_tacite, $sql_evenement_suivant_tacite_by_id, false);
        // evenement_suivant_tacite_incompletude
        $this->init_select($form, $this->f->db, $maj, null, "evenement_suivant_tacite_incompletude", $sql_evenement_suivant_tacite_incompletude, $sql_evenement_suivant_tacite_incompletude_by_id, false);
        // instructeur
        $this->init_select($form, $this->f->db, $maj, null, "instructeur", $sql_instructeur, $sql_instructeur_by_id, true);
        // instructeur_2
        $this->init_select($form, $this->f->db, $maj, null, "instructeur_2", $sql_instructeur_2, $sql_instructeur_2_by_id, true);
        // om_collectivite
        $this->init_select($form, $this->f->db, $maj, null, "om_collectivite", $sql_om_collectivite, $sql_om_collectivite_by_id, false);
        // quartier
        $this->init_select($form, $this->f->db, $maj, null, "quartier", $sql_quartier, $sql_quartier_by_id, false);
        // geom
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("dossier", $this->getParameter("idx"), "0");
            $form->setSelect("geom", $contenu);
        }
        // geom1
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("dossier", $this->getParameter("idx"), "1");
            $form->setSelect("geom1", $contenu);
        }
    }


    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier', $this->retourformulaire))
                $form->setVal('autorisation_contestee', $idxformulaire);
            if($this->is_in_context_of_foreign_key('autorite_competente', $this->retourformulaire))
                $form->setVal('autorite_competente', $idxformulaire);
            if($this->is_in_context_of_foreign_key('avis_decision', $this->retourformulaire))
                $form->setVal('avis_decision', $idxformulaire);
            if($this->is_in_context_of_foreign_key('division', $this->retourformulaire))
                $form->setVal('division', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_autorisation', $this->retourformulaire))
                $form->setVal('dossier_autorisation', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction_type', $this->retourformulaire))
                $form->setVal('dossier_instruction_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
            if($this->is_in_context_of_foreign_key('quartier', $this->retourformulaire))
                $form->setVal('quartier', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('etat', $this->retourformulaire))
                $form->setVal('etat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etat', $this->retourformulaire))
                $form->setVal('etat_pendant_incompletude', $idxformulaire);
            if($this->is_in_context_of_foreign_key('evenement', $this->retourformulaire))
                $form->setVal('evenement_suivant_tacite', $idxformulaire);
            if($this->is_in_context_of_foreign_key('evenement', $this->retourformulaire))
                $form->setVal('evenement_suivant_tacite_incompletude', $idxformulaire);
            if($this->is_in_context_of_foreign_key('instructeur', $this->retourformulaire))
                $form->setVal('instructeur', $idxformulaire);
            if($this->is_in_context_of_foreign_key('instructeur', $this->retourformulaire))
                $form->setVal('instructeur_2', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : blocnote
        $this->rechercheTable($this->f->db, "blocnote", "dossier", $id);
        // Verification de la cle secondaire : consultation
        $this->rechercheTable($this->f->db, "consultation", "dossier", $id);
        // Verification de la cle secondaire : demande
        $this->rechercheTable($this->f->db, "demande", "autorisation_contestee", $id);
        // Verification de la cle secondaire : demande
        $this->rechercheTable($this->f->db, "demande", "dossier_instruction", $id);
        // Verification de la cle secondaire : document_numerise
        $this->rechercheTable($this->f->db, "document_numerise", "dossier", $id);
        // Verification de la cle secondaire : donnees_techniques
        $this->rechercheTable($this->f->db, "donnees_techniques", "dossier_instruction", $id);
        // Verification de la cle secondaire : dossier
        $this->rechercheTable($this->f->db, "dossier", "autorisation_contestee", $id);
        // Verification de la cle secondaire : dossier_commission
        $this->rechercheTable($this->f->db, "dossier_commission", "dossier", $id);
        // Verification de la cle secondaire : dossier_contrainte
        $this->rechercheTable($this->f->db, "dossier_contrainte", "dossier", $id);
        // Verification de la cle secondaire : dossier_geolocalisation
        $this->rechercheTable($this->f->db, "dossier_geolocalisation", "dossier", $id);
        // Verification de la cle secondaire : dossier_message
        $this->rechercheTable($this->f->db, "dossier_message", "dossier", $id);
        // Verification de la cle secondaire : dossier_parcelle
        $this->rechercheTable($this->f->db, "dossier_parcelle", "dossier", $id);
        // Verification de la cle secondaire : instruction
        $this->rechercheTable($this->f->db, "instruction", "dossier", $id);
        // Verification de la cle secondaire : lien_dossier_demandeur
        $this->rechercheTable($this->f->db, "lien_dossier_demandeur", "dossier", $id);
        // Verification de la cle secondaire : lien_dossier_dossier
        $this->rechercheTable($this->f->db, "lien_dossier_dossier", "dossier_cible", $id);
        // Verification de la cle secondaire : lien_dossier_dossier
        $this->rechercheTable($this->f->db, "lien_dossier_dossier", "dossier_src", $id);
        // Verification de la cle secondaire : lot
        $this->rechercheTable($this->f->db, "lot", "dossier", $id);
        // Verification de la cle secondaire : rapport_instruction
        $this->rechercheTable($this->f->db, "rapport_instruction", "dossier_instruction", $id);
    }


}

?>
