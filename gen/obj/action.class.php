<?php
//$Id$ 
//gen openMairie le 01/02/2017 17:03

require_once "../obj/om_dbform.class.php";

class action_gen extends om_dbform {

    var $table = "action";
    var $clePrimaire = "action";
    var $typeCle = "A";
    var $required_field = array(
        "action",
        "libelle"
    );
    
    var $foreign_keys_extended = array(
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        $this->valF['action'] = $val['action'];
        $this->valF['libelle'] = $val['libelle'];
        if ($val['regle_etat'] == "") {
            $this->valF['regle_etat'] = NULL;
        } else {
            $this->valF['regle_etat'] = $val['regle_etat'];
        }
        if ($val['regle_delai'] == "") {
            $this->valF['regle_delai'] = NULL;
        } else {
            $this->valF['regle_delai'] = $val['regle_delai'];
        }
        if ($val['regle_accord_tacite'] == "") {
            $this->valF['regle_accord_tacite'] = NULL;
        } else {
            $this->valF['regle_accord_tacite'] = $val['regle_accord_tacite'];
        }
        if ($val['regle_avis'] == "") {
            $this->valF['regle_avis'] = NULL;
        } else {
            $this->valF['regle_avis'] = $val['regle_avis'];
        }
        if ($val['regle_date_limite'] == "") {
            $this->valF['regle_date_limite'] = NULL;
        } else {
            $this->valF['regle_date_limite'] = $val['regle_date_limite'];
        }
        if ($val['regle_date_notification_delai'] == "") {
            $this->valF['regle_date_notification_delai'] = NULL;
        } else {
            $this->valF['regle_date_notification_delai'] = $val['regle_date_notification_delai'];
        }
        if ($val['regle_date_complet'] == "") {
            $this->valF['regle_date_complet'] = NULL;
        } else {
            $this->valF['regle_date_complet'] = $val['regle_date_complet'];
        }
        if ($val['regle_date_validite'] == "") {
            $this->valF['regle_date_validite'] = NULL;
        } else {
            $this->valF['regle_date_validite'] = $val['regle_date_validite'];
        }
        if ($val['regle_date_decision'] == "") {
            $this->valF['regle_date_decision'] = NULL;
        } else {
            $this->valF['regle_date_decision'] = $val['regle_date_decision'];
        }
        if ($val['regle_date_chantier'] == "") {
            $this->valF['regle_date_chantier'] = NULL;
        } else {
            $this->valF['regle_date_chantier'] = $val['regle_date_chantier'];
        }
        if ($val['regle_date_achevement'] == "") {
            $this->valF['regle_date_achevement'] = NULL;
        } else {
            $this->valF['regle_date_achevement'] = $val['regle_date_achevement'];
        }
        if ($val['regle_date_conformite'] == "") {
            $this->valF['regle_date_conformite'] = NULL;
        } else {
            $this->valF['regle_date_conformite'] = $val['regle_date_conformite'];
        }
        if ($val['regle_date_rejet'] == "") {
            $this->valF['regle_date_rejet'] = NULL;
        } else {
            $this->valF['regle_date_rejet'] = $val['regle_date_rejet'];
        }
        if ($val['regle_date_dernier_depot'] == "") {
            $this->valF['regle_date_dernier_depot'] = NULL;
        } else {
            $this->valF['regle_date_dernier_depot'] = $val['regle_date_dernier_depot'];
        }
        if ($val['regle_date_limite_incompletude'] == "") {
            $this->valF['regle_date_limite_incompletude'] = NULL;
        } else {
            $this->valF['regle_date_limite_incompletude'] = $val['regle_date_limite_incompletude'];
        }
        if ($val['regle_delai_incompletude'] == "") {
            $this->valF['regle_delai_incompletude'] = NULL;
        } else {
            $this->valF['regle_delai_incompletude'] = $val['regle_delai_incompletude'];
        }
        if ($val['regle_autorite_competente'] == "") {
            $this->valF['regle_autorite_competente'] = NULL;
        } else {
            $this->valF['regle_autorite_competente'] = $val['regle_autorite_competente'];
        }
        if ($val['regle_date_cloture_instruction'] == "") {
            $this->valF['regle_date_cloture_instruction'] = NULL;
        } else {
            $this->valF['regle_date_cloture_instruction'] = $val['regle_date_cloture_instruction'];
        }
        if ($val['regle_date_premiere_visite'] == "") {
            $this->valF['regle_date_premiere_visite'] = NULL;
        } else {
            $this->valF['regle_date_premiere_visite'] = $val['regle_date_premiere_visite'];
        }
        if ($val['regle_date_derniere_visite'] == "") {
            $this->valF['regle_date_derniere_visite'] = NULL;
        } else {
            $this->valF['regle_date_derniere_visite'] = $val['regle_date_derniere_visite'];
        }
        if ($val['regle_date_contradictoire'] == "") {
            $this->valF['regle_date_contradictoire'] = NULL;
        } else {
            $this->valF['regle_date_contradictoire'] = $val['regle_date_contradictoire'];
        }
        if ($val['regle_date_retour_contradictoire'] == "") {
            $this->valF['regle_date_retour_contradictoire'] = NULL;
        } else {
            $this->valF['regle_date_retour_contradictoire'] = $val['regle_date_retour_contradictoire'];
        }
        if ($val['regle_date_ait'] == "") {
            $this->valF['regle_date_ait'] = NULL;
        } else {
            $this->valF['regle_date_ait'] = $val['regle_date_ait'];
        }
        if ($val['regle_date_transmission_parquet'] == "") {
            $this->valF['regle_date_transmission_parquet'] = NULL;
        } else {
            $this->valF['regle_date_transmission_parquet'] = $val['regle_date_transmission_parquet'];
        }
        if ($val['regle_donnees_techniques1'] == "") {
            $this->valF['regle_donnees_techniques1'] = NULL;
        } else {
            $this->valF['regle_donnees_techniques1'] = $val['regle_donnees_techniques1'];
        }
        if ($val['regle_donnees_techniques2'] == "") {
            $this->valF['regle_donnees_techniques2'] = NULL;
        } else {
            $this->valF['regle_donnees_techniques2'] = $val['regle_donnees_techniques2'];
        }
        if ($val['regle_donnees_techniques3'] == "") {
            $this->valF['regle_donnees_techniques3'] = NULL;
        } else {
            $this->valF['regle_donnees_techniques3'] = $val['regle_donnees_techniques3'];
        }
        if ($val['regle_donnees_techniques4'] == "") {
            $this->valF['regle_donnees_techniques4'] = NULL;
        } else {
            $this->valF['regle_donnees_techniques4'] = $val['regle_donnees_techniques4'];
        }
        if ($val['regle_donnees_techniques5'] == "") {
            $this->valF['regle_donnees_techniques5'] = NULL;
        } else {
            $this->valF['regle_donnees_techniques5'] = $val['regle_donnees_techniques5'];
        }
        if ($val['cible_regle_donnees_techniques1'] == "") {
            $this->valF['cible_regle_donnees_techniques1'] = NULL;
        } else {
            $this->valF['cible_regle_donnees_techniques1'] = $val['cible_regle_donnees_techniques1'];
        }
        if ($val['cible_regle_donnees_techniques2'] == "") {
            $this->valF['cible_regle_donnees_techniques2'] = NULL;
        } else {
            $this->valF['cible_regle_donnees_techniques2'] = $val['cible_regle_donnees_techniques2'];
        }
        if ($val['cible_regle_donnees_techniques3'] == "") {
            $this->valF['cible_regle_donnees_techniques3'] = NULL;
        } else {
            $this->valF['cible_regle_donnees_techniques3'] = $val['cible_regle_donnees_techniques3'];
        }
        if ($val['cible_regle_donnees_techniques4'] == "") {
            $this->valF['cible_regle_donnees_techniques4'] = NULL;
        } else {
            $this->valF['cible_regle_donnees_techniques4'] = $val['cible_regle_donnees_techniques4'];
        }
        if ($val['cible_regle_donnees_techniques5'] == "") {
            $this->valF['cible_regle_donnees_techniques5'] = NULL;
        } else {
            $this->valF['cible_regle_donnees_techniques5'] = $val['cible_regle_donnees_techniques5'];
        }
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("action", "text");
            $form->setType("libelle", "text");
            $form->setType("regle_etat", "text");
            $form->setType("regle_delai", "text");
            $form->setType("regle_accord_tacite", "text");
            $form->setType("regle_avis", "text");
            $form->setType("regle_date_limite", "text");
            $form->setType("regle_date_notification_delai", "text");
            $form->setType("regle_date_complet", "text");
            $form->setType("regle_date_validite", "text");
            $form->setType("regle_date_decision", "text");
            $form->setType("regle_date_chantier", "text");
            $form->setType("regle_date_achevement", "text");
            $form->setType("regle_date_conformite", "text");
            $form->setType("regle_date_rejet", "text");
            $form->setType("regle_date_dernier_depot", "text");
            $form->setType("regle_date_limite_incompletude", "text");
            $form->setType("regle_delai_incompletude", "text");
            $form->setType("regle_autorite_competente", "text");
            $form->setType("regle_date_cloture_instruction", "text");
            $form->setType("regle_date_premiere_visite", "text");
            $form->setType("regle_date_derniere_visite", "text");
            $form->setType("regle_date_contradictoire", "text");
            $form->setType("regle_date_retour_contradictoire", "text");
            $form->setType("regle_date_ait", "text");
            $form->setType("regle_date_transmission_parquet", "text");
            $form->setType("regle_donnees_techniques1", "text");
            $form->setType("regle_donnees_techniques2", "text");
            $form->setType("regle_donnees_techniques3", "text");
            $form->setType("regle_donnees_techniques4", "text");
            $form->setType("regle_donnees_techniques5", "text");
            $form->setType("cible_regle_donnees_techniques1", "text");
            $form->setType("cible_regle_donnees_techniques2", "text");
            $form->setType("cible_regle_donnees_techniques3", "text");
            $form->setType("cible_regle_donnees_techniques4", "text");
            $form->setType("cible_regle_donnees_techniques5", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("action", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("regle_etat", "text");
            $form->setType("regle_delai", "text");
            $form->setType("regle_accord_tacite", "text");
            $form->setType("regle_avis", "text");
            $form->setType("regle_date_limite", "text");
            $form->setType("regle_date_notification_delai", "text");
            $form->setType("regle_date_complet", "text");
            $form->setType("regle_date_validite", "text");
            $form->setType("regle_date_decision", "text");
            $form->setType("regle_date_chantier", "text");
            $form->setType("regle_date_achevement", "text");
            $form->setType("regle_date_conformite", "text");
            $form->setType("regle_date_rejet", "text");
            $form->setType("regle_date_dernier_depot", "text");
            $form->setType("regle_date_limite_incompletude", "text");
            $form->setType("regle_delai_incompletude", "text");
            $form->setType("regle_autorite_competente", "text");
            $form->setType("regle_date_cloture_instruction", "text");
            $form->setType("regle_date_premiere_visite", "text");
            $form->setType("regle_date_derniere_visite", "text");
            $form->setType("regle_date_contradictoire", "text");
            $form->setType("regle_date_retour_contradictoire", "text");
            $form->setType("regle_date_ait", "text");
            $form->setType("regle_date_transmission_parquet", "text");
            $form->setType("regle_donnees_techniques1", "text");
            $form->setType("regle_donnees_techniques2", "text");
            $form->setType("regle_donnees_techniques3", "text");
            $form->setType("regle_donnees_techniques4", "text");
            $form->setType("regle_donnees_techniques5", "text");
            $form->setType("cible_regle_donnees_techniques1", "text");
            $form->setType("cible_regle_donnees_techniques2", "text");
            $form->setType("cible_regle_donnees_techniques3", "text");
            $form->setType("cible_regle_donnees_techniques4", "text");
            $form->setType("cible_regle_donnees_techniques5", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("action", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("regle_etat", "hiddenstatic");
            $form->setType("regle_delai", "hiddenstatic");
            $form->setType("regle_accord_tacite", "hiddenstatic");
            $form->setType("regle_avis", "hiddenstatic");
            $form->setType("regle_date_limite", "hiddenstatic");
            $form->setType("regle_date_notification_delai", "hiddenstatic");
            $form->setType("regle_date_complet", "hiddenstatic");
            $form->setType("regle_date_validite", "hiddenstatic");
            $form->setType("regle_date_decision", "hiddenstatic");
            $form->setType("regle_date_chantier", "hiddenstatic");
            $form->setType("regle_date_achevement", "hiddenstatic");
            $form->setType("regle_date_conformite", "hiddenstatic");
            $form->setType("regle_date_rejet", "hiddenstatic");
            $form->setType("regle_date_dernier_depot", "hiddenstatic");
            $form->setType("regle_date_limite_incompletude", "hiddenstatic");
            $form->setType("regle_delai_incompletude", "hiddenstatic");
            $form->setType("regle_autorite_competente", "hiddenstatic");
            $form->setType("regle_date_cloture_instruction", "hiddenstatic");
            $form->setType("regle_date_premiere_visite", "hiddenstatic");
            $form->setType("regle_date_derniere_visite", "hiddenstatic");
            $form->setType("regle_date_contradictoire", "hiddenstatic");
            $form->setType("regle_date_retour_contradictoire", "hiddenstatic");
            $form->setType("regle_date_ait", "hiddenstatic");
            $form->setType("regle_date_transmission_parquet", "hiddenstatic");
            $form->setType("regle_donnees_techniques1", "hiddenstatic");
            $form->setType("regle_donnees_techniques2", "hiddenstatic");
            $form->setType("regle_donnees_techniques3", "hiddenstatic");
            $form->setType("regle_donnees_techniques4", "hiddenstatic");
            $form->setType("regle_donnees_techniques5", "hiddenstatic");
            $form->setType("cible_regle_donnees_techniques1", "hiddenstatic");
            $form->setType("cible_regle_donnees_techniques2", "hiddenstatic");
            $form->setType("cible_regle_donnees_techniques3", "hiddenstatic");
            $form->setType("cible_regle_donnees_techniques4", "hiddenstatic");
            $form->setType("cible_regle_donnees_techniques5", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("action", "static");
            $form->setType("libelle", "static");
            $form->setType("regle_etat", "static");
            $form->setType("regle_delai", "static");
            $form->setType("regle_accord_tacite", "static");
            $form->setType("regle_avis", "static");
            $form->setType("regle_date_limite", "static");
            $form->setType("regle_date_notification_delai", "static");
            $form->setType("regle_date_complet", "static");
            $form->setType("regle_date_validite", "static");
            $form->setType("regle_date_decision", "static");
            $form->setType("regle_date_chantier", "static");
            $form->setType("regle_date_achevement", "static");
            $form->setType("regle_date_conformite", "static");
            $form->setType("regle_date_rejet", "static");
            $form->setType("regle_date_dernier_depot", "static");
            $form->setType("regle_date_limite_incompletude", "static");
            $form->setType("regle_delai_incompletude", "static");
            $form->setType("regle_autorite_competente", "static");
            $form->setType("regle_date_cloture_instruction", "static");
            $form->setType("regle_date_premiere_visite", "static");
            $form->setType("regle_date_derniere_visite", "static");
            $form->setType("regle_date_contradictoire", "static");
            $form->setType("regle_date_retour_contradictoire", "static");
            $form->setType("regle_date_ait", "static");
            $form->setType("regle_date_transmission_parquet", "static");
            $form->setType("regle_donnees_techniques1", "static");
            $form->setType("regle_donnees_techniques2", "static");
            $form->setType("regle_donnees_techniques3", "static");
            $form->setType("regle_donnees_techniques4", "static");
            $form->setType("regle_donnees_techniques5", "static");
            $form->setType("cible_regle_donnees_techniques1", "static");
            $form->setType("cible_regle_donnees_techniques2", "static");
            $form->setType("cible_regle_donnees_techniques3", "static");
            $form->setType("cible_regle_donnees_techniques4", "static");
            $form->setType("cible_regle_donnees_techniques5", "static");
        }

    }

    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("action", 20);
        $form->setTaille("libelle", 30);
        $form->setTaille("regle_etat", 30);
        $form->setTaille("regle_delai", 30);
        $form->setTaille("regle_accord_tacite", 30);
        $form->setTaille("regle_avis", 30);
        $form->setTaille("regle_date_limite", 30);
        $form->setTaille("regle_date_notification_delai", 30);
        $form->setTaille("regle_date_complet", 30);
        $form->setTaille("regle_date_validite", 30);
        $form->setTaille("regle_date_decision", 30);
        $form->setTaille("regle_date_chantier", 30);
        $form->setTaille("regle_date_achevement", 30);
        $form->setTaille("regle_date_conformite", 30);
        $form->setTaille("regle_date_rejet", 30);
        $form->setTaille("regle_date_dernier_depot", 30);
        $form->setTaille("regle_date_limite_incompletude", 30);
        $form->setTaille("regle_delai_incompletude", 30);
        $form->setTaille("regle_autorite_competente", 30);
        $form->setTaille("regle_date_cloture_instruction", 30);
        $form->setTaille("regle_date_premiere_visite", 30);
        $form->setTaille("regle_date_derniere_visite", 30);
        $form->setTaille("regle_date_contradictoire", 30);
        $form->setTaille("regle_date_retour_contradictoire", 30);
        $form->setTaille("regle_date_ait", 30);
        $form->setTaille("regle_date_transmission_parquet", 30);
        $form->setTaille("regle_donnees_techniques1", 30);
        $form->setTaille("regle_donnees_techniques2", 30);
        $form->setTaille("regle_donnees_techniques3", 30);
        $form->setTaille("regle_donnees_techniques4", 30);
        $form->setTaille("regle_donnees_techniques5", 30);
        $form->setTaille("cible_regle_donnees_techniques1", 30);
        $form->setTaille("cible_regle_donnees_techniques2", 30);
        $form->setTaille("cible_regle_donnees_techniques3", 30);
        $form->setTaille("cible_regle_donnees_techniques4", 30);
        $form->setTaille("cible_regle_donnees_techniques5", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("action", 20);
        $form->setMax("libelle", 60);
        $form->setMax("regle_etat", 60);
        $form->setMax("regle_delai", 60);
        $form->setMax("regle_accord_tacite", 60);
        $form->setMax("regle_avis", 60);
        $form->setMax("regle_date_limite", 60);
        $form->setMax("regle_date_notification_delai", 60);
        $form->setMax("regle_date_complet", 60);
        $form->setMax("regle_date_validite", 60);
        $form->setMax("regle_date_decision", 60);
        $form->setMax("regle_date_chantier", 60);
        $form->setMax("regle_date_achevement", 60);
        $form->setMax("regle_date_conformite", 60);
        $form->setMax("regle_date_rejet", 60);
        $form->setMax("regle_date_dernier_depot", 60);
        $form->setMax("regle_date_limite_incompletude", 60);
        $form->setMax("regle_delai_incompletude", 60);
        $form->setMax("regle_autorite_competente", 60);
        $form->setMax("regle_date_cloture_instruction", 60);
        $form->setMax("regle_date_premiere_visite", 60);
        $form->setMax("regle_date_derniere_visite", 60);
        $form->setMax("regle_date_contradictoire", 60);
        $form->setMax("regle_date_retour_contradictoire", 60);
        $form->setMax("regle_date_ait", 60);
        $form->setMax("regle_date_transmission_parquet", 60);
        $form->setMax("regle_donnees_techniques1", 60);
        $form->setMax("regle_donnees_techniques2", 60);
        $form->setMax("regle_donnees_techniques3", 60);
        $form->setMax("regle_donnees_techniques4", 60);
        $form->setMax("regle_donnees_techniques5", 60);
        $form->setMax("cible_regle_donnees_techniques1", 250);
        $form->setMax("cible_regle_donnees_techniques2", 250);
        $form->setMax("cible_regle_donnees_techniques3", 250);
        $form->setMax("cible_regle_donnees_techniques4", 250);
        $form->setMax("cible_regle_donnees_techniques5", 250);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('action',_('action'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('regle_etat',_('regle_etat'));
        $form->setLib('regle_delai',_('regle_delai'));
        $form->setLib('regle_accord_tacite',_('regle_accord_tacite'));
        $form->setLib('regle_avis',_('regle_avis'));
        $form->setLib('regle_date_limite',_('regle_date_limite'));
        $form->setLib('regle_date_notification_delai',_('regle_date_notification_delai'));
        $form->setLib('regle_date_complet',_('regle_date_complet'));
        $form->setLib('regle_date_validite',_('regle_date_validite'));
        $form->setLib('regle_date_decision',_('regle_date_decision'));
        $form->setLib('regle_date_chantier',_('regle_date_chantier'));
        $form->setLib('regle_date_achevement',_('regle_date_achevement'));
        $form->setLib('regle_date_conformite',_('regle_date_conformite'));
        $form->setLib('regle_date_rejet',_('regle_date_rejet'));
        $form->setLib('regle_date_dernier_depot',_('regle_date_dernier_depot'));
        $form->setLib('regle_date_limite_incompletude',_('regle_date_limite_incompletude'));
        $form->setLib('regle_delai_incompletude',_('regle_delai_incompletude'));
        $form->setLib('regle_autorite_competente',_('regle_autorite_competente'));
        $form->setLib('regle_date_cloture_instruction',_('regle_date_cloture_instruction'));
        $form->setLib('regle_date_premiere_visite',_('regle_date_premiere_visite'));
        $form->setLib('regle_date_derniere_visite',_('regle_date_derniere_visite'));
        $form->setLib('regle_date_contradictoire',_('regle_date_contradictoire'));
        $form->setLib('regle_date_retour_contradictoire',_('regle_date_retour_contradictoire'));
        $form->setLib('regle_date_ait',_('regle_date_ait'));
        $form->setLib('regle_date_transmission_parquet',_('regle_date_transmission_parquet'));
        $form->setLib('regle_donnees_techniques1',_('regle_donnees_techniques1'));
        $form->setLib('regle_donnees_techniques2',_('regle_donnees_techniques2'));
        $form->setLib('regle_donnees_techniques3',_('regle_donnees_techniques3'));
        $form->setLib('regle_donnees_techniques4',_('regle_donnees_techniques4'));
        $form->setLib('regle_donnees_techniques5',_('regle_donnees_techniques5'));
        $form->setLib('cible_regle_donnees_techniques1',_('cible_regle_donnees_techniques1'));
        $form->setLib('cible_regle_donnees_techniques2',_('cible_regle_donnees_techniques2'));
        $form->setLib('cible_regle_donnees_techniques3',_('cible_regle_donnees_techniques3'));
        $form->setLib('cible_regle_donnees_techniques4',_('cible_regle_donnees_techniques4'));
        $form->setLib('cible_regle_donnees_techniques5',_('cible_regle_donnees_techniques5'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : evenement
        $this->rechercheTable($this->f->db, "evenement", "action", $id);
        // Verification de la cle secondaire : instruction
        $this->rechercheTable($this->f->db, "instruction", "action", $id);
    }


}

?>
