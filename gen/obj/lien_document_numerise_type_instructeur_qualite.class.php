<?php
//$Id$ 
//gen openMairie le 24/05/2017 17:06

require_once "../obj/om_dbform.class.php";

class lien_document_numerise_type_instructeur_qualite_gen extends om_dbform {

    var $table = "lien_document_numerise_type_instructeur_qualite";
    var $clePrimaire = "lien_document_numerise_type_instructeur_qualite";
    var $typeCle = "N";
    var $required_field = array(
        "document_numerise_type",
        "instructeur_qualite",
        "lien_document_numerise_type_instructeur_qualite"
    );
    
    var $foreign_keys_extended = array(
        "document_numerise_type" => array("document_numerise_type", ),
        "instructeur_qualite" => array("instructeur_qualite", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_document_numerise_type_instructeur_qualite'])) {
            $this->valF['lien_document_numerise_type_instructeur_qualite'] = ""; // -> requis
        } else {
            $this->valF['lien_document_numerise_type_instructeur_qualite'] = $val['lien_document_numerise_type_instructeur_qualite'];
        }
        if (!is_numeric($val['document_numerise_type'])) {
            $this->valF['document_numerise_type'] = ""; // -> requis
        } else {
            $this->valF['document_numerise_type'] = $val['document_numerise_type'];
        }
        if (!is_numeric($val['instructeur_qualite'])) {
            $this->valF['instructeur_qualite'] = ""; // -> requis
        } else {
            $this->valF['instructeur_qualite'] = $val['instructeur_qualite'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_document_numerise_type_instructeur_qualite", "hidden");
            if ($this->is_in_context_of_foreign_key("document_numerise_type", $this->retourformulaire)) {
                $form->setType("document_numerise_type", "selecthiddenstatic");
            } else {
                $form->setType("document_numerise_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("instructeur_qualite", $this->retourformulaire)) {
                $form->setType("instructeur_qualite", "selecthiddenstatic");
            } else {
                $form->setType("instructeur_qualite", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_document_numerise_type_instructeur_qualite", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("document_numerise_type", $this->retourformulaire)) {
                $form->setType("document_numerise_type", "selecthiddenstatic");
            } else {
                $form->setType("document_numerise_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("instructeur_qualite", $this->retourformulaire)) {
                $form->setType("instructeur_qualite", "selecthiddenstatic");
            } else {
                $form->setType("instructeur_qualite", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_document_numerise_type_instructeur_qualite", "hiddenstatic");
            $form->setType("document_numerise_type", "selectstatic");
            $form->setType("instructeur_qualite", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_document_numerise_type_instructeur_qualite", "static");
            $form->setType("document_numerise_type", "selectstatic");
            $form->setType("instructeur_qualite", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_document_numerise_type_instructeur_qualite','VerifNum(this)');
        $form->setOnchange('document_numerise_type','VerifNum(this)');
        $form->setOnchange('instructeur_qualite','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_document_numerise_type_instructeur_qualite", 11);
        $form->setTaille("document_numerise_type", 11);
        $form->setTaille("instructeur_qualite", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_document_numerise_type_instructeur_qualite", 11);
        $form->setMax("document_numerise_type", 11);
        $form->setMax("instructeur_qualite", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_document_numerise_type_instructeur_qualite',_('lien_document_numerise_type_instructeur_qualite'));
        $form->setLib('document_numerise_type',_('document_numerise_type'));
        $form->setLib('instructeur_qualite',_('instructeur_qualite'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // document_numerise_type
        $this->init_select($form, $this->f->db, $maj, null, "document_numerise_type", $sql_document_numerise_type, $sql_document_numerise_type_by_id, false);
        // instructeur_qualite
        $this->init_select($form, $this->f->db, $maj, null, "instructeur_qualite", $sql_instructeur_qualite, $sql_instructeur_qualite_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('document_numerise_type', $this->retourformulaire))
                $form->setVal('document_numerise_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('instructeur_qualite', $this->retourformulaire))
                $form->setVal('instructeur_qualite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
