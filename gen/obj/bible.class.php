<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class bible_gen extends om_dbform {

    var $table = "bible";
    var $clePrimaire = "bible";
    var $typeCle = "N";
    var $required_field = array(
        "bible",
        "contenu",
        "libelle",
        "om_collectivite"
    );
    
    var $foreign_keys_extended = array(
        "dossier_autorisation_type" => array("dossier_autorisation_type", ),
        "evenement" => array("evenement", ),
        "om_collectivite" => array("om_collectivite", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['bible'])) {
            $this->valF['bible'] = ""; // -> requis
        } else {
            $this->valF['bible'] = $val['bible'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if (!is_numeric($val['evenement'])) {
            $this->valF['evenement'] = NULL;
        } else {
            $this->valF['evenement'] = $val['evenement'];
        }
            $this->valF['contenu'] = $val['contenu'];
        if (!is_numeric($val['complement'])) {
            $this->valF['complement'] = NULL;
        } else {
            $this->valF['complement'] = $val['complement'];
        }
        if ($val['automatique'] == "") {
            $this->valF['automatique'] = ""; // -> default
        } else {
            $this->valF['automatique'] = $val['automatique'];
        }
        if (!is_numeric($val['dossier_autorisation_type'])) {
            $this->valF['dossier_autorisation_type'] = NULL;
        } else {
            $this->valF['dossier_autorisation_type'] = $val['dossier_autorisation_type'];
        }
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("bible", "hidden");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement", "selecthiddenstatic");
            } else {
                $form->setType("evenement", "select");
            }
            $form->setType("contenu", "textarea");
            $form->setType("complement", "text");
            $form->setType("automatique", "text");
            if ($this->is_in_context_of_foreign_key("dossier_autorisation_type", $this->retourformulaire)) {
                $form->setType("dossier_autorisation_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("bible", "hiddenstatic");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("evenement", $this->retourformulaire)) {
                $form->setType("evenement", "selecthiddenstatic");
            } else {
                $form->setType("evenement", "select");
            }
            $form->setType("contenu", "textarea");
            $form->setType("complement", "text");
            $form->setType("automatique", "text");
            if ($this->is_in_context_of_foreign_key("dossier_autorisation_type", $this->retourformulaire)) {
                $form->setType("dossier_autorisation_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("bible", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("evenement", "selectstatic");
            $form->setType("contenu", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            $form->setType("automatique", "hiddenstatic");
            $form->setType("dossier_autorisation_type", "selectstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("bible", "static");
            $form->setType("libelle", "static");
            $form->setType("evenement", "selectstatic");
            $form->setType("contenu", "textareastatic");
            $form->setType("complement", "static");
            $form->setType("automatique", "static");
            $form->setType("dossier_autorisation_type", "selectstatic");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('bible','VerifNum(this)');
        $form->setOnchange('evenement','VerifNum(this)');
        $form->setOnchange('complement','VerifNum(this)');
        $form->setOnchange('dossier_autorisation_type','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("bible", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("evenement", 11);
        $form->setTaille("contenu", 80);
        $form->setTaille("complement", 11);
        $form->setTaille("automatique", 10);
        $form->setTaille("dossier_autorisation_type", 11);
        $form->setTaille("om_collectivite", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("bible", 11);
        $form->setMax("libelle", 60);
        $form->setMax("evenement", 11);
        $form->setMax("contenu", 6);
        $form->setMax("complement", 11);
        $form->setMax("automatique", 3);
        $form->setMax("dossier_autorisation_type", 11);
        $form->setMax("om_collectivite", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('bible',_('bible'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('evenement',_('evenement'));
        $form->setLib('contenu',_('contenu'));
        $form->setLib('complement',_('complement'));
        $form->setLib('automatique',_('automatique'));
        $form->setLib('dossier_autorisation_type',_('dossier_autorisation_type'));
        $form->setLib('om_collectivite',_('om_collectivite'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // dossier_autorisation_type
        $this->init_select($form, $this->f->db, $maj, null, "dossier_autorisation_type", $sql_dossier_autorisation_type, $sql_dossier_autorisation_type_by_id, false);
        // evenement
        $this->init_select($form, $this->f->db, $maj, null, "evenement", $sql_evenement, $sql_evenement_by_id, false);
        // om_collectivite
        $this->init_select($form, $this->f->db, $maj, null, "om_collectivite", $sql_om_collectivite, $sql_om_collectivite_by_id, false);
    }


    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_autorisation_type', $this->retourformulaire))
                $form->setVal('dossier_autorisation_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('evenement', $this->retourformulaire))
                $form->setVal('evenement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
