<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class lien_lot_demandeur_gen extends om_dbform {

    var $table = "lien_lot_demandeur";
    var $clePrimaire = "lien_lot_demandeur";
    var $typeCle = "N";
    var $required_field = array(
        "lien_lot_demandeur"
    );
    
    var $foreign_keys_extended = array(
        "demandeur" => array("demandeur", ),
        "lot" => array("lot", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_lot_demandeur'])) {
            $this->valF['lien_lot_demandeur'] = ""; // -> requis
        } else {
            $this->valF['lien_lot_demandeur'] = $val['lien_lot_demandeur'];
        }
        if (!is_numeric($val['lot'])) {
            $this->valF['lot'] = NULL;
        } else {
            $this->valF['lot'] = $val['lot'];
        }
        if (!is_numeric($val['demandeur'])) {
            $this->valF['demandeur'] = NULL;
        } else {
            $this->valF['demandeur'] = $val['demandeur'];
        }
        if ($val['petitionnaire_principal'] == 1 || $val['petitionnaire_principal'] == "t" || $val['petitionnaire_principal'] == "Oui") {
            $this->valF['petitionnaire_principal'] = true;
        } else {
            $this->valF['petitionnaire_principal'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_lot_demandeur", "hidden");
            if ($this->is_in_context_of_foreign_key("lot", $this->retourformulaire)) {
                $form->setType("lot", "selecthiddenstatic");
            } else {
                $form->setType("lot", "select");
            }
            if ($this->is_in_context_of_foreign_key("demandeur", $this->retourformulaire)) {
                $form->setType("demandeur", "selecthiddenstatic");
            } else {
                $form->setType("demandeur", "select");
            }
            $form->setType("petitionnaire_principal", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_lot_demandeur", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("lot", $this->retourformulaire)) {
                $form->setType("lot", "selecthiddenstatic");
            } else {
                $form->setType("lot", "select");
            }
            if ($this->is_in_context_of_foreign_key("demandeur", $this->retourformulaire)) {
                $form->setType("demandeur", "selecthiddenstatic");
            } else {
                $form->setType("demandeur", "select");
            }
            $form->setType("petitionnaire_principal", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_lot_demandeur", "hiddenstatic");
            $form->setType("lot", "selectstatic");
            $form->setType("demandeur", "selectstatic");
            $form->setType("petitionnaire_principal", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_lot_demandeur", "static");
            $form->setType("lot", "selectstatic");
            $form->setType("demandeur", "selectstatic");
            $form->setType("petitionnaire_principal", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_lot_demandeur','VerifNum(this)');
        $form->setOnchange('lot','VerifNum(this)');
        $form->setOnchange('demandeur','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_lot_demandeur", 11);
        $form->setTaille("lot", 11);
        $form->setTaille("demandeur", 11);
        $form->setTaille("petitionnaire_principal", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_lot_demandeur", 11);
        $form->setMax("lot", 11);
        $form->setMax("demandeur", 11);
        $form->setMax("petitionnaire_principal", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_lot_demandeur',_('lien_lot_demandeur'));
        $form->setLib('lot',_('lot'));
        $form->setLib('demandeur',_('demandeur'));
        $form->setLib('petitionnaire_principal',_('petitionnaire_principal'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // demandeur
        $this->init_select($form, $this->f->db, $maj, null, "demandeur", $sql_demandeur, $sql_demandeur_by_id, false);
        // lot
        $this->init_select($form, $this->f->db, $maj, null, "lot", $sql_lot, $sql_lot_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('demandeur', $this->retourformulaire))
                $form->setVal('demandeur', $idxformulaire);
            if($this->is_in_context_of_foreign_key('lot', $this->retourformulaire))
                $form->setVal('lot', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
