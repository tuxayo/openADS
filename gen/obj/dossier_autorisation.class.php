<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class dossier_autorisation_gen extends om_dbform {

    var $table = "dossier_autorisation";
    var $clePrimaire = "dossier_autorisation";
    var $typeCle = "A";
    var $required_field = array(
        "dossier_autorisation",
        "om_collectivite"
    );
    
    var $foreign_keys_extended = array(
        "arrondissement" => array("arrondissement", ),
        "avis_decision" => array("avis_decision", ),
        "dossier_autorisation_type_detaille" => array("dossier_autorisation_type_detaille", ),
        "etat_dossier_autorisation" => array("etat_dossier_autorisation", ),
        "om_collectivite" => array("om_collectivite", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        $this->valF['dossier_autorisation'] = $val['dossier_autorisation'];
        if (!is_numeric($val['dossier_autorisation_type_detaille'])) {
            $this->valF['dossier_autorisation_type_detaille'] = NULL;
        } else {
            $this->valF['dossier_autorisation_type_detaille'] = $val['dossier_autorisation_type_detaille'];
        }
        if (!is_numeric($val['exercice'])) {
            $this->valF['exercice'] = NULL;
        } else {
            $this->valF['exercice'] = $val['exercice'];
        }
        if ($val['insee'] == "") {
            $this->valF['insee'] = NULL;
        } else {
            $this->valF['insee'] = $val['insee'];
        }
            $this->valF['terrain_references_cadastrales'] = $val['terrain_references_cadastrales'];
        if ($val['terrain_adresse_voie_numero'] == "") {
            $this->valF['terrain_adresse_voie_numero'] = NULL;
        } else {
            $this->valF['terrain_adresse_voie_numero'] = $val['terrain_adresse_voie_numero'];
        }
        if ($val['terrain_adresse_voie'] == "") {
            $this->valF['terrain_adresse_voie'] = NULL;
        } else {
            $this->valF['terrain_adresse_voie'] = $val['terrain_adresse_voie'];
        }
        if ($val['terrain_adresse_lieu_dit'] == "") {
            $this->valF['terrain_adresse_lieu_dit'] = NULL;
        } else {
            $this->valF['terrain_adresse_lieu_dit'] = $val['terrain_adresse_lieu_dit'];
        }
        if ($val['terrain_adresse_localite'] == "") {
            $this->valF['terrain_adresse_localite'] = NULL;
        } else {
            $this->valF['terrain_adresse_localite'] = $val['terrain_adresse_localite'];
        }
        if ($val['terrain_adresse_code_postal'] == "") {
            $this->valF['terrain_adresse_code_postal'] = NULL;
        } else {
            $this->valF['terrain_adresse_code_postal'] = $val['terrain_adresse_code_postal'];
        }
        if ($val['terrain_adresse_bp'] == "") {
            $this->valF['terrain_adresse_bp'] = NULL;
        } else {
            $this->valF['terrain_adresse_bp'] = $val['terrain_adresse_bp'];
        }
        if ($val['terrain_adresse_cedex'] == "") {
            $this->valF['terrain_adresse_cedex'] = NULL;
        } else {
            $this->valF['terrain_adresse_cedex'] = $val['terrain_adresse_cedex'];
        }
        if (!is_numeric($val['terrain_superficie'])) {
            $this->valF['terrain_superficie'] = NULL;
        } else {
            $this->valF['terrain_superficie'] = $val['terrain_superficie'];
        }
        if (!is_numeric($val['arrondissement'])) {
            $this->valF['arrondissement'] = NULL;
        } else {
            $this->valF['arrondissement'] = $val['arrondissement'];
        }
        if ($val['depot_initial'] != "") {
            $this->valF['depot_initial'] = $this->dateDB($val['depot_initial']);
        } else {
            $this->valF['depot_initial'] = NULL;
        }
        if (!is_numeric($val['erp_numero_batiment'])) {
            $this->valF['erp_numero_batiment'] = NULL;
        } else {
            $this->valF['erp_numero_batiment'] = $val['erp_numero_batiment'];
        }
        if ($val['erp_ouvert'] == 1 || $val['erp_ouvert'] == "t" || $val['erp_ouvert'] == "Oui") {
            $this->valF['erp_ouvert'] = true;
        } else {
            $this->valF['erp_ouvert'] = false;
        }
        if ($val['erp_date_ouverture'] != "") {
            $this->valF['erp_date_ouverture'] = $this->dateDB($val['erp_date_ouverture']);
        } else {
            $this->valF['erp_date_ouverture'] = NULL;
        }
        if ($val['erp_arrete_decision'] == 1 || $val['erp_arrete_decision'] == "t" || $val['erp_arrete_decision'] == "Oui") {
            $this->valF['erp_arrete_decision'] = true;
        } else {
            $this->valF['erp_arrete_decision'] = false;
        }
        if ($val['erp_date_arrete_decision'] != "") {
            $this->valF['erp_date_arrete_decision'] = $this->dateDB($val['erp_date_arrete_decision']);
        } else {
            $this->valF['erp_date_arrete_decision'] = NULL;
        }
        if (!is_numeric($val['numero_version'])) {
            $this->valF['numero_version'] = NULL;
        } else {
            $this->valF['numero_version'] = $val['numero_version'];
        }
        if (!is_numeric($val['etat_dossier_autorisation'])) {
            $this->valF['etat_dossier_autorisation'] = NULL;
        } else {
            $this->valF['etat_dossier_autorisation'] = $val['etat_dossier_autorisation'];
        }
        if ($val['date_depot'] != "") {
            $this->valF['date_depot'] = $this->dateDB($val['date_depot']);
        } else {
            $this->valF['date_depot'] = NULL;
        }
        if ($val['date_decision'] != "") {
            $this->valF['date_decision'] = $this->dateDB($val['date_decision']);
        } else {
            $this->valF['date_decision'] = NULL;
        }
        if ($val['date_validite'] != "") {
            $this->valF['date_validite'] = $this->dateDB($val['date_validite']);
        } else {
            $this->valF['date_validite'] = NULL;
        }
        if ($val['date_chantier'] != "") {
            $this->valF['date_chantier'] = $this->dateDB($val['date_chantier']);
        } else {
            $this->valF['date_chantier'] = NULL;
        }
        if ($val['date_achevement'] != "") {
            $this->valF['date_achevement'] = $this->dateDB($val['date_achevement']);
        } else {
            $this->valF['date_achevement'] = NULL;
        }
        if (!is_numeric($val['avis_decision'])) {
            $this->valF['avis_decision'] = NULL;
        } else {
            $this->valF['avis_decision'] = $val['avis_decision'];
        }
        if (!is_numeric($val['etat_dernier_dossier_instruction_accepte'])) {
            $this->valF['etat_dernier_dossier_instruction_accepte'] = NULL;
        } else {
            $this->valF['etat_dernier_dossier_instruction_accepte'] = $val['etat_dernier_dossier_instruction_accepte'];
        }
        if ($val['dossier_autorisation_libelle'] == "") {
            $this->valF['dossier_autorisation_libelle'] = NULL;
        } else {
            $this->valF['dossier_autorisation_libelle'] = $val['dossier_autorisation_libelle'];
        }
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
        if ($val['cle_acces_citoyen'] == "") {
            $this->valF['cle_acces_citoyen'] = NULL;
        } else {
            $this->valF['cle_acces_citoyen'] = $val['cle_acces_citoyen'];
        }
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_autorisation", "text");
            if ($this->is_in_context_of_foreign_key("dossier_autorisation_type_detaille", $this->retourformulaire)) {
                $form->setType("dossier_autorisation_type_detaille", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation_type_detaille", "select");
            }
            $form->setType("exercice", "text");
            $form->setType("insee", "text");
            $form->setType("terrain_references_cadastrales", "textarea");
            $form->setType("terrain_adresse_voie_numero", "text");
            $form->setType("terrain_adresse_voie", "text");
            $form->setType("terrain_adresse_lieu_dit", "text");
            $form->setType("terrain_adresse_localite", "text");
            $form->setType("terrain_adresse_code_postal", "text");
            $form->setType("terrain_adresse_bp", "text");
            $form->setType("terrain_adresse_cedex", "text");
            $form->setType("terrain_superficie", "text");
            if ($this->is_in_context_of_foreign_key("arrondissement", $this->retourformulaire)) {
                $form->setType("arrondissement", "selecthiddenstatic");
            } else {
                $form->setType("arrondissement", "select");
            }
            $form->setType("depot_initial", "date");
            $form->setType("erp_numero_batiment", "text");
            $form->setType("erp_ouvert", "checkbox");
            $form->setType("erp_date_ouverture", "date");
            $form->setType("erp_arrete_decision", "checkbox");
            $form->setType("erp_date_arrete_decision", "date");
            $form->setType("numero_version", "text");
            if ($this->is_in_context_of_foreign_key("etat_dossier_autorisation", $this->retourformulaire)) {
                $form->setType("etat_dossier_autorisation", "selecthiddenstatic");
            } else {
                $form->setType("etat_dossier_autorisation", "select");
            }
            $form->setType("date_depot", "date");
            $form->setType("date_decision", "date");
            $form->setType("date_validite", "date");
            $form->setType("date_chantier", "date");
            $form->setType("date_achevement", "date");
            if ($this->is_in_context_of_foreign_key("avis_decision", $this->retourformulaire)) {
                $form->setType("avis_decision", "selecthiddenstatic");
            } else {
                $form->setType("avis_decision", "select");
            }
            if ($this->is_in_context_of_foreign_key("etat_dossier_autorisation", $this->retourformulaire)) {
                $form->setType("etat_dernier_dossier_instruction_accepte", "selecthiddenstatic");
            } else {
                $form->setType("etat_dernier_dossier_instruction_accepte", "select");
            }
            $form->setType("dossier_autorisation_libelle", "text");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("cle_acces_citoyen", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_autorisation", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("dossier_autorisation_type_detaille", $this->retourformulaire)) {
                $form->setType("dossier_autorisation_type_detaille", "selecthiddenstatic");
            } else {
                $form->setType("dossier_autorisation_type_detaille", "select");
            }
            $form->setType("exercice", "text");
            $form->setType("insee", "text");
            $form->setType("terrain_references_cadastrales", "textarea");
            $form->setType("terrain_adresse_voie_numero", "text");
            $form->setType("terrain_adresse_voie", "text");
            $form->setType("terrain_adresse_lieu_dit", "text");
            $form->setType("terrain_adresse_localite", "text");
            $form->setType("terrain_adresse_code_postal", "text");
            $form->setType("terrain_adresse_bp", "text");
            $form->setType("terrain_adresse_cedex", "text");
            $form->setType("terrain_superficie", "text");
            if ($this->is_in_context_of_foreign_key("arrondissement", $this->retourformulaire)) {
                $form->setType("arrondissement", "selecthiddenstatic");
            } else {
                $form->setType("arrondissement", "select");
            }
            $form->setType("depot_initial", "date");
            $form->setType("erp_numero_batiment", "text");
            $form->setType("erp_ouvert", "checkbox");
            $form->setType("erp_date_ouverture", "date");
            $form->setType("erp_arrete_decision", "checkbox");
            $form->setType("erp_date_arrete_decision", "date");
            $form->setType("numero_version", "text");
            if ($this->is_in_context_of_foreign_key("etat_dossier_autorisation", $this->retourformulaire)) {
                $form->setType("etat_dossier_autorisation", "selecthiddenstatic");
            } else {
                $form->setType("etat_dossier_autorisation", "select");
            }
            $form->setType("date_depot", "date");
            $form->setType("date_decision", "date");
            $form->setType("date_validite", "date");
            $form->setType("date_chantier", "date");
            $form->setType("date_achevement", "date");
            if ($this->is_in_context_of_foreign_key("avis_decision", $this->retourformulaire)) {
                $form->setType("avis_decision", "selecthiddenstatic");
            } else {
                $form->setType("avis_decision", "select");
            }
            if ($this->is_in_context_of_foreign_key("etat_dossier_autorisation", $this->retourformulaire)) {
                $form->setType("etat_dernier_dossier_instruction_accepte", "selecthiddenstatic");
            } else {
                $form->setType("etat_dernier_dossier_instruction_accepte", "select");
            }
            $form->setType("dossier_autorisation_libelle", "text");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("cle_acces_citoyen", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_autorisation", "hiddenstatic");
            $form->setType("dossier_autorisation_type_detaille", "selectstatic");
            $form->setType("exercice", "hiddenstatic");
            $form->setType("insee", "hiddenstatic");
            $form->setType("terrain_references_cadastrales", "hiddenstatic");
            $form->setType("terrain_adresse_voie_numero", "hiddenstatic");
            $form->setType("terrain_adresse_voie", "hiddenstatic");
            $form->setType("terrain_adresse_lieu_dit", "hiddenstatic");
            $form->setType("terrain_adresse_localite", "hiddenstatic");
            $form->setType("terrain_adresse_code_postal", "hiddenstatic");
            $form->setType("terrain_adresse_bp", "hiddenstatic");
            $form->setType("terrain_adresse_cedex", "hiddenstatic");
            $form->setType("terrain_superficie", "hiddenstatic");
            $form->setType("arrondissement", "selectstatic");
            $form->setType("depot_initial", "hiddenstatic");
            $form->setType("erp_numero_batiment", "hiddenstatic");
            $form->setType("erp_ouvert", "hiddenstatic");
            $form->setType("erp_date_ouverture", "hiddenstatic");
            $form->setType("erp_arrete_decision", "hiddenstatic");
            $form->setType("erp_date_arrete_decision", "hiddenstatic");
            $form->setType("numero_version", "hiddenstatic");
            $form->setType("etat_dossier_autorisation", "selectstatic");
            $form->setType("date_depot", "hiddenstatic");
            $form->setType("date_decision", "hiddenstatic");
            $form->setType("date_validite", "hiddenstatic");
            $form->setType("date_chantier", "hiddenstatic");
            $form->setType("date_achevement", "hiddenstatic");
            $form->setType("avis_decision", "selectstatic");
            $form->setType("etat_dernier_dossier_instruction_accepte", "selectstatic");
            $form->setType("dossier_autorisation_libelle", "hiddenstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
            $form->setType("cle_acces_citoyen", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_autorisation", "static");
            $form->setType("dossier_autorisation_type_detaille", "selectstatic");
            $form->setType("exercice", "static");
            $form->setType("insee", "static");
            $form->setType("terrain_references_cadastrales", "textareastatic");
            $form->setType("terrain_adresse_voie_numero", "static");
            $form->setType("terrain_adresse_voie", "static");
            $form->setType("terrain_adresse_lieu_dit", "static");
            $form->setType("terrain_adresse_localite", "static");
            $form->setType("terrain_adresse_code_postal", "static");
            $form->setType("terrain_adresse_bp", "static");
            $form->setType("terrain_adresse_cedex", "static");
            $form->setType("terrain_superficie", "static");
            $form->setType("arrondissement", "selectstatic");
            $form->setType("depot_initial", "datestatic");
            $form->setType("erp_numero_batiment", "static");
            $form->setType("erp_ouvert", "checkboxstatic");
            $form->setType("erp_date_ouverture", "datestatic");
            $form->setType("erp_arrete_decision", "checkboxstatic");
            $form->setType("erp_date_arrete_decision", "datestatic");
            $form->setType("numero_version", "static");
            $form->setType("etat_dossier_autorisation", "selectstatic");
            $form->setType("date_depot", "datestatic");
            $form->setType("date_decision", "datestatic");
            $form->setType("date_validite", "datestatic");
            $form->setType("date_chantier", "datestatic");
            $form->setType("date_achevement", "datestatic");
            $form->setType("avis_decision", "selectstatic");
            $form->setType("etat_dernier_dossier_instruction_accepte", "selectstatic");
            $form->setType("dossier_autorisation_libelle", "static");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("cle_acces_citoyen", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_autorisation_type_detaille','VerifNum(this)');
        $form->setOnchange('exercice','VerifNum(this)');
        $form->setOnchange('terrain_superficie','VerifFloat(this)');
        $form->setOnchange('arrondissement','VerifNum(this)');
        $form->setOnchange('depot_initial','fdate(this)');
        $form->setOnchange('erp_numero_batiment','VerifNum(this)');
        $form->setOnchange('erp_date_ouverture','fdate(this)');
        $form->setOnchange('erp_date_arrete_decision','fdate(this)');
        $form->setOnchange('numero_version','VerifNum(this)');
        $form->setOnchange('etat_dossier_autorisation','VerifNum(this)');
        $form->setOnchange('date_depot','fdate(this)');
        $form->setOnchange('date_decision','fdate(this)');
        $form->setOnchange('date_validite','fdate(this)');
        $form->setOnchange('date_chantier','fdate(this)');
        $form->setOnchange('date_achevement','fdate(this)');
        $form->setOnchange('avis_decision','VerifNum(this)');
        $form->setOnchange('etat_dernier_dossier_instruction_accepte','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_autorisation", 20);
        $form->setTaille("dossier_autorisation_type_detaille", 11);
        $form->setTaille("exercice", 11);
        $form->setTaille("insee", 10);
        $form->setTaille("terrain_references_cadastrales", 80);
        $form->setTaille("terrain_adresse_voie_numero", 20);
        $form->setTaille("terrain_adresse_voie", 30);
        $form->setTaille("terrain_adresse_lieu_dit", 30);
        $form->setTaille("terrain_adresse_localite", 30);
        $form->setTaille("terrain_adresse_code_postal", 10);
        $form->setTaille("terrain_adresse_bp", 15);
        $form->setTaille("terrain_adresse_cedex", 15);
        $form->setTaille("terrain_superficie", 20);
        $form->setTaille("arrondissement", 11);
        $form->setTaille("depot_initial", 12);
        $form->setTaille("erp_numero_batiment", 11);
        $form->setTaille("erp_ouvert", 1);
        $form->setTaille("erp_date_ouverture", 12);
        $form->setTaille("erp_arrete_decision", 1);
        $form->setTaille("erp_date_arrete_decision", 12);
        $form->setTaille("numero_version", 11);
        $form->setTaille("etat_dossier_autorisation", 11);
        $form->setTaille("date_depot", 12);
        $form->setTaille("date_decision", 12);
        $form->setTaille("date_validite", 12);
        $form->setTaille("date_chantier", 12);
        $form->setTaille("date_achevement", 12);
        $form->setTaille("avis_decision", 11);
        $form->setTaille("etat_dernier_dossier_instruction_accepte", 11);
        $form->setTaille("dossier_autorisation_libelle", 20);
        $form->setTaille("om_collectivite", 11);
        $form->setTaille("cle_acces_citoyen", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_autorisation", 20);
        $form->setMax("dossier_autorisation_type_detaille", 11);
        $form->setMax("exercice", 11);
        $form->setMax("insee", 5);
        $form->setMax("terrain_references_cadastrales", 6);
        $form->setMax("terrain_adresse_voie_numero", 20);
        $form->setMax("terrain_adresse_voie", 30);
        $form->setMax("terrain_adresse_lieu_dit", 30);
        $form->setMax("terrain_adresse_localite", 30);
        $form->setMax("terrain_adresse_code_postal", 5);
        $form->setMax("terrain_adresse_bp", 15);
        $form->setMax("terrain_adresse_cedex", 15);
        $form->setMax("terrain_superficie", 20);
        $form->setMax("arrondissement", 11);
        $form->setMax("depot_initial", 12);
        $form->setMax("erp_numero_batiment", 11);
        $form->setMax("erp_ouvert", 1);
        $form->setMax("erp_date_ouverture", 12);
        $form->setMax("erp_arrete_decision", 1);
        $form->setMax("erp_date_arrete_decision", 12);
        $form->setMax("numero_version", 11);
        $form->setMax("etat_dossier_autorisation", 11);
        $form->setMax("date_depot", 12);
        $form->setMax("date_decision", 12);
        $form->setMax("date_validite", 12);
        $form->setMax("date_chantier", 12);
        $form->setMax("date_achevement", 12);
        $form->setMax("avis_decision", 11);
        $form->setMax("etat_dernier_dossier_instruction_accepte", 11);
        $form->setMax("dossier_autorisation_libelle", 20);
        $form->setMax("om_collectivite", 11);
        $form->setMax("cle_acces_citoyen", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_autorisation',_('dossier_autorisation'));
        $form->setLib('dossier_autorisation_type_detaille',_('dossier_autorisation_type_detaille'));
        $form->setLib('exercice',_('exercice'));
        $form->setLib('insee',_('insee'));
        $form->setLib('terrain_references_cadastrales',_('terrain_references_cadastrales'));
        $form->setLib('terrain_adresse_voie_numero',_('terrain_adresse_voie_numero'));
        $form->setLib('terrain_adresse_voie',_('terrain_adresse_voie'));
        $form->setLib('terrain_adresse_lieu_dit',_('terrain_adresse_lieu_dit'));
        $form->setLib('terrain_adresse_localite',_('terrain_adresse_localite'));
        $form->setLib('terrain_adresse_code_postal',_('terrain_adresse_code_postal'));
        $form->setLib('terrain_adresse_bp',_('terrain_adresse_bp'));
        $form->setLib('terrain_adresse_cedex',_('terrain_adresse_cedex'));
        $form->setLib('terrain_superficie',_('terrain_superficie'));
        $form->setLib('arrondissement',_('arrondissement'));
        $form->setLib('depot_initial',_('depot_initial'));
        $form->setLib('erp_numero_batiment',_('erp_numero_batiment'));
        $form->setLib('erp_ouvert',_('erp_ouvert'));
        $form->setLib('erp_date_ouverture',_('erp_date_ouverture'));
        $form->setLib('erp_arrete_decision',_('erp_arrete_decision'));
        $form->setLib('erp_date_arrete_decision',_('erp_date_arrete_decision'));
        $form->setLib('numero_version',_('numero_version'));
        $form->setLib('etat_dossier_autorisation',_('etat_dossier_autorisation'));
        $form->setLib('date_depot',_('date_depot'));
        $form->setLib('date_decision',_('date_decision'));
        $form->setLib('date_validite',_('date_validite'));
        $form->setLib('date_chantier',_('date_chantier'));
        $form->setLib('date_achevement',_('date_achevement'));
        $form->setLib('avis_decision',_('avis_decision'));
        $form->setLib('etat_dernier_dossier_instruction_accepte',_('etat_dernier_dossier_instruction_accepte'));
        $form->setLib('dossier_autorisation_libelle',_('dossier_autorisation_libelle'));
        $form->setLib('om_collectivite',_('om_collectivite'));
        $form->setLib('cle_acces_citoyen',_('cle_acces_citoyen'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // arrondissement
        $this->init_select($form, $this->f->db, $maj, null, "arrondissement", $sql_arrondissement, $sql_arrondissement_by_id, false);
        // avis_decision
        $this->init_select($form, $this->f->db, $maj, null, "avis_decision", $sql_avis_decision, $sql_avis_decision_by_id, false);
        // dossier_autorisation_type_detaille
        $this->init_select($form, $this->f->db, $maj, null, "dossier_autorisation_type_detaille", $sql_dossier_autorisation_type_detaille, $sql_dossier_autorisation_type_detaille_by_id, false);
        // etat_dernier_dossier_instruction_accepte
        $this->init_select($form, $this->f->db, $maj, null, "etat_dernier_dossier_instruction_accepte", $sql_etat_dernier_dossier_instruction_accepte, $sql_etat_dernier_dossier_instruction_accepte_by_id, false);
        // etat_dossier_autorisation
        $this->init_select($form, $this->f->db, $maj, null, "etat_dossier_autorisation", $sql_etat_dossier_autorisation, $sql_etat_dossier_autorisation_by_id, false);
        // om_collectivite
        $this->init_select($form, $this->f->db, $maj, null, "om_collectivite", $sql_om_collectivite, $sql_om_collectivite_by_id, false);
    }


    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('arrondissement', $this->retourformulaire))
                $form->setVal('arrondissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('avis_decision', $this->retourformulaire))
                $form->setVal('avis_decision', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_autorisation_type_detaille', $this->retourformulaire))
                $form->setVal('dossier_autorisation_type_detaille', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('etat_dossier_autorisation', $this->retourformulaire))
                $form->setVal('etat_dernier_dossier_instruction_accepte', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etat_dossier_autorisation', $this->retourformulaire))
                $form->setVal('etat_dossier_autorisation', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : demande
        $this->rechercheTable($this->f->db, "demande", "dossier_autorisation", $id);
        // Verification de la cle secondaire : donnees_techniques
        $this->rechercheTable($this->f->db, "donnees_techniques", "dossier_autorisation", $id);
        // Verification de la cle secondaire : dossier
        $this->rechercheTable($this->f->db, "dossier", "dossier_autorisation", $id);
        // Verification de la cle secondaire : dossier_autorisation_parcelle
        $this->rechercheTable($this->f->db, "dossier_autorisation_parcelle", "dossier_autorisation", $id);
        // Verification de la cle secondaire : lien_dossier_autorisation_demandeur
        $this->rechercheTable($this->f->db, "lien_dossier_autorisation_demandeur", "dossier_autorisation", $id);
        // Verification de la cle secondaire : lot
        $this->rechercheTable($this->f->db, "lot", "dossier_autorisation", $id);
    }


}

?>
