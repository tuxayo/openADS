<?php
//$Id$ 
//gen openMairie le 07/12/2016 12:40

require_once "../obj/om_dbform.class.php";

class document_numerise_gen extends om_dbform {

    var $table = "document_numerise";
    var $clePrimaire = "document_numerise";
    var $typeCle = "N";
    var $required_field = array(
        "date_creation",
        "document_numerise",
        "document_numerise_type",
        "dossier",
        "nom_fichier",
        "uid"
    );
    var $unique_key = array(
      "uid",
      array("dossier","nom_fichier"),
    );
    var $foreign_keys_extended = array(
        "document_numerise_type" => array("document_numerise_type", ),
        "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['document_numerise'])) {
            $this->valF['document_numerise'] = ""; // -> requis
        } else {
            $this->valF['document_numerise'] = $val['document_numerise'];
        }
        $this->valF['uid'] = $val['uid'];
        $this->valF['dossier'] = $val['dossier'];
        $this->valF['nom_fichier'] = $val['nom_fichier'];
        if ($val['date_creation'] != "") {
            $this->valF['date_creation'] = $this->dateDB($val['date_creation']);
        }
        if (!is_numeric($val['document_numerise_type'])) {
            $this->valF['document_numerise_type'] = ""; // -> requis
        } else {
            $this->valF['document_numerise_type'] = $val['document_numerise_type'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("document_numerise", "hidden");
            $form->setType("uid", "text");
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("dossier", "selecthiddenstatic");
            } else {
                $form->setType("dossier", "select");
            }
            $form->setType("nom_fichier", "text");
            $form->setType("date_creation", "date");
            if ($this->is_in_context_of_foreign_key("document_numerise_type", $this->retourformulaire)) {
                $form->setType("document_numerise_type", "selecthiddenstatic");
            } else {
                $form->setType("document_numerise_type", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("document_numerise", "hiddenstatic");
            $form->setType("uid", "text");
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("dossier", "selecthiddenstatic");
            } else {
                $form->setType("dossier", "select");
            }
            $form->setType("nom_fichier", "text");
            $form->setType("date_creation", "date");
            if ($this->is_in_context_of_foreign_key("document_numerise_type", $this->retourformulaire)) {
                $form->setType("document_numerise_type", "selecthiddenstatic");
            } else {
                $form->setType("document_numerise_type", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("document_numerise", "hiddenstatic");
            $form->setType("uid", "hiddenstatic");
            $form->setType("dossier", "selectstatic");
            $form->setType("nom_fichier", "hiddenstatic");
            $form->setType("date_creation", "hiddenstatic");
            $form->setType("document_numerise_type", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("document_numerise", "static");
            $form->setType("uid", "static");
            $form->setType("dossier", "selectstatic");
            $form->setType("nom_fichier", "static");
            $form->setType("date_creation", "datestatic");
            $form->setType("document_numerise_type", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('document_numerise','VerifNum(this)');
        $form->setOnchange('date_creation','fdate(this)');
        $form->setOnchange('document_numerise_type','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("document_numerise", 11);
        $form->setTaille("uid", 30);
        $form->setTaille("dossier", 30);
        $form->setTaille("nom_fichier", 30);
        $form->setTaille("date_creation", 12);
        $form->setTaille("document_numerise_type", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("document_numerise", 11);
        $form->setMax("uid", 64);
        $form->setMax("dossier", 30);
        $form->setMax("nom_fichier", 255);
        $form->setMax("date_creation", 12);
        $form->setMax("document_numerise_type", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('document_numerise',_('document_numerise'));
        $form->setLib('uid',_('uid'));
        $form->setLib('dossier',_('dossier'));
        $form->setLib('nom_fichier',_('nom_fichier'));
        $form->setLib('date_creation',_('date_creation'));
        $form->setLib('document_numerise_type',_('document_numerise_type'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // document_numerise_type
        $this->init_select($form, $this->f->db, $maj, null, "document_numerise_type", $sql_document_numerise_type, $sql_document_numerise_type_by_id, false);
        // dossier
        $this->init_select($form, $this->f->db, $maj, null, "dossier", $sql_dossier, $sql_dossier_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('document_numerise_type', $this->retourformulaire))
                $form->setVal('document_numerise_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier', $this->retourformulaire))
                $form->setVal('dossier', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : instruction
        $this->rechercheTable($this->f->db, "instruction", "document_numerise", $id);
    }


}

?>
