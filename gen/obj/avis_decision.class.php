<?php
//$Id$ 
//gen openMairie le 20/10/2016 14:00

require_once "../obj/om_dbform.class.php";

class avis_decision_gen extends om_dbform {

    var $table = "avis_decision";
    var $clePrimaire = "avis_decision";
    var $typeCle = "N";
    var $required_field = array(
        "avis_decision",
        "libelle"
    );
    
    var $foreign_keys_extended = array(
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        $this->valF['libelle'] = $val['libelle'];
        if ($val['typeavis'] == "") {
            $this->valF['typeavis'] = ""; // -> default
        } else {
            $this->valF['typeavis'] = $val['typeavis'];
        }
        if ($val['sitadel'] == "") {
            $this->valF['sitadel'] = ""; // -> default
        } else {
            $this->valF['sitadel'] = $val['sitadel'];
        }
        if ($val['sitadel_motif'] == "") {
            $this->valF['sitadel_motif'] = ""; // -> default
        } else {
            $this->valF['sitadel_motif'] = $val['sitadel_motif'];
        }
        if (!is_numeric($val['avis_decision'])) {
            $this->valF['avis_decision'] = ""; // -> requis
        } else {
            $this->valF['avis_decision'] = $val['avis_decision'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("libelle", "text");
            $form->setType("typeavis", "text");
            $form->setType("sitadel", "text");
            $form->setType("sitadel_motif", "text");
            $form->setType("avis_decision", "hidden");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("libelle", "text");
            $form->setType("typeavis", "text");
            $form->setType("sitadel", "text");
            $form->setType("sitadel_motif", "text");
            $form->setType("avis_decision", "hiddenstatic");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("libelle", "hiddenstatic");
            $form->setType("typeavis", "hiddenstatic");
            $form->setType("sitadel", "hiddenstatic");
            $form->setType("sitadel_motif", "hiddenstatic");
            $form->setType("avis_decision", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("libelle", "static");
            $form->setType("typeavis", "static");
            $form->setType("sitadel", "static");
            $form->setType("sitadel_motif", "static");
            $form->setType("avis_decision", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('avis_decision','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("libelle", 30);
        $form->setTaille("typeavis", 10);
        $form->setTaille("sitadel", 10);
        $form->setTaille("sitadel_motif", 10);
        $form->setTaille("avis_decision", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("libelle", 50);
        $form->setMax("typeavis", 1);
        $form->setMax("sitadel", 1);
        $form->setMax("sitadel_motif", 1);
        $form->setMax("avis_decision", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('libelle',_('libelle'));
        $form->setLib('typeavis',_('typeavis'));
        $form->setLib('sitadel',_('sitadel'));
        $form->setLib('sitadel_motif',_('sitadel_motif'));
        $form->setLib('avis_decision',_('avis_decision'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : dossier
        $this->rechercheTable($this->f->db, "dossier", "avis_decision", $id);
        // Verification de la cle secondaire : dossier_autorisation
        $this->rechercheTable($this->f->db, "dossier_autorisation", "avis_decision", $id);
        // Verification de la cle secondaire : evenement
        $this->rechercheTable($this->f->db, "evenement", "avis_decision", $id);
        // Verification de la cle secondaire : instruction
        $this->rechercheTable($this->f->db, "instruction", "avis_decision", $id);
    }


}

?>
