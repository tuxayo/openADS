<?php
//$Id$ 
//gen openMairie le 07/12/2016 12:40

require_once "../obj/om_dbform.class.php";

class consultation_gen extends om_dbform {

    var $table = "consultation";
    var $clePrimaire = "consultation";
    var $typeCle = "N";
    var $required_field = array(
        "consultation",
        "date_envoi",
        "dossier",
        "service"
    );
    var $unique_key = array(
      "code_barres",
    );
    var $foreign_keys_extended = array(
        "avis_consultation" => array("avis_consultation", ),
        "dossier" => array("dossier", "dossier_instruction", "dossier_instruction_mes_encours", "dossier_instruction_tous_encours", "dossier_instruction_mes_clotures", "dossier_instruction_tous_clotures", "dossier_contentieux", "dossier_contentieux_mes_infractions", "dossier_contentieux_toutes_infractions", "dossier_contentieux_mes_recours", "dossier_contentieux_tous_recours", ),
        "service" => array("service", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['consultation'])) {
            $this->valF['consultation'] = ""; // -> requis
        } else {
            $this->valF['consultation'] = $val['consultation'];
        }
        $this->valF['dossier'] = $val['dossier'];
        if ($val['date_envoi'] != "") {
            $this->valF['date_envoi'] = $this->dateDB($val['date_envoi']);
        }
        if ($val['date_retour'] != "") {
            $this->valF['date_retour'] = $this->dateDB($val['date_retour']);
        } else {
            $this->valF['date_retour'] = NULL;
        }
        if ($val['date_limite'] != "") {
            $this->valF['date_limite'] = $this->dateDB($val['date_limite']);
        } else {
            $this->valF['date_limite'] = NULL;
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if (!is_numeric($val['avis_consultation'])) {
            $this->valF['avis_consultation'] = NULL;
        } else {
            $this->valF['avis_consultation'] = $val['avis_consultation'];
        }
        if ($val['date_reception'] != "") {
            $this->valF['date_reception'] = $this->dateDB($val['date_reception']);
        } else {
            $this->valF['date_reception'] = NULL;
        }
            $this->valF['motivation'] = $val['motivation'];
        if ($val['fichier'] == "") {
            $this->valF['fichier'] = NULL;
        } else {
            $this->valF['fichier'] = $val['fichier'];
        }
        if ($val['lu'] == 1 || $val['lu'] == "t" || $val['lu'] == "Oui") {
            $this->valF['lu'] = true;
        } else {
            $this->valF['lu'] = false;
        }
        if ($val['code_barres'] == "") {
            $this->valF['code_barres'] = NULL;
        } else {
            $this->valF['code_barres'] = $val['code_barres'];
        }
        if ($val['om_fichier_consultation'] == "") {
            $this->valF['om_fichier_consultation'] = NULL;
        } else {
            $this->valF['om_fichier_consultation'] = $val['om_fichier_consultation'];
        }
        if ($val['om_final_consultation'] == 1 || $val['om_final_consultation'] == "t" || $val['om_final_consultation'] == "Oui") {
            $this->valF['om_final_consultation'] = true;
        } else {
            $this->valF['om_final_consultation'] = false;
        }
        if ($val['marque'] == 1 || $val['marque'] == "t" || $val['marque'] == "Oui") {
            $this->valF['marque'] = true;
        } else {
            $this->valF['marque'] = false;
        }
        if ($val['visible'] == 1 || $val['visible'] == "t" || $val['visible'] == "Oui") {
            $this->valF['visible'] = true;
        } else {
            $this->valF['visible'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("consultation", "hidden");
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("dossier", "selecthiddenstatic");
            } else {
                $form->setType("dossier", "select");
            }
            $form->setType("date_envoi", "date");
            $form->setType("date_retour", "date");
            $form->setType("date_limite", "date");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->is_in_context_of_foreign_key("avis_consultation", $this->retourformulaire)) {
                $form->setType("avis_consultation", "selecthiddenstatic");
            } else {
                $form->setType("avis_consultation", "select");
            }
            $form->setType("date_reception", "date");
            $form->setType("motivation", "textarea");
            $form->setType("fichier", "text");
            $form->setType("lu", "checkbox");
            $form->setType("code_barres", "text");
            $form->setType("om_fichier_consultation", "text");
            $form->setType("om_final_consultation", "checkbox");
            $form->setType("marque", "checkbox");
            $form->setType("visible", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("consultation", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("dossier", $this->retourformulaire)) {
                $form->setType("dossier", "selecthiddenstatic");
            } else {
                $form->setType("dossier", "select");
            }
            $form->setType("date_envoi", "date");
            $form->setType("date_retour", "date");
            $form->setType("date_limite", "date");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->is_in_context_of_foreign_key("avis_consultation", $this->retourformulaire)) {
                $form->setType("avis_consultation", "selecthiddenstatic");
            } else {
                $form->setType("avis_consultation", "select");
            }
            $form->setType("date_reception", "date");
            $form->setType("motivation", "textarea");
            $form->setType("fichier", "text");
            $form->setType("lu", "checkbox");
            $form->setType("code_barres", "text");
            $form->setType("om_fichier_consultation", "text");
            $form->setType("om_final_consultation", "checkbox");
            $form->setType("marque", "checkbox");
            $form->setType("visible", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("consultation", "hiddenstatic");
            $form->setType("dossier", "selectstatic");
            $form->setType("date_envoi", "hiddenstatic");
            $form->setType("date_retour", "hiddenstatic");
            $form->setType("date_limite", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("avis_consultation", "selectstatic");
            $form->setType("date_reception", "hiddenstatic");
            $form->setType("motivation", "hiddenstatic");
            $form->setType("fichier", "hiddenstatic");
            $form->setType("lu", "hiddenstatic");
            $form->setType("code_barres", "hiddenstatic");
            $form->setType("om_fichier_consultation", "hiddenstatic");
            $form->setType("om_final_consultation", "hiddenstatic");
            $form->setType("marque", "hiddenstatic");
            $form->setType("visible", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("consultation", "static");
            $form->setType("dossier", "selectstatic");
            $form->setType("date_envoi", "datestatic");
            $form->setType("date_retour", "datestatic");
            $form->setType("date_limite", "datestatic");
            $form->setType("service", "selectstatic");
            $form->setType("avis_consultation", "selectstatic");
            $form->setType("date_reception", "datestatic");
            $form->setType("motivation", "textareastatic");
            $form->setType("fichier", "static");
            $form->setType("lu", "checkboxstatic");
            $form->setType("code_barres", "static");
            $form->setType("om_fichier_consultation", "static");
            $form->setType("om_final_consultation", "checkboxstatic");
            $form->setType("marque", "checkboxstatic");
            $form->setType("visible", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('consultation','VerifNum(this)');
        $form->setOnchange('date_envoi','fdate(this)');
        $form->setOnchange('date_retour','fdate(this)');
        $form->setOnchange('date_limite','fdate(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('avis_consultation','VerifNum(this)');
        $form->setOnchange('date_reception','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("consultation", 11);
        $form->setTaille("dossier", 30);
        $form->setTaille("date_envoi", 12);
        $form->setTaille("date_retour", 12);
        $form->setTaille("date_limite", 12);
        $form->setTaille("service", 11);
        $form->setTaille("avis_consultation", 11);
        $form->setTaille("date_reception", 12);
        $form->setTaille("motivation", 80);
        $form->setTaille("fichier", 30);
        $form->setTaille("lu", 1);
        $form->setTaille("code_barres", 12);
        $form->setTaille("om_fichier_consultation", 30);
        $form->setTaille("om_final_consultation", 1);
        $form->setTaille("marque", 1);
        $form->setTaille("visible", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("consultation", 11);
        $form->setMax("dossier", 30);
        $form->setMax("date_envoi", 12);
        $form->setMax("date_retour", 12);
        $form->setMax("date_limite", 12);
        $form->setMax("service", 11);
        $form->setMax("avis_consultation", 11);
        $form->setMax("date_reception", 12);
        $form->setMax("motivation", 6);
        $form->setMax("fichier", 100);
        $form->setMax("lu", 1);
        $form->setMax("code_barres", 12);
        $form->setMax("om_fichier_consultation", 64);
        $form->setMax("om_final_consultation", 1);
        $form->setMax("marque", 1);
        $form->setMax("visible", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('consultation',_('consultation'));
        $form->setLib('dossier',_('dossier'));
        $form->setLib('date_envoi',_('date_envoi'));
        $form->setLib('date_retour',_('date_retour'));
        $form->setLib('date_limite',_('date_limite'));
        $form->setLib('service',_('service'));
        $form->setLib('avis_consultation',_('avis_consultation'));
        $form->setLib('date_reception',_('date_reception'));
        $form->setLib('motivation',_('motivation'));
        $form->setLib('fichier',_('fichier'));
        $form->setLib('lu',_('lu'));
        $form->setLib('code_barres',_('code_barres'));
        $form->setLib('om_fichier_consultation',_('om_fichier_consultation'));
        $form->setLib('om_final_consultation',_('om_final_consultation'));
        $form->setLib('marque',_('marque'));
        $form->setLib('visible',_('visible'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // avis_consultation
        $this->init_select($form, $this->f->db, $maj, null, "avis_consultation", $sql_avis_consultation, $sql_avis_consultation_by_id, true);
        // dossier
        $this->init_select($form, $this->f->db, $maj, null, "dossier", $sql_dossier, $sql_dossier_by_id, false);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('avis_consultation', $this->retourformulaire))
                $form->setVal('avis_consultation', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier', $this->retourformulaire))
                $form->setVal('dossier', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
