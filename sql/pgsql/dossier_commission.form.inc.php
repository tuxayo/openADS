<?php
//$Id: dossier_commission.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 07/12/2012 17:34

include('../gen/sql/pgsql/dossier_commission.form.inc.php');

// Récupèration des paramètres
(isset($_GET['idxformulaire']) ? $idx = $_GET['idxformulaire'] : $idx = "");
(isset($_GET['retourformulaire']) ? $retourformulaire = $_GET['retourformulaire'] : $retourformulaire = "");
(isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "");

// Marquée comme lue
$portlet_actions['lu'] =
    array('lien' => "#\" onclick=\"portletUpdateData(",
          'id' => ",'dossier_commission','dossier_commission', 'marquer_comme_lu', 'lu', 'Oui');",
          'lib' => "<span class=\"om-prev-icon om-icon-16 om-icon-fix lu-16\" title=\"".
                _("Edition")."\">"._("Marquer comme lu")."</span>",
          'ordre' => 30,
          'rights' => array('list' => array($obj, $obj."_modifier_lu"), 'operator' => 'OR'),
          'ajax' => false);

// Surcharge bouton modifier
$portlet_actions['modifier'] =
    array('lien' => "#\" onclick=\"ajaxIt('dossier_commission','../scr/sousform.php?obj=$obj&action=1&idx=",
          'id' => "&premiersf=0&trisf=&retourformulaire=$retourformulaire&idxformulaire=$idx&retour=form')",
          'lib' => "<span class=\"om-prev-icon om-icon-16 om-icon-fix edit-16\" title=\"".
                _("Modifier")."\">"._("Modifier")."</span>",
          'ordre' => 10,
          'rights' => array('list' => array($obj, $obj.'_modifier_instruction'), 'operator' => 'OR'),
          'ajax' => false);

// Surcharge bouton supprimer
$portlet_actions['supprimer'] =
    array('lien' => "#\" onclick=\"ajaxIt('dossier_commission','../scr/sousform.php?obj=$obj&action=2&idx=",
          'id' => "&premiersf=0&trisf=&retourformulaire=$retourformulaire&idxformulaire=$idx&retour=form')",
          'lib' => "<span class=\"om-prev-icon om-icon-16 om-icon-fix delete-16\" title=\"".
                _("Supprimer")."\">"._("Supprimer")."</span>",
          'ordre' => 10,
          'rights' => array('list' => array($obj, $obj.'_supprimer_instruction'), 'operator' => 'OR'),
          'ajax' => false);

$sql_commission_type_by_di = "SELECT
commission_type.commission_type, commission_type.libelle
FROM " . DB_PREFIXE . "commission_type
WHERE commission_type.om_collectivite = <collectivite_di>
    AND ((commission_type.om_validite_debut IS NULL
      AND (commission_type.om_validite_fin IS NULL OR commission_type.om_validite_fin > CURRENT_DATE))
      OR (commission_type.om_validite_debut <= CURRENT_DATE
      AND (commission_type.om_validite_fin IS NULL
      OR commission_type.om_validite_fin > CURRENT_DATE)))
ORDER BY commission_type.libelle ASC";

?>