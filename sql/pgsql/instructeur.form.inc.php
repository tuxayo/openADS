<?php
//$Id: instructeur.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 17/10/2012 17:46

include('../gen/sql/pgsql/instructeur.form.inc.php');

// Récupère la liste des utilisateurs appartement à la même collectivité que la
// direction de la division sur laquelle la condition est faite
$sql_utilisateur_by_division = "SELECT
    om_utilisateur.om_utilisateur,
    om_utilisateur.nom as lib
FROM " . DB_PREFIXE . "division
    INNER JOIN " . DB_PREFIXE . "direction
        ON division.direction = direction.direction
    INNER JOIN " . DB_PREFIXE . "om_utilisateur
        ON om_utilisateur.om_collectivite = direction.om_collectivite
WHERE division.division = '<id_division>'
    AND ((division.om_validite_debut IS NULL AND (division.om_validite_fin IS NULL
        OR division.om_validite_fin > CURRENT_DATE)) OR (division.om_validite_debut
            <= CURRENT_DATE AND (division.om_validite_fin IS NULL
                OR division.om_validite_fin > CURRENT_DATE)))
ORDER BY om_utilisateur.nom";

// Récuperation des division par l'utilisateur
$sql_division_by_utilisateur = "SELECT
    division.division,
    division.libelle as lib
FROM " . DB_PREFIXE . "division
INNER JOIN " . DB_PREFIXE . "direction
    ON division.direction = direction.direction
INNER JOIN " . DB_PREFIXE . "om_utilisateur
    ON om_utilisateur.om_collectivite = direction.om_collectivite
WHERE om_utilisateur.om_utilisateur = '<id_utilisateur>'
ORDER BY division.libelle";

// Si on est en mono on doit voir que les divisions de sa collectivite
$sql_division_by_collectivite = "SELECT
    division.division, division.libelle
FROM " . DB_PREFIXE . "division
INNER JOIN " . DB_PREFIXE . "direction
    ON division.direction = direction.direction
WHERE ((division.om_validite_debut IS NULL AND (division.om_validite_fin
            IS NULL OR division.om_validite_fin > CURRENT_DATE))
        OR (division.om_validite_debut <= CURRENT_DATE AND (division.om_validite_fin
            IS NULL OR division.om_validite_fin > CURRENT_DATE)))
    AND om_collectivite = <id_collectivite>
ORDER BY division.libelle ASC";

?>
