<?php
//$Id: dossier.import.inc.php 4587 2015-04-15 16:15:22Z stimezouaght $ 
//gen openMairie le 10/04/2015 20:10

$import= "Insertion dans la table dossier voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."dossier";
$id=''; // numerotation non automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "dossier" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "30",
    ),
    "annee" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "2",
    ),
    "etat" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "20",
        "fkey" => array(
            "foreign_table_name" => "etat",
            "foreign_column_name" => "etat",
            "sql_exist" => "select * from ".DB_PREFIXE."etat where etat = '",
        ),
    ),
    "instructeur" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "instructeur",
            "foreign_column_name" => "instructeur",
            "sql_exist" => "select * from ".DB_PREFIXE."instructeur where instructeur = '",
        ),
    ),
    "date_demande" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "date_depot" => array(
        "notnull" => "1",
        "type" => "date",
        "len" => "12",
    ),
    "date_complet" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "date_rejet" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "date_notification_delai" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "delai" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
    ),
    "date_limite" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "accord_tacite" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "3",
    ),
    "date_decision" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "date_validite" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "date_chantier" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "date_achevement" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "date_conformite" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "description" => array(
        "notnull" => "1",
        "type" => "blob",
        "len" => "-5",
    ),
    "erp" => array(
        "notnull" => "",
        "type" => "bool",
        "len" => "1",
    ),
    "avis_decision" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "avis_decision",
            "foreign_column_name" => "avis_decision",
            "sql_exist" => "select * from ".DB_PREFIXE."avis_decision where avis_decision = '",
        ),
    ),
    "enjeu_erp" => array(
        "notnull" => "",
        "type" => "bool",
        "len" => "1",
    ),
    "enjeu_urba" => array(
        "notnull" => "",
        "type" => "bool",
        "len" => "1",
    ),
    "division" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "division",
            "foreign_column_name" => "division",
            "sql_exist" => "select * from ".DB_PREFIXE."division where division = '",
        ),
    ),
    "autorite_competente" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "autorite_competente",
            "foreign_column_name" => "autorite_competente",
            "sql_exist" => "select * from ".DB_PREFIXE."autorite_competente where autorite_competente = '",
        ),
    ),
    "a_qualifier" => array(
        "notnull" => "",
        "type" => "bool",
        "len" => "1",
    ),
    "terrain_references_cadastrales" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "terrain_adresse_voie_numero" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "20",
    ),
    "terrain_adresse_voie" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "30",
    ),
    "terrain_adresse_lieu_dit" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "30",
    ),
    "terrain_adresse_localite" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "30",
    ),
    "terrain_adresse_code_postal" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "terrain_adresse_bp" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "15",
    ),
    "terrain_adresse_cedex" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "15",
    ),
    "terrain_superficie" => array(
        "notnull" => "",
        "type" => "float",
        "len" => "20",
    ),
    "dossier_autorisation" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "20",
        "fkey" => array(
            "foreign_table_name" => "dossier_autorisation",
            "foreign_column_name" => "dossier_autorisation",
            "sql_exist" => "select * from ".DB_PREFIXE."dossier_autorisation where dossier_autorisation = '",
        ),
    ),
    "dossier_instruction_type" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "dossier_instruction_type",
            "foreign_column_name" => "dossier_instruction_type",
            "sql_exist" => "select * from ".DB_PREFIXE."dossier_instruction_type where dossier_instruction_type = '",
        ),
    ),
    "date_dernier_depot" => array(
        "notnull" => "1",
        "type" => "date",
        "len" => "12",
    ),
    "version" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
    ),
    "incompletude" => array(
        "notnull" => "1",
        "type" => "bool",
        "len" => "1",
    ),
    "evenement_suivant_tacite" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "evenement",
            "foreign_column_name" => "evenement",
            "sql_exist" => "select * from ".DB_PREFIXE."evenement where evenement = '",
        ),
    ),
    "evenement_suivant_tacite_incompletude" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "evenement",
            "foreign_column_name" => "evenement",
            "sql_exist" => "select * from ".DB_PREFIXE."evenement where evenement = '",
        ),
    ),
    "etat_pendant_incompletude" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "20",
        "fkey" => array(
            "foreign_table_name" => "etat",
            "foreign_column_name" => "etat",
            "sql_exist" => "select * from ".DB_PREFIXE."etat where etat = '",
        ),
    ),
    "date_limite_incompletude" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "delai_incompletude" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
    ),
    "dossier_libelle" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "25",
    ),
    "numero_versement_archive" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "100",
    ),
    "duree_validite" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
    ),
    "quartier" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "quartier",
            "foreign_column_name" => "quartier",
            "sql_exist" => "select * from ".DB_PREFIXE."quartier where quartier = '",
        ),
    ),
    "incomplet_notifie" => array(
        "notnull" => "",
        "type" => "bool",
        "len" => "1",
    ),
    "geom" => array(
        "notnull" => "",
        "type" => "geom",
        "len" => "551424",
    ),
    "geom1" => array(
        "notnull" => "",
        "type" => "geom",
        "len" => "551444",
    ),
    "om_collectivite" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "om_collectivite",
            "foreign_column_name" => "om_collectivite",
            "sql_exist" => "select * from ".DB_PREFIXE."om_collectivite where om_collectivite = '",
        ),
    ),
);
?>