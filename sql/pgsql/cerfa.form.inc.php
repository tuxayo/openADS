<?php
//$Id: cerfa.form.inc.php 5856 2016-02-03 11:35:25Z stimezouaght $ 
//gen openMairie le 13/02/2013 14:41

include('../gen/sql/pgsql/cerfa.form.inc.php');

//Affiche les champs dans l'ordre souhaité
$champs=array(
    "cerfa",
    "libelle",
    "code",
    "om_validite_debut",
    "om_validite_fin",
    //Début fieldset 3
        "terr_juri_titul",
        "terr_juri_lot",
        "terr_juri_zac",
        "terr_juri_afu",
        "terr_juri_pup",
        "terr_juri_oin",
        "terr_juri_desc",
        "terr_div_surf_etab",
        "terr_div_surf_av_div",
        "ope_proj_desc",
        "ope_proj_div_co",
        "ope_proj_div_contr",

        // ERP
        "erp_class_cat",
        "erp_class_type",
        "erp_cstr_neuve",
        "erp_trvx_acc",
        "erp_extension",
        "erp_rehab",
        "erp_trvx_am",
        "erp_vol_nouv_exist",
        "tab_erp_eff",

        //Debut premier bloc 4.1
        "am_lotiss",
        "am_div_mun",
        "am_autre_div",
        "am_camping",
        "am_parc_resid_loi",
        "am_sport_moto",
        "am_sport_attrac",
        "am_sport_golf",
        "am_caravane",
        "am_carav_duree",
        "am_statio",
        "am_statio_cont",
        "am_affou_exhau",
        "am_affou_exhau_sup",
        "am_affou_prof",
        "am_exhau_haut",
        "am_terr_res_demon",
        "am_air_terr_res_mob",
        "am_chem_ouv_esp",
        "am_agri_peche",
        "am_crea_voie",
        "am_modif_voie_exist",
        "am_crea_esp_sauv",
        "am_crea_esp_class",
        "am_coupe_abat",
        "am_prot_plu",
        "am_prot_muni",
        "am_mobil_voyage",
        "am_aire_voyage",
        "am_rememb_afu",
        "co_ouvr_infra",
        "am_mob_art",
        "am_modif_voie_esp",
        "am_plant_voie_esp",
        "co_ouvr_elec",

        //Autre champ du premier bloc, ancien cerfa
        "am_projet_desc",
        "am_terr_surf",
        "am_tranche_desc",
        
        //Fin premier bloc

        //Début second bloc 4.2
        "am_lot_max_nb",
        "am_lot_max_shon",
        "am_lot_cstr_cos",
        "am_lot_cstr_plan",
        "am_lot_cstr_vente",
        "am_lot_fin_diff",
        "am_lot_consign",
        "am_lot_gar_achev",
        "am_lot_vente_ant",
        

        //Fin second bloc

        //Début troisième bloc 4.3
        "am_exist_agrand",
        "am_exist_date",
        "am_exist_num",
        "am_exist_nb_avant",
        "am_exist_nb_apres",
        "am_empl_nb",
        "am_tente_nb",
        "am_carav_nb",
        "am_mobil_nb",
        "am_pers_nb",
        "am_empl_hll_nb",
        "am_hll_shon",
        "am_periode_exploit",
        
        "am_coupe_bois",
        "am_coupe_parc",
        "am_coupe_align",
        "am_coupe_ess",
        "am_coupe_age",
        "am_coupe_dens",
        "am_coupe_qual",
        "am_coupe_trait",
        "am_coupe_autr",


        //Fin troisième bloc
    //Fin premier fieldset
    //
    ////Début second fieldset
        //Début premier bloc 5.1
        "co_archi_recours",
        "architecte",
        //Fin premier bloc

        //Début second bloc 5.2
        "co_cstr_nouv",
        "avap_co_elt_pro",
        "avap_nouv_haut_surf",
        "co_cstr_exist",
        "co_div_terr",
        "co_cloture",
        "co_projet_desc",
        "co_elec_tension",

        //Fin autre champ du second fieldset, ancien cerfa
        
        //Fin second bloc

        //Début troisième bloc 5.3
        "co_tot_log_nb",
        "co_tot_ind_nb",
        "co_tot_coll_nb",
        "co_mais_piece_nb",
        "co_mais_niv_nb",
        "co_fin_lls_nb",
        "co_fin_aa_nb",
        "co_fin_ptz_nb",
        "co_fin_autr_nb",
        "co_fin_autr_desc",
        "co_mais_contrat_ind",
        "co_uti_pers",
        "co_uti_vente",
        "co_uti_loc",
        "co_uti_princ",
        "co_uti_secon",

        "co_anx_pisc",
        "co_anx_gara",
        "co_anx_veran",
        "co_anx_abri",
        "co_anx_autr",
        "co_anx_autr_desc",

        "co_resid_agees",
        "co_resid_etud",
        "co_resid_tourism",
        "co_resid_hot_soc",
        "co_resid_soc",
        "co_resid_hand",
        "co_resid_autr",
        "co_resid_autr_desc",
        "co_foyer_chamb_nb",
        "co_log_1p_nb",
        "co_log_2p_nb",
        "co_log_3p_nb",
        "co_log_4p_nb",
        "co_log_5p_nb",
        "co_log_6p_nb",
        "co_bat_niv_nb",
        "co_trx_exten",
        "co_trx_surelev",
        "co_trx_nivsup",

        //Fin autre champ du troisième fieldset, ancien cerfa
        
        //Fin troisième bloc

        //Début quatrième bloc 5.4
        "co_demont_periode",
        //Fin quatrième bloc

        //Début cinquième bloc 5.5
        "tab_surface",
        "co_sp_transport",
        "co_sp_enseign",
        "co_sp_act_soc",
        "co_sp_ouvr_spe",
        "co_sp_sante",
        "co_sp_culture",
        //Fin cinquième bloc

        //Début sixième bloc 5.6
        "tab_surface2",
        //Fin sixième bloc

        //Début septième bloc 5.7
        "co_statio_avt_nb",
        "co_statio_apr_nb",
        "co_statio_adr",
        "co_statio_place_nb",
        "co_statio_tot_surf",
        "co_statio_tot_shob",
        "co_statio_comm_cin_surf",
        "co_perf_energ",

        //Fin septième bloc
    //Fin second fieldset

        //Fin septième bloc
    //Fin second fieldset
        //Début troisième fieldset
        //Début premier bloc
        "dm_constr_dates",
        "dm_total",
        "dm_partiel",
        "dm_projet_desc",
        "dm_tot_log_nb",
        //Fin premier bloc  
    //Fin troisième fieldset
    // Doc/Daact
        "doc_date",
        "doc_tot_trav",
        "doc_tranche_trav",
        "doc_tranche_trav_desc",
        "doc_surf",
        "doc_nb_log",
        "doc_nb_log_indiv",
        "doc_nb_log_coll",
        "doc_nb_log_lls",
        "doc_nb_log_aa",
        "doc_nb_log_ptz",
        "doc_nb_log_autre",
        "daact_date",
        "daact_date_chgmt_dest",
        "daact_tot_trav",
        "daact_tranche_trav",
        "daact_tranche_trav_desc",
        "daact_surf",
        "daact_nb_log",
        "daact_nb_log_indiv",
        "daact_nb_log_coll",
        "daact_nb_log_lls",
        "daact_nb_log_aa",
        "daact_nb_log_ptz",
        "daact_nb_log_autre",

        // Début DIA
        // Désignation du bien
        "dia_imm_non_bati",
        "dia_imm_bati_terr_propr",
        "dia_imm_bati_terr_autr",
        "dia_imm_bati_terr_autr_desc",
        "dia_occ_sol_su_terre",
        "dia_occ_sol_su_pres",
        "dia_occ_sol_su_verger",
        "dia_occ_sol_su_vigne",
        "dia_occ_sol_su_bois",
        "dia_occ_sol_su_lande",
        "dia_occ_sol_su_carriere",
        "dia_occ_sol_su_eau_cadastree",
        "dia_occ_sol_su_jardin",
        "dia_occ_sol_su_terr_batir",
        "dia_occ_sol_su_terr_agr",
        "dia_occ_sol_su_sol",
        "dia_bati_vend_tot",
        "dia_bati_vend_tot_txt",
        "dia_su_co_sol",
        "dia_su_util_hab",
        "dia_nb_niv",
        "dia_nb_appart",
        "dia_nb_autre_loc",
        "dia_vente_lot_volume",
        "dia_vente_lot_volume_txt",
        "dia_bat_copro",
        "dia_bat_copro_desc",
        "dia_lot_numero",
        "dia_lot_bat",
        "dia_lot_etage",
        "dia_lot_quote_part",
        "dia_lot_nat_su",
        "dia_lot_bat_achv_plus_10",
        "dia_lot_bat_achv_moins_10",
        "dia_lot_regl_copro_publ_hypo_plus_10",
        "dia_lot_regl_copro_publ_hypo_moins_10",
        "dia_indivi_quote_part",
        "dia_design_societe",
        "dia_design_droit",
        "dia_droit_soc_nat",
        "dia_droit_soc_nb",
        "dia_droit_soc_num_part",
        // Usage et occupation
        "dia_us_hab",
        "dia_us_pro",
        "dia_us_mixte",
        "dia_us_comm",
        "dia_us_agr",
        "dia_us_autre",
        "dia_us_autre_prec",
        "dia_occ_prop",
        "dia_occ_loc",
        "dia_occ_sans_occ",
        "dia_occ_autre",
        "dia_occ_autre_prec",
        // Droits réels ou personnels
        "dia_droit_reel_perso_grevant_bien_oui",
        "dia_droit_reel_perso_grevant_bien_non",
        "dia_droit_reel_perso_nat",
        "dia_droit_reel_perso_viag",
        // Modalités de la cession
        "dia_mod_cess_prix_vente",
        "dia_mod_cess_prix_vente_mob",
        "dia_mod_cess_prix_vente_cheptel",
        "dia_mod_cess_prix_vente_recol",
        "dia_mod_cess_prix_vente_autre",
        "dia_mod_cess_adr",
        "dia_mod_cess_sign_act_auth",
        "dia_mod_cess_terme",
        "dia_mod_cess_terme_prec",
        "dia_mod_cess_commi",
        "dia_mod_cess_commi_ttc",
        "dia_mod_cess_commi_ht",
        "dia_mod_cess_bene_acquereur",
        "dia_mod_cess_bene_vendeur",
        "dia_mod_cess_paie_nat",
        "dia_mod_cess_design_contr_alien",
        "dia_mod_cess_eval_contr",
        "dia_mod_cess_rente_viag",
        "dia_mod_cess_mnt_an",
        "dia_mod_cess_mnt_compt",
        "dia_mod_cess_bene_rente",
        "dia_mod_cess_droit_usa_hab",
        "dia_mod_cess_droit_usa_hab_prec",
        "dia_mod_cess_eval_usa_usufruit",
        "dia_mod_cess_vente_nue_prop",
        "dia_mod_cess_vente_nue_prop_prec",
        "dia_mod_cess_echange",
        "dia_mod_cess_design_bien_recus_ech",
        "dia_mod_cess_mnt_soulte",
        "dia_mod_cess_prop_contre_echan",
        "dia_mod_cess_apport_societe",
        "dia_mod_cess_bene",
        "dia_mod_cess_esti_bien",
        "dia_mod_cess_cess_terr_loc_co",
        "dia_mod_cess_esti_terr",
        "dia_mod_cess_esti_loc",
        "dia_mod_cess_esti_imm_loca",
        "dia_mod_cess_adju_vol",
        "dia_mod_cess_adju_obl",
        "dia_mod_cess_adju_fin_indivi",
        "dia_mod_cess_adju_date_lieu",
        "dia_mod_cess_mnt_mise_prix",
        // Les soussignés déclarent
        "dia_prop_titu_prix_indique",
        "dia_prop_recherche_acqu_prix_indique",
        "dia_acquereur_nom_prenom",
        "dia_acquereur_prof",
        "dia_acquereur_adr_num_voie",
        "dia_acquereur_adr_ext",
        "dia_acquereur_adr_type_voie",
        "dia_acquereur_adr_nom_voie",
        "dia_acquereur_adr_lieu_dit_bp",
        "dia_acquereur_adr_cp",
        "dia_acquereur_adr_localite",
        "dia_indic_compl_ope",
        "dia_vente_adju",
        "dia_observation",
        // Fin DIA

        "code_cnil",
    //Début fieldset 1 des taxes
        //Bloc 1.1
        "tax_surf_tot_cstr",
        "tax_surf_loc_stat",
        "tax_surf_tot",
        "tax_surf",
        "tax_surf_suppr_mod",
        //Fin bloc 1.1
        //Bloc 1.2
        //Bloc 1.2.1
        "tab_tax_su_princ",
        "tab_tax_su_secon",
        "tab_tax_su_heber",
        "tab_tax_su_tot",
        //Fin bloc 1.2.1
        //Bloc 1.2.2
        "tax_ext_pret",
        "tax_ext_desc",
        "tax_surf_tax_exist_cons",
        "tax_log_exist_nb",
        "tax_log_ap_trvx_nb",
        "tax_surf_abr_jard_pig_colom",
        // Fin bloc 1.2.2
        //Bloc 1.2.3
        "tax_comm_nb",
        "tab_tax_su_non_habit_surf",
        "tab_tax_su_parc_statio_expl_comm",
        "tax_su_non_habit_abr_jard_pig_colom",
        //Fin bloc 1.2.3
        //Bloc 1.3
        "tab_tax_am",
        "tax_am_statio_ext_cr",
        "tax_sup_bass_pisc_cr",
        "tax_empl_ten_carav_mobil_nb_cr",
        "tax_empl_hll_nb_cr",
        "tax_eol_haut_nb_cr",
        "tax_pann_volt_sup_cr",
        //Fin bloc 1.3
        //Bloc 1.4
        "tax_surf_loc_arch",
        "tax_surf_pisc_arch",
        "tax_am_statio_ext_arch",
        "tax_empl_ten_carav_mobil_nb_arch",
        "tax_empl_hll_nb_arch",
        "tax_eol_haut_nb_arch",
        //Fin bloc 1.4
        //Bloc 1.5
        "tax_trx_presc_ppr",
        "tax_monu_hist",
        //Fin bloc 1.5
    //Fieldset 2 taxes
        //Bloc 2.1
        "vsd_surf_planch_smd",
        "vsd_unit_fonc_sup",
        "vsd_unit_fonc_constr_sup",
        "vsd_val_terr",
        "vsd_const_sxist_non_dem_surf",
        "vsd_rescr_fisc",
        //Fin Bloc 2.1
        //Bloc 2.2
        "pld_val_terr",
        "pld_const_exist_dem",
        "pld_const_exist_dem_surf",
        // Autres renseignements
        "tax_desc",
        // Fieldset Exonération
        // Fieldset TA
        // Exonérations de plein droit TA
        "exo_ta_1",
        "exo_ta_2",
        "exo_ta_3",
        "exo_ta_4",
        "exo_ta_5",
        "exo_ta_6",
        "exo_ta_7",
        "exo_ta_8",
        "exo_ta_9",
        // Exonérations facultatives TA
        "exo_facul_1",
        "exo_facul_2",
        "exo_facul_3",
        "exo_facul_4",
        "exo_facul_5",
        "exo_facul_6",
        "exo_facul_7",
        "exo_facul_8",
        "exo_facul_9",
        // Montants exonération TA
        "mtn_exo_ta_part_commu",
        "mtn_exo_ta_part_depart",
        "mtn_exo_ta_part_reg",
        // Fin fieldset TA
        // Fieldset RAP
        // Exonération RAP
        "exo_rap_1",
        "exo_rap_2",
        "exo_rap_3",
        "exo_rap_4",
        "exo_rap_5",
        "exo_rap_6",
        "exo_rap_7",
        "exo_rap_8",
        // Montant exonération RAP
        "mtn_exo_rap",
        // Fin fieldset RAP
        // Fin fieldset Exonération

        // Champs pour les dossiers contentieux
        "ctx_objet_recours",
        "ctx_moyen_souleve",
        "ctx_moyen_retenu_juge",
        "ctx_reference_sagace",
        "ctx_nature_travaux_infra_om_html",
        "ctx_synthese_nti",
        "ctx_article_non_resp_om_html",
        "ctx_synthese_anr",
        "ctx_reference_parquet",
        "ctx_element_taxation",
        "ctx_infraction",
        "ctx_regularisable",
        "ctx_reference_courrier",
        "ctx_date_audience",
        "ctx_date_ajournement",

        // Droit de préemption commercial
        "dpc_type",
        "dpc_desc_actv_ex",
        "dpc_desc_ca",
        "dpc_desc_aut_prec",
        "dpc_desig_comm_arti",
        "dpc_desig_loc_hab",
        "dpc_desig_loc_ann",
        "dpc_desig_loc_ann_prec",
        "dpc_bail_comm_date",
        "dpc_bail_comm_loyer",
        "dpc_actv_acqu",
        "dpc_nb_sala_di",
        "dpc_nb_sala_dd",
        "dpc_nb_sala_tc",
        "dpc_nb_sala_tp",
        "dpc_moda_cess_vente_am",
        "dpc_moda_cess_adj",
        "dpc_moda_cess_prix",
        "dpc_moda_cess_adj_date",
        "dpc_moda_cess_adj_prec",
        "dpc_moda_cess_paie_comp",
        "dpc_moda_cess_paie_terme",
        "dpc_moda_cess_paie_terme_prec",
        "dpc_moda_cess_paie_nat",
        "dpc_moda_cess_paie_nat_desig_alien",
        "dpc_moda_cess_paie_nat_desig_alien_prec",
        "dpc_moda_cess_paie_nat_eval",
        "dpc_moda_cess_paie_nat_eval_prec",
        "dpc_moda_cess_paie_aut",
        "dpc_moda_cess_paie_aut_prec",
        "dpc_ss_signe_demande_acqu",
        "dpc_ss_signe_recher_trouv_acqu",
        "dpc_notif_adr_prop",
        "dpc_notif_adr_manda",
        "dpc_obs",

        // Anciens champs
        "co_statio_avt_shob",
        "co_statio_apr_shob",
        "co_statio_avt_surf",
        "co_statio_apr_surf",
        "co_trx_amgt",
        "co_modif_aspect",
        "co_modif_struct",
        "co_trx_imm",
        "co_cstr_shob",
        "am_voyage_deb",
        "am_voyage_fin",
        "am_modif_amgt",
        "am_lot_max_shob",
        "mod_desc",
        "tr_total",
        "tr_partiel",
        "tr_desc",
        "avap_co_clot",
        "avap_aut_coup_aba_arb",
        "avap_ouv_infra",
        "avap_aut_inst_mob",
        "avap_aut_plant",
        "avap_aut_auv_elec",
        "tax_dest_loc_tr",
        );

// liste des tableaux de surfaces
$tab_surface = array("" => _("Ne pas afficher cet element"), 13409 => _("Tableau du cerfa numero 13409*02"));
$tab_surface2 = array("" => _("Ne pas afficher cet element"), 1340905 => _("Tableau du cerfa numero 13409*05"));
$tab_tax_su_princ = array("" => _("Ne pas afficher cet element"), 13409 => _("Tableau du cerfa numero 13409*02"), 1340903 => _("Tableau du cerfa numero 13409*03"));
$tab_tax_su_heber = array("" => _("Ne pas afficher cet element"), 13409 => _("Tableau du cerfa numero 13409*02"), 1340903 => _("Tableau du cerfa numero 13409*03"));
$tab_tax_su_secon = array("" => _("Ne pas afficher cet element"), 13409 => _("Tableau du cerfa numero 13409*02"), 1340903 => _("Tableau du cerfa numero 13409*03"));
$tab_tax_su_tot = array("" => _("Ne pas afficher cet element"), 13409 => _("Tableau du cerfa numero 13409*02"), 1340903 => _("Tableau du cerfa numero 13409*03"));
$tab_tax_su_non_habit_surf = array("" => _("Ne pas afficher cet element"), 13409 => _("Tableau du cerfa numero 13409*02"), 1340903 => _("Tableau du cerfa numero 13409*03"));
$tab_tax_su_parc_statio_expl_comm = array("" => _("Ne pas afficher cet element"), 1340903 => _("Tableau du cerfa numero 13409*03"));
$tab_tax_am = array("" => _("Ne pas afficher cet element"), 13409 => _("Tableau du cerfa numero 13409*02"));
$tab_erp_eff = array("" => _("Ne pas afficher cet element"), 1382403 => _("Tableau du cerfa numero 13824*03"));
?>