<?php
/**
 * Surcharge de dossier_qualifier pour le profil qualificateur
 * pour afficher tous les dossiers à qualifier
 */

//
include('../sql/pgsql/dossier_qualifier.inc.php');

// Titre de la page
$ent = _("qualification")." -> "._("dossiers a qualifier");

//
$selection = "  WHERE
        groupe.code != 'CTX'
        AND dossier.a_qualifier IS TRUE";

// Surcharge du bouton consulter pour que le bouton retour retourne à cette page
$tab_actions['left']["consulter"] = 
    array('lien' => '../scr/form.php?obj=dossier_instruction&amp;action=3'.'&amp;idx=',
              'id' => '&amp;retourformulaire=dossier_qualifier_qualificateur',
              'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'._('Consulter').'</span>',
              'rights' => array('list' => array('dossier_instruction', 'dossier_instruction_consulter'), 'operator' => 'OR'),
              'ordre' => 10,
              'ajax' => false);
              
$tab_actions['content'] = $tab_actions['left']["consulter"] ;

?>
