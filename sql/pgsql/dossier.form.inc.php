<?php
//$Id: dossier.form.inc.php 5560 2015-12-07 18:56:55Z nmeucci $ 
//gen openMairie le 10/02/2011 20:39 
include('../gen/sql/pgsql/dossier.form.inc.php');

//
$tableSelect = DB_PREFIXE."dossier
LEFT JOIN ".DB_PREFIXE."avis_decision
    ON avis_decision.avis_decision=dossier.avis_decision
LEFT JOIN ".DB_PREFIXE."dossier_autorisation
    ON dossier.dossier_autorisation=dossier_autorisation.dossier_autorisation
LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
    ON dossier_autorisation.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
LEFT JOIN (
        SELECT * 
        FROM ".DB_PREFIXE."lien_dossier_demandeur
        INNER JOIN ".DB_PREFIXE."demandeur
            ON demandeur.demandeur = lien_dossier_demandeur.demandeur
        WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
        AND LOWER(demandeur.type_demandeur) = LOWER('petitionnaire')
    ) as petitionnaire_principal
    ON petitionnaire_principal.dossier = dossier.dossier
LEFT JOIN (
        SELECT * 
        FROM ".DB_PREFIXE."lien_dossier_demandeur
        INNER JOIN ".DB_PREFIXE."demandeur
            ON demandeur.demandeur = lien_dossier_demandeur.demandeur
        WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
        AND LOWER(demandeur.type_demandeur) = LOWER('requerant')
    ) as requerant_principal
    ON requerant_principal.dossier = dossier.dossier
LEFT JOIN (
        SELECT * 
        FROM ".DB_PREFIXE."lien_dossier_demandeur
        INNER JOIN ".DB_PREFIXE."demandeur
            ON demandeur.demandeur = lien_dossier_demandeur.demandeur
        WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
        AND LOWER(demandeur.type_demandeur) = LOWER('contrevenant')
    ) as contrevenant_principal
    ON contrevenant_principal.dossier = dossier.dossier
LEFT JOIN ".DB_PREFIXE."donnees_techniques
    ON donnees_techniques.dossier_instruction = dossier.dossier
LEFT JOIN ".DB_PREFIXE."etat
    ON dossier.etat = etat.etat
LEFT JOIN ".DB_PREFIXE."arrondissement
    ON dossier.terrain_adresse_code_postal = arrondissement.code_postal";
//
$champs = array(
    "dossier.dossier",
    "dossier.om_collectivite AS om_collectivite",
    "dossier.dossier_libelle",
    "dossier.dossier_instruction_type",
    "dossier_autorisation_type_detaille.libelle as dossier_autorisation_type_detaille",
    "autorisation_contestee",
    "CASE WHEN requerant_principal.qualite='particulier' THEN
        TRIM(CONCAT(requerant_principal.particulier_nom, ' ', requerant_principal.particulier_prenom))
    ELSE
        TRIM(CONCAT(requerant_principal.personne_morale_raison_sociale, ' ', requerant_principal.personne_morale_denomination))
    END as requerants",
    "CASE WHEN petitionnaire_principal.qualite='particulier' THEN
        TRIM(CONCAT(petitionnaire_principal.particulier_nom, ' ', petitionnaire_principal.particulier_prenom))
    ELSE
        TRIM(CONCAT(petitionnaire_principal.personne_morale_raison_sociale, ' ', petitionnaire_principal.personne_morale_denomination))
    END as dossier_petitionnaire",
    "'' as dossier_petitionnaires",
    "CASE WHEN contrevenant_principal.qualite='particulier' THEN
        TRIM(CONCAT(contrevenant_principal.particulier_nom, ' ', contrevenant_principal.particulier_prenom))
    ELSE
        TRIM(CONCAT(contrevenant_principal.personne_morale_raison_sociale, ' ', contrevenant_principal.personne_morale_denomination))
    END as contrevenants",
    "concat(replace(dossier.terrain_references_cadastrales,';',' '),'<br/>',
     dossier.terrain_adresse_voie,' ',
     dossier.terrain_adresse_code_postal) as terrain",
    "arrondissement.libelle as dossier_arrondissement",
    "CONCAT_WS(
        '<br/>',
        CASE WHEN co_projet_desc = '' THEN
            NULL
        ELSE
            TRIM(co_projet_desc)
        END,
        CASE WHEN ope_proj_desc = '' THEN
            NULL
        ELSE
            TRIM(ope_proj_desc)
        END,
        CASE WHEN am_projet_desc = '' THEN
            NULL
        ELSE
            TRIM(am_projet_desc)
        END,
        CASE WHEN dm_projet_desc = '' THEN
            NULL
        ELSE
            TRIM(dm_projet_desc)
        END,
        CASE WHEN donnees_techniques.erp_cstr_neuve IS TRUE
            THEN '".str_replace("'", "''", _('erp_cstr_neuve'))."' END,
        CASE WHEN donnees_techniques.erp_trvx_acc IS TRUE
            THEN '".str_replace("'", "''", _('erp_trvx_acc'))."' END,
        CASE WHEN donnees_techniques.erp_extension IS TRUE
            THEN '".str_replace("'", "''", _('erp_extension'))."' END,
        CASE WHEN donnees_techniques.erp_rehab IS TRUE
            THEN '".str_replace("'", "''", _('erp_rehab'))."' END,
        CASE WHEN donnees_techniques.erp_trvx_am IS TRUE
            THEN '".str_replace("'", "''", _('erp_trvx_am'))."' END,
        CASE WHEN donnees_techniques.erp_vol_nouv_exist IS TRUE
            THEN '".str_replace("'", "''", _('erp_vol_nouv_exist'))."' END
    ) as \"description_projet\"",
    "donnees_techniques.ctx_synthese_nti as dt_ctx_synthese_nti",
    "donnees_techniques.ctx_synthese_anr as dt_ctx_synthese_anr ",
    "donnees_techniques.ctx_infraction as dt_ctx_infraction ",
    "donnees_techniques.ctx_regularisable as dt_ctx_regularisable ",
    "dossier_autorisation.dossier_autorisation",
    "dossier_autorisation.dossier_autorisation_libelle",
    "annee",
    "autorite_competente",
    "dossier.instructeur",
    "instructeur_2",
    "dossier.division",
    "public.ST_AsText(dossier.geom::geometry) as geom",
    // Enjeu
    "enjeu_urba",
    "enjeu_erp",
    "'' as enjeu_ctx",
    // Qualification
    "erp",
    "a_qualifier",
    // Archive
    "numero_versement_archive",
    "date_demande",
    // Fieldset instruction
    // Suivi
    // Col 1
    "dossier.date_depot",
    "date_premiere_visite",
    "date_derniere_visite",
    "date_contradictoire",
    "date_retour_contradictoire",
    "date_ait",
    "date_transmission_parquet",
    "donnees_techniques.ctx_date_audience as date_audience",
    "delai",
    "delai_incompletude",
    "date_dernier_depot",
    "date_limite",
    "date_complet",
    "date_limite_incompletude",
    "date_cloture_instruction",
    // Col 2
    "etat.libelle as etat",
    "evenement_suivant_tacite",
    "evenement_suivant_tacite_incompletude",
    // Bloc 2 fieldsets
    // Col 1 : Fieldset "Décision"
    "dossier.date_decision",
    "dossier.avis_decision",
    // Col 2 : Fieldset "Validité de l'autorisation"
    "dossier.date_validite",
    // Autre
    // Col 1    
    "accord_tacite",    
    // Col 2
    "date_rejet",
    "date_notification_delai",
    "dossier.date_chantier",
    "dossier.date_achevement",
    // Col 3    
    "date_conformite",
    // Fieldset "Simulation des taxes"
    "tax_secteur",
    "tax_mtn_part_commu",
    "tax_mtn_part_commu_sans_exo",
    "tax_mtn_part_depart",
    "tax_mtn_part_depart_sans_exo",
    "tax_mtn_part_reg",
    "tax_mtn_part_reg_sans_exo",
    "tax_mtn_total",
    "tax_mtn_total_sans_exo",
    "tax_mtn_rap",
    "tax_mtn_rap_sans_exo",
    // Fieldset "Localisation du terrain"
    // Col 1
    "dossier.terrain_adresse_voie_numero",
    "dossier.terrain_adresse_lieu_dit",
    "dossier.terrain_adresse_code_postal",
    "dossier.terrain_adresse_cedex",
    "dossier.terrain_references_cadastrales",
    // Col 2
    "dossier.terrain_adresse_voie",
    "dossier.terrain_adresse_bp",
    "dossier.terrain_adresse_localite",
    "dossier.terrain_superficie",
    //
    "geom1",
    "dossier.description",
    "version",
    "incompletude",
    "incomplet_notifie",
    "etat_pendant_incompletude",
    "dossier.duree_validite",
    "quartier",
    "dossier.log_instructions",
    "interface_referentiel_erp",
);
//
$selection =" ";
//
$sql_avis_decision = "select avis_decision,libelle from ".DB_PREFIXE."avis_decision order by libelle";
$sql_avis_decision_by_id = "SELECT avis_decision.avis_decision, libelle FROM ".DB_PREFIXE."avis_decision WHERE avis_decision = '<idx>'";

$sql_dossier_autorisation="SELECT dossier_autorisation.dossier_autorisation, dossier_autorisation.dossier_autorisation FROM ".DB_PREFIXE."dossier_autorisation ORDER BY dossier_autorisation.dossier_autorisation ASC";
$sql_dossier_autorisation_by_id = "SELECT dossier_autorisation.dossier_autorisation, dossier_autorisation.dossier_autorisation FROM ".DB_PREFIXE."dossier_autorisation WHERE dossier_autorisation = '<idx>'";

// Requêtes pour la recherche de dossiers existant
$sql_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement ORDER BY arrondissement.libelle ASC";
$sql_arrondissement_by_id = "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE arrondissement = <idx>";

$sql_autorisation_contestee="SELECT dossier.dossier, dossier.dossier_libelle FROM ".DB_PREFIXE."dossier ORDER BY dossier.annee ASC";
$sql_autorisation_contestee_by_id = "SELECT dossier.dossier, dossier.dossier_libelle FROM ".DB_PREFIXE."dossier WHERE dossier = '<idx>'";

$sql_dossier_autorisation_type_detaille="SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.code 
  FROM ".DB_PREFIXE."dossier_autorisation_type_detaille
  ORDER BY dossier_autorisation_type_detaille.libelle";
$sql_dossier_autorisation_type_detaille_by_id = "SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.code FROM ".DB_PREFIXE."dossier_autorisation_type_detaille WHERE dossier_autorisation_type_detaille = <idx>";


$sql_demandeur="SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle 
  FROM ".DB_PREFIXE."dossier_autorisation_type_detaille
  ORDER BY dossier_autorisation_type_detaille.libelle";
$sql_demandeur_by_id = "SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle FROM ".DB_PREFIXE."dossier_autorisation_type_detaille WHERE dossier_autorisation_type_detaille = <idx>";


// Requetes a utiliser lorsque l'option "option_afficher_division" est active
$sql_instructeur_div="SELECT instructeur.instructeur, instructeur.nom||' ('||division.code||')' 
FROM ".DB_PREFIXE."instructeur 
INNER JOIN ".DB_PREFIXE."division ON division.division=instructeur.division
INNER JOIN ".DB_PREFIXE."instructeur_qualite ON instructeur_qualite.instructeur_qualite=instructeur.instructeur_qualite
WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)))
<instructeur_qualite>
ORDER BY nom";
$sql_instructeur_div_by_id = "SELECT instructeur.instructeur, instructeur.nom||' ('||division.code||')' 
FROM ".DB_PREFIXE."instructeur 
INNER JOIN ".DB_PREFIXE."division ON division.division=instructeur.division 
INNER JOIN ".DB_PREFIXE."instructeur_qualite ON instructeur_qualite.instructeur_qualite=instructeur.instructeur_qualite
WHERE instructeur = <idx>";

$sql_instructeur_div_by_di = "SELECT instructeur.instructeur, instructeur.nom||' ('||division.code||')' 
FROM ".DB_PREFIXE."instructeur 
INNER JOIN ".DB_PREFIXE."division ON division.division=instructeur.division
INNER JOIN ".DB_PREFIXE."instructeur_qualite ON instructeur_qualite.instructeur_qualite=instructeur.instructeur_qualite
LEFT JOIN ".DB_PREFIXE."direction
    ON division.direction = direction.direction
LEFT JOIN ".DB_PREFIXE."om_collectivite
    ON direction.om_collectivite = om_collectivite.om_collectivite
WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)))
AND (om_collectivite.niveau = '2' OR direction.om_collectivite = '<collectivite_di>')
<instructeur_qualite>
ORDER BY nom";
$sql_instructeur_by_di = "SELECT instructeur.instructeur, instructeur.nom 
FROM ".DB_PREFIXE."instructeur 
INNER JOIN ".DB_PREFIXE."instructeur_qualite ON instructeur_qualite.instructeur_qualite=instructeur.instructeur_qualite
LEFT JOIN ".DB_PREFIXE."division
    ON instructeur.division = division.division
LEFT JOIN ".DB_PREFIXE."direction
    ON division.direction = direction.direction
LEFT JOIN ".DB_PREFIXE."om_collectivite
    ON direction.om_collectivite = om_collectivite.om_collectivite
WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE))) 
AND (om_collectivite.niveau = '2' OR direction.om_collectivite = '<collectivite_di>')
<instructeur_qualite>
ORDER BY instructeur.nom ASC";



// Requetes a utiliser lorsque l'option "option_afficher_division" est active
$sql_instructeur_2_div="SELECT instructeur.instructeur, instructeur.nom||' ('||division.code||')' 
FROM ".DB_PREFIXE."instructeur 
INNER JOIN ".DB_PREFIXE."division ON division.division=instructeur.division
INNER JOIN ".DB_PREFIXE."instructeur_qualite ON instructeur_qualite.instructeur_qualite=instructeur.instructeur_qualite
WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)))
AND instructeur_qualite.code = 'tech'
ORDER BY nom";
$sql_instructeur_2_div_by_id = $sql_instructeur_div_by_id;

$sql_instructeur_2_div_by_di = "SELECT instructeur.instructeur, instructeur.nom||' ('||division.code||')' 
FROM ".DB_PREFIXE."instructeur 
INNER JOIN ".DB_PREFIXE."division ON division.division=instructeur.division
INNER JOIN ".DB_PREFIXE."instructeur_qualite ON instructeur_qualite.instructeur_qualite=instructeur.instructeur_qualite
LEFT JOIN ".DB_PREFIXE."direction
    ON division.direction = direction.direction
LEFT JOIN ".DB_PREFIXE."om_collectivite
    ON direction.om_collectivite = om_collectivite.om_collectivite
WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)))
AND (om_collectivite.niveau = '2' OR direction.om_collectivite = '<collectivite_di>')
AND instructeur_qualite.code = 'tech'
ORDER BY nom";
$sql_instructeur_2_by_di = "SELECT instructeur.instructeur, instructeur.nom 
FROM ".DB_PREFIXE."instructeur 
INNER JOIN ".DB_PREFIXE."instructeur_qualite ON instructeur_qualite.instructeur_qualite=instructeur.instructeur_qualite
LEFT JOIN ".DB_PREFIXE."division
    ON instructeur.division = division.division
LEFT JOIN ".DB_PREFIXE."direction
    ON division.direction = direction.direction
LEFT JOIN ".DB_PREFIXE."om_collectivite
    ON direction.om_collectivite = om_collectivite.om_collectivite
WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE))) 
AND (om_collectivite.niveau = '2' OR direction.om_collectivite = '<collectivite_di>')
AND instructeur_qualite.code = 'tech'
ORDER BY instructeur.nom ASC";

//
$sql_division_by_di="SELECT division.division, division.libelle
FROM ".DB_PREFIXE."division 
LEFT JOIN ".DB_PREFIXE."direction
    ON division.direction = direction.direction
LEFT JOIN ".DB_PREFIXE."om_collectivite
    ON direction.om_collectivite = om_collectivite.om_collectivite
WHERE ((division.om_validite_debut IS NULL AND (division.om_validite_fin IS NULL OR division.om_validite_fin > CURRENT_DATE)) OR (division.om_validite_debut <= CURRENT_DATE AND (division.om_validite_fin IS NULL OR division.om_validite_fin > CURRENT_DATE))) 
AND (om_collectivite.niveau = '2' OR direction.om_collectivite = '<collectivite_di>')
ORDER BY division.libelle ASC";
?>
