<?php
/**
 * @package openads
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_contentieux.inc.php";

// Fil d'Ariane
$ent = _("contentieux")." -> "._("recours");

// Les onglets
$sousformulaire=array();
$sousformulaire[] = "dossier_contrainte_contexte_ctx";
$sousformulaire[] = "instruction_contexte_ctx_re";
$sousformulaire[] = "dossier_message_contexte_ctx";
$sousformulaire[] = "blocnote_contexte_ctx";
$sousformulaire[] = "document_numerise_contexte_ctx";
$sousformulaire[] = "lien_dossier_dossier_contexte_ctx_re";

$retourformulaire = 'dossier_contentieux_mes_recours';

/*Ajout de paramètre à certains sous-formulaire*/
$sousformulaire_parameters = array(
    "instruction_contexte_ctx_re" => array(
        "title" => _("Instruction"),
    ),
    "dossier_message_contexte_ctx" => array(
        "title" => _("Message(s)"),
    ),
    "blocnote_contexte_ctx" => array(
        "title" => _("Bloc-note"),
    ),
    "lien_dossier_dossier_contexte_ctx_re" => array(
        "title" => _("Dossiers liés"),
        "href" => "../scr/sousform.php?obj=lien_dossier_dossier_contexte_ctx_re".
            "&action=4&idx=0&idxformulaire=".((isset($idx))? $idx:"").
            "&retourformulaire=".$retourformulaire."&contentonly=true&",
    ),
);

// On modifie le lien du paramètre dossier_contrainte_contexte_ctx
$sousformulaire_parameters["dossier_contrainte_contexte_ctx"] = array(
    "title" => _("Contrainte(s)"),
    "href" => "../scr/form.php?obj=dossier&action=4&idx=".((isset($idx))?$idx:"")."&retourformulaire=".((isset($_GET['obj']))?$_GET['obj']:"")."&",
);

// On modifie le lien du paramètre document_numerise_contexte_ctx
$sousformulaire_parameters["document_numerise_contexte_ctx"] = array(
    "title" => _("Piece(s)"),
    "href" => "../scr/form.php?obj=dossier&action=5&idx=".((isset($idx))?$idx:"")."&retourformulaire=".((isset($_GET['obj']))?$_GET['obj']:"")."&",
);

// Jointures
$table = $table_rec;

// Affiche seulement les recours où l'utilisateur connecté est affecté
$selection = $selection_re;

// Colonne affichées sur le tableau
$champAffiche = $champs_affiche_rec;

// Recherche simple
$champRecherche = $champs_recherche_rec;

// Gestion des groupes et confidentialité
include('../sql/pgsql/filter_group.inc.php');

?>
