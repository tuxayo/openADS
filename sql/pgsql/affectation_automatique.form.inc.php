<?php
//$Id: affectation_automatique.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 30/11/2012 12:37

include('../gen/sql/pgsql/affectation_automatique.form.inc.php');
$sql_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle
FROM ".DB_PREFIXE."arrondissement ORDER BY NULLIF(arrondissement.libelle,'')::int ASC NULLS LAST";

$champs = array(
    'affectation_automatique',
    'om_collectivite',
    'dossier_autorisation_type_detaille',
    'instructeur',
    'instructeur_2',
    'arrondissement',
    'quartier',
    'section',
);

// Requête pour récupérer le nom de l'instructeur et sa division
$sql_instructeur = "SELECT instructeur.instructeur, CONCAT(instructeur.nom, ' (', division.code, ')')
FROM " . DB_PREFIXE . "instructeur
    INNER JOIN " . DB_PREFIXE . "division ON division.division=instructeur.division
WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL
        OR instructeur.om_validite_fin > CURRENT_DATE))
    OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL
        OR instructeur.om_validite_fin > CURRENT_DATE)))
ORDER BY nom";
//
$sql_instructeur_by_id = "SELECT instructeur.instructeur, CONCAT(instructeur.nom, ' (', division.code, ')')
FROM " . DB_PREFIXE . "instructeur
    INNER JOIN " . DB_PREFIXE . "division
        ON division.division=instructeur.division
    INNER JOIN " . DB_PREFIXE . "instructeur_qualite
        ON instructeur_qualite.instructeur_qualite=instructeur.instructeur_qualite
WHERE instructeur = <idx>";

// Requête pour récupérer le nom de l'instructeur 2 et sa division
$sql_instructeur_2 = $sql_instructeur;
//
$sql_instructeur_2_by_id = $sql_instructeur_by_id;

?>
