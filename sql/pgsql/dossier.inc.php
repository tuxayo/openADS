<?php
//$Id: dossier.inc.php 5232 2015-09-30 10:41:34Z stimezouaght $ 
//gen openMairie le 10/02/2011 20:39 
include('../gen/sql/pgsql/dossier.inc.php');

/*Tables sur lesquels la requête va s'effectuer*/
$table = DB_PREFIXE."dossier
LEFT JOIN ".DB_PREFIXE."lien_dossier_demandeur 
        ON lien_dossier_demandeur.dossier=dossier.dossier
            AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
LEFT JOIN ".DB_PREFIXE."demandeur
    ON lien_dossier_demandeur.demandeur=demandeur.demandeur
LEFT JOIN ".DB_PREFIXE."instructeur
    ON dossier.instructeur = instructeur.instructeur
LEFT JOIN ".DB_PREFIXE."om_utilisateur
    ON instructeur.om_utilisateur = om_utilisateur.om_utilisateur
LEFT JOIN ".DB_PREFIXE."avis_decision
    ON avis_decision.avis_decision=dossier.avis_decision
LEFT JOIN ".DB_PREFIXE."division
    ON dossier.division=division.division";

/*Champs du début de la requête*/
$champAffiche=array(
    'dossier.dossier as "'._("dossier").'"',
    'TRIM(CONCAT(personne_morale_denomination,\' \',personne_morale_nom,\' \',demandeur.particulier_nom)) as "'._("petitionaire").'"',
    'instructeur.nom as "'._("instructeur").'"',
    'to_char(dossier.date_demande ,\'DD/MM/YYYY\') as "'._("date_demande").'"',
    'dossier.date_dernier_depot as "'._("date_dernier_depot").'"',
    'to_char(dossier.date_complet ,\'DD/MM/YYYY\') as "'._("date_complet").'"',
    'to_char(dossier.date_notification_delai ,\'DD/MM/YYYY\') as "'._("date_notification_delai").'"',
    'to_char(dossier.date_limite ,\'DD/MM/YYYY\') as "'._("date_limite").'"',
    'etat as "'._("etat").'"',
    'avis_decision.libelle as "'._("avis_decision").'"',
    'CASE WHEN dossier.enjeu_erp is TRUE THEN \'<span class="om-icon om-icon-16 om-icon-fix enjeu_erp-16" title="'._('Enjeu ERP').'">ERP</span>\' ELSE \'\' END ||
     CASE WHEN dossier.enjeu_urba is TRUE THEN \'<span class="om-icon om-icon-16 om-icon-fix enjeu_urba-16" title="'._('Enjeu Urba').'">URBA</span>\' ELSE \'\' END
     as "'._("enjeu").'"',
);

/*Tri*/
$triOrder= "order by dossier.dossier";
$tri = $triOrder;

/*Recherche simple*/
$champRecherche=array(    'dossier.dossier as "'._("dossier").'"',
    'personne_morale_denomination as "'._("personne_morale_denomination").'"',
    'particulier_nom as "'._("particulier_nom").'"',
    );

/*Icone*/
$ico = "../img/ico_dossier.png";

$edition="";

/**
 * OPTIONS
 */
//
if (!isset($options)) {
    $options = array();
}

/*Si l'on se trouve dans le formulaire dossier_instruction*/
if (isset($_GET["obj"]) && $_GET["obj"] == "dossier_instruction") {
    /**
     * OPTIONS - ADVSEARCH
     */
    // Options pour les select de faux booléens
    $args = array(
        0 => array("", "Oui", "Non", ),
        1 => array(_("choisir")." "._("accord_tacite"), _("Oui"), _("Non"), ),
    );
    //
    $champs['dossier'] = array(
        'libelle' => _('dossier'),
        'type' => 'text',
        'table' => 'dossier',
        'colonne' => array(
            'dossier', 
            'dossier_libelle',
        ),
        'taille' => 30,
    );
    // On ignore les DATD contentieux
    $champs['dossier_autorisation_type_detaille'] = array(
        'table' => 'dossier_autorisation_type_detaille',
        'colonne' => 'dossier_autorisation_type_detaille',
        'type' => 'select',
        'taille' => 30,
        'libelle' => _('type'),
        'subtype' => 'sqlselect',
        'sql' => "SELECT dossier_autorisation_type_detaille.dossier_autorisation_type_detaille, dossier_autorisation_type_detaille.libelle
            FROM ".DB_PREFIXE."dossier_autorisation_type_detaille 
            WHERE (LOWER(dossier_autorisation_type_detaille.code) != LOWER('REC')
                AND LOWER(dossier_autorisation_type_detaille.code) != LOWER('REG')
                AND LOWER(dossier_autorisation_type_detaille.code) != LOWER('IN'))
            ORDER BY code",
    );
    //
    $champs['particulier'] = array(
        'libelle' => _('Demandeur'),
        'help' => _("Recherche dans les champs : nom, prénom, raison sociale, dénomination. 

La chaîne recherchée doit figurer dans l'un de ces champs.

Par exemple, dans le cas d'un demandeur avec le nom 'DUPONT' et le prénom 'JEAN' :
- la recherche de 'JEAN' donne des résultats car le champ prénom contient 'JEAN',
- la recherche de 'DUPONT' donne des résultats car le champ nom contient 'DUPONT',
- la recherche de 'DUPONT JEAN' ne donne aucun résultat car ni le nom ni le prénom ni la raison sociale ni la dénomination ne contient 'DUPONT JEAN'."),
        'type' => 'text',
        'table' => 'demandeur',
        'colonne' => array(
            'particulier_nom',
            'particulier_prenom',
            'personne_morale_raison_sociale',
            'personne_morale_denomination',
        ),
        'taille' => 30,
    );
    //
    $champs['parcelle'] = array(
        'table' => 'dossier_parcelle',
        'where' => 'injoin',
        'tablejoin' => 'INNER JOIN (SELECT DISTINCT dossier FROM '.DB_PREFIXE.'dossier_parcelle WHERE lower(dossier_parcelle.libelle) like %s ) AS A1 ON A1.dossier = dossier.dossier' ,
        'colonne' => 'libelle',
        'type' => 'text',
        'taille' => 30,
        'libelle' => _('parcelle'),
    );
    //
    $champs['etat'] = array(
        'table' => 'dossier',
        'colonne' => 'etat',
        'type' => 'select',
        'libelle' => _('etat'),
    );
    //
    $champs['accord_tacite'] = array(
        'table' => 'dossier',
        'colonne' => 'accord_tacite',
        'type' => 'select',
        "subtype" => "manualselect",
        'libelle' => _('accord_tacite'),
        "args" => $args,
    );
    //
    $champs['instructeur'] = array(
        'table' => 'dossier',
        'colonne' => 'instructeur',
        'type' => 'select',
        'libelle' => _('instructeur'),
    );
    //
    if ($_SESSION['niveau'] == '2') {
        $champs['instructeur']['subtype'] = 'sqlselect';
        $champs['instructeur']['sql'] = "SELECT instructeur.instructeur, instructeur.nom||' ('||division.code||')' 
FROM ".DB_PREFIXE."instructeur 
INNER JOIN ".DB_PREFIXE."division ON division.division=instructeur.division
WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)))
ORDER BY nom";
    }
    //
    $champs['date_depot'] = array(
        'colonne' => 'date_depot',
        'table' => 'dossier',
        'libelle' => _('date_depot'),
        'lib1'=> _("debut"),
        'lib2' => _("fin"),
        'type' => 'date',
        'taille' => 8,
        'where' => 'intervaldate',
    );
    //
    $champs['date_rejet'] = array(
        'colonne' => 'date_rejet',
        'table' => 'dossier',
        'libelle' => _('date_rejet'),
        'lib1'=> _("debut"),
        'lib2' => _("fin"),
        'type' => 'date',
        'taille' => 8,
        'where' => 'intervaldate',
    );
    //
    $champs['date_validite'] = array(
        'colonne' => 'date_validite',
        'table' => 'dossier',
        'libelle' => _('date_validite'),
        'lib1'=> _("debut"),
        'lib2' => _("fin"),
        'type' => 'date',
        'taille' => 8,
        'where' => 'intervaldate',
    );
    //
    $champs['date_complet'] = array(
        'colonne' => 'date_complet',
        'table' => 'dossier',
        'libelle' => _('date_complet'),
        'lib1'=> _("debut"),
        'lib2' => _("fin"),
        'type' => 'date',
        'taille' => 8,
        'where' => 'intervaldate',
    );
    //
    $champs['date_decision'] = array(
        'colonne' => 'date_decision',
        'table' => 'dossier',
        'libelle' => _('date_decision'),
        'lib1'=> _("debut"),
        'lib2' => _("fin"),
        'type' => 'date',
        'taille' => 8,
        'where' => 'intervaldate',
    );
    //
    $champs['date_limite'] = array(
        'colonne' => 'date_limite',
        'table' => 'dossier',
        'libelle' => _('date_limite'),
        'lib1'=> _("debut"),
        'lib2' => _("fin"),
        'type' => 'date',
        'taille' => 8,
        'where' => 'intervaldate',
    );
    //
    $champs['date_chantier'] = array(
        'colonne' => 'date_chantier',
        'table' => 'dossier',
        'libelle' => _('date_chantier'),
        'lib1'=> _("debut"),
        'lib2' => _("fin"),
        'type' => 'date',
        'taille' => 8,
        'where' => 'intervaldate',
    );
    //
    $champs['date_achevement'] = array(
        'colonne' => 'date_achevement',
        'table' => 'dossier',
        'libelle' => _('date_achevement'),
        'lib1'=> _("debut"),
        'lib2' => _("fin"),
        'type' => 'date',
        'taille' => 8,
        'where' => 'intervaldate',
    );
    //
    $champs['date_conformite'] = array(
        'colonne' => 'date_conformite',
        'table' => 'dossier',
        'libelle' => _('date_conformite'),
        'lib1'=> _("debut"),
        'lib2' => _("fin"),
        'type' => 'date',
        'taille' => 8,
        'where' => 'intervaldate',
    );
    //
    if ($_SESSION['niveau'] == '2') {
        $champs['om_collectivite'] = array(
            'table' => 'dossier',
            'colonne' => 'om_collectivite',
            'type' => 'select',
            'libelle' => _('om_collectivite')
        );
    }
    // advsearch -> options
    $options[] = array(
        'type' => 'search',
        'display' => true,
        'advanced'  => $champs,
        'default_form'  => 'advanced',
        'absolute_object' => 'dossier',
        'export' => array("csv"),
    );

    /**
     * OPTIONS
     */
    //
    $options[] = array(
        'type' => 'pagination_select',
        'display' => ''
    );
}

$retourformulaire = (isset($_GET['retourformulaire']) ? $_GET['retourformulaire'] : "");

// On change l'ordre d'affichage des onglets
$sousformulaire=array();
$sousformulaire[] = "dossier_contrainte";
$sousformulaire[] = "instruction";
// Ajout d'une permission en plus pour afficher cette onglet afin de faciliter
// l'utilisation de son contenu dans le contexte des demandes d'avis
if ($f->isAccredited(array("consultation", "consultation_tab_di"), "OR")) {
    //
    $sousformulaire[] = "consultation";
}
$sousformulaire[] = "dossier_commission";
$sousformulaire[] = "lot";
$sousformulaire[] = "dossier_message";
$sousformulaire[] = "blocnote";
//
if ($f->isAccredited("document_numerise") || $f->isAccredited(array("dossier", "dossier_document_numerise"), "OR")) {
    $sousformulaire[] = "document_numerise";
}
$sousformulaire[] = "lien_dossier_dossier";

/*Ajout de paramètre à certains sous-formulaire*/
$sousformulaire_parameters = array(
    "consultation" => array(
        "title" => _("consultation(s)"),
    ),
    "dossier_message" => array(
        "title" => _("message(s)"),
    ),
    "dossier_commission" => array(
        "title" => _("commission(s)"),
    ),
    "lot" => array(
        "title" => _("lot(s)"),
    ),
    "lien_dossier_dossier" => array(
        "title" => _("Dossiers liés"),
        "href" => "../scr/form.php?obj=lien_dossier_dossier&action=4&idx=0&idxformulaire=".((isset($idx))? $idx:"").
            "&retourformulaire=".$retourformulaire."&contentonly=true&",
    ),
);

// On modifie le lien du paramètre dossier_contrainte
$sousformulaire_parameters["dossier_contrainte"] = array(
    "title" => _("Contrainte(s)"),
    "href" => "../scr/form.php?obj=dossier&action=4&idx=".((isset($idx))?$idx:"")."&retourformulaire=".((isset($_GET['obj']))?$_GET['obj']:"")."&",
);

//
$sousformulaire_parameters["document_numerise"] = null;
//
if ($f->isAccredited("document_numerise") || $f->isAccredited(array("dossier", "dossier_document_numerise"), "OR")) {
    //
    $sousformulaire_parameters["document_numerise"] = array(
        "title" => _("Piece(s)"),
        "href" => "../scr/form.php?obj=dossier&action=5&idx=".((isset($idx))?$idx:"")."&retourformulaire=".((isset($_GET['obj']))?$_GET['obj']:"")."&",
    );
}

?>
