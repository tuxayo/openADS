<?php
//$Id: instruction.form.inc.php 5188 2015-09-23 12:55:01Z nhaye $ 
//gen openMairie le 10/02/2011 20:34 
include('../gen/sql/pgsql/instruction.form.inc.php');
$champs=array("instruction",
              "destinataire",
              "instruction.evenement",
              "date_evenement",
              "instruction.lettretype",
              "om_final_instruction_utilisateur",
              "date_finalisation_courrier",
              "date_envoi_signature",
              "date_envoi_rar",
              "date_envoi_controle_legalite",
              "signataire_arrete",
              
              "date_retour_signature",
              "date_retour_rar",
              "date_retour_controle_legalite",

              "numero_arrete",
              
              "complement_om_html",
              "'' as bible_auto",
              "'' as bible",
              "complement2_om_html",
              "'' as bible2",
              "complement3_om_html",
              "'' as bible3",
              "complement4_om_html",
              "'' as bible4",
              "complement5_om_html",
              "'' as bible5",
              "complement6_om_html",
              "'' as bible6",
              "complement7_om_html",
              "'' as bible7",
              "complement8_om_html",
              "'' as bible8",
              "complement9_om_html",
              "'' as bible9",
              "complement10_om_html",
              "'' as bible10",
              "complement11_om_html",
              "'' as bible11",
              "dossier",
              "instruction.action",
              "instruction.delai",
              "instruction.etat",
              "instruction.autorite_competente",
              "instruction.accord_tacite",
              "instruction.delai_notification",
              "instruction.avis_decision",
              "archive_delai",
              "archive_accord_tacite",
              "archive_etat",
              "archive_avis",
              "archive_date_complet",
              "archive_date_rejet",
              "archive_date_limite",
              "archive_date_notification_delai",
              "archive_date_decision",
              "archive_date_validite",
              "archive_date_achevement",
              "archive_date_conformite",
              "archive_date_chantier",
              "archive_date_dernier_depot",
              "date_depot",
              "complement12_om_html",
              "complement13_om_html",
              "complement14_om_html",
              "complement15_om_html",
              "archive_incompletude",
              "archive_incomplet_notifie",
              "archive_evenement_suivant_tacite",
              "archive_evenement_suivant_tacite_incompletude",
              "archive_etat_pendant_incompletude",
              "archive_date_limite_incompletude",
              "archive_delai_incompletude",
              "archive_autorite_competente",
              "code_barres",
              "om_fichier_instruction",
              "om_final_instruction",
              "document_numerise",
              "duree_validite_parametrage",
              "duree_validite",
              "created_by_commune",
              "archive_date_cloture_instruction",
              "archive_date_premiere_visite",
              "archive_date_derniere_visite",
              "archive_date_contradictoire",
              "archive_date_retour_contradictoire",
              "archive_date_ait",
              "archive_date_transmission_parquet",
              );

// signataire
$sql_signataire_arrete = "SELECT
signataire_arrete.signataire_arrete,
CONCAT(signataire_arrete.prenom, ' ', signataire_arrete.nom)
FROM ".DB_PREFIXE."signataire_arrete
WHERE ((signataire_arrete.om_validite_debut IS NULL AND (signataire_arrete.om_validite_fin IS NULL OR signataire_arrete.om_validite_fin > CURRENT_DATE)) OR (signataire_arrete.om_validite_debut <= CURRENT_DATE AND (signataire_arrete.om_validite_fin IS NULL OR signataire_arrete.om_validite_fin > CURRENT_DATE)))
ORDER BY signataire_arrete.prenom, signataire_arrete.nom";
$sql_signataire_arrete_by_id = "SELECT
signataire_arrete.signataire_arrete,
CONCAT(signataire_arrete.prenom, ' ', signataire_arrete.nom)
FROM ".DB_PREFIXE."signataire_arrete
WHERE signataire_arrete.signataire_arrete = <idx>";
//
$sql_signataire_arrete_by_di = "SELECT
signataire_arrete.signataire_arrete,
CONCAT(signataire_arrete.prenom, ' ', signataire_arrete.nom)
FROM ".DB_PREFIXE."signataire_arrete
LEFT JOIN ".DB_PREFIXE."om_collectivite ON signataire_arrete.om_collectivite = om_collectivite.om_collectivite
WHERE ((signataire_arrete.om_validite_debut IS NULL AND (signataire_arrete.om_validite_fin IS NULL OR signataire_arrete.om_validite_fin > CURRENT_DATE)) OR (signataire_arrete.om_validite_debut <= CURRENT_DATE AND (signataire_arrete.om_validite_fin IS NULL OR signataire_arrete.om_validite_fin > CURRENT_DATE)))
AND (om_collectivite.niveau = '2' OR signataire_arrete.om_collectivite = <collectivite_di>)
ORDER BY signataire_arrete.prenom, signataire_arrete.nom";
//
$sql_signataire_arrete_defaut = "SELECT
signataire_arrete.signataire_arrete,
CONCAT(signataire_arrete.prenom, ' ', signataire_arrete.nom)
FROM ".DB_PREFIXE."signataire_arrete
WHERE ((signataire_arrete.om_validite_debut IS NULL AND (signataire_arrete.om_validite_fin IS NULL OR signataire_arrete.om_validite_fin > CURRENT_DATE)) OR (signataire_arrete.om_validite_debut <= CURRENT_DATE AND (signataire_arrete.om_validite_fin IS NULL OR signataire_arrete.om_validite_fin > CURRENT_DATE))) AND signataire_arrete.defaut IS TRUE
ORDER BY signataire_arrete.prenom, signataire_arrete.nom";
//
$sql_signataire_arrete_defaut_by_di = "SELECT
signataire_arrete.signataire_arrete,
CONCAT(signataire_arrete.prenom, ' ', signataire_arrete.nom)
FROM ".DB_PREFIXE."signataire_arrete
LEFT JOIN ".DB_PREFIXE."om_collectivite ON signataire_arrete.om_collectivite = om_collectivite.om_collectivite
WHERE ((signataire_arrete.om_validite_debut IS NULL AND (signataire_arrete.om_validite_fin IS NULL OR signataire_arrete.om_validite_fin > CURRENT_DATE)) OR (signataire_arrete.om_validite_debut <= CURRENT_DATE AND (signataire_arrete.om_validite_fin IS NULL OR signataire_arrete.om_validite_fin > CURRENT_DATE))) AND signataire_arrete.defaut IS TRUE
AND (om_collectivite.niveau = '2' OR signataire_arrete.om_collectivite = <collectivite_di>)
ORDER BY signataire_arrete.prenom, signataire_arrete.nom";

?>
