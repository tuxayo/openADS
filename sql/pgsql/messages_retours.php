<?php
/**
 * LISTING - Retours de messages
 *
 * Le listing 'Retours de messages' permet d'afficher la même liste de
 * dossiers que le widget 'Retours de messages'. Ce script permet de
 * définir les paramètres génériques du listing qui se trouvent dans la
 * définition du widget obj/om_widget.class.php et a pour objectif d'être
 * inclus par les scripts de définition des listings spécifiques à chaque
 * filtre : instructeur, division, aucun.
 *
 * @package openads
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/dossier_message.inc.php";

/**
 *
 */
switch ($obj) {
    case 'messages_mes_retours':
        $filtre = 'instructeur';
        $contexte = 'standard';
        break;
    case 'messages_retours_ma_division':
        $filtre = 'division';
        $contexte = 'standard';
        break;
    case 'messages_tous_retours':
        $filtre = 'aucun';
        $contexte = 'standard';
        break;
    case 'messages_contentieux_mes_retours':
        $filtre = 'instructeur';
        $contexte = 'contentieux';
        break;
    case 'messages_contentieux_retours_ma_division':
        $filtre = 'division';
        $contexte = 'contentieux';
        break;
    case 'messages_contentieux_tous_retours':
        $filtre = 'aucun';
        $contexte = 'contentieux';
        break;
}

//
$params = array(
    "filtre" => $filtre,
    "contexte" => $contexte,
);

/**
 * Récupération de la configuration de la requête à partir du widget.
 */
//
require_once "../obj/om_widget.class.php";
$om_widget = new om_widget(0);
//
$conf = $om_widget->get_config_messages_retours($params);

/**
 *
 */
//
$tab_description = $conf["message_help"];

//
$tab_title = _("message");

//
$displayed_fields_begin = array(
    'dossier_message.dossier_message as "'._("dossier_message").'"',
    'dossier.dossier_libelle as "'._("dossier").'"',
    'dossier_message.type as "'._("type").'"',
    'dossier_message.emetteur as "'._("emetteur").'"',
    'dossier_message.destinataire as "'._("destinataire").'"',
    'to_char(dossier_message.date_emission ,\'DD/MM/YYYY HH24:MI:SS\') as "'._("date_emission").'"',
);
$displayed_field_instructeur = array(
    'instructeur.nom as "'._("instructeur").'"',
);
// Dans le contexte contentieux, affiche les deux instructeurs
if ($contexte === 'contentieux') {
    //
    $displayed_field_instructeur = array(
        'CONCAT_WS(\' / \', instructeur.nom, instructeur_2.nom) as "'._("juriste / technicien").'"',
    );
}
$displayed_field_division = array(
    'division.code as "'._("division").'"',
);
$displayed_field_collectivite = array(
    'om_collectivite.libelle as "'._("collectivite").'"',
);
$displayed_fields_end = array(
    'CASE WHEN dossier.enjeu_erp is TRUE THEN \'<span class="om-icon om-icon-16 om-icon-fix enjeu_erp-16" title="'._('Enjeu ERP').'">ERP</span>\' ELSE \'\' END ||
     CASE WHEN dossier.enjeu_urba is TRUE THEN \'<span class="om-icon om-icon-16 om-icon-fix enjeu_urba-16" title="'._('Enjeu Urba').'">URBA</span>\' ELSE \'\' END
     as "'._("enjeu").'"',
);

// FROM
$table = $conf["query_ct_from"];

// WHERE
$selection = sprintf(
    "WHERE 
        %s
    ",
    $conf["query_ct_where_common"]
);

//
$tri = " ORDER BY dossier.enjeu_erp DESC, dossier.enjeu_urba DESC, dossier_message.date_emission ASC ";
//
if ($contexte === 'contentieux') {
    //
    $tri = " ORDER BY dossier_message.date_emission ASC ";
}

//
$tab_actions['corner']['ajouter'] = null;

/**
 * Options - ADVSEARCH
 */
//
$advsearch_fields_begin = array(
    //
    'dossier' => array(
        'table' => 'dossier_message',
        'colonne' => 'dossier',
        'type' => 'text',
        'libelle' => _('Dossier'),
    ),
    //
    'type' => array(
        'table' => 'dossier_message',
        'colonne' => 'type',
        'type' => 'select',
        'libelle' => _('Type message'),
    ),
    //
    'emetteur' => array(
        'colonne' => 'emetteur',
        'table' => 'dossier_message',
        'libelle' => _('Emetteur'),
        'type' => 'text',
    ),
    //
    'date_emission' => array(
        'colonne' => 'date_emission',
        'table' => 'dossier_message',
        'libelle' => _('Date d\'emission'),
        'type' => 'date',
        'where' => 'intervaldate',
    ),
);
//
$advsearch_field_instructeur = array(
    //
    'instructeur' => array(
        'colonne' => 'nom',
        'table' => 'instructeur',
        'libelle' => _('Juriste/Technicien'),
        'type' => 'text',
    ),
);
// Dans le contexte contentieux, affiche les deux instructeurs
if ($contexte === 'contentieux') {
    //
    $advsearch_field_instructeur = array(
        'instructeur' => array(
            'table' => 'dossier',
            'colonne' => 'instructeur',
            'type' => 'select',
            'libelle' => _('Juriste'),
            'subtype' => 'sqlselect',
            'sql' => "SELECT instructeur.instructeur, instructeur.nom
                FROM ".DB_PREFIXE."instructeur 
                INNER JOIN ".DB_PREFIXE."instructeur_qualite ON instructeur_qualite.instructeur_qualite=instructeur.instructeur_qualite
                WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)))
                    AND LOWER(instructeur_qualite.code) = LOWER('juri')
                ORDER BY nom",
        ),
        //
        'instructeur_2' => array(
            'table' => 'dossier',
            'colonne' => 'instructeur_2',
            'type' => 'select',
            'libelle' => _('Technicien'),
            'subtype' => 'sqlselect',
            'sql' => "SELECT instructeur.instructeur, instructeur.nom
                FROM ".DB_PREFIXE."instructeur 
                INNER JOIN ".DB_PREFIXE."instructeur_qualite ON instructeur_qualite.instructeur_qualite=instructeur.instructeur_qualite
                WHERE ((instructeur.om_validite_debut IS NULL AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)) OR (instructeur.om_validite_debut <= CURRENT_DATE AND (instructeur.om_validite_fin IS NULL OR instructeur.om_validite_fin > CURRENT_DATE)))
                    AND LOWER(instructeur_qualite.code) = LOWER('tech')
                ORDER BY nom",
        ),
    );
}
//
$advsearch_field_division = array(
    //
    'division' => array(
        'colonne' => 'code',
        'table' => 'division',
        'libelle' => _('Division'),
        'type' => 'text',
    ),
);
//
$advsearch_field_collectivite = array(
    //
    'collectivite' => array(
        'table' => 'om_collectivite',
        'colonne' => 'libelle',
        'type' => 'text',
        'libelle' => _('om_collectivite'),
    ),
);
//
$advsearch_fields_end = array(
    //
    'enjeu_erp' => array(
        'colonne' => 'enjeu_erp',
        'table' => 'dossier',
        'libelle' => _('Dossier enjeu ERP'),
        "subtype" => "manualselect",
        'type' => 'select',
        "args" => array(
            array("", "true", "false", ),
            array(_("Tous"), _("Oui"), _("Non"), ),
        ),
    ),
    //
    'enjeu_urba' => array(
        'colonne' => 'enjeu_urba',
        'table' => 'dossier',
        'libelle' => _('Dossier enjeu URBA'),
        'type' => 'select',
        "subtype" => "manualselect",
        "args" => array(
            array("", "true", "false", ),
            array(_("Tous"), _("Oui"), _("Non"), ),
        ),
    ),
);

?>
