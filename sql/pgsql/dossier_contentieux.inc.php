<?php
/**
 * @package openads
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_instruction.inc.php";

//
// COMMON
//

/**
 * Récupération des configurations des requêtes à partir des widgets.
 */
// Composition du tableau de paramètres nécessaire à la méthode qui permet
// de récupérer le configuration de la requête du widget
$params = array();
$params['filtre'] = 'instructeur';
//
if ($obj === 'dossier_contentieux_toutes_infractions'
    || $obj === 'dossier_contentieux_tous_recours') {
    //
    $params['filtre'] = 'aucun';
}
//
require_once "../obj/om_widget.class.php";
$om_widget = new om_widget(0);
//
$conf_inf = $om_widget->get_config_dossier_contentieux_infraction($params);
//
$conf_re = $om_widget->get_config_dossier_contentieux_recours($params);

$obj_redirection = "demande_nouveau_dossier_contentieux";
// Si l'utilisateur n'a pas accès au menu Contentieux > Nouvelle demande, on le redirige
// vers le guichet unique
if ($f->isAccredited("demande_nouveau_dossier_contentieux_ajouter") === false) {
    $obj_redirection = "demande_nouveau_dossier";
}

// Actions en coin : ajouter
$tab_actions['corner']['ajouter'] = array(
    'lien' => '../scr/form.php?obj=' . $obj_redirection . '&amp;action=0&amp;advs_id=&amp;tricol=&amp;valide=&amp;retour=tab&amp;new=',
    'id' => '',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix add-16" title="'._('Ajouter').'">'._('Ajouter').'</span>',
    'rights' => array('list' => array($obj_redirection, $obj_redirection . '_ajouter'), 'operator' => 'OR'),
    'ordre' => 10,
);

// Tri par défaut des tableaux contentieux
$tri = " ORDER BY dossier.dossier DESC ";

//
// INFRACTIONS
//

// Jointures necessaires aux infractions
$table_inf = $conf_inf["query_ct_from"];

// Affiche les contrevenants pour les infractions
$case_contrevenant = "
CASE WHEN demandeur_contrevenant.qualite = 'particulier' 
    THEN TRIM(CONCAT(demandeur_contrevenant.particulier_nom, ' ', demandeur_contrevenant.particulier_prenom)) 
    ELSE TRIM(CONCAT(demandeur_contrevenant.personne_morale_raison_sociale, ' ', demandeur_contrevenant.personne_morale_denomination)) 
END
";

// Colonnes affichées sur les tableaux des infractions
$champs_affiche_inf = $conf_inf["query_ct_select_champaffiche"];

// Recherche simple pour les infractions
$champs_recherche_inf = array(
    'dossier.dossier as "'._("Dossier").'"',
    'CONCAT(dossier.terrain_adresse_voie_numero, \' \', dossier.terrain_adresse_voie) as "'._("Localisation (numéro et voie)").'"',
    'dossier.terrain_adresse_code_postal as "'._("Localisation (code postal)").'"',
    'dossier.terrain_adresse_localite as "'._("Localisation (ville)").'"',
);

// Affiche le champ de recherche sur l'arrondissement seulement si l'option est
// activée
if ($f->getParameter('option_arrondissement') === 'true') {
    //
    $champs_recherche_inf = array_merge($champs_recherche_inf, 
        array(
            'arrondissement.libelle as "'._("Arrondissement").'"',
        )
    );
}

// Suite de la recherche simple des infractions
$champs_recherche_inf = array_merge($champs_recherche_inf, 
    array(
        'demandeur_contrevenant.personne_morale_denomination as "'._("Contrevenant personne morale").'"',
        'demandeur_contrevenant.particulier_nom as "'._("Contrevenant particulier").'"',
        'etat.libelle as "'._("État").'"',
    )
);

// Conditions
$selection_inf = "
WHERE LOWER(dossier_autorisation_type.code) = LOWER('IN')
";

//
// RECOURS
//

// Jointures necessaires aux recours
$table_rec = $conf_re["query_ct_from"];

// Affiche les contrevenants pour les recours
$case_requerant = "
CASE WHEN demandeur_requerant.qualite = 'particulier' 
    THEN TRIM(CONCAT(demandeur_requerant.particulier_nom, ' ', demandeur_requerant.particulier_prenom)) 
    ELSE TRIM(CONCAT(demandeur_requerant.personne_morale_raison_sociale, ' ', demandeur_requerant.personne_morale_denomination)) 
END
";

// Colonnes affichées sur les tableaux des recours
$champs_affiche_rec = $conf_re["query_ct_select_champaffiche"];

// Recherche simple pour les recours
$champs_recherche_rec = array(
    'dossier.dossier as "'._("Dossier").'"',
    'dossier_autorisation_type_detaille.libelle as "'._("Type").'"',
    'dossier_autorisation_contestee.dossier as "'._("Autorisation").'"',
    'demandeur.personne_morale_denomination as "'._("Pétitionnaire dénomination de la personne morale").'"',
    'demandeur.particulier_nom as "'._("Pétitionnaire nom du particulier").'"',
    'CONCAT(dossier.terrain_adresse_voie_numero, \' \', dossier.terrain_adresse_voie) as "'._("Localisation (numéro et voie)").'"',
    'dossier.terrain_adresse_code_postal as "'._("Localisation (code postal)").'"',
    'dossier.terrain_adresse_localite as "'._("Localisation (ville)").'"',
);

// Affiche le champ de recherche sur l'arrondissement seulement si l'option est
// activée
if ($f->getParameter('option_arrondissement') === 'true') {
    //
    $champs_recherche_rec = array_merge($champs_recherche_rec, 
        array(
            'arrondissement.libelle as "'._("Arrondissement").'"',
        )
    );
}

// Suite de la recherche simple des recours
$champs_recherche_rec = array_merge($champs_recherche_rec, 
    array(
        'demandeur_requerant.personne_morale_denomination as "'._("Requérant dénomination de la personne morale").'"',
        'demandeur_requerant.particulier_nom as "'._("Requérant nom du particulier").'"',
        'etat.libelle as "'._("État").'"',
        'to_char(dossier.date_depot ,\'DD/MM/YYYY\') as "'._("Date de recours").'"',
        'to_char(dossier.date_cloture_instruction ,\'DD/MM/YYYY\') as "'._("Date de clôture d'instruction").'"',
        'avis_decision.libelle as "'._("Décision").'"',
        'to_char(dossier.date_decision ,\'DD/MM/YYYY\') as "'._("Date de décision").'"',
    )
);

// Conditions
$selection_re = "
WHERE LOWER(dossier_autorisation_type.code) = LOWER('RE')
";

?>
