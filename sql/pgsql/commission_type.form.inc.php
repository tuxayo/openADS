<?php
//$Id: commission_type.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 07/12/2012 17:33

include('../gen/sql/pgsql/commission_type.form.inc.php');


$sql_om_collectivite_multi = "SELECT
		om_collectivite.om_collectivite, om_collectivite.libelle
	FROM
		".DB_PREFIXE."om_collectivite
	WHERE
		om_collectivite.niveau = '1'
	ORDER BY
		om_collectivite.libelle ASC";
?>