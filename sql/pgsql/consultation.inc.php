<?php
/**
 *
 *
 * @package openfoncier
 * @version SVN : $Id: consultation.inc.php 5851 2016-02-02 16:45:09Z nmeucci $
 */

//
include "../gen/sql/pgsql/consultation.inc.php";

/*Titre de la page*/
$ent = _("consultation");

//
$case_type_consultation = 'CASE WHEN service.type_consultation=\'avec_avis_attendu\' 
            THEN \''._("avec avis attendu").'\'
            WHEN service.type_consultation=\'pour_conformite\' 
            THEN \''._("pour conformite").'\'
            WHEN service.type_consultation=\'pour_information\' 
            THEN \''._("pour information").'\'
    END';
$case_consultation_lu = "case consultation.lu when 't' then 'Oui' else 'Non' end";
$case_consultation_visible = "case consultation.visible when 't' then 'Oui' else 'Non' end";
// SELECT 
$champAffiche = array(
    'consultation.consultation as "'._("consultation").'"',
    'consultation.dossier as "'._("dossier").'"',
    'to_char(consultation.date_envoi ,\'DD/MM/YYYY\') as "'._("date_envoi").'"',
    'to_char(consultation.date_reception ,\'DD/MM/YYYY\') as "'._("date_reception").'"',
    'to_char(consultation.date_retour ,\'DD/MM/YYYY\') as "'._("date_retour").'"',
    'to_char(consultation.date_limite ,\'DD/MM/YYYY\') as "'._("date_limite").'"',
    'concat(service.abrege,\' - \',service.libelle) as "'._("service").'"',
    $case_type_consultation.' as "'._("type_consultation").'"',
    'avis_consultation.libelle as "'._("avis_consultation").'"',
    'instructeur.nom as "'._("instructeur").'"',
    'division.code as "'._("division").'"',
    $case_consultation_lu." as \""._("lu")."\"",
    $case_consultation_visible." as \""._("visible")."\""
);

$table .= "
LEFT JOIN ".DB_PREFIXE."instructeur 
    ON instructeur.instructeur=dossier.instructeur
LEFT JOIN ".DB_PREFIXE."om_utilisateur
    ON instructeur.om_utilisateur = om_utilisateur.om_utilisateur
LEFT JOIN ".DB_PREFIXE."division 
    ON dossier.division=division.division";

$tri= "ORDER BY consultation.date_envoi ASC, consultation.consultation";

/**
 * Gestion particulière de l'affichage du listing des consultations dans le
 * contexte d'un dossier d'instruction (pour un service consulté)
 */
if ($retourformulaire == 'service') {
    // Actions en coin : ajouter
    $tab_actions['corner']['ajouter'] = NULL;
    // Actions a gauche : consulter
    $tab_actions['left']['consulter'] = NULL;
    $selection=" where consultation.service ='".$idx."'";
}

/**
 * Gestion particulière de l'affichage du listing des consultations dans le
 * contexte d'un dossier d'instruction (pour l'instructeur)
 */
if ($retourformulaire == 'dossier'
    OR $retourformulaire == 'dossier_instruction'
    OR $retourformulaire == 'dossier_instruction_mes_encours'
    OR $retourformulaire == 'dossier_instruction_tous_encours'
    OR $retourformulaire == 'dossier_instruction_mes_clotures'
    OR $retourformulaire == 'dossier_instruction_tous_clotures'
    OR $retourformulaire == 'dossier_contentieux_mes_infractions'
    OR $retourformulaire == 'dossier_contentieux_toutes_infractions'
    OR $retourformulaire == "dossier_contentieux_mes_recours"
    OR $retourformulaire == "dossier_contentieux_tous_recours") {
    //
    $case_consultation_lu = "case when consultation.avis_consultation is null then ''
              else case consultation.lu
                       when 't' then 'Oui'
                       else 'Non'
                   end
         end";
    // SELECT 
    $champAffiche = array(
        'consultation.consultation as "'._("id").'"',
        'to_char(consultation.date_envoi ,\'DD/MM/YYYY\') as "'._("date_envoi").'"',
        'to_char(consultation.date_reception ,\'DD/MM/YYYY\') as "'._("date_reception").'"',
        'to_char(consultation.date_retour ,\'DD/MM/YYYY\') as "'._("date_retour").'"',
        'to_char(consultation.date_limite ,\'DD/MM/YYYY\') as "'._("date_limite").'"',
        'concat(service.abrege,\' - \',service.libelle) as "'._("service").'"',
        $case_type_consultation.' as "'._("type_consultation").'"',
        'avis_consultation.libelle as "'._("avis_consultation").'"',
        $case_consultation_lu." as \""._("lu")."\"",
        $case_consultation_visible." as \""._("visible")."\""
    );
    // Filtre dur les données du dossier
    $selection=" WHERE (consultation.dossier ='".$idx."')";
    // Ajout d'une action supplémentaire - ajout de consultations multiples
    $tab_actions['corner']['ajouter_multiple'] =
    array('lien' => '../scr/sousform.php?obj='.$obj.'&amp;action=40&amp;idx=0',
          'id' => '&amp;tri='.$tricolsf.'&amp;objsf='.$obj.'&amp;premiersf='.$premier.'&amp;retourformulaire='.$retourformulaire.'&amp;idxformulaire='.$idxformulaire.'&amp;trisf='.$tricolsf.'&amp;retour=tab',
          'lib' => '<span class="om-icon om-icon-16 om-icon-fix mut-add-16" title="'._('Ajouter plusieurs').'">'._('Ajouter plusieurs').'</span>',
          'rights' => array('list' => array($obj, $obj.'_ajouter'), 'operator' => 'OR'),
          );

    // Vérifie que l'utilisateur possède la permission bypass.
    // Vérifie que l'utilisateur est un instructeur, qu'il fait partie de la
    // division du dossier et que le dossier n'est pas clôturé.
    // Ces conditions sont identiques à la méthode can_show_or_hide_in_edition()
    // de la classe consultation permettant d'afficher ou cacher les actions
    // dans le portlet d'action du formulaire.
    // Il est nécessaire que ce if et cette méthode restent concordants en tout
    // point afin que le comportement des actions soit identique depuis le
    // formulaire et depuis le listing.
    if ($this->can_bypass("consultation", "visibilite_dans_edition") === true
        || ($this->getStatutDossier($idxformulaire) != "cloture")
            && ($this->isUserInstructeur() === true
                && $this->om_utilisateur["division"] == $this->get_division_from_dossier_without_inst($idxformulaire))) {

        // Ajout d'actions supplémentaires - Afficher et masquer un consultation dans les éditions
        $tab_actions['left']['masquer_dans_edition'] =
        array('lien' => '../scr/sousform.php?obj='.$obj.'&amp;action=140&amp;idx=',
              'id' => '&amp;tri='.$tricolsf.'&amp;objsf='.$obj.'&amp;premiersf='.$premier.'&amp;retourformulaire='.$retourformulaire.'&amp;idxformulaire='.$idxformulaire.'&amp;trisf='.$tricolsf.'&amp;retour=tab',
              'lib' => "<span class=\"om-icon om-icon-16 om-icon-fix unwatch-16\" title=\""._("Masquer dans les éditions")."\">"._("Masquer dans les éditions")."</span>",
              'type' => 'action-direct',
              'rights' => array('list' => array('consultation', 'consultation_visibilite_dans_edition'), 'operator' => 'OR'),
              'ordre' => 100
              );

        $tab_actions['left']['afficher_dans_edition'] =
        array('lien' => '../scr/sousform.php?obj='.$obj.'&amp;action=130&amp;idx=',
              'id' => '&amp;tri='.$tricolsf.'&amp;objsf='.$obj.'&amp;premiersf='.$premier.'&amp;retourformulaire='.$retourformulaire.'&amp;idxformulaire='.$idxformulaire.'&amp;trisf='.$tricolsf.'&amp;retour=tab',
              'lib' => "<span class=\"om-icon om-icon-16 om-icon-fix watch-16\" title=\""._("Afficher dans les éditions")."\">"._("Afficher dans les éditions")."</span>",
              'type' => 'action-direct',
              'rights' => array('list' => array('consultation', 'consultation_visibilite_dans_edition'), 'operator' => 'OR'),
              'ordre' => 110
              );



        // Création d'une option pour afficher l'affichage ou le masquage d'une
        // consultation dans les édition
        $options[] = array(
            'type' => 'condition',
            'field' => $case_consultation_visible,
            'case' => array(
                array(
                    'values' => array('Non', ),
                    'style' => 'afficher-consultation-edition',
                ),
                array(
                    'values' => array('Oui', ),
                    'style' => 'masquer-consultation-edition',
                ),
            ),
        );
    }
}
 
/**
 * Options
 */
// On affiche le champ lu en gras
$options[] = array(
    "type" => "condition",
    "field" => $case_consultation_lu,
    "case" => array(
            "0" => array(
                "values" => array("Non", ),
                "style" => "non_lu",
                ),
            ),
    );
/**
 * Options
 */
// On met la ligne en couleur selon le type de condition
$options[] = array(
    "type" => "condition",
    "field" => $case_type_consultation,
    "case" => array(
                 array(
                    "values" => array(_("avec avis attendu"), ),
                    "style" => "consultation-avec-avis-attendu",
                ),
                array(
                    "values" => array(_("pour conformite"), ),
                    "style" => "consultation-pour-conformite",
                ),
                array(
                    "values" => array(_("pour information"), ),
                    "style" => "consultation-pour-information",
                ),
            ),
);

/**
 * Gestion particulière de l'affichage du listing dans le contexte d'un dossier
 * d'instruction
 */
include "../sql/pgsql/dossier_instruction_droit_specifique_par_division.inc.php";
// Gestion des groupes et confidentialité
include('../sql/pgsql/filter_group.inc.php');