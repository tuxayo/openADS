<?php
/**
 *
 * @package openfoncier
 * @version SVN : $Id: dossier_instruction.inc.php 6128 2016-03-08 15:43:42Z jymadier $
 */

/*Etend la classe dossier*/
include('../sql/pgsql/dossier.inc.php');

/*Titre de la page*/
$ent = _("instruction")." -> "._("dossiers d'instruction");
if (isset($idz) && trim($idz) != '') {
    $ent .= "->&nbsp;".mb_strtoupper($idz, "UTF-8")."&nbsp;";
} else {
  $idz = '';
}

$tab_title = _("DI");

/* Test SQL pour récupérer les bons champs selon la qualité du demandeur : 
 * particulier ou personne morale*/
$case_demandeur = "CASE WHEN demandeur.qualite='particulier' 
THEN TRIM(CONCAT(demandeur.particulier_nom, ' ', demandeur.particulier_prenom)) 
ELSE TRIM(CONCAT(demandeur.personne_morale_raison_sociale, ' ', demandeur.personne_morale_denomination)) 
END";

/*Formatage de l'adresse du terrain, concatenantion de plusieurs champs pour les 
 * mettrent dans une seule colonne*/
$trim_concat_terrain ='TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',
        dossier.terrain_adresse_voie,\' \',
        dossier.terrain_adresse_lieu_dit,\' \',
        dossier.terrain_adresse_code_postal,\' \',
        dossier.terrain_adresse_localite,\' \',
        dossier.terrain_adresse_bp,\' \',
        dossier.terrain_adresse_cedex
    )) as "'._("localisation").'"'; 

/*Tables sur lesquels la requête va s'effectuer*/
$table = DB_PREFIXE."dossier
LEFT JOIN (
    SELECT * 
    FROM ".DB_PREFIXE."lien_dossier_demandeur
    INNER JOIN ".DB_PREFIXE."demandeur
        ON demandeur.demandeur = lien_dossier_demandeur.demandeur
    WHERE lien_dossier_demandeur.petitionnaire_principal IS TRUE
    AND LOWER(demandeur.type_demandeur) = LOWER('petitionnaire')
) as demandeur
    ON demandeur.dossier = dossier.dossier
LEFT JOIN ".DB_PREFIXE."dossier_autorisation
    ON dossier_autorisation.dossier_autorisation = dossier.dossier_autorisation
LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
    ON dossier_autorisation_type_detaille.dossier_autorisation_type_detaille = dossier_autorisation.dossier_autorisation_type_detaille
LEFT JOIN ".DB_PREFIXE."instructeur
    ON dossier.instructeur = instructeur.instructeur
LEFT JOIN ".DB_PREFIXE."om_utilisateur
    ON instructeur.om_utilisateur = om_utilisateur.om_utilisateur
LEFT JOIN ".DB_PREFIXE."instructeur as instructeur2
    ON dossier.instructeur_2 = instructeur2.instructeur
LEFT JOIN ".DB_PREFIXE."om_utilisateur as om_utilisateur2
    ON instructeur2.om_utilisateur = om_utilisateur2.om_utilisateur
LEFT JOIN ".DB_PREFIXE."etat
    ON dossier.etat = etat.etat
LEFT JOIN ".DB_PREFIXE."division
    ON dossier.division = division.division
LEFT JOIN ".DB_PREFIXE."avis_decision   
   ON avis_decision.avis_decision=dossier.avis_decision
LEFT JOIN ".DB_PREFIXE."om_collectivite 
    ON dossier.om_collectivite=om_collectivite.om_collectivite
LEFT OUTER JOIN ".DB_PREFIXE."arrondissement
    ON arrondissement.code_postal = dossier.terrain_adresse_code_postal 
";


/*Champs du début de la requête*/
$champAffiche_debut_commun = array(
    'dossier.dossier as "'._("dossier").'"',
    'dossier.dossier_libelle as "'._("dossier").'"',
    $case_demandeur.' as "'._("petitionnaire").'"',
    $trim_concat_terrain,
    'dossier_autorisation_type_detaille.libelle as "'._("nature_dossier").'"',
    'to_char(dossier.date_depot ,\'DD/MM/YYYY\') as "'._("date_depot").'"',
    'to_char(dossier.date_complet ,\'DD/MM/YYYY\') as "'._("date_complet").'"',
    'CASE WHEN dossier.incomplet_notifie IS TRUE AND dossier.incompletude IS TRUE 
        THEN to_char(dossier.date_limite_incompletude ,\'DD/MM/YYYY\') 
        ELSE to_char(dossier.date_limite ,\'DD/MM/YYYY\')
    END as "'._("date_limite").'"',
);

/*Champs de la fin de la requête*/
$champAffiche_fin_commun = array(
    'etat.libelle as "'._("etat").'"',
    'CASE WHEN dossier.enjeu_urba is TRUE
          THEN \'<span class="om-icon om-icon-16 om-icon-fix enjeu_urba-16" title="'._("Enjeu URBA").'">URBA</span>\'
          ELSE \'\'
          END ||
     CASE WHEN dossier.enjeu_erp is TRUE
          THEN \'<span class="om-icon om-icon-16 om-icon-fix enjeu_erp-16" title="'._("Enjeu ERP").'">ERP</span>\'
          ELSE \'\'
          END
     as "'._("enjeu").'"',
);
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche_fin_commun, "om_collectivite.libelle as \""._("collectivite")."\"");
}

$selection .= "AND groupe.code != 'CTX'";

/*Liste des champs affichés dans le tableau de résultat*/
$champAffiche = array_merge($champAffiche_debut_commun,
                            array('instructeur.nom as "'._("instructeur").'"',
                                  'division.code as "'._("division").'"',
                                  ),
                            $champAffiche_fin_commun);

// Suppression du bouton d'ajout, qui n'est pas affiché par défaut dans les listings de
// dossiers d'instruction
$tab_actions['corner']['ajouter'] = NULL;

// Liste des autres dossiers d'instructions
if ($retourformulaire== 'dossier_instruction'){
    $champAffiche=array(
        'b.dossier as "'._("dossier_instruction").'"',
        'b.dossier_libelle as "'._("dossier").'"',
        'dossier_instruction_type.libelle as "'._("demande_type").'"',
        'to_char(b.date_depot ,\'DD/MM/YYYY\') as "'._("date_depot").'"',
        'b.etat as "'._("etat").'"',
    );

    $table =DB_PREFIXE.'dossier as a
            JOIN '.DB_PREFIXE.'dossier_autorisation
            ON a.dossier_autorisation=dossier_autorisation.dossier_autorisation
            JOIN '.DB_PREFIXE.'dossier as b
            ON b.dossier_autorisation=dossier_autorisation.dossier_autorisation
            JOIN '.DB_PREFIXE.'dossier_instruction_type
            ON dossier_instruction_type.dossier_instruction_type = b.dossier_instruction_type';
    $selection = 'WHERE a.dossier=\''.$idx.'\'';
    $tri= "order by b.date_depot ASC";

    //
    $tab_actions['left']["consulter"] = 
        array('lien' => 'form.php?obj=dossier_instruction&amp;action=3'.'&amp;idx=',
              'id' => '&retourformulaire='.$retourformulaire.'&retour=',
              'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'._('Consulter').'</span>',
              'rights' => array('list' => array('dossier_instruction', 'dossier_instruction_consulter'), 'operator' => 'OR'),
              'ordre' => 10,
              'ajax' => false);

    $tab_actions['content'] = $tab_actions['left']["consulter"];
    $options[] = array(
        "type"=>"pagination_select",
        "display"=>false,
    );
}
if ( $retourformulaire == "dossier_autorisation"){

    //
    $tab_actions['left']["consulter"] = 
    array('lien' => '../scr/form.php?obj=dossier_instruction&amp;action=3&amp;idx=',
              'id' => '&amp;retourformulaire='.$retourformulaire,
              'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'._('Consulter').'</span>',
              'rights' => array('list' => array('dossier_instruction', 'dossier_instruction_consulter'), 'operator' => 'OR'),
              'ordre' => 10,
              'ajax' => false);
              
    $tab_actions['content'] = $tab_actions['left']["consulter"] ;
}


// Affichage du bouton de redirection vers le SIG externe si configuré
// XXX Ajouter filtre pour afficher l'icone géolocalisation en fonction de la conf SIG du dossier
if($f->getParameter('option_sig') == 'sig_externe') {
    $tab_actions['left']["localiser-sig-externe"] = array(
                'lien' => 'form.php?obj=dossier_instruction&amp;action=140&amp;idx=',
                'id' => '',
                'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'._('Localiser').'">'._('Localiser').'</span>',
                'rights' => array('list' => array('dossier_instruction', 'dossier_instruction_consulter'), 'operator' => 'OR'),
                'ordre' => 20,
                'target' => "_SIG",
                'ajax' => false);
}

/*Paramétrage des actions du portlet*/
$portlet_actions['modifier'] = array('lien' => "form.php?obj=dossier_instruction&amp;action=1&amp;idx=",
          'id' => "&amp;idz=$idz&amp;premier=0&amp;advs_id=&amp;recherche=&amp;tricol=-0&amp;selectioncol=&amp;valide=&amp;retour=form&retourformulaire=".$obj."&retourformulaire2=".$retourformulaire,
          'lib' => "<span class=\"om-prev-icon om-icon-16 edit-16\" title=\"".
                _("Modifier")."\">"._("Modifier")."</span>",
          'ordre' => 10,
          'rights' => array('list' => array("dossier_instruction_mes_encours_modifier", "dossier_instruction_tous_encours_modifier", "dossier_instruction_mes_clotures_modifier", "dossier_instruction_tous_clotures_modifier", "dossier_instruction_modifier",
            "dossier_contentieux_mes_infraction", "dossier_contentieux_toutes_infraction"), 'operator' => 'OR'),
          'ajax' => false);

$portlet_actions['supprimer'] = array('lien' => "form.php?obj=dossier_instruction&amp;action=2&amp;idx=",
          'id' => "&amp;idz=$idz&amp;premier=0&amp;advs_id=&amp;recherche=&amp;tricol=-0&amp;selectioncol=&amp;valide=&amp;retour=form&retourformulaire=".$obj."&retourformulaire2=".$retourformulaire,
          'lib' => "<span class=\"om-prev-icon om-icon-16 delete-16\" title=\"".
                _("Supprimer")."\">"._("Supprimer")."</span>",
          'ordre' => 28,
          'rights' => array('list' => array("dossier_instruction_mes_encours_supprimer", "dossier_instruction_tous_encours_supprimer", "dossier_instruction_mes_clotures_supprimer", "dossier_instruction_tous_clotures_supprimer", "dossier_instruction_supprimer",
            "dossier_contentieux_mes_infraction", "dossier_contentieux_toutes_infraction"), 'operator' => 'OR'),
          'ajax' => false);

// Si filtre DI auxquels on peut proposer une autre décision
if (isset($extra_parameters["filtre_decision"])
    && $extra_parameters["filtre_decision"] == true) {

    // Augmentation de la limite
    $serie = 50;
    // Réinitialisation des options
    $options = array();
    // Suppression de la recherche avancée
    $options[] = array(
        'type' => 'search',
        'display' => false);
    // Suppression du sélecteur de pages
    $options[] = array(
        'type' => 'pagination_select',
        'display' => false);
    // Ajout jointure
    $table .= "JOIN ".DB_PREFIXE."instruction ON instruction.instruction = (
                SELECT instruction
                FROM ".DB_PREFIXE."instruction
                JOIN ".DB_PREFIXE."evenement on instruction.evenement=evenement.evenement
                WHERE instruction.dossier = dossier.dossier
                AND evenement.retour IS FALSE
                ORDER BY date_evenement DESC, instruction DESC
                LIMIT 1
            )
            JOIN ".DB_PREFIXE."evenement ON instruction.evenement=evenement.evenement
            JOIN ".DB_PREFIXE."dossier_instruction_type ON dossier.dossier_instruction_type=dossier_instruction_type.dossier_instruction_type AND dossier_instruction_type.code IN ('P', 'T', 'M')";
    // Modification sélection
    $selection = "WHERE
                groupe.code != 'CTX'
                AND ((evenement.type = 'arrete' AND instruction.om_final_instruction IS TRUE) OR evenement.type = 'changement_decision')
                AND evenement.retour IS FALSE
                AND instruction.date_retour_signature IS NULL
                AND instruction.date_envoi_rar IS NULL
                AND instruction.date_retour_rar IS NULL
                AND instruction.date_envoi_controle_legalite IS NULL
                AND instruction.date_retour_controle_legalite IS NULL
                AND etat.statut = 'encours'
                AND om_utilisateur.login != '".$_SESSION['login']."'";
    // Si collectivité de l'utilisateur niveau mono alors filtre sur celle-ci
    if ($f->isCollectiviteMono($_SESSION['collectivite']) === true) {
        $selection .= " AND dossier.om_collectivite=".$_SESSION['collectivite'];
    }
    // Modification tri
    $tri = " ORDER BY dossier.dossier ";
}

// Gestion des groupes et confidentialité
include ('../sql/pgsql/filter_group.inc.php');

?>