<?php
/**
 *
 *
 * @package openfoncier
 * @version SVN : $Id: demande_nature.inc.php 4651 2015-04-26 09:15:48Z tbenita $
 */

//
include "../gen/sql/pgsql/demande_nature.inc.php";

//
$ent = _("parametrage dossiers")." -> "._("demandes")." -> "._("nature");
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".mb_strtoupper($idz, "UTF-8")."&nbsp;";
}

//
$champAffiche[0] = "demande_nature.demande_nature as \""._("id")."\"";
$champRecherche[0] = "demande_nature.demande_nature as \""._("id")."\"";

?>
