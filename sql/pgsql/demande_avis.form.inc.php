<?php
//$Id: demande_avis.form.inc.php 5429 2015-11-13 17:08:37Z nmeucci $ 
//gen openMairie le 10/02/2011 20:32 
include('../gen/sql/pgsql/consultation.form.inc.php');

$tableSelect=DB_PREFIXE."consultation
    LEFT JOIN ".DB_PREFIXE."dossier 
        ON consultation.dossier=dossier.dossier
    LEFT JOIN ".DB_PREFIXE."service
        ON service.service=consultation.service
    LEFT OUTER JOIN ".DB_PREFIXE."instructeur
        ON instructeur.instructeur=dossier.instructeur
    LEFT OUTER JOIN ".DB_PREFIXE."division
        ON division.division=instructeur.division
    LEFT OUTER JOIN ".DB_PREFIXE."avis_consultation 
        ON consultation.avis_consultation=avis_consultation.avis_consultation 
    LEFT JOIN ".DB_PREFIXE."donnees_techniques
        ON donnees_techniques.dossier_instruction = dossier.dossier
    LEFT JOIN ".DB_PREFIXE."lien_dossier_demandeur
        ON dossier.dossier=lien_dossier_demandeur.dossier AND lien_dossier_demandeur.petitionnaire_principal IS TRUE
    LEFT JOIN ".DB_PREFIXE."demandeur
        ON demandeur.demandeur=lien_dossier_demandeur.demandeur
    LEFT JOIN ".DB_PREFIXE."etat
        ON dossier.etat = etat.etat
        ";

$champs=array("consultation.consultation as \"consultation\"",
// Fieldset "Infos générales"
    // Fielsdet "Dossier"
        // 1ere ligne
            // 1ere colonne
                "consultation.dossier as \"dossier\"",
                "dossier.dossier_libelle as \"dossier_libelle\"",
                "division.chef as \"responsable\"",
                "etat.libelle as \"etat\"",
            // 2eme colonne
                "division.libelle as \"division\"",
                "concat(instructeur.nom,' tel. : '||instructeur.telephone) as \"instructeur\"",
        // 2eme ligne
                "to_char(dossier.date_depot ,'DD/MM/YYYY') as \"date_depot\"",
                "to_char(dossier.date_dernier_depot ,'DD/MM/YYYY') as \"date_dernier_depot\"",
                
                "CASE WHEN dossier.incomplet_notifie IS TRUE AND dossier.incompletude IS TRUE 
                    THEN to_char(dossier.date_limite_incompletude ,'DD/MM/YYYY') 
                    ELSE to_char(dossier.date_limite ,'DD/MM/YYYY')
                END as \"dossier_date_limite\"",
    
                //"to_char(dossier.date_limite ,'DD/MM/YYYY') as \"dossier_date_limite\"",
        // 3eme ligne
                "dossier.autorite_competente as \"autorite_competente\"",
        // 4eme ligne
                'TRIM(CONCAT(personne_morale_denomination,\' \',personne_morale_nom,\' \',demandeur.particulier_nom)) as "petitionnaire"',
                "TRIM(CONCAT(demandeur.numero,' ',demandeur.voie,' ',demandeur.complement,
                      ' ',demandeur.lieu_dit,' ',demandeur.code_postal,' ',demandeur.localite,
                      ' ',demandeur.bp,' ',demandeur.cedex,' ',demandeur.pays)) as \"adresse_petitionnaire\"",
                "replace(dossier.terrain_references_cadastrales, ';', ' ') as \"parcelle\"",
                'TRIM(CONCAT(dossier.terrain_adresse_voie_numero,\' \',dossier.terrain_adresse_voie,
    \' \', dossier.terrain_adresse_lieu_dit, \' \', dossier.terrain_adresse_code_postal,\' \', dossier.terrain_adresse_localite)) as "terrain"',
                "public.ST_AsText(dossier.geom::geometry) as \"geom\"",
    // Fieldset "Demande d'avis"
                "to_char(consultation.date_envoi ,'DD/MM/YYYY') as \"date_envoi\"",
                "concat(service.delai,' ','"._("mois")."') as \"delai\"",
                "to_char(consultation.date_limite ,'DD/MM/YYYY') as \"date_limite\"",
                "consultation.marque",

                // Fieldset "Avis rendu"
                "consultation.avis_consultation as \"avis_consultation\"",
                "consultation.motivation as \"motivation\"",
                "consultation.fichier as \"fichier\"",
// Fermeture fieldset "Infos générales"

// Fieldset "Principales caractéristiques du projet"
                "CONCAT_WS(
                    '<br/>',
                    CASE WHEN co_projet_desc = '' THEN
                        NULL
                    ELSE
                        TRIM(co_projet_desc)
                    END,
                    CASE WHEN ope_proj_desc = '' THEN
                        NULL
                    ELSE
                        TRIM(ope_proj_desc)
                    END,
                    CASE WHEN am_projet_desc = '' THEN
                        NULL
                    ELSE
                        TRIM(am_projet_desc)
                    END,
                    CASE WHEN dm_projet_desc = '' THEN
                        NULL
                    ELSE
                        TRIM(dm_projet_desc)
                    END
                ) as \"description_projet\"",
                "donnees_techniques.su_tot_shon_tot||' m²' as \"surface_total_projet\"",
                "REGEXP_REPLACE(CONCAT(
                    CASE
                        WHEN donnees_techniques.su_cstr_shon1 IS NULL
                        THEN ''
                        ELSE CONCAT('Habitation - ', donnees_techniques.su_cstr_shon1, ' m² <br/>')
                    END,
                    CASE
                        WHEN donnees_techniques.su_cstr_shon2 IS NULL
                        THEN ''
                        ELSE CONCAT('Hébergement hôtelier - ', donnees_techniques.su_cstr_shon2, ' m² <br/>')
                    END,
                    CASE
                        WHEN donnees_techniques.su_cstr_shon3 IS NULL
                        THEN ''
                        ELSE CONCAT('Bureaux - ', donnees_techniques.su_cstr_shon3, ' m² <br/>')
                    END,
                    CASE
                        WHEN donnees_techniques.su_cstr_shon4 IS NULL
                        THEN ''
                        ELSE CONCAT('Commerce - ', donnees_techniques.su_cstr_shon4, ' m² <br/>')
                    END,
                    CASE
                        WHEN donnees_techniques.su_cstr_shon5 IS NULL
                        THEN ''
                        ELSE CONCAT('Artisanat - ', donnees_techniques.su_cstr_shon5, ' m² <br/>')
                    END,
                    CASE
                        WHEN donnees_techniques.su_cstr_shon6 IS NULL
                        THEN ''
                        ELSE CONCAT('Industrie - ', donnees_techniques.su_cstr_shon6, ' m² <br/>')
                    END,
                    CASE
                        WHEN donnees_techniques.su_cstr_shon7 IS NULL
                        THEN ''
                        ELSE CONCAT('Exploitation agricole ou forestière - ', donnees_techniques.su_cstr_shon7, ' m² <br/>')
                    END,
                    CASE
                        WHEN donnees_techniques.su_cstr_shon8 IS NULL
                        THEN ''
                        ELSE CONCAT('Entrepôt - ', donnees_techniques.su_cstr_shon8, ' m² <br/>')
                    END, 
                    CASE
                        WHEN donnees_techniques.su_cstr_shon9 IS NULL
                        THEN ''
                        ELSE CONCAT('Service public ou d''intérêt collectif - ', donnees_techniques.su_cstr_shon9, ' m²')
                    END
                ), ' <br/>$', '') as \"surface\"",
                "donnees_techniques.co_tot_ind_nb as \"nombre_logement_crees_individuel\"",
                "donnees_techniques.co_tot_coll_nb as \"nombre_logement_crees_collectif\"",
                "donnees_techniques.co_statio_apr_nb as \"nombre_places_parking\"",
// Fermeture fieldset "Principales caractéristiques du projet"

                "consultation.service",
);

$sql_service="SELECT service.service, service.abrege||' '||service.libelle 
    FROM ".DB_PREFIXE."service 
    WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) 
        OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))
    ORDER BY service.abrege, service.libelle";
$sql_service_by_id = "SELECT service.service, service.abrege||' '||service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
    
$sql_autorite_competente="SELECT autorite_competente.autorite_competente,
                                autorite_competente.libelle
                         FROM ".DB_PREFIXE."autorite_competente
                         ORDER BY autorite_competente.libelle";
$sql_autorite_competente_by_id = "SELECT autorite_competente.autorite_competente,
                                autorite_competente.libelle
                         FROM ".DB_PREFIXE."autorite_competente
                         WHERE autorite_competente.autorite_competente = <idx>";
             
?>