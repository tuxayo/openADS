<?php
/*
 * Ce script permet de d'ajouter aux clauses where des listing ($$query_ct_select) les
 * notions de groupes et de confidentialité.
 * 
 * @package openfoncier
 * @version SVN : $Id: filter_group_widgets.inc.php 6565 2017-04-21 16:14:15Z softime $
 */


// Tableau temporaire contenant les clauses pour chaque groupe
$group_clause = array();
foreach ($_SESSION["groupe"] as $key => $value) {
    $group_clause[$key] = "(groupe.code = '".$key."'";
    if($value["confidentiel"] !== true) {
        $group_clause[$key] .= " AND dossier_autorisation_type.confidentiel IS NOT TRUE";
    }
    $group_clause[$key] .= ")";
}
// Mise en chaîne des clauses
$conditions = implode(" OR ", $group_clause);
if ($conditions !== "") {
    $query_ct_where_groupe .= " AND (".$conditions.")";
}


// Jointures manquantes
if (preg_match("/".DB_PREFIXE."dossier_autorisation(?!_)/i", $query_ct_from) === 0) {
    $query_ct_from .= "
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation
        ON dossier_autorisation.dossier_autorisation = dossier.dossier_autorisation";
}
if (preg_match("/".DB_PREFIXE."dossier_autorisation_type_detaille(?!_)/i", $query_ct_from) === 0) {
    $query_ct_from .= "
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
        ON dossier_autorisation.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille";
}
if (preg_match("/".DB_PREFIXE."dossier_autorisation_type(?!_)/i", $query_ct_from) === 0) {
    $query_ct_from .= "
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
        ON dossier_autorisation_type.dossier_autorisation_type = dossier_autorisation_type_detaille.dossier_autorisation_type";
}
if (preg_match("/".DB_PREFIXE."groupe(?!_)/i", $query_ct_from) === 0) {
    $query_ct_from .= "
    LEFT JOIN ".DB_PREFIXE."groupe
        ON dossier_autorisation_type.groupe = groupe.groupe";
}

?>
