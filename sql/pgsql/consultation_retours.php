<?php
/**
 * LISTING - Retours de consultation
 *
 * Le listing 'Retours de consultation' permet d'afficher la même liste de
 * dossiers que le widget 'Retours de consultation'. Ce script permet de
 * définir les paramètres génériques du listing qui se trouvent dans la
 * définition du widget obj/om_widget.class.php et a pour objectif d'être
 * inclus par les scripts de définition des listings spécifiques à chaque
 * filtre : instructeur, division, aucun.
 *
 * @package openads
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/consultation.inc.php";

/**
 * Filtres
 */
//
if ($obj === "consultation_tous_retours") {
    $filtre = "aucun";
} elseif ($obj === "consultation_retours_ma_division") {
    $filtre = "division";
} else {
    $filtre = "instructeur";
}
//
$params = array(
    "filtre" => $filtre,
);

/**
 * Récupération de la configuration de la requête à partir du widget.
 */
//
require_once "../obj/om_widget.class.php";
$om_widget = new om_widget(0);
//
$conf = $om_widget->get_config_consultation_retours($params);
//
$filtre = $conf["arguments"]["filtre"];

/**
 *
 */
//
$tab_description = $conf["message_help"];

//
$tab_title = _("consultation");

//
$displayed_fields_begin = array(
    'consultation.consultation as "'._("consultation").'"',
    'dossier.dossier_libelle as "'._("dossier").'"',
    'concat(service.abrege,\' \',service.libelle) as "'._("service").'"',
    'CASE WHEN service.type_consultation=\'avec_avis_attendu\' 
            THEN \''._("avec avis attendu").'\'
            WHEN service.type_consultation=\'pour_conformite\' 
            THEN \''._("pour conformite").'\'
            WHEN service.type_consultation=\'pour_information\' 
            THEN \''._("pour information").'\'
    END as "'._("type_consultation").'"',
    'to_char(consultation.date_reception ,\'DD/MM/YYYY\') as "'._("date_reception").'"',
    'to_char(consultation.date_retour ,\'DD/MM/YYYY\') as "'._("date_retour").'"',
    'to_char(consultation.date_limite ,\'DD/MM/YYYY\') as "'._("date_limite").'"',
    'avis_consultation.libelle as "'._("avis_consultation").'"',
);
$displayed_field_instructeur = array(
    'instructeur.nom as "'._("instructeur").'"',
);
$displayed_field_division = array(
    'division.code as "'._("division").'"',
);
$displayed_field_collectivite = array(
    'om_collectivite.libelle as "'._("collectivite").'"',
);
$displayed_fields_end = array(
    'CASE WHEN dossier.enjeu_erp is TRUE THEN \'<span class="om-icon om-icon-16 om-icon-fix enjeu_erp-16" title="'._('Enjeu ERP').'">ERP</span>\' ELSE \'\' END ||
     CASE WHEN dossier.enjeu_urba is TRUE THEN \'<span class="om-icon om-icon-16 om-icon-fix enjeu_urba-16" title="'._('Enjeu Urba').'">URBA</span>\' ELSE \'\' END
     as "'._("enjeu").'"',
);

// FROM
$table = $conf["query_ct_from"];
// WHERE
$selection = sprintf(
    "WHERE 
        %s
    ",
    $conf["query_ct_where_common"]
);

//
$tri = " ORDER BY dossier.enjeu_erp, dossier.enjeu_urba, consultation.date_retour ";

//
$tab_actions['corner']['ajouter'] = null;

/**
 * Options - Style CSS sur certaines lignes
 * On met la ligne en couleur selon le type de consultation
 */
$options[] = array(
    "type" => "condition",
    "field" => 'CASE WHEN service.type_consultation=\'avec_avis_attendu\' 
            THEN \''._("avec avis attendu").'\'
            WHEN service.type_consultation=\'pour_conformite\' 
            THEN \''._("pour conformite").'\'
            WHEN service.type_consultation=\'pour_information\' 
            THEN \''._("pour information").'\'
    END',
    "case" => array(
        array(
            "values" => array(_("avec avis attendu"), ),
            "style" => "consultation-avec-avis-attendu",
        ),
        array(
            "values" => array(_("pour conformite"), ),
            "style" => "consultation-pour-conformite",
        ),
        array(
            "values" => array(_("pour information"), ),
            "style" => "consultation-pour-information",
        ),
    ),
);

/**
 * Options - ADVSEARCH
 */
//
$advsearch_fields_begin = array(
    //
    'service_abrege' => array(
        'table' => 'service',
        'colonne' => 'abrege',
        'type' => 'text',
        'libelle' => _('Service (abrege)'),
    ),
    //
    'service' => array(
        'table' => 'service',
        'colonne' => 'libelle',
        'type' => 'text',
        'libelle' => _('Service'),
    ),
);
//
$advsearch_field_instructeur = array(
    //
    'instructeur' => array(
        'colonne' => 'nom',
        'table' => 'instructeur',
        'libelle' => _('Instructeur'),
        'type' => 'text',
    ),
);
//
$advsearch_field_division = array(
    //
    'division' => array(
        'colonne' => 'code',
        'table' => 'division',
        'libelle' => _('Division'),
        'type' => 'text',
    ),
);
//
$advsearch_field_collectivite = array(
    //
    'collectivite' => array(
        'table' => 'om_collectivite',
        'colonne' => 'libelle',
        'type' => 'text',
        'libelle' => _('om_collectivite'),
    ),
);
//
$advsearch_fields_end = array(
    //
    'date_envoi' => array(
        'colonne' => 'date_envoi',
        'table' => 'consultation',
        'libelle' => _('Date d\'envoi'),
        'type' => 'date',
        'where' => 'intervaldate',
    ),
    //
    'date_retour' => array(
        'colonne' => 'date_retour',
        'table' => 'consultation',
        'libelle' => _('Date de retour'),
        'type' => 'date',
        'where' => 'intervaldate',
    ),
    //
    'date_limite' => array(
        'colonne' => 'date_limite',
        'table' => 'consultation',
        'libelle' => _('Date limite'),
        'type' => 'date',
        'where' => 'intervaldate',
    ),
);

?>
