<?php
//$Id: dossier_autorisation_type_detaille.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 07/11/2012 12:55

include('../gen/sql/pgsql/dossier_autorisation_type_detaille.form.inc.php');

//champs select
$sql_dossier_autorisation_type="SELECT
dossier_autorisation_type.dossier_autorisation_type,
(dossier_autorisation_type.code ||' ('||dossier_autorisation_type.libelle||')') as lib
FROM ".DB_PREFIXE."dossier_autorisation_type
ORDER BY lib";

$sql_dossier_autorisation_type_by_id = "SELECT
dossier_autorisation_type.dossier_autorisation_type,
(dossier_autorisation_type.code ||' ('||dossier_autorisation_type.libelle||')') as lib
FROM ".DB_PREFIXE."dossier_autorisation_type
WHERE dossier_autorisation_type = <idx>";

?>