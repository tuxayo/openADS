<?php
//$Id: affectation_automatique.inc.php 4651 2015-04-26 09:15:48Z tbenita $ 
//gen openMairie le 30/11/2012 12:37

include('../gen/sql/pgsql/affectation_automatique.inc.php');
$ent = _("parametrage")." -> "._("gestion des dossiers")." -> "._("affectation_automatique");

// FROM
$table .= "
    LEFT JOIN ".DB_PREFIXE."division as division2
        ON instructeur2.division = division2.division
    LEFT JOIN ".DB_PREFIXE."division as division3
        ON instructeur3.division = division3.division
";

// SELECT 
$champAffiche = array(
    'affectation_automatique.affectation_automatique as "'._("affectation_automatique").'"',
    'dossier_autorisation_type_detaille.libelle as "'._("dossier_autorisation_type_detaille").'"',
    'CASE WHEN instructeur2.nom IS NOT NULL
        THEN CONCAT(instructeur2.nom, \' (\', division2.code, \')\') 
        ELSE \'\'
    END as "'._("instructeur").'"',
    'CASE WHEN instructeur3.nom IS NOT NULL
        THEN CONCAT(instructeur3.nom, \' (\', division3.code, \')\') 
        ELSE \'\'
    END as "'._("instructeur").'"',
    'arrondissement.libelle as "'._("arrondissement").'"',
    'quartier.libelle as "'._("quartier").'"',
    'affectation_automatique.section as "'._("section").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \""._("collectivite")."\"");
}

$champRecherche = array(
    'affectation_automatique.affectation_automatique as "'._("affectation_automatique").'"',
    'arrondissement.libelle as "'._("arrondissement").'"',
    'quartier.libelle as "'._("quartier").'"',
    'affectation_automatique.section as "'._("section").'"',
    'CASE WHEN instructeur2.nom IS NOT NULL
        THEN CONCAT(instructeur2.nom, \' (\', division2.code, \')\') 
        ELSE \'\'
    END as "'._("instructeur").'"',
    'dossier_autorisation_type_detaille.libelle as "'._("dossier_autorisation_type_detaille").'"',
    'CASE WHEN instructeur3.nom IS NOT NULL
        THEN CONCAT(instructeur3.nom, \' (\', division3.code, \')\') 
        ELSE \'\'
    END as "'._("instructeur_2").'"',
    );

?>
