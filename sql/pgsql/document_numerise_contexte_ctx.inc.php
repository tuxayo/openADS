<?php
/**
 * Script de paramétrage du listing des documents numérisés.
 *
 * Dans le contexte d'un dossier contentieux. 
 *
 * @package openads
 * @version SVN : $Id: document_numerise_contexte_ctx.inc.php 6565 2017-04-21 16:14:15Z softime $
 */

include('../sql/pgsql/document_numerise.inc.php');

?>