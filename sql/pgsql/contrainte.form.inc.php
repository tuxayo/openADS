<?php
//$Id: contrainte.form.inc.php 4692 2015-04-30 13:54:08Z tbenita $ 
//gen openMairie le 03/01/2014 16:53

include('../gen/sql/pgsql/contrainte.form.inc.php');

$champs=array(
    "contrainte",
    // Contrainte
    "libelle",
    "nature",
    "reference",
    "numero",
    "no_ordre",
    // Catégorie
    "groupe",
    "sousgroupe",
    // Détail
    "texte",
    "service_consulte",
    "om_collectivite",
    "om_validite_debut",
    "om_validite_fin"
);

// Ajout des tableaux des données pour les select
$nature = array(
    array("PLU", "POS", "CC", "RNU"), 
    array(_("PLU"), _("POS"), _("CC"), _("RNU")),
);

?>
