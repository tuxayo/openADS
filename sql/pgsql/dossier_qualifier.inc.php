<?php

//
include('../sql/pgsql/dossier_instruction.inc.php');

// Titre de la page
$ent = _("instruction")." -> "._("qualification")." -> "._("dossiers a qualifier");

//Actions du tableau
$tab_actions['left']['consulter'] =
    array('lien' => 'form.php?obj=dossier_instruction&amp;action=3'.'&amp;idx=',
          'id' => '&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;recherche='.$recherche1.'&amp;tricol='.$tricol.'&amp;selectioncol='.$selectioncol.'&amp;valide='.$valide.'&amp;retour=tab',
          'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'._('Consulter').'</span>',
          'rights' => array('list' => array($obj, $obj.'_consulter'), 'operator' => 'OR'),
          'ordre' => 10,);
$tab_actions['content'] = $tab_actions['left']['consulter'];

$tab_actions['corner']['ajouter'] = "";
//
$champRecherche = array(
    'dossier.dossier as "'._("dossier").'"',
    'dossier.dossier_libelle as "'._("libelle dossier").'"',
    'demandeur.particulier_nom as "'._("petitionnaire particulier").'"',
    'demandeur.personne_morale_denomination as "'._("petitionnaire personne morale").'"',

    );
//
$selection = "  WHERE
        groupe.code != 'CTX' AND
        dossier.a_qualifier IS TRUE AND
        om_utilisateur.login = '".$_SESSION['login']."'";
        
$tri="ORDER BY dossier.date_depot, dossier.dossier";

// Gestion des groupes et confidentialité
include('../sql/pgsql/filter_group.inc.php');

?>
