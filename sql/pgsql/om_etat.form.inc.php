<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id: om_etat.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $
 */

//
include "../core/sql/pgsql/om_etat.form.inc.php";

$sql_om_sousetat="select id, (id||' '||libelle) as libelle from ".DB_PREFIXE."om_sousetat";
$sql_om_sousetat.=" where actif IS TRUE and om_collectivite=".$_SESSION['collectivite'];
$sql_om_sousetat.=" order by libelle";

$sql_om_sousetat_by_id="select id, (id||' '||libelle) as libelle from ".DB_PREFIXE."om_sousetat";
$sql_om_sousetat_by_id.=" ";
?>
