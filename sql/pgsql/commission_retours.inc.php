<?php
/**
 * Ce script permet de ...
 *
 * @package openfoncier
 * @version SVN : $Id: commission_retours.inc.php 4418 2015-02-24 17:30:28Z tbenita $
 */

//
include "../sql/pgsql/dossier_commission.inc.php";

//
$ent = _("instruction")." -> "._("commissions")." -> ";

//
$tab_title = _("commission");


/**
 * Filtres
 */
//
if ($obj === "commission_tous_retours") {
    $filtre = "aucun";
} elseif ($obj === "commission_retours_ma_division") {
    $filtre = "division";
} else {
    $filtre = "instructeur";
}
//
$params = array(
    "filtre" => $filtre,
);

/**
 * Récupération de la configuration de la requête à partir du widget.
 */
//
require_once "../obj/om_widget.class.php";
$om_widget = new om_widget(0);
//
$conf = $om_widget->get_config_commission_retours($params);
//
$filtre = $conf["arguments"]["filtre"];

/**
 *
 */
//
$tab_description = $conf["message_help"];



//
$tab_actions['corner']['ajouter']=NULL;

// FROM
$table = $conf["query_ct_from"];
// WHERE
$selection = sprintf(
    "WHERE 
        %s
    ",
    $conf["query_ct_where_common"]
);

?>