<?php
/**
 * Script de paramétrage de la classe des données techniques.
 *
 * Dans le contexte d'un dossier contentieux. 
 *
 * @package openads
 * @version SVN : $Id: donnees_techniques_contexte_ctx.inc.php 6565 2017-04-21 16:14:15Z softime $
 */

include('../sql/pgsql/donnees_techniques.inc.php');

?>