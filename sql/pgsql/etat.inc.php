<?php
/**
 *
 *
 * @package openfoncier
 * @version SVN : $Id: etat.inc.php 4651 2015-04-26 09:15:48Z tbenita $
 */

//
include "../gen/sql/pgsql/etat.inc.php";

//
$ent = _("parametrage dossiers")." -> "._("workflows")." -> "._("etat");
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".mb_strtoupper($idz, "UTF-8")."&nbsp;";
}

//
$sousformulaire = array(
);

?>
