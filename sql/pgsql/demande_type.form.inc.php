<?php
/**
 *
 *
 * @package openfoncier
 * @version SVN : $Id: demande_type.form.inc.php 4777 2015-05-21 15:13:37Z nhaye $
 */

//
include "../gen/sql/pgsql/demande_type.form.inc.php";

//
$retourformulaire = (isset($_GET['retourformulaire'])) ? $_GET['retourformulaire'] : "";

// si en sous-formulaire alors on cache le bouton modifier
// pour éviter un bug sur le multiselect
if (isset($retourformulaire) && $retourformulaire != '') {
    // Actions en coin : modifier
    $portlet_actions['modifier'] = NULL;
}

//
$champs = array(
    "demande_type.demande_type",
    "demande_type.code",
    "demande_type.libelle",
    "demande_type.description",
    "demande_type.groupe",
    "demande_type.dossier_autorisation_type_detaille",
    "demande_type.demande_nature",
    "array_to_string(array_agg(lien_demande_type_etat.etat
                        ORDER BY etat.libelle), ';') as etats_autorises",
    "demande_type.contraintes",
    "demande_type.dossier_instruction_type",
    "demande_type.qualification",
    "demande_type.evenement",
    "demande_type.document_obligatoire",
);

//
$tableSelect = "
".DB_PREFIXE."demande_type 
  LEFT JOIN ".DB_PREFIXE."lien_demande_type_etat
    ON lien_demande_type_etat.demande_type=demande_type.demande_type
  LEFT JOIN ".DB_PREFIXE."etat
    ON lien_demande_type_etat.etat=etat.etat";
//
$selection = " GROUP BY demande_type.demande_type, demande_type.libelle ";

//
$sql_dossier_instruction_type = "SELECT
dossier_instruction_type.dossier_instruction_type,
(dossier_autorisation_type_detaille.code||' - '||dossier_instruction_type.libelle) as lib
FROM ".DB_PREFIXE."dossier_instruction_type
LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
ORDER BY lib";

$sql_dossier_instruction_type_by_id = "SELECT
dossier_instruction_type.dossier_instruction_type,
(dossier_autorisation_type_detaille.code||' - '||dossier_instruction_type.libelle) as lib
FROM ".DB_PREFIXE."dossier_instruction_type
LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
WHERE dossier_instruction_type.dossier_instruction_type = <idx>";

$sql_dossier_autorisation_type_detaille="SELECT
dossier_autorisation_type_detaille.dossier_autorisation_type_detaille,
(dossier_autorisation_type_detaille.code||' ('||dossier_autorisation_type_detaille.libelle||')') as lib
FROM ".DB_PREFIXE."dossier_autorisation_type_detaille
ORDER BY lib";

$sql_dossier_autorisation_type_detaille_by_id = "SELECT
dossier_autorisation_type_detaille.dossier_autorisation_type_detaille,
(dossier_autorisation_type_detaille.code||' ('||dossier_autorisation_type_detaille.libelle||')') as lib
FROM ".DB_PREFIXE."dossier_autorisation_type_detaille
WHERE dossier_autorisation_type_detaille = <idx>";

$sql_evenement="SELECT evenement.evenement, evenement.libelle as lib FROM ".DB_PREFIXE."evenement
ORDER BY lib";
$sql_evenement_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";

// Liaison NaN - demande_type/etat_dossier_autorisation
$sql_etats_autorises = "
SELECT
    etat.etat,
    etat.libelle as lib
FROM ".DB_PREFIXE."etat
ORDER BY lib";

$sql_etats_autorises_by_id = "
SELECT
    etat.etat,
    etat.libelle as lib
FROM ".DB_PREFIXE."etat
WHERE etat.etat IN (<idx>)
ORDER BY lib";

// Requête permettant de récupérer les types de dossier d'instruction par rapport
// au type détaillé de dossier d'autorisation
$sql_dossier_instruction_type_by_dossier_autorisation_type_detaille = "
SELECT 
    dossier_instruction_type.dossier_instruction_type,
    (dossier_autorisation_type_detaille.code||' - '||dossier_instruction_type.libelle) as lib
FROM ".DB_PREFIXE."dossier_instruction_type
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
        ON dossier_instruction_type.dossier_autorisation_type_detaille = dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
WHERE dossier_autorisation_type_detaille.dossier_autorisation_type_detaille = <idx_dossier_autorisation_type_detaille>
ORDER BY dossier_autorisation_type_detaille.code, dossier_instruction_type.libelle";

// Requête permettant de récupérer les types détaillés de dossier d'autorisation 
// par rapport au groupe
$sql_dossier_autorisation_type_detaille_by_groupe = "
SELECT 
    dossier_autorisation_type_detaille.dossier_autorisation_type_detaille,
    (dossier_autorisation_type_detaille.code||' ('||dossier_autorisation_type_detaille.libelle||')') as lib
FROM ".DB_PREFIXE."dossier_autorisation_type_detaille
    LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
        ON dossier_autorisation_type_detaille.dossier_autorisation_type = dossier_autorisation_type.dossier_autorisation_type
WHERE dossier_autorisation_type.groupe = <idx_groupe>
ORDER BY dossier_autorisation_type_detaille.code, dossier_autorisation_type_detaille.libelle";

?>