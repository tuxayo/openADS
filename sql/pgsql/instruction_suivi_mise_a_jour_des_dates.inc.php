<?php
/**
 * Surcharge de la classe instruction afin d'afficher une entrée menu pour 
 * la mise à jour des dates de l'instruction.
 * 
 * @package openfoncier
 * @version SVN : $Id$
 */

require_once "../sql/pgsql/instruction.inc.php";

// Fil d'ariane
$ent = _("suivi")." -> "._("suivi des pieces")." -> "._("mise a jour des dates");

//
$sousformulaire = array();

//
$tab_title = _("instruction");

?>
