<?php
//$Id: demande_avis_encours.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 18/10/2012 16:21
include('demande_avis.inc.php');
$ent = _("Demandes d'avis")." -> "._("en cours");
$tab_title = _("Demandes d'avis en cours");

//
$case_marque = "
CASE
    consultation.marque
WHEN
    't'
THEN
    '<span class=\"om-prev-icon om-icon-16 om-icon-fix marque-16\" title=\""._('Oui')."\"> </span>'
END";
array_push($champAffiche, $case_marque." as \""._("marqué")."\"");

$selection=' WHERE consultation.date_limite >= current_date
AND consultation.avis_consultation IS NULL
AND om_utilisateur.login=\''.$_SESSION['login'].'\'';
$tri="ORDER BY consultation.marque DESC, consultation.date_limite::date ASC, consultation.consultation";

?>