<?php
//$Id: demande.form.inc.php 4466 2015-03-26 17:46:48Z stimezouaght $ 
//gen openMairie le 08/11/2012 14:00

include('../gen/sql/pgsql/demande.form.inc.php');

$idz = "";
if (isset($_GET['idz']) && trim($_GET['idz']) != '') {
  $idz = $_GET['idz'];
}

/*Modification des requête générées*/
$sql_lien_demande_demandeur = "SELECT petitionnaire_principal,
        demandeur,
        demande
FROM ".DB_PREFIXE."lien_demande_demandeur
WHERE demande = <demande>";

$tableSelect .= 
" LEFT JOIN ".DB_PREFIXE."dossier 
    ON demande.dossier_instruction = dossier.dossier ";

$champs=array(
    "demande",
    "demande.om_collectivite",
    "dossier_autorisation_type_detaille",
    "demande_type",
    "dossier_instruction",
    "demande.dossier_autorisation",
    "dossier.etat as \"etat\"",
    "demande.autorisation_contestee",
    "demande.date_demande",
    "demande.terrain_references_cadastrales",
    "demande.terrain_adresse_voie_numero",
    "demande.terrain_adresse_voie",
    "demande.terrain_adresse_lieu_dit",
    "demande.terrain_adresse_localite",
    "demande.terrain_adresse_code_postal",
    "demande.terrain_adresse_bp",
    "demande.terrain_adresse_cedex",
    "demande.terrain_superficie",
    "instruction_recepisse",
    "arrondissement");

$sql_infos_dossier = "SELECT
        dossier_autorisation.dossier_autorisation,
        dossier_autorisation.dossier_autorisation_type_detaille,
        dossier_autorisation.depot_initial,
        dossier_autorisation.terrain_references_cadastrales,
        dossier_autorisation.terrain_adresse_voie_numero,
        dossier_autorisation.terrain_adresse_voie,
        dossier_autorisation.terrain_adresse_lieu_dit,
        dossier_autorisation.terrain_adresse_localite,
        dossier_autorisation.terrain_adresse_code_postal,
        dossier_autorisation.terrain_adresse_bp,
        dossier_autorisation.terrain_adresse_cedex,
        dossier_autorisation.terrain_superficie,
        etat.libelle as etat
        FROM ".DB_PREFIXE."dossier_autorisation
        INNER JOIN ".DB_PREFIXE."dossier
            ON dossier_autorisation.dossier_autorisation=dossier.dossier_autorisation
            LEFT JOIN ".DB_PREFIXE."etat
    ON dossier.etat = etat.etat
        WHERE dossier = '<idx>'";

$sql_demande_type_details_by_id = "SELECT demande_type.demande_type, demande_type.libelle,
 demande_type.dossier_autorisation_type_detaille,
 demande_type. dossier_instruction_type 
 FROM ".DB_PREFIXE."demande_type WHERE demande_type = <idx>";

$sql_autreDossierEnCour = "SELECT count(*) FROM ".DB_PREFIXE."dossier as a
                            JOIN ".DB_PREFIXE."dossier_autorisation ON dossier_autorisation.dossier_autorisation = a.dossier_autorisation
                            JOIN ".DB_PREFIXE."dossier as b ON b.dossier_autorisation = dossier_autorisation.dossier_autorisation
                            JOIN ".DB_PREFIXE."etat ON etat.etat = b.etat
                            WHERE etat.statut='encours' AND a.dossier='<idx>'";

$sql_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle
FROM ".DB_PREFIXE."arrondissement ORDER BY NULLIF(arrondissement.libelle,'')::int ASC NULLS LAST";

$sql_dossier_autorisation_type_detaille=" SELECT
  dossier_autorisation_type_detaille.dossier_autorisation_type_detaille,
  dossier_autorisation_type_detaille.libelle
FROM ".DB_PREFIXE."dossier_autorisation_type_detaille
LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type
    ON dossier_autorisation_type_detaille.dossier_autorisation_type=dossier_autorisation_type.dossier_autorisation_type
LEFT JOIN ".DB_PREFIXE."groupe
    ON dossier_autorisation_type.groupe=groupe.groupe
LEFT JOIN ".DB_PREFIXE."cerfa ON dossier_autorisation_type_detaille.cerfa = cerfa.cerfa
WHERE ((now()<=om_validite_fin AND now()>=om_validite_debut) OR
    dossier_autorisation_type_detaille.cerfa IS NULL OR
    (om_validite_fin IS NULL and om_validite_debut IS NULL) OR
    (now()<=om_validite_fin and om_validite_debut IS NULL) OR
    (om_validite_fin IS NULL AND now()>=om_validite_debut))
    <ajout_condition_requete>
ORDER BY dossier_autorisation_type_detaille.libelle ASC";


// Requête permettant de récupérer les types de demande par rapport au type 
// détaillé du dossier d'autorisation et à la nature de la demande
$sql_demande_type_by_dossier_autorisation_type_detaille = "
SELECT 
  demande_type.demande_type, demande_type.libelle as lib 
FROM ".DB_PREFIXE."demande_type
  LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
      ON demande_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
  LEFT JOIN ".DB_PREFIXE."lien_demande_type_etat
      ON lien_demande_type_etat.demande_type=demande_type.demande_type
  LEFT JOIN ".DB_PREFIXE."etat
      ON lien_demande_type_etat.etat=etat.etat
WHERE dossier_autorisation_type_detaille.dossier_autorisation_type_detaille = <idx_dossier_autorisation_type_detaille>
AND demande_type.demande_nature = <idx_demande_nature>
<ajout_condition_requête>
ORDER BY demande_type.libelle, demande_type.code
";
?>