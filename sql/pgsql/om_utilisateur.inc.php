<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id: om_utilisateur.inc.php 4651 2015-04-26 09:15:48Z tbenita $
 */

//
include "../core/sql/pgsql/om_utilisateur.inc.php";
$ent = _("administration")." -> "._("gestion des utilisateurs")." -> "._("om_utilisateur");

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'instructeur',
    'lien_service_om_utilisateur',
    'lien_om_utilisateur_groupe',
);

$sousformulaire_parameters['lien_om_utilisateur_groupe']['title'] = _('Groupe');
