<?php
//$Id: dossier_message.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 15/11/2012 18:30

include('../gen/sql/pgsql/dossier_message.form.inc.php');

// Champs du formulaire
$champs = array(
    "dossier_message",
    "dossier",
    "type",
    "categorie",
    "emetteur",
    "destinataire",
    'to_char(date_emission ,\'DD/MM/YYYY HH24:MI:SS\') as date_emission',
    "lu",
    "contenu");

// Requête nécessaire pour la recherche avancée des messages dans les listings
// de messages non lus. Elle présente la liste des types de message.
$sql_messages_type = "
SELECT
    DISTINCT dossier_message.type,
    dossier_message.type
FROM
    ".DB_PREFIXE."dossier_message
    LEFT JOIN ".DB_PREFIXE."dossier
        ON dossier_message.dossier = dossier.dossier
    LEFT JOIN ".DB_PREFIXE."instructeur
        ON dossier.instructeur = instructeur.instructeur
WHERE
    instructeur.division = (
        SELECT 
            division
        FROM
            ".DB_PREFIXE."instructeur
        WHERE 
            instructeur.om_utilisateur = (
                SELECT
                    om_utilisateur
                FROM
                    ".DB_PREFIXE."om_utilisateur
                WHERE
                    login = '".$_SESSION['login']."'
            )
    )    
ORDER BY
    dossier_message.type ASC
    ";

?>
