<?php
//$Id$ 
//gen openMairie le 14/06/2016 18:06

include "../gen/sql/pgsql/commission.form.inc.php";

$champs=array(
    "commission",
    "code",
    "om_collectivite",
    "commission_type",
    "libelle",
    "date_commission",
    "heure_commission",
    "lieu_adresse_ligne1",
    "lieu_adresse_ligne2",
    "lieu_salle",
    "listes_de_diffusion",
    "participants",
    "om_fichier_commission_ordre_jour",
    "om_final_commission_ordre_jour",
    "om_fichier_commission_compte_rendu",
    "om_final_commission_compte_rendu");

$sql_commission_type_by_collectivite="SELECT commission_type.commission_type, commission_type.libelle FROM " . DB_PREFIXE . "commission_type WHERE ((commission_type.om_validite_debut IS NULL AND (commission_type.om_validite_fin IS NULL OR commission_type.om_validite_fin > CURRENT_DATE)) OR (commission_type.om_validite_debut <= CURRENT_DATE AND (commission_type.om_validite_fin IS NULL OR commission_type.om_validite_fin > CURRENT_DATE))) AND commission_type.om_collectivite = <id_collectivite> ORDER BY commission_type.libelle ASC";

$sql_commission_type_sans_resultat="SELECT commission_type.commission_type, commission_type.libelle FROM " . DB_PREFIXE . "commission_type WHERE 0 = 1";

$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM " . DB_PREFIXE . "om_collectivite WHERE om_collectivite.niveau = '1' ORDER BY om_collectivite.libelle ASC";

?>
