<?php
//$Id: service.form.inc.php 4859 2015-06-24 16:31:05Z stimezouaght $ 
//gen openMairie le 10/02/2011 20:36 
include('../gen/sql/pgsql/service.form.inc.php');
$champs=array(
    "service",
    "abrege",
    "libelle",
    "adresse",
    "adresse2",
    "cp",
    "ville",
    "email",
    "notification_email",
    "delai_type",
    "delai",
    "consultation_papier",
    "om_validite_debut",
    "om_validite_fin",
    "om_collectivite",
    "type_consultation",
    "edition");

$sql_edition="SELECT om_etat.om_etat, om_etat.libelle FROM ".DB_PREFIXE."om_etat WHERE id LIKE 'consultation_%' ORDER BY om_etat.libelle ASC";
?>