<?php
//
include "../sql/pgsql/architecte.inc.php";

//
$ent = _("instruction")." -> "._("qualification")." -> "._("architecte_frequent");
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".mb_strtoupper($idz, "UTF-8")."&nbsp;";
}

// SELECT 
$champAffiche = array(
    'architecte.architecte as "'._("id").'"',
    $concat_nom_prenom.' as "'._("nom").'"',
    $concat_adresse.' as "'._("adresse").'"',
    'architecte.inscription as "'._("inscription").'"',
);

//
$selection = "WHERE architecte.frequent='t' ";

?>
