<?php
//$Id: dossier_instruction_type.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 07/11/2012 12:55

include('../gen/sql/pgsql/dossier_instruction_type.form.inc.php');

$sql_dossier_autorisation_type_detaille="SELECT
dossier_autorisation_type_detaille.dossier_autorisation_type_detaille,
(dossier_autorisation_type_detaille.code||' ('||dossier_autorisation_type_detaille.libelle||')') as lib
FROM ".DB_PREFIXE."dossier_autorisation_type_detaille
ORDER BY lib";

$sql_dossier_autorisation_type_detaille_by_id="SELECT
dossier_autorisation_type_detaille.dossier_autorisation_type_detaille,
(dossier_autorisation_type_detaille.code||' ('||dossier_autorisation_type_detaille.libelle||')') as lib
FROM ".DB_PREFIXE."dossier_autorisation_type_detaille
WHERE dossier_autorisation_type_detaille = <idx>";

?>