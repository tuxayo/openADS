<?php
//$Id: dossier_autorisation.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 14/11/2012 12:54

include('../gen/sql/pgsql/dossier_autorisation.form.inc.php');

/*Ajout d'une action au portlet action*/
$portlet_actions['nouvelle_demande'] =
    array('lien' => '../scr/form.php?obj=demande&amp;idx_dossier=',
          'id' => '&amp;action=0',
          'lib' => "<span class=\"om-prev-icon om-icon-16 add-16\" title=\""._("Nouvelle demande")."\">"._("Nouvelle demande")."</span>",
          'ordre' => 5,);

$sql_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle
FROM ".DB_PREFIXE."arrondissement ORDER BY NULLIF(arrondissement.libelle,'')::int ASC NULLS LAST";
?>