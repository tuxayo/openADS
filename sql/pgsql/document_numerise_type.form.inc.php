<?php
//$Id: document_numerise_type.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 02/05/2013 15:03

include('../gen/sql/pgsql/document_numerise_type.form.inc.php');

$tableSelect=DB_PREFIXE."document_numerise_type
LEFT JOIN ".DB_PREFIXE."lien_document_numerise_type_instructeur_qualite
  ON lien_document_numerise_type_instructeur_qualite.document_numerise_type=document_numerise_type.document_numerise_type
LEFT JOIN ".DB_PREFIXE."instructeur_qualite
  ON lien_document_numerise_type_instructeur_qualite.instructeur_qualite=instructeur_qualite.instructeur_qualite";

$champs=array(
    "document_numerise_type.document_numerise_type",
    "document_numerise_type.code",
    "document_numerise_type.libelle",
    "document_numerise_type.document_numerise_type_categorie",
    "array_to_string(array_agg(lien_document_numerise_type_instructeur_qualite.instructeur_qualite
                        ORDER BY instructeur_qualite.libelle), ';') as instructeur_qualite",
    "document_numerise_type.aff_service_consulte",
    "document_numerise_type.aff_da",
    "document_numerise_type.synchro_metadonnee");
    
$selection = " GROUP BY document_numerise_type.document_numerise_type";
    
// Liaison NaN - demande_type/etat_dossier_autorisation
$sql_instructeur_qualite = "
SELECT
    instructeur_qualite.instructeur_qualite,
    instructeur_qualite.libelle as lib
FROM ".DB_PREFIXE."instructeur_qualite
ORDER BY lib";

$sql_instructeur_qualite_by_id = "
SELECT
    instructeur_qualite.instructeur_qualite,
    instructeur_qualite.libelle as lib
FROM ".DB_PREFIXE."instructeur_qualite
WHERE instructeur_qualite.instructeur_qualite IN (<idx>)
ORDER BY lib";
