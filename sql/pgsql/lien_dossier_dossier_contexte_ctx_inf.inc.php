<?php
/**
 * Script de paramétrage du listing des dossiers liés.
 *
 * Dans le contexte d'un dossier contentieux infraction. 
 *
 * @package openads
 * @version SVN : $Id: lien_dossier_dossier_contexte_ctx_inf.inc.php 6565 2017-04-21 16:14:15Z softime $
 */

include ('../sql/pgsql/lien_dossier_dossier_contexte_ctx.inc.php');

?>