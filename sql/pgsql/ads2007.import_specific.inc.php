<?php
//$Id: ads2007.import_specific.inc.php 4818 2015-06-09 11:06:14Z nhaye $ 
//gen openMairie le 10/02/2011 20:39 
$import= "Import des dossiers ADS 2007 clôturés";
$treatment = "ads2007";
$table= DB_PREFIXE."dossier";
$id=''; // numerotation non automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$DEBUG=0; // =0 pas d affichage messages / =1 affichage detail enregistrement
$fic_erreur=1; // =0 pas de fichier d erreur / =1  fichier erreur
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon


$column = array();

// Type
$column["type"] = array(
    "header" => "Type",
    "type"=> "string",
    "require" => true,
    "link" => array(
        "PC MI" => "PI",
        "PA PC" => "PA",
        "PC PD" => "PC",
        "PA PD" => "PA",
        ),
    "foreign_key" => array(
        "sql"=>"SELECT dossier_autorisation_type_detaille FROM ".DB_PREFIXE."dossier_autorisation_type_detaille WHERE code ='<value>'",
        "table" => "dossier_autorisation_type_detaille",
        "field" => "code",
    ),
    );
// Numéro
$column["numero"] = array(
    "header" => "Numéro",
    "type"=> "string",
    "require" => true,
    );
// Initial
$column["initial"] = array(
    "header" => "Initial",
    "type"=> "string",
    "require" => false,

    );
// INSEE
$column["insee"] = array(
    "header" => "INSEE",
    "type"=> "integer",
    "require" => true,
    );
// Commune
$column["commune"] = array(
    "header" => "Commune",
    "type"=> "string",
    "require" => false,
    );
// Autonome
$column["autonome"] = array(
    "header" => "Autonome",
    "type"=> "boolean",
    "require" => false,
    );
// Projet
$column["projet"] = array(
    "header" => "Projet",
    "type"=> "string",
    "require" => false,
    );
// Destination
$column["destination"] = array(
    "header" => "Destination",
    "type"=> "string",
    "require" => false,
    );
// Nb logements
$column["nb_logements"] = array(
    "header" => "Nb logements",
    "type"=> "integer",
    "require" => false,
    );
// Surface terrain
$column["surface_terrain"] = array(
    "header" => "Surface terrain",
    "type"=> "float",
    "require" => false,
    );
// SHON existante
$column["shon_existante"] = array(
    "header" => "SHON existante",
    "type"=> "float",
    "require" => false,
    );
// SHON construite
$column["shon_construite"] = array(
    "header" => "SHON construite",
    "type"=> "float",
    "require" => false,
    );
// SHON transformation SHOB
$column["shon_transformation_shob"] = array(
    "header" => "SHON transformation SHOB",
    "type"=> "float",
    "require" => false,
    );
// SHON changement destination
$column["shon_changement_destination"] = array(
    "header" => "SHON changement destination",
    "type"=> "float",
    "require" => false,
    );
// SHON démolie
$column["shon_demolie"] = array(
    "header" => "SHON démolie",
    "type"=> "float",
    "require" => false,
    );
// SHON supprimée
$column["shon_supprimee"] = array(
    "header" => "SHON supprimée",
    "type"=> "float",
    "require" => false,
    );
// Architecte
$column["architecte"] = array(
    "header" => "Architecte",
    "type"=> "boolean",
    "require" => false,
    );
// Demandeur
$column["demandeur"] = array(
    "header" => "Demandeur",
    "type"=> "string",
    "require" => true,
    );
// Opposition CNIL
$column["opposition_cnil"] = array(
    "header" => "Opposition CNIL",
    "type"=> "boolean",
    "require" => false,
    );
// Adresse demandeur
$column["adresse_demandeur"] = array(
    "header" => "Adresse demandeur",
    "type"=> "string",
    "require" => false,
    );
// Terrain
$column["terrain"] = array(
    "header" => "Terrain",
    "type"=> "string",
    "require" => false,
    );
// Références cadastrales
$column["references_cadastrales"] = array(
    "header" => "Références cadastrales",
    "type"=> "string",
    "require" => false,
    );
// Lotissement
$column["lotissement"] = array(
    "header" => "Lotissement",
    "type"=> "boolean",
    "require" => false,
    );
// AFU
$column["afu"] = array(
    "header" => "AFU",
    "type"=> "boolean",
    "require" => false,
    );
// Détail ZAC AFU
$column["detail_zac_afu"] = array(
    "header" => "Détail ZAC AFU",
    "type"=> "string",
    "require" => false,
    );
// Autorité
$column["autorite"] = array(
    "header" => "Autorité",
    "type"=> "string",
    "require" => true,
    "link" => array(
        "Le préfet Au nom de l'État" => "ETAT",
        "Le maire Au nom de l'État" => "ETATMAIRE",
        "Le maire Au nom de la commune" => "COM",
        ),

    "foreign_key" => array(
        "sql"=>"SELECT autorite_competente FROM ".DB_PREFIXE."autorite_competente WHERE code ='<value>'",
        "table" => "autorite_competente",
        "field" => "code",
    )
    );
// Etat
$column["etat"] = array(
    "header" => "Etat",
    "type"=> "string",
    "require" => true,
    "link" => array(
        "Décision notifiée au demandeur" => "terminer",
        "Non instruit" => "annule",
        "Rejet tacite" => "refuse_tacite",
        "Refus tacite présumé" => "refuse_tacite",
        "Accord tacite présumé" => "accepte_tacite",
        ),
    "foreign_key" => array(
        "sql"=>"SELECT etat FROM ".DB_PREFIXE."etat WHERE etat ='<value>'",
        "table" => "etat",
        "field" => "etat",
    )
    );
// Centre instructeur
$column["centre_instructeur"] = array(
    "header" => "Centre instructeur",
    "type"=> "string",
    "require" => false,
    );
// Instructeur
$column["instructeur"] = array(
    "header" => "Instructeur",
    "type"=> "string",
    "require" => false,
    );
// Liquidateur
$column["liquidateur"] = array(
    "header" => "Liquidateur",
    "type"=> "string",
    "require" => false,
    );
// Complexité
$column["complexite"] = array(
    "header" => "Complexité",
    "type"=> "string",
    "require" => false,
    );
// Dépôt en mairie
$column["depot_en_mairie"] = array(
    "header" => "Dépôt en mairie",
    "type"=> "date",
    "require" => true,
    );
// Réception DDE
$column["reception_dde"] = array(
    "header" => "Réception DDE",
    "type"=> "date",
    "require" => true,
    );
// Complétude
$column["completude"] = array(
    "header" => "Complétude",
    "type"=> "date",
    "require" => false,
    );
// Notification majoration
$column["notification_majoration"] = array(
    "header" => "Notification majoration",
    "type"=> "date",
    "require" => false,
    );
// DLI
$column["dli"] = array(
    "header" => "DLI",
    "type"=> "date",
    "require" => true,
    );
// Date envoi demande de pièces
$column["date_envoi_demande_de_pieces"] = array(
    "header" => "Date envoi demande de pièces",
    "type"=> "date",
    "require" => false,
    );
// Date notification demande de pièces
$column["date_notification_demande_de_pieces"] = array(
    "header" => "Date notification demande de pièces",
    "type"=> "date",
    "require" => false,
    );
// Date envoi délai majoration
$column["date_envoi_delai_majoration"] = array(
    "header" => "Date envoi délai majoration",
    "type"=> "date",
    "require" => false,
    );
// Date notification délai majoration
$column["date_notification_delai_majoration"] = array(
    "header" => "Date notification délai majoration",
    "type"=> "date",
    "require" => false,
    );
// Service consulté
$column["service_consulte"] = array(
    "header" => "Service consulté",
    "type"=> "string",
    "require" => false,
    );
// Proposition service
$column["proposition_service"] = array(
    "header" => "Proposition service",
    "type"=> "string",
    "require" => false,
    );
// Date proposition service
$column["date_proposition_service"] = array(
    "header" => "Date proposition service",
    "type"=> "date",
    "require" => false,
    );
// Date transmission proposition
$column["date_transmission_proposition"] = array(
    "header" => "Date transmission proposition",
    "type"=> "date",
    "require" => false,
    );
// Date instruction terminée
$column["date_instruction_terminee"] = array(
    "header" => "Date instruction terminée",
    "type"=> "date",
    "require" => false,
    );
// Date accord tacite
$column["date_accord_tacite"] = array(
    "header" => "Date accord tacite",
    "type"=> "date",
    "require" => false,
    );
// Date de rejet tacite
$column["date_de_rejet_tacite"] = array(
    "header" => "Date de rejet tacite",
    "type"=> "date",
    "require" => false,
    );
// Date de refus tacite
$column["date_de_refus_tacite"] = array(
    "header" => "Date de refus tacite",
    "type"=> "date",
    "require" => false,
    );
// Date de décision
$column["date_de_decision"] = array(
    "header" => "Date de décision",
    "type"=> "date",
    "require" => false,
    );
// Date notification décision
$column["date_notification_decision"] = array(
    "header" => "Date notification décision",
    "type"=> "date",
    "require" => false,
    );
// Nature décision
$column["nature_decision"] = array(
    "header" => "Nature décision",
    "type"=> "string",
    "require" => false,
    "link" => array(
        "Accord sous condition" => "Favorable avec Reserves",
        "Accord" => "Favorable",
        "Sursis à statuer" => "Sursis a statuer",
        "Refus" => "Defavorable",
        ),
    "foreign_key" => array(
        "sql"=>"SELECT avis_decision FROM ".DB_PREFIXE."avis_decision WHERE libelle ='<value>'",
        "table" => "avis_decision",
        "field" => "libelle",
    )
    );
// Récolement
$column["recolement"] = array(
    "header" => "Récolement",
    "type"=> "string",
    "require" => false,
    );
// DOC
$column["doc"] = array(
    "header" => "DOC",
    "type"=> "date",
    "require" => false,
    );
// DAACT
$column["daact"] = array(
    "header" => "DAACT",
    "type"=> "date",
    "require" => false,
    );
// Type Evolution
$column["type_evolution"] = array(
    "header" => "Type Evolution",
    "type"=> "string",
    "require" => false,
    );
// Statut Evolution
$column["statut_evolution"] = array(
    "header" => "Statut Evolution",
    "type"=> "string",
    "require" => false,
    );
// Type dernières taxes
$column["type_dernieres_taxes"] = array(
    "header" => "Type dernières taxes",
    "type"=> "string",
    "require" => false,
    );
// Statut dernières taxes
$column["statut_dernieres_taxes"] = array(
    "header" => "Statut dernières taxes",
    "type"=> "string",
    "require" => false,
    );
// Type dernière RAP
$column["type_derniere_rap"] = array(
    "header" => "Type dernière RAP",
    "type"=> "string",
    "require" => false,
    );
// Statut dernière RAP
$column["statut_derniere_rap"] = array(
    "header" => "Statut dernière RAP",
    "type"=> "string",
    "require" => false,
    );
// EPCI
$column["epci"] = array(
    "header" => "EPCI",
    "type"=> "string",
    "require" => false,
    );

?>