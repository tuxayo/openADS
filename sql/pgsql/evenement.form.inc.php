<?php
/**
 *
 *
 * @package openfoncier
 * @version SVN : $Id: evenement.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $
 */

//
include "../gen/sql/pgsql/evenement.form.inc.php";

// Récupère le paramètre "retourformulaire"
$retourformulaire = (isset($_GET['retourformulaire'])) ? $_GET['retourformulaire'] : "";

// XXX Si c'est un sous-formulaire, alors le bouton "Modifier" n'est pas affiché
// pour éviter le bug concernant les tableaux multi-select
// À retirer lorsque le bug sera résolu dans OpenMairie
if (isset($retourformulaire) && $retourformulaire != '') {
    // Actions du porrtlet : modifier
    $portlet_actions['modifier'] = null;
}

//
$champs = array(
    "evenement.evenement",
    "evenement.libelle",
    "evenement.type",
    "evenement.non_verrouillable",
    "evenement.retour",
    "array_to_string(array_agg(distinct(transition.etat) ORDER BY transition.etat), ';') as etats_depuis_lequel_l_evenement_est_disponible",
    "array_to_string(array_agg(distinct(dossier_instruction_type) ORDER BY dossier_instruction_type), ';') as dossier_instruction_type",
    "evenement.restriction",
    "evenement.action",
    "evenement.etat",
    "evenement.delai",
    "evenement.accord_tacite",
    "evenement.delai_notification",
    "evenement.avis_decision",
    "evenement.autorite_competente",
    "evenement.lettretype",
    "evenement.consultation",
    "evenement.phase",
    "evenement.evenement_suivant_tacite",
    "evenement.evenement_retour_ar",
    "evenement.evenement_retour_signature",
);

//
$tableSelect = "
".DB_PREFIXE."evenement 
  LEFT JOIN ".DB_PREFIXE."lien_dossier_instruction_type_evenement 
    ON lien_dossier_instruction_type_evenement.evenement=evenement.evenement
  LEFT JOIN ".DB_PREFIXE."transition 
    ON transition.evenement=evenement.evenement";

//
$selection = " GROUP BY evenement.evenement, evenement.libelle ";

// FKEY - OM_LETTRETYPE
$sql_om_lettretype = "
SELECT
  id,
  (id||' '||libelle) as lib
FROM ".DB_PREFIXE."om_lettretype
ORDER BY lib";

$sql_action="SELECT action.action, action.libelle as lib FROM ".DB_PREFIXE."action ORDER BY lib";
$sql_etat="SELECT etat.etat, etat.libelle as lib FROM ".DB_PREFIXE."etat ORDER BY lib";
$sql_avis_decision="SELECT avis_decision.avis_decision, avis_decision.libelle as lib FROM ".DB_PREFIXE."avis_decision ORDER BY lib";
$sql_evenement_retour_ar="SELECT evenement.evenement, evenement.libelle as lib FROM ".DB_PREFIXE."evenement ORDER BY lib";
$sql_evenement_suivant_tacite="SELECT evenement.evenement, evenement.libelle as lib FROM ".DB_PREFIXE."evenement ORDER BY lib";
$sql_evenement_retour_signature="SELECT evenement.evenement, evenement.libelle as lib FROM ".DB_PREFIXE."evenement ORDER BY lib";


// LIAISON NaN - DOSSIER_INSTRUCTION_TYPE (LIEN_DOSSIER_INSTRUCTION_TYPE_EVENEMENT)
$sql_dossier_instruction_type = "
SELECT 
  dossier_instruction_type.dossier_instruction_type, 
  CONCAT(dossier_autorisation_type_detaille.code,' - ',dossier_instruction_type.code,' - ',dossier_instruction_type.libelle) as lib
FROM ".DB_PREFIXE."dossier_instruction_type 
  LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
    ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
ORDER BY dossier_autorisation_type_detaille.code";
$sql_dossier_instruction_type_by_id = "
SELECT 
  dossier_instruction_type.dossier_instruction_type, 
  CONCAT(dossier_autorisation_type_detaille.code,' - ',dossier_instruction_type.code,' - ',dossier_instruction_type.libelle) as lib
FROM ".DB_PREFIXE."dossier_instruction_type 
  LEFT JOIN ".DB_PREFIXE."dossier_autorisation_type_detaille
    ON dossier_instruction_type.dossier_autorisation_type_detaille=dossier_autorisation_type_detaille.dossier_autorisation_type_detaille
WHERE dossier_instruction_type IN (<idx>) 
ORDER BY dossier_autorisation_type_detaille.code";

// LIAISON NaN - ETAT (TRANSITION)
$sql_etats_depuis_lequel_l_evenement_est_disponible = "
SELECT
  etat.etat,
  etat.libelle as lib
FROM ".DB_PREFIXE."etat
ORDER BY lib";
$sql_etats_depuis_lequel_l_evenement_est_disponible_by_id = "
SELECT
  etat.etat,
  etat.libelle as lib
FROM ".DB_PREFIXE."etat 
WHERE etat.etat IN (<idx>) 
ORDER BY lib";

// Filtre le select evenement_retour_signature pour n'afficher que les
// événements "retour"
$sql_evenement_retour_signature="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement.retour = 't' ORDER BY evenement.libelle ASC";
$sql_evenement_retour_signature_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";

// Filtre le select evenement_retour_ar pour n'afficher que les
// événements "retour"
$sql_evenement_retour_ar="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement.retour = 't' ORDER BY evenement.libelle ASC";
$sql_evenement_retour_ar_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";

// Filtre le select evenement_suivant_tacite pour ne pas afficher les
// événements "retour"
$sql_evenement_suivant_tacite="SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement.retour = 'f' ORDER BY evenement.libelle ASC";
$sql_evenement_suivant_tacite_by_id = "SELECT evenement.evenement, evenement.libelle FROM ".DB_PREFIXE."evenement WHERE evenement = <idx>";

?>