<?php
//$Id: demandeur.form.inc.php 4765 2015-05-13 16:57:48Z nhaye $ 
//gen openMairie le 08/11/2012 14:59

include('../gen/sql/pgsql/demandeur.form.inc.php');

$champs = array(
    'demandeur',
    'type_demandeur',
    'qualite',
    'om_collectivite',
    'particulier_civilite', 'particulier_nom', 'particulier_prenom',
    'particulier_date_naissance', 'particulier_commune_naissance', 
    'particulier_departement_naissance',
    'personne_morale_denomination', 
    'personne_morale_raison_sociale', 'personne_morale_siret',
    'personne_morale_categorie_juridique',
    'personne_morale_civilite', 'personne_morale_nom', 'personne_morale_prenom',
    'numero',
    'voie',
    'complement',
    'lieu_dit',
    'localite',
    'code_postal',
    'bp',
    'cedex',
    'pays',
    'division_territoriale',
    'telephone_fixe',
    'telephone_mobile',
    'indicatif',
    'fax',
    'courriel',
    'notification',
    'frequent',
);

?>