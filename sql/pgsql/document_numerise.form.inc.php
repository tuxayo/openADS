<?php
//$Id: document_numerise.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 02/05/2013 15:03

include('../gen/sql/pgsql/document_numerise.form.inc.php');

// Surcharge de la requête de récupération du select de type de pièces
$sql_document_numerise_type_for_user = "
SELECT document_numerise_type.document_numerise_type, document_numerise_type.libelle
FROM ".DB_PREFIXE."document_numerise_type 
JOIN ".DB_PREFIXE."lien_document_numerise_type_instructeur_qualite ON 
lien_document_numerise_type_instructeur_qualite.document_numerise_type = document_numerise_type.document_numerise_type
JOIN ".DB_PREFIXE."instructeur_qualite ON lien_document_numerise_type_instructeur_qualite.instructeur_qualite = instructeur_qualite.instructeur_qualite
JOIN ".DB_PREFIXE."instructeur ON instructeur.instructeur_qualite = instructeur_qualite.instructeur_qualite
JOIN ".DB_PREFIXE."om_utilisateur ON instructeur.om_utilisateur = om_utilisateur.om_utilisateur
WHERE om_utilisateur.login = '<om_utilisateur_login>'
ORDER BY document_numerise_type.libelle ASC";

?>