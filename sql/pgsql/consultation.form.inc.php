<?php
//$Id: consultation.form.inc.php 5870 2016-02-04 14:08:49Z jymadier $ 
//gen openMairie le 10/02/2011 20:32 
include('../gen/sql/pgsql/consultation.form.inc.php');

//
$tableSelect .= "
  LEFT JOIN ".DB_PREFIXE."dossier
    ON consultation.dossier = dossier.dossier";

//
$champs=array("consultation",
"dossier.dossier",
"dossier_libelle",
"service",
"to_char(consultation.date_envoi ,'DD/MM/YYYY') as \"date_envoi\"",
"visible",
"to_char(consultation.date_reception ,'DD/MM/YYYY') as \"date_reception\"",
"to_char(consultation.date_limite ,'DD/MM/YYYY') as \"date_limite\"",
"to_char(consultation.date_retour ,'DD/MM/YYYY') as \"date_retour\"",
"avis_consultation",
"motivation",
"fichier",
"lu",
"code_barres",
"om_fichier_consultation",
"om_final_consultation",
"marque"
);

$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle";


$sql_service_by_di="SELECT service.service, CONCAT(service.abrege, ' - ', service.libelle)
FROM ".DB_PREFIXE."service
LEFT JOIN ".DB_PREFIXE."om_collectivite ON service.om_collectivite = om_collectivite.om_collectivite
WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))
AND (om_collectivite.niveau = '2' OR service.om_collectivite = <collectivite_di>)
ORDER BY service.abrege, service.libelle";
$sql_service_by_id = "SELECT service.service, CONCAT(service.abrege, ' - ', service.libelle)
FROM ".DB_PREFIXE."service
WHERE service = <idx>";

?>