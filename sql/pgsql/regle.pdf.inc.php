<?php
//$Id: regle.pdf.inc.php 4418 2015-02-24 17:30:28Z tbenita $ 
//gen openMairie le 10/02/2011 20:38 
$DEBUG=0;
$orientation='L';// orientation P-> portrait L->paysage
$format='A4';// format A3 A4 A5
$police='arial';
$margeleft=10;// marge gauche
$margetop=5;// marge haut
$margeright=5;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1=0;// couleur texte  R
$C2=0;// couleur texte  V
$C3=0;// couleur texte  B
$size=10; //taille POLICE
$height=4; // hauteur ligne tableau 
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1=234;// couleur fond  R 241
$C2fond1=240;// couleur fond  V 241
$C3fond1=245;// couleur fond  B 241
$C1fond2=255;// couleur fond  R
$C2fond2=255;// couleur fond  V
$C3fond2=255;// couleur fond  B
$libtitre='Liste public.regle'; // libelle titre
$flagsessionliste=0;// 1 - > affichage session liste ou 0 -> pas d'affichage
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre='B';//$gras='B' -> BOLD OU $gras=''
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond=181;// couleur fond  R
$C2titrefond=182;// couleur fond  V
$C3titrefond=188;// couleur fond  B
$C1titre=75;// couleur texte  R
$C2titre=79;// couleur texte  V
$C3titre=81;// couleur texte  B
$sizetitre=15;
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=10;//hauteur ligne entete colonne
$C1fondentete=210;// couleur fond  R
$C2fondentete=216;// couleur fond  V
$C3fondentete=249;// couleur fond  B
$C1entetetxt=0;// couleur texte R
$C2entetetxt=0;// couleur texte V
$C3entetetxt=0;// couleur texte B
$C1border=159;// couleur texte  R
$C2border=160;// couleur texte  V
$C3border=167;// couleur texte  B
$l0=16; // largeur colone -> champs 0 - regle
$be0='L';// border entete colone
$b0='L';// border cellule colone
$ae0='C'; // align cellule entete colone
$a0='L';
$l1=40; // largeur colone -> champs 1 - sens
$be1='L';// border entete colone
$b1='L';// border cellule colone
$ae1='C'; // align cellule entete colone
$a1='L';
$l2=8; // largeur colone -> champs 2 - ordre
$be2='L';// border entete colone
$b2='L';// border cellule colone
$ae2='C'; // align cellule entete colone
$a2='L';
$l3=40; // largeur colone -> champs 3 - controle
$be3='L';// border entete colone
$b3='L';// border cellule colone
$ae3='C'; // align cellule entete colone
$a3='L';
$l4=16; // largeur colone -> champs 4 - id
$be4='L';// border entete colone
$b4='L';// border cellule colone
$ae4='C'; // align cellule entete colone
$a4='L';
$l5=40; // largeur colone -> champs 5 - champ
$be5='L';// border entete colone
$b5='L';// border cellule colone
$ae5='C'; // align cellule entete colone
$a5='L';
$l6=40; // largeur colone -> champs 6 - operateur
$be6='L';// border entete colone
$b6='L';// border cellule colone
$ae6='C'; // align cellule entete colone
$a6='L';
$l7=80; // largeur colone -> champs7 - valeur
$be7='LR';// border entete colone
$b7='LR';// border cellule colone
$ae7='C'; // align cellule entete colone
$a7='L';
$widthtableau=280;
$bt=1;// border 1ere  et derniere ligne  du tableau par page->0 ou 1
$sql="select regle, sens, ordre, controle, id, champ, operateur, valeur from ".DB_PREFIXE."regle";
?>