<?php
/**
 *
 *
 * @package openfoncier
 * @version SVN : $Id: etat.form.inc.php 4418 2015-02-24 17:30:28Z tbenita $
 */

//
include "../gen/sql/pgsql/etat.form.inc.php";

//
$champs = array (
    "etat.etat",
    "etat.libelle",
    "etat.statut",
    "array_to_string(array_agg(transition.evenement ORDER BY transition.evenement), ';') as evenement",
);

//
$tableSelect = DB_PREFIXE."etat
LEFT JOIN ".DB_PREFIXE."transition
    ON transition.etat=etat.etat";

//
$selection = " GROUP BY etat.etat, libelle ";

// LIAISON NaN - EVENEMENT (TRANSITION)
$sql_evenement="SELECT 
evenement.evenement,
evenement.libelle 
FROM ".DB_PREFIXE."evenement 
ORDER BY evenement.libelle";
$sql_evenement_by_id = "SELECT 
evenement.evenement,
evenement.libelle
FROM ".DB_PREFIXE."evenement 
WHERE evenement IN (<idx>) 
ORDER BY evenement.libelle";

?>
