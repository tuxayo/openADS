<?php
/**
 *
 *
 * @package openfoncier
 * @version SVN : $Id: instruction.inc.php 4418 2015-02-24 17:30:28Z tbenita $
 */

//
include "../gen/sql/pgsql/instruction.inc.php";

// =======================================================
// href special edition instruction
// =======================================================
$table=DB_PREFIXE."instruction inner join ".DB_PREFIXE."evenement on instruction.evenement=evenement.evenement 
    LEFT JOIN ".DB_PREFIXE."signataire_arrete 
        ON instruction.signataire_arrete=signataire_arrete.signataire_arrete
    LEFT JOIN ".DB_PREFIXE."dossier
        ON instruction.dossier = dossier.dossier
    LEFT JOIN ".DB_PREFIXE."etat
        ON instruction.etat = etat.etat";
$champAffiche=array("instruction as no",
                    "evenement.libelle",
                    "to_char(date_evenement ,'DD/MM/YYYY') as \""._("date_evenement")."\"",
                    "etat.libelle as \""._("etat")."\"",
                    "instruction.lettretype",
                    "signataire_arrete.nom as \""._("signataire_arrete")."\"",
                    );
$champRecherche=array(
    "evenement.libelle",
    );
$tri= " order by date_evenement asc, instruction asc ";
if(isset($idx)){
    $selection=" where dossier.dossier like '".$idx."'";
}else{
   $selection="";
}

// Gestion particulière de l'affichage du listing dans le contexte d'un dossier
// d'instruction.
include "../sql/pgsql/dossier_instruction_droit_specifique_par_division.inc.php";
// Gestion des groupes et confidentialité
include('../sql/pgsql/filter_group.inc.php');