<?php
//$Id: rapport_instruction.form.inc.php 4497 2015-04-03 09:51:27Z stimezouaght $ 
//gen openMairie le 07/01/2013 15:28

include('../gen/sql/pgsql/rapport_instruction.form.inc.php'); 

//
$tableSelect .= " 
  LEFT JOIN ".DB_PREFIXE."dossier
    ON rapport_instruction.dossier_instruction=dossier.dossier";

//
$champs=array(
    "rapport_instruction",
    "dossier_instruction",
    "dossier_libelle",
    "analyse_reglementaire_om_html",
    "description_projet_om_html",
    "complement_om_html",
    "proposition_decision",
    "om_fichier_rapport_instruction",
    "om_final_rapport_instruction"
    );

?>
