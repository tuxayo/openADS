<?php
/**
 * Variable servant à l'export SITADEL
 * 
 * @package openfoncier
 * @version SVN : $Id: export_sitadel.inc.php 5044 2015-08-14 16:34:39Z vpihour $
 */
//
$pf_departement="0";
//Variables
// etat civil
$val['civpart']="";
$val['prenompart']="";
$val['rspm']="";
$val['siret']="";
$val['catjur']="";
$val['civrep']="";
$val['prenomrep']="";
// delegataire
$val['civtier']="";
$val['prenomtier']="";
$val['typevoietier']="";
$val['bptier']="";
$val['cedextier']="";
$val['paystier']="100";
$val['divtertier']="";
// adresse
$val['numvoiemo']="";
$val['typvoiemo']="";
$val['bpmo']="";
$val['cedexmo']="";
$val['paysmo']="100";
$val['divtermo']="";
// terrain
$val['typvoiete']="";
$val['bpte']="";
$val['cedexte']="";
// cadastre
$val['scadastre1']="";
$val['ncadastre1']="";
$val['scadastre2']="";
$val['ncadastre2']="";
$val['scadastre3']="";
$val['ncadastre3']="";
// divers
$val['contrat']="0";
$val['cnil']="0";
// amenagement
$val['zac']="0";
$val['afu']="0";
// travaux
$val['annexe']="00000";
$val['niax']="";
// descriptif
$val['cpublic']="000000";
$val['nbmaison']="";
$val['nblogcoll']="";
$val['nbtotlog']="";
$val['natres']="0000000"; 
$val['libres']="";
$val['util']="00000";
$val['chambre']="";
$val['finis']="";
$val['finaa']="";
$val['finptz']="";
$val['finaf']="";
$val['piec1']="";
$val['piec2']="";
$val['piec3']="";
$val['piec4']="";
$val['piec5']="";
$val['piec6']="";

//$val['']="";
// suivi occupation
$val['nblogoc']="";
$val['nbmaisoc']="";
$val['nbcolloc']="";
$val['finisoc']="";
$val['finaaoc']="";
$val['finptzoc']="";
$val['finafoc']="";
$val['indoc']="";
// suivi achevement
$val['nblogat']="";
$val['nbmaisat']="";
$val['nbcollat']="";
$val['finisat']="";
$val['finaaat']="";
$val['finptzat']="";
$val['finafat']="";
$val['indat']="";
$val['origat']="1";
?>